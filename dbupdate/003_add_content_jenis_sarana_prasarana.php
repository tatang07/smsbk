<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_content_jenis_sarana_prasarana extends CI_Migration {

	public function up()
	{
		$this->db->query("INSERT INTO `r_jenis_sarana_prasarana` (`jenis_sarana_prasarana`) VALUES
			('Auditorium'),
			('Rangkuman Profil Sarana dan Prasarana'),
			('Ruang Kelas'),
			('Fasilitas Olah Raga'),
			('Laboratorium, Bengkel dan Studio'),
			('Teknologi Informasi dan Komunikasi'),
			('Peralatan Pendidikan'),
			('Perpustakaan Pendidikan'),
			('Penyimpanan'),
			('Pengamanan'),
			('Kafetaria/Warung'),
			('Mesjid/Mushala'),
			('WC/Kamar Mandi'),
			('Listrik dan Air'),
			('Sarana Prasarana Umum'),
			('Sarana Pembelajaran'),
			('Inventaris Buku');");
	}

	public function down()
	{

	}
}

