<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_table_kondisi extends CI_Migration {

	public function up()
	{
		$this->db->query("CREATE TABLE IF NOT EXISTS `r_kondisi` (
			`id` bigint(20) NOT NULL AUTO_INCREMENT,
			`kondisi` varchar(50) DEFAULT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");

		$this->db->query("INSERT INTO `r_kondisi` (`kondisi`) VALUES
			('Baik'),('Rusak Ringan'),('Rusak Berat');");
	}

	public function down()
	{
		$this->dbforge->drop_table('r_kondisi');
	}
}