<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_all_tahap1 extends CI_Migration {

	public function up(){
		$filename = 'extras/db_update/006.sql'; 
		$file_restore = $this->load->file($filename, true);
		$file_array = explode(';', $file_restore);
		$this->db->query("SET FOREIGN_KEY_CHECKS = 0");
		foreach ($file_array as $query){
			if(!empty($query)){
				 $this->db->query($query);			 
			}
		}		
		$this->db->query("SET FOREIGN_KEY_CHECKS = 1");
	}

	public function down()
	{
		
	}
}