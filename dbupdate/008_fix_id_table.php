<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_fix_id_table extends CI_Migration {

	public function up(){
		$this->db->query("ALTER TABLE  `m_dokumen_kurikulum` CHANGE  `iddokumen_kurikulum`  `id_dokumen_kurikulum` BIGINT( 20 ) NOT NULL AUTO_INCREMENT ;");
		$this->db->query("ALTER TABLE  `m_sarana_prasarana` CHANGE  `id`  `id_sarana_prasarana` BIGINT( 20 ) NOT NULL AUTO_INCREMENT ;");
		$this->db->query("ALTER TABLE  `m_sarana_prasarana_detail` CHANGE  `id`  `id_sarana_prasarana_detail` BIGINT( 20 ) NOT NULL AUTO_INCREMENT ;");
		$this->db->query("ALTER TABLE  `m_sekolah_misi` CHANGE  `id`  `id_sekolah_misi` BIGINT( 20 ) NOT NULL AUTO_INCREMENT ;");
		$this->db->query("ALTER TABLE  `m_sekolah_visi` CHANGE  `id`  `id_sekolah_visi` BIGINT( 20 ) NOT NULL AUTO_INCREMENT ;");
		$this->db->query("ALTER TABLE  `m_tahun_ajaran` CHANGE  `id`  `id_tahun_ajaran` BIGINT( 20 ) NOT NULL AUTO_INCREMENT ;");
		$this->db->query("ALTER TABLE  `r_jenis_sarana_prasarana` CHANGE  `id`  `id_jenis_sarana_prasarana` BIGINT( 20 ) NOT NULL AUTO_INCREMENT ;");
		$this->db->query("ALTER TABLE  `r_kondisi` CHANGE  `id`  `id_kondisi` BIGINT( 20 ) NOT NULL AUTO_INCREMENT ;");
	}

	public function down()
	{
		
	}
}