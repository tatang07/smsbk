<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_all_sekolah extends CI_Migration {

	public function up(){
		$filename = 'extras/db_update/005.sql'; 
		$file_restore = $this->load->file($filename, true);
		$file_array = explode(';', $file_restore);
		$this->db->query("SET FOREIGN_KEY_CHECKS = 0");
		$this->db->query("INSERT INTO `r_jenis_akreditasi` (`jenis_akreditasi`) VALUES
			('A'),
			('B');
			");
		$this->db->query("INSERT INTO `m_sekolah` (`nss`, `nps`, `nama_strip`, `sekolah`, `alamat`, `kode_pos`, `telepon`, `fax`, `sk_pendirian`, `tgl_sk_pendirian`, `no_sk_akreditasi`, `tgl_akreditasi`, `email`, `website`, `luas_halaman`, `luas_tanah`, `luas_bangunan`, `luas_olahraga`, `logo`, `foto`, `id_jenis_akreditasi`, `id_kecamatan`, `id_status_sekolah`, `status_aktif`) VALUES
			('0011', '0011', NULL, 'SMAN 1 Nusantara', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1);
			");
		$this->db->query("INSERT INTO `r_kabupaten_kota` (`kabupaten_kota`, `id_provinsi`) VALUES
			('Kota Bandung', 1);
			");
		$this->db->query("INSERT INTO `r_kecamatan` (`kecamatan`, `id_kabupaten_kota`) VALUES
			('Isola', 1);
			");
		$this->db->query("INSERT INTO `r_provinsi` (`provinsi`) VALUES
			('Jawa Barat');
			");
		$this->db->query("INSERT INTO `r_status_sekolah` (`status_sekolah`) VALUES
			('Negeri'),
			('Swasta');
			");
		$this->db->query("SET FOREIGN_KEY_CHECKS = 1");
	}

	public function down()
	{
		
	}
}