<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_table_sarana_prasarana extends CI_Migration {

	public function up()
	{
		$this->db->query("CREATE TABLE IF NOT EXISTS `r_jenis_sarana_prasarana` (
			`id` bigint(20) NOT NULL AUTO_INCREMENT,
			`jenis_sarana_prasarana` varchar(128) DEFAULT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");

		$this->db->query("CREATE TABLE IF NOT EXISTS `m_sarana_prasarana` (
			`id` bigint(20) NOT NULL AUTO_INCREMENT,
			`kode_sarana_prasarana` varchar(30) DEFAULT NULL,
			`nama_sarana_prasarana` varchar(50) DEFAULT NULL,
			`penanggung_jawab` varchar(50) DEFAULT NULL,
			`jenis_sarana_prasarana_id` bigint(20) DEFAULT NULL,
			`kondisi_id` bigint(20) DEFAULT NULL,
			`keterangan` varchar(255) DEFAULT NULL,
			`daya_tampung` double DEFAULT NULL,
			`sekolah_id` bigint(20) DEFAULT NULL,
			PRIMARY KEY (`id`),
			KEY `jenis_sarana_prasarana_id_idx` (`jenis_sarana_prasarana_id`),
			KEY `kondisi_id_idx` (`kondisi_id`),
			KEY `sekolah_id_idx` (`sekolah_id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");

		$this->db->query("CREATE TABLE IF NOT EXISTS `m_sarana_prasarana_detail` (
			`id` bigint(20) NOT NULL AUTO_INCREMENT,
			`kode_barang` varchar(30) DEFAULT NULL,
			`nama_barang` varchar(50) DEFAULT NULL,
			`sarana_prasarana_id` bigint(20) DEFAULT NULL,
			`no_tanggal_sertifikat` varchar(32) DEFAULT NULL,
			`cc` varchar(64) DEFAULT NULL,
			`no_rangka` varchar(64) DEFAULT NULL,
			`no_mesin` varchar(64) DEFAULT NULL,
			`merek` varchar(64) DEFAULT NULL,
			`type` varchar(64) DEFAULT NULL,
			`warna` varchar(64) DEFAULT NULL,
			`no_tanggal_bpkb` varchar(32) DEFAULT NULL,
			`no_polisi` varchar(32) DEFAULT NULL,
			`tanggal_peroleh` date DEFAULT NULL,
			`asal_peroleh` varchar(32) DEFAULT NULL,
			`jumlah` double DEFAULT NULL,
			`satuan` varchar(32) DEFAULT NULL,
			`kondisi_id` bigint(20) DEFAULT NULL,
			`harga_total` double DEFAULT NULL,
			`keterangan` varchar(255) DEFAULT NULL,
			PRIMARY KEY (`id`),
			KEY `sarana_prasarana_id_idx` (`sarana_prasarana_id`),
			KEY `kondisi_id_idx` (`kondisi_id`)
			) ENGINE=MyISAM  DEFAULT CHARSET=latin1;");
	}

	public function down()
	{
		$this->dbforge->drop_table('r_jenis_sarana_prasarana');
		$this->dbforge->drop_table('m_sarana_prasarana');
		$this->dbforge->drop_table('m_sarana_prasarana_detail');
	}
}