<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_alter_field_sapras extends CI_Migration {

	public function up(){
		$this->db->query("ALTER TABLE  `m_sarana_prasarana` CHANGE  `jenis_sarana_prasarana_id`  `id_jenis_sarana_prasarana` BIGINT( 20 ) NULL DEFAULT NULL ,
			CHANGE  `kondisi_id`  `id_kondisi` BIGINT( 20 ) NULL DEFAULT NULL ,
			CHANGE  `sekolah_id`  `id_sekolah` BIGINT( 20 ) NULL DEFAULT NULL ;
			");
	}

	public function down()
	{
		
	}
}