<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_all_komunikasi extends CI_Migration {

	public function up(){
		$filename = 'extras/db_update/004.sql'; 
		$file_restore = $this->load->file($filename, true);
		$file_array = explode(';', $file_restore);
		$this->db->query("SET FOREIGN_KEY_CHECKS = 0");

		$this->db->query("CREATE TABLE IF NOT EXISTS r_pihak_komunikasi (
			id_pihak_komunikasi BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
			pihak_komunikasi VARCHAR(200)
			);");
		$this->db->query("INSERT INTO r_pihak_komunikasi(pihak_komunikasi)
			VALUES('Komunikasi dengan Komite Sekolah'),
			('Komunikasi dengan Orang Tua Siswa'),
			('Komunikasi dengan Tokoh Masyarakat'),
			('Komunikasi dengan Kepolisian / Narkoba'),
			('Komunikasi dengan Berbagai Media Cetak dan Elektronika'),
			('Kerjasama dengan Bisnis dan Industri'),
			('Kerjasama dengan Berbagai Instansi Pemerintah'),
			('Kerjasama dengan Berbagai Instansi Instansi Swasta'),
			('Kerjasama Internasional'),
			('Kerjasama antar Sekolah');");
		
		$this->db->query("CREATE TABLE IF NOT EXISTS t_komunikasi (
			id_komunikasi BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
			kegiatan VARCHAR(255),
			tanggal DATE,
			hasil TEXT,
			id_sekolah BIGINT NOT NULL,
			id_pihak_komunikasi BIGINT NOT NULL
		);
		");
		$this->db->query("ALTER TABLE t_komunikasi ADD CONSTRAINT FK_t_komunikasi_0 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);");
		$this->db->query("ALTER TABLE t_komunikasi ADD CONSTRAINT FK_t_komunikasi_1 FOREIGN KEY (id_pihak_komunikasi) REFERENCES r_pihak_komunikasi (id_pihak_komunikasi);");
		$this->db->query("INSERT INTO sys_information(information_name,information_data) VALUES ('nama_sistem','SMSBK');");

		$this->db->query("SET FOREIGN_KEY_CHECKS = 1");
	}

	public function down()
	{
		
	}
}