-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 06, 2015 at 12:21 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smsbkv23`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `version` int(3) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(9),
(9);

-- --------------------------------------------------------

--
-- Table structure for table `m_beban_belajar`
--

CREATE TABLE IF NOT EXISTS `m_beban_belajar` (
  `id_beban_belajar` bigint(20) NOT NULL AUTO_INCREMENT,
  `jam_per_minggu` int(11) DEFAULT NULL,
  `min_minggu_per_semester` int(11) DEFAULT NULL,
  `max_minggu_per_semester` int(11) DEFAULT NULL,
  `min_minggu_per_tahun` int(11) DEFAULT NULL,
  `max_minggu_per_tahun` int(11) DEFAULT NULL,
  `jam_pilihan_lintas_kelompok` int(11) DEFAULT NULL,
  `jam_peminatan_akademik` int(11) DEFAULT NULL,
  `jam_wajib` int(11) DEFAULT NULL,
  `id_kurikulum` bigint(20) DEFAULT NULL,
  `id_tingkat_kelas` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_beban_belajar`),
  KEY `fk_m_beban_belajar_0` (`id_kurikulum`),
  KEY `fk_m_beban_belajar_1` (`id_tingkat_kelas`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_beban_belajar`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_dokumen_kurikulum`
--

CREATE TABLE IF NOT EXISTS `m_dokumen_kurikulum` (
  `id_dokumen_kurikulum` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_dokumen` varchar(255) DEFAULT NULL,
  `lokasi_file` varchar(255) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_dokumen_kurikulum`),
  KEY `fk_m_dokumen_kurikulum_0` (`id_sekolah`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_dokumen_kurikulum`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_ekstra_kurikuler`
--

CREATE TABLE IF NOT EXISTS `m_ekstra_kurikuler` (
  `id_ekstra_kurikuler` bigint(20) NOT NULL AUTO_INCREMENT,
  `jenis` varchar(17) DEFAULT NULL,
  `nama_ekstra_kurikuler` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `pelatih` varchar(100) DEFAULT NULL,
  `proker` text,
  `lokasi_file_lambang` text,
  `id_sekolah` bigint(20) DEFAULT NULL,
  `id_guru` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_ekstra_kurikuler`),
  KEY `fk_m_ekstra_kurikuler_0` (`id_sekolah`),
  KEY `fk_m_ekstra_kurikuler_1` (`id_guru`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_ekstra_kurikuler`
--

INSERT INTO `m_ekstra_kurikuler` (`id_ekstra_kurikuler`, `jenis`, `nama_ekstra_kurikuler`, `tujuan`, `pelatih`, `proker`, `lokasi_file_lambang`, `id_sekolah`, `id_guru`) VALUES
(1, '1', 'Pramuka', '-', 'Imam Yustian', '-', NULL, 1, NULL),
(2, '1', 'Paskibraka', NULL, NULL, NULL, NULL, 2, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_ekstra_kurikuler_jadwal`
--

CREATE TABLE IF NOT EXISTS `m_ekstra_kurikuler_jadwal` (
  `id_ekstra_kurikuler_jadwal` bigint(20) NOT NULL AUTO_INCREMENT,
  `hari` varchar(30) DEFAULT NULL,
  `jam_mulai` time DEFAULT NULL,
  `jam_selesai` time DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `id_ekstra_kurikuler` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_ekstra_kurikuler_jadwal`),
  KEY `fk_m_ekstra_kurikuler_jadwal_0` (`id_ekstra_kurikuler`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `m_ekstra_kurikuler_jadwal`
--

INSERT INTO `m_ekstra_kurikuler_jadwal` (`id_ekstra_kurikuler_jadwal`, `hari`, `jam_mulai`, `jam_selesai`, `tempat`, `id_ekstra_kurikuler`) VALUES
(1, 'Senin', '08:00:00', '09:00:00', 'Lapang Basket', 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_guru`
--

CREATE TABLE IF NOT EXISTS `m_guru` (
  `id_guru` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_sertifikasi` varchar(30) DEFAULT NULL,
  `nuptk` varchar(20) DEFAULT NULL,
  `kode_guru` varchar(20) DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `hp` varchar(15) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `tgl_diangkat` date DEFAULT NULL,
  `gaji` double DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `status_aktif` tinyint(4) DEFAULT '1',
  `id_golongan_darah` bigint(20) DEFAULT NULL,
  `id_status_pernikahan` bigint(20) DEFAULT NULL,
  `id_agama` bigint(20) DEFAULT NULL,
  `id_status_pegawai` bigint(20) DEFAULT NULL,
  `id_kecamatan` bigint(20) DEFAULT NULL,
  `id_jenjang_pendidikan` bigint(20) DEFAULT NULL,
  `id_pangkat_golongan` bigint(20) DEFAULT NULL,
  `id_kualifikasi_guru` bigint(20) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_guru`),
  KEY `fk_m_guru_0` (`id_golongan_darah`),
  KEY `fk_m_guru_1` (`id_status_pernikahan`),
  KEY `fk_m_guru_2` (`id_agama`),
  KEY `fk_m_guru_3` (`id_status_pegawai`),
  KEY `fk_m_guru_4` (`id_kecamatan`),
  KEY `fk_m_guru_5` (`id_jenjang_pendidikan`),
  KEY `fk_m_guru_6` (`id_pangkat_golongan`),
  KEY `fk_m_guru_7` (`id_kualifikasi_guru`),
  KEY `fk_m_guru_8` (`id_sekolah`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_guru`
--

INSERT INTO `m_guru` (`id_guru`, `status_sertifikasi`, `nuptk`, `kode_guru`, `nip`, `nama`, `jenis_kelamin`, `tempat_lahir`, `tgl_lahir`, `alamat`, `hp`, `telepon`, `tgl_diangkat`, `gaji`, `foto`, `status_aktif`, `id_golongan_darah`, `id_status_pernikahan`, `id_agama`, `id_status_pegawai`, `id_kecamatan`, `id_jenjang_pendidikan`, `id_pangkat_golongan`, `id_kualifikasi_guru`, `id_sekolah`) VALUES
(1, NULL, NULL, 'G001', NULL, 'Ahmad Yasin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Tersertifikasi', '11020231945', 'G001', '1989021022014', 'Ahmad Yasin', 'l', 'Bandung', '1968-01-09', 'Jl. Geger Kalong No. 340', '08111118999', '022898970', '2014-01-02', 1970000, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Table structure for table `m_jadwal_pelajaran`
--

CREATE TABLE IF NOT EXISTS `m_jadwal_pelajaran` (
  `id_jadwal_pelajaran` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama` varchar(100) DEFAULT NULL,
  `status_aktif` tinyint(4) DEFAULT NULL,
  `tanggal_dibuat` date DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_jadwal_pelajaran`),
  KEY `fk_m_jadwal_pelajaran_0` (`id_sekolah`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_jadwal_pelajaran`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_jadwal_pelajaran_detail`
--

CREATE TABLE IF NOT EXISTS `m_jadwal_pelajaran_detail` (
  `id_jadwal_pelajaran_detail` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_ruang_belajar` bigint(20) DEFAULT NULL,
  `id_jam_pelajaran` bigint(20) DEFAULT NULL,
  `id_guru_matpel_rombel` bigint(20) DEFAULT NULL,
  `id_jadwal_pelajaran` bigint(20) DEFAULT NULL,
  `hari` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_jadwal_pelajaran_detail`),
  KEY `fk_m_jadwal_pelajaran_detail_0` (`id_ruang_belajar`),
  KEY `fk_m_jadwal_pelajaran_detail_1` (`id_jam_pelajaran`),
  KEY `fk_m_jadwal_pelajaran_detail_2` (`id_guru_matpel_rombel`),
  KEY `fk_m_jadwal_pelajaran_detail_3` (`id_jadwal_pelajaran`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_jadwal_pelajaran_detail`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_jam_pelajaran`
--

CREATE TABLE IF NOT EXISTS `m_jam_pelajaran` (
  `id_jam_pelajaran` bigint(20) NOT NULL AUTO_INCREMENT,
  `mulai` time DEFAULT NULL,
  `selesai` time DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  `nama_jam` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id_jam_pelajaran`),
  KEY `fk_m_jam_pelajaran_0` (`id_sekolah`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_jam_pelajaran`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_kelompok_pelajaran`
--

CREATE TABLE IF NOT EXISTS `m_kelompok_pelajaran` (
  `id_kelompok_pelajaran` bigint(20) NOT NULL AUTO_INCREMENT,
  `kelompok_pelajaran` varchar(100) DEFAULT NULL,
  `status_pilihan` tinyint(4) DEFAULT NULL,
  `id_kurikulum` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_kelompok_pelajaran`),
  KEY `fk_m_kelompok_pelajaran_0` (`id_kurikulum`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `m_kelompok_pelajaran`
--

INSERT INTO `m_kelompok_pelajaran` (`id_kelompok_pelajaran`, `kelompok_pelajaran`, `status_pilihan`, `id_kurikulum`) VALUES
(1, 'Kelompok A (Wajib)', 0, 2),
(2, 'Kelompok B (Wajib)', 0, 2),
(3, 'Kelompok C (Peminatan)', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `m_kompetensi_inti`
--

CREATE TABLE IF NOT EXISTS `m_kompetensi_inti` (
  `id_kompetensi_inti` bigint(20) NOT NULL AUTO_INCREMENT,
  `kompetensi_inti` text,
  `id_kurikulum` bigint(20) DEFAULT NULL,
  `id_tingkat_kelas` bigint(20) DEFAULT NULL,
  `kode_kompetensi_inti` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_kompetensi_inti`),
  KEY `fk_m_kompetensi_inti_0` (`id_kurikulum`),
  KEY `fk_m_kompetensi_inti_1` (`id_tingkat_kelas`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `m_kompetensi_inti`
--

INSERT INTO `m_kompetensi_inti` (`id_kompetensi_inti`, `kompetensi_inti`, `id_kurikulum`, `id_tingkat_kelas`, `kode_kompetensi_inti`) VALUES
(1, 'Menghayati dan mengamalkan ajaran agama yang dianutnya', 2, 10, 'KI-1'),
(2, 'Menghayati dan mengamalkan ajaran agama yang dianutnya', 2, 11, 'KI-1'),
(3, 'Menghayati dan mengamalkan ajaran agama yang dianutnya', 2, 12, 'KI-1'),
(4, 'Menghayati dan mengamalkan perilaku jujur, disiplin, tanggungjawab, peduli (gotong royong, kerjasama, toleran, damai), santun, responsif dan pro-aktif dan menunjukkan sikap sebagai bagian dari solusi atas berbagai permasalahan dalam berinteraksi secara efektif dengan lingkungan sosial dan alam serta dalam menempatkan diri sebagai cerminan bangsa dalam pergaulan dunia.', 2, 10, 'KI-2'),
(5, 'Menghayati dan mengamalkan perilaku jujur, disiplin, tanggungjawab, peduli (gotong royong, kerjasama, toleran, damai), santun, responsif dan pro-aktif dan menunjukkan sikap sebagai bagian dari solusi atas berbagai permasalahan dalam berinteraksi secara efektif dengan lingkungan sosial dan alam serta dalam menempatkan diri sebagai cerminan bangsa dalam pergaulan dunia.', 2, 11, 'KI-2'),
(6, 'Menghayati dan mengamalkan perilaku jujur, disiplin, tanggungjawab, peduli (gotong royong, kerjasama, toleran, damai), santun, responsif dan pro-aktif dan menunjukkan sikap sebagai bagian dari solusi atas berbagai permasalahan dalam berinteraksi secara efektif dengan lingkungan sosial dan alam serta dalam menempatkan diri sebagai cerminan bangsa dalam pergaulan dunia.', 2, 12, 'KI-2'),
(9, 'Memahami,menerapkan, menganalisis pengetahuan faktual, konseptual, prosedural berdasarkan rasa ingintahunya tentang ilmu pengetahuan, \r\nteknologi, seni, budaya, dan humaniora dengan wawasan kemanusiaan, kebangsaan, \r\nkenegaraan, dan peradaban terkait \r\npenyebab fenomena dan kejadian, serta \r\nmenerapkan pengetahuan prosedural pada bidang kajian yang spesifik sesuai dengan bakat dan minatnya untuk memecahkan masalah', 2, 10, 'KI-3'),
(10, 'Memahami, menerapkan, dan menganalisis pengetahuan faktual, konseptual, prosedural, dan metakognitif berdasarkan rasa ingin tahunya tentang ilmu pengetahuan, teknologi, seni, budaya, dan humaniora dengan wawasan kemanusiaan, kebangsaan, \r\nkenegaraan, dan peradaban terkait \r\npenyebab fenomena dan kejadian, serta \r\nmenerapkan pengetahuan prosedural pada \r\nbidang kajian yang spesifik sesuai dengan bakat dan minatnya untuk memecahkan masalah', 2, 11, 'KI-3'),
(11, 'Memahami, menerapkan, menganalisis dan mengevaluasi \r\npengetahuan faktual, konseptual, prosedural, dan metakognitif berdasarkan rasa ingin tahunya tentang ilmu pengetahuan, teknologi, seni, budaya, dan humaniora dengan wawasan kemanusiaan,kebangsaan, kenegaraan, dan \r\nperadaban terkait penyebab fenomena \r\ndan kejadian, serta menerapkan \r\npengetahuan prosedural pada \r\nbidang kajian yang spesifik sesuai \r\ndengan bakat dan minatnya untuk \r\nmemecahkan masalah ', 2, 12, 'KI-3'),
(12, 'Mengolah, menalar, dan menyaji dalam ranah konkret dan ranah abstrak terkait dengan pengembangan dari yang dipelajarinya di sekolah secara mandiri, dan mampu menggunakan metoda sesuai kaidah keilmuan', 2, 10, 'KI-4'),
(13, 'Mengolah, menalar, dan menyaji dalam \r\nranah konkret dan ranah abstrak terkait \r\ndengan pengembangan dari yang dipelajarinya di sekolah secara \r\nmandiri, bertindak secara efektif dan \r\nkreatif, serta mampu menggunakan metoda \r\nsesuai kaidah keilmuan', 2, 11, 'KI-4'),
(14, 'Mengolah, menalar, menyaji, dan \r\nmencipta dalam ranah konkret dan \r\nranah abstrak terkait dengan \r\npengembangan dari yang dipelajarinya \r\ndi sekolah secara mandiri serta \r\nbertindak secara efektif dan kreatif, \r\ndan mampu menggunakan metoda sesuai \r\nkaidah keilmuan', 2, 12, 'KI-4');

-- --------------------------------------------------------

--
-- Table structure for table `m_komponen_nilai`
--

CREATE TABLE IF NOT EXISTS `m_komponen_nilai` (
  `id_komponen_nilai` bigint(20) NOT NULL AUTO_INCREMENT,
  `komponen_nilai` varchar(100) DEFAULT NULL,
  `nilai_min` double DEFAULT NULL,
  `nilai_max` double DEFAULT NULL,
  `id_kurikulum` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_komponen_nilai`),
  KEY `fk_m_komponen_nilai_0` (`id_kurikulum`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_komponen_nilai`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_konversi_nilai`
--

CREATE TABLE IF NOT EXISTS `m_konversi_nilai` (
  `id_konversi_nilai` bigint(20) NOT NULL AUTO_INCREMENT,
  `nilai_huruf` varchar(5) DEFAULT NULL,
  `nilai_min` double DEFAULT NULL,
  `nilai_max` double DEFAULT NULL,
  `nilai_angka` double DEFAULT NULL,
  `id_komponen_nilai` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_konversi_nilai`),
  KEY `fk_m_konversi_nilai_0` (`id_komponen_nilai`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_konversi_nilai`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_kurikulum`
--

CREATE TABLE IF NOT EXISTS `m_kurikulum` (
  `id_kurikulum` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_kurikulum` char(150) DEFAULT NULL,
  `keterangan` char(255) DEFAULT NULL,
  PRIMARY KEY (`id_kurikulum`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_kurikulum`
--

INSERT INTO `m_kurikulum` (`id_kurikulum`, `nama_kurikulum`, `keterangan`) VALUES
(1, 'KTSP', 'Kurikulum Tingkat Satuan Pendidikan'),
(2, 'Kurikulum 2013', 'Kurikulum Tahun 2013');

-- --------------------------------------------------------

--
-- Table structure for table `m_ortu`
--

CREATE TABLE IF NOT EXISTS `m_ortu` (
  `id_ortu` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_ayah` varchar(100) DEFAULT NULL,
  `nama_ibu` varchar(10) DEFAULT NULL,
  `nama_wali` varchar(100) DEFAULT NULL,
  `alamat_ayah` varchar(200) DEFAULT NULL,
  `alamat_ibu` varchar(200) DEFAULT NULL,
  `alamat_wali` varchar(200) DEFAULT NULL,
  `kecamatan_id` bigint(20) DEFAULT NULL,
  `id_jenjang_pendidikan_ayah` bigint(20) DEFAULT NULL,
  `id_jenjang_pendidikan_ibu` bigint(20) DEFAULT NULL,
  `id_jenjang_pendidikan_wali` bigint(20) DEFAULT NULL,
  `id_pekerjaan_ayah` bigint(20) DEFAULT NULL,
  `id_pekerjaan_ibu` bigint(20) DEFAULT NULL,
  `id_pekerjaan_wali` bigint(20) DEFAULT NULL,
  `id_range_penghasilan_ayah` bigint(20) DEFAULT NULL,
  `id_range_penghasilan_ibu` bigint(20) DEFAULT NULL,
  `id_range_penghasilan_wali` bigint(20) DEFAULT NULL,
  `id_kecamatan_ayah` bigint(20) DEFAULT NULL,
  `id_kecamatan_ibu` bigint(20) DEFAULT NULL,
  `id_kecamatan_wali` bigint(20) DEFAULT NULL,
  `id_siswa` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_ortu`),
  KEY `fk_m_ortu_0` (`id_jenjang_pendidikan_ayah`),
  KEY `fk_m_ortu_1` (`id_pekerjaan_ayah`),
  KEY `fk_fk_m_ortu_10` (`id_range_penghasilan_wali`),
  KEY `fk_fk_m_ortu_11` (`id_kecamatan_wali`),
  KEY `fk_fk_m_ortu_12` (`id_jenjang_pendidikan_ibu`),
  KEY `fk_m_ortu_2` (`id_range_penghasilan_ayah`),
  KEY `fk_m_ortu_3` (`id_kecamatan_ayah`),
  KEY `fk_m_ortu_4` (`id_siswa`),
  KEY `fk_fk_m_ortu_5` (`id_pekerjaan_ibu`),
  KEY `fk_fk_m_ortu_6` (`id_range_penghasilan_ibu`),
  KEY `fk_fk_m_ortu_7` (`id_kecamatan_ibu`),
  KEY `fk_fk_m_ortu_8` (`id_jenjang_pendidikan_wali`),
  KEY `fk_fk_m_ortu_9` (`id_pekerjaan_wali`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_ortu`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_pelajaran`
--

CREATE TABLE IF NOT EXISTS `m_pelajaran` (
  `id_pelajaran` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_pelajaran` varchar(100) DEFAULT NULL,
  `pelajaran` varchar(100) DEFAULT NULL,
  `id_kelompok_pelajaran` bigint(20) DEFAULT NULL,
  `id_kualifikasi_guru` bigint(20) DEFAULT NULL,
  `id_peminatan` bigint(20) DEFAULT NULL,
  `id_tingkat_kelas` bigint(20) DEFAULT NULL,
  `jumlah_jam` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_pelajaran`),
  KEY `fk_m_pelajaran_0` (`id_kelompok_pelajaran`),
  KEY `fk_m_pelajaran_1` (`id_kualifikasi_guru`),
  KEY `fk_m_pelajaran_2` (`id_peminatan`),
  KEY `fk_fk_m_pelajaran_9` (`id_tingkat_kelas`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `m_pelajaran`
--

INSERT INTO `m_pelajaran` (`id_pelajaran`, `kode_pelajaran`, `pelajaran`, `id_kelompok_pelajaran`, `id_kualifikasi_guru`, `id_peminatan`, `id_tingkat_kelas`, `jumlah_jam`) VALUES
(1, 'A001', 'Pendidikan Agama dan Budi Pekerti', 1, 1, 0, 10, NULL),
(2, 'A002', 'Pendidikan Pancasila dan  Kewarganegaraan', 1, 1, 0, 10, NULL),
(3, 'A003', 'Bahasa Indonesia', 1, 1, 0, 10, NULL),
(4, 'A004', 'Matematika', 1, 1, 0, 10, NULL),
(5, 'A005', 'Sejarah Indonesia ', 1, 1, 0, 10, NULL),
(6, 'A006', 'Bahasa Inggris', 1, 1, 0, 10, NULL),
(7, 'B001', 'Seni Budaya', 2, 1, 0, 10, NULL),
(8, 'B002', 'Pendidikan Jasmani, Olah Raga, dan  Kesehatan', 2, 1, 0, 10, NULL),
(9, 'B003', 'Prakarya dan Kewirausahaan', 2, 1, 0, NULL, NULL),
(10, 'C101', 'Matematika', 3, 1, 1, 10, NULL),
(11, 'C102', 'Biologi', 3, 1, 1, 10, NULL),
(12, 'C103', 'Fisika', 3, 1, 1, 10, NULL),
(13, 'C104', 'Kimia', 3, 1, 1, 10, NULL),
(14, 'C201', 'Geografi', 3, 1, 2, 10, NULL),
(15, 'C202', 'Sejarah', 3, 1, 2, 10, NULL),
(16, 'C203', 'Sosiologi', 3, 1, 2, 10, NULL),
(17, 'C204', 'Ekonomi', 3, 1, 2, 10, NULL),
(18, 'C301', 'Bahasa dan Sastra Indonesia', 3, 1, 3, 10, NULL),
(19, 'C302', 'Bahasa dan Sastra Inggeris', 3, 1, 3, 10, NULL),
(20, 'C303', 'Bahasa dan Sastra Arab', 3, 1, 3, 10, NULL),
(21, 'C304', 'Bahasa dan Sastra Mandarin', 3, 1, 3, 10, NULL),
(22, 'C305', 'Bahasa dan Sastra Jepang', 3, 1, 3, 10, NULL),
(23, 'C306', 'Bahasa dan Sastra Korea', 3, 1, 3, 10, NULL),
(24, 'C307', 'Antropologi', 3, 1, 3, 10, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_peminatan`
--

CREATE TABLE IF NOT EXISTS `m_peminatan` (
  `id_peminatan` bigint(20) NOT NULL AUTO_INCREMENT,
  `peminatan` varchar(100) DEFAULT NULL,
  `peminatan_singkatan` varchar(20) DEFAULT NULL,
  `id_kelompok_pelajaran` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_peminatan`),
  KEY `fk_m_peminatan_0` (`id_kelompok_pelajaran`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `m_peminatan`
--

INSERT INTO `m_peminatan` (`id_peminatan`, `peminatan`, `peminatan_singkatan`, `id_kelompok_pelajaran`) VALUES
(1, 'Matematika dan Ilmu Alam', 'MIA', 3),
(2, 'Ilmu-ilmu Sosial', 'IIS', 3),
(3, 'Ilmu Bahasa dan Budaya', 'IBB', 3);

-- --------------------------------------------------------

--
-- Table structure for table `m_program`
--

CREATE TABLE IF NOT EXISTS `m_program` (
  `id_program` bigint(20) NOT NULL AUTO_INCREMENT,
  `program` varchar(20) DEFAULT NULL,
  `status_aktif` tinyint(4) DEFAULT NULL,
  `id_peminatan` bigint(20) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_program`),
  KEY `fk_m_program_0` (`id_peminatan`),
  KEY `fk_m_program_1` (`id_sekolah`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_program`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_rombel`
--

CREATE TABLE IF NOT EXISTS `m_rombel` (
  `id_rombel` bigint(20) NOT NULL AUTO_INCREMENT,
  `rombel` varchar(20) DEFAULT NULL,
  `id_tingkat_kelas` bigint(20) DEFAULT NULL,
  `id_tahun_ajaran` bigint(20) DEFAULT NULL,
  `id_program` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_rombel`),
  KEY `fk_m_rombel_0` (`id_tingkat_kelas`),
  KEY `fk_m_rombel_1` (`id_tahun_ajaran`),
  KEY `fk_m_rombel_2` (`id_program`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_rombel`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_ruang_belajar`
--

CREATE TABLE IF NOT EXISTS `m_ruang_belajar` (
  `id_ruang_belajar` bigint(20) NOT NULL AUTO_INCREMENT,
  `ruang_belajar` varchar(50) DEFAULT NULL,
  `kode_ruang_belajar` varchar(10) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_ruang_belajar`),
  KEY `fk_m_ruang_belajar_0` (`id_sekolah`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_ruang_belajar`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_sekolah`
--

CREATE TABLE IF NOT EXISTS `m_sekolah` (
  `id_sekolah` bigint(20) NOT NULL AUTO_INCREMENT,
  `nss` varchar(100) DEFAULT NULL,
  `nps` varchar(10) DEFAULT NULL,
  `nama_strip` varchar(32) DEFAULT NULL,
  `sekolah` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kode_pos` varchar(5) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `sk_pendirian` varchar(15) DEFAULT NULL,
  `tgl_sk_pendirian` date DEFAULT NULL,
  `no_sk_akreditasi` varchar(100) DEFAULT NULL,
  `tgl_akreditasi` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `luas_halaman` double DEFAULT NULL,
  `luas_tanah` double DEFAULT NULL,
  `luas_bangunan` double DEFAULT NULL,
  `luas_olahraga` double DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `status_aktif` tinyint(4) DEFAULT '1',
  `id_jenis_akreditasi` bigint(20) DEFAULT NULL,
  `id_kecamatan` bigint(20) DEFAULT NULL,
  `id_status_sekolah` bigint(20) DEFAULT NULL,
  `id_jenjang_sekolah` bigint(20) DEFAULT NULL,
  `id_term_aktif` int(11) NOT NULL DEFAULT '1',
  `id_tahun_ajaran_aktif` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_sekolah`),
  KEY `fk_m_sekolah_0` (`id_jenis_akreditasi`),
  KEY `fk_m_sekolah_1` (`id_kecamatan`),
  KEY `fk_m_sekolah_2` (`id_status_sekolah`),
  KEY `fk_m_sekolah_3` (`id_jenjang_sekolah`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_sekolah`
--

INSERT INTO `m_sekolah` (`id_sekolah`, `nss`, `nps`, `nama_strip`, `sekolah`, `alamat`, `kode_pos`, `telepon`, `fax`, `sk_pendirian`, `tgl_sk_pendirian`, `no_sk_akreditasi`, `tgl_akreditasi`, `email`, `website`, `luas_halaman`, `luas_tanah`, `luas_bangunan`, `luas_olahraga`, `logo`, `foto`, `status_aktif`, `id_jenis_akreditasi`, `id_kecamatan`, `id_status_sekolah`, `id_jenjang_sekolah`, `id_term_aktif`, `id_tahun_ajaran_aktif`) VALUES
(1, NULL, NULL, NULL, 'SMAN 1 Nusantara', 'Jl. Dr. Setiabudhi No. 30', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 4, 1, 1),
(2, NULL, NULL, NULL, 'SMP N 1 Bandung', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, 3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_sekolah_misi`
--

CREATE TABLE IF NOT EXISTS `m_sekolah_misi` (
  `id_sekolah_misi` bigint(20) NOT NULL AUTO_INCREMENT,
  `misi` varchar(255) DEFAULT NULL,
  `urutan` tinyint(4) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_sekolah_misi`),
  KEY `fk_m_sekolah_misi_0` (`id_sekolah`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_sekolah_misi`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_sekolah_visi`
--

CREATE TABLE IF NOT EXISTS `m_sekolah_visi` (
  `id_sekolah_visi` bigint(20) NOT NULL AUTO_INCREMENT,
  `visi` varchar(255) DEFAULT NULL,
  `urutan` tinyint(4) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_sekolah_visi`),
  KEY `fk_m_sekolah_visi_0` (`id_sekolah`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_sekolah_visi`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_silabus`
--

CREATE TABLE IF NOT EXISTS `m_silabus` (
  `id_silabus` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_silabus` varchar(150) DEFAULT NULL,
  `lokasi_file` varchar(255) DEFAULT NULL,
  `id_pelajaran` bigint(20) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_silabus`),
  KEY `fk_m_silabus_0` (`id_pelajaran`),
  KEY `fk_m_silabus_1` (`id_sekolah`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `m_silabus`
--

INSERT INTO `m_silabus` (`id_silabus`, `nama_silabus`, `lokasi_file`, `id_pelajaran`, `id_sekolah`) VALUES
(1, 'Silabus Tahun 2014 V1', NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_siswa`
--

CREATE TABLE IF NOT EXISTS `m_siswa` (
  `id_siswa` bigint(20) NOT NULL AUTO_INCREMENT,
  `telepon` char(20) DEFAULT NULL,
  `nis` varchar(20) DEFAULT NULL,
  `nisn` varchar(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `anak_ke` tinyint(4) DEFAULT NULL,
  `jumlah_sdr` tinyint(4) DEFAULT NULL,
  `alamat` varchar(150) DEFAULT NULL,
  `kode_pos` varchar(5) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `status_aktif` tinyint(4) DEFAULT '1',
  `id_agama` bigint(20) DEFAULT NULL,
  `id_golongan_darah` bigint(20) DEFAULT NULL,
  `id_status_anak` bigint(20) DEFAULT NULL,
  `id_kecamatan` bigint(20) DEFAULT NULL,
  `id_asal_sekolah` bigint(20) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_siswa`),
  KEY `fk_m_siswa_0` (`id_agama`),
  KEY `fk_m_siswa_1` (`id_golongan_darah`),
  KEY `fk_m_siswa_2` (`id_status_anak`),
  KEY `fk_m_siswa_3` (`id_kecamatan`),
  KEY `fk_m_siswa_4` (`id_asal_sekolah`),
  KEY `fk_m_siswa_5` (`id_sekolah`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `m_siswa`
--

INSERT INTO `m_siswa` (`id_siswa`, `telepon`, `nis`, `nisn`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `anak_ke`, `jumlah_sdr`, `alamat`, `kode_pos`, `foto`, `status_aktif`, `id_agama`, `id_golongan_darah`, `id_status_anak`, `id_kecamatan`, `id_asal_sekolah`, `id_sekolah`) VALUES
(1, '08999909', '1112001', '11120001', 'Antoni Fajar', 'Bandung', '2004-01-07', 'l', 1, 3, 'Jl. Dr. Setiabudhi No. 36', '45414', NULL, 1, NULL, NULL, NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_tahun_ajaran`
--

CREATE TABLE IF NOT EXISTS `m_tahun_ajaran` (
  `id_tahun_ajaran` bigint(20) NOT NULL AUTO_INCREMENT,
  `tahun_awal` int(11) NOT NULL,
  `tahun_akhir` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tahun_ajaran`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_tahun_ajaran`
--

INSERT INTO `m_tahun_ajaran` (`id_tahun_ajaran`, `tahun_awal`, `tahun_akhir`, `keterangan`) VALUES
(1, 2014, 2025, NULL),
(2, 2015, 2016, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_tenaga_kependidikan`
--

CREATE TABLE IF NOT EXISTS `m_tenaga_kependidikan` (
  `id_tenaga_kependidikan` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_golongan_darah` bigint(20) DEFAULT NULL,
  `id_status_pernikahan` bigint(20) DEFAULT NULL,
  `id_agama` bigint(20) DEFAULT NULL,
  `id_kecamatan` bigint(20) DEFAULT NULL,
  `id_status_pegawai` bigint(20) DEFAULT NULL,
  `id_jenis_kepegawaian` bigint(20) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  `id_pangkat_golongan` bigint(20) DEFAULT NULL,
  `id_jenjang_pendidikan` bigint(20) DEFAULT NULL,
  `nuptk` varchar(20) DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `hp` varchar(15) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `tgl_diangkat` date DEFAULT NULL,
  `gaji` double DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_tenaga_kependidikan`),
  KEY `fk_fk_m_tenaga_kependidikan_0` (`id_golongan_darah`),
  KEY `fk_fk_m_tenaga_kependidikan_1` (`id_status_pernikahan`),
  KEY `fk_fk_m_tenaga_kependidikan_2` (`id_agama`),
  KEY `fk_fk_m_tenaga_kependidikan_3` (`id_kecamatan`),
  KEY `fk_fk_m_tenaga_kependidikan_4` (`id_status_pegawai`),
  KEY `fk_fk_m_tenaga_kependidikan_5` (`id_jenis_kepegawaian`),
  KEY `fk_fk_m_tenaga_kependidikan_6` (`id_sekolah`),
  KEY `fk_fk_m_tenaga_kependidikan_7` (`id_jenjang_pendidikan`),
  KEY `fk_fk_m_tenaga_kependidikan_9` (`id_pangkat_golongan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `m_tenaga_kependidikan`
--


-- --------------------------------------------------------

--
-- Table structure for table `m_term`
--

CREATE TABLE IF NOT EXISTS `m_term` (
  `id_term` bigint(20) NOT NULL AUTO_INCREMENT,
  `term` varchar(30) DEFAULT NULL,
  `id_tahun_ajaran` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_term`),
  KEY `fk_m_term_0` (`id_tahun_ajaran`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_term`
--

INSERT INTO `m_term` (`id_term`, `term`, `id_tahun_ajaran`) VALUES
(1, 'Semester-1', 1),
(2, 'Semester-2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_tingkat_kelas`
--

CREATE TABLE IF NOT EXISTS `m_tingkat_kelas` (
  `id_tingkat_kelas` bigint(20) NOT NULL AUTO_INCREMENT,
  `tingkat_kelas` varchar(20) DEFAULT NULL,
  `id_jenjang_sekolah` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_tingkat_kelas`),
  KEY `fk_m_tingkat_kelas_0` (`id_jenjang_sekolah`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `m_tingkat_kelas`
--

INSERT INTO `m_tingkat_kelas` (`id_tingkat_kelas`, `tingkat_kelas`, `id_jenjang_sekolah`) VALUES
(1, 'I', 2),
(2, 'II', 2),
(3, 'III', 2),
(4, 'IV', 2),
(5, 'V', 2),
(6, 'VI', 2),
(7, 'VII', 3),
(8, 'VIII', 3),
(9, 'IX', 3),
(10, 'X', 4),
(11, 'XI', 4),
(12, 'XII', 4);

-- --------------------------------------------------------

--
-- Table structure for table `r_agama`
--

CREATE TABLE IF NOT EXISTS `r_agama` (
  `id_agama` bigint(20) NOT NULL AUTO_INCREMENT,
  `agama` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id_agama`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `r_agama`
--

INSERT INTO `r_agama` (`id_agama`, `agama`) VALUES
(1, 'Islam'),
(2, 'Katholik'),
(3, 'Protestan'),
(4, 'Hindu'),
(5, 'Budha'),
(6, 'Konghucu');

-- --------------------------------------------------------

--
-- Table structure for table `r_asal_sekolah`
--

CREATE TABLE IF NOT EXISTS `r_asal_sekolah` (
  `id_asal_sekolah` bigint(20) NOT NULL AUTO_INCREMENT,
  `nps` varchar(10) NOT NULL,
  `nama_sekolah` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `id_jenjang_sekolah` bigint(20) DEFAULT NULL,
  `id_kecamatan` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_asal_sekolah`),
  KEY `fk_r_asal_sekolah_0` (`id_jenjang_sekolah`),
  KEY `fk_r_asal_sekolah_1` (`id_kecamatan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_asal_sekolah`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_golongan_darah`
--

CREATE TABLE IF NOT EXISTS `r_golongan_darah` (
  `id_golongan_darah` bigint(20) NOT NULL AUTO_INCREMENT,
  `golongan_darah` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id_golongan_darah`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `r_golongan_darah`
--

INSERT INTO `r_golongan_darah` (`id_golongan_darah`, `golongan_darah`) VALUES
(1, 'A'),
(2, 'B'),
(3, 'AB'),
(4, 'O');

-- --------------------------------------------------------

--
-- Table structure for table `r_hobi`
--

CREATE TABLE IF NOT EXISTS `r_hobi` (
  `id_hobi` bigint(20) NOT NULL AUTO_INCREMENT,
  `hobi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_hobi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_hobi`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_jalur_masuk_pt`
--

CREATE TABLE IF NOT EXISTS `r_jalur_masuk_pt` (
  `id_jalur_masuk_pt` bigint(20) NOT NULL AUTO_INCREMENT,
  `jalur_masuk_pt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_jalur_masuk_pt`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_jalur_masuk_pt`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_jenis_absen`
--

CREATE TABLE IF NOT EXISTS `r_jenis_absen` (
  `id_jenis_absen` bigint(20) NOT NULL AUTO_INCREMENT,
  `jenis_absen` varchar(64) DEFAULT NULL,
  `status_hadir` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_absen`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_jenis_absen`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_jenis_akreditasi`
--

CREATE TABLE IF NOT EXISTS `r_jenis_akreditasi` (
  `id_jenis_akreditasi` bigint(20) NOT NULL AUTO_INCREMENT,
  `jenis_akreditasi` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_akreditasi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_jenis_akreditasi`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_jenis_jabatan`
--

CREATE TABLE IF NOT EXISTS `r_jenis_jabatan` (
  `id_jenis_jabatan` bigint(20) NOT NULL AUTO_INCREMENT,
  `jenis_jabatan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_jabatan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_jenis_jabatan`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_jenis_kepegawaian`
--

CREATE TABLE IF NOT EXISTS `r_jenis_kepegawaian` (
  `id_jenis_kepegawaian` bigint(20) NOT NULL AUTO_INCREMENT,
  `jenis_kepegawaian` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_kepegawaian`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_jenis_kepegawaian`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_jenis_mutasi_siswa`
--

CREATE TABLE IF NOT EXISTS `r_jenis_mutasi_siswa` (
  `id_jenis_mutasi_siswa` bigint(20) NOT NULL AUTO_INCREMENT,
  `jenis_mutasi` varchar(40) DEFAULT NULL,
  PRIMARY KEY (`id_jenis_mutasi_siswa`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_jenis_mutasi_siswa`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_jenjang_pendidikan`
--

CREATE TABLE IF NOT EXISTS `r_jenjang_pendidikan` (
  `id_jenjang_pendidikan` bigint(20) NOT NULL AUTO_INCREMENT,
  `jenjang_pendidikan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_jenjang_pendidikan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_jenjang_pendidikan`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_jenjang_sekolah`
--

CREATE TABLE IF NOT EXISTS `r_jenjang_sekolah` (
  `id_jenjang_sekolah` bigint(20) NOT NULL AUTO_INCREMENT,
  `jenjang_sekolah` varchar(10) NOT NULL,
  PRIMARY KEY (`id_jenjang_sekolah`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `r_jenjang_sekolah`
--

INSERT INTO `r_jenjang_sekolah` (`id_jenjang_sekolah`, `jenjang_sekolah`) VALUES
(1, 'TK'),
(2, 'SD'),
(3, 'SMP'),
(4, 'SMA');

-- --------------------------------------------------------

--
-- Table structure for table `r_kabupaten_kota`
--

CREATE TABLE IF NOT EXISTS `r_kabupaten_kota` (
  `id_kabupaten_kota` bigint(20) NOT NULL AUTO_INCREMENT,
  `kabupaten_kota` varchar(255) DEFAULT NULL,
  `id_provinsi` bigint(20) NOT NULL,
  PRIMARY KEY (`id_kabupaten_kota`),
  KEY `fk_r_kabupaten_kota_0` (`id_provinsi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_kabupaten_kota`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_kecamatan`
--

CREATE TABLE IF NOT EXISTS `r_kecamatan` (
  `id_kecamatan` bigint(20) NOT NULL AUTO_INCREMENT,
  `kecamatan` varchar(255) DEFAULT NULL,
  `id_kabupaten_kota` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_kecamatan`),
  KEY `fk_r_kecamatan_0` (`id_kabupaten_kota`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_kecamatan`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_kualifikasi_guru`
--

CREATE TABLE IF NOT EXISTS `r_kualifikasi_guru` (
  `id_kualifikasi_guru` bigint(20) NOT NULL AUTO_INCREMENT,
  `kualifikasi_guru` varchar(150) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_kualifikasi_guru`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `r_kualifikasi_guru`
--

INSERT INTO `r_kualifikasi_guru` (`id_kualifikasi_guru`, `kualifikasi_guru`, `deskripsi`) VALUES
(1, 'Matematika', NULL),
(2, 'Fisika', NULL),
(3, 'Bahasa Indonesia', NULL),
(4, 'Bahasa Inggris', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `r_pangkat_golongan`
--

CREATE TABLE IF NOT EXISTS `r_pangkat_golongan` (
  `id_pangkat_golongan` bigint(20) NOT NULL AUTO_INCREMENT,
  `pangkat` varchar(50) DEFAULT NULL,
  `golongan` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`id_pangkat_golongan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_pangkat_golongan`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_pekerjaan`
--

CREATE TABLE IF NOT EXISTS `r_pekerjaan` (
  `id_pekerjaan` bigint(20) NOT NULL AUTO_INCREMENT,
  `pekerjaan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_pekerjaan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_pekerjaan`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_perguruan_tinggi`
--

CREATE TABLE IF NOT EXISTS `r_perguruan_tinggi` (
  `id_perguruan_tinggi` bigint(20) NOT NULL AUTO_INCREMENT,
  `perguruan_tinggi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_perguruan_tinggi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_perguruan_tinggi`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_pihak_komunikasi`
--

CREATE TABLE IF NOT EXISTS `r_pihak_komunikasi` (
  `id_pihak_komunikasi` bigint(20) NOT NULL AUTO_INCREMENT,
  `pihak_komunikasi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_pihak_komunikasi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_pihak_komunikasi`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_provinsi`
--

CREATE TABLE IF NOT EXISTS `r_provinsi` (
  `id_provinsi` bigint(20) NOT NULL AUTO_INCREMENT,
  `provinsi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_provinsi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_provinsi`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_range_penghasilan`
--

CREATE TABLE IF NOT EXISTS `r_range_penghasilan` (
  `id_range_penghasilan` bigint(20) NOT NULL AUTO_INCREMENT,
  `range_penghasilan` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_range_penghasilan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_range_penghasilan`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_status_anak`
--

CREATE TABLE IF NOT EXISTS `r_status_anak` (
  `id_status_anak` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_anak` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_status_anak`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_status_anak`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_status_pegawai`
--

CREATE TABLE IF NOT EXISTS `r_status_pegawai` (
  `id_status_pegawai` bigint(20) NOT NULL AUTO_INCREMENT,
  `status` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id_status_pegawai`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_status_pegawai`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_status_pernikahan`
--

CREATE TABLE IF NOT EXISTS `r_status_pernikahan` (
  `id_status_pernikahan` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_pernikahan` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id_status_pernikahan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_status_pernikahan`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_status_sekolah`
--

CREATE TABLE IF NOT EXISTS `r_status_sekolah` (
  `id_status_sekolah` bigint(20) NOT NULL AUTO_INCREMENT,
  `status_sekolah` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_status_sekolah`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_status_sekolah`
--


-- --------------------------------------------------------

--
-- Table structure for table `r_tingkat_wilayah`
--

CREATE TABLE IF NOT EXISTS `r_tingkat_wilayah` (
  `id_tingkat_wilayah` bigint(20) NOT NULL AUTO_INCREMENT,
  `tingkat_wilayah` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_tingkat_wilayah`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `r_tingkat_wilayah`
--


-- --------------------------------------------------------

--
-- Table structure for table `sys_code_numbering`
--

CREATE TABLE IF NOT EXISTS `sys_code_numbering` (
  `id_code_numbering` bigint(20) NOT NULL AUTO_INCREMENT,
  `table_name` varchar(100) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `current_state` varchar(100) DEFAULT NULL,
  `code_numbering` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_code_numbering`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `sys_code_numbering`
--


-- --------------------------------------------------------

--
-- Table structure for table `sys_group`
--

CREATE TABLE IF NOT EXISTS `sys_group` (
  `id_group` int(11) NOT NULL AUTO_INCREMENT,
  `groupname` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_group`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sys_group`
--

INSERT INTO `sys_group` (`id_group`, `groupname`) VALUES
(1, 'Super Admin'),
(2, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `sys_information`
--

CREATE TABLE IF NOT EXISTS `sys_information` (
  `id_information` int(11) NOT NULL AUTO_INCREMENT,
  `information_name` varchar(100) NOT NULL,
  `information_label` varchar(100) NOT NULL,
  `information_type` varchar(100) NOT NULL,
  `information_data` varchar(255) NOT NULL,
  PRIMARY KEY (`id_information`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sys_information`
--

INSERT INTO `sys_information` (`id_information`, `information_name`, `information_label`, `information_type`, `information_data`) VALUES
(1, 'nama_sistem', '', '', 'SMSBK');

-- --------------------------------------------------------

--
-- Table structure for table `sys_user`
--

CREATE TABLE IF NOT EXISTS `sys_user` (
  `id_user` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `default_password` varchar(100) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `id_personal` bigint(20) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `id_group` int(11) DEFAULT NULL,
  `id_sekolah` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_user`),
  KEY `fk_sys_user_0` (`id_group`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sys_user`
--

INSERT INTO `sys_user` (`id_user`, `username`, `password`, `default_password`, `email`, `date_created`, `id_personal`, `is_active`, `id_group`, `id_sekolah`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, NULL, NULL, b'1', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_agenda_kelas`
--

CREATE TABLE IF NOT EXISTS `t_agenda_kelas` (
  `id_agenda_kelas` bigint(20) NOT NULL AUTO_INCREMENT,
  `tanggal` date DEFAULT NULL,
  `materi` varchar(255) DEFAULT NULL,
  `proses_belajar` varchar(255) DEFAULT NULL,
  `id_guru_matpel_rombel` bigint(20) DEFAULT NULL,
  `id_term` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_agenda_kelas`),
  KEY `fk_t_agenda_kelas_0` (`id_guru_matpel_rombel`),
  KEY `fk_fk_t_agenda_kelas_1` (`id_term`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_agenda_kelas`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_ekstra_kurikuler_fasilitas`
--

CREATE TABLE IF NOT EXISTS `t_ekstra_kurikuler_fasilitas` (
  `id_ekstra_kurikuler_fasilitas` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_ekstra_kurikuler` bigint(20) DEFAULT NULL,
  `ekstra_kurikuler_fasilitas` varchar(255) DEFAULT NULL,
  `jumlah_baik` int(11) DEFAULT NULL,
  `jumlah_rusak_ringan` int(11) DEFAULT NULL,
  `jumlah_rusak_berat` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_ekstra_kurikuler_fasilitas`),
  KEY `fk_fk_t_ekskul_jadwal_0` (`id_ekstra_kurikuler`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_ekstra_kurikuler_fasilitas`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_ekstra_kurikuler_peserta`
--

CREATE TABLE IF NOT EXISTS `t_ekstra_kurikuler_peserta` (
  `id_ekstra_kurikuler_peserta` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_ekstra_kurikuler` bigint(20) DEFAULT NULL,
  `id_siswa` bigint(20) DEFAULT NULL,
  `status_aktif` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_ekstra_kurikuler_peserta`),
  KEY `fk_fk_t_ekskul_peserta_0` (`id_ekstra_kurikuler`),
  KEY `fk_fk_t_ekskul_peserta_1` (`id_siswa`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_ekstra_kurikuler_peserta`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_guru_jabatan`
--

CREATE TABLE IF NOT EXISTS `t_guru_jabatan` (
  `id_guru_jabatan` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_jenis_jabatan` bigint(20) DEFAULT NULL,
  `id_guru` bigint(20) DEFAULT NULL,
  `status` varchar(1) DEFAULT NULL,
  `no_sk` varchar(30) DEFAULT NULL,
  `tgl_sk` date DEFAULT NULL,
  `tmt_sk` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_guru_jabatan`),
  KEY `fk_fk_t_guru_jabatan_0` (`id_guru`),
  KEY `fk_fk_t_guru_jabatan_1` (`id_jenis_jabatan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_guru_jabatan`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_guru_matpel_rombel`
--

CREATE TABLE IF NOT EXISTS `t_guru_matpel_rombel` (
  `id_guru_matpel_rombel` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_rombel` bigint(20) DEFAULT NULL,
  `id_pelajaran` bigint(20) DEFAULT NULL,
  `id_guru` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_guru_matpel_rombel`),
  KEY `fk_fk_t_guru_matpel_rombel_0` (`id_rombel`),
  KEY `fk_fk_t_guru_matpel_rombel_1` (`id_pelajaran`),
  KEY `fk_fk_t_guru_matpel_rombel_2` (`id_guru`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_guru_matpel_rombel`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_guru_penataran`
--

CREATE TABLE IF NOT EXISTS `t_guru_penataran` (
  `id_guru_penataran` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_guru` bigint(20) DEFAULT NULL,
  `tahun` bigint(20) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `penataran` varchar(100) DEFAULT NULL,
  `deskripsi` text,
  PRIMARY KEY (`id_guru_penataran`),
  KEY `fk_fk_t_guru_penataran_0` (`id_guru`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_guru_penataran`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_guru_pendidikan`
--

CREATE TABLE IF NOT EXISTS `t_guru_pendidikan` (
  `id_guru_pendidikan` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_guru` bigint(20) DEFAULT NULL,
  `id_jenjang_pendidikan` bigint(20) DEFAULT NULL,
  `program_studi` varchar(255) DEFAULT NULL,
  `nama_instansi` varchar(100) DEFAULT NULL,
  `tahun_mulai` bigint(20) DEFAULT NULL,
  `tahun_selesai` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_guru_pendidikan`),
  KEY `fk_fk_t_guru_pendidikan_0` (`id_guru`),
  KEY `fk_fk_t_guru_pendidikan_1` (`id_jenjang_pendidikan`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_guru_pendidikan`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_guru_pengalaman`
--

CREATE TABLE IF NOT EXISTS `t_guru_pengalaman` (
  `id_guru_pengalaman` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_guru` bigint(20) DEFAULT NULL,
  `pengalaman` varchar(255) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  `tgl_mulai` date DEFAULT NULL,
  `tgl_akhir` date DEFAULT NULL,
  PRIMARY KEY (`id_guru_pengalaman`),
  KEY `fk_fk_t_guru_pengalaman_0` (`id_guru`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_guru_pengalaman`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_guru_prestasi`
--

CREATE TABLE IF NOT EXISTS `t_guru_prestasi` (
  `id_guru_prestasi` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_guru` bigint(20) DEFAULT NULL,
  `id_tingkat_wilayah` bigint(20) DEFAULT NULL,
  `tahun` bigint(20) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `lokasi` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_guru_prestasi`),
  KEY `fk_fk_t_guru_prestasi_0` (`id_guru`),
  KEY `fk_fk_t_guru_prestasi_1` (`id_tingkat_wilayah`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_guru_prestasi`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_komunikasi`
--

CREATE TABLE IF NOT EXISTS `t_komunikasi` (
  `id_komunikasi` bigint(20) NOT NULL AUTO_INCREMENT,
  `kegiatan` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `hasil` text,
  `id_pihak_komunikasi` bigint(20) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_komunikasi`),
  KEY `fk_t_komunikasi_0` (`id_pihak_komunikasi`),
  KEY `fk_fk_t_komunikasi_1` (`id_sekolah`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_komunikasi`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_prestasi_siswa`
--

CREATE TABLE IF NOT EXISTS `t_prestasi_siswa` (
  `id_prestasi_siswa` bigint(20) NOT NULL AUTO_INCREMENT,
  `prestasi` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `perlombaan` varchar(255) DEFAULT NULL,
  `id_tingkat_wilayah` bigint(20) DEFAULT NULL,
  `id_ekstra_kurikuler` bigint(20) DEFAULT NULL,
  `id_siswa` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_prestasi_siswa`),
  KEY `fk_t_prestasi_siswa_0` (`id_tingkat_wilayah`),
  KEY `fk_fk_t_prestasi_siswa_1` (`id_siswa`),
  KEY `fk_fk_t_prestasi_siswa_11` (`id_ekstra_kurikuler`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_prestasi_siswa`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_rombel_detail`
--

CREATE TABLE IF NOT EXISTS `t_rombel_detail` (
  `id_rombel_detail` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_rombel` bigint(20) DEFAULT NULL,
  `id_siswa` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_rombel_detail`),
  KEY `fk_fk_t_rombel_detail_0` (`id_rombel`),
  KEY `fk_fk_t_rombel_detail_1` (`id_siswa`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_rombel_detail`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_rpp`
--

CREATE TABLE IF NOT EXISTS `t_rpp` (
  `id_rpp` bigint(20) NOT NULL AUTO_INCREMENT,
  `nama_rpp` varchar(255) DEFAULT NULL,
  `pertemuan_ke` varchar(100) DEFAULT NULL,
  `lokasi_file` varchar(255) DEFAULT NULL,
  `id_guru_matpel_rombel` bigint(20) DEFAULT NULL,
  `id_term` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_rpp`),
  KEY `fk_t_rpp_0` (`id_guru_matpel_rombel`),
  KEY `fk_fk_t_rpp_1` (`id_term`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_rpp`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_siswa_absen`
--

CREATE TABLE IF NOT EXISTS `t_siswa_absen` (
  `id_siswa_absen` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_jenis_absen` bigint(20) DEFAULT NULL,
  `id_agenda_kelas` bigint(20) DEFAULT NULL,
  `id_siswa` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_siswa_absen`),
  KEY `fk_t_siswa_absen_1` (`id_jenis_absen`),
  KEY `fk_t_siswa_absen_2` (`id_agenda_kelas`),
  KEY `fk_fk_t_siswa_absen_3` (`id_siswa`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_siswa_absen`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_siswa_catatan_bk`
--

CREATE TABLE IF NOT EXISTS `t_siswa_catatan_bk` (
  `id_siswa_catatan_bk` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_siswa` bigint(20) DEFAULT NULL,
  `tanggal_catatan` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_siswa_catatan_bk`),
  KEY `fk_fk_t_siswa_catatan_bk_0` (`id_siswa`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_siswa_catatan_bk`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_siswa_hobi`
--

CREATE TABLE IF NOT EXISTS `t_siswa_hobi` (
  `id_siswa_hobi` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_siswa` bigint(20) DEFAULT NULL,
  `id_hobi` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_siswa_hobi`),
  KEY `fk_fk_t_siswa_hobi_0` (`id_siswa`),
  KEY `fk_fk_t_siswa_hobi_1` (`id_hobi`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_siswa_hobi`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_siswa_mutasi`
--

CREATE TABLE IF NOT EXISTS `t_siswa_mutasi` (
  `id_siswa_mutasi` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_sekolah` bigint(20) DEFAULT NULL,
  `id_jenis_mutasi_siswa` bigint(20) DEFAULT NULL,
  `id_siswa` bigint(20) DEFAULT NULL,
  `tanggal_mutasi` date DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id_siswa_mutasi`),
  KEY `fk_fk_t_siswa_mutasi_0` (`id_sekolah`),
  KEY `fk_fk_t_siswa_mutasi_1` (`id_jenis_mutasi_siswa`),
  KEY `fk_fk_t_siswa_mutasi_2` (`id_siswa`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_siswa_mutasi`
--


-- --------------------------------------------------------

--
-- Table structure for table `t_siswa_perguruan_tinggi`
--

CREATE TABLE IF NOT EXISTS `t_siswa_perguruan_tinggi` (
  `id_siswa_perguruan_tinggi` bigint(20) NOT NULL AUTO_INCREMENT,
  `id_jalur_masuk_pt` bigint(20) DEFAULT NULL,
  `id_siswa` bigint(20) DEFAULT NULL,
  `id_perguruan_tinggi` bigint(20) DEFAULT NULL,
  `id_tahun_ajaran` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id_siswa_perguruan_tinggi`),
  KEY `fk_fk_t_siswa_perguruan_tinggi_0` (`id_siswa`),
  KEY `fk_fk_t_siswa_perguruan_tinggi_1` (`id_perguruan_tinggi`),
  KEY `fk_fk_t_siswa_perguruan_tinggi_2` (`id_jalur_masuk_pt`),
  KEY `fk_fk_t_siswa_perguruan_tinggi_5` (`id_tahun_ajaran`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `t_siswa_perguruan_tinggi`
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
