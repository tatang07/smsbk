-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2015 at 04:15 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smsbkv21`
--

-- --------------------------------------------------------

--
-- Structure for view `v_siswa_absen_rekap`
--

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_siswa_absen_rekap` AS select `gm`.`id_guru_matpel_rombel` AS `id_guru_matpel_rombel`,`sa`.`id_jenis_absen` AS `id_jenis_absen`,`s`.`nis` AS `nis`,`s`.`nama` AS `nama`,count(`sa`.`id_siswa_absen`) AS `jumlah` from (((`t_siswa_absen` `sa` left join `m_siswa` `s` on((`s`.`id_siswa` = `sa`.`id_siswa`))) left join `t_agenda_kelas` `ak` on((`ak`.`id_agenda_kelas` = `sa`.`id_agenda_kelas`))) left join `t_guru_matpel_rombel` `gm` on((`gm`.`id_guru_matpel_rombel` = `ak`.`id_guru_matpel_rombel`))) group by `gm`.`id_guru_matpel_rombel`,`s`.`nis`,`s`.`nama`,`sa`.`id_jenis_absen`;

--
-- VIEW  `v_siswa_absen_rekap`
-- Data: None
--


/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
