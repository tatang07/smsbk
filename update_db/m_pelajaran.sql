-- phpMyAdmin SQL Dump
-- version 3.2.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 06, 2015 at 08:21 PM
-- Server version: 5.1.41
-- PHP Version: 5.3.1

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smsbkv23`
--

-- --------------------------------------------------------

--
-- Table structure for table `m_pelajaran`
--

CREATE TABLE IF NOT EXISTS `m_pelajaran` (
  `id_pelajaran` bigint(20) NOT NULL AUTO_INCREMENT,
  `kode_pelajaran` varchar(100) DEFAULT NULL,
  `pelajaran` varchar(100) DEFAULT NULL,
  `id_kelompok_pelajaran` bigint(20) DEFAULT NULL,
  `id_kualifikasi_guru` bigint(20) DEFAULT NULL,
  `id_peminatan` bigint(20) DEFAULT NULL,
  `id_tingkat_kelas` bigint(20) DEFAULT NULL,
  `jumlah_jam` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`id_pelajaran`),
  KEY `fk_m_pelajaran_0` (`id_kelompok_pelajaran`),
  KEY `fk_m_pelajaran_1` (`id_kualifikasi_guru`),
  KEY `fk_m_pelajaran_2` (`id_peminatan`),
  KEY `fk_fk_m_pelajaran_9` (`id_tingkat_kelas`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=79 ;

--
-- Dumping data for table `m_pelajaran`
--

INSERT INTO `m_pelajaran` (`id_pelajaran`, `kode_pelajaran`, `pelajaran`, `id_kelompok_pelajaran`, `id_kualifikasi_guru`, `id_peminatan`, `id_tingkat_kelas`, `jumlah_jam`) VALUES
(1, 'A001', 'Pendidikan Agama dan Budi Pekerti', 1, 1, 0, 10, 3),
(2, 'A002', 'Pendidikan Pancasila dan  Kewarganegaraan', 1, 1, 0, 10, 2),
(3, 'A003', 'Bahasa Indonesia', 1, 1, 0, 10, 4),
(4, 'A004', 'Matematika', 1, 1, 0, 10, 4),
(5, 'A005', 'Sejarah Indonesia ', 1, 1, 0, 10, 2),
(6, 'A006', 'Bahasa Inggris', 1, 1, 0, 10, 2),
(7, 'B001', 'Seni Budaya', 2, 1, 0, 10, 2),
(8, 'B002', 'Pendidikan Jasmani, Olah Raga, dan  Kesehatan', 2, 1, 0, 10, 3),
(9, 'B003', 'Prakarya dan Kewirausahaan', 2, 1, 0, 10, 2),
(10, 'C101', 'Matematika', 3, 1, 1, 10, 3),
(11, 'C102', 'Biologi', 3, 1, 1, 10, 3),
(12, 'C103', 'Fisika', 3, 1, 1, 10, 3),
(13, 'C104', 'Kimia', 3, 1, 1, 10, 3),
(14, 'C201', 'Geografi', 3, 1, 2, 10, 3),
(15, 'C202', 'Sejarah', 3, 1, 2, 10, 3),
(16, 'C203', 'Sosiologi', 3, 1, 2, 10, 3),
(17, 'C204', 'Ekonomi', 3, 1, 2, 10, 3),
(18, 'C301', 'Bahasa dan Sastra Indonesia', 3, 1, 3, 10, 3),
(19, 'C302', 'Bahasa dan Sastra Inggeris', 3, 1, 3, 10, 3),
(20, 'C303', 'Bahasa dan Sastra Arab', 3, 1, 3, 10, 3),
(21, 'C304', 'Bahasa dan Sastra Mandarin', 3, 1, 3, 10, 3),
(22, 'C305', 'Bahasa dan Sastra Jepang', 3, 1, 3, 10, 3),
(23, 'C306', 'Bahasa dan Sastra Korea', 3, 1, 3, 10, 3),
(24, 'C307', 'Antropologi', 3, 1, 3, 10, 3),
(32, 'A2002', 'Pendidikan Pancasila dan  Kewarganegaraan', 1, 1, 0, 11, 2),
(31, 'A2001', 'Pendidikan Agama dan Budi Pekerti', 1, 1, 0, 11, 3),
(33, 'A2003', 'Bahasa Indonesia', 1, 1, 0, 11, 4),
(34, 'A2004', 'Matematika', 1, 1, 0, 11, 4),
(35, 'A2005', 'Sejarah Indonesia ', 1, 1, 0, 11, 2),
(36, 'A2006', 'Bahasa Inggris', 1, 1, 0, 11, 2),
(37, 'B2001', 'Seni Budaya', 2, 1, 0, 11, 2),
(38, 'B2002', 'Pendidikan Jasmani, Olah Raga, dan  Kesehatan', 2, 1, 0, 11, 3),
(39, 'B2003', 'Prakarya dan Kewirausahaan', 2, 1, 0, 11, 2),
(40, 'C2101', 'Matematika', 3, 1, 1, 11, 4),
(41, 'C2102', 'Biologi', 3, 1, 1, 11, 4),
(42, 'C2103', 'Fisika', 3, 1, 1, 11, 4),
(43, 'C2104', 'Kimia', 3, 1, 1, 11, 4),
(44, 'C2201', 'Geografi', 3, 1, 2, 11, 4),
(45, 'C2202', 'Sejarah', 3, 1, 2, 11, 4),
(46, 'C2203', 'Sosiologi', 3, 1, 2, 11, 4),
(47, 'C2204', 'Ekonomi', 3, 1, 2, 11, 4),
(48, 'C2301', 'Bahasa dan Sastra Indonesia', 3, 1, 3, 11, 4),
(49, 'C2302', 'Bahasa dan Sastra Inggeris', 3, 1, 3, 11, 4),
(50, 'C2303', 'Bahasa dan Sastra Arab', 3, 1, 3, 11, 4),
(51, 'C2304', 'Bahasa dan Sastra Mandarin', 3, 1, 3, 11, 4),
(52, 'C2305', 'Bahasa dan Sastra Jepang', 3, 1, 3, 11, 4),
(53, 'C2306', 'Bahasa dan Sastra Korea', 3, 1, 3, 11, 4),
(54, 'C2307', 'Antropologi', 3, 1, 3, 11, 4),
(55, 'A3001', 'Pendidikan Agama dan Budi Pekerti', 1, 1, 0, 12, 3),
(56, 'A3002', 'Pendidikan Pancasila dan  Kewarganegaraan', 1, 1, 0, 12, 2),
(57, 'A3003', 'Bahasa Indonesia', 1, 1, 0, 12, 4),
(58, 'A3004', 'Matematika', 1, 1, 0, 12, 4),
(59, 'A3005', 'Sejarah Indonesia ', 1, 1, 0, 12, 2),
(60, 'A3006', 'Bahasa Inggris', 1, 1, 0, 12, 2),
(61, 'B3001', 'Seni Budaya', 2, 1, 0, 12, 2),
(62, 'B3002', 'Pendidikan Jasmani, Olah Raga, dan  Kesehatan', 2, 1, 0, 12, 3),
(63, 'B3003', 'Prakarya dan Kewirausahaan', 2, 1, 0, 12, 2),
(64, 'C3101', 'Matematika', 3, 1, 1, 12, 4),
(65, 'C3102', 'Biologi', 3, 1, 1, 12, 4),
(66, 'C3103', 'Fisika', 3, 1, 1, 12, 4),
(67, 'C3104', 'Kimia', 3, 1, 1, 12, 4),
(68, 'C3201', 'Geografi', 3, 1, 2, 12, 4),
(69, 'C3202', 'Sejarah', 3, 1, 2, 12, 4),
(70, 'C3203', 'Sosiologi', 3, 1, 2, 12, 4),
(71, 'C3204', 'Ekonomi', 3, 1, 2, 12, 4),
(72, 'C3301', 'Bahasa dan Sastra Indonesia', 3, 1, 3, 12, 4),
(73, 'C3302', 'Bahasa dan Sastra Inggeris', 3, 1, 3, 12, 4),
(74, 'C3303', 'Bahasa dan Sastra Arab', 3, 1, 3, 12, 4),
(75, 'C3304', 'Bahasa dan Sastra Mandarin', 3, 1, 3, 12, 4),
(76, 'C3305', 'Bahasa dan Sastra Jepang', 3, 1, 3, 12, 4),
(77, 'C3306', 'Bahasa dan Sastra Korea', 3, 1, 3, 12, 4),
(78, 'C3307', 'Antropologi', 3, 1, 3, 12, 4);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
