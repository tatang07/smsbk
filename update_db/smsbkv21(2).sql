-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2015 at 04:31 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `smsbkv21`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `version` int(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `m_alokasi_waktu_per_minggu`
--

CREATE TABLE IF NOT EXISTS `m_alokasi_waktu_per_minggu` (
`id_alokasi_waktu_per_minggu` bigint(20) NOT NULL,
  `jumlah_jam` int(11) DEFAULT NULL,
  `id_tingkat_kelas` bigint(20) DEFAULT NULL,
  `id_pelajaran` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `m_alokasi_waktu_per_minggu`
--

INSERT INTO `m_alokasi_waktu_per_minggu` (`id_alokasi_waktu_per_minggu`, `jumlah_jam`, `id_tingkat_kelas`, `id_pelajaran`) VALUES
(1, 4, 10, 1),
(2, 3, 11, 1),
(3, 3, 12, 1),
(4, 2, 10, 2),
(5, 2, 11, 2),
(6, 2, 12, 2);

-- --------------------------------------------------------

--
-- Table structure for table `m_beban_belajar`
--

CREATE TABLE IF NOT EXISTS `m_beban_belajar` (
`id_beban_belajar` bigint(20) NOT NULL,
  `jam_per_minggu` int(11) DEFAULT NULL,
  `min_minggu_per_semester` int(11) DEFAULT NULL,
  `max_minggu_per_semester` int(11) DEFAULT NULL,
  `min_minggu_per_tahun` int(11) DEFAULT NULL,
  `max_minggu_per_tahun` int(11) DEFAULT NULL,
  `jam_pilihan_lintas_kelompok` int(11) DEFAULT NULL,
  `jam_peminatan_akademik` int(11) DEFAULT NULL,
  `jam_wajib` int(11) DEFAULT NULL,
  `id_kurikulum` bigint(20) DEFAULT NULL,
  `id_tingkat_kelas` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_dokumen_kurikulum`
--

CREATE TABLE IF NOT EXISTS `m_dokumen_kurikulum` (
`id_dokumen_kurikulum` bigint(20) NOT NULL,
  `nama_dokumen` varchar(255) DEFAULT NULL,
  `lokasi_file` varchar(255) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  `id_kurikulum` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_ekstra_kurikuler`
--

CREATE TABLE IF NOT EXISTS `m_ekstra_kurikuler` (
`id_ekstra_kurikuler` bigint(20) NOT NULL,
  `jenis` varchar(17) DEFAULT NULL,
  `nama_ekstra_kurikuler` varchar(255) DEFAULT NULL,
  `tujuan` varchar(255) DEFAULT NULL,
  `pelatih` varchar(100) DEFAULT NULL,
  `proker` text,
  `lokasi_file_lambang` text,
  `id_sekolah` bigint(20) DEFAULT NULL,
  `id_guru` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `m_ekstra_kurikuler`
--

INSERT INTO `m_ekstra_kurikuler` (`id_ekstra_kurikuler`, `jenis`, `nama_ekstra_kurikuler`, `tujuan`, `pelatih`, `proker`, `lokasi_file_lambang`, `id_sekolah`, `id_guru`) VALUES
(1, '1', 'Pramuka', '-', 'Imam Yustian', '-', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `m_ekstra_kurikuler_jadwal`
--

CREATE TABLE IF NOT EXISTS `m_ekstra_kurikuler_jadwal` (
`id_ekstra_kurikuler_jadwal` bigint(20) NOT NULL,
  `hari` varchar(30) DEFAULT NULL,
  `jam_mulai` time DEFAULT NULL,
  `jam_selesai` time DEFAULT NULL,
  `tempat` varchar(100) DEFAULT NULL,
  `id_ekstra_kurikuler` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `m_ekstra_kurikuler_jadwal`
--

INSERT INTO `m_ekstra_kurikuler_jadwal` (`id_ekstra_kurikuler_jadwal`, `hari`, `jam_mulai`, `jam_selesai`, `tempat`, `id_ekstra_kurikuler`) VALUES
(1, 'qwe', '00:00:01', '00:00:01', 'erqwe', 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_guru`
--

CREATE TABLE IF NOT EXISTS `m_guru` (
`id_guru` bigint(20) NOT NULL,
  `status_sertifikasi` varchar(30) DEFAULT NULL,
  `nuptk` varchar(20) DEFAULT NULL,
  `kode_guru` varchar(20) DEFAULT NULL,
  `nip` varchar(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tgl_lahir` date DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `hp` varchar(15) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `tgl_diangkat` date DEFAULT NULL,
  `gaji` double DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `status_aktif` tinyint(4) DEFAULT '1',
  `id_golongan_darah` bigint(20) DEFAULT NULL,
  `id_status_pernikahan` bigint(20) DEFAULT NULL,
  `id_agama` bigint(20) DEFAULT NULL,
  `id_status_pegawai` bigint(20) DEFAULT NULL,
  `id_kecamatan` bigint(20) DEFAULT NULL,
  `id_jenjang_pendidikan` bigint(20) DEFAULT NULL,
  `id_pangkat_golongan` bigint(20) DEFAULT NULL,
  `id_kualifikasi_guru` bigint(20) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_guru`
--

INSERT INTO `m_guru` (`id_guru`, `status_sertifikasi`, `nuptk`, `kode_guru`, `nip`, `nama`, `jenis_kelamin`, `tempat_lahir`, `tgl_lahir`, `alamat`, `hp`, `telepon`, `tgl_diangkat`, `gaji`, `foto`, `status_aktif`, `id_golongan_darah`, `id_status_pernikahan`, `id_agama`, `id_status_pegawai`, `id_kecamatan`, `id_jenjang_pendidikan`, `id_pangkat_golongan`, `id_kualifikasi_guru`, `id_sekolah`) VALUES
(1, 'asd', 'asd', 'asd', 'asd', 'dr hasan', 'l', 'asd', '2014-12-02', 'asd', 'asd', 'asd', '2014-12-01', 123123, 'asd', 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_jadwal_pelajaran`
--

CREATE TABLE IF NOT EXISTS `m_jadwal_pelajaran` (
`id_jadwal_pelajaran` bigint(20) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `status_aktif` tinyint(4) DEFAULT NULL,
  `tanggal_dibuat` date DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_jadwal_pelajaran_detail`
--

CREATE TABLE IF NOT EXISTS `m_jadwal_pelajaran_detail` (
`id_jadwal_pelajaran_detail` bigint(20) NOT NULL,
  `id_ruang_belajar` bigint(20) DEFAULT NULL,
  `id_jam_pelajaran` bigint(20) DEFAULT NULL,
  `id_guru_matpel_rombel` bigint(20) DEFAULT NULL,
  `id_jadwal_pelajaran` bigint(20) DEFAULT NULL,
  `hari` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_jam_pelajaran`
--

CREATE TABLE IF NOT EXISTS `m_jam_pelajaran` (
`id_jam_pelajaran` bigint(20) NOT NULL,
  `mulai` time DEFAULT NULL,
  `selesai` time DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  `nama_jam` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_kelompok_pelajaran`
--

CREATE TABLE IF NOT EXISTS `m_kelompok_pelajaran` (
`id_kelompok_pelajaran` bigint(20) NOT NULL,
  `kelompok_pelajaran` varchar(100) DEFAULT NULL,
  `status_pilihan` tinyint(4) DEFAULT NULL,
  `id_kurikulum` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `m_kelompok_pelajaran`
--

INSERT INTO `m_kelompok_pelajaran` (`id_kelompok_pelajaran`, `kelompok_pelajaran`, `status_pilihan`, `id_kurikulum`) VALUES
(1, 'Kelompok A (Wajib)', 0, 2),
(2, 'Kelompok B (Wajib)', 0, 2),
(3, 'Kelompok C (Peminatan)', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `m_kompetensi_inti`
--

CREATE TABLE IF NOT EXISTS `m_kompetensi_inti` (
`id_kompetensi_inti` bigint(20) NOT NULL,
  `kode_kompetensi_inti` varchar(50) NOT NULL,
  `kompetensi_inti` text,
  `id_kurikulum` bigint(20) DEFAULT NULL,
  `id_tingkat_kelas` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `m_kompetensi_inti`
--

INSERT INTO `m_kompetensi_inti` (`id_kompetensi_inti`, `kode_kompetensi_inti`, `kompetensi_inti`, `id_kurikulum`, `id_tingkat_kelas`) VALUES
(11, 'asd', 'asd', 1, 11);

-- --------------------------------------------------------

--
-- Table structure for table `m_komponen_nilai`
--

CREATE TABLE IF NOT EXISTS `m_komponen_nilai` (
`id_komponen_nilai` bigint(20) NOT NULL,
  `komponen_nilai` varchar(100) DEFAULT NULL,
  `nilai_min` double DEFAULT NULL,
  `nilai_max` double DEFAULT NULL,
  `id_kurikulum` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_konversi_nilai`
--

CREATE TABLE IF NOT EXISTS `m_konversi_nilai` (
`id_konversi_nilai` bigint(20) NOT NULL,
  `nilai_huruf` varchar(5) DEFAULT NULL,
  `nilai_min` double DEFAULT NULL,
  `nilai_max` double DEFAULT NULL,
  `nilai_angka` double DEFAULT NULL,
  `id_komponen_nilai` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_kurikulum`
--

CREATE TABLE IF NOT EXISTS `m_kurikulum` (
`id_kurikulum` bigint(20) NOT NULL,
  `nama_kurikulum` char(150) DEFAULT NULL,
  `keterangan` char(255) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_kurikulum`
--

INSERT INTO `m_kurikulum` (`id_kurikulum`, `nama_kurikulum`, `keterangan`) VALUES
(1, 'KTSP', 'Kurikulum Tingkat Satuan Pendidikan'),
(2, 'Kurikulum 2013', 'Kurikulum Tahun 2013');

-- --------------------------------------------------------

--
-- Table structure for table `m_ortu`
--

CREATE TABLE IF NOT EXISTS `m_ortu` (
`id_ortu` bigint(20) NOT NULL,
  `nama_ayah` varchar(100) DEFAULT NULL,
  `nama_ibu` varchar(10) DEFAULT NULL,
  `nama_wali` varchar(100) DEFAULT NULL,
  `alamat_ayah` varchar(200) DEFAULT NULL,
  `alamat_ibu` varchar(200) DEFAULT NULL,
  `alamat_wali` varchar(200) DEFAULT NULL,
  `kecamatan_id` bigint(20) DEFAULT NULL,
  `id_jenjang_pendidikan_ayah` bigint(20) DEFAULT NULL,
  `id_jenjang_pendidikan_ibu` bigint(20) DEFAULT NULL,
  `id_jenjang_pendidikan_wali` bigint(20) DEFAULT NULL,
  `id_pekerjaan_ayah` bigint(20) DEFAULT NULL,
  `id_pekerjaan_ibu` bigint(20) DEFAULT NULL,
  `id_pekerjaan_wali` bigint(20) DEFAULT NULL,
  `id_range_penghasilan_ayah` bigint(20) DEFAULT NULL,
  `id_range_penghasilan_ibu` bigint(20) DEFAULT NULL,
  `id_range_penghasilan_wali` bigint(20) DEFAULT NULL,
  `id_kecamatan_ayah` bigint(20) DEFAULT NULL,
  `id_kecamatan_ibu` bigint(20) DEFAULT NULL,
  `id_kecamatan_wali` bigint(20) DEFAULT NULL,
  `id_siswa` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_pelajaran`
--

CREATE TABLE IF NOT EXISTS `m_pelajaran` (
`id_pelajaran` bigint(20) NOT NULL,
  `kode_pelajaran` varchar(100) DEFAULT NULL,
  `pelajaran` varchar(100) DEFAULT NULL,
  `id_kelompok_pelajaran` bigint(20) DEFAULT NULL,
  `id_kualifikasi_guru` bigint(20) DEFAULT NULL,
  `id_peminatan` bigint(20) DEFAULT NULL,
  `id_tingkat_kelas` bigint(20) DEFAULT NULL,
  `jumlah_jam` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=79 ;

--
-- Dumping data for table `m_pelajaran`
--

INSERT INTO `m_pelajaran` (`id_pelajaran`, `kode_pelajaran`, `pelajaran`, `id_kelompok_pelajaran`, `id_kualifikasi_guru`, `id_peminatan`, `id_tingkat_kelas`, `jumlah_jam`) VALUES
(1, 'A001', 'Pendidikan Agama dan Budi Pekerti', 1, 1, 0, 10, 3),
(2, 'A002', 'Pendidikan Pancasila dan  Kewarganegaraan', 1, 1, 0, 10, 2),
(3, 'A003', 'Bahasa Indonesia', 1, 1, 0, 10, 4),
(4, 'A004', 'Matematika', 1, 1, 0, 10, 4),
(5, 'A005', 'Sejarah Indonesia ', 1, 1, 0, 10, 2),
(6, 'A006', 'Bahasa Inggris', 1, 1, 0, 10, 2),
(7, 'B001', 'Seni Budaya', 2, 1, 0, 10, 2),
(8, 'B002', 'Pendidikan Jasmani, Olah Raga, dan  Kesehatan', 2, 1, 0, 10, 3),
(9, 'B003', 'Prakarya dan Kewirausahaan', 2, 1, 0, 10, 2),
(10, 'C101', 'Matematika', 3, 1, 1, 10, 3),
(11, 'C102', 'Biologi', 3, 1, 1, 10, 2),
(12, 'C103', 'Fisika', 3, 1, 1, 10, 3),
(13, 'C104', 'Kimia', 3, 1, 1, 10, 3),
(14, 'C201', 'Geografi', 3, 1, 2, 10, 3),
(15, 'C202', 'Sejarah', 3, 1, 2, 10, 3),
(16, 'C203', 'Sosiologi', 3, 1, 2, 10, 3),
(17, 'C204', 'Ekonomi', 3, 1, 2, 10, 3),
(18, 'C301', 'Bahasa dan Sastra Indonesia', 3, 1, 3, 10, 3),
(19, 'C302', 'Bahasa dan Sastra Inggeris', 3, 1, 3, 10, 3),
(20, 'C303', 'Bahasa dan Sastra Arab', 3, 1, 3, 10, 3),
(21, 'C304', 'Bahasa dan Sastra Mandarin', 3, 1, 3, 10, 3),
(22, 'C305', 'Bahasa dan Sastra Jepang', 3, 1, 3, 10, 3),
(23, 'C306', 'Bahasa dan Sastra Korea', 3, 1, 3, 10, 3),
(24, 'C307', 'Antropologi', 3, 1, 3, 10, 3),
(32, 'A2002', 'Pendidikan Pancasila dan  Kewarganegaraan', 1, 1, 0, 11, 2),
(31, 'A2001', 'Pendidikan Agama dan Budi Pekerti', 1, 1, 0, 11, 3),
(33, 'A2003', 'Bahasa Indonesia', 1, 1, 0, 11, 4),
(34, 'A2004', 'Matematika', 1, 1, 0, 11, 4),
(35, 'A2005', 'Sejarah Indonesia ', 1, 1, 0, 11, 2),
(36, 'A2006', 'Bahasa Inggris', 1, 1, 0, 11, 2),
(37, 'B2001', 'Seni Budaya', 2, 1, 0, 11, 2),
(38, 'B2002', 'Pendidikan Jasmani, Olah Raga, dan  Kesehatan', 2, 1, 0, 11, 3),
(39, 'B2003', 'Prakarya dan Kewirausahaan', 2, 1, 0, 11, 2),
(40, 'C2101', 'Matematika', 3, 1, 1, 11, 4),
(41, 'C2102', 'Biologi', 3, 1, 1, 11, 4),
(42, 'C2103', 'Fisika', 3, 1, 1, 11, 4),
(43, 'C2104', 'Kimia', 3, 1, 1, 11, 4),
(44, 'C2201', 'Geografi', 3, 1, 2, 11, 4),
(45, 'C2202', 'Sejarah', 3, 1, 2, 11, 4),
(46, 'C2203', 'Sosiologi', 3, 1, 2, 11, 4),
(47, 'C2204', 'Ekonomi', 3, 1, 2, 11, 4),
(48, 'C2301', 'Bahasa dan Sastra Indonesia', 3, 1, 3, 11, 4),
(49, 'C2302', 'Bahasa dan Sastra Inggeris', 3, 1, 3, 11, 4),
(50, 'C2303', 'Bahasa dan Sastra Arab', 3, 1, 3, 11, 4),
(51, 'C2304', 'Bahasa dan Sastra Mandarin', 3, 1, 3, 11, 4),
(52, 'C2305', 'Bahasa dan Sastra Jepang', 3, 1, 3, 11, 4),
(53, 'C2306', 'Bahasa dan Sastra Korea', 3, 1, 3, 11, 4),
(54, 'C2307', 'Antropologi', 3, 1, 3, 11, 4),
(55, 'A3001', 'Pendidikan Agama dan Budi Pekerti', 1, 1, 0, 12, 3),
(56, 'A3002', 'Pendidikan Pancasila dan  Kewarganegaraan', 1, 1, 0, 12, 2),
(57, 'A3003', 'Bahasa Indonesia', 1, 1, 0, 12, 4),
(58, 'A3004', 'Matematika', 1, 1, 0, 12, 4),
(59, 'A3005', 'Sejarah Indonesia ', 1, 1, 0, 12, 2),
(60, 'A3006', 'Bahasa Inggris', 1, 1, 0, 12, 2),
(61, 'B3001', 'Seni Budaya', 2, 1, 0, 12, 2),
(62, 'B3002', 'Pendidikan Jasmani, Olah Raga, dan  Kesehatan', 2, 1, 0, 12, 3),
(63, 'B3003', 'Prakarya dan Kewirausahaan', 2, 1, 0, 12, 2),
(64, 'C3101', 'Matematika', 3, 1, 1, 12, 4),
(65, 'C3102', 'Biologi', 3, 1, 1, 12, 4),
(66, 'C3103', 'Fisika', 3, 1, 1, 12, 4),
(67, 'C3104', 'Kimia', 3, 1, 1, 12, 4),
(68, 'C3201', 'Geografi', 3, 1, 2, 12, 4),
(69, 'C3202', 'Sejarah', 3, 1, 2, 12, 4),
(70, 'C3203', 'Sosiologi', 3, 1, 2, 12, 4),
(71, 'C3204', 'Ekonomi', 3, 1, 2, 12, 4),
(72, 'C3301', 'Bahasa dan Sastra Indonesia', 3, 1, 3, 12, 4),
(73, 'C3302', 'Bahasa dan Sastra Inggeris', 3, 1, 3, 12, 4),
(74, 'C3303', 'Bahasa dan Sastra Arab', 3, 1, 3, 12, 4),
(75, 'C3304', 'Bahasa dan Sastra Mandarin', 3, 1, 3, 12, 4),
(76, 'C3305', 'Bahasa dan Sastra Jepang', 3, 1, 3, 12, 4),
(77, 'C3306', 'Bahasa dan Sastra Korea', 3, 1, 3, 12, 4),
(78, 'C3307', 'Antropologi', 3, 1, 3, 12, 4);

-- --------------------------------------------------------

--
-- Table structure for table `m_peminatan`
--

CREATE TABLE IF NOT EXISTS `m_peminatan` (
`id_peminatan` bigint(20) NOT NULL,
  `peminatan` varchar(100) DEFAULT NULL,
  `peminatan_singkatan` varchar(20) DEFAULT NULL,
  `id_kelompok_pelajaran` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `m_peminatan`
--

INSERT INTO `m_peminatan` (`id_peminatan`, `peminatan`, `peminatan_singkatan`, `id_kelompok_pelajaran`) VALUES
(1, 'Matematika dan Ilmu Alam', 'MIA', 3),
(2, 'Ilmu-ilmu Sosial', 'IIS', 3),
(3, 'Ilmu Bahasa dan Budaya', 'IBB', 3);

-- --------------------------------------------------------

--
-- Table structure for table `m_program`
--

CREATE TABLE IF NOT EXISTS `m_program` (
`id_program` bigint(20) NOT NULL,
  `program` varchar(20) DEFAULT NULL,
  `status_aktif` tinyint(4) DEFAULT NULL,
  `id_peminatan` bigint(20) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `m_program`
--

INSERT INTO `m_program` (`id_program`, `program`, `status_aktif`, `id_peminatan`, `id_sekolah`) VALUES
(1, 'IPA', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_rombel`
--

CREATE TABLE IF NOT EXISTS `m_rombel` (
`id_rombel` bigint(20) NOT NULL,
  `rombel` varchar(20) DEFAULT NULL,
  `id_tingkat_kelas` bigint(20) DEFAULT NULL,
  `id_tahun_ajaran` bigint(20) DEFAULT NULL,
  `id_program` bigint(20) DEFAULT NULL,
  `id_sekolah` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_rombel`
--

INSERT INTO `m_rombel` (`id_rombel`, `rombel`, `id_tingkat_kelas`, `id_tahun_ajaran`, `id_program`, `id_sekolah`) VALUES
(1, 'X 1', 1, 1, 1, 0),
(2, 'XI IPA 1', 2, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `m_rpp`
--

CREATE TABLE IF NOT EXISTS `m_rpp` (
`id_rpp` int(11) NOT NULL,
  `nama_rpp` varchar(255) NOT NULL,
  `lokasi_file` text NOT NULL,
  `id_pelajaran` int(11) NOT NULL,
  `id_sekolah` int(11) NOT NULL,
  `id_kurikulum` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_ruang_belajar`
--

CREATE TABLE IF NOT EXISTS `m_ruang_belajar` (
`id_ruang_belajar` bigint(20) NOT NULL,
  `ruang_belajar` varchar(50) DEFAULT NULL,
  `kode_ruang_belajar` varchar(10) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_sekolah`
--

CREATE TABLE IF NOT EXISTS `m_sekolah` (
`id_sekolah` bigint(20) NOT NULL,
  `nss` varchar(100) DEFAULT NULL,
  `nps` varchar(10) DEFAULT NULL,
  `nama_strip` varchar(32) DEFAULT NULL,
  `sekolah` varchar(100) DEFAULT NULL,
  `alamat` varchar(255) DEFAULT NULL,
  `kode_pos` varchar(5) DEFAULT NULL,
  `telepon` varchar(15) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL,
  `sk_pendirian` varchar(15) DEFAULT NULL,
  `tgl_sk_pendirian` date DEFAULT NULL,
  `no_sk_akreditasi` varchar(100) DEFAULT NULL,
  `tgl_akreditasi` date DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `luas_halaman` double DEFAULT NULL,
  `luas_tanah` double DEFAULT NULL,
  `luas_bangunan` double DEFAULT NULL,
  `luas_olahraga` double DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `status_aktif` tinyint(4) DEFAULT '1',
  `id_jenis_akreditasi` bigint(20) DEFAULT NULL,
  `id_kecamatan` bigint(20) DEFAULT NULL,
  `id_status_sekolah` bigint(20) DEFAULT NULL,
  `id_jenjang_sekolah` bigint(20) DEFAULT NULL,
  `id_term_aktif` int(11) NOT NULL DEFAULT '1',
  `id_tahun_ajaran_aktif` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `m_sekolah`
--

INSERT INTO `m_sekolah` (`id_sekolah`, `nss`, `nps`, `nama_strip`, `sekolah`, `alamat`, `kode_pos`, `telepon`, `fax`, `sk_pendirian`, `tgl_sk_pendirian`, `no_sk_akreditasi`, `tgl_akreditasi`, `email`, `website`, `luas_halaman`, `luas_tanah`, `luas_bangunan`, `luas_olahraga`, `logo`, `foto`, `status_aktif`, `id_jenis_akreditasi`, `id_kecamatan`, `id_status_sekolah`, `id_jenjang_sekolah`, `id_term_aktif`, `id_tahun_ajaran_aktif`) VALUES
(1, 'qweq', 'qwe', 'qwe', 'sma14', 'qweq', 'qwe', 'qwe', 'qwe', 'qwe', '2015-01-05', 'qwe', '2015-01-07', 'qwe', 'qwe', 12, 12, 123, 12, 'qwe', 'qwe', 1, 1, 1, 1, 4, 1, 1),
(2, 'cv', 'cv', 'cv', 'cv', 'cv', 'cv', 'cv', 'cv', 'cv', '2015-01-01', 'cv', '2015-01-12', 'cv', 'cv', 12, 212, 12, 12, '12', '12', 1, 1, 1, 1, 3, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_sekolah_misi`
--

CREATE TABLE IF NOT EXISTS `m_sekolah_misi` (
`id_sekolah_misi` bigint(20) NOT NULL,
  `misi` varchar(255) DEFAULT NULL,
  `urutan` tinyint(4) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_sekolah_visi`
--

CREATE TABLE IF NOT EXISTS `m_sekolah_visi` (
`id_sekolah_visi` bigint(20) NOT NULL,
  `visi` varchar(255) DEFAULT NULL,
  `urutan` tinyint(4) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `m_silabus`
--

CREATE TABLE IF NOT EXISTS `m_silabus` (
`id_silabus` bigint(20) NOT NULL,
  `nama_silabus` varchar(150) DEFAULT NULL,
  `lokasi_file` varchar(255) DEFAULT NULL,
  `id_pelajaran` bigint(20) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL,
  `id_kurikulum` int(11) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `m_silabus`
--

INSERT INTO `m_silabus` (`id_silabus`, `nama_silabus`, `lokasi_file`, `id_pelajaran`, `id_sekolah`, `id_kurikulum`) VALUES
(1, 'matemat', NULL, 0, 0, 0),
(2, 'sdf', NULL, 0, 0, 0),
(3, 'qweqe', 'C:/xampp/htdocs/smsbkv2/extras/silabus/March_Kanzashi.txt', 1, 1, 2),
(4, 'qwe', 'C:/xampp/htdocs/smsbkv2/extras/silabus/March_Kanzashi2.txt', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_siswa`
--

CREATE TABLE IF NOT EXISTS `m_siswa` (
`id_siswa` bigint(20) NOT NULL,
  `telepon` char(20) DEFAULT NULL,
  `nis` varchar(20) DEFAULT NULL,
  `nisn` varchar(20) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `tempat_lahir` varchar(100) DEFAULT NULL,
  `tanggal_lahir` date DEFAULT NULL,
  `jenis_kelamin` varchar(10) DEFAULT NULL,
  `anak_ke` tinyint(4) DEFAULT NULL,
  `jumlah_sdr` tinyint(4) DEFAULT NULL,
  `alamat` varchar(150) DEFAULT NULL,
  `kode_pos` varchar(5) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `status_aktif` tinyint(4) DEFAULT '1',
  `id_agama` bigint(20) DEFAULT NULL,
  `id_golongan_darah` bigint(20) DEFAULT NULL,
  `id_status_anak` bigint(20) DEFAULT NULL,
  `id_kecamatan` bigint(20) DEFAULT NULL,
  `id_asal_sekolah` bigint(20) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_siswa`
--

INSERT INTO `m_siswa` (`id_siswa`, `telepon`, `nis`, `nisn`, `nama`, `tempat_lahir`, `tanggal_lahir`, `jenis_kelamin`, `anak_ke`, `jumlah_sdr`, `alamat`, `kode_pos`, `foto`, `status_aktif`, `id_agama`, `id_golongan_darah`, `id_status_anak`, `id_kecamatan`, `id_asal_sekolah`, `id_sekolah`) VALUES
(1, 'qwe', 'qwe', 'qwe', 'hasan', 'qwe', '2014-12-01', 'l', 1, 2, 'sfsdf', 'asda', 'asdad', 1, 1, 1, 1, 1, 1, 1),
(2, 'asd', 'asd', 'asd', 'husein', 'asd', '2014-12-01', 'l', 2, 3, 'asd', 'ASD', 'asd', 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_tahun_ajaran`
--

CREATE TABLE IF NOT EXISTS `m_tahun_ajaran` (
`id_tahun_ajaran` bigint(20) NOT NULL,
  `tahun_awal` int(11) NOT NULL,
  `tahun_akhir` int(11) NOT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_tahun_ajaran`
--

INSERT INTO `m_tahun_ajaran` (`id_tahun_ajaran`, `tahun_awal`, `tahun_akhir`, `keterangan`) VALUES
(1, 2014, 2015, 'asdad'),
(2, 2015, 2016, 'qweqweqe');

-- --------------------------------------------------------

--
-- Table structure for table `m_term`
--

CREATE TABLE IF NOT EXISTS `m_term` (
`id_term` bigint(20) NOT NULL,
  `term` varchar(30) DEFAULT NULL,
  `id_tahun_ajaran` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `m_term`
--

INSERT INTO `m_term` (`id_term`, `term`, `id_tahun_ajaran`) VALUES
(1, 'Semester 1', 1),
(2, 'Semester 2', 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_tingkat_kelas`
--

CREATE TABLE IF NOT EXISTS `m_tingkat_kelas` (
`id_tingkat_kelas` bigint(20) NOT NULL,
  `tingkat_kelas` varchar(20) DEFAULT NULL,
  `id_jenjang_sekolah` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `m_tingkat_kelas`
--

INSERT INTO `m_tingkat_kelas` (`id_tingkat_kelas`, `tingkat_kelas`, `id_jenjang_sekolah`) VALUES
(1, 'I', 2),
(2, 'II', 2),
(3, 'III', 2),
(4, 'IV', 2),
(5, 'V', 2),
(6, 'VI', 2),
(7, 'VII', 3),
(8, 'VIII', 3),
(9, 'IX', 3),
(10, 'X', 4),
(11, 'XI', 4),
(12, 'XII', 4);

-- --------------------------------------------------------

--
-- Table structure for table `r_agama`
--

CREATE TABLE IF NOT EXISTS `r_agama` (
`id_agama` bigint(20) NOT NULL,
  `agama` varchar(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_asal_sekolah`
--

CREATE TABLE IF NOT EXISTS `r_asal_sekolah` (
`id_asal_sekolah` bigint(20) NOT NULL,
  `nps` varchar(10) NOT NULL,
  `nama_sekolah` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `id_jenjang_sekolah` bigint(20) DEFAULT NULL,
  `id_kecamatan` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_golongan_darah`
--

CREATE TABLE IF NOT EXISTS `r_golongan_darah` (
`id_golongan_darah` bigint(20) NOT NULL,
  `golongan_darah` varchar(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_jenis_absen`
--

CREATE TABLE IF NOT EXISTS `r_jenis_absen` (
`id_jenis_absen` bigint(20) NOT NULL,
  `jenis_absen` varchar(64) DEFAULT NULL,
  `status_hadir` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `r_jenis_absen`
--

INSERT INTO `r_jenis_absen` (`id_jenis_absen`, `jenis_absen`, `status_hadir`) VALUES
(1, 'hadir', 1),
(2, 'sakit', 0),
(3, 'ijin', 0),
(4, 'alpha', 0);

-- --------------------------------------------------------

--
-- Table structure for table `r_jenis_akreditasi`
--

CREATE TABLE IF NOT EXISTS `r_jenis_akreditasi` (
`id_jenis_akreditasi` bigint(20) NOT NULL,
  `jenis_akreditasi` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_jenjang_pendidikan`
--

CREATE TABLE IF NOT EXISTS `r_jenjang_pendidikan` (
`id_jenjang_pendidikan` bigint(20) NOT NULL,
  `jenjang_pendidikan` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_jenjang_sekolah`
--

CREATE TABLE IF NOT EXISTS `r_jenjang_sekolah` (
`id_jenjang_sekolah` bigint(20) NOT NULL,
  `jenjang_sekolah` varchar(10) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `r_jenjang_sekolah`
--

INSERT INTO `r_jenjang_sekolah` (`id_jenjang_sekolah`, `jenjang_sekolah`) VALUES
(1, 'TK'),
(2, 'SD'),
(3, 'SMP'),
(4, 'SMA');

-- --------------------------------------------------------

--
-- Table structure for table `r_kabupaten_kota`
--

CREATE TABLE IF NOT EXISTS `r_kabupaten_kota` (
`id_kabupaten_kota` bigint(20) NOT NULL,
  `kabupaten_kota` varchar(255) DEFAULT NULL,
  `id_provinsi` bigint(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_kecamatan`
--

CREATE TABLE IF NOT EXISTS `r_kecamatan` (
`id_kecamatan` bigint(20) NOT NULL,
  `kecamatan` varchar(255) DEFAULT NULL,
  `id_kabupaten_kota` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_kualifikasi_guru`
--

CREATE TABLE IF NOT EXISTS `r_kualifikasi_guru` (
`id_kualifikasi_guru` bigint(20) NOT NULL,
  `kualifikasi_guru` varchar(150) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `r_kualifikasi_guru`
--

INSERT INTO `r_kualifikasi_guru` (`id_kualifikasi_guru`, `kualifikasi_guru`, `deskripsi`) VALUES
(1, 'Matematika', NULL),
(2, 'Fisika', NULL),
(3, 'Bahasa Indonesia', NULL),
(4, 'Bahasa Inggris', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `r_pangkat_golongan`
--

CREATE TABLE IF NOT EXISTS `r_pangkat_golongan` (
`id_pangkat_golongan` bigint(20) NOT NULL,
  `pangkat` varchar(50) DEFAULT NULL,
  `golongan` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_pekerjaan`
--

CREATE TABLE IF NOT EXISTS `r_pekerjaan` (
`id_pekerjaan` bigint(20) NOT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_pihak_komunikasi`
--

CREATE TABLE IF NOT EXISTS `r_pihak_komunikasi` (
`id_pihak_komunikasi` bigint(20) NOT NULL,
  `pihak_komunikasi` varchar(200) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_provinsi`
--

CREATE TABLE IF NOT EXISTS `r_provinsi` (
`id_provinsi` bigint(20) NOT NULL,
  `provinsi` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_range_penghasilan`
--

CREATE TABLE IF NOT EXISTS `r_range_penghasilan` (
`id_range_penghasilan` bigint(20) NOT NULL,
  `range_penghasilan` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_status_anak`
--

CREATE TABLE IF NOT EXISTS `r_status_anak` (
`id_status_anak` bigint(20) NOT NULL,
  `status_anak` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_status_pegawai`
--

CREATE TABLE IF NOT EXISTS `r_status_pegawai` (
`id_status_pegawai` bigint(20) NOT NULL,
  `status` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_status_pernikahan`
--

CREATE TABLE IF NOT EXISTS `r_status_pernikahan` (
`id_status_pernikahan` bigint(20) NOT NULL,
  `status_pernikahan` varchar(30) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_status_sekolah`
--

CREATE TABLE IF NOT EXISTS `r_status_sekolah` (
`id_status_sekolah` bigint(20) NOT NULL,
  `status_sekolah` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `r_tingkat_wilayah`
--

CREATE TABLE IF NOT EXISTS `r_tingkat_wilayah` (
`id_tingkat_wilayah` bigint(20) NOT NULL,
  `tingkat_wilayah` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_code_numbering`
--

CREATE TABLE IF NOT EXISTS `sys_code_numbering` (
`id_code_numbering` bigint(20) NOT NULL,
  `table_name` varchar(100) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `current_state` varchar(100) DEFAULT NULL,
  `code_numbering` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sys_group`
--

CREATE TABLE IF NOT EXISTS `sys_group` (
`id_group` int(11) NOT NULL,
  `groupname` varchar(100) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `sys_group`
--

INSERT INTO `sys_group` (`id_group`, `groupname`) VALUES
(1, 'Super Admin'),
(2, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `sys_information`
--

CREATE TABLE IF NOT EXISTS `sys_information` (
`id_information` int(11) NOT NULL,
  `information_name` varchar(100) NOT NULL,
  `information_label` varchar(100) NOT NULL,
  `information_type` varchar(100) NOT NULL,
  `information_data` varchar(255) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sys_information`
--

INSERT INTO `sys_information` (`id_information`, `information_name`, `information_label`, `information_type`, `information_data`) VALUES
(1, 'nama_sistem', '', '', 'SMSBK');

-- --------------------------------------------------------

--
-- Table structure for table `sys_user`
--

CREATE TABLE IF NOT EXISTS `sys_user` (
`id_user` bigint(20) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `default_password` varchar(100) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `id_personal` bigint(20) DEFAULT NULL,
  `is_active` bit(1) DEFAULT NULL,
  `id_group` int(11) DEFAULT NULL,
  `id_sekolah` int(11) NOT NULL DEFAULT '1'
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `sys_user`
--

INSERT INTO `sys_user` (`id_user`, `username`, `password`, `default_password`, `email`, `date_created`, `id_personal`, `is_active`, `id_group`, `id_sekolah`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', NULL, NULL, NULL, NULL, b'1', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_agenda_kelas`
--

CREATE TABLE IF NOT EXISTS `t_agenda_kelas` (
`id_agenda_kelas` bigint(20) NOT NULL,
  `tanggal` date DEFAULT NULL,
  `materi` varchar(255) DEFAULT NULL,
  `proses_belajar` varchar(255) DEFAULT NULL,
  `id_guru_matpel_rombel` bigint(20) DEFAULT NULL,
  `id_term` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `t_agenda_kelas`
--

INSERT INTO `t_agenda_kelas` (`id_agenda_kelas`, `tanggal`, `materi`, `proses_belajar`, `id_guru_matpel_rombel`, `id_term`) VALUES
(1, '2014-12-01', 'qwe', 'qwe', 1, 1),
(2, '2014-12-03', 'asdasdd', 'asdad', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_ekstra_kurikuler_fasilitas`
--

CREATE TABLE IF NOT EXISTS `t_ekstra_kurikuler_fasilitas` (
`id_ekstra_kurikuler_fasilitas` bigint(20) NOT NULL,
  `id_ekstra_kurikuler` bigint(20) DEFAULT NULL,
  `ekstra_kurikuler_fasilitas` varchar(255) DEFAULT NULL,
  `jumlah_baik` int(11) DEFAULT NULL,
  `jumlah_rusak_ringan` int(11) DEFAULT NULL,
  `jumlah_rusak_berat` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_ekstra_kurikuler_peserta`
--

CREATE TABLE IF NOT EXISTS `t_ekstra_kurikuler_peserta` (
`id_ekstra_kurikuler_peserta` bigint(20) NOT NULL,
  `id_ekstra_kurikuler` bigint(20) DEFAULT NULL,
  `id_siswa` bigint(20) DEFAULT NULL,
  `status_aktif` tinyint(4) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_guru_matpel_rombel`
--

CREATE TABLE IF NOT EXISTS `t_guru_matpel_rombel` (
`id_guru_matpel_rombel` bigint(20) NOT NULL,
  `id_rombel` bigint(20) DEFAULT NULL,
  `id_pelajaran` bigint(20) DEFAULT NULL,
  `id_guru` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `t_guru_matpel_rombel`
--

INSERT INTO `t_guru_matpel_rombel` (`id_guru_matpel_rombel`, `id_rombel`, `id_pelajaran`, `id_guru`) VALUES
(1, 1, 1, 1),
(2, 2, 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_komunikasi`
--

CREATE TABLE IF NOT EXISTS `t_komunikasi` (
`id_komunikasi` bigint(20) NOT NULL,
  `kegiatan` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `hasil` text,
  `id_pihak_komunikasi` bigint(20) DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_prestasi_siswa`
--

CREATE TABLE IF NOT EXISTS `t_prestasi_siswa` (
`id_prestasi_siswa` bigint(20) NOT NULL,
  `prestasi` varchar(255) DEFAULT NULL,
  `tanggal` date DEFAULT NULL,
  `kategori` varchar(255) DEFAULT NULL,
  `perlombaan` varchar(255) DEFAULT NULL,
  `id_tingkat_wilayah` bigint(20) DEFAULT NULL,
  `id_ekstra_kurikuler` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_rombel_detail`
--

CREATE TABLE IF NOT EXISTS `t_rombel_detail` (
`id_rombel_detail` bigint(20) NOT NULL,
  `id_rombel` bigint(20) DEFAULT NULL,
  `id_siswa` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `t_rpp`
--

CREATE TABLE IF NOT EXISTS `t_rpp` (
`id_rpp` bigint(20) NOT NULL,
  `nama_rpp` varchar(255) DEFAULT NULL,
  `pertemuan_ke` varchar(100) DEFAULT NULL,
  `lokasi_file` varchar(255) DEFAULT NULL,
  `id_guru_matpel_rombel` bigint(20) DEFAULT NULL,
  `id_term` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `t_rpp`
--

INSERT INTO `t_rpp` (`id_rpp`, `nama_rpp`, `pertemuan_ke`, `lokasi_file`, `id_guru_matpel_rombel`, `id_term`) VALUES
(1, 'RPP versi 1', '1', 'asdada', 1, 1),
(3, 'qwe', '2', '', 1, 1),
(5, '123', '123', 'C:/xampp/htdocs/smsbkv2/extras/rpp/March_Kanzashi1.txt', 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `t_siswa_absen`
--

CREATE TABLE IF NOT EXISTS `t_siswa_absen` (
`id_siswa_absen` bigint(20) NOT NULL,
  `id_jenis_absen` bigint(20) DEFAULT NULL,
  `id_agenda_kelas` bigint(20) DEFAULT NULL,
  `id_siswa` bigint(20) DEFAULT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `t_siswa_absen`
--

INSERT INTO `t_siswa_absen` (`id_siswa_absen`, `id_jenis_absen`, `id_agenda_kelas`, `id_siswa`) VALUES
(1, 1, 1, 1),
(2, 1, 2, 1);

-- --------------------------------------------------------

--
-- Stand-in structure for view `v_siswa_absen_rekap`
--
CREATE TABLE IF NOT EXISTS `v_siswa_absen_rekap` (
`id_guru_matpel_rombel` bigint(20)
,`id_jenis_absen` bigint(20)
,`nis` varchar(20)
,`nama` varchar(100)
,`jumlah` bigint(21)
);
-- --------------------------------------------------------

--
-- Structure for view `v_siswa_absen_rekap`
--
DROP TABLE IF EXISTS `v_siswa_absen_rekap`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `v_siswa_absen_rekap` AS select `gm`.`id_guru_matpel_rombel` AS `id_guru_matpel_rombel`,`sa`.`id_jenis_absen` AS `id_jenis_absen`,`s`.`nis` AS `nis`,`s`.`nama` AS `nama`,count(`sa`.`id_siswa_absen`) AS `jumlah` from (((`t_siswa_absen` `sa` left join `m_siswa` `s` on((`s`.`id_siswa` = `sa`.`id_siswa`))) left join `t_agenda_kelas` `ak` on((`ak`.`id_agenda_kelas` = `sa`.`id_agenda_kelas`))) left join `t_guru_matpel_rombel` `gm` on((`gm`.`id_guru_matpel_rombel` = `ak`.`id_guru_matpel_rombel`))) group by `gm`.`id_guru_matpel_rombel`,`s`.`nis`,`s`.`nama`,`sa`.`id_jenis_absen`;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `m_alokasi_waktu_per_minggu`
--
ALTER TABLE `m_alokasi_waktu_per_minggu`
 ADD PRIMARY KEY (`id_alokasi_waktu_per_minggu`), ADD KEY `fk_fk_m_alokasi_waktu_per_minggu_0` (`id_tingkat_kelas`), ADD KEY `fk_fk_m_alokasi_waktu_per_minggu_1` (`id_pelajaran`);

--
-- Indexes for table `m_beban_belajar`
--
ALTER TABLE `m_beban_belajar`
 ADD PRIMARY KEY (`id_beban_belajar`), ADD KEY `fk_m_beban_belajar_0` (`id_kurikulum`), ADD KEY `fk_m_beban_belajar_1` (`id_tingkat_kelas`);

--
-- Indexes for table `m_dokumen_kurikulum`
--
ALTER TABLE `m_dokumen_kurikulum`
 ADD PRIMARY KEY (`id_dokumen_kurikulum`), ADD KEY `fk_m_dokumen_kurikulum_0` (`id_sekolah`);

--
-- Indexes for table `m_ekstra_kurikuler`
--
ALTER TABLE `m_ekstra_kurikuler`
 ADD PRIMARY KEY (`id_ekstra_kurikuler`), ADD KEY `fk_m_ekstra_kurikuler_0` (`id_sekolah`), ADD KEY `fk_m_ekstra_kurikuler_1` (`id_guru`);

--
-- Indexes for table `m_ekstra_kurikuler_jadwal`
--
ALTER TABLE `m_ekstra_kurikuler_jadwal`
 ADD PRIMARY KEY (`id_ekstra_kurikuler_jadwal`), ADD KEY `fk_m_ekstra_kurikuler_jadwal_0` (`id_ekstra_kurikuler`);

--
-- Indexes for table `m_guru`
--
ALTER TABLE `m_guru`
 ADD PRIMARY KEY (`id_guru`), ADD KEY `fk_m_guru_0` (`id_golongan_darah`), ADD KEY `fk_m_guru_1` (`id_status_pernikahan`), ADD KEY `fk_m_guru_2` (`id_agama`), ADD KEY `fk_m_guru_3` (`id_status_pegawai`), ADD KEY `fk_m_guru_4` (`id_kecamatan`), ADD KEY `fk_m_guru_5` (`id_jenjang_pendidikan`), ADD KEY `fk_m_guru_6` (`id_pangkat_golongan`), ADD KEY `fk_m_guru_7` (`id_kualifikasi_guru`), ADD KEY `fk_m_guru_8` (`id_sekolah`);

--
-- Indexes for table `m_jadwal_pelajaran`
--
ALTER TABLE `m_jadwal_pelajaran`
 ADD PRIMARY KEY (`id_jadwal_pelajaran`), ADD KEY `fk_m_jadwal_pelajaran_0` (`id_sekolah`);

--
-- Indexes for table `m_jadwal_pelajaran_detail`
--
ALTER TABLE `m_jadwal_pelajaran_detail`
 ADD PRIMARY KEY (`id_jadwal_pelajaran_detail`), ADD KEY `fk_m_jadwal_pelajaran_detail_0` (`id_ruang_belajar`), ADD KEY `fk_m_jadwal_pelajaran_detail_1` (`id_jam_pelajaran`), ADD KEY `fk_m_jadwal_pelajaran_detail_2` (`id_guru_matpel_rombel`), ADD KEY `fk_m_jadwal_pelajaran_detail_3` (`id_jadwal_pelajaran`);

--
-- Indexes for table `m_jam_pelajaran`
--
ALTER TABLE `m_jam_pelajaran`
 ADD PRIMARY KEY (`id_jam_pelajaran`), ADD KEY `fk_m_jam_pelajaran_0` (`id_sekolah`);

--
-- Indexes for table `m_kelompok_pelajaran`
--
ALTER TABLE `m_kelompok_pelajaran`
 ADD PRIMARY KEY (`id_kelompok_pelajaran`), ADD KEY `fk_m_kelompok_pelajaran_0` (`id_kurikulum`);

--
-- Indexes for table `m_kompetensi_inti`
--
ALTER TABLE `m_kompetensi_inti`
 ADD PRIMARY KEY (`id_kompetensi_inti`), ADD KEY `fk_m_kompetensi_inti_0` (`id_kurikulum`), ADD KEY `fk_m_kompetensi_inti_1` (`id_tingkat_kelas`);

--
-- Indexes for table `m_komponen_nilai`
--
ALTER TABLE `m_komponen_nilai`
 ADD PRIMARY KEY (`id_komponen_nilai`), ADD KEY `fk_m_komponen_nilai_0` (`id_kurikulum`);

--
-- Indexes for table `m_konversi_nilai`
--
ALTER TABLE `m_konversi_nilai`
 ADD PRIMARY KEY (`id_konversi_nilai`), ADD KEY `fk_m_konversi_nilai_0` (`id_komponen_nilai`);

--
-- Indexes for table `m_kurikulum`
--
ALTER TABLE `m_kurikulum`
 ADD PRIMARY KEY (`id_kurikulum`);

--
-- Indexes for table `m_ortu`
--
ALTER TABLE `m_ortu`
 ADD PRIMARY KEY (`id_ortu`), ADD KEY `fk_m_ortu_0` (`id_jenjang_pendidikan_ayah`), ADD KEY `fk_m_ortu_1` (`id_pekerjaan_ayah`), ADD KEY `fk_fk_m_ortu_10` (`id_range_penghasilan_wali`), ADD KEY `fk_fk_m_ortu_11` (`id_kecamatan_wali`), ADD KEY `fk_fk_m_ortu_12` (`id_jenjang_pendidikan_ibu`), ADD KEY `fk_m_ortu_2` (`id_range_penghasilan_ayah`), ADD KEY `fk_m_ortu_3` (`id_kecamatan_ayah`), ADD KEY `fk_m_ortu_4` (`id_siswa`), ADD KEY `fk_fk_m_ortu_5` (`id_pekerjaan_ibu`), ADD KEY `fk_fk_m_ortu_6` (`id_range_penghasilan_ibu`), ADD KEY `fk_fk_m_ortu_7` (`id_kecamatan_ibu`), ADD KEY `fk_fk_m_ortu_8` (`id_jenjang_pendidikan_wali`), ADD KEY `fk_fk_m_ortu_9` (`id_pekerjaan_wali`);

--
-- Indexes for table `m_pelajaran`
--
ALTER TABLE `m_pelajaran`
 ADD PRIMARY KEY (`id_pelajaran`), ADD KEY `fk_m_pelajaran_0` (`id_kelompok_pelajaran`), ADD KEY `fk_m_pelajaran_1` (`id_kualifikasi_guru`), ADD KEY `fk_m_pelajaran_2` (`id_peminatan`), ADD KEY `fk_fk_m_pelajaran_9` (`id_tingkat_kelas`);

--
-- Indexes for table `m_peminatan`
--
ALTER TABLE `m_peminatan`
 ADD PRIMARY KEY (`id_peminatan`), ADD KEY `fk_m_peminatan_0` (`id_kelompok_pelajaran`);

--
-- Indexes for table `m_program`
--
ALTER TABLE `m_program`
 ADD PRIMARY KEY (`id_program`), ADD KEY `fk_m_program_0` (`id_peminatan`), ADD KEY `fk_m_program_1` (`id_sekolah`);

--
-- Indexes for table `m_rombel`
--
ALTER TABLE `m_rombel`
 ADD PRIMARY KEY (`id_rombel`), ADD KEY `fk_m_rombel_0` (`id_tingkat_kelas`), ADD KEY `fk_m_rombel_1` (`id_tahun_ajaran`), ADD KEY `fk_m_rombel_2` (`id_program`);

--
-- Indexes for table `m_rpp`
--
ALTER TABLE `m_rpp`
 ADD PRIMARY KEY (`id_rpp`);

--
-- Indexes for table `m_ruang_belajar`
--
ALTER TABLE `m_ruang_belajar`
 ADD PRIMARY KEY (`id_ruang_belajar`), ADD KEY `fk_m_ruang_belajar_0` (`id_sekolah`);

--
-- Indexes for table `m_sekolah`
--
ALTER TABLE `m_sekolah`
 ADD PRIMARY KEY (`id_sekolah`), ADD KEY `fk_m_sekolah_0` (`id_jenis_akreditasi`), ADD KEY `fk_m_sekolah_1` (`id_kecamatan`), ADD KEY `fk_m_sekolah_2` (`id_status_sekolah`), ADD KEY `fk_m_sekolah_3` (`id_jenjang_sekolah`);

--
-- Indexes for table `m_sekolah_misi`
--
ALTER TABLE `m_sekolah_misi`
 ADD PRIMARY KEY (`id_sekolah_misi`), ADD KEY `fk_m_sekolah_misi_0` (`id_sekolah`);

--
-- Indexes for table `m_sekolah_visi`
--
ALTER TABLE `m_sekolah_visi`
 ADD PRIMARY KEY (`id_sekolah_visi`), ADD KEY `fk_m_sekolah_visi_0` (`id_sekolah`);

--
-- Indexes for table `m_silabus`
--
ALTER TABLE `m_silabus`
 ADD PRIMARY KEY (`id_silabus`), ADD KEY `fk_m_silabus_0` (`id_pelajaran`), ADD KEY `fk_m_silabus_1` (`id_sekolah`);

--
-- Indexes for table `m_siswa`
--
ALTER TABLE `m_siswa`
 ADD PRIMARY KEY (`id_siswa`), ADD KEY `fk_m_siswa_0` (`id_agama`), ADD KEY `fk_m_siswa_1` (`id_golongan_darah`), ADD KEY `fk_m_siswa_2` (`id_status_anak`), ADD KEY `fk_m_siswa_3` (`id_kecamatan`), ADD KEY `fk_m_siswa_4` (`id_asal_sekolah`), ADD KEY `fk_m_siswa_5` (`id_sekolah`);

--
-- Indexes for table `m_tahun_ajaran`
--
ALTER TABLE `m_tahun_ajaran`
 ADD PRIMARY KEY (`id_tahun_ajaran`);

--
-- Indexes for table `m_term`
--
ALTER TABLE `m_term`
 ADD PRIMARY KEY (`id_term`), ADD KEY `fk_m_term_0` (`id_tahun_ajaran`);

--
-- Indexes for table `m_tingkat_kelas`
--
ALTER TABLE `m_tingkat_kelas`
 ADD PRIMARY KEY (`id_tingkat_kelas`), ADD KEY `fk_m_tingkat_kelas_0` (`id_jenjang_sekolah`);

--
-- Indexes for table `r_agama`
--
ALTER TABLE `r_agama`
 ADD PRIMARY KEY (`id_agama`);

--
-- Indexes for table `r_asal_sekolah`
--
ALTER TABLE `r_asal_sekolah`
 ADD PRIMARY KEY (`id_asal_sekolah`), ADD KEY `fk_r_asal_sekolah_0` (`id_jenjang_sekolah`), ADD KEY `fk_r_asal_sekolah_1` (`id_kecamatan`);

--
-- Indexes for table `r_golongan_darah`
--
ALTER TABLE `r_golongan_darah`
 ADD PRIMARY KEY (`id_golongan_darah`);

--
-- Indexes for table `r_jenis_absen`
--
ALTER TABLE `r_jenis_absen`
 ADD PRIMARY KEY (`id_jenis_absen`);

--
-- Indexes for table `r_jenis_akreditasi`
--
ALTER TABLE `r_jenis_akreditasi`
 ADD PRIMARY KEY (`id_jenis_akreditasi`);

--
-- Indexes for table `r_jenjang_pendidikan`
--
ALTER TABLE `r_jenjang_pendidikan`
 ADD PRIMARY KEY (`id_jenjang_pendidikan`);

--
-- Indexes for table `r_jenjang_sekolah`
--
ALTER TABLE `r_jenjang_sekolah`
 ADD PRIMARY KEY (`id_jenjang_sekolah`);

--
-- Indexes for table `r_kabupaten_kota`
--
ALTER TABLE `r_kabupaten_kota`
 ADD PRIMARY KEY (`id_kabupaten_kota`), ADD KEY `fk_r_kabupaten_kota_0` (`id_provinsi`);

--
-- Indexes for table `r_kecamatan`
--
ALTER TABLE `r_kecamatan`
 ADD PRIMARY KEY (`id_kecamatan`), ADD KEY `fk_r_kecamatan_0` (`id_kabupaten_kota`);

--
-- Indexes for table `r_kualifikasi_guru`
--
ALTER TABLE `r_kualifikasi_guru`
 ADD PRIMARY KEY (`id_kualifikasi_guru`);

--
-- Indexes for table `r_pangkat_golongan`
--
ALTER TABLE `r_pangkat_golongan`
 ADD PRIMARY KEY (`id_pangkat_golongan`);

--
-- Indexes for table `r_pekerjaan`
--
ALTER TABLE `r_pekerjaan`
 ADD PRIMARY KEY (`id_pekerjaan`);

--
-- Indexes for table `r_pihak_komunikasi`
--
ALTER TABLE `r_pihak_komunikasi`
 ADD PRIMARY KEY (`id_pihak_komunikasi`);

--
-- Indexes for table `r_provinsi`
--
ALTER TABLE `r_provinsi`
 ADD PRIMARY KEY (`id_provinsi`);

--
-- Indexes for table `r_range_penghasilan`
--
ALTER TABLE `r_range_penghasilan`
 ADD PRIMARY KEY (`id_range_penghasilan`);

--
-- Indexes for table `r_status_anak`
--
ALTER TABLE `r_status_anak`
 ADD PRIMARY KEY (`id_status_anak`);

--
-- Indexes for table `r_status_pegawai`
--
ALTER TABLE `r_status_pegawai`
 ADD PRIMARY KEY (`id_status_pegawai`);

--
-- Indexes for table `r_status_pernikahan`
--
ALTER TABLE `r_status_pernikahan`
 ADD PRIMARY KEY (`id_status_pernikahan`);

--
-- Indexes for table `r_status_sekolah`
--
ALTER TABLE `r_status_sekolah`
 ADD PRIMARY KEY (`id_status_sekolah`);

--
-- Indexes for table `r_tingkat_wilayah`
--
ALTER TABLE `r_tingkat_wilayah`
 ADD PRIMARY KEY (`id_tingkat_wilayah`);

--
-- Indexes for table `sys_code_numbering`
--
ALTER TABLE `sys_code_numbering`
 ADD PRIMARY KEY (`id_code_numbering`);

--
-- Indexes for table `sys_group`
--
ALTER TABLE `sys_group`
 ADD PRIMARY KEY (`id_group`);

--
-- Indexes for table `sys_information`
--
ALTER TABLE `sys_information`
 ADD PRIMARY KEY (`id_information`);

--
-- Indexes for table `sys_user`
--
ALTER TABLE `sys_user`
 ADD PRIMARY KEY (`id_user`), ADD KEY `fk_sys_user_0` (`id_group`);

--
-- Indexes for table `t_agenda_kelas`
--
ALTER TABLE `t_agenda_kelas`
 ADD PRIMARY KEY (`id_agenda_kelas`), ADD KEY `fk_t_agenda_kelas_0` (`id_guru_matpel_rombel`), ADD KEY `fk_fk_t_agenda_kelas_1` (`id_term`);

--
-- Indexes for table `t_ekstra_kurikuler_fasilitas`
--
ALTER TABLE `t_ekstra_kurikuler_fasilitas`
 ADD PRIMARY KEY (`id_ekstra_kurikuler_fasilitas`), ADD KEY `fk_fk_t_ekskul_jadwal_0` (`id_ekstra_kurikuler`);

--
-- Indexes for table `t_ekstra_kurikuler_peserta`
--
ALTER TABLE `t_ekstra_kurikuler_peserta`
 ADD PRIMARY KEY (`id_ekstra_kurikuler_peserta`), ADD KEY `fk_fk_t_ekskul_peserta_0` (`id_ekstra_kurikuler`), ADD KEY `fk_fk_t_ekskul_peserta_1` (`id_siswa`);

--
-- Indexes for table `t_guru_matpel_rombel`
--
ALTER TABLE `t_guru_matpel_rombel`
 ADD PRIMARY KEY (`id_guru_matpel_rombel`), ADD KEY `fk_fk_t_guru_matpel_rombel_0` (`id_rombel`), ADD KEY `fk_fk_t_guru_matpel_rombel_1` (`id_pelajaran`), ADD KEY `fk_fk_t_guru_matpel_rombel_2` (`id_guru`);

--
-- Indexes for table `t_komunikasi`
--
ALTER TABLE `t_komunikasi`
 ADD PRIMARY KEY (`id_komunikasi`), ADD KEY `fk_t_komunikasi_0` (`id_pihak_komunikasi`), ADD KEY `fk_fk_t_komunikasi_1` (`id_sekolah`);

--
-- Indexes for table `t_prestasi_siswa`
--
ALTER TABLE `t_prestasi_siswa`
 ADD PRIMARY KEY (`id_prestasi_siswa`), ADD KEY `fk_t_prestasi_siswa_0` (`id_tingkat_wilayah`), ADD KEY `fk_fk_t_prestasi_siswa_1` (`id_ekstra_kurikuler`);

--
-- Indexes for table `t_rombel_detail`
--
ALTER TABLE `t_rombel_detail`
 ADD PRIMARY KEY (`id_rombel_detail`), ADD KEY `fk_fk_t_rombel_detail_0` (`id_rombel`), ADD KEY `fk_fk_t_rombel_detail_1` (`id_siswa`);

--
-- Indexes for table `t_rpp`
--
ALTER TABLE `t_rpp`
 ADD PRIMARY KEY (`id_rpp`), ADD KEY `fk_t_rpp_0` (`id_guru_matpel_rombel`), ADD KEY `fk_fk_t_rpp_1` (`id_term`);

--
-- Indexes for table `t_siswa_absen`
--
ALTER TABLE `t_siswa_absen`
 ADD PRIMARY KEY (`id_siswa_absen`), ADD KEY `fk_t_siswa_absen_1` (`id_jenis_absen`), ADD KEY `fk_t_siswa_absen_2` (`id_agenda_kelas`), ADD KEY `fk_fk_t_siswa_absen_3` (`id_siswa`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `m_alokasi_waktu_per_minggu`
--
ALTER TABLE `m_alokasi_waktu_per_minggu`
MODIFY `id_alokasi_waktu_per_minggu` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `m_beban_belajar`
--
ALTER TABLE `m_beban_belajar`
MODIFY `id_beban_belajar` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_dokumen_kurikulum`
--
ALTER TABLE `m_dokumen_kurikulum`
MODIFY `id_dokumen_kurikulum` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_ekstra_kurikuler`
--
ALTER TABLE `m_ekstra_kurikuler`
MODIFY `id_ekstra_kurikuler` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_ekstra_kurikuler_jadwal`
--
ALTER TABLE `m_ekstra_kurikuler_jadwal`
MODIFY `id_ekstra_kurikuler_jadwal` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_guru`
--
ALTER TABLE `m_guru`
MODIFY `id_guru` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_jadwal_pelajaran`
--
ALTER TABLE `m_jadwal_pelajaran`
MODIFY `id_jadwal_pelajaran` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_jadwal_pelajaran_detail`
--
ALTER TABLE `m_jadwal_pelajaran_detail`
MODIFY `id_jadwal_pelajaran_detail` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_jam_pelajaran`
--
ALTER TABLE `m_jam_pelajaran`
MODIFY `id_jam_pelajaran` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_kelompok_pelajaran`
--
ALTER TABLE `m_kelompok_pelajaran`
MODIFY `id_kelompok_pelajaran` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_kompetensi_inti`
--
ALTER TABLE `m_kompetensi_inti`
MODIFY `id_kompetensi_inti` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `m_komponen_nilai`
--
ALTER TABLE `m_komponen_nilai`
MODIFY `id_komponen_nilai` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_konversi_nilai`
--
ALTER TABLE `m_konversi_nilai`
MODIFY `id_konversi_nilai` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_kurikulum`
--
ALTER TABLE `m_kurikulum`
MODIFY `id_kurikulum` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_ortu`
--
ALTER TABLE `m_ortu`
MODIFY `id_ortu` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_pelajaran`
--
ALTER TABLE `m_pelajaran`
MODIFY `id_pelajaran` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=79;
--
-- AUTO_INCREMENT for table `m_peminatan`
--
ALTER TABLE `m_peminatan`
MODIFY `id_peminatan` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_program`
--
ALTER TABLE `m_program`
MODIFY `id_program` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `m_rombel`
--
ALTER TABLE `m_rombel`
MODIFY `id_rombel` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_rpp`
--
ALTER TABLE `m_rpp`
MODIFY `id_rpp` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_ruang_belajar`
--
ALTER TABLE `m_ruang_belajar`
MODIFY `id_ruang_belajar` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_sekolah`
--
ALTER TABLE `m_sekolah`
MODIFY `id_sekolah` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `m_sekolah_misi`
--
ALTER TABLE `m_sekolah_misi`
MODIFY `id_sekolah_misi` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_sekolah_visi`
--
ALTER TABLE `m_sekolah_visi`
MODIFY `id_sekolah_visi` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `m_silabus`
--
ALTER TABLE `m_silabus`
MODIFY `id_silabus` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `m_siswa`
--
ALTER TABLE `m_siswa`
MODIFY `id_siswa` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_tahun_ajaran`
--
ALTER TABLE `m_tahun_ajaran`
MODIFY `id_tahun_ajaran` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_term`
--
ALTER TABLE `m_term`
MODIFY `id_term` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `m_tingkat_kelas`
--
ALTER TABLE `m_tingkat_kelas`
MODIFY `id_tingkat_kelas` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `r_agama`
--
ALTER TABLE `r_agama`
MODIFY `id_agama` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_asal_sekolah`
--
ALTER TABLE `r_asal_sekolah`
MODIFY `id_asal_sekolah` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_golongan_darah`
--
ALTER TABLE `r_golongan_darah`
MODIFY `id_golongan_darah` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_jenis_absen`
--
ALTER TABLE `r_jenis_absen`
MODIFY `id_jenis_absen` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `r_jenis_akreditasi`
--
ALTER TABLE `r_jenis_akreditasi`
MODIFY `id_jenis_akreditasi` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_jenjang_pendidikan`
--
ALTER TABLE `r_jenjang_pendidikan`
MODIFY `id_jenjang_pendidikan` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_jenjang_sekolah`
--
ALTER TABLE `r_jenjang_sekolah`
MODIFY `id_jenjang_sekolah` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `r_kabupaten_kota`
--
ALTER TABLE `r_kabupaten_kota`
MODIFY `id_kabupaten_kota` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_kecamatan`
--
ALTER TABLE `r_kecamatan`
MODIFY `id_kecamatan` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_kualifikasi_guru`
--
ALTER TABLE `r_kualifikasi_guru`
MODIFY `id_kualifikasi_guru` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `r_pangkat_golongan`
--
ALTER TABLE `r_pangkat_golongan`
MODIFY `id_pangkat_golongan` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_pekerjaan`
--
ALTER TABLE `r_pekerjaan`
MODIFY `id_pekerjaan` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_pihak_komunikasi`
--
ALTER TABLE `r_pihak_komunikasi`
MODIFY `id_pihak_komunikasi` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_provinsi`
--
ALTER TABLE `r_provinsi`
MODIFY `id_provinsi` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_range_penghasilan`
--
ALTER TABLE `r_range_penghasilan`
MODIFY `id_range_penghasilan` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_status_anak`
--
ALTER TABLE `r_status_anak`
MODIFY `id_status_anak` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_status_pegawai`
--
ALTER TABLE `r_status_pegawai`
MODIFY `id_status_pegawai` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_status_pernikahan`
--
ALTER TABLE `r_status_pernikahan`
MODIFY `id_status_pernikahan` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_status_sekolah`
--
ALTER TABLE `r_status_sekolah`
MODIFY `id_status_sekolah` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `r_tingkat_wilayah`
--
ALTER TABLE `r_tingkat_wilayah`
MODIFY `id_tingkat_wilayah` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_code_numbering`
--
ALTER TABLE `sys_code_numbering`
MODIFY `id_code_numbering` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sys_group`
--
ALTER TABLE `sys_group`
MODIFY `id_group` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `sys_information`
--
ALTER TABLE `sys_information`
MODIFY `id_information` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sys_user`
--
ALTER TABLE `sys_user`
MODIFY `id_user` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `t_agenda_kelas`
--
ALTER TABLE `t_agenda_kelas`
MODIFY `id_agenda_kelas` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_ekstra_kurikuler_fasilitas`
--
ALTER TABLE `t_ekstra_kurikuler_fasilitas`
MODIFY `id_ekstra_kurikuler_fasilitas` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_ekstra_kurikuler_peserta`
--
ALTER TABLE `t_ekstra_kurikuler_peserta`
MODIFY `id_ekstra_kurikuler_peserta` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_guru_matpel_rombel`
--
ALTER TABLE `t_guru_matpel_rombel`
MODIFY `id_guru_matpel_rombel` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `t_komunikasi`
--
ALTER TABLE `t_komunikasi`
MODIFY `id_komunikasi` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_prestasi_siswa`
--
ALTER TABLE `t_prestasi_siswa`
MODIFY `id_prestasi_siswa` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_rombel_detail`
--
ALTER TABLE `t_rombel_detail`
MODIFY `id_rombel_detail` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `t_rpp`
--
ALTER TABLE `t_rpp`
MODIFY `id_rpp` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `t_siswa_absen`
--
ALTER TABLE `t_siswa_absen`
MODIFY `id_siswa_absen` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
