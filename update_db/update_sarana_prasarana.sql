CREATE TABLE IF NOT EXISTS `m_sarana_prasarana` (
  `id_sarana_prasarana` bigint(20) NOT NULL,
  `kode_sarana_prasarana` varchar(30) DEFAULT NULL,
  `nama_sarana_prasarana` varchar(50) DEFAULT NULL,
  `penanggung_jawab` varchar(50) DEFAULT NULL,
  `id_jenis_sarana_prasarana` bigint(20) DEFAULT NULL,
  `id_kondisi` bigint(20) DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL,
  `daya_tampung` double DEFAULT NULL,
  `id_sekolah` bigint(20) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_sarana_prasarana`
--

INSERT INTO `m_sarana_prasarana` (`id_sarana_prasarana`, `kode_sarana_prasarana`, `nama_sarana_prasarana`, `penanggung_jawab`, `id_jenis_sarana_prasarana`, `id_kondisi`, `keterangan`, `daya_tampung`, `id_sekolah`) VALUES
(6, 'K01', 'asdasda124234', 'a', 1, 1, 'a', 100, 1),
(5, '123', 'asdasd', 'asdasd', 3, 1, 'qeqwe', 123, 1),
(30, '123', '123', '1231', 3, 1, '1231', NULL, 1),
(8, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(9, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(10, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(11, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(12, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(13, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(14, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(15, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(16, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(17, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(18, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(19, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(20, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(21, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(22, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(23, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(24, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(25, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(26, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(27, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(28, 'K01', 'a', 'a', 3, 1, 'a', 100, 1),
(29, 'asd', 'adad', 'asdad', 3, 1, '12313', 123, 1),
(33, '123asd', '123', '123', 12, 1, '123', 123, 1);

-- --------------------------------------------------------

--
-- Table structure for table `m_sarana_prasarana_detail`
--

CREATE TABLE IF NOT EXISTS `m_sarana_prasarana_detail` (
  `id_sarana_prasarana_detail` bigint(20) NOT NULL,
  `kode_barang` varchar(30) DEFAULT NULL,
  `nama_barang` varchar(50) DEFAULT NULL,
  `sarana_prasarana_id` bigint(20) DEFAULT NULL,
  `no_tanggal_sertifikat` varchar(32) DEFAULT NULL,
  `cc` varchar(64) DEFAULT NULL,
  `no_rangka` varchar(64) DEFAULT NULL,
  `no_mesin` varchar(64) DEFAULT NULL,
  `merek` varchar(64) DEFAULT NULL,
  `type` varchar(64) DEFAULT NULL,
  `warna` varchar(64) DEFAULT NULL,
  `no_tanggal_bpkb` varchar(32) DEFAULT NULL,
  `no_polisi` varchar(32) DEFAULT NULL,
  `tanggal_peroleh` date DEFAULT NULL,
  `asal_peroleh` varchar(32) DEFAULT NULL,
  `jumlah` double DEFAULT NULL,
  `satuan` varchar(32) DEFAULT NULL,
  `kondisi_id` bigint(20) DEFAULT NULL,
  `harga_total` double DEFAULT NULL,
  `keterangan` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;


CREATE TABLE IF NOT EXISTS `r_sapras_kondisi` (
  `id_sapras_kondisi` bigint(20) NOT NULL,
  `kondisi` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE IF NOT EXISTS `r_jenis_sarana_prasarana` (
  `id_jenis_sarana_prasarana` bigint(20) NOT NULL,
  `jenis_sarana_prasarana` varchar(128) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `r_jenis_sarana_prasarana`
--

INSERT INTO `r_jenis_sarana_prasarana` (`id_jenis_sarana_prasarana`, `jenis_sarana_prasarana`) VALUES
(1, 'Auditorium'),
(2, 'Rangkuman Profil Sarana dan Prasarana'),
(3, 'Ruang Kelas'),
(4, 'Fasilitas Olah Raga'),
(5, 'Laboratorium, Bengkel dan Studio'),
(6, 'Teknologi Informasi dan Komunikasi'),
(7, 'Peralatan Pendidikan'),
(8, 'Perpustakaan Pendidikan'),
(9, 'Penyimpanan'),
(10, 'Pengamanan'),
(11, 'Kafetaria/Warung'),
(12, 'Mesjid/Mushala'),
(13, 'WC/Kamar Mandi'),
(14, 'Listrik dan Air'),
(15, 'Sarana Prasarana Umum'),
(16, 'Sarana Pembelajaran'),
(17, 'Inventaris Buku');
