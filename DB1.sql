CREATE TABLE m_tahun_ajaran (
 id BIGINT(20) NOT NULL,
 tahun_awal INT NOT NULL,
 tahun_akhir INT NOT NULL,
 keterangan VARCHAR(255)
);

ALTER TABLE m_tahun_ajaran ADD CONSTRAINT PK_m_tahun_ajaran PRIMARY KEY (id);


CREATE TABLE r_agama (
 id_agama BIGINT NOT NULL,
 agama VARCHAR(20)
);

ALTER TABLE r_agama ADD CONSTRAINT PK_r_agama PRIMARY KEY (id_agama);


CREATE TABLE r_golongan_darah (
 id_golongan_darah BIGINT NOT NULL,
 golongan_darah VARCHAR(2)
);

ALTER TABLE r_golongan_darah ADD CONSTRAINT PK_r_golongan_darah PRIMARY KEY (id_golongan_darah);


CREATE TABLE r_jenis_akreditasi (
 id_jenis_akreditasi BIGINT NOT NULL,
 jenis_akreditasi VARCHAR(30)
);

ALTER TABLE r_jenis_akreditasi ADD CONSTRAINT PK_r_jenis_akreditasi PRIMARY KEY (id_jenis_akreditasi);


CREATE TABLE r_jenjang_pendidikan (
 id_jenjang_pendidikan BIGINT NOT NULL,
 jenjang_pendidikan VARCHAR(50)
);

ALTER TABLE r_jenjang_pendidikan ADD CONSTRAINT PK_r_jenjang_pendidikan PRIMARY KEY (id_jenjang_pendidikan);


CREATE TABLE r_jenjang_sekolah (
 id_jenjang_sekolah BIGINT NOT NULL,
 jenjang_sekolah VARCHAR(10) NOT NULL
);

ALTER TABLE r_jenjang_sekolah ADD CONSTRAINT PK_r_jenjang_sekolah PRIMARY KEY (id_jenjang_sekolah);


CREATE TABLE r_pangkat_golongan (
 id_pangkat_golongan BIGINT NOT NULL,
 pangkat VARCHAR(50),
 golongan VARCHAR(15)
);

ALTER TABLE r_pangkat_golongan ADD CONSTRAINT PK_r_pangkat_golongan PRIMARY KEY (id_pangkat_golongan);


CREATE TABLE r_provinsi (
 id_provinsi BIGINT NOT NULL,
 provinsi VARCHAR(255)
);

ALTER TABLE r_provinsi ADD CONSTRAINT PK_r_provinsi PRIMARY KEY (id_provinsi);


CREATE TABLE r_status_anak (
 id_status_anak BIGINT NOT NULL,
 status_anak VARCHAR(100)
);

ALTER TABLE r_status_anak ADD CONSTRAINT PK_r_status_anak PRIMARY KEY (id_status_anak);


CREATE TABLE r_status_pegawai (
 id_status_pegawai BIGINT NOT NULL,
 status VARCHAR(50)
);

ALTER TABLE r_status_pegawai ADD CONSTRAINT PK_r_status_pegawai PRIMARY KEY (id_status_pegawai);


CREATE TABLE r_status_pernikahan (
 id_status_pernikahan BIGINT NOT NULL,
 status_pernikahan VARCHAR(30)
);

ALTER TABLE r_status_pernikahan ADD CONSTRAINT PK_r_status_pernikahan PRIMARY KEY (id_status_pernikahan);


CREATE TABLE r_status_sekolah (
 id_status_sekolah BIGINT NOT NULL,
 status_sekolah VARCHAR(100)
);

ALTER TABLE r_status_sekolah ADD CONSTRAINT PK_r_status_sekolah PRIMARY KEY (id_status_sekolah);


CREATE TABLE sys_code_numbering (
 id_code_numbering BIGINT NOT NULL,
 table_name VARCHAR(100),
 length INT(11),
 current_state VARCHAR(100),
 code_numbering VARCHAR(255)
);

ALTER TABLE sys_code_numbering ADD CONSTRAINT PK_sys_code_numbering PRIMARY KEY (id_code_numbering);


CREATE TABLE sys_group (
 id_group INT(11) NOT NULL,
 groupname VARCHAR(100)
);

ALTER TABLE sys_group ADD CONSTRAINT PK_sys_group PRIMARY KEY (id_group);


CREATE TABLE sys_information (
 id_information INT NOT NULL,
 information_name VARCHAR(100) NOT NULL,
 information_label VARCHAR(100) NOT NULL,
 information_type VARCHAR(100) NOT NULL,
 information_data VARCHAR(255) NOT NULL
);

ALTER TABLE sys_information ADD CONSTRAINT PK_sys_information PRIMARY KEY (id_information);


CREATE TABLE sys_user (
 id_user BIGINT NOT NULL,
 username VARCHAR(100),
 password VARCHAR(255),
 default_password VARCHAR(100) NOT NULL,
 email VARCHAR(150),
 date_created DATETIME,
 id_personal BIGINT NOT NULL,
 is_active BIT(1),
 id_group INT(11) NOT NULL
);

ALTER TABLE sys_user ADD CONSTRAINT PK_sys_user PRIMARY KEY (id_user);


CREATE TABLE r_kabupaten_kota (
 id_kabupaten_kota BIGINT(20) NOT NULL,
 kabupaten_kota VARCHAR(255),
 id_provinsi BIGINT NOT NULL
);

ALTER TABLE r_kabupaten_kota ADD CONSTRAINT PK_r_kabupaten_kota PRIMARY KEY (id_kabupaten_kota);


CREATE TABLE r_kecamatan (
 id_kecamatan BIGINT NOT NULL,
 kecamatan VARCHAR(255),
 id_kabupaten_kota BIGINT(20) NOT NULL
);

ALTER TABLE r_kecamatan ADD CONSTRAINT PK_r_kecamatan PRIMARY KEY (id_kecamatan);


CREATE TABLE m_sekolah (
 id_sekolah BIGINT NOT NULL,
 nss VARCHAR(100),
 nps VARCHAR(10),
 nama_strip VARCHAR(32),
 sekolah VARCHAR(100),
 alamat VARCHAR(255),
 kode_pos VARCHAR(5),
 telepon VARCHAR(15),
 fax VARCHAR(20),
 sk_pendirian VARCHAR(15),
 tgl_sk_pendirian DATE,
 no_sk_akreditasi VARCHAR(100),
 tgl_akreditasi DATE,
 email VARCHAR(255),
 website VARCHAR(255),
 luas_halaman DOUBLE,
 luas_tanah DOUBLE,
 luas_bangunan DOUBLE,
 luas_olahraga DOUBLE,
 logo VARCHAR(255),
 foto VARCHAR(255),
 id_jenis_akreditasi BIGINT NOT NULL,
 id_kecamatan BIGINT NOT NULL,
 id_status_sekolah BIGINT NOT NULL,
 status_aktif TINYINT DEFAULT 1
);

ALTER TABLE m_sekolah ADD CONSTRAINT PK_m_sekolah PRIMARY KEY (id_sekolah);


CREATE TABLE m_sekolah_misi (
 id BIGINT NOT NULL,
 misi VARCHAR(255),
 urutan TINYINT,
 id_sekolah BIGINT NOT NULL
);

ALTER TABLE m_sekolah_misi ADD CONSTRAINT PK_m_sekolah_misi PRIMARY KEY (id);


CREATE TABLE m_sekolah_visi (
 id BIGINT(20) NOT NULL,
 visi VARCHAR(255),
 urutan TINYINT,
 id_sekolah BIGINT NOT NULL
);

ALTER TABLE m_sekolah_visi ADD CONSTRAINT PK_m_sekolah_visi PRIMARY KEY (id);


CREATE TABLE r_asal_sekolah (
 id_asal_sekolah BIGINT(20) NOT NULL,
 nps VARCHAR(10) NOT NULL,
 nama_sekolah VARCHAR(100),
 alamat VARCHAR(100),
 id_jenjang_sekolah BIGINT NOT NULL,
 id_kecamatan BIGINT NOT NULL
);

ALTER TABLE r_asal_sekolah ADD CONSTRAINT PK_r_asal_sekolah PRIMARY KEY (id_asal_sekolah);


CREATE TABLE m_guru (
 id_guru BIGINT NOT NULL,
 status_sertifikasi VARCHAR(30) NOT NULL,
 nuptk VARCHAR(20),
 kode_guru VARCHAR(20),
 nip VARCHAR(20),
 nama VARCHAR(100),
 jenis_kelamin VARCHAR(10),
 tempat_lahir VARCHAR(100),
 tgl_lahir DATE,
 alamat VARCHAR(255),
 hp VARCHAR(15),
 telepon VARCHAR(15),
 tgl_diangkat DATE,
 gaji DOUBLE,
 foto VARCHAR(255),
 status_aktif TINYINT DEFAULT 1,
 id_golongan_darah BIGINT NOT NULL,
 id_status_pernikahan BIGINT NOT NULL,
 id_agama BIGINT NOT NULL,
 id_status_pegawai BIGINT NOT NULL,
 id_kecamatan BIGINT NOT NULL,
 id_jenjang_pendidikan BIGINT NOT NULL,
 id_pangkat_golongan BIGINT NOT NULL,
 id_sekolah BIGINT NOT NULL
);

ALTER TABLE m_guru ADD CONSTRAINT PK_m_guru PRIMARY KEY (id_guru);


CREATE TABLE m_kurikulum (
 id_kurikulum BIGINT NOT NULL,
 id_sekolah BIGINT NOT NULL,
 nama_kurikulum CHAR(150) NOT NULL,
 keterangan CHAR(255) NOT NULL
);

ALTER TABLE m_kurikulum ADD CONSTRAINT PK_m_kurikulum PRIMARY KEY (id_kurikulum,id_sekolah);


CREATE TABLE m_siswa (
 id_siswa BIGINT NOT NULL,
 telepon CHAR(20),
 nis VARCHAR(20) NOT NULL,
 nisn VARCHAR(20),
 nama VARCHAR(100),
 tempat_lahir VARCHAR(100),
 tanggal_lahir DATE,
 jenis_kelamin VARCHAR(10),
 anak_ke TINYINT,
 jumlah_sdr TINYINT,
 alamat VARCHAR(150),
 kode_pos VARCHAR(5),
 foto VARCHAR(255),
 status_aktif TINYINT DEFAULT 1,
 id_agama BIGINT NOT NULL,
 id_golongan_darah BIGINT NOT NULL,
 id_status_anak BIGINT NOT NULL,
 id_kecamatan BIGINT NOT NULL,
 id_sekolah BIGINT NOT NULL,
 id_asal_sekolah BIGINT(20)
);

ALTER TABLE m_siswa ADD CONSTRAINT PK_m_siswa PRIMARY KEY (id_siswa);


ALTER TABLE sys_user ADD CONSTRAINT FK_sys_user_0 FOREIGN KEY (id_group) REFERENCES sys_group (id_group);


ALTER TABLE r_kabupaten_kota ADD CONSTRAINT FK_r_kabupaten_kota_0 FOREIGN KEY (id_provinsi) REFERENCES r_provinsi (id_provinsi);


ALTER TABLE r_kecamatan ADD CONSTRAINT FK_r_kecamatan_0 FOREIGN KEY (id_kabupaten_kota) REFERENCES r_kabupaten_kota (id_kabupaten_kota);


ALTER TABLE m_sekolah ADD CONSTRAINT FK_m_sekolah_0 FOREIGN KEY (id_jenis_akreditasi) REFERENCES r_jenis_akreditasi (id_jenis_akreditasi);
ALTER TABLE m_sekolah ADD CONSTRAINT FK_m_sekolah_1 FOREIGN KEY (id_kecamatan) REFERENCES r_kecamatan (id_kecamatan);
ALTER TABLE m_sekolah ADD CONSTRAINT FK_m_sekolah_2 FOREIGN KEY (id_status_sekolah) REFERENCES r_status_sekolah (id_status_sekolah);


ALTER TABLE m_sekolah_misi ADD CONSTRAINT FK_m_sekolah_misi_0 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);


ALTER TABLE m_sekolah_visi ADD CONSTRAINT FK_m_sekolah_visi_0 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);


ALTER TABLE r_asal_sekolah ADD CONSTRAINT FK_r_asal_sekolah_0 FOREIGN KEY (id_jenjang_sekolah) REFERENCES r_jenjang_sekolah (id_jenjang_sekolah);
ALTER TABLE r_asal_sekolah ADD CONSTRAINT FK_r_asal_sekolah_1 FOREIGN KEY (id_kecamatan) REFERENCES r_kecamatan (id_kecamatan);


ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_0 FOREIGN KEY (id_golongan_darah) REFERENCES r_golongan_darah (id_golongan_darah);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_1 FOREIGN KEY (id_status_pernikahan) REFERENCES r_status_pernikahan (id_status_pernikahan);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_2 FOREIGN KEY (id_agama) REFERENCES r_agama (id_agama);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_3 FOREIGN KEY (id_status_pegawai) REFERENCES r_status_pegawai (id_status_pegawai);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_4 FOREIGN KEY (id_kecamatan) REFERENCES r_kecamatan (id_kecamatan);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_5 FOREIGN KEY (id_jenjang_pendidikan) REFERENCES r_jenjang_pendidikan (id_jenjang_pendidikan);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_6 FOREIGN KEY (id_pangkat_golongan) REFERENCES r_pangkat_golongan (id_pangkat_golongan);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_7 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);


ALTER TABLE m_kurikulum ADD CONSTRAINT FK_m_kurikulum_0 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);


ALTER TABLE m_siswa ADD CONSTRAINT FK_m_siswa_0 FOREIGN KEY (id_agama) REFERENCES r_agama (id_agama);
ALTER TABLE m_siswa ADD CONSTRAINT FK_m_siswa_1 FOREIGN KEY (id_golongan_darah) REFERENCES r_golongan_darah (id_golongan_darah);
ALTER TABLE m_siswa ADD CONSTRAINT FK_m_siswa_2 FOREIGN KEY (id_status_anak) REFERENCES r_status_anak (id_status_anak);
ALTER TABLE m_siswa ADD CONSTRAINT FK_m_siswa_3 FOREIGN KEY (id_kecamatan) REFERENCES r_kecamatan (id_kecamatan);
ALTER TABLE m_siswa ADD CONSTRAINT FK_m_siswa_4 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);
ALTER TABLE m_siswa ADD CONSTRAINT FK_m_siswa_5 FOREIGN KEY (id_asal_sekolah) REFERENCES r_asal_sekolah (id_asal_sekolah);


