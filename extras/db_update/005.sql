INSERT INTO `m_sekolah` (`id_sekolah`, `nss`, `nps`, `nama_strip`, `sekolah`, `alamat`, `kode_pos`, `telepon`, `fax`, `sk_pendirian`, `tgl_sk_pendirian`, `no_sk_akreditasi`, `tgl_akreditasi`, `email`, `website`, `luas_halaman`, `luas_tanah`, `luas_bangunan`, `luas_olahraga`, `logo`, `foto`, `id_jenis_akreditasi`, `id_kecamatan`, `id_status_sekolah`, `status_aktif`) VALUES
(1, '0011', '0011', NULL, 'SMAN 1 Nusantara', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 1);

INSERT INTO `r_jenis_akreditasi` (`id_jenis_akreditasi`, `jenis_akreditasi`) VALUES
(1, 'A'),
(2, 'B');

INSERT INTO `r_kabupaten_kota` (`id_kabupaten_kota`, `kabupaten_kota`, `id_provinsi`) VALUES
(1, 'Kota Bandung', 1);

INSERT INTO `r_kecamatan` (`id_kecamatan`, `kecamatan`, `id_kabupaten_kota`) VALUES
(1, 'Isola', 1);

INSERT INTO `r_provinsi` (`id_provinsi`, `provinsi`) VALUES
(1, 'Jawa Barat');

INSERT INTO `r_status_sekolah` (`id_status_sekolah`, `status_sekolah`) VALUES
(1, 'Negeri'),
(2, 'Swasta');
