CREATE TABLE IF NOT EXISTS r_agama (
 id_agama BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 agama VARCHAR(20)
);

CREATE TABLE IF NOT EXISTS r_golongan_darah (
 id_golongan_darah BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 golongan_darah VARCHAR(2)
);

CREATE TABLE IF NOT EXISTS r_hobi (
 id_hobi BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 hobi VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS r_jenis_akreditasi (
 id_jenis_akreditasi BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 jenis_akreditasi VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS r_jenjang_pendidikan (
 id_jenjang_pendidikan BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 jenjang_pendidikan VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS r_jenjang_sekolah (
 id_jenjang_sekolah BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 jenjang_sekolah VARCHAR(10) NOT NULL
);

CREATE TABLE IF NOT EXISTS ar_kualifikasi_guru (
 id_kualifikasi_guru BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 kualifikasi_guru VARCHAR(150),
 deskripsi VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS r_pangkat_golongan (
 id_pangkat_golongan BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 pangkat VARCHAR(50),
 golongan VARCHAR(15)
);

CREATE TABLE IF NOT EXISTS r_pihak_komunikasi (
 id_pihak_komunikasi BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 pihak_komunikasi VARCHAR(200)
);

CREATE TABLE IF NOT EXISTS r_provinsi (
 id_provinsi BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 provinsi VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS r_range_penghasilan (
 id_range_penghasilan BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 range_penghasilan VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS r_sapras_kondisi (
 id_sapras_kondisi BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 kondisi VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS r_status_anak (
 id_status_anak BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 status_anak VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS r_status_pegawai (
 id_status_pegawai BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 status VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS r_status_pernikahan (
 id_status_pernikahan BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 status_pernikahan VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS r_status_sekolah (
 id_status_sekolah BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 status_sekolah VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS r_tingkat_wilayah (
 id_tingkat_wilayah BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 tingkat_wilayah VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS sys_code_numbering (
 id_code_numbering BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 table_name VARCHAR(100),
 length INT(11),
 current_state VARCHAR(100),
 code_numbering VARCHAR(255)
);

CREATE TABLE IF NOT EXISTS sys_group (
 id_group INT(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
 groupname VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS sys_information (
 id_information INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 information_name VARCHAR(100) NOT NULL,
 information_label VARCHAR(100) NOT NULL,
 information_type VARCHAR(100) NOT NULL,
 information_data VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS sys_user (
 id_user BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 username VARCHAR(100),
 password VARCHAR(255),
 default_password VARCHAR(100) NOT NULL,
 email VARCHAR(150),
 date_created DATETIME,
 id_personal BIGINT NOT NULL,
 is_active BIT(1),
 id_group INT(11) NOT NULL
);

CREATE TABLE IF NOT EXISTS t_prestasi (
 id_prestasi BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 prestasi VARCHAR(255),
 deskripsi TEXT,
 tanggal DATE,
 foto VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS r_pekerjaan (
 id_pekerjaan BIGINT NOT NULL,
 pekerjaan VARCHAR(100)
);

CREATE TABLE IF NOT EXISTS r_kabupaten_kota (
 id_kabupaten_kota BIGINT(20) NOT NULL PRIMARY KEY AUTO_INCREMENT,
 kabupaten_kota VARCHAR(255),
 id_provinsi BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS r_kecamatan (
 id_kecamatan BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 kecamatan VARCHAR(255),
 id_kabupaten_kota BIGINT(20) NOT NULL
);

CREATE TABLE IF NOT EXISTS m_sekolah (
 id_sekolah BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 nss VARCHAR(100),
 nps VARCHAR(10),
 nama_strip VARCHAR(32),
 sekolah VARCHAR(100),
 alamat VARCHAR(255),
 kode_pos VARCHAR(5),
 telepon VARCHAR(15),
 fax VARCHAR(20),
 sk_pendirian VARCHAR(15),
 tgl_sk_pendirian DATE,
 no_sk_akreditasi VARCHAR(100),
 tgl_akreditasi DATE,
 email VARCHAR(255),
 website VARCHAR(255),
 luas_halaman DOUBLE,
 luas_tanah DOUBLE,
 luas_bangunan DOUBLE,
 luas_olahraga DOUBLE,
 logo VARCHAR(255),
 foto VARCHAR(255),
 status_aktif TINYINT DEFAULT 1,
 id_jenis_akreditasi BIGINT NOT NULL,
 id_kecamatan BIGINT NOT NULL,
 id_status_sekolah BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_sekolah_misi (
 id_sekolah_misi BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 misi VARCHAR(255),
 urutan TINYINT,
 id_sekolah BIGINT NOT NULL
);


CREATE TABLE IF NOT EXISTS m_sekolah_visi (
 id_sekolah_visi BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 visi VARCHAR(255),
 urutan TINYINT,
 id_sekolah BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_tahun_ajaran (
 id_tahun_ajaran BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 tahun_awal INT NOT NULL,
 tahun_akhir INT NOT NULL,
 keterangan VARCHAR(255),
 id_sekolah BIGINT NOT NULL
);


CREATE TABLE IF NOT EXISTS m_term (
 id_term BIGINT NOT NULL  PRIMARY KEY AUTO_INCREMENT,
 term VARCHAR(30),
 id_tahun_ajaran BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_tingkat_kelas (
 id_tingkat_kelas BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 tingkat_kelas VARCHAR(20),
 id_sekolah BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS r_asal_sekolah (
 id_asal_sekolah BIGINT(20) NOT NULL PRIMARY KEY AUTO_INCREMENT,
 nps VARCHAR(10) NOT NULL,
 nama_sekolah VARCHAR(100),
 alamat VARCHAR(100),
 id_jenjang_sekolah BIGINT NOT NULL,
 id_kecamatan BIGINT NOT NULL
);


CREATE TABLE IF NOT EXISTS t_kalender_akademik (
 id_kalender_akademik BIGINT(20) NOT NULL PRIMARY KEY AUTO_INCREMENT,
 kegiatan VARCHAR(255),
 tanggal_mulai DATE,
 tanggal_selesai DATE,
 keterangan VARCHAR(255),
 id_term BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS t_komunikasi (
 id_komunikasi BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 kegiatan VARCHAR(255),
 tanggal DATE,
 hasil TEXT,
 id_sekolah BIGINT NOT NULL,
 id_pihak_komunikasi BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_dokumen_kurikulum (
 iddokumen_kurikulum BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 nama_dokumen VARCHAR(255),
 lokasi_file VARCHAR(255),
 id_sekolah BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_guru (
 id_guru BIGINT PRIMARY KEY AUTO_INCREMENT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 status_sertifikasi VARCHAR(30) NOT NULL,
 nuptk VARCHAR(20),
 kode_guru VARCHAR(20),
 nip VARCHAR(20),
 nama VARCHAR(100),
 jenis_kelamin VARCHAR(10),
 tempat_lahir VARCHAR(100),
 tgl_lahir DATE,
 alamat VARCHAR(255),
 hp VARCHAR(15),
 telepon VARCHAR(15),
 tgl_diangkat DATE,
 gaji DOUBLE,
 foto VARCHAR(255),
 status_aktif TINYINT DEFAULT 1,
 id_golongan_darah BIGINT NOT NULL,
 id_status_pernikahan BIGINT NOT NULL,
 id_agama BIGINT NOT NULL,
 id_status_pegawai BIGINT NOT NULL,
 id_kecamatan BIGINT NOT NULL,
 id_jenjang_pendidikan BIGINT NOT NULL,
 id_pangkat_golongan BIGINT NOT NULL,
 id_sekolah BIGINT NOT NULL,
 id_kualifikasi_guru BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_kurikulum (
 id_kurikulum BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 nama_kurikulum CHAR(150) NOT NULL,
 keterangan CHAR(255) NOT NULL,
 id_sekolah BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_program (
 id_program BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 progam VARCHAR(100),
 id_sekolah BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_sapras_ruang_kelas (
 id_sapras_ruang_kelas BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 id_sapras_kondisi BIGINT NOT NULL,
 id_sekolah BIGINT NOT NULL,
 kode VARCHAR(20),
 nama VARCHAR(150),
 penanggung_jawab VARCHAR(100),
 daya_tampung DOUBLE
);

CREATE TABLE IF NOT EXISTS m_siswa (
 id_siswa BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 telepon CHAR(20),
 nis VARCHAR(20) NOT NULL,
 nisn VARCHAR(20),
 nama VARCHAR(100),
 tempat_lahir VARCHAR(100),
 tanggal_lahir DATE,
 jenis_kelamin VARCHAR(10),
 anak_ke TINYINT,
 jumlah_sdr TINYINT,
 alamat VARCHAR(150),
 kode_pos VARCHAR(5),
 foto VARCHAR(255),
 status_aktif TINYINT DEFAULT 1,
 id_agama BIGINT NOT NULL,
 id_golongan_darah BIGINT NOT NULL,
 id_status_anak BIGINT NOT NULL,
 id_kecamatan BIGINT NOT NULL,
 id_sekolah BIGINT NOT NULL,
 id_asal_sekolah BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS t_siswa_hobi (
 id_siswa_hobi BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 id_hobi BIGINT NOT NULL,
 id_siswa BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_kelompok_pelajaran (
 id_kelompok_pelajaran BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 kelompok_pelajaran VARCHAR(100),
 id_kurikulum BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_komponen_nilai (
 id_komponen_nilai BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 komponen_nilai VARCHAR(100),
 nilai_min DOUBLE,
 max DOUBLE,
 id_kurikulum BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_konversi_nilai (
 id_konversi_nilai BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 nilai_huruf VARCHAR(5),
 nilai_min DOUBLE,
 nilai_max DOUBLE,
 nilai_angka DOUBLE,
 id_komponen_nilai BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_ortu (
 id_ortu BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 nama_ayah VARCHAR(100),
 nama_ibu VARCHAR(10),
 nama_wali VARCHAR(100),
 alamat_ayah VARCHAR(200),
 alamat_ibu VARCHAR(200),
 alamat_wali VARCHAR(200),
 kecamatan_id BIGINT,
 range_penghasilan_id BIGINT,
 id_jenjang_pendidikan_ayah BIGINT NOT NULL,
 id_jenjang_pendidikan_ayah_ibu BIGINT,
 id_jenjang_pendidikan_wali BIGINT,
 id_pekerjaan_ayah BIGINT NOT NULL,
 id_pekerjaan_ibu BIGINT,
 id_pekerjaan_wali BIGINT,
 id_range_penghasilan_ayah BIGINT NOT NULL,
 id_range_penghasilan_ibu BIGINT,
 id_range_penghasilan_wali BIGINT,
 id_kecamatan_ayah BIGINT NOT NULL,
 id_kecamatan_ibu BIGINT,
 id_kecamatan_wali BIGINT,
 id_siswa BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_pelajaran (
 id_pelajaran BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 kode_pelajaran VARCHAR(100),
 pelajaran VARCHAR(100),
 id_kelompok_pelajaran BIGINT NOT NULL,
 id_kualifikasi_guru BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS m_mata_pelajaran (
 id_mata_pelajaran BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 kode_mata_pelajaran VARCHAR(20),
 jumlah_jam DOUBLE,
 id_program BIGINT NOT NULL,
 id_tingkat_kelas BIGINT NOT NULL,
 id_pelajaran BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS t_silabus (
 id_silabus BIGINT NOT NULL PRIMARY KEY AUTO_INCREMENT,
 nama_silabus VARCHAR(150),
 lokasi_file VARCHAR(255),
 id_term BIGINT NOT NULL,
 id_mata_pelajaran BIGINT NOT NULL
);

ALTER TABLE sys_user ADD CONSTRAINT FK_sys_user_0 FOREIGN KEY (id_group) REFERENCES sys_group (id_group);

ALTER TABLE r_kabupaten_kota ADD CONSTRAINT FK_r_kabupaten_kota_0 FOREIGN KEY (id_provinsi) REFERENCES r_provinsi (id_provinsi);


ALTER TABLE r_kecamatan ADD CONSTRAINT FK_r_kecamatan_0 FOREIGN KEY (id_kabupaten_kota) REFERENCES r_kabupaten_kota (id_kabupaten_kota);


ALTER TABLE m_sekolah ADD CONSTRAINT FK_m_sekolah_0 FOREIGN KEY (id_jenis_akreditasi) REFERENCES r_jenis_akreditasi (id_jenis_akreditasi);
ALTER TABLE m_sekolah ADD CONSTRAINT FK_m_sekolah_1 FOREIGN KEY (id_kecamatan) REFERENCES r_kecamatan (id_kecamatan);
ALTER TABLE m_sekolah ADD CONSTRAINT FK_m_sekolah_2 FOREIGN KEY (id_status_sekolah) REFERENCES r_status_sekolah (id_status_sekolah);


ALTER TABLE m_sekolah_misi ADD CONSTRAINT FK_m_sekolah_misi_0 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);


ALTER TABLE m_sekolah_visi ADD CONSTRAINT FK_m_sekolah_visi_0 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);


ALTER TABLE m_tahun_ajaran ADD CONSTRAINT FK_m_tahun_ajaran_0 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);


ALTER TABLE m_term ADD CONSTRAINT FK_m_term_0 FOREIGN KEY (id_tahun_ajaran) REFERENCES m_tahun_ajaran (id_tahun_ajaran);


ALTER TABLE m_tingkat_kelas ADD CONSTRAINT FK_m_tingkat_kelas_0 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);


ALTER TABLE r_asal_sekolah ADD CONSTRAINT FK_r_asal_sekolah_0 FOREIGN KEY (id_jenjang_sekolah) REFERENCES r_jenjang_sekolah (id_jenjang_sekolah);
ALTER TABLE r_asal_sekolah ADD CONSTRAINT FK_r_asal_sekolah_1 FOREIGN KEY (id_kecamatan) REFERENCES r_kecamatan (id_kecamatan);


ALTER TABLE t_kalender_akademik ADD CONSTRAINT FK_t_kalender_akademik_0 FOREIGN KEY (id_term) REFERENCES m_term (id_term);


ALTER TABLE t_komunikasi ADD CONSTRAINT FK_t_komunikasi_0 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);
ALTER TABLE t_komunikasi ADD CONSTRAINT FK_t_komunikasi_1 FOREIGN KEY (id_pihak_komunikasi) REFERENCES r_pihak_komunikasi (id_pihak_komunikasi);


ALTER TABLE m_dokumen_kurikulum ADD CONSTRAINT FK_m_dokumen_kurikulum_0 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);


ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_0 FOREIGN KEY (id_golongan_darah) REFERENCES r_golongan_darah (id_golongan_darah);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_1 FOREIGN KEY (id_status_pernikahan) REFERENCES r_status_pernikahan (id_status_pernikahan);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_2 FOREIGN KEY (id_agama) REFERENCES r_agama (id_agama);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_3 FOREIGN KEY (id_status_pegawai) REFERENCES r_status_pegawai (id_status_pegawai);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_4 FOREIGN KEY (id_kecamatan) REFERENCES r_kecamatan (id_kecamatan);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_5 FOREIGN KEY (id_jenjang_pendidikan) REFERENCES r_jenjang_pendidikan (id_jenjang_pendidikan);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_6 FOREIGN KEY (id_pangkat_golongan) REFERENCES r_pangkat_golongan (id_pangkat_golongan);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_7 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);
ALTER TABLE m_guru ADD CONSTRAINT FK_m_guru_8 FOREIGN KEY (id_kualifikasi_guru) REFERENCES ar_kualifikasi_guru (id_kualifikasi_guru);


ALTER TABLE m_kurikulum ADD CONSTRAINT FK_m_kurikulum_0 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);


ALTER TABLE m_program ADD CONSTRAINT FK_m_program_0 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);


ALTER TABLE m_sapras_ruang_kelas ADD CONSTRAINT FK_m_sapras_ruang_kelas_0 FOREIGN KEY (id_sapras_kondisi) REFERENCES r_sapras_kondisi (id_sapras_kondisi);
ALTER TABLE m_sapras_ruang_kelas ADD CONSTRAINT FK_m_sapras_ruang_kelas_1 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);


ALTER TABLE m_siswa ADD CONSTRAINT FK_m_siswa_0 FOREIGN KEY (id_agama) REFERENCES r_agama (id_agama);
ALTER TABLE m_siswa ADD CONSTRAINT FK_m_siswa_1 FOREIGN KEY (id_golongan_darah) REFERENCES r_golongan_darah (id_golongan_darah);
ALTER TABLE m_siswa ADD CONSTRAINT FK_m_siswa_2 FOREIGN KEY (id_status_anak) REFERENCES r_status_anak (id_status_anak);
ALTER TABLE m_siswa ADD CONSTRAINT FK_m_siswa_3 FOREIGN KEY (id_kecamatan) REFERENCES r_kecamatan (id_kecamatan);
ALTER TABLE m_siswa ADD CONSTRAINT FK_m_siswa_4 FOREIGN KEY (id_sekolah) REFERENCES m_sekolah (id_sekolah);
ALTER TABLE m_siswa ADD CONSTRAINT FK_m_siswa_5 FOREIGN KEY (id_asal_sekolah) REFERENCES r_asal_sekolah (id_asal_sekolah);


ALTER TABLE t_siswa_hobi ADD CONSTRAINT FK_t_siswa_hobi_0 FOREIGN KEY (id_hobi) REFERENCES r_hobi (id_hobi);
ALTER TABLE t_siswa_hobi ADD CONSTRAINT FK_t_siswa_hobi_1 FOREIGN KEY (id_siswa) REFERENCES m_siswa (id_siswa);


ALTER TABLE m_kelompok_pelajaran ADD CONSTRAINT FK_m_kelompok_pelajaran_0 FOREIGN KEY (id_kurikulum) REFERENCES m_kurikulum (id_kurikulum);


ALTER TABLE m_komponen_nilai ADD CONSTRAINT FK_m_komponen_nilai_0 FOREIGN KEY (id_kurikulum) REFERENCES m_kurikulum (id_kurikulum);


ALTER TABLE m_konversi_nilai ADD CONSTRAINT FK_m_konversi_nilai_0 FOREIGN KEY (id_komponen_nilai) REFERENCES m_komponen_nilai (id_komponen_nilai);


ALTER TABLE m_ortu ADD CONSTRAINT FK_m_ortu_0 FOREIGN KEY (id_jenjang_pendidikan_ayah) REFERENCES r_jenjang_pendidikan (id_jenjang_pendidikan);
ALTER TABLE m_ortu ADD CONSTRAINT FK_m_ortu_1 FOREIGN KEY (id_pekerjaan_ayah) REFERENCES r_pekerjaan (id_pekerjaan);
ALTER TABLE m_ortu ADD CONSTRAINT FK_m_ortu_2 FOREIGN KEY (id_range_penghasilan_ayah) REFERENCES r_range_penghasilan (id_range_penghasilan);
ALTER TABLE m_ortu ADD CONSTRAINT FK_m_ortu_3 FOREIGN KEY (id_kecamatan_ayah) REFERENCES r_kecamatan (id_kecamatan);
ALTER TABLE m_ortu ADD CONSTRAINT FK_m_ortu_4 FOREIGN KEY (id_siswa) REFERENCES m_siswa (id_siswa);


ALTER TABLE m_pelajaran ADD CONSTRAINT FK_m_pelajaran_0 FOREIGN KEY (id_kelompok_pelajaran) REFERENCES m_kelompok_pelajaran (id_kelompok_pelajaran);
ALTER TABLE m_pelajaran ADD CONSTRAINT FK_m_pelajaran_1 FOREIGN KEY (id_kualifikasi_guru) REFERENCES ar_kualifikasi_guru (id_kualifikasi_guru);


ALTER TABLE m_mata_pelajaran ADD CONSTRAINT FK_m_mata_pelajaran_0 FOREIGN KEY (id_program) REFERENCES m_program (id_program);
ALTER TABLE m_mata_pelajaran ADD CONSTRAINT FK_m_mata_pelajaran_1 FOREIGN KEY (id_tingkat_kelas) REFERENCES m_tingkat_kelas (id_tingkat_kelas);
ALTER TABLE m_mata_pelajaran ADD CONSTRAINT FK_m_mata_pelajaran_2 FOREIGN KEY (id_pelajaran) REFERENCES m_pelajaran (id_pelajaran);


ALTER TABLE t_silabus ADD CONSTRAINT FK_t_silabus_0 FOREIGN KEY (id_term) REFERENCES m_term (id_term);
ALTER TABLE t_silabus ADD CONSTRAINT FK_t_silabus_1 FOREIGN KEY (id_mata_pelajaran) REFERENCES m_mata_pelajaran (id_mata_pelajaran);


