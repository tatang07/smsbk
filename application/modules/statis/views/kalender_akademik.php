<h1 class="grid_12">Pengelolaan Sekolah</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Kalender Akademik Tahun Ajaran 2014/2015</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-print"></i>Cetak</a> 
			  	<a href=""><i class="icon-plus"></i>Tambah</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter"> 
            	<form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">             
					<table>
						<tr>
							<th width=80px align='left'><span class="text">Tahun Ajaran</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>2014/2015</option>
									<option value=''>2013/2014</option>
									<option value=''>2012/2013</option>
									<option value=''>2011/2012</option>
									<option value=''>2010/2011</option>									
									<option value=''>2009/2010</option>									
								</select>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kegiatan</th>
								<th>Tanggal Mulai</th>
								<th>Tanggal Selesai</th>
								<th>Penanggung Jawab</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>Masuk Sekolah</td>
								<td width='' class='center'>13 Juli 2014</td>					
								<td width='' class='center'>20 Juli 2015</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>Libur Awal Puasa</td>
								<td width='' class='center'>20 Agustus 2014</td>					
								<td width='' class='center'>22 Agustus 2014</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>Libur Idul Fitri 1430 H</td>
								<td width='' class='center'>14 September 2014</td>					
								<td width='' class='center'>29 September 2014</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>Perkiraan Ulangan Tengah Semester</td>
								<td width='' class='center'>5 Oktober 2014</td>					
								<td width='' class='center'>10 Oktober 2014</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='center'>Perkiraan Ulangan Akhir Semester</td>
								<td width='' class='center'>14 Desember 2014</td>					
								<td width='' class='center'>19 Desember 2014</td>													
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>6</td>
								<td width='' class='center'>Pembagian Raport</td>
								<td width='' class='center'>26 Desember 2014</td>					
								<td width='' class='center'>0 0</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>7</td>
								<td width='' class='center'>Libur Semester</td>
								<td width='' class='center'>27 Desember 2014</td>					
								<td width='' class='center'>10 Januari 2015</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>8</td>
								<td width='' class='center'>Masuk Sekolah</td>
								<td width='' class='center'>11 Januari 2015</td>					
								<td width='' class='center'>0 0</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>9</td>
								<td width='' class='center'>Perkiraan Ulangan Tengah Semester</td>
								<td width='' class='center'>15 Maret 2015</td>					
								<td width='' class='center'>20 Maret 2015</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>10</td>
								<td width='' class='center'>Perkiraan Jeda Tengah Semester</td>
								<td width='' class='center'>22 Maret 2015</td>					
								<td width='' class='center'>27 Maret 2015</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>11</td>
								<td width='' class='center'>Perkiraan US SMA</td>
								<td width='' class='center'>17 Mei 2015</td>					
								<td width='' class='center'>22 Mei 2015</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>12</td>
								<td width='' class='center'>Libur Semester</td>
								<td width='' class='center'>11 Juni 2015</td>					
								<td width='' class='center'>27 Juni 2015</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>13</td>
								<td width='' class='center'>Perkiraan Ulangan Kenaikan Kelas</td>
								<td width='' class='center'>14 Juni 2015</td>					
								<td width='' class='center'>0 0</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>14</td>
								<td width='' class='center'>Pembagian Raport</td>
								<td width='' class='center'>26 Juni 2015</td>					
								<td width='' class='center'>0 0</td>					
								<td width='' class='center'>Kepala Sekolah</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>							
						</tbody>
					</table>
					<div class="footer">
					
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
