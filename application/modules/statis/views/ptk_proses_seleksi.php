<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       	<div class="box">
				
					<div class="header">
						<h2>Proses Seleksi Pendidik dan Tenaga Kependidikan</h2>
					</div>
					
					<div class="accordion toggle">
					
						<h3><a href="#">Seleksi Persyaratan Administrasi</a></h3>
						<div>
							<ul>							
								<li>Mengenai pribadi, misalnya nama lengkap dan tempat tinggal</li>
								<li>Keterangan perorangan: umur, status perkawinan, tanggungan, jumlah saudara, tempat, dan alamat orang tua</li>
								<li>Keterangan fisik, tinggi badan, berat badan, kesehatan, dan ciri khusus lainnya</li>
								<li>Pendidikan: sekolah dasar, sekolah lanjutan tingkat pertama,sekolah lanjutan tingkat atas, diploma, sarjana, pascasarjana,dan sebagainya</li>
								<li>Pengalaman kerja: di mana pernah bertugas, berapa lama,bagian apa, mengapa berhenti, dan sebagainya</li>
								<li>Keterangan lain: hobi, prestasi lain, dan sebagainya</li>
						
							</ul>
						</div>
						
						<h3><a href="#">Lampiran-Lampiran</a></h3>
						<div>
							<ul>							
								<li>Fotokopi ijazah serta sertifikat pelatihan/kursus yang telah dimiliki</li>
   							 	<li>Daftar riwayat hidup</li>
 							    <li>Surat keterangan berkelakuan baik dari kepolisian</li>
 							    <li>Surat keterangan sehat dari dokter</li>
 							    <li>Kartu tanda bukti mencatatkan diri dari departemmen/dinas tenaga pendidik dan kependidikan setempat</li>
 							    <li>Pas foto sesuai dengan permintaan</li>
 							    <li>Fotokopi kartu tanda penduduk</li>
 							    <li>Surat keterangan pengalaman kerja</li>

							</ul>
						</div>
						
						<h3><a href="#">Seleksi Pengetahuan Umum</a></h3>
						<div>
							<ul>							
								<li>Pengetahuan umum yang berhubungan dengan ruang lingkup sekolah, menurut pandangan praktis maupun teoritis</li>
    							<li>Pengetahuan umum yang berhubungan dengan system ketatanegaraan Indonesia termasuk kebijakan-kebijakan pemerintah mengenai sekolah yang relevan dengan usahanya</li>
    							
							</ul>
						</div>
						
						<h3><a href="#">Seleksi Psikologi</a></h3>
						<div>
							<ul>
							
								<li>Tes Hasil Kerja (Achievement Test) Tes ini dimaksudkan untuk mengukur hasil kerja para pelamar. Tes demikian menunjukkan apa yang dapat dikerjakan sekarang. Kadang-kadang tes ini juga disebut proficiency test , yaitu tes kepandaian atau tes kecakapan/keahlian. Tujuan tes ini adalah untuk mengetahui tingkat kemampuan bertugas yang dimiliki para pelamar, serta prediksi terhadap kecakapan mengerjakan suatu jenis tenaga pendidik dan kependidikanan setelah diberikan induksi,orientasi, pendidikan, dan pelatihan</li>
    							<li>Tes Bakat/Pembawaan (Aptitude Test)Tes bakat/pembawaan adalah tes untuk mengukur bakat atau kemampuan yang mungkin telah dikembangkan atau masih terpendam, dan tidak digunakan. Tujuan penyelenggaraan tes ini adalah untuk memprediksi kecakapan belajar para pelamar di kemudian hari, bukan kecakapannya untuk mengerjakan tugas tenaga pendidik dan kependidikanan yang sekarang</li>
    							<li>Tes Kecerdasan (Intellegence Test) Kualitas kecerdasan seseorang sering dinyatakan dengan intellegence quotient  (IQ). Tes kecerdasan adalah tes yang digunakan baik dalam seleksi maupun untuk peningkatan (upgrading). Pengukuran kecerdasan sering dilakukan pertama-tama dalam program pengujian, karena pengukuran ini memberikan suatu bentuk pengukuran yang pokok atau yang utama. Tes kecerdasan adalah tes untuk mengukur kemampuan berpikir</li>
    							<li>Tes Minat (Interest Test)Tes minat adalah tes untuk mengetahui luasnya minat para pelamar. Tes minat merupakan segala jenis tes psikologi yang bermaksud untuk menentukan aktivitas mana yang paling menarik perhatian seorang calon tenaga pendidik dan kependidikan</li>
    							<li>es Kepribadian (Personality Test) Kepribadian menunjukkan individu secara keseluruhan, cara berpikir, merasakan, bertindak, cara bergaul dengan oranglain, cara penyesuaian diri dengan lingkungannya. Semuanya merupakan sifat penting yang membedakan masing-masing individu dengan orang lain. Tes kepribadian ini adalah suatu tes untuk mengukur atau menilai sifat kepribadian yang dimiliki para pelamar sebelumnya</li>
							</ul>
						</div>						
					</div><!-- End of .content -->
					
				</div><!-- End of .box -->		
        </div>
 </div>
