<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekap Penerima Keringanan Pembayaran Tahun Ajaran 2009/2010</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-print"></i>Cetak File</a> 
			  	<a href=""><i class="icon-plus"></i>Tambah Penerima</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=100px align='left'><span class="text">Tahun Ajaran:</span></th>
							<td width=100px align='left'>
								<select>
									<option value=''>2009/2010</option>
									<option value=''>2010/2011</option>
									<option value=''>2011/2012</option>
									<option value=''>2012/2013</option>
									<option value=''>2013/2014</option>
									<option value=''>2014/2015</option>
								</select>
							</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Bulan</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Jenis</th>
								<th>Jumlah</th>
								<th>Ket</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width=''>Januari</td>
								<td width=''>080910158</td>
								<td width=''>Aan Nurjanah</td>
								<td width=''>D. Sumbangan Pendidikan Bulanan</td>
								<td width=''>Rp. 1.000.000,-</td>
								<td width=''>Lunas</td>
								
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 1</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
