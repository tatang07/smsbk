<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Kualifikasi Pendidikan</h2>
       </div>
	   <div class="box">
	   <div class="content" style="height: 250px;">
			<table class=chart data-type=pie data-donut=0>
				<thead>
					<tr>
						<th></th>
						<th>TK</th>
						<th>SMA/S</th>
						<th>S1</th>
						<th>S2</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>TK</th>
						<td>1</td>
					</tr>
					<tr>
						<th>SMA/S</th>
						<td>1</td>
					</tr>
					<tr>
						<th>S1</th>
						<td>19</td>
					</tr>
					<tr>
						<th>S2</th>
						<td>2</td>
					</tr>
				</tbody>	
			</table>
		</div><!-- End of .content -->
		</div><!-- End of .box -->
					
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jenjang Pendidikan Terakhir</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30px' class='center'>1</td>
								<td width='500px'>TK</td>
								<td width='50px' class='center'>1</td>
							</tr>
							<tr>
								<td width='30px' class='center'>2</td>
								<td width='500px'>SMA/S</td>
								<td width='50px' class='center'>1</td>
							</tr>
							<tr>
								<td width='30px' class='center'>3</td>
								<td width='500px'>S1</td>
								<td width='50px' class='center'>19</td>
							</tr>
							<tr>
								<td width='30px' class='center'>4</td>
								<td width='500px'>S2</td>
								<td width='50px' class='center'>2</td>
							</tr>
						</tbody>
					</table>
					
				</div>
            </div>
        </div>        
    </div>
 </div>
