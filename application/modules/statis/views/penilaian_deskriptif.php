<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Penilaian Deskriptif</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
            <div class="dataTables_filter">
                
					<table>
						<tr>					
							<th width=1px align='left'><span class="text">Kelas</span></th>
										<td width=100px align='left'>
											<select>
												<option value=''>X 1</option>
												<option value=''>X 2</option>
												<option value=''>X 3</option>
												<option value=''>X 4</option>
												<option value=''>X 5</option>
												<option value=''>XI IPA 1</option>
												<option value=''>XI IPA 2</option>
												<option value=''>XI IPA 3</option>
												<option value=''>XI IPA 4</option>
												<option value=''>XI IPS 1</option>
												<option value=''>XI IPS 2</option>
												<option value=''>XI IPS 3</option>
												<option value=''>XI BAHASA 1</option>
												<option value=''>XI BAHASA 2</option>
												<option value='' selected>XII IPA 1</option>
												<option value=''>XII IPA 2</option>
												<option value=''>XII IPA 3</option>
												<option value=''>XII IPA 4</option>
												<option value=''>XII IPS 1</option>
												<option value=''>XII IPS 2</option>
												<option value=''>XII IPS 3</option>
												<option value=''>XII BAHASA 1</option>
												<option value=''>XII BAHASA 2</option>
											</select>
										</td>
							<td width=10px>&nbsp;</td>
							<th width=30px align='left'><span class="text"> Matpel</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>A003 - Matematika - XI IPA</option>									
									<option value=''>A001 - Matematika - X</option>									
									<option value=''>S003 - Matematika - XI IPS</option>									
								</select>
							</td>
							<td width=10px>&nbsp;</td>
							<th width=40px align='left'><span class="text"> Komponen</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>Pengetahuan</option>									
									<option value=''>Sikap</option>									
									<option value=''>Keterampilan</option>									
								</select>
							</td>
							<td width=10px>&nbsp;</td>
							<td><a href="javascript:changePage(1)" original-title="Cari" class="button grey tooltip"><i class="icon-search"></i></a></td>
						</tr>
					</table>                
            </div>            
        </div>
    </div>
</div>
<div class="grid_12">
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">   
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nilai Huruf</th>
								<th>Range Nilai Angka</th>								
								<th>Deskripsi</th>		
								<th>Aksi</th>						
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>								
								<td width='' class='center'>A</td>								
								<td width='' class='center'>3,83 > x <= 4,00</td>								
								<td width='' class='center'>Menguasai Seluruh Kompetensi Dasar</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>								
								<td width='' class='center'>A-</td>								
								<td width='' class='center'>3,50 > x <= 3,83</td>								
								<td width='' class='center'></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>								
								<td width='' class='center'>B+</td>								
								<td width='' class='center'>3,17 > x <= 3,50</td>								
								<td width='' class='center'></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>								
								<td width='' class='center'>B-</td>								
								<td width='' class='center'>2,83 > x <= 3,17</td>								
								<td width='' class='center'></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>5</td>								
								<td width='' class='center'>B</td>								
								<td width='' class='center'>2,83 > x <= 3,17</td>								
								<td width='' class='center'></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>6</td>								
								<td width='' class='center'>B-</td>								
								<td width='' class='center'>2,50 > x <= 2,83</td>								
								<td width='' class='center'></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>7</td>								
								<td width='' class='center'>C+</td>								
								<td width='' class='center'>2,17 > x <= 2,50</td>								
								<td width='' class='center'></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>8</td>								
								<td width='' class='center'>C</td>								
								<td width='' class='center'>1,83 > x <= 2,17</td>								
								<td width='' class='center'></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>9</td>								
								<td width='' class='center'>C-</td>								
								<td width='' class='center'>1,50 > x <= 1,83</td>								
								<td width='' class='center'></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>10</td>								
								<td width='' class='center'>D</td>								
								<td width='' class='center'>1,00 > x <= 1,17</td>								
								<td width='' class='center'></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>							
						</tbody>
					</table>					
				</div>
            </div>
        </div>        
    </div>
 </div>
