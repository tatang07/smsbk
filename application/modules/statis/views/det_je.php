<h1 class="grid_12">Ekstrakulikuler</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2><a href="<?php echo site_url('statis/jenis_ekstrakulikuler'); ?>">Jenis Ekstrakulikuler</a> &bull; Bulutangkis</h2>
       </div>
       <div class="tabletools">
	
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>Nama Ekstrakurikuler: </td>
								<td >Bulutangkis</td>
							</tr>
							<tr>
								<td width='' class='center'>Tujuan</td>
								<td >Memberikan Peluang Terhadap Siswa Mengembangkan Bakat Khususnya pada Bidang Olahraga Bulutangkis</td>
							</tr>
							<tr>
								<td width='' class='center'>Pelatih</td>
								<td >Dr.Toni</td>

							</tr>
							<tr>
								<td width='' class='center'>Proker</td>
								<td >1. Persiapan menghadapi event-event Bulutangkis</td>

							</tr>
							<tr>
								<td width='' class='center'></td>
								<td >2. Persiapan menghadapi event-event Bulutangkis Nasional</td>

							</tr>
						</tbody>
					</table>
					<div class="footer">
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
