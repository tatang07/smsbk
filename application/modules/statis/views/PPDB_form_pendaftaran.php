<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>
<div class="grid_12">
    <div class="box with-table">
    	<div class="header">
            <h2>Formulir Pendaftaran Siswa Baru Tahun Ajaran 2014/2015</h2>
        </div>

        <div class="content">
			<table style="margin:20px;">
				<tbody>
					<tr>
						<td width='150px' >Nama Siswa</td>
						<td ><input type="text" style="height:10px;width:400px;"/></td>
					
					</tr>
					<tr>
						<td width='' >Tempat Lahir</td>
						<td ><input type="text" style="height:10px;width:400px;"/></td>
					</tr>
					<tr>
						<td width='' >Tanggal Lahir</td>
						<td ><input type="text" style="height:10px;width:400px;"/></td>

					</tr>
					<tr>
						<td width='' >Jenis Kelamin</td>
						<td >
							<select>
								<option>Laki-laki</option>
								<option>Perempuan</option>
							</select>
						</td>

					</tr>
					<tr>
						<td width='' >Agama</td>
						<td >
							<select>
								<option>Islam</option>
								<option>Kristen Protestan</option>
								<option>Kristen Katolik</option>
								<option>Hindu</option>
								<option>Budha</option>
								<option>Konghuchu</option>
							</select>
						</td>

					</tr>
					<tr>
						<td width='' >Golongan Darah</td>
						<td >
							<select>
								<option>A</option>
								<option>B</option>
								<option>O</option>
								<option>AB</option>
							</select>
						</td>

					</tr>
					<tr>
						<td width='' >Anak Ke</td>
						<td ><input type="text" style="height:10px;width:400px;"/></td>

					</tr>
					<tr>
						<td width='' >Jumlah Saudara</td>
						<td ><input type="text" style="height:10px;width:400px;"/></td>

					</tr>
					<tr>
						<td width='' >Status Anak</td>
						<td >
							<select>
								<option>Kandung</option>
								<option>Tiri</option>
							</select>
						</td>

					</tr>
					<tr>
						<td width='' >Alamat</td>
						<td >
							<textarea cols="75" rows="3">
							</textarea>
						</td>

					</tr>
					<tr>
						<td width='' >Kecamatan</td>
						<td >
							<select>
								<option>INDRAMAYU</option>
								<option>SUKRA</option>
								<option>LOSARANG</option>
								<option>KANDANGHAUR</option>
								<option>BALONGAN</option>
								<option>ANJATAN</option>
								<option>BONGAS</option>
								<option>JUNTINYUAT</option>
								<option>GABUSWETAN</option>
								<option>SLIYEG</option>
								<option>JATIBARANG</option>
								<option>LELEA</option>
								<option>HAURGEULIS</option>
								<option>CIKEDUNG</option>
								<option>WIDASARI</option>
								<option>KARANGAMPEL</option>
								<option>KROYA</option>
								<option>KRANGKENG</option>
								<option>KERTASEMAYA</option>
								<option>BANGODUA</option>
								<option>LOHBENER</option>
								<option>ARAHAN</option>
								<option>SINDANG</option>
								<option>CANTIGI</option>
								<option>TERISI</option>
								<option>TUKDANA</option>
								<option>SUKAGUMIWANG</option>
								<option>PATROL</option>
								<option>KEDOKAN BUNDER</option>
								<option>INDRAMAYU</option>
								<option>KERTASEMAYA</option>
								<option>LAJER</option>
								<option>LAJER</option>
								<option>LAJER</option>
								<option>LAJER</option>
								<option>Tidak Ada Data</option>
							</select>
						</td>

					</tr>
					<tr>
						<td width='' >Kabupaten Kota</td>
						<td >
							<select>
								<option>ACEH BARAT</option>
								<option>ACEH BARAT DAYA</option>
								<option>ACEH BESAR</option>
								<option>ACEH JAYA</option>
								<option>ACEH SELATAN</option>
								<option>ACEH SINGKIL</option>
								<option>ACEH TAMIANG</option>
								<option>ACEH TENGGARA</option>
								<option>ACEH TIMUR</option>
								<option>ACEH UTARA</option>
								<option>AMBON</option>
								<option>B A T A M</option>
								<option>BALIKPAPAN</option>
								<option>BANDA ACEH</option>
								<option>BANDAR LAMPUNG</option>
								<option>BANGGAI KEPULAUAN</option>
								<option>BANGKA</option>
								<option>BANJAR</option>
								<option>BANJAR BARU</option>
								<option>BANJARMASIN</option>
							</select>
						</td>

					</tr>
					<tr>
						<td width='' >Provinsi</td>
						<td >
							<select>
								<option>BALI</option>
								<option>BANTEN</option>
								<option>BENGKULU</option>
								<option>DI YOGYAKARTA</option>
								<option>DKI - JAKARTA</option>
								<option>GORONTALO</option>
								<option>JAMBI</option>
								<option>JAWA BARAT</option>
								<option>JAWA TENGAH</option>
								<option>JAWA TIMUR</option>
								<option>KALIMANTAN BARAT</option>
								<option>KALIMANTAN SELATAN</option>
								<option>KALIMANTAN TENGAH</option>
								<option>KALIMANTAN TIMUR</option>
								<option>KEP. BANGKA BELITUNG</option>
								<option>LAMPUNG</option>
								<option>MALUKU SELATAN</option>
								<option>MALUKU UTARA</option>
								<option>NAD</option>
								<option>NUSA TENGGARA BARAT</option>
							</select>
						</td>

					</tr>
					<tr>
						<td width='' >Kode Pos</td>
						<td ><input type="text" style="height:10px;width:400px;"/></td>

					</tr>
					<tr>
						<td width='' >No Tlp</td>
						<td ><input type="text" style="height:10px;width:400px;"/></td>

					</tr>
					<tr>
						<td width='' >Asal Sekolah</td>
						<td >
							<select>
								<option>MTs Al-Musyahadah</option>
								<option>MTS HASANUDIN</option>
								<option>MTs Ma&#039;had Al-Zaytun</option>
								<option>MTs NURUL HIKMAH</option>
								<option>MTSN ANJATAN</option>
								<option>MTSN BANGODUA</option>
								<option>MTSN CIKEDUNG</option>
								<option>MTSN JATIBARANG</option>
								<option>MTSN KANDANGHAUR</option>
								<option>MTSN KARANG AMPEL</option>
								<option>MTSN KRANGKENG</option>
								<option>MTSN LOHBENER</option>
								<option>MTSN LOSARANG</option>
								<option>MTSN SLIYEG</option>
								<option>MTSN TEMIYANG</option>
								<option>MTSN WIDASARI</option>
								<option>MTSN WOTBOGOR</option>
								<option>MTSS AL AMIN</option>
								<option>MTSS AL BASYARIYAH</option>
								<option>MTSS AL GHOZALI</option>
							</select>
						</td>

					</tr>
					<tr>
						<td width='' >Foto</td>
						<td ><input type="file" style="height:10px;width:400px;"/></td>

					</tr>
				</tbody>
			</table>
		</div>
    </div>

    <div class="box with-table">
    	<div class="header">
            <h2>Nilai Ujian Nasional</h2>
        </div>

        <div class="content">
			<table style="margin:20px;">
				<tbody>
					<tr>
						<td width='150px' >Matematika</td>
						<td ><input type="text" style="height:10px;width:400px;"/></td>					
					</tr>
					<tr>
						<td>Bahasa Inggris</td>
						<td ><input type="text" style="height:10px;width:400px;"/></td>					
					</tr>
					<tr>
						<td>Bahasa Indonesia</td>
						<td ><input type="text" style="height:10px;width:400px;"/></td>					
					</tr>
					<tr>
						<td>IPA</td>
						<td ><input type="text" style="height:10px;width:400px;"/></td>					
					</tr>
				</tbody>
			</table>
		</div>
    </div>

    <div class="box with-table">
    	<div class="header">
            <h2>Persyaratan siswa baru yang harus dilengkapi</h2>
        </div>

        <div class="content">
			<table style="margin:20px;">
				<tbody>
					<tr>
						<td width='150px' >STK/Surat Tanda Kelulusan</td>
						<td ><input type="file" style="height:10px;width:400px;"/></td>

					</tr>
				</tbody>
			</table>
		</div>
    </div>

    <div class="actions">
        <div class="left">
            <input type="submit" value="Simpan" />
        </div>
    </div>
 </div>
