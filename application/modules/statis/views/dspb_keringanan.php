<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Keringanan</h2> <?php $g=get_jenjang_sekolah(); //echo $g; ?>
       </div>
	   <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pembiayaan/rekap_dspb/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			<div class="dataTables_filter">                
					<table>
						<tr>
							<th width=70px align='left'><span class="text">Kelas</span></th>
							<td width=50px align='left'>
								<?php if($g==4){ ?>
								<select>
									<option value=''>X</option>
									<option value=''>XI</option>
									<option value=''>XII</option>
								</select>
								<?php }elseif($g==3){?>
								<select>
									<option value=''>VII</option>
									<option value=''>VIII</option>
									<option value=''>IX</option>
								</select>
								<?php }elseif($g==2){?>
								<select>
									<option value=''>I</option>
									<option value=''>II</option>
									<option value=''>III</option>
									<option value=''>IV</option>
									<option value=''>V</option>
									<option value=''>VI</option>
								</select>
								<?php } ?>					
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
							
							</td>
						</tr>
					</table>               
            </div>
       </div>            
        </div>
        <div class="content" style="border: 1px solid #bbb">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<?php if($g==4){?>
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Jumlah Keringanan</th>								
								<th>Presentase Keringanan</th>								
								<th>Aksi</th>								
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td width='20px' class='center'>1</td>
								<td width='' class='center'>110294484</td>								
								<td width='' class='center'>Zaki Zima</td>
								<td width='' class='center'>40.000</td>
								<td width='' class='center'>50%</td>
								<td width='100px' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>11023456</td>								
								<td width='' class='center'>Mawar Munawaroh</td>								
								<td width='' class='center'>35.000</td>
								<td width='' class='center'>15%</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>							
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>110294484</td>								
								<td width='' class='center'>Aceng Alim</td>								
								<td width='' class='center'>30.000</td>
								<td width='' class='center'>0%</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>	
						</tbody>
					</table>
					<?php }elseif($g==3){?>
							<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Jumlah Keringanan</th>								
								<th>Presentase Keringanan</th>								
								<th>Aksi</th>								
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td width='20px' class='center'>1</td>
								<td width='' class='center'>110294484</td>								
								<td width='' class='center'>Zaki Zima</td>
								<td width='' class='center'>40.000</td>
								<td width='' class='center'>50%</td>
								<td width='100px' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>11023456</td>								
								<td width='' class='center'>Mawar Munawaroh</td>								
								<td width='' class='center'>35.000</td>
								<td width='' class='center'>15%</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>							
							<tr>
								<td width='' class='center'>3</td>																
								<td width='' class='center'>110294484</td>								
								<td width='' class='center'>Aceng Alim</td>								
								<td width='' class='center'>30.000</td>
								<td width='' class='center'>0%</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>	
						</tbody>
					</table>
					
					<?php }elseif($g==2){ ?>
							<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Jumlah Keringanan</th>								
								<th>Presentase Keringanan</th>								
								<th>Aksi</th>								
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td width='20px' class='center'>1</td>
								<td width='' class='center'>11029123</td>								
								<td width='' class='center'>Zaki Zima</td>
								<td width='' class='center'>40.000</td>
								<td width='' class='center'>50%</td>
								<td width='100px' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>110294112</td>								
								<td width='' class='center'>Yusuf Kurniawan</td>
								<td width='' class='center'>40.000</td>
								<td width='' class='center'>50%</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>							
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>110291234</td>								
								<td width='' class='center'>Ateng Fikri</td>
								<td width='' class='center'>40.000</td>
								<td width='' class='center'>50%</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>110123484</td>								
								<td width='' class='center'>Kurniawan</td>
								<td width='' class='center'>40.000</td>
								<td width='' class='center'>50%</td>								
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='center'>110122284</td>								
								<td width='' class='center'>Bahar Kurniawan</td>
								<td width='' class='center'>40.000</td>
								<td width='' class='center'>50%</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>		
							<tr>
								<td width='' class='center'>6</td>
								<td width='' class='center'>110212384</td>								
								<td width='' class='center'>Dante </td>
								<td width='' class='center'>40.000</td>
								<td width='' class='center'>50%</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>								
						</tbody>
					</table>
					
					<?php }?>
				</div>
            </div>
        </div>        
    </div>
 </div>
