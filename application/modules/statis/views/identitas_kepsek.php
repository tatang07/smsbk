<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
       	<div class="box">
				
					<div class="header">
						<h2>Identitas Kepala Sekolah</h2>
					</div>
					
					<div class="tabletools">
						<div class="right">
							<a href=""><i class="icon-print"></i>Cetak Ke File</a> 
						  <br/><br/>
						</div>
					</div>
		
					<div class="accordion toggle">
					
						<h3><a href="#">Biodata Sekolah</a></h3>
						<div>
							<table class="styled" >
								<thead>
									<tr>
										
									</tr>
								</thead>
								<tbody>
									<tr>
										<td width='200px' class='center'>Nama Sekolah: </td>
										<td >SMA Negeri 1 Sindang</td>
									</tr>
									<tr>
										<td width='' class='center'>Nomor Statistik Sekolah: </td>
										<td >301021816001</td>
									</tr>
									<tr>
										<td width='' class='center'>Nomor Identitas Sekolah</td>
										<td ></td>
									</tr>
									<tr>
										<td width='' class='center'>Status Sekolah: </td>
										<td >Negeri</td>

									</tr>
									<tr>
										<td width='' class='center'>Tanggal Berdiri: </td>
										<td >29 Agustus 1961</td>
									</tr>
									<tr>
										<td width='' class='center'>SK Pendirian: </td>
										<td >135/SK/B/1961</td>
									</tr>
									<tr>
										<td width='' class='center'>Alamat: </td>
										<td >Jl. Letjen MT. Haryono, Kec. SINDANG, Kab. KAB. INDRAMAYU, Provinsi. JAWA BARAT </td>
									</tr>
									<tr>
										<td width='' class='center'>Kode Pos: </td>
										<td >45222</td>
									</tr>
									<tr>
										<td width='' class='center'>Nomor Telepon: </td>
										<td >0234272089</td>
									</tr>
									<tr>
										<td width='' class='center'>Fax: </td>
										<td >0234275315</td>
									</tr>
									<tr>
										<td width='' class='center'>Email: </td>
										<td >smanegeri1_sindang@yahoo.co.id</td>
									</tr>
									<tr>
										<td width='' class='center'>Website: </td>
										<td >http://www.osissasi-b.blogspot.com</td>
									</tr>
									<tr>
										<td width='' class='center'>Luas Halaman: </td>
										<td >0 m2</td>
									</tr>
									<tr>
										<td width='' class='center'>Luas Tanah: </td>
										<td >14521 m2</td>
									</tr>
									<tr>
										<td width='' class='center'>Luas Bangunan: </td>
										<td >5000 m2</td>
									</tr>
									<tr>
										<td width='' class='center'>Luas Lapangan Olahraga: </td>
										<td >0 m2</td>
									</tr>
								</tbody>
							</table>
						</div>
						
						<h3><a href="#">Kepala Sekolah</a></h3>
						<div>
							<table class="styled" >
								<thead>
									<tr>
										
									</tr>
								</thead>
								<tbody>
									<tr>
										<td width='200px' class='center'>Nama: </td>
										<td >Dra. Hj. SULASTRI DJ, M.Pd</td>
									</tr>
									<tr>
										<td width='' class='center'>NIP: </td>
										<td >1955051119</td>
									</tr>
									<tr>
										<td width='' class='center'>Pangkat/Golongan: </td>
										<td >Pembi/IVb</td>

									</tr>
									<tr>
										<td width='' class='center'>Pendidikan: </td>
										<td >S2</td>

									</tr>
									<tr>
										<td width='' class='center'>Nomor SK Pengangkatan: </td>
										<td >824/Kep.284-BKD/2007</td>
									</tr>
									<tr>
										<td width='' class='center'>Tanggal Pengangkatan: </td>
										<td >27 November 2007</td>
									</tr>
									<tr>
										<td width='' class='center'>TMT: </td>
										<td >27 November 2007</td>
									</tr>
								</tbody>
							</table>
						</div>
						
					</div><!-- End of .content -->
					
				</div><!-- End of .box -->		
 </div>
