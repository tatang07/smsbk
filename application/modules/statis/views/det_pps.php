<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Profil Siswa</h2>
       </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<div class="content" style='margin:5px;'>
					<table style="border-spacing: 0px; margin: 5px 0 20px 0; width: 100%; border: 1px solid #D2D2D2; background: #EDF2FD; padding: 5px;">
						<tbody>
							<tr>
								<td style='width:100px;'>Nama</td>
								<td>:&nbsp;</td>
								<td>DESSY AYU INDRAHADI</td>
								<td rowspan='6'>
								<img class="foto" align="right"
								src= 'http://smsbk.medialengka.com/sma/files/foto_siswa/default.jpg' />
								</td>
							</tr>
							<tr>
								<td>NIS</td>
								<td>:&nbsp;</td>
								<td>0910110064</td>
							</tr>
							<tr>
								<td>NISN</td>
								<td>:&nbsp;</td>
								<td>01</td>
							</tr>
							<tr>
								<td>Tempat / Tgl. Lahir</td>
								<td>:&nbsp;</td>
								<td>INDRAMAYU  / 0  0</td>
							</tr>
							<tr>
								<td>Jenis Kelamin</td>
								<td>:&nbsp;</td>
								<td>PEREMPUAN</td>
							</tr>
							<tr>
								<td>Golongan Darah</td>
								<td>:&nbsp;</td>
								<td>A</td>
							</tr>
							<tr>
								<td>Agama</td>
								<td>:&nbsp;</td>
								<td>ISLAM</td>
							</tr>
							<tr>
								<td>Status Anak</td>
								<td>:&nbsp;</td>
								<td>Kandung</td>
							</tr>
							<tr>
								<tr>
								<td>Anak ke</td>
								<td>:&nbsp;</td>
								<td>2 Dari 3 saudara   </td>
							</tr>
							<tr>
								<td>Alamat</td>
								<td>:&nbsp;</td>
								<td>JALAN APEL NO.15 RT/RW. 03/10 BTN BUMI MEKAR KEL.LEMAH MEKAR KEC.INDRAMAYU KAB.INDRAMAYU PROVINSI.JAWA BARAT, Kec. INDRAMAYU, Kab. KAB. INDRAMAYU, Provinsi. JAWA BARAT      </td>
							</tr>
							<tr>
								<td>Nomor Telepon</td>
								<td>:&nbsp;</td>
								<td>08976582227</td>
							</tr>
							<tr>
								<td>Hobby</td>
								<td>:&nbsp;</td>
								<td>Jalan-jalan,Browsing,Olahraga Volley,</td>
							</tr>
							<tr>
								<td>Ektra Kurikuler</td>
								<td>:&nbsp;</td>
								<td></td>
							</tr>
							<tr>
								<td>Nama Ayah</td>
								<td>:&nbsp;</td>
								<td>ACHMAD  SUHADI S.Pd</td>
							</tr>
							<tr>
								<td>Pekerjaan</td>
								<td>:&nbsp;</td>
								<td>PNS</td>
							</tr>
							<tr>
								<td>Penghasilan</td>
								<td>:&nbsp;</td>
								<td>Rp. 2.000.000 - Rp. 5.000.000</td>
							</tr>
							<tr>
								<td>Alamat</td>
								<td>:&nbsp;</td>
								<td>JALAN APEL NO.15 RT/RW.03/10 BTN BUMI MEKAR KEL.LEMAH MEKAR KEC.INDRAMAYU KAB.INDRAMAYU PROV. JAWA BARAT, Kec. INDRAMAYU, Kab. KAB. INDRAMAYU, Provinsi. JAWA BARAT      </td>
							</tr>
								<td>Nama Ibu</td>
								<td>:&nbsp;</td>
								<td>DUNSARI INDRAWATI</td>
							</tr>
							<tr>
								<td>Pekerjaan</td>
								<td>:&nbsp;</td>
								<td>Ibu Rumah Tangga</td>
							</tr>
							<tr>
								<td>Penghasilan</td>
								<td>:&nbsp;</td>
								<td>< Rp. 500.000</td>
							</tr>
								<tr>
								<td>Alamat</td>
								<td>:&nbsp;</td>
							<td>JALAN APEL NO.15 RT/RW.03/10 BTN BUMI MEKAR KEL.LEMAH MEKAR KEC.INDRAMAYU KAB.INDRAMAYU PROV. JAWA BARAT, Kec. INDRAMAYU, Kab. KAB. INDRAMAYU, Provinsi. JAWA BARAT      </td>
							</tr>
						</tbody>
					</table>
					<div class="box tabbedBox">
						<div class="header">
							<h2>Prestasi dan Hobby</h2>
							<ul>
								<li><a href="#t1-c1">Prestasi Ekstrakurikuler</a></li>
								<li><a href="#t1-c2">Prestasi Intrakulikuler</a></li>
								<li><a href="#t1-c3">Hobby</a></li>
							</ul>
						</div><!-- End of .header -->
						
						<div class="content tabbed">
							<div id="t1-c1">
								<table class="styled">
									<thead>
										<tr>
											<th>No.</th>
											<th>Prestasi Ektrakurikuler</th>
											<th>Ekskul</th>
											<th>Tingkat</th>
											<th>Tahun</th>
											<th colspan='2'>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width='' class='center'>1</td>
											<td width='' class='center'>Juara 1 LKBB</td>
											<td width='' class='center'>Paskibra</td>
											<td width='' class='center'>Nasional</td>
											<td width='' class='center'>2011</td>
											<td width='' class='center'>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
											</td>
										</tr>
									</tbody>
								</table>
								<br>
								<button href="javascript:void(0);" class="block">TAMBAH PRESTASI EKSKUL</button>
							</div>
							
							<div id="t1-c2">
								<table class="styled">
									<thead>
										<tr>
											<th>No.</th>
											<th>Prestasi Intrakurikuler</th>
											<th>Tingkat</th>
											<th>Tahun</th>
											<th colspan='2'>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width='' class='center'>1</td>
											<td width='' class='center'>Jenis Prestasi</td>
											<td width='' class='center'>Tingkat Provinsi</td>
											<td width='' class='center'>2011</td>
											<td width='' class='center'>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
											</td>
										</tr>
									</tbody>
								</table>
								<br>
								<button href="javascript:void(0);" class="block">TAMBAH PRESTASI INTRAKULIKULER</button>
							</div>
							
							<div id="t1-c3">
								<table class="styled">
									<thead>
										<tr>
											<th>No.</th>
											<th>Hobby</th>
											<th colspan='2'>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width='' class='center'>1</td>
											<td width='' class='center'>Jalan-jalan</td>
											<td width='' class='center'>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
											</td>
										</tr>
										<tr>
											<td width='' class='center'>2</td>
											<td width='' class='center'>Berenang</td>
											<td width='' class='center'>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
											</td>
										</tr>
										<tr>
											<td width='' class='center'>3</td>
											<td width='' class='center'>Olahraga Volley</td>
											<td width='' class='center'>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
											</td>
										</tr>
									</tbody>
								</table>
								<br>
								<button href="javascript:void(0);" class="block">TAMBAH HOBBY</button>
							</div>
							
						</div><!-- End of .content -->
					</div><!-- End of .box -->
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
