<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Detail Supervisi</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-edit"></i> Cetak Ke File</a> 
			  <br/><br/>
            </div>
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<div class="content">
					<table style="border-spacing: 0px; margin: 5px 0 20px 0; width: 100%; border: 1px solid #D2D2D2; background: #EDF2FD; padding: 5px;">
						<tbody>
							<tr>
								<td width='150' class='left'>Kode Supervisi</td>
								<td width='20' class='left'>:</td>
								<td width='300' class='left'>FORM-KUR-10</td>
							</tr>
							<tr>
								<td width='' class='left'>Hasil</td>
								<td width='' class='left'>:</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>Tanggal Terbit</td>
								<td width='' class='left'>:</td>
								<td width='' class='left'>2009-12-04</td>
							</tr>
							<tr>
								<td width='' class='left'>Waktu</td>
								<td width='' class='left'>:</td>
								<td width='' class='left'>2009-08-12 00:00:00</td>
							</tr>
							<tr>
								<td width='' class='left'>Materi</td>
								<td width='' class='left'>:</td>
								<td width='' class='left'>Matematika</td>
							</tr>
							<tr>
								<td width='' class='left'>Deskripsi</td>
								<td width='' class='left'>:</td>
								<td width='' class='left'>XII SCIENCE - 1</td>
							</tr>
							<tr>
								<td width='' class='left'>Guru</td>
								<td width='' class='left'>:</td>
								<td width='' class='left'>TISNO WIHARDI, SPD</td>
							</tr>
							<tr>
								<td width='' class='left'>Mata Pelajaran</td>
								<td width='' class='left'>:</td>
								<td width='' class='left'>Matematika</td>
							</tr>
							
						</tbody>
					</table>
					</div>
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Butir Supervisi</th>
								<th>Hasil</th>
								<th>Bobot</th>
								<th>Keterangan</th>
						
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='left'>1</td>
								<td width='' class='left'>Penampilan Guru</td>
								<td width='' class='center'>A</td>
								<td width='' class='center'>3</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>2</td>
								<td width='' class='left'>Motivasi</td>
								<td width='' class='center'>A</td>
								<td width='' class='center'>3</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>3</td>
								<td width='' class='left'>Apresepsi</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>2</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>4</td>
								<td width='' class='left'>Penyampaian Tujuan Pembelajaran</td>
								<td width='' class='center'>A</td>
								<td width='' class='center'>3</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>5</td>
								<td width='' class='left'>Penguasaan Materi</td>
								<td width='' class='center'>A</td>
								<td width='' class='center'>3</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>6</td>
								<td width='' class='left'>Metode dan Model Pembelajaran</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>2</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>7</td>
								<td width='' class='left'>Media Pembelajaran</td>
								<td width='' class='center'>A</td>
								<td width='' class='center'>3</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>8</td>
								<td width='' class='left'>Keterlibatan Siswa</td>
								<td width='' class='center'>A</td>
								<td width='' class='center'>3</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>9</td>
								<td width='' class='left'>Bimbingan Kepada Siswa</td>
								<td width='' class='center'>A</td>
								<td width='' class='center'>3</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>10</td>
								<td width='' class='left'>Teknik Bertanya</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>2</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>11</td>
								<td width='' class='left'>Pengelolaan Kelas</td>
								<td width='' class='center'>A</td>
								<td width='' class='center'>3</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>12</td>
								<td width='' class='left'>Bahan Ajar</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>2</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>13</td>
								<td width='' class='left'>Refleksi</td>
								<td width='' class='center'>A</td>
								<td width='' class='center'>3</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>14</td>
								<td width='' class='left'>Kesimpulan</td>
								<td width='' class='center'>A</td>
								<td width='' class='center'>3</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>15</td>
								<td width='' class='left'>Evaluasi</td>
								<td width='' class='center'>A</td>
								<td width='' class='center'>3</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left'>16</td>
								<td width='' class='left'>Penugasan</td>
								<td width='' class='center'>A</td>
								<td width='' class='center'>3</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left' colspan='3'>Jumlah</td>
								<td width='' class='center'>44</td>
								<td width='' class='left'></td>
							</tr>
							<tr>
								<td width='' class='left' colspan='3'>Indeks Prestasi Guru</td>
								<td width='' class='center'>2.75</td>
								<td width='' class='left'></td>
							</tr>
							
						</tbody>
					</table>
				</div>
            </div>
        </div>        
    </div>
 </div>
