<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekapitulasi Jumlah Siswa</h2>
       </div>
        <div class="content">
	       <div class="header">
	            <h2>Tingkat X</h2>
	       </div>
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan='2'>No.</th>
								<th rowspan='2'>Kelas</th>
								<th colspan='3'>Jumlah</th>
							</tr>
							<tr>
								<th>L</th>
								<th>P</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30' class='center'>1</td>
								<td width='500' class='left'>X 1</td>
								<td width='50' class='center'>14</td>
								<td width='50' class='center'>35</td>
								<td width='50' class='center'>49</td>
							</tr>
							<tr>
								<td width='30' class='center'>2</td>
								<td width='500' class='left'>X 2</td>
								<td width='50' class='center'>12</td>
								<td width='50' class='center'>33</td>
								<td width='50' class='center'>45</td>
							</tr>
							<tr>
								<td width='30' class='center'>3</td>
								<td width='500' class='left'>X 3</td>
								<td width='50' class='center'>14</td>
								<td width='50' class='center'>35</td>
								<td width='50' class='center'>49</td>
							</tr>
							<tr>
								<td width='30' class='center'>4</td>
								<td width='500' class='left'>X 4</td>
								<td width='50' class='center'>12</td>
								<td width='50' class='center'>33</td>
								<td width='50' class='center'>45</td>
							</tr>
							<tr>
								<td width='30' class='center'>5</td>
								<td width='500' class='left'>X 5</td>
								<td width='50' class='center'>13</td>
								<td width='50' class='center'>34</td>
								<td width='50' class='center'>47</td>
							</tr>
						</tbody>
					</table>
				</div> <!-- end of datatable wrapper -->
            </div>  <!-- end of datatable -->
        </div> <!--  end of content -->

        <div class="content" style="border-top: 1px solid #ccc;">
	       <div class="header">
	            <h2>Tingkat XI</h2>
	       </div>
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan='2'>No.</th>
								<th rowspan='2'>Kelas</th>
								<th colspan='3'>Jumlah</th>
							</tr>
							<tr>
								<th>L</th>
								<th>P</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30' class='center'>1</td>
								<td width='500' class='left'>XI IPA 1</td>
								<td width='50' class='center'>14</td>
								<td width='50' class='center'>35</td>
								<td width='50' class='center'>49</td>
							</tr>
							<tr>
								<td width='30' class='center'>2</td>
								<td width='500' class='left'>XI IPA 2</td>
								<td width='50' class='center'>12</td>
								<td width='50' class='center'>33</td>
								<td width='50' class='center'>45</td>
							</tr>
							<tr>
								<td width='30' class='center'>3</td>
								<td width='500' class='left'>XI IPS 1</td>
								<td width='50' class='center'>14</td>
								<td width='50' class='center'>35</td>
								<td width='50' class='center'>49</td>
							</tr>
							<tr>
								<td width='30' class='center'>4</td>
								<td width='500' class='left'>XI IPS 2</td>
								<td width='50' class='center'>12</td>
								<td width='50' class='center'>33</td>
								<td width='50' class='center'>45</td>
							</tr>
							<tr>
								<td width='30' class='center'>5</td>
								<td width='500' class='left'>XI Bahasa</td>
								<td width='50' class='center'>13</td>
								<td width='50' class='center'>34</td>
								<td width='50' class='center'>47</td>
							</tr>
						</tbody>
					</table>
				</div> <!-- end of datatable wrapper -->
            </div>  <!-- end of datatable -->
        </div> <!--  end of content -->

        <div class="content" style="border-top: 1px solid #ccc;">
	       <div class="header">
	            <h2>Tingkat XII</h2>
	       </div>
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan='2'>No.</th>
								<th rowspan='2'>Kelas</th>
								<th colspan='3'>Jumlah</th>
							</tr>
							<tr>
								<th>L</th>
								<th>P</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30' class='center'>1</td>
								<td width='500' class='left'>XII IPA 1</td>
								<td width='50' class='center'>14</td>
								<td width='50' class='center'>35</td>
								<td width='50' class='center'>49</td>
							</tr>
							<tr>
								<td width='30' class='center'>2</td>
								<td width='500' class='left'>XII IPA 2</td>
								<td width='50' class='center'>12</td>
								<td width='50' class='center'>33</td>
								<td width='50' class='center'>45</td>
							</tr>
							<tr>
								<td width='30' class='center'>3</td>
								<td width='500' class='left'>XII IPS 1</td>
								<td width='50' class='center'>14</td>
								<td width='50' class='center'>35</td>
								<td width='50' class='center'>49</td>
							</tr>
							<tr>
								<td width='30' class='center'>4</td>
								<td width='500' class='left'>XII IPS 2</td>
								<td width='50' class='center'>12</td>
								<td width='50' class='center'>33</td>
								<td width='50' class='center'>45</td>
							</tr>
							<tr>
								<td width='30' class='center'>5</td>
								<td width='500' class='left'>XII Bahasa</td>
								<td width='50' class='center'>13</td>
								<td width='50' class='center'>34</td>
								<td width='50' class='center'>47</td>
							</tr>
						</tbody>
					</table>
				</div> <!-- end of datatable wrapper -->
            </div>  <!-- end of datatable -->
        </div> <!--  end of content -->
    </div>
 </div>
