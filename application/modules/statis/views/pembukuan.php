<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Pembukuan</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-print"></i>Cetak File</a> 
			  	<a href=""><i class="icon-plus"></i>Sirkulasi Keuangan</a> 
			  <br/><br/>
            </div>
			
			<div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=100px align='left'><span class="text">Tahun Ajaran:</span></th>
							<td width=100px align='left'>
								<select>
									<option value=''>2009/2010</option>
									<option value=''>2010/2011</option>
									<option value=''>2011/2012</option>
									<option value=''>2012/2013</option>
									<option value=''>2013/2014</option>
									<option value=''>2014/2015</option>
								</select>
							</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							
							<th width=100px align='left'><span class="text">Semester:</span></th>
							<td width=100px align='left'>
								<select>
									<option value=''>Ganjil</option>
									<option value=''>Genap</option>
								</select>
							</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i>Tampilkan</i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
        </div>
        <div class="content">
			<div class="header">
				<h2>Penerimaan</h2>
		   </div>
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Tanggal</th>
								<th>Uraian</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width=''>2014-12-02</td>
								<td width=''>Dana BOS</td>
								<td width=''>Rp. 50.000.000,-</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width=''>2014-12-02</td>
								<td width=''>Sumbangan Orangtua</td>
								<td width=''>Rp. 10.000.000,-</td>
							</tr>
							
							<tr>
								<td width='' colspan=3>Total</td>
								<td width=''>Rp. 60.000.000,-</td>
							</tr>
						</tbody>
					</table>
				</div>
            </div>
			<div class="header">
				<h2>Pengeluaran</h2>
		   </div>
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Tanggal</th>
								<th>Uraian</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width=''>2013-12-02</td>
								<td width=''>Perbaikan Gedung</td>
								<td width=''>Rp. 30.000.000,-</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width=''>2012-12-02</td>
								<td width=''>Dies Natalis Sekolah</td>
								<td width=''>Rp. 20.000.000,-</td>
							</tr>
							<tr>
								<td width='' colspan=3>Total</td>
								<td width=''>Rp. 50.000.000,-</td>
							</tr>
						</tbody>
					</table>
				</div>
            </div>
        </div>        
    </div>
 </div>
