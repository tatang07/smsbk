<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Proporsi Agama Siswa</h2>
       </div>
	   	<div class="box">
					<div class="content" style="height: 250px;">
						<table class=chart data-type=bars>
							<thead>
								<tr>
									<th></th>
									<th>Islam</th>
									<th>Protestan</th>
									<th>Katholik</th>
									<th>Hindu</th>
									<th>Budha</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th>X</th>
									<td>253</td>
									<td>2</td>
									<td>1</td>
									<td>0</td>
									<td>0</td>
								</tr>
								<tr>
									<th>XI</th>
									<td>244</td>
									<td>1</td>
									<td>1</td>
									<td>1</td>
									<td>0</td>
								</tr>
								<tr>
									<th>XII</th>
									<td>284</td>
									<td>0</td>
									<td>0</td>
									<td>0</td>
									<td>1</td>
								</tr>
							</tbody>	
						</table>
					</div><!-- End of .content -->
					
				</div><!-- End of .box -->		
        <div class="content" style="border-top:1px solid #bbb">
		
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=2>No.</th>
								<th rowspan=2>Tingkat</th>
								<th colspan=6>Jumlah Siswa Menurut Agama</th>
							</tr>
							<tr>
								<th>Islam</th>
								<th>Protestan</th>
								<th>Katholik</th>
								<th>Hindu</th>
								<th>Budha</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30px' class='center'>1</td>
								<td width='50px'>X</td>
								<td width='50px' class='center'>253</td>
								<td width='50px' class='center'>2</td>
								<td width='50px' class='center'>1</td>
								<td width='50px' class='center'>0</td>
								<td width='50px' class='center'>0</td>
								<td width='50px' class='center'>256</td>
							</tr>
							<tr>
								<td width='30px' class='center'>2</td>
								<td width='50px'>XI</td>
								<td width='50px' class='center'>244</td>
								<td width='50px' class='center'>1</td>
								<td width='50px' class='center'>1</td>
								<td width='50px' class='center'>1</td>
								<td width='50px' class='center'>0</td>
								<td width='50px' class='center'>247</td>
							</tr>
							<tr>
								<td width='30px' class='center'>3</td>
								<td width='50px'>XII</td>
								<td width='50px' class='center'>284</td>
								<td width='50px' class='center'>0</td>
								<td width='50px' class='center'>0</td>
								<td width='50px' class='center'>0</td>
								<td width='50px' class='center'>1</td>
								<td width='50px' class='center'>285</td>
							</tr>
						</tbody>
					</table>
					
				</div>
            </div>
        </div>        
    </div>
 </div>
