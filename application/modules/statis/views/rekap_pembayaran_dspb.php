<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
	<div class="box with-table">
		<div class="header">
			<h2>Rekapitulasi Pembayaran DSPB</h2>
		</div>
		<div class="tabletools">
			<div class="right">
				<a href=""><i class="icon-print"></i>Cetak File</a> 
				<a href=""><i class="icon-plus"></i>Pembayaran DSPB</a> 
				<br><br>
			</div>
		</div>
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Bulan</th>
								<th>Jumlah</th>								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>Juli</td>					
								<td width='' class='center'>1</td>																
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>Januari</td>					
								<td width='' class='center'>1</td>														
							</tr>							
						</tbody>
					</table>
					<div class="footer">

					</div>
				</div>
			</div>
		</div>        
	</div>
</div>
