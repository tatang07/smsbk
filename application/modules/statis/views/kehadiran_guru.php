<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekap Kehadiran Guru</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=80px align='left'><span class="text">Guru</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>Dr. Hasan</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
							<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Mata Pelajaran</th>
								<th>Kelas</th>
								<th>Pertemuan</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>A003</td>								
								<td width='' class='center'>Matematika</td>
								<td width='' class='center'>X-1</td>
								<td width='' class='center'>3</td>								
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>A004</td>								
								<td width='' class='center'>Matematika</td>
								<td width='' class='center'>X-2</td>
								<td width='' class='center'>2</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>A005</td>								
								<td width='' class='center'>Matematika</td>
								<td width='' class='center'>XI-IPA-1</td>
								<td width='' class='center'>2</td>
							</tr>							
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 3</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
