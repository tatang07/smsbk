<h1 class="grid_12">Pendidik Dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Orientasi Kerja/Jabatan Tenaga Kependidikan</h2>
       </div> 
		<fieldset>
		
		<div class="grid_12">
			Keterampilan :
			<ol>
				<li>keterampilan teknis</li>
				<li>keterampilan hubungan manusia</li>
				<li>keterampilan konsepsional</li>
				<li>keterampilan pendidikan dan pengajaran</li>
				<li>keterampilan kognitif</li>
			</ol>

			<br/>
			Kompetensi :
			<ol>
				<li>komitmen terhadap misi sekolah dan keterampilan untuk menjadikan gambaran bagi sekolahnya</li>
				<li>orientasi kepemimpinan proaktif</li>
				<li>ketegasan</li>
				<li>sensitif terhadap hubungan yang bersifat interpersonal dan organisasi</li>
				<li>mengumpulkan informasi, menganalisis pembentukan konsep</li>
				<li>fleksibilitas intelektual</li>
				<li>persuasif dan manajemen interaksi</li>
				<li>kemampuan beradaptasi secara taktis</li>
				<li>motivasi dan perhatian terhadap pengembangan</li>
				<li>manajemen kontrol</li>
				<li>kemampuan berorganisasi dan pendelegasian</li>
				<li>komunikasi</li>
			</ol>
    	</div>
    </div>
 </div>
