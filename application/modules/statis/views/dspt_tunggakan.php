<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>DSPT Tunggakan</h2> <?php $g=get_jenjang_sekolah(); //echo $g; ?>
       </div>
	   <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pembiayaan/rekap_dspb/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			<div class="dataTables_filter">                
					<table>
						<tr>
							<th width=70px align='left'><span class="text">Kelas</span></th>
							<td width=100px align='left'>
								<?php if($g==4){ ?>
								<select>
									<option value='X-1'>X-1</option>
									<option value=''>X-2</option>
									<option value=''>X-3</option>
									<option value=''>XI-IPA</option>
									<option value=''>XI-IPS</option>
									<option value=''>XI-BAHASA</option>
									<option value=''>XII-IPA</option>
									<option value=''>XII-IPS</option>
									<option value=''>XII-BAHASA</option>
								</select>
								<?php }elseif($g==3){?>
								<select>
									<option value=''>VII-1</option>
									<option value=''>VII-2</option>
									<option value=''>VII-3</option>
									<option value=''>VIII-1</option>
									<option value=''>VIII-2</option>
									<option value=''>VIII-3</option>
									<option value=''>IX-1</option>
									<option value=''>IX-2</option>
									<option value=''>IX-3</option>
								</select>
								<?php }elseif($g==2){?>
								<select>
									<option value=''>I-1</option>
									<option value=''>I-2</option>
									<option value=''>I-3</option>
									<option value=''>II-1</option>
									<option value=''>II-2</option>
									<option value=''>II-3</option>
									<option value=''>III-1</option>
									<option value=''>III-2</option>
									<option value=''>III-3</option>
									<option value=''>IV-1</option>
									<option value=''>IV-2</option>
									<option value=''>IV-3</option>
									<option value=''>V-1</option>
									<option value=''>V-2</option>
									<option value=''>V-3</option>
									<option value=''>VI-1</option>
									<option value=''>VI-2</option>
									<option value=''>VI-3</option>
								</select>
								<?php } ?>					
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
							
							</td>
						</tr>
					</table>               
            </div>
       </div>            
        </div>
        <div class="content" style="border: 1px solid #bbb">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<?php if($g==3){?>
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nis</th>
								<th>Nama</th>
								<th>Jumlah Tagihan</th>								
								<th>Dibayar</th>								
								<th>Tunggakan</th>								
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td width='20px' class='center'>1</td>
								<td width='' class='center'>1203524</td>								
								<td width='' class='left'>Andriyanto Handian</td>
								<td width='' class='left'>40.000</td>
								<td width='' class='left'>20.000</td>
								<td width='' class='left'>20.000</td>
								
							</tr>
							<tr>
								<td width='20px' class='center'>2</td>
								<td width='' class='center'>1203525</td>								
								<td width='' class='left'>Ratna Galih</td>
								<td width='' class='left'>40.000</td>
								<td width='' class='left'>20.000</td>
								<td width='' class='left'>20.000</td>
							</tr>							
							<tr>
								<td width='20px' class='center'>3</td>
								<td width='' class='center'>1203526</td>								
								<td width='' class='left'>Mulyadi Mulawarman</td>
								<td width='' class='left'>40.000</td>
								<td width='' class='left'>20.000</td>
								<td width='' class='left'>20.000</td>
							</tr>	
						</tbody>
					</table>
					<?php }elseif($g==2){?>
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nis</th>
								<th>Nama</th>
								<th>Jumlah Tagihan</th>								
								<th>Dibayar</th>								
								<th>Tunggakan</th>								
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td width='20px' class='center'>1</td>
								<td width='' class='center'>1203524</td>								
								<td width='' class='left'>Andriyanto Handian</td>
								<td width='' class='left'>40.000</td>
								<td width='' class='left'>20.000</td>
								<td width='' class='left'>20.000</td>
								
							</tr>
							<tr>
								<td width='20px' class='center'>2</td>
								<td width='' class='center'>1203525</td>								
								<td width='' class='left'>Ratna Galih</td>
								<td width='' class='left'>40.000</td>
								<td width='' class='left'>20.000</td>
								<td width='' class='left'>20.000</td>
							</tr>							
							<tr>
								<td width='20px' class='center'>3</td>
								<td width='' class='center'>1203526</td>								
								<td width='' class='left'>Mulyadi Mulawarman</td>
								<td width='' class='left'>40.000</td>
								<td width='' class='left'>20.000</td>
								<td width='' class='left'>20.000</td>
							</tr>	
						</tbody>
					</table>
					<?php }else{ ?>
								<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nis</th>
								<th>Nama</th>
								<th>Jumlah Tagihan</th>								
								<th>Dibayar</th>								
								<th>Tunggakan</th>								
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td width='20px' class='center'>1</td>
								<td width='' class='center'>1203524</td>								
								<td width='' class='left'>Andriyanto Handian</td>
								<td width='' class='left'>40.000</td>
								<td width='' class='left'>20.000</td>
								<td width='' class='left'>20.000</td>
								
							</tr>
							<tr>
								<td width='20px' class='center'>2</td>
								<td width='' class='center'>1203525</td>								
								<td width='' class='left'>Ratna Galih</td>
								<td width='' class='left'>40.000</td>
								<td width='' class='left'>20.000</td>
								<td width='' class='left'>20.000</td>
							</tr>							
							<tr>
								<td width='20px' class='center'>3</td>
								<td width='' class='center'>1203526</td>								
								<td width='' class='left'>Mulyadi Mulawarman</td>
								<td width='' class='left'>40.000</td>
								<td width='' class='left'>20.000</td>
								<td width='' class='left'>20.000</td>
							</tr>	
						</tbody>
					</table>
					
					<?php }?>
				</div>
            </div>
        </div>        
    </div>
 </div>
