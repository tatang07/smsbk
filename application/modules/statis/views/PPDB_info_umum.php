<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>
<div class="grid_12">
    <div class="box with-table">
        <div class="header">
            <div class="right" style="float:right;padding-right:10px;padding-top:9px;">
			  	<a href=""><i class="icon-print"></i> Cetak</a> 
            </div>
            <h2>Informasi Umum Seleksi dan Penerimaan Siswa Baru Tahun Ajaran 2014/2015</h2>
        </div>

        <div class="content">
            <div id="datatable" url="">            	
				<div class="dataTables_wrapper" role="grid">    
					<table class="styled" >
						<tbody>
							<?php foreach($data as $d):?>
							<tr>
							
								<td width='' >Nama Sekolah: </td>
								<td ><?php echo $d['sekolah']?></td>
							
							</tr>
							<tr>
								<td width='' >Nomor Statistik Sekolah (NSS)</td>
								<td ><?php echo $d['nss']?></td>
							</tr>
							<tr>
								<td width='' >No Pokok Sekolah</td>
								<td ><?php echo $d['nps']?></td>

							</tr>
							<tr>
								<td width='' >Status Sekolah</td>
								<td ><?php echo $d['status_sekolah']?></td>

							</tr>
							<tr>
								<td width='' >Tanggal Berdiri</td>
								<td ><?php echo $d['tgl_sk_pendirian']?></td>

							</tr>
							<tr>
								<td width='' >SK Pendirian</td>
								<td ><?php echo $d['sk_pendirian']?></td>

							</tr>
							<tr>
								<td width='' >Alamat</td>
								<td ><?php echo $d['alamat']?></td>

							</tr>
							<tr>
								<td width='' >Kode Pos</td>
								<td ><?php echo $d['kode_pos']?></td>

							</tr>
							<tr>
								<td width='' >No Telepon</td>
								<td ><?php echo $d['telepon']?></td>

							</tr>
							<tr>
								<td width='' >Fax</td>
								<td ><?php echo $d['fax']?></td>

							</tr>
							<tr>
								<td width='' >E-mail</td>
								<td ><?php echo $d['email']?></td>

							</tr>
							<tr>
								<td width='' >Website</td>
								<td ><?php echo $d['website']?></td>

							</tr>
							<tr>
								<td width='' >Luas Halaman</td>
								<td ><?php echo $d['luas_halaman']?></td>

							</tr>
							<tr>
								<td width='' >Luas Tanah</td>
								<td ><?php echo $d['luas_tanah']?></td>

							</tr>
							<tr>
								<td width='' >Luas Bangunan</td>
								<td ><?php echo $d['luas_bangunan']?></td>

							</tr>
							<tr>
								<td width='' >Luas Lapangan Olahraga</td>
								<td ><?php echo $d['luas_olahraga']?></td>

							</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </div>

	
 </div>
