<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Penilaian Kinerja Guru</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-print"></i>Cetak File</a> 
			  	<a href=""><i class="icon-plus"></i>Tambah Penilaian Kompetensi</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=50px align='left'><span class="text">Guru:</span></th>
							<td width=200px align='left'>
								<select>
								<option value="18" selected="selected">ACHMAD SUHADI, SPd</option>
								<option value="10">AGUS RIYANTO,S.Pd</option>
								<option value="1" selected="selected">Diana Susanti, S.Si.</option>
								<option value="25">Dra. Hj. SULASTRI DJ, M.Pd</option>
								<option value="6">Drs Iman Permana Hadi</option>
								<option value="14">Enah Suhaenah S Pd</option>
								<option value="15">Evi Sofia Nurajizah</option>
								<option value="11">H.Muhammad Ishak, S.Pd.</option>
								<option value="8">Imas Iriani</option>
								<option value="5">Indra Yuliyanti</option>
								<option value="2">LILIT SUTARSIH, S.PD</option>
								<option value="17">MAMAN RACHMAN, S.Pd.</option>
								<option value="7">Masruri</option>
								<option value="19">NURHAYATI, M, S.Pd</option>
								<option value="12">Repiana</option>
								<option value="4">Rudi Hartono, S.Pd</option>
								<option value="20">Saptarini Kismawati, SS</option>
								<option value="23">SOIPAH, S. KOM</option>
								<option value="24">Tias Beni Purabaya, S.Kom</option>
								<option value="13">TISNO WIHARDI, SPD</option>
								<option value="3">Warsidi,S.Si</option>
								<option value="21">Yati Mulyati, S.Pd</option>
								<option value="9">Yulianingsih, S.S</option>
								</select>
							</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Periode Penilaian</th>
								<th>Penilai</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width=''>10 Desember 2014</td>
								<td width=''>ACHMAD SUHADI, SPd</td>
								<td width='' class='center'>
								<a href="<?php echo base_url("statis/detail_penilaian_kinerja_guru"); ?>" class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<td width='' class='center'>2</td>
								<td width=''>08 Desember 2014</td>
								<td width=''>Warsidi,S.Si</td>
								<td width='' class='center'>
								<a href="<?php echo base_url("statis/detail_penilaian_kinerja_guru"); ?>" class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 2</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
