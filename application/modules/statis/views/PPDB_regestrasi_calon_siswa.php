<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>
<div class="grid_12">
    <div class="box with-table">
        <div class="header">
            <div class="right" style="float:right;padding-right:10px;padding-top:9px;">
			  	<a href=""><i class="icon-print"></i> Cetak</a> 
            </div>
            <h2>Registrasi Ulang </h2>
        </div>
	</div>

    <div class="box with-table">
		<div class="content" style="padding-left:10px;padding-right:20px;">
			<h3>
				Aktifkan berdasarkan no Registrasi
			</h3>       
			<table style="margin-bottom:20px;">
				<tbody>
					<tr>
						<td width="70px">No Reg</td>			
						<td><input type="text" style="height:10px;width:400px;"/></td>			
					</tr>
				</tbody>
			</table>
	        <div class="left" style="margin-bottom:20px;">
	            <input type="submit" value="Aktifkan" />
	        </div>
		</div>
	</div>

	<div class="box with-table">
		<div class="content" style="padding-left:10px;padding-right:20px;">
			<h3>
				Cari
			</h3>       
			<table style="margin-bottom:20px;">
				<tbody>
					<tr>
						<td width="70px">Kriteria</td>			
						<td>
							<select>
								<option>Nama</option>
								<option>No Regestrasi</option>
							</select>
						</td>			
					</tr>
				</tbody>
			</table>
	        <div class="left" style="margin-bottom:20px;">
	            <input type="submit" value="Cari" />
	        </div>
		</div>
	</div>

	<div class="box with-table">
		<div class="content">
            <div id="datatable" url="">            	
				<div class="dataTables_wrapper" role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No</th>
								<th>No Regestrasi</th>
								<th>Nama</th>
								<th>Nilai</th>
								<th>L/P</th>
								<th>Aksi</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td>1</td>
								<td>0890761536</td>							
								<td>Ahmad Zaenuddin</td>							
								<td>95</td>						
								<td>L</td>					
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-file"></i> Lihat</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-download"></i> Download</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>					
							</tr>
							<tr>
								<td>2</td>
								<td>08906153928</td>							
								<td>Ardian August Rachman</td>							
								<td>80</td>						
								<td>L</td>								
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-file"></i> Lihat</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-download"></i> Download</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>							
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </div>
 </div>
