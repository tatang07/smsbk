<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2><a href="<?php echo site_url('statis/jenis_ekstrakulikuler'); ?>">Informasi Umum Seleksi dan Penerimaan Siswa Baru Tahun Ajaran 2014/2015</a> &bull;</h2>
			
       </div>
	
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon"></i>Ubah Informasi</a>
						
			  <br/><br/>
            </div>
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' >Nama Sekolah </td>
								<td >SMA Negeri 1 Sindang</td>
								<td><img width="100px" src="<?php echo base_url("extras/sekolah/logo1.png"); ?>" /></td>
							</tr>
							<tr>
								<td width='' >Nomor Statistik Sekolah (NSS)</td>
								<td >301021816001</td>
							</tr>
							<tr>
								<td width='' >No Identitas Sekolah</td>
								<td ></td>

							</tr>
							<tr>
								<td width='' >Status Sekolah</td>
								<td >Negeri</td>

							</tr>
							<tr>
								<td width='' >Tahun Berdiri</td>
								<td >1961-08-29</td>

							</tr>
							<tr>
								<td width='' >SK Pendirian</td>
								<td >135/SK/B/1961</td>

							</tr>
							<tr>
								<td width='' >Kode Pos</td>
								<td >45222</td>

							</tr>
							<tr>
								<td width='' >No Telepon</td>
								<td >0234272089</td>

							</tr>
							<tr>
								<td width='' >Fax</td>
								<td >0234275315</td>

							</tr>
							<tr>
								<td width='' >E-mail</td>
								<td > smanegeri1_sindang@yahoo.co.id</td>

							</tr>
							<tr>
								<td width='' >Website</td>
								<td >http://www.osissasi-b.blogspot.com</td>

							</tr>
							<tr>
								<td width='' >Luas Halaman</td>
								<td >0 m2</td>

							</tr>
							<tr>
								<td width='' >Luas Tanah</td>
								<td >14521 m2</td>

							</tr>
							<tr>
								<td width='' >Luas Bangunan</td>
								<td >5000 m2</td>

							</tr>
							<tr>
								<td width='' >Luas Lapangan Olahraga</td>
								<td >0 m2</td>

							</tr>
						
							
						</tbody>
					</table>
					
					<br/>
					<table class="styled" >
						<thead>
							<tr>
								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>Selamat datang di SMAN 1 Sindang </td>
								<td >Anda dapat melakukan registrasi siswa baru secara Online melalui hlaman ini.</td>
							</tr>
					
							
						</tbody>
					</table>
					<div class="footer">
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
