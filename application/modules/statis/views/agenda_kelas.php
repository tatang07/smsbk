<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
		<div class="box tabbedBox">

			<div class="header">
				<h2>Agenda Kelas</h2>
				<ul>
					<li><a href="#t1-c1">Harian</a></li>
					<li><a href="#t1-c2">Mata Pelajaran</a></li>
				</ul>
			</div><!-- End of .header -->

			<div class="content tabbed">

				<div id="t1-c1">
					<div class="tabletools">
						<div class="right">
							<a href=""><i class="icon-plus"></i>Tambah</a> 
							<br/><br/>
						</div>
						<div class="dataTables_filter">
							<form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
								<table>
									<tr>
										<th width=1px align='left'><span class="text">Kelas:</span></th>
										<td width=100px align='left'>
											<select>
												<option value=''>X 1</option>
												<option value=''>X 2</option>
												<option value=''>X 3</option>
												<option value=''>X 4</option>
												<option value=''>X 5</option>
												<option value=''>XI IPA 1</option>
												<option value=''>XI IPA 2</option>
												<option value=''>XI IPA 3</option>
												<option value=''>XI IPA 4</option>
												<option value=''>XI IPS 1</option>
												<option value=''>XI IPS 2</option>
												<option value=''>XI IPS 3</option>
												<option value=''>XI BAHASA 1</option>
												<option value=''>XI BAHASA 2</option>
												<option value=''>XII IPA 1</option>
												<option value=''>XII IPA 2</option>
												<option value=''>XII IPA 3</option>
												<option value=''>XII IPA 4</option>
												<option value=''>XII IPS 1</option>
												<option value=''>XII IPS 2</option>
												<option value=''>XII IPS 3</option>
												<option value=''>XII BAHASA 1</option>
												<option value=''>XII BAHASA 2</option>
											</select>
										</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<th width=30px align='left'><span class="text">Tanggal</span></th>
										<td width=100px align='left'>
											<input type="date" name=f3_date id=f3_date />
										</td>					
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>
											<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
										</td>
									</tr>
								</table>
							</form>
						</div>
						</div>
						
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">    
								<table class="styled" >
									<thead>
										<tr>
											<th>No.</th>
											<th>Kode Mata Pelajaran</th>
											<th>Mata Pelajaran</th>
											<th>Materi</th>
											<th>Proses Belajar</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width='30px' class='center'>1</td>
											<td width='100px' class='center'>A003</td>
											<td width='200px'>Matematika</td>
											<td width='200px'>Himpunan</td>
											<td width='100px'></td>
											<td class='center'>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
										<tr>
											<td width='' class='center'>2</td>
											<td width='' class='center'>A004</td>
											<td>Bahasa Indonesia</td>
											<td>Kalimat Aktif</td>
											<td></td>
											<td class='center'>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
										<tr>
											<td width='' class='center'>3</td>
											<td width='' class='center'>A005</td>
											<td>Bahasa Inggris</td>
											<td>Industry</td>
											<td></td>
											<td class='center'>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>

									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Data: 3</div>
									<div class="dataTables_paginate paging_full_numbers">
										<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
										<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
											<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
										</span>
										<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>

										<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="t1-c2">
						<div class="tabletools">
						<div class="dataTables_filter">
							<form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
								<table>
									<tr>
										<th width=1px align='left'><span class="text">Kelas:</span></th>
										<td width=100px align='left'>
											<select>
												<option value=''>X 1</option>
												<option value=''>X 2</option>
												<option value=''>X 3</option>
												<option value=''>X 4</option>
												<option value=''>X 5</option>
												<option value=''>XI IPA 1</option>
												<option value=''>XI IPA 2</option>
												<option value=''>XI IPA 3</option>
												<option value=''>XI IPA 4</option>
												<option value=''>XI IPS 1</option>
												<option value=''>XI IPS 2</option>
												<option value=''>XI IPS 3</option>
												<option value=''>XI BAHASA 1</option>
												<option value=''>XI BAHASA 2</option>
												<option value=''>XII IPA 1</option>
												<option value=''>XII IPA 2</option>
												<option value=''>XII IPA 3</option>
												<option value=''>XII IPA 4</option>
												<option value=''>XII IPS 1</option>
												<option value=''>XII IPS 2</option>
												<option value=''>XII IPS 3</option>
												<option value=''>XII BAHASA 1</option>
												<option value=''>XII BAHASA 2</option>
											</select>
										</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<th width=80px align='left'><span class="text">Mata Pelajaran</span></th>
										<td width=200px align='left'>
											<select>
												<option value=''>A001 - Biologi - XI IPA</option>
												<option value=''>A002 - Kimia - XI IPA</option>
												<option value=''>A003 - Matematika - XI IPA</option>
												<option value=''>A004 - Fisika - XI IPA</option>
												<option value=''>A005 - Sejarah - XI IPS</option>
												<option value=''>A006 - Sosiologi - XI IPS</option>
												<option value=''>A007 - Geografi - XI IPS</option>
												<option value=''>A008 - Ekonomi - XI IPS</option>
											</select>
										</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>
											<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
										</td>
									</tr>
								</table>
							</form>
						</div>
						</div>
						
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">    
								<table class="styled" >
									<thead>
										<tr>
											<th>No.</th>
											<th>Tanggal</th>
											<th>Materi</th>
											<th>Proses Belajar</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width='30px' class='center'>1</td>
											<td width='100px' class='center'>09/12/2014</td>
											<td width='300px'>Himpunan</td>
											<td width='200px'></td>
											<td class='center'>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
										<tr>
											<td width='' class='center'>2</td>
											<td width='' class='center'>10/12/2014</td>
											<td>Operasi Himpunan</td>
											<td></td>
											<td class='center'>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
										<tr>
											<td width='' class='center'>3</td>
											<td width='' class='center'>16/12/2014</td>
											<td>Tes Kompetensi 1</td>
											<td></td>
											<td class='center'>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
												<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>

									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Data: 3</div>
									<div class="dataTables_paginate paging_full_numbers">
										<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
										<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
											<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
										</span>
										<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>

										<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div><!-- End of .content -->

			</div><!-- End of .box -->
	</div>
