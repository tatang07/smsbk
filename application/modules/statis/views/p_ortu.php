<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekapitulasi Pekerjaan dan Penghasilan Orang Tua</h2>
       </div>
	   <div class="box">
	   <div class="content" style="height: 250px;">
			<table class=chart data-type=pie data-donut=0>
				<thead>
					<tr>
						<th></th>
						<th>Grafik Rekap Pekerjaan Orang Tua</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>Dagang</th>
						<td>10</td>
					</tr>
					<tr>
						<th>Programming</th>
						<td>34</td>
					</tr>
					<tr>
						<th>Ibu Rumah Tangga</th>
						<td>33</td>
					</tr>
					<tr>
						<th>Pegawai BUMN</th>
						<td>15</td>
					</tr>
					<tr>
						<th>Pegawai BUMS</th>
						<td>7</td>
					</tr>
					<tr>
						<th>PNS</th>
						<td>72</td>
					</tr>
					<tr>
						<th>POLRI</th>
						<td>2</td>
					</tr>
					<tr>
						<th>Tani</th>
						<td>3</td>
					</tr>
					<tr>
						<th>Tani</th>
						<td>3</td>
					</tr>
					<tr>
						<th>Wirausaha</th>
						<td>40</td>
					</tr>					
				</tbody>	
			</table>
		</div><!-- End of .content -->
		</div><!-- End of .box -->
					
        <div class="content" style="border-top:1px solid #bbb">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jenis Pekerjaan</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30px' class='center'>1</td>
								<td width='500px'>Dagang</td>
								<td width='50px' class='center'>10</td>
							</tr>
							<tr>
								<td width='30px' class='center'>2</td>
								<td width='500px'>Ibu Rumah Tangga</td>
								<td width='50px' class='center'>33</td>
							</tr>
							<tr>
								<td width='30px' class='center'>3</td>
								<td width='500px'>Pegawai BUMN</td>
								<td width='50px' class='center'>15</td>
							</tr>
							<tr>
								<td width='30px' class='center'>4</td>
								<td width='500px'>Pegawai BUMS</td>
								<td width='50px' class='center'>7</td>
							</tr>
							<tr>
								<td width='30px' class='center'>5</td>
								<td width='500px'>PNS</td>
								<td width='50px' class='center'>72</td>
							</tr>
							<tr>
								<td width='30px' class='center'>6</td>
								<td width='500px'>POLRI</td>
								<td width='50px' class='center'>2</td>
							</tr>
							<tr>
								<td width='30px' class='center'>7</td>
								<td width='500px'>Tani</td>
								<td width='50px' class='center'>3</td>
							</tr>
							<tr>
								<td width='30px' class='center'>8</td>
								<td width='500px'>Wirausaha</td>
								<td width='50px' class='center'>40</td>
							</tr>
						</tbody>
					</table>
					
				</div>
            </div>
        </div>        
		<div class="header">
            <h2>Rekapitulasi Penghasilan Orang Tua</h2>
       </div>
	   	   	<div class="box">
					<div class="content" style="height: 250px;">
						<table class=chart data-type=bars>
				<thead>					
				</thead>
				<tbody>
					<tr>
						<th>< Rp. 500.000</th>
						<td>47</td>
					</tr>								
					<tr>
						<th>Rp. 500.000 - Rp. 1.000.000</th>
						<td>25</td>
					</tr>									
					<tr>
						<th>Rp. 1.000.000 - Rp. 2.000.000</th>
						<td>53</td>
					</tr>
					<tr>
						<th>Rp. 2.000.000 - Rp. 5.000.000</th>
						<td>44</td>
					</tr>
					<tr>
						<th>> Rp. 5.000.000</th>
						<td>13</td>
					</tr>
					
				</tbody>	
			</table>
		</div><!-- End of .content -->
		</div><!-- End of .box -->
		
		        <div class="content" style="border-top:1px solid #bbb">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>						
								<th>< Rp. 500.000</th>
								<th> Rp. 500.000 - Rp. 1.000.000 </th>
								<th> Rp. 1.000.000 - Rp. 2.000.000 </th>
								<th> Rp. 2.000.000 - Rp. 5.000.000 </th>
								<th>> Rp. 5.000.000</th>
							</tr>
						</thead>
						<tbody>
							<tr>								
								<td width='' class='center'>47</td>
								<td width='' class='center'>25</td>
								<td width='' class='center'>53</td>
								<td width='' class='center'>44</td>
								<td width='' class='center'>13</td>
							</tr>
							
						</tbody>
					</table>
					
				</div>
            </div>
        </div> 	
    </div>
 </div>
