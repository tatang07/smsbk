<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Penugasan dan Penentuan Beban Mengajar</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i><img src="http://localhost/smsbkv2//theme/img/icons/packs/iconsweets2/14x14/excel-document.png"></i>Excel</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							
							<th width=1px align='left'><span class="text">Kriteria:</span></th>
							<td width=100px align='left'><select>
									<option value=''>Nama</option>
									<option value=''>Beban Mengajar</option>
									<option value=''>Pendidikan</option>
									<option value=''>Jumlah Mata Pelajaran</option>
									</select>
								</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<th width=75px align='left'><span class="text">Kata Kunci:</span></th>
							<td width=100px align='left'><input type='text'></td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Guru</th>
								<th>Beban Mengajar</th>
								<th>Pendidikan</th>
								<th>Jumlah Mata Pelajaran</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30px' class='center'>1</td>
								<td width='400px'>Diana Susanti, S.Si.</td>
								<td width='100px' class='center'>4</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>2</td>
								<td width='400px'>LILIT SUTARSIH, S.PD</td>
								<td width='100px' class='center'>5</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>3</td>
								<td width='400px'>Warsidi,S.Si</td>
								<td width='100px' class='center'>9</td>
								<td width='100px' class='center'>SMA/S</td>
								<td width='100px' class='center'>2</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>4</td>
								<td width='400px'>Rudi Hartono, S.Pd</td>
								<td width='100px' class='center'>6</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>3</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>5</td>
								<td width='400px'>Indra Yuliyanti</td>
								<td width='100px' class='center'>4</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>6</td>
								<td width='400px'>Drs Iman Permana Hadi</td>
								<td width='100px' class='center'>5</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>7</td>
								<td width='400px'>Masruri</td>
								<td width='100px' class='center'>4</td>
								<td width='100px' class='center'>S2</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>8</td>
								<td width='400px'>Imas Iriani</td>
								<td width='100px' class='center'>4</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>9</td>
								<td width='400px'>Yulianingsih, S.S</td>
								<td width='100px' class='center'>6</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>3</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr><tr>
								<td width='30px' class='center'>10</td>
								<td width='400px'>AGUS RIYANTO,S.Pd</td>
								<td width='100px' class='center'>2</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>11</td>
								<td width='400px'>H.Muhammad Ishak, S.Pd.</td>
								<td width='100px' class='center'>6</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>2</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr><tr>
								<td width='30px' class='center'>12</td>
								<td width='400px'>Repiana</td>
								<td width='100px' class='center'>5</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>13</td>
								<td width='400px'> TISNO WIHARDI, SPD</td>
								<td width='100px' class='center'>5</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>14</td>
								<td width='400px'>Enah Suhaenah S Pd</td>
								<td width='100px' class='center'>6</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>3</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>15</td>
								<td width='400px'>Evi Sofia Nurajizah</td>
								<td width='100px' class='center'>5</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>16</td>
								<td width='400px'>MAMAN RACHMAN, S.Pd.</td>
								<td width='100px' class='center'>5</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>17</td>
								<td width='400px'>ACHMAD SUHADI, SPd</td>
								<td width='100px' class='center'>2</td>
								<td width='100px' class='center'>TK</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>18</td>
								<td width='400px'>Yati Mulyati, S.Pd</td>
								<td width='100px' class='center'>4</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>1</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>19</td>
								<td width='400px'>SOIPAH, S. KOM</td>
								<td width='100px' class='center'>4</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>2</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<tr>
								<td width='30px' class='center'>20</td>
								<td width='400px'>Tias Beni Purabaya, S.Kom</td>
								<td width='100px' class='center'>8</td>
								<td width='100px' class='center'>S1</td>
								<td width='100px' class='center'>4</td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 20</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
