<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Persyaratan Menjadi Kepala Sekolah</h2>
       </div> 
		<fieldset>
		
		<div class="grid_12">
			Persyaratan :
			<ul>
				<li>Pendidikan Minimal SI/D4</li>
				<li>Usia Maksimal 56 tahun</li>
				<li>Pengalaman mengajar minimal 5 tahun</li>
			</ul>
    	</div>
    </div>
 </div>
