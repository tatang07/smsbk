<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekap Tunggakan DSPB</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-print"></i>Cetak File</a> 
			  	<a href=""><i class="icon-plus"></i>Tambah</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=100px align='left'><span class="text">Kelas:</span></th>
							<td width=100px align='left'>
								<select>
									<option value=''>X-1</option>
									<option value=''>X-2</option>
									<option value=''>X-3</option>
									<option value=''>X-4</option>
									<option value=''>X-5</option>
									<option value=''>X-6</option>
									<option value=''>X-7</option>
									<option value=''>XI IPA-1</option>
									<option value=''>XI IPA-2</option>
									<option value=''>XI IPA-3</option>
									<option value=''>XI IPA-4</option>
									<option value=''>XI IPS-1</option>
									<option value=''>XI IPS-2</option>
									<option value=''>XI IPS-3</option>
									<option value=''>XII IPA-1</option>
									<option value=''>XII IPA-2</option>
									<option value=''>XII IPA-3</option>
									<option value=''>XII IPA-4</option>
									<option value=''>XII IPS-1</option>
									<option value=''>XII IPS-2</option>
									<option value=''>XII IPS-3</option>
								</select>
							</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Jan</th>
								<th>Feb</th>
								<th>Mar</th>
								<th>Apr</th>
								<th>Mei</th>
								<th>Jun</th>
								<th>Jul</th>
								<th>Aug</th>
								<th>Sep</th>
								<th>Okt</th>
								<th>Nov</th>
								<th>Des</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width=''>0910110064</td>
								<td width=''>DESSY AYU INDRAHADI</td>
								<td width='' class="lunas">L</td>
								<td width='' class="tunggak">T</td>
								<td width='' class="tunggak">T</td>
								<td width='' class="tunggak">T</td>
								<td width='' class="tunggak">T</td>
								<td width='' class="tunggak">T</td>
								<td width='' class="tunggak">T</td>
								<td width='' class="tunggak">T</td>
								<td width='' class="tunggak">T</td>
								<td width='' class="tunggak">T</td>
								<td width='' class="tunggak">T</td>
								<td width='' class="tunggak">T</td>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 1</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
