<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Catatan BP/BK</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-print"></i>Cetak</a> 
			  	<a href=""><i class="icon-plus"></i>Tambah</a> 
			  <br/><br/>
            </div>
	   </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama Siswa</th>
								<th>Tanggal Dicatat</th>
								<th>Keterangan</th>
								<th>Aksi</th>
						
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>0910127</td>
								<td width='' class='left'>Iip Ar</td>
								<td width='' class='center'>23-10-2014</td>
								<td width='' class='left'>Baju dikeluarkan, memakai cat kuku.</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>0910128</td>
								<td width='' class='left'>Aan Nurjannah</td>
								<td width='' class='center'>21-10-2014</td>
								<td width='' class='left'>Bercerita tentang keadaan orang tua.</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>0910129</td>
								<td width='' class='left'>Asep Rahman</td>
								<td width='' class='center'>19-10-2014</td>
								<td width='' class='left'>Ketahuan guru membawa rokok ke sekolah.</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>0910121</td>
								<td width='' class='left'>Siswo Handoko</td>
								<td width='' class='center'>30-10-2014</td>
								<td width='' class='left'>Mendapat penghargaan kalpataru.</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='center'>0910234</td>
								<td width='' class='left'>Hasan Mustofa</td>
								<td width='' class='center'>21-10-2014</td>
								<td width='' class='left'>Juara 1 tingkat Kabupaten, lomba melukis.</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 5</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
