<h1 class="grid_12">Pengelolaan Sekolah</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Uraian Tugas (Job Description)</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-plus"></i>Tambah</a> 
			  <br/><br/>
            </div>
       </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Posisi Jabatan</th>
								<th>Uraian Tugas (Job Description)</th>
								<th>Aksi</th>
						
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='left'>Wakasek Kurikulum</td>
								<td width='' class='left'></td>
							
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-edit"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='left'>Wakasek Sapras</td>
								<td width='' class='left'></td>
							
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-edit"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='left'>Wakasek Kesiswaan</td>
								<td width='' class='left'></td>
							
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-edit"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='left'>Wakasek Humas</td>
								<td width='' class='left'></td>
							
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-edit"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 4</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
														<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
