<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Grafik Rekap Penerima Beasiswa</h2>
       </div>
	   	<div class="box">
				
					<div class="content" style="height: 350px;">
						<table class=chart data-type=bars>
							<thead>
								<tr>
									<th></th>
									<th>Sampoerna</th>
									<th>Djarum</th>
									<th>Dikti</th>
									<th>Pemerintah</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th>Jumlah Siswa</th>
									<td>50</td>
									<td>10</td>
									<td>28</td>
									<td>30</td>
								</tr>
							</tbody>	
						</table>
					</div><!-- End of .content -->
					
				</div><!-- End of .box -->		
        <div class="content">
		
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=2>No.</th>
								<th rowspan=2>Sumber</th>
								<th colspan=6>Jumlah Siswa</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30px' class='center'>1</td>
								<td width='50px'>Sampoerna</td>
								<td width='50px' class='center'>50</td>
							</tr>
							<tr>
								<td width='30px' class='center'>2</td>
								<td width='50px'>Djarum</td>
								<td width='50px' class='center'>10</td>
							</tr>
							<tr>
								<td width='30px' class='center'>3</td>
								<td width='50px'>Dikti</td>
								<td width='50px' class='center'>28</td>
							</tr>
							<tr>
								<td width='30px' class='center'>4</td>
								<td width='50px'>Pemerintah</td>
								<td width='50px' class='center'>30</td>
							</tr>
						</tbody>
					</table>
					
				</div>
            </div>
        </div>        
    </div>
 </div>
