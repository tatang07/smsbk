<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>
<div class="grid_12">
    <div class="box with-table">
        <div class="header">
            <h2>Pengkelasan Siswa Baru</h2>
        </div>
	
		<div class="content">
            <div id="datatable" url="">            	
				<div class="dataTables_wrapper" role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan="2">Kelas</th>
								<th rowspan="2">Prioritas</th>
								<th colspan="2">Daya Tampung</th>
								<th rowspan="2">Kelas Unggulan</th>
							</tr>
							<tr>
								<th>L</th>
								<th>P</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td align="center">X-1</td>
								<td width="50px"><input type="text" value="1" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>					
								<td align="center"><input type="checkbox"</td>												
							</tr>
							<tr>
								<td align="center">X-2</td>
								<td width="50px"><input type="text" value="2" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>						
								<td align="center"><input type="checkbox"</td>												
							</tr>
							<tr>
								<td align="center">X-3</td>
								<td width="50px"><input type="text" value="3" style="height:10px;"/></td>								
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>						
								<td align="center"><input type="checkbox"</td>													
							</tr>
							<tr>
								<td align="center">X-4</td>
								<td width="50px"><input type="text" value="4" style="height:10px;"/></td>								
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>						
								<td align="center"><input type="checkbox"</td>													
							</tr>
							<tr>
								<td align="center">X-5</td>
								<td width="50px"><input type="text" value="5" style="height:10px;"/></td>								
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>						
								<td align="center"><input type="checkbox"</td>													
							</tr>
							<tr>
								<td align="center">X-6</td>
								<td width="50px"><input type="text" value="6" style="height:10px;"/></td>								
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>						
								<td align="center"><input type="checkbox"</td>													
							</tr>
							<tr>
								<td align="center">X-7</td>
								<td width="50px"><input type="text" value="7" style="height:10px;"/></td>								
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>					
								<td align="center"><input type="checkbox"</td>													
							</tr>
							<tr>
								<td align="center">X-8</td>
								<td width="50px"><input type="text" value="8" style="height:10px;"/></td>								
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>						
								<td align="center"><input type="checkbox"</td>													
							</tr>
							<tr>
								<td align="center">X-9</td>
								<td width="50px"><input type="text" value="9" style="height:10px;"/></td>								
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>						
								<td align="center"><input type="checkbox"</td>												
							</tr>
							<tr>
								<td align="center">X-10</td>
								<td width="50px"><input type="text" value="10" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>							
								<td width="50px"><input type="text" value="0" style="height:10px;"/></td>					
								<td align="center"><input type="checkbox"</td>												
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </div>

    <div class="actions">
        <div class="left">
            <input type="submit" value="Lakukan Pengkelasan" />
        </div>
    </div>
 </div>
