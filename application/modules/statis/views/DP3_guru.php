<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar Penilaian Pelaksanaan Pekerjaan Pegawai Negeri Sipil (DPPP)</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-plus"></i>Tambah</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<th width=80px align='left'><span class="text">Guru</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>Achmad Suhadi, S.Pd</option>
									<option value=''>Agus Riyanto, S.Pd</option>
									<option value=''>Diana Susanti, S.Si</option>
									<option value=''>Enah Suhaenah, S.Pd</option>
									<option value=''>Lilit Sutarsih, S.Pd</option>
									<option value=''>Maman Rachman, S.Pd</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Periode Penilaian Penilaian</th>
								<th>Penilai</th>
								<th>Aksi</th>
						
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>Januari/2007 - Desember / 2007 </td>
								<td width='' class='center'>Dra. Hj. SULASTRI DJ, M.Pd</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s" href="<?php echo site_url('statis/DP3_guru_detail')?>"><i class="icon-remove"></i> Detail</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							
							</tr>

							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>Januari/2008 - Desember / 2008 </td>
								<td width='' class='center'>Dra. Hj. SULASTRI DJ, M.Pd</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s" href="<?php echo site_url('statis/DP3_guru_detail')?>"><i class="icon-remove"></i> Detail</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 2</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
