<h1 class="grid_12">Pelayanan Dan Pengembangan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar Siswa Berprestasi</h2>
			
       </div>
	
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="40px" align="left"><span class="text">Nama</span></th>
								<td align="left"><input type="text" name="nama" id="nama"></td>
								<td width="10px"></td>
								
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Nis</th>
								<th >Nama</th>
								<th >Prestasi</th>
								<th >Kategori</th>
								<th >Perlombaan</th>
								<th >Tingkat</th>
								<th >Tanggal</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
						
						
								<tr>
									<td class='center'>
									1
									</td>
									<td class='center'>
									1203524
									</td class='center'>
									<td class='center'>
									Sundari Sukoco
									</td>
									<td class='center'>
									Badminton
									</td>
									<td class='center'>
									Putra
									</td>
									<td class='center'>
									Badminton
									</td>
									<td class='center'>
									Jawa Barat
									</td>
									<td class='center'>
									2005-12-01
									</td>
									<td class='center'>
									<a href="" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
								<tr>
									<td class='center'>
									2
									</td>
									<td class='center'>
									1203523
									</td class='center'>
									<td class='center'>
									Mandala Ajie
									</td>
									<td class='center'>
									Sepak Bola
									</td>
									<td class='center'>
									Putra
									</td>
									<td class='center'>
									Sepak Bola
									</td>
									<td class='center'>
									Jawa Barat
									</td>
									<td class='center'>
									2005-11-04
									</td>
									<td class='center'>
									<a href="" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
								<tr>
									<td class='center'>
									3
									</td>
									<td class='center'>
									1203521
									</td class='center'>
									<td class='center'>
									Sasa salsabila
									</td>
									<td class='center'>
									Dansa
									</td>
									<td class='center'>
									Putri
									</td>
									<td class='center'>
									Dansa
									</td>
									<td class='center'>
									Jawa Barat
									</td>
									<td class='center'>
									2005-11-04
									</td>
									<td class='center'>
									<a href="" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : 3 </div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>