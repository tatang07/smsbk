<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar Calon Siswa Baru Tahun Ajaran 2014/2015</h2>
       </div>
       <div class="tabletools">
			<div class="right">			  	
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                
					<table>
						<tr>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<th width=100px align='left'><span class="text">Tahun Ajaran</span></th>
										<td width=100px align='left'>
											<select>
												<option value=''>2014/2015</option>										
												<option value=''>2013/2014</option>										
												<option value=''>2012/2013</option>										
												<option value=''>2011/2012</option>										
												<option value=''>2010/2011</option>										
												<option value=''>2009/2010</option>										
											</select>
										</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
						  </tr>
						  <tr>
						  <td width=1px>&nbsp;</td>
						  <td width=1px>&nbsp;</td>
						  <th width=30px align='left'><span class="text">Daya Tampung</span></th>	
							<td width=100px align='left'><input type='text'></td>	
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
						  </tr>
						  <tr>
						  <td width=1px>&nbsp;</td>
						  <td width=1px>&nbsp;</td>
							<th width=30px align='left'><span class="text">Batas Kelulusan</span></th>
							<td width=100px align='left'><input type='text'></td>	
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
						  </tr>
					</table>                
            </div>            			
        </div>
			<td><a class="button " data-gravity="s">Tampil</a></td>
    </div>										
</div>
<div class="grid_12">
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">   
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>No. Pendaftaran</th>
								<th>Nama</th>								
								<th>L/P</th>		
								<th>Tempat / Tanggal Lahir</th>		
								<th>Asal Sekolah</th>		
								<th>Nilai UN</th>		
								<th>Aksi</th>						
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>								
								<td width='' class='center'>A09877656</td>								
								<td width='' class='center'>Ghifari</td>								
								<td width='' class='center'>L</td>
								<td width='' class='center'>Cirebon 11 Januari 1997</td>
								<td width='' class='center'>SMP N 1 Cirebon</td>
								<td width='' class='center'>89</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>						
						</tbody>
					</table>					
				</div>
            </div>
        </div>        
    </div>
 </div>