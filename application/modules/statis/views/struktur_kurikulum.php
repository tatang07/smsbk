<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
	<div class="box with-table">
		<div class="header">
			<h2>Struktur Kurikulum</h2>
		</div>
		<div class="tabletools">
			<div class="right">
				<a href=""><i class="icon-pencil"></i>Edit</a> 
				<br/><br/>
			</div>
			
			<div class="dataTables_filter">
				<form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=80px align='left'><span class="text">Jenis Kurikulum</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>Kurikulum 2013</option>
									<option value=''>Kurikulum KSTP</option>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>

		</div>
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th colspan="3">KOMPETENSI INTI</th>
							</tr>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Deskripsi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>KI1</td>
								<td>Menghayati dan mengamalkan ajaran agama yang dianutnya</td>
							</tr>
							<tr>
								<td>2</td>
								<td>KI2</td>
								<td>Menghayati dan mengamalkan perilaku jujur, disiplin, tanggungjawab, peduli (gotong royong, kerjasama, toleran, damai), santun, responsif dan pro -aktif dan menunjukkan sikap sebagai bagian dari solusi atas berbagai permasalahan dalam berinteraksi secara efektif dengan lingkungan sosial dan alam serta dalam menempatkan diri sebagai cerminan bangsa dalam pergaulan dunia.</td>
							</tr>
							<tr>
								<td>3</td>
								<td>KI3</td>
								<td>Memahami,menerapkan, menganalisis pengetahuan faktual , konseptual, prosedural berdasarkan rasa ingintahunya tentang ilmu pengetahuan, teknologi, seni, budaya, dan humaniora dengan wawasan kemanusiaan, kebangsaan, kenegaraan, dan peradaban terkait penyebab fenomena dan kejadian, serta menerapkan pengetahuan p rosedural pada bidang kajian yang spesifik sesuai dengan bakat dan minatnya untuk memecahkan masalah</td>
							</tr>
							<tr>
								<td>4</td>
								<td>KI4</td>
								<td>Mengolah, menalar, dan menyaji dalam rana h konkret dan ranah abstrak terkait dengan pengembangan dari yang dipelajarinya di sekolah secara mandiri, dan mampu menggunakan metoda sesuai kaidah keilmuan</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>        
	</div>

	<div class="box with-table">
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Mata Pelajaran</th>
								<th>Jam/Minggu</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan=4><b>KELOMPOK A (WAJIB)</b></td>
							</tr>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>A001</td>
								<td>Pendidikan Agama dan Budi Pekerti</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>A002</td>
								<td>Pendidikan Pancasila dan Kewarganegaraan</td>
								<td width='' class='center'>2</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>A003</td>
								<td>Bahasa Indonesia</td>
								<td width='' class='center'>4</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>A004</td>
								<td>Matematika</td>
								<td width='' class='center'>4</td>
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='center'>A005</td>
								<td>Sejarah Indonesia</td>
								<td width='' class='center'>2</td>
							</tr>
							<tr>
								<td width='' class='center'>6</td>
								<td width='' class='center'>A006</td>
								<td>Bahasa Inggris</td>
								<td width='' class='center'>2</td>
							</tr>
							<tr>
								<td colspan=5><b>KELOMPOK B (WAJIB)</b></td>
							</tr>
							<tr>
								<td width='' class='center'>7</td>
								<td width='' class='center'>B001</td>
								<td>Seni Budaya</td>
								<td width='' class='center'>2</td>
							</tr>
							<tr>
								<td width='' class='center'>8</td>
								<td width='' class='center'>B002</td>
								<td>Pendidikan Jasmani, Olah Raga, dan Kesehatan</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' class='center'>9</td>
								<td width='' class='center'>B003</td>
								<td>Prakarya dan Kewirausahaan</td>
								<td width='' class='center'>2</td>
							</tr>
							<tr>
								<td colspan=3>Jumlah Jam Pelajaran Kelompok A dan B per minggu</td>
								<td width='' class='center'>24</td>
							</tr>

							<tr>
								<td width='' colspan=4><b>KELOMPOK C (PEMINATAN)</b></td>
							</tr>
							<tr>
								<td width='' colspan=5><strong>Peminatan Matematika dan Ilmu Alam</strong></td>
							</tr>
							<tr>
								<td width='30px' class='center'>1</td>
								<td width='' class='center'>C101</td>
								<td width='500px'>Matematika</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>C102</td>
								<td width=''>Biologi</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>C103</td>
								<td width=''>Fisika</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>C104</td>
								<td width=''>Kimia</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' colspan=5><strong>Peminatan Ilmu-Ilmu Sosial</strong></td>
							</tr>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>C201</td>
								<td width=''>Geografi</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>C202</td>
								<td width=''>Sejarah</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>C203</td>
								<td width=''>Sosiologi</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>C204</td>
								<td width=''>Ekonomi</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' colspan=5><strong>Peminatan Ilmu-Ilmu Bahasa dan Budaya</strong></td>
							</tr>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>C301</td>
								<td width=''>Bahasa dan Sastra Indonesia</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>C302</td>
								<td width=''>Bahasa dan Sastra Inggris</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>C303</td>
								<td width=''>Bahasa dan Sastra Asing Lainnya</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>C304</td>
								<td width=''>Antropologi</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' colspan=5><strong>Mata Pelajaran Pilihan dan Pendalaman</strong></td>
							</tr>
							<tr>
								<td  width=''>1</td>
								<td  width='' class='center'>C401</td>
								<td  width=''>Pilihan Lintas Minat dan/atau Pendalaman Minat</td>
								<td  width='' class='center'>6</td>
							</tr>
							<tr>
								<td width='' colspan=3><b>Jumlah Jam Pelajaran yang Tersedia per Minggu</b></td>
								<td width='' class='center'>66</td>
							</tr>
							<tr>
								<td width='' colspan=3><b>Jumlah Jam Pelajaran yang Harus Ditempuh per Minggu</b></td>
								<td width='' class='center'>42</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>        
	</div>
</div>