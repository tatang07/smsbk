<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>
<div class="grid_12">
    <div class="box with-table">
        <div class="header">
        	<div class="right" style="float:right;padding-right:10px;padding-top:9px;">
			  	<a href=""><i class="icon-print"></i> Cetak</a> 
            </div>
            <h2>Seleksi dan Penerimaan Siswa Baru Tahun Ajaran 2014/2015</h2>
        </div>
    </div>

    <div class="box with-table">
		<div class="content" style="padding-left:20px;padding-right:20px;padding-top:10px;">
			<h3>
				Jumlah Siswa Pendaftar <b>2</b> Siswa
			</h3>
			<h3>
				Jumlah Siswa Yang Tidak Diterima <b>0</b> Siswa
			</h3>
			<h3>
				Jumlah Siswa Yang Diterima <b>2</b> Siswa
			</h3>
		</div>
	</div>

	<div class="box with-table">
    	<div class="header">
            <h2>Formulir Pendaftaran Siswa Baru Tahun Ajaran 2014/2015</h2>
        </div>

		<div class="content">
            <div id="datatable" url="">            	
				<div class="dataTables_wrapper" role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No</th>
								<th>No Pendaftaran</th>
								<th>Nama</th>
								<th>L/P</th>
								<th>Tempat / Tanggal Lahir</th>
								<th>Asal Sekolah</th>
								<th>Nilai UN</th>
							</tr>
						</thead>

						<tbody>
							<tr>
								<td>1</td>
								<td>0890761536</td>							
								<td>Ahmad Zaenuddin</td>							
								<td>L</td>							
								<td>Bandung, 17 Juni 1996</td>							
								<td>SMPN 1 Garut</td>							
								<td>95</td>						
							</tr>
							<tr>
								<td>2</td>
								<td>08906153928</td>							
								<td>Ardian August Rachman</td>							
								<td>L</td>							
								<td>Garut, 23 Agustus 1995</td>							
								<td>SMPN 1 Sumedang</td>							
								<td>80</td>						
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </div>
 </div>
