<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekap Kelulusan</h2>
       </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=2>Tahun</th>
								<th colspan=2>Lulus</th>															
								<th rowspan=4>Rata-Rata NEM</th>
								<th rowspan=4>Aksi</th>
								
							</tr>
							<tr>
								<th>Jumlah</th>
								<th>%</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>2010</td>
								<td width='' class='center'>100</td>
								<td width='' class='center'>90%</td>							
								<td width='' class='center'>89</td>							
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>						
							</tr>
							<tr>
								<td width='' class='center'>2011</td>
								<td width='' class='center'>140</td>
								<td width='' class='center'>95%</td>							
								<td width='' class='center'>90</td>							
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>						
							</tr>
							<tr>
								<td width='' class='center'>2012</td>
								<td width='' class='center'>189</td>
								<td width='' class='center'>100%</td>							
								<td width='' class='center'>80</td>							
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>						
							</tr>							
							<tr>
								<td width='' class='center'>2013</td>
								<td width='' class='center'>167</td>
								<td width='' class='center'>95%</td>							
								<td width='' class='center'>95</td>							
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>						
							</tr>							
							<tr>
								<td width='' class='center'>2014</td>
								<td width='' class='center'>198</td>
								<td width='' class='center'>90%</td>							
								<td width='' class='center'>90</td>							
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>						
							</tr>							
							<tr>
								<td width='' class='center'>2015</td>
								<td width='' class='center'>200</td>
								<td width='' class='center'>100%</td>							
								<td width='' class='center'>87</td>							
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>						
							</tr>														
						</tbody>
					</table>
					
				</div>
            </div>
        </div>        
    </div>
 </div>
