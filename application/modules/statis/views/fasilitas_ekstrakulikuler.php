<h1 class="grid_12">Ekstrakulikuler</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Fasilitas Ekstrakulikuler</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-plus"></i>Tambah</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
						
							<th width=1px align='left'><span class="text">Ekstrakulikuler:</span></th>
							<td width=100px align='left'>
								<select>
									<option value=''>Bulutangkis</option>
									<option value=''>Futsal</option>
									<option value=''>Pramuka</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=2>No.</th>
								<th rowspan=2>Fasilitas</th>
								<th colspan=4>Jumlah</th>
								<th rowspan=2>aksi</th>
							</tr>
							<tr>
								<th>Baik</th>
								<th>Rusak Ringan</th>
								<th>Rusak Berat</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>Lapang Bulutangkis</td>
								<td width='' class='center'>2</td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'>2</td>
								
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>Net</td>
								<td width='' class='center'>2</td>
								<td width='' class='center'>1</td>
								<td width='' class='center'>1</td>
								<td width='' class='center'>4</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
								
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>Raket</td>
								<td width='' class='center'>10</td>
								<td width='' class='center'>8</td>
								<td width='' class='center'>1</td>
								<td width='' class='center'>19</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
								
								
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 3</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
