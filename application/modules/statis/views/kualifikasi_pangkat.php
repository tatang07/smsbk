<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Proporsi Golongan/Pangkat</h2>
       </div>
	   <div class="box">
	   <div class="content" style="height: 250px;">
						<table class=chart data-type=pie data-donut=0>
							<thead>
								<tr>
									<th></th>
									<th>Ia</th>
									<th>IIIa</th>
									<th>IIc</th>
									<th>IIIb</th>
									<th>IIIc</th>
									<th>IIId</th>
									<th>IVa</th>
									<th>IVb</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th>Ia</th>
									<td>1</td>
								</tr>	
								<tr>
									<th>IIIa</th>
									<td>6</td>
								</tr>
								<tr>
									<th>IIc</th>
									<td>1</td>
								</tr>
								<tr>
									<th>IIIb</th>
									<td>1</td>
								</tr>
								<tr>
									<th>IIIc</th>
									<td>3</td>
								</tr>
								<tr>
									<th>IIId</th>
									<td>2</td>
								</tr>
								<tr>
									<th>IVa</th>
									<td>8</td>
								</tr>	
								<tr>
									<th>IVb</th>
									<td>1</td>
								</tr>								
							</tbody>	
						</table>
					</div><!-- End of .content -->
				</div><!-- End of .box -->
	   
	   
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Golongan Guru</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>Ia</td>
								<td width='' class='center'>1</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>IIIa</td>
								<td width='' class='center'>6</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>IIc</td>
								<td width='' class='center'>1</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>IIIb</td>
								<td width='' class='center'>1</td>
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='center'>IIIc</td>
								<td width='' class='center'>3</td>
							</tr>						
							<tr>
								<td width='' class='center'>6</td>
								<td width='' class='center'>IIId</td>
								<td width='' class='center'>2</td>
							</tr>							
							<tr>
								<td width='' class='center'>7</td>
								<td width='' class='center'>IVa</td>
								<td width='' class='center'>8</td>
							</tr>							
							<tr>
								<td width='' class='center'>8</td>
								<td width='' class='center'>IVb</td>
								<td width='' class='center'>1</td>
							</tr>							
						</tbody>
					</table>
					
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
