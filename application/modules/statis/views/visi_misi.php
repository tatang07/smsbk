<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
	<div class="box with-table">
		<div class="header">
			<h2>Visi dan Misi</h2>
		</div>
	</div>
	<div class="box with-table">

		<div class="header">
			<h2>Visi</h2>
		</div>

		<div class="content">
			<div class="tabletools">
				<div class="right">
					<a href=""><i class="icon-plus"></i>Tambah</a> 
				  <br/><br/>
				</div>
			</div>
			
			<div id="datatable" url="">
				<div class="dataTables_wrapper" role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Visi</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30px' class='center'>1</td>
								<td width='600px'>Optimistic to be religious champions whith strong Nationalism, Social Awareness, Multiple intelligence and High Technology to fly globalThe House of Champions </td>
								
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="box with-table">
		<div class="header">
			<h2>Misi</h2>
		</div>
		<div class="content">
			<div class="tabletools">
				<div class="right">
					<a href=""><i class="icon-plus"></i>Tambah</a> 
				  <br/><br/>
				</div>
			</div>
			<div id="datatable" url="">
				<div class="dataTables_wrapper" role="grid"> 
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Misi</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30px' class='center'>1</td>
								<td width='600px'>To create a long life religious atmosphere with strong nationalism.</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='30px' class='center'>2</td>
								<td width='600px'>To optimalize intelligences through various academic and non academic activities gradually.</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='30px' class='center'>3</td>
								<td width='600px'>To establish global acc</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>
