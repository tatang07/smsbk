<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2><a href="<?php echo site_url('statis/informasi_daftar'); ?>">Informasi Pendaftaran Tahun Ajaran 2014/2015</a> &bull; </h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon"></i>Ubah Informasi</a> 
			  <br/><br/>
            </div>
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>Pendaftaran dilakukan pada:: </td>
								<td >22 s.d. 29 Juli 2014.</td>
							</tr>
							<tr>
								<td width='' class='center'></td>
								<td >Untuk PPDB Bandung tahun pelajaran 2014/2015 ada beberapa perubahan yang perlu diperhatikan oleh orang tua/wali calon peserta didik, antara lain :

								<li>Seleksi akademis menggunakan nilai Ujian Nasional (UN)</li>
								<li>Calon Peserta Didik tamatan sekolah di Bandung tetapi bukan warga Kota Bandung (untuk selanjutnya disebut Rekomendasi Dalam Kota), diberi kesempatan untuk sekolah di Bandung dengan pagu 1 % (satu prosen) baik dari pagu kota maupun pagu sekolah.</li>
								<li>Calon Peserta Didik tamatan sekolah Luar Kota tetapi warga Kota Bandung (untuk selanjutnya disebut Mutasi) tidak dikenakan pagu 1 % (satu persen) .</li>

								PPDB/PSB dengan konsep tanpa  kertas ini masih dijadikan basis pelayanan dan terus ditingkatkan, sehingga diharapkan dapat memberikan kemudahan bagi siswa  dan  orang tua (masyarakat) dalam melakukan pendaftaran. Masyarakat dapat melakukan pendaftaran di rumah, di sekolah-sekolah negeri yang siap membantu, di semua  lokasi yang memiliki akses internet, dimana saja sesuai dengan jangka waktu yang ditentukan. Hal ini disebabkan karena konsep yang digunakan dalam PPDB adalah  berbasis WEB.
								Meskipun demikian, PPDB yang berbasis WEB ini masih dimungkinkan menimbulkan kekhawatiran Orang Tua/wali dan masyarakat tentang sulitnya melakukan pendaftaran, maka Dinas Pendidikan Kota Surabaya melakukan berbagai upaya agar kekhawatiran itu  dapat diminimalisir, antara lain :

								<li>Menyederhanakan cara pendaftaran lewat internet</li>
								<li>Semua calon siswa yang berasal dari SD/SDLB/MI/SMP/SMPLB/MTs sebanyak lebih kurang 80.000 calon siswa diberikan panduan bagaimana cara mendaftar</li>
								<li>Bekerjasama dengan berbagai pihak dalam sarana prasarana,  sosialisasi,  maupun membantu pendaftaran</li>
								<li>Memberi kesempatan untuk mencoba (latihan pendaftaran) sebanyak 4 (empat) kali.</li>
								<li>Menyediakan tenaga-tenaga  (helper) yang siap membantu calon peserta didik untuk melakukan pendaftaran baik disekolah-sekolah, mobil keliling, dan tempat-tempat strategis.</li>
								</td>
							</tr>
							
						</tbody>
					</table>
					<div class="footer">
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
