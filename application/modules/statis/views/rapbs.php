<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rencana Anggaran Pendapatan dan Belanja Sekolah (RAPBS) </h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-print"></i>Cetak File</a> 
			  	<a href=""><i class="icon-time"></i>Program Kerja Tahunan</a> 
			  	<a href=""><i class="icon-file"></i>Rencana Kerja Sekolah</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=100px align='left'><span class="text">Tahun Ajaran:</span></th>
							<td width=100px align='left'>
								<select>
									<option value=''>2009/2010</option>
									<option value=''>2010/2011</option>
									<option value=''>2011/2012</option>
									<option value=''>2012/2013</option>
									<option value=''>2013/2014</option>
									<option value=''>2014/2015</option>
								</select>
							</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Program Kerja</th>
								<th>Periode</th>
								<th>Aksi Dana</th>
								<th>Realisasi</th>
								<th>Keterangan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width=''>Administrasi</td>
								<td width=''>Januari 2010-Desember 2010</td>
								<td width='50px'>Rp. 0,-</td>
								<td width=''>100%</td>
								<td width=''>Memantapkan Organisasi Manajemen</td>
								<td width='50px' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width=''>Kurikulum</td>
								<td width=''>0-0</td>
								<td width=''>Rp. 0,-</td>
								<td width=''>100%</td>
								<td width=''>Meningkatkan Mutu Pendidikan</td>
								
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width=''>Ketatausahaan</td>
								<td width=''>0-0</td>
								<td width=''>Rp. 0,-</td>
								<td width=''>100%</td>
								<td width=''>Peningkatan Pelayanan Tata Usaha</td>
								
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width=''>Kesiswaan</td>
								<td width=''>0-0</td>
								<td width=''>Rp. 0,-</td>
								<td width=''>100%</td>
								<td width=''>Penertiban dan Peningkatan OSIS, Pramuka, PMR, Pencinta Alam, Olah Raga Prestasi, Majelis Ta'lim, Merpati Putih, Kesenian.</td>
								
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width=''>Sarana dan Prasarana</td>
								<td width=''>0-0</td>
								<td width=''>Rp. 0,-</td>
								<td width=''>100%</td>
								<td width=''>Pemenuhan Sarana Dan Prasarana</td>
								
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>6</td>
								<td width=''>Humas</td>
								<td width=''>0-0</td>
								<td width=''>Rp. 0,-</td>
								<td width=''>100%</td>
								<td width=''>Meningkatkan Hubungan Dengan Masyarakat</td>
								
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>7</td>
								<td width=''>BP /BK</td>
								<td width=''>0-0</td>
								<td width=''>Rp. 0,-</td>
								<td width=''>100%</td>
								<td width=''>Memberikan Bimbingan Kepada Siswa.</td>
								
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								</td>
							</tr>
							<tr>
								<td width='' colspan=2>Total</td>
								<td width=''></td>
								<td width=''>Rp. 0,-</td>
								<td width='' colspan=3></td>
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 7</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
