<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar Identitas Pendidik</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-plus"></i>Tambah</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=1px align='left'><span class="text">Kriteria:</span></th>
							<td width=100px align='left'>
								<select>
									<option value=''>NIP</option>
									<option value=''>Kode Guru</option>
									<option value=''>Guru Tidak Tetap</option>
									<option value=''>Guru Tidak Tetap</option>
								
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>	
							<th width=80px align='left'><span class="text">Kata Kunci</span></th>
							<td width=200px align='left'>
								<input type="text" name="keyword">

								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama</th>
								<th>Jenis Kelamin</th>
								<th>Tempat,Tanggal Lahir</th>
								<th>Pangkat/Gol</th>
								<th>Telepon</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30px' class='center'>1</td>
								<td width='300px' class='center'>ACHMAD SUHADI, S.Pd. <br>NIP : 131286228</td>
								<td width='50px' class='center'>Laki-laki</td>
								<td width='50px' class='center'>Rembang,10/10/1993</td>
								<td width='50px' class='center'>Pembina/IVA</td>
								<td width='50px' class='center'>09876857464</td>
								<td width='200px' class='center'>
								<a href="<?php echo base_url('statis/det_identitas_tenaga_pendidik');?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
										<tr>
								<td width='30px' class='center'>2</td>
								<td width='300px' class='center'>AGUS RIYANTO,S.Pd. <br>NIP : 196308021988031006</td>
								<td width='50px' class='center'>Laki-laki</td>
								<td width='50px' class='center'>Rembang,10/11/1993</td>
								<td width='50px' class='center'>Pembina/IIA</td>
								<td width='50px' class='center'>09876857464</td>
								<td width='' class='center'>
								<a href="<?php echo base_url('statis/det_identitas_tenaga_pendidik');?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='30px' class='center'>3</td>
								<td width='300px' class='center'>Diana Susanti, S.Si. <br>NIP : 198003102008012009</td>
								<td width='50px' class='center'>Perempuan</td>
								<td width='50px' class='center'>Rembang,10/10/1990</td>
								<td width='50px' class='center'>Pembina/IIIB</td>
								<td width='50px' class='center'>09876857464</td>
								<td width='' class='center'>
								<a href="<?php echo base_url('statis/det_identitas_tenaga_pendidik');?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 3</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
