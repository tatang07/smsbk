<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       	<div class="box">
				
					<div class="header">
						<h2>Kompetensi Guru</h2>
					</div>
					
					<div class="accordion toggle">
					
						<h3><a href="#">Kompetensi Pedagogik</a></h3>
						<div>
							<ul>							
								<li>Menguasai karakteristik peserta didik dari aspek fisik, moral, sosial, kultural, emosional, dan intelektual</li>
								<li>Menguasai teori belajar dan prinsip-prinsip pembelajaran yang mendidik</li>
								<li>Mengembangkan kurikulum yang terkait dengan mata pelajaran yang diampu</li>
								<li>Menyelenggarakan pembelajaran yang mendidik</li>
								<li>Memanfaatkan teknologi informasi dan komunikasi untuk kepentingan pembelajaran</li>
								<li>Memfasilitasi pengembangan potensi peserta didik untuk mengaktulisasikan berbagai potensi yang dimiliki</li>
								<li>Berkomunikasi secara efektif, empatik, dan santun dengan pesertad didik</li>
								<li>Menyelenggarakan penilaian evaluasi proses dan hasil belajar</li>
								<li>Memanfaatkan hasil penilaian dan evaluasi untk kepentingan pembelajaran</li>
								<li>Melakukan tindakan reflektif untuk peningkatan kualitas pembelajaran</li>
							</ul>
						</div>
						
						<h3><a href="#">Kompetensi Kepribadian</a></h3>
						<div>
							<ul>							
								<li>Bertindak sesuai dengan norma agama, hukum, sosial dan kebudayaan nasional Indonesia</li>
   							 	<li>Menampilkan diri sebagai pribadi yang jujur, berkahlak mulia, dan teladan abgi peserta didik dan masyarakat</li>
 							    <li>Menampilkan diri sebagai pribadi yang mantap, stabil, dewasa, arif dan berwibawa</li>
 							    <li>Menunjukkan etos kerja, tanggung jawab yang tinggi, rasa bangga menjadi guru, dan rasa percaya diri</li>
 							    <li>Menjunjung tinggi kode etik profesi guru</li>

							</ul>
						</div>
						
						<h3><a href="#">Kompetensi Sosial</a></h3>
						<div>
							<ul>							
								<li>Bersikap inklusif, bertindak objektif, serta tidak diskriminatif karena pertimbangan jenis kelamin, agama, ras, kondisi fisik, latar belakang keluarga, dan status sosial ekonomi</li>
    							<li>Berkomunikasi secara efektif,empatif,dan santun dengan sesama pendidik, tenaga kependidikan, orang tua, dan masyarakat</li>
    							<li>Beradaptasi di tempat bertugas di seluruh wilayah Republik Indonesia yang memiliki keragaman sosial budaya</li>
								<li>Berkomunikasi dengan komunikasi profesi sendiri dan profesi lain secara lisan dan tulisan atau bentuk lain</li>
							</ul>
						</div>
						
						<h3><a href="#">Kompetensi Profesional</a></h3>
						<div>
							<ul>
							
								<li>Menguasai materi, struktur, konsep, dan pola pikir keilmuan yang mendukung mata pelajaran yang diampu</li>
    							<li>Menguasai standar kompetensi dan kompetensi dasar mata pelajaran yang diampu</li>
    							<li>Mengembangkan materi pembelajaran yang diampu secara kreatif</li>
    							<li>Mengembangkan keprofesionalan secara bekelanjutan dengan melakukan tindakan reflektif</li>
    							<li>Memanfaatkan teknologi informasi dan komunikasi untuk mengembangkan diri</li>
							</ul>
						</div>						
					</div><!-- End of .content -->
					
				</div><!-- End of .box -->		
        </div>
 </div>
