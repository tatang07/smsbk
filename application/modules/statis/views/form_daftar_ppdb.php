<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
		<div class="header">
		    <h2>Seleksi dan Penerimaan Siswa Baru Tahun Ajaran 2013/2014</h2>
		</div>
		<div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">
					<table class='styled'>
						<tr>
							<td colspan='2'><b>Formulir Pendaftaran Siswa Baru</b></td>
						</tr>
						<tr>
							<td width='150px'>Nama Siswa</td>
							<td><input type="text" style='magrin-left:0px;width:98%;'/></td>
						</tr>
						<tr>
							<td width='150px'>Tempat Lahir</td>
							<td><input type="text" style='magrin-left:0px;width:98%;'/></td>
						</tr>
						<tr>
							<td width='150px'>Tanggal Lahir</td>
							<td><input type="date" style='magrin-left:0px;width:98%;'/></td>
						</tr>
						<tr>
							<td width='150px'>Jenis Kelamin</td>
							<td>
								<select>
									<option value="Laki-laki">Laki-laki</option> 
									<option value="Perempuan">Perempuan</option> 
								</select>
							</td>
						</tr>
						<tr>
							<td width='150px'>Agama</td>
							<td>
								<select>
									<option value="Islam">Islam</option> 
									<option value="Kristen Protestan">Kristen Protestan</option> 
									<option value="Kristen Katolik">Kristen Katolik</option> 
									<option value="Hindu">Hindu</option> 
									<option value="Budha">Budha</option> 
									<option value="Konghucu">Konghucu</option> 
								</select>
							</td>
						</tr>
						<tr>
							<td width='150px'>Golongan Darah</td>
							<td>
								<select>
									<option value="A">A</option> 
									<option value="B">B</option> 
									<option value="AB">AB</option> 
									<option value="O">O</option> 
								</select>
							</td>
						</tr>
						<tr>
							<td width='150px'>Anak Ke</td>
							<td><input data-type="spinner" min=1 max=20 value=1 /></td>
						</tr>
						<tr>
							<td width='150px'>Jumlah Saudara</td>
							<td><input data-type="spinner" min=1 max=20 value=1 /></td>
						</tr>
						<tr>
							<td width='150px'>Status Anak</td>
							<td>
								<select>
									<option value="Kandung">Kandung</option> 
									<option value="Tiri">Tiri</option> 
								</select>
							</td>
						</tr>
						<tr>
							<td width='150px'>Alamat</td>
							<td><textarea style='magrin-left:0px;width:98%;'></textarea></td>
						</tr>
						<tr>
							<td width='150px'>Kecamatan</td>
							<td>
								<select class="search" data-placeholder="Choose a Name">
									<option value="INDRAMAYU">INDRAMAYU</option> 
									<option value="SUKRA">SUKRA</option> 
									<option value="LOSARANG">LOSARANG</option> 
									<option value="KANDANGHAUR">KANDANGHAUR</option> 
									<option value="BALONGAN">BALONGAN</option> 
									<option value="ANJATAN">ANJATAN</option> 
									<option value="BONGAS">BONGAS</option> 
									<option value="JATIBARANG">JATIBARANG</option> 
								</select>
							</td>
						</tr>
						<tr>
							<td width='150px'>Kabupaten/Kota</td>
							<td>
								<select class="search" data-placeholder="Choose a Name">
									<option value="ACEH BARAT">ACEH BARAT</option> 
									<option value="KOTA BANDUNG">KOTA BANDUNG</option> 
									<option value="BEKASI">BEKASI</option> 
									<option value="BELITUNG">BELITUNG</option> 
									<option value="SERANG">SERANG</option> 
									<option value="CILEGON">CILEGON</option> 
									<option value="KAB.BANDUNG">KAB.BANDUNG</option> 
									<option value="DEPOK">DEPOK</option> 
								</select>
							</td>
						</tr>
						<tr>
							<td width='150px'>Provinsi</td>
							<td>
								<select class="search" data-placeholder="Choose a Name">
									<option value="BANTEN">BANTEN</option> 
									<option value="DKI JAKARTA">DKI JAKARTA</option> 
									<option value="JAWA BARAT">JAWA BARAT</option> 
									<option value="BALI">BALI</option> 
									<option value="ACEH">ACEH</option> 
									<option value="JAMBI">JAMBI</option> 
									<option value="JAWA TIMUR">JAWA TIMUR</option> 
									<option value="PAPUA BARAT">PAPUA BARAT</option> 
								</select>
							</td>
						</tr>
						<tr>
							<td width='150px'>Kode Pos</td>
							<td><input type="text" style='magrin-left:0px;width:98%;'/></td>
						</tr>
						<tr>
							<td width='150px'>No. Telepon</td>
							<td><input type="text" style='magrin-left:0px;width:98%;'/></td>
						</tr>
						<tr>
							<td width='150px'>Asal Sekolah</td>
							<td><input type="text" style='magrin-left:0px;width:98%;'/></td>
						</tr>
						<tr>
							<td width='150px'>Foto</td>
							<td><input type="file"/></td>
						</tr>
					<!-- field gitu di sini -->
						<tr>
							<td colspan='2'><b>Nilai Ujian Nasional</b></td>
						</tr>
						<tr>
							<td width='150px'>Matematika</td>
							<td><input type="text" style='magrin-left:0px;width:98%;'/></td>
						</tr>
						<tr>
							<td colspan='2'><b>Persyaratan siswa baru yang harus dilengkapi</b></td>
						</tr>
						<tr>
							<td width='150px'>STK/Surat Tanda Kelulusan</td>
							<td><input type="file"/></td>
						</tr>
					</table>


					<div style='margin:20px;'>
						<button>Simpan</button>
					</div>
				</div> <!-- end of datatable wrapper -->
            </div>  <!-- end of datatable -->
        </div> <!--  end of content -->
    </div>
 </div>
