<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekapitulasi Pekerjaan Orang Tua Siswa</h2>
       </div>
       <div class="tabletools">
			<div class="content" style="height: 250px;">
				<table class=chart data-type=pie >
					<thead>
						<tr>
							<th>Jenis Pekerjaan</th>
							<th>Jumlah</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<th class='left'>Pedagang</th>
							<td class='center'>10</td>
						</tr>
						<tr>
							<th class='left'>Pegawai BUMN</th>
							<td class='center'>11</td>
						</tr>
						<tr>
							<th class='left'>Pegawai BUMS</th>
							<td class='center'>23</td>
						</tr>
						<tr>
							<th class='left'>PNS</th>
							<td class='center'>12</td>
						</tr>
						<tr>
							<th class='left'>POLRI</th>
							<td class='center'>3</td>
						</tr>
						<tr>
							<th class='left'>Petani</th>
							<td class='center'>45</td>
						</tr>
						<tr>
							<th class='left'>Wirausaha</th>
							<td class='center'>67</td>
						</tr>
						<tr>
							<th class='left'>Jumlah</th>
							<td class='center'>171</td>
						</tr>
					</tbody>
				</table>
			</div><!-- End of .content -->
				
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jenis Pekerjaan</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30' class='center'>1</td>
								<td width='500' class='left'>Pedagang</td>
								<td width='50' class='center'>10</td>
							</tr>
							<tr>
								<td width='30' class='center'>2</td>
								<td width='500' class='left'>Pegawai BUMN</td>
								<td width='50' class='center'>11</td>
							</tr>
							<tr>
								<td width='30' class='center'>3</td>
								<td width='500' class='left'>Pegawai BUMS</td>
								<td width='50' class='center'>23</td>
							</tr>
							<tr>
								<td width='30' class='center'>4</td>
								<td width='500' class='left'>PNS</td>
								<td width='50' class='center'>12</td>
							</tr>
							<tr>
								<td width='30' class='center'>5</td>
								<td width='500' class='left'>POLRI</td>
								<td width='50' class='center'>3</td>
							</tr>
							<tr>
								<td width='30' class='center'>6</td>
								<td width='500' class='left'>Petani</td>
								<td width='50' class='center'>45</td>
							</tr>
							<tr>
								<td width='30' class='center'>7</td>
								<td width='500' class='left'>Wirausaha</td>
								<td width='50' class='center'>67</td>
							</tr>
							<tr>
								<td width='30' class='center'></td>
								<td width='500' class='left'>Jumlah</td>
								<td width='50' class='center'>171</td>
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 3</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
