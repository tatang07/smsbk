<h1 class="grid_12">Pelayanan Dan Pembinaan Siswa</h1>
<div class="grid_12">
	<div class="box with-table">
		<div class="header">
			<h2>Proporsi Gender</h2>
		</div>
		<div class="box" style="padding:5px 10px">
			<div class="content" style="height: 250px;">
				<table class=chart data-type=bars >
					<thead>
						<tr>
							<th></th>
							<th>X</th>
							<th>XI</th>
							<th>XII</th>


						</tr>
					</thead>
					<tbody>
						<tr>
							<th>Laki-laki</th>
							<td>89</td>
							<td>94</td>
							<td>112</td>

						</tr>
						<tr>
							<th>Perempuan</th>
							<td>167</td>
							<td>153</td>
							<td>173</td>
						</tr>

					</tbody>	
				</table>
			</div><!-- End of .content -->
		</div><!-- End of .box -->
		<div class="content" style="border-top:1px solid #bbb">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan="2">No.</th>
								<th rowspan="2">Tingkat</th>
								<th colspan="3">Jumlah Siswa Menurut Jenis Kelamin</th>
							</tr>
							<tr>
								<th>L</th>
								<th>P</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>X</td>
								<td width='' class='center'>89</td>
								<td width='' class='center'>167</td>
								<td width='' class='center'>256</td>

							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>XI</td>
								<td width='' class='center'>94</td>
								<td width='' class='center'>153</td>
								<td width='' class='center'>247</td>

							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>XII</td>
								<td width='' class='center'>112</td>
								<td width='' class='center'>173</td>
								<td width='' class='center'>285</td>
								
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 3</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
							<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
							<span>
								<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
							</span>
							<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
			</div>
		</div>        
	</div>
</div>
