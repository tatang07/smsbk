<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Kebijakan Nasional, Provinsi, Kabupaten Kota</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=80px align='left'><span class="text">Tahun Ajaran</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>2009/2010</option>
									<option value=''>2011/2012</option>
									<option value=''>2012/2013</option>
									<option value=''>2013/2014</option>
									<option value=''>2014/2015</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>							
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kebijakan Nasional</th>
								<th>Periode</th>
								<th>Keterangan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>Administrasi</td>								
								<td width='' class='center'>Januari 2010-Desember 2010</td>
								<td width='' class='center'>Memantapkan Organisasi Manajemen</td>
								<td width='20%' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>Kurikulum</td>								
								<td width='' class='center'>Januari 2010-Desember 2010</td>
								<td width='' class='center'>Meningkatkan Mutu Pendidikan</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>Ketatausahaan</td>								
								<td width='' class='center'>Januari 2010-Desember 2010</td>
								<td width='' class='center'>Peningkatan Pelayanaan Tata Usaha</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>							
							<tr>
								<td width='' class='center'>4 </td>
								<td width='' class='center'>Kesiswaan</td>								
								<td width='' class='center'>Januari 2010-Desember 2010</td>
								<td width='' class='center'>Penertiban dan Peningkatan OSIS, Pramuka, PMR, Pencinta Alam, Olah Raga Prestasi, Majelis Ta,lim, Merpati Putih, Kesenian.</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='center'>Sarana dan Prasarana</td>								
								<td width='' class='center'>Januari 2010-Desember 2010</td>
								<td width='' class='center'>Pemenuhan Sarana Dan Prasarana</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>6</td>
								<td width='' class='center'>Humas</td>								
								<td width='' class='center'>Januari 2010-Desember 2010</td>
								<td width='' class='center'>Meningkatkan Hubungan Dengan Masyarakat</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>7</td>
								<td width='' class='center'>BP/BK</td>								
								<td width='' class='center'>Januari 2010-Desember 2010</td>
								<td width='' class='center'>Memberikan Bimbingan Kepada Siswa.</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>							
						</tbody>
					</table>
					<div class="footer">
					
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
