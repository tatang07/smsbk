<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Keadaan peserta didik tahun ajaran 2014/2015</h2>
       </div>
       <div class="tabletools">		
			<div class="right">
			  	<a href="<?php echo base_url("statis/pps");?>"><i class="icon"></i>Daftar Siswa</a> 
			  <br/><br/>
            </div>
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>						
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>							
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=2>No.</th>
								<th rowspan=2>Tingkat Kelas</th>
								<th rowspan=2>Jumlah Ruangan Belajar</th>
								<th colspan=2>Jumlah Siswa</th>
								<th rowspan=2>Total</th>
							</tr>
							<tr>
								<th>L</th>
								<th>P</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>X</td>
								<td width='' class='center'>10</td>
								<td width='' class='center'>3</td>
								<td width='' class='center'>7</td>								
								<td width='' class='center'>21</td>															
							</tr>							
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>XI</td>
								<td width='' class='center'>10</td>
								<td width='' class='center'>3</td>
								<td width='' class='center'>4</td>								
								<td width='' class='center'>45</td>							
							</tr>							
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>XII</td>
								<td width='' class='center'>10</td>
								<td width='' class='center'>13</td>
								<td width='' class='center'>18</td>								
								<td width='' class='center'>31</td>														
							</tr>														
							<tr>
								<td width='' class='center' colspan='2'>Jumlah</td>
								<td width='' class='center'>30</td>
								<td width='' class='center'>19</td>
								<td width='' class='center'>39</td>								
								<td width='' class='center'>97</td>							
							</tr>														
						</tbody>
					</table>
					<div class="footer">
					
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
