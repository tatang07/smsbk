<h1 class="grid_12">Keuangan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekap Dana Sumbangan Pendidikan Tahunan</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-print"></i>Cetak File</a> 
			  	<a href=""><i class="icon-plus"></i>Pembayaran DSPT</a> 
			  <br><br>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=80px align='left'><span class="text">Kelas</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>X-1</option>									
									<option value=''>X-2</option>									
									<option value=''>X-3</option>									
									<option value=''>X-4</option>									
									<option value=''>X-5</option>									
									<option value=''>X-6</option>									
									<option value=''>X-7</option>									
									<option value=''>X-8</option>									
									<option value=''>X-9</option>									
									<option value=''>X-10</option>	
									<option value=''>XI SCIENCE - 1</option>										
									<option value=''>XI SCIENCE - 2</option>										
									<option value=''>XI SCIENCE - 3</option>										
									<option value=''>XI SCIENCE - 4</option>										
									<option value=''>XI SCIENCE - 5</option>										
									<option value=''>XI SCIENCE - 6</option>										
									<option value=''>XII SCIENCE - 1</option>										
									<option value=''>XII SCIENCE - 2</option>										
									<option value=''>XII SCIENCE - 3</option>										
									<option value=''>XII SCIENCE - 4</option>										
									<option value=''>XII SCIENCE - 5</option>										
									<option value=''>XII SCIENCE - 6</option>										
									<option value=''>XI SOCIAL - 1</option>
									<option value=''>XI SOCIAL - 2</option>
									<option value=''>XI SOCIAL - 3</option>
									<option value=''>XII SOCIAL - 1</option>
									<option value=''>XII SOCIAL - 2</option>
									<option value=''>XII SOCIAL - 3</option>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>							
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Jumlah Bayar</th>
								<th>Tunggakan</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>0910110064</td>					
								<td width='' class='center'>DESSY AYU INDRAHADI</td>																
								<td width='' class='center'>Rp 0,-</td>																
								<td width='' class='center'>Rp 1.500.000,-</td>																
							</tr>
						</tbody>
					</table>
					<div class="footer">
					
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
