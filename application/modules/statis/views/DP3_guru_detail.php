<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar Penilaian Pelaksanaan Pekerjaan Pegawai Negeri Sipil (DPPP)</h2>
       </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<tbody>
							<tr>
								<td width='30px' class='center'rowspan = "6">1</td>
								<td colspan = "3"><b>YANG DINILAI</b></td>						
							</tr>
							<tr>
								<td width='' class='left'>a. Nama</td>
								<td width='' class='left'>Achmad Suhadi, S.Pd</td>							
							</tr>
							<tr>
								<td width='' class='left'>b. NIP</td>
								<td width='' class='left'>198003102008012009</td>							
							</tr>
							<tr>
								<td width='' class='left'>c. Pangkat,golongan ruang</td>
								<td width='' class='left'>Pembi, IIIa</td>							
							</tr>
							<tr>
								<td width='' class='left'>d. Jabatan/Pekerjaan</td>
								<td width='' class='left'>Guru</td>							
							</tr>							
							<tr>
								<td width='' class='left'>e. Unit Organisasi</td>
								<td width='' class='left'>Guru</td>							
							</tr>

							<tr>
								<td width='30px' class='center'rowspan = "6">2</td>
								<td colspan = "3"><b>PEJABAT PENILAI</b></td>						
							</tr>
							<tr>
								<td width='' class='left'>a. Nama</td>
								<td width='' class='left'>Dra. Hj. SULASTRI DJ, M.Pd</td>							
							</tr>
							<tr>
								<td width='' class='left'>b. NIP</td>
								<td width='' class='left'>1955051119</td>							
							</tr>
							<tr>
								<td width='' class='left'>c. Pangkat,golongan ruang</td>
								<td width='' class='left'>Pembi, IVb </td>							
							</tr>
							<tr>
								<td width='' class='left'>d. Jabatan/Pekerjaan</td>
								<td width='' class='left'>Kepala Sekolah </td>							
							</tr>							
							<tr>
								<td width='' class='left'>e. Unit Organisasi</td>
								<td width='' class='left'>Kepala Sekolah </td>							
							</tr>

							<tr>
								<td width='30px' class='center'rowspan = "6">3</td>
								<td colspan = "3"><b>ATASAN PEJABAT PENILAI</b></td>						
							</tr>
							<tr>
								<td width='' class='left'>a. Nama</td>
								<td width='' class='left'>Dra. Hj. SULASTRI DJ, M.Pd</td>							
							</tr>
							<tr>
								<td width='' class='left'>b. NIP</td>
								<td width='' class='left'>1955051119</td>							
							</tr>
							<tr>
								<td width='' class='left'>c. Pangkat,golongan ruang</td>
								<td width='' class='left'>Pembi, IVb </td>							
							</tr>
							<tr>
								<td width='' class='left'>d. Jabatan/Pekerjaan</td>
								<td width='' class='left'>Kepala Sekolah </td>							
							</tr>							
							<tr>
								<td width='' class='left'>e. Unit Organisasi</td>
								<td width='' class='left'>Kepala Sekolah </td>							
							</tr>									
						</tbody>
					</table>
				</div>
            </div>
        </div>        
    </div>
 </div>

<div class="grid_12">
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<tbody>		
							<tr>
								<td width='30px' class='center'rowspan = "13">4</td>
								<td colspan = "4"><b>PENILAIAN</b></td>						
							</tr>					
							<tr>
								<td width='' class='center' rowspan = "2">Unsur Yang Di Nilai</td>
								<td width='' class='center' colspan = "2">Nilai </td>							
								<td width='' class='center' rowspan = "2">Keterangan</td>							
							</tr>
							<tr>
								<td width='' class='center'>Angka </td>							
								<td width='' class='center'>Sebutan</td>
							</tr>	
							<tr>
								<td width='' class='left'>a. Kesetiaan</td>
								<td width='' class='center'>91</td>							
								<td width='' class='center'>sangat baik </td>				
								<td width='' class='center'> </td>				
							</tr>		
							<tr>
								<td width='' class='left'>b. Prestasi Kerja</td>
								<td width='' class='center'>78</td>							
								<td width='' class='center'>baik </td>				
								<td width='' class='center'> </td>				
							</tr>	
							<tr>
								<td width='' class='left'>c. Tanggung Jawab</td>
								<td width='' class='center'>78</td>							
								<td width='' class='center'>baik </td>				
								<td width='' class='center'> </td>				
							</tr>									
							<tr>
								<td width='' class='left'>d. Ketaatan</td>
								<td width='' class='center'>79</td>							
								<td width='' class='center'>baik </td>				
								<td width='' class='center'> </td>				
							</tr>									
							<tr>
								<td width='' class='left'>e. Kejujuran</td>
								<td width='' class='center'>78</td>							
								<td width='' class='center'>baik </td>				
								<td width='' class='center'> </td>				
							</tr>
							<tr>
								<td width='' class='left'>f. Kerjasama</td>
								<td width='' class='center'>76</td>							
								<td width='' class='center'>baik </td>				
								<td width='' class='center'> </td>				
							</tr>	
							<tr>
								<td width='' class='left'>g. Prakarsa</td>
								<td width='' class='center'>76</td>							
								<td width='' class='center'>baik </td>				
								<td width='' class='center'> </td>				
							</tr>	
							<tr>
								<td width='' class='left'>h. Kepemimpinan</td>
								<td width='' class='center'>10</td>							
								<td width='' class='center'>kurang </td>				
								<td width='' class='center'> </td>				
							</tr>
							<tr>
								<td width='' class='left'><b>Jumlah</b></td>
								<td width='' class='center'>566</td>							
								<td width='' class='center'>- </td>				
								<td width='' class='center'> </td>				
							</tr>								
							<tr>
								<td width='' class='left'><b>Rata - Rata</b></td>
								<td width='' class='center'>70</td>							
								<td width='' class='center'>baik </td>				
								<td width='' class='center'> </td>				
							</tr>	
						</tbody>
					</table>
				</div>
            </div>
        </div>        
    </div>
 </div>

<div class="grid_12">
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<tbody>		
							<tr>
								<td width='30px' class='center'rowspan = "13">5</td>
								<td colspan = "4"><b>KEBERATAN DARI PEGAWAI NEGERI SIPIL YANG DINILAI (APABILA ADA)</b></td>						
							</tr>	
						</tbody>
					</table>
				</div>
            </div>
        </div>        
    </div>
 </div>


<div class="grid_12">
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<tbody>		
							<tr>
								<td width='30px' class='center'rowspan = "13">6</td>
								<td colspan = "4"><b>TANGGAPAN PEJABAT PENILAI ATAS KEBERATAN
</b></td>						
							</tr>	
						</tbody>
					</table>
				</div>
            </div>
        </div>        
    </div>
 </div>


<div class="grid_12">
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<tbody>		
							<tr>
								<td width='30px' class='center'rowspan = "13">7</td>
								<td colspan = "4"><b>KEPUTUSAN ATASAN PEJABAT PENILAI ATAS KEBERATAN</b></td>						
							</tr>	
						</tbody>
					</table>
				</div>
            </div>
        </div>        
    </div>
 </div>


<div class="grid_12">
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<tbody>		
							<tr>
								<td width='30px' class='center'rowspan = "13">8</td>
								<td colspan = "4"><b>LAIN-LAIN</b></td>						
							</tr>	
						</tbody>
					</table>
				</div>
            </div>
        </div>        
    </div>
 </div>