<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>
<div class="grid_12">
    <div class="box with-table">
        <div class="header">
            <h2>Daftar Calon Siswa Baru Tahun Ajaran 2014/2015</h2>
        </div>

        <div class="tabletools">
        	<div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<th width=80px align='left'><span class="text">Tahun Ajaran</span></th>
							<td width=200px align='left'>
								<select>
									<option>2009/2010</option>
									<option>2010/2011</option>
									<option>2011/2012</option>
									<option>2012/2013</option>
									<option>2013/2014</option>
									<option>2014/2015</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<th width=80px align='left'><span class="text">Daya Tampung</span></th>
							<td width=100px align='left'>
								<input type="text" />
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<th width=80px align='left'><span class="text">Batas Kelulusan</span></th>
							<td width=100px align='left'>
								<input type="text" />
							</td>

							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>

        <div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No</th>
								<th>No Pendaftaran</th>
								<th>Nama</th>
								<th>L/P</th>
								<th>Tempat / Tanggal Lahir</th>
								<th>Asal Sekolah</th>
								<th>Nilai UN</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>0890761536</td>							
								<td>Ahmad Zaenuddin</td>							
								<td>L</td>							
								<td>Bandung, 17 Juni 1996</td>							
								<td>SMPN 1 Garut</td>							
								<td>95</td>							
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-file"></i> Lihat</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-download"></i> Download</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>							
							</tr>
							<tr>
								<td>2</td>
								<td>08906153928</td>							
								<td>Ardian August Rachman</td>							
								<td>L</td>							
								<td>Garut, 23 Agustus 1995</td>							
								<td>SMPN 1 Sumedang</td>							
								<td>80</td>							
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-file"></i> Lihat</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-download"></i> Download</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>							
							</tr>
						</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </div>
 </div>
