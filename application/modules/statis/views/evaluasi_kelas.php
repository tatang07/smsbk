<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Evaluasi Kelas</h2>
       </div>
       <div class="tabletools">
			<div class="right">			  	
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                
					<table>
						<tr>
							<th width=1px align='left'><span class="text">Kelas</span></th>
										<td width=100px align='left'>
											<select>
												<option value=''>X 1</option>
												<option value=''>X 2</option>
												<option value=''>X 3</option>
												<option value=''>X 4</option>
												<option value=''>X 5</option>
												<option value=''>XI IPA 1</option>
												<option value=''>XI IPA 2</option>
												<option value=''>XI IPA 3</option>
												<option value=''>XI IPA 4</option>
												<option value=''>XI IPS 1</option>
												<option value=''>XI IPS 2</option>
												<option value=''>XI IPS 3</option>
												<option value=''>XI BAHASA 1</option>
												<option value=''>XI BAHASA 2</option>
												<option value='' selected>XII IPA 1</option>
												<option value=''>XII IPA 2</option>
												<option value=''>XII IPA 3</option>
												<option value=''>XII IPA 4</option>
												<option value=''>XII IPS 1</option>
												<option value=''>XII IPS 2</option>
												<option value=''>XII IPS 3</option>
												<option value=''>XII BAHASA 1</option>
												<option value=''>XII BAHASA 2</option>
											</select>
										</td>
							<td width=10px>&nbsp;</td>
							<th width=30px align='left'><span class="text">Matpel</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>A003 - Matematika - XI IPA</option>									
									<option value=''>A001 - Matematika - X</option>									
									<option value=''>S003 - Matematika - XI IPS</option>									
								</select>
							</td>
							<td width=10px>&nbsp;</td>
							<th width=30px align='left'><span class="text">Komponen</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>Pengetahuan</option>									
									<option value=''>Sikap</option>									
									<option value=''>Keterampilan</option>									
								</select>
							</td>
							<td width=10px>&nbsp;</td>
							<td><a href="javascript:changePage(1)" original-title="Cari" class="button grey tooltip"><i class="icon-search"></i></a></td>
						</tr>
					</table>
                
            </div>            
        </div>
    </div>
</div>
<div class="grid_12">
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">   
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan="3">No.</th>
								<th rowspan="3">Nis</th>
								<th rowspan="3">Nama</th>								
								<th colspan="5">Nilai</th>						
							</tr>
							<tr>
								<th>Tugas</td>
								<th>Ulangan Harian</td>
								<th>UTS</td>
								<th>UAS</td>
								<th rowspan="2">Nilai Akhir</th>						
							</tr>
							<tr>
								<th>20</td>
								<th>30</td>
								<th>25</td>
								<th>25</td>								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30px' class='center'>1</td>
								<td width='100px' class='center'>1112001</td>
								<td width='200px'>Sundari Sukoco</td>
								<td width='200px'>87</td>
								<td width='200px'>96</td>
								<td width='200px'>78</td>
								<td width='200px'>85</td>
								<td width='200px'>86.95</td>
							</tr>										
							<tr>
								<td width='30px' class='center'>2</td>
								<td width='100px' class='center'>1112002</td>
								<td width='200px'>Nur Hasan</td>
								<td width='200px'>76</td>
								<td width='200px'>79</td>
								<td width='200px'>70</td>
								<td width='200px'>75</td>
								<td width='200px'>75.15</td>
							</tr>						
							<tr>
								<td width='30px' class='center'>3</td>
								<td width='100px' class='center'>1112003</td>
								<td width='200px'>Devi Yulianti</td>
								<td width='200px'>78</td>
								<td width='200px'>82</td>
								<td width='200px'>64</td>
								<td width='200px'>78</td>
								<td width='200px'>75.7</td>
							</tr>													
						</tbody>
					</table>					
				</div>
            </div>
        </div>        
    </div>
 </div>
