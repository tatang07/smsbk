<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Jenis Ekstrakurikuler</h2>
       </div>
       <div class="tabletools">
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Ekstrakurikuler</th>
								<th>Pelatih</th>
								<th>Aksi</th>
						
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>Bulutangkis</td>
								<td width='' class='center'>Dr.Toni</td>
							
								<td width='' class='center'>
								<a href="<?php echo base_url('statis/det_je');?>" class="button small grey tooltip" data-gravity="s"><i class="icon"></i> Detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>Pramuka</td>
								<td width='' class='center'>Dr.Hasan</td>
								<td width='' class='center'>
								<a href="<?php echo base_url('statis/det_je');?>" class="button small grey tooltip" data-gravity="s"><i class="icon"></i> Detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>Paskibraka</td>
								<td width='' class='center'>Dr.Iip</td>
								<td width='' class='center'>
								<a href="<?php echo base_url('statis/det_je');?>" class="button small grey tooltip" data-gravity="s"><i class="icon"></i> Detail</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
								
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 3</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
