<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Pengkelasan Siswa Baru</h2>
       </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=2>Kelas</th>
								<th rowspan=2>Prioritas</th>															
								<th colspan=2>Daya Tampung</th>
								<th rowspan=2>Kelas Unggulan</th>								
							</tr>
							<tr>
								<th>L</th>
								<th>P</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>X-1</td>
								<td width='' class='center'><input type='text' value='1'></td>
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='checkbox'></td>						
							</tr>	
							<tr>
								<td width='' class='center'>X-2</td>
								<td width='' class='center'><input type='text' value='2'></td>
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='checkbox'></td>						
							</tr>
							<tr>
								<td width='' class='center'>X-3</td>
								<td width='' class='center'><input type='text' value='3'></td>
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='checkbox'></td>						
							</tr>
							<tr>
								<td width='' class='center'>X-4</td>
								<td width='' class='center'><input type='text' value='4'></td>
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='checkbox'></td>						
							</tr>
							<tr>
								<td width='' class='center'>X-5</td>
								<td width='' class='center'><input type='text' value='5'></td>
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='checkbox'></td>						
							</tr>
							<tr>
								<td width='' class='center'>X-6</td>
								<td width='' class='center'><input type='text' value='6'></td>
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='checkbox'></td>						
							</tr>
							<tr>
								<td width='' class='center'>X-7</td>
								<td width='' class='center'><input type='text' value='7'></td>
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='checkbox'></td>						
							</tr>
							<tr>
								<td width='' class='center'>X-8</td>
								<td width='' class='center'><input type='text' value='8'></td>
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='checkbox'></td>						
							</tr>
							<tr>
								<td width='' class='center'>X-9</td>
								<td width='' class='center'><input type='text' value='9'></td>
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='checkbox'></td>						
							</tr>
							<tr>
								<td width='' class='center'>X-10</td>
								<td width='' class='center'><input type='text' value='10'></td>
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='text' value='0'></td>							
								<td width='' class='center'><input type='checkbox'></td>						
							</tr>							
						</tbody>
					</table>
				</div>
            </div>
        </div>        
    </div>
					<a class="button" data-gravity="s"><i class="icon"></i>Lakukan Pengkelasan</a>
 </div>
