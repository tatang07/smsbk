<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
    	<div class="content" style="padding:20px;">
	    	<div style="float:left">
		    	<table>
		    		<tr>
		    			<td>Nama Sekolah</td>
		    			<td>:</td>
		    			<td> SMA LABORATORIUM PERCONTOHAN UPI</td>
		    		</tr>
		    		<tr>
		    			<td>Alamat Sekolah</td>
		    			<td>:</td>
		    			<td> JL. Dr. Setiabudhi No.229 Bandung 40154</td>
		    		</tr>
		    		<tr>
		    			<td>&nbsp;</td>
		    		</tr>
		    		<tr>
		    			<td>Nama</td>
		    			<td>:</td>
		    			<td> SYIFA RIZQI AMALIA</td>
		    		</tr>
		    		<tr>
		    			<td>Nomor Induk/NISN</td>
		    			<td>:</td>
		    			<td> 0004190229</td>
		    		</tr>
		    	</table>
	    	</div>
	    	<div style="float:right;">
	    		<table>
		    		<tr>
		    			<td>Kelas</td>
		    			<td>:</td>
		    			<td> X MIA-2</td> 
		    		</tr>
		    		<tr>
		    			<td>Semester</td>
		    			<td>:</td>
		    			<td> 1 (Satu)</td>
		    		</tr>
	    			<tr>
		    			<td>Tahun Pelajaran</td>
		    			<td>:</td>
		    			<td> 2013/2014</td>
	    			</tr>
	    		</table>
	    	</div>
    	</div>
       <div class="header">
            <h2>CAPAIAN KOMPETENSI</h2>
       </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper" role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan='2' colspan='2'>MATA PELAJARAN</th>
								<th colspan='2'>Pengetahuan</th>
								<th colspan='2'>Keterampilan</th>
								<th colspan='2'>Sikap Spiritual</th>
							</tr>
							<tr>
								<th>Angka</th>
								<th>Perdikat</th>
								<th>Angka</th>
								<th>Predikat</th>
								<th>Dalam Mapel</th>
								<th>Antar Mapel</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td colspan='8' width='' class='left'><b>Kelompok A (Wajib)</b></td>
							</tr>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='left'>
									Pendidikan Agama dan Budi Pekerti <br>
									Nama Guru: Nanang Karyana
								</td>
								<td width='' class='center'>3.66</td>
								<td width='' class='center'>A-</td>
								<td width='' class='center'>3.33</td>
								<td width='' class='center'>B+</td>
								<td width='' class='center'>SB</td>
								<td width='150px' class='left' rowspan='17'>
									Pengamalan ajaran agama baik, 
									kejujurannya baik, kedisiplinannya baik,
									tanggung jawab baik, kepedulian baik,
									kesantunan baik, dan kepercayaan dirinya baik.
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='left'>
									Pendidikan Pancasila dan Kewarganaan <br>
									Nama Guru: Dwi Haryanto
								</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>3.33</td>
								<td width='' class='center'>B+</td>
								<td width='' class='center'>SB</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='left'>
									Bahasa Indonesia<br>
									Nama Guru: Risbon Sianturi
								</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>B</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='left'>
									Matematika<br>
									Nama Guru: Titi Jualawati
								</td>
								<td width='' class='center'>3.33</td>
								<td width='' class='center'>B+</td>
								<td width='' class='center'>3.66</td>
								<td width='' class='center'>A-</td>
								<td width='' class='center'>SB</td>
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='left'>
									Sejarah Indonesia<br>
									Nama Guru: Lina Herlina
								</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>3.33</td>
								<td width='' class='center'>B+</td>
								<td width='' class='center'>B</td>
							</tr>
							<tr>
								<td width='' class='center'>6</td>
								<td width='' class='left'>
									Bahasa Ingrris<br>
									Nama Guru: Aan Handoyo
								</td>
								<td width='' class='center'>3.33</td>
								<td width='' class='center'>B+</td>
								<td width='' class='center'>3.33</td>
								<td width='' class='center'>B+</td>
								<td width='' class='center'>B</td>
							</tr>
							<tr>
								<td colspan='7' width='' class='left'><b>Kelompok B (Wajib)</b></td>
							</tr>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='left'>
									Seni Budaya (Seni Musik)<br>
									Nama Guru: Ahmad Zaldi
								</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>2.66</td>
								<td width='' class='center'>B-</td>
								<td width='' class='center'>B</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='left'>
									Pendidikan Jasmani dan Kesehatan <br>
									Nama Guru: Firman Budyana
								</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>B</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='left'>
									Prakarya (Rekayasa) dan Kewirausahaan <br>
									Nama Guru: Mohamad Agus Kusmayadi
								</td>
								<td width='' class='center'>3.66</td>
								<td width='' class='center'>A-</td>
								<td width='' class='center'>3.33</td>
								<td width='' class='center'>B+</td>
								<td width='' class='center'>B</td>
							</tr>
							<tr>
								<td colspan='7' width='' class='left'><b>Kelompok C (Peminatan) Matematika dan Ilmu Alam</b></td>
							</tr>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='left'>
									Matematika<br>
									Nama Guru: Oo Ohir
								</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>B</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='left'>
									Biologi<br>
									Nama Guru: Mohammad Ikhsanul Hakim
								</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>2.66</td>
								<td width='' class='center'>B-</td>
								<td width='' class='center'>B</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='left'>
									Fisika<br>
									Nama Guru: Yoga Adi Pratama
								</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>2.66</td>
								<td width='' class='center'>B-</td>
								<td width='' class='center'>B</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='left'>
									Kimia<br>
									Nama Guru: Mokhamad Irwan
								</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>2.66</td>
								<td width='' class='center'>B-</td>
								<td width='' class='center'>B</td>
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='left'>
									Geografi<br>
									Nama Guru: Mulyana Abdullah
								</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>2.66</td>
								<td width='' class='center'>B-</td>
								<td width='' class='center'>B</td>
							</tr>
							<tr>
								<td width='' class='center'>6</td>
								<td width='' class='left'>
									Bahasa Asing<br>
									Nama Guru: Rika Seftiana
								</td>
								<td width='' class='center'>3.00</td>
								<td width='' class='center'>B</td>
								<td width='' class='center'>2.66</td>
								<td width='' class='center'>B-</td>
								<td width='' class='center'>B</td>
							</tr>
						</tbody>
					</table>
				</div>
            </div>
        </div>        
    </div>
 </div>
