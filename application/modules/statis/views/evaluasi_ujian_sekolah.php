<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Nilai Evaluasi Ujian Sekolah</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-print"></i>Cetak File</a> 
			  	<a href=""><i class="icon-list"></i>Rekap Evaluasi</a> 
			  	<a href=""><i class="icon-plus"></i>Entri Nilai</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=30px align='left'><span class="text">Kelas:</span></th>
							<td width=200px align='left'>
							<select>
								<option value="1" selected="selected">X-1</option>
								<option value="2">X-2</option>
								<option value="3">X-3</option>
								<option value="4">X-4</option>
								<option value="5">X-5</option>
								<option value="6">X-6</option>
								<option value="7">X-7</option>
								<option value="8">X-8</option>
								<option value="9">X-9</option>
								<option value="10">X-10</option>
								<option value="11">XI SCIENCE - 1</option>
								<option value="12">XI SCIENCE - 2</option>
								<option value="13">XI SCIENCE - 3</option>
								<option value="14">XI SCIENCE - 4</option>
								<option value="15">XI SCIENCE - 5</option>
								<option value="16">XI SCIENCE - 6</option>
								<option value="21">XII SCIENCE - 1</option>
								<option value="22">XII SCIENCE - 2</option>
								<option value="23">XII SCIENCE - 3</option>
								<option value="24">XII SCIENCE - 4</option>
								<option value="25">XII SCIENCE - 5</option>
								<option value="26">XII SCIENCE - 6</option>
								<option value="30">XII SOCIAL - 4</option>
								<option value="17">XI SOCIAL - 1</option>
								<option value="18">XI SOCIAL - 2</option>
								<option value="19">XI SOCIAL - 3</option>
								<option value="20">XI SOCIAL - 4</option>
								<option value="27">XII SOCIAL - 1</option>
								<option value="28">XII SOCIAL - 2</option>
								<option value="29">XII SOCIAL - 3</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							</tr>
							<tr>
							<th width=100px align='left'><span class="text">Mata Pelajaran:</span></th>
							<td width=200px align='left'>
							<select>
								<option value="102" selected="selected">BHI333 - Bahasa Inggris</option>
								<option value="103">BHI333 - Bahasa Inggris</option>
								<option value="2">BIO310 - Biologi</option>
								<option value="15">BIO321 - Biologi</option>
								<option value="27">BIO331 - Biologi</option>
								<option value="9">EKO310 - Ekonomi</option>
								<option value="45">EKO322 - Ekonomi</option>
								<option value="58">EKO332 - Ekonomi</option>
								<option value="3">ENG310 - Bahasa Inggris</option>
								<option value="19">ENG321 - Bahasa Inggris</option>
								<option value="40">ENG322 - Bahasa Inggris</option>
								<option value="31">ENG331 - Bahasa Inggris</option>
								<option value="4">FIS310 - Fisika</option>
								<option value="16">FIS321 - Fisika</option>
								<option value="28">FIS331 - Fisika</option>
								<option value="77">FRC310 - Bahasa Perancis</option>
								<option value="78">FRC311 - Bahasa Perancis</option>
								<option value="80">FRC321 - Bahasa Perancis</option>
								<option value="83">FRC332 - Bahasa Perancis</option>
								<option value="84">FRC333 - Bahasa Perancis</option>
								<option value="11">GEO310 - Geografi dan Kependudukan</option>
								<option value="44">GEO322 - Geografi dan Kependudukan</option>
								<option value="57">GEO332 - Geografi dan Kependudukan</option>
								<option value="74">IND310 - Bahasa Indonesia</option>
								<option value="18">IND321 - Bahasa Indonesia</option>
								<option value="39">IND322 - Bahasa Indonesia</option>
								<option value="30">IND331 - Bahasa Indonesia</option>
								<option value="54">IND332 - Bahasa Indonesia</option>
								<option value="79">JEP110 - Bahasa Jepang</option>
								<option value="81">JEP210 - Bahasa Jepang</option>
								<option value="85">JEP220 - Bahasa Jepang</option>
								<option value="86">JEP230 - Bahasa Jepang</option>
								<option value="87">JEP240 - Bahasa Jepang</option>
								<option value="82">JEP310 - Bahasa Jepang</option>
								<option value="88">JEP320 - Bahasa Jepang</option>
								<option value="89">JEP330 - Bahasa Jepang</option>
								<option value="90">JEP340 - Bahasa Jepang</option>
								<option value="75">JER310 - Bahasa Jerman</option>
								<option value="76">JER310 - Bahasa Jerman</option>
								<option value="92">KES310 - Kesenian</option>
								<option value="24">KES321 - Kesenian</option>
								<option value="51">KES322 - Kesenian</option>
								<option value="36">KES331 - Kesenian</option>
								<option value="63">KES332 - Kesenian</option>
								<option value="5">KIM310 - Kimia</option>
								<option value="17">KIM321 - Kimia</option>
								<option value="29">KIM331 - Kimia</option>
								<option value="21">MAN321 - Bahasa Mandarin</option>
								<option value="47">MAN322 - Bahasa Mandarin</option>
								<option value="33">MAN331 - Bahasa Mandarin</option>
								<option value="60">MAN332 - Bahasa Mandarin</option>
								<option value="1">MAT310 - Matematika</option>
								<option value="14">MAT321 - Matematika</option>
								<option value="38">MAT322 - Matematika</option>
								<option value="26">MAT331 - Matematika</option>
								<option value="53">MAT332 - Matematika</option>
								<option value="94">MLL301 - Muatan Lokal</option>
								<option value="96">MLL321 - Muatan Lokal</option>
								<option value="100">MLL322 - Muatan Lokal</option>
								<option value="104">MLL322 - Muatan Lokal</option>
								<option value="99">MLL331 - Muatan Lokal</option>
								<option value="12">PAI310 - Pendidikan Agama Islam</option>
								<option value="22">PAI321 - Pendidikan Agama Islam</option>
								<option value="48">PAI322 - Pendidikan Agama Islam</option>
								<option value="34">PAI331 - Pendidikan Agama Islam</option>
								<option value="61">PAI332 - Pendidikan Agama Islam</option>
								<option value="10">PEN310 - Pendidikan Jasmani dan Kesehatan</option>
								<option value="23">PEN321 - Pendidikan Jasmani dan Kesehatan</option>
								<option value="50">PEN322 - Pendidikan Jasmani dan Kesehatan</option>
								<option value="35">PEN331 - Pendidikan Jasmani dan Kesehatan</option>
								<option value="62">PEN332 - Pendidikan Jasmani dan Kesehatan</option>
								<option value="7">PKN310 - Pendidikan Kewarganegaraan</option>
								<option value="20">PKN321 - Pendidikan Kewarganegaraan</option>
								<option value="42">PKN322 - Pendidikan Kewarganegaraan</option>
								<option value="32">PKN331 - Pendidikan Kewarganegaraan</option>
								<option value="55">PKN332 - Pendidikan Kewarganegaraan</option>
								<option value="108">PLH310 - PLH</option>
								<option value="106">PLH321 - PLH</option>
								<option value="105">PLH322 - PLH</option>
								<option value="93">SBB310 - Seni Budaya</option>
								<option value="101">SBB322 - Seni Budaya</option>
								<option value="8">SEJ310 - Sejarah Umum</option>
								<option value="43">SEJ322 - Sejarah Umum</option>
								<option value="56">SEJ332 - Sejarah Umum</option>
								<option value="97">SJH321 - Sejarah Umum</option>
								<option value="95">SJH321 - Sejarah Umum</option>
								<option value="98">SJH331 - Sejarah Umum</option>
								<option value="91">SOS310 - Sosiologi</option>
								<option value="46">SOS322 - Sosiologi</option>
								<option value="59">SOS332 - Sosiologi</option>
								<option value="13">TIK310 - Teknologi Informasi dan Komunikasi</option>
								<option value="25">TIK321 - Teknologi Informasi dan Komunikasi</option>
								<option value="52">TIK322 - Teknologi Informasi dan Komunikasi</option>
								<option value="37">TIK331 - Teknologi Informasi dan Komunikasi</option>
								<option value="64">TIK332 - Teknologi Informasi dan Komunikasi</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							</tr>
							<tr>
							<th width=30px align='left'><span class="text">Guru:</span></th>
							<td width=200px align='left'>
								<select>
								<option value="18" selected="selected">ACHMAD SUHADI, SPd</option>
								<option value="10">AGUS RIYANTO,S.Pd</option>
								<option value="1" selected="selected">Diana Susanti, S.Si.</option>
								<option value="25">Dra. Hj. SULASTRI DJ, M.Pd</option>
								<option value="6">Drs Iman Permana Hadi</option>
								<option value="14">Enah Suhaenah S Pd</option>
								<option value="15">Evi Sofia Nurajizah</option>
								<option value="11">H.Muhammad Ishak, S.Pd.</option>
								<option value="8">Imas Iriani</option>
								<option value="5">Indra Yuliyanti</option>
								<option value="2">LILIT SUTARSIH, S.PD</option>
								<option value="17">MAMAN RACHMAN, S.Pd.</option>
								<option value="7">Masruri</option>
								<option value="19">NURHAYATI, M, S.Pd</option>
								<option value="12">Repiana</option>
								<option value="4">Rudi Hartono, S.Pd</option>
								<option value="20">Saptarini Kismawati, SS</option>
								<option value="23">SOIPAH, S. KOM</option>
								<option value="24">Tias Beni Purabaya, S.Kom</option>
								<option value="13">TISNO WIHARDI, SPD</option>
								<option value="3">Warsidi,S.Si</option>
								<option value="21">Yati Mulyati, S.Pd</option>
								<option value="9">Yulianingsih, S.S</option>
								</select>
							</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							</tr>
							<tr>
							<td></td>
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i>Tamplikan</i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
		<div class="header">
            <h2>KKM: 0</h2>
       </div>
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>US 1</th>
								<th>Tanggal</th>
								<th>US 2</th>
								<th>Tanggal</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width=''>0910110064</td>
								<td width=''>DESSY AYU INDRAHADI</td>
								<td width='' class='center'>87</td>
								<td width='' class='center'>2 Desember 2014	</td>
								<td width='' class='center'>70	</td>
								<td width='' class='center'>2 Desember 2014	</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 1</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
