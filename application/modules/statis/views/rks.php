<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekap Kehadiran Siswa</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-plus"></i>Tambah</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=1px align='left'><span class="text">Kelas:</span></th>
							<td width=100px align='left'>
								<select>
									<option value=''>X</option>
									<option value=''>XI IPA</option>
									<option value=''>XI IPS</option>
									<option value=''>XI BAHASA</option>
									<option value=''>XII IPA</option>
									<option value=''>XII IPS</option>
									<option value=''>XII BAHASA</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<th width=80px align='left'><span class="text">Mata Pelajaran</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>A001 - Biologi - XI IPA</option>
									<option value=''>A002 - Kimia - XI IPA</option>
									<option value=''>A003 - Matematika - XI IPA</option>
									<option value=''>A004 - Fisika - XI IPA</option>
									<option value=''>A005 - Sejarah - XI IPS</option>
									<option value=''>A006 - Sosiologi - XI IPS</option>
									<option value=''>A007 - Geografi - XI IPS</option>
									<option value=''>A008 - Ekonomi - XI IPS</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Jumlah Kehadiran</th>
						
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>1201212</td>
								<td width='' class='center'>Nur Hasan</td>
								<td width='' class='center'>1</td>
							
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>121313</td>
								<td width='' class='center'>Sundari Sukoco</td>
								<td width='' class='center'>1</td>
							
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>1201414</td>
								<td width='' class='center'>Devi Yulianti</td>
								<td width='' class='center'>2</td>
								
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 3</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
