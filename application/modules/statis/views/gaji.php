<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar Gaji Berdasarkan Pangkat/Golongan</h2>
       </div>

			
            <div class="dataTables_filter">                
					<table>
						<tr>
							<th width=70px align='left'><span class="text">Golongan</span></th>
							<td width=50px align='left'>
								<select>
									<option value=''>Ia</option>
									<option value=''>IIIa</option>
									<option value=''>IIc</option>
									<option value=''>IIIb</option>
									<option value=''>IIIc</option>
									<option value=''>IIId</option>
									<option value=''>IVa</option>
									<option value=''>IVb</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
							
							</td>
						</tr>
					</table>               
            </div>
            
        </div>
        <div class="content" style="border: 1px solid #bbb">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIP</th>
								<th>Nama</th>
								<th>Gaji</th>								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>198003102008012009</td>								
								<td width='' class='left'>Diana Susanti, S.Si.</td>
								<td width='' class='center'>Rp 1.738.100,-</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>196701111991012001</td>								
								<td width='' class='left'>LILIT SUTARSIH, S.Pd</td>
								<td width='' class='center'>Rp 2.112.600,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>196701111343135155</td>								
								<td width='' class='left'>Warsidi,S.Si</td>
								<td width='' class='center'>Rp 2.400.000,-</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>196701818951982184</td>								
								<td width='' class='left'>Rudi Hartono, S.Kom</td>
								<td width='' class='center'>Rp 2.400.000,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='center'>196702325523532001</td>								
								<td width='' class='left'>Enah Suhaenah S Pd</td>
								<td width='' class='center'>Rp 2.663.000,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>6</td>
								<td width='' class='center'>196703231241434001</td>								
								<td width='' class='left'>Indra Yuliyanti</td>
								<td width='' class='center'>Rp 1.780.800,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>7</td>
								<td width='' class='center'>196701111991313002</td>								
								<td width='' class='left'>Drs Iman Permana Hadi</td>
								<td width='' class='center'>Rp 2.750.000,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>8</td>
								<td width='' class='center'>196701111233231002</td>								
								<td width='' class='left'>Masruri</td>
								<td width='' class='center'>Rp 2.000.175,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>9</td>
								<td width='' class='center'>196701111123455001</td>								
								<td width='' class='left'>Imas Iriani</td>
								<td width='' class='center'>Rp 2.600.000,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>10</td>
								<td width='' class='center'>196701112112342001</td>								
								<td width='' class='left'>Yulianingsih, S.S</td>
								<td width='' class='center'>Rp 1.738.100,-</td>
							</tr>								
							<tr>
								<td width='' class='center'>11</td>
								<td width='' class='center'>196124459291012001</td>								
								<td width='' class='left'>AGUS RIYANTO,S.Pd</td>
								<td width='' class='center'>Rp 3.042.500,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>12</td>
								<td width='' class='center'>196701112324552001</td>								
								<td width='' class='left'>H.Muhammad Ishak, S.Pd</td>
								<td width='' class='center'>Rp 3.042.000,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>13</td>
								<td width='' class='center'>196701111002097701</td>								
								<td width='' class='left'>Repiana, S.PD</td>
								<td width='' class='center'>Rp 2.200.000,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>14</td>
								<td width='' class='center'>196701324567278901</td>								
								<td width='' class='left'>TISNO WIHARDI, SPD</td>
								<td width='' class='center'>Rp 2.616.200,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>15</td>
								<td width='' class='center'>196701109886726001</td>								
								<td width='' class='left'>Yati Mulyati, S.Pd</td>
								<td width='' class='center'>Rp 2.500.000,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>16</td>
								<td width='' class='center'>196701111123242001</td>								
								<td width='' class='left'>Evi Sofia Nurajizah</td>
								<td width='' class='center'>Rp 1.650.000,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>17</td>
								<td width='' class='center'>196701115678122001</td>								
								<td width='' class='left'>ACHMAD SUHADI, SPd</td>
								<td width='' class='center'>Rp 3.000.000,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>18</td>
								<td width='' class='center'>196701111091970201</td>								
								<td width='' class='left'>NURHAYATI, M, SPd</td>
								<td width='' class='center'>Rp 2.111.600,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>19</td>
								<td width='' class='center'>196701119882112001</td>								
								<td width='' class='left'>MAMAN RACHMAN, S.Pd.</td>
								<td width='' class='center'>Rp 2.750.000,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>20</td>
								<td width='' class='center'>196701109101212001</td>								
								<td width='' class='left'>Saptarini Kismawati, SS</td>
								<td width='' class='center'>Rp 1.800.000,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>21</td>
								<td width='' class='center'>196701245602121002</td>								
								<td width='' class='left'>Tias Beni Purabaya, S.Kom</td>
								<td width='' class='center'>Rp 1.842.000,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>22</td>
								<td width='' class='center'>191245679120120201</td>								
								<td width='' class='left'>Dra. Hj. SULASTRI DJ, M.Pd</td>
								<td width='' class='center'>Rp 2.812.600,-</td>
							</tr>							
							<tr>
								<td width='' class='center'>23</td>
								<td width='' class='center'>196701111122451502</td>								
								<td width='' class='left'>SOIPAH, S. Kom</td>
								<td width='' class='center'>Rp 1.812.600,-</td>
							</tr>											
							<tr>
								<td width='' class='center'>Total:</td>
								<td width='' class='center'></td>								
								<td width='' class='center'></td>
								<td width='' class='center'>Rp 48.757.075,-</td>
							</tr>							
						</tbody>
					</table>
				</div>
            </div>
        </div>        
    </div>
 </div>
