<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       	<div class="box">
				
					<div class="header">
						<h2>Persyaratan Pendidik dan Tenaga Kependidikan</h2>
					</div>
					
					<div class="accordion toggle">
					
						<h3><a href="#">Seleksi Persyaratan Administrasi</a></h3>
						<div>
							<ul>							
								<li>Tenaga kependidikan adalah anggota masyarakat yang mengabdikan diri secara langsung dalam penyelenggaraan pendidikan</li>
								<li>Tenaga pendidik adalah tenaga kependidikan yang bertugas
									membimbing, mengajar, dan/atau melatih peserta didik
								</li>
								<li>Tenaga pembimbing adalah tenaga pendidik yang bertugas utama membimbing peserta didik.</li>
								<li>Tenaga pengajar adalah tenaga pendidik yang bertugas utama mengajar peserta didik.</li>
								<li>Tenaga pelatih adalah tenaga pendidik yang bertugas utama melatih peserta didik.</li>
								<li>Satuan pendidikan adalah satuan pelaksana kegiatan belajar-mengajar yang dilaksanakan di jalur pendidikan sekolah atau di jalur pendidikan luar sekolah.</li>
								<li>Penyelenggara satuan pendidikan adalah perorangan, Pemerintah atau badan sosial yang menyelenggarakan satuan pendidikan yang bersangkutan.</li>
								<li>Lembaga pendidikan tenaga keguruan adalah satuan atau bagian dari satuan pendidikan tinggi yang khusus menyelenggarakan pendidikan bagi calon tenaga pendidik untuk pendidikan prasekolah, pendidikan dasar, dan pendidikan menengah.</li>
						
							</ul>
						</div>
						
						
					</div><!-- End of .content -->
					
				</div><!-- End of .box -->		
        </div>
 </div>
