<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Data Rekap Sirkulasi Keuangan Tahun Ajaran 2014/2015</h2>
       </div>
       <div class="tabletools">
       	<div class="right">
			  	<a href=""><i class="icon-print"></i>Cetak File</a> 
			  	<a href=""><i class="icon-plus"></i>Tambah Sirkulasi Keuangan</a> 
			  <br><br>
            </div>		
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=80px align='left'><span class="text">Kelas</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>2014/2015</option>
									<option value=''>2013/2014</option>
									<option value=''>2012/2013</option>
									<option value=''>2011/2012</option>									
									<option value=''>2010/2011</option>									
									<option value=''>2009/2010</option>				
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>							
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Transaksi</th>
								<th>Penanggung Jawab</th>								
								<th>Debet</th>								
								<th>Kredit</th>								
								<th>Keterangan</th>								
								<th>Aksi</th>								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>Pembangunan</td>
								<td width='' class='center'>Dra. Hj. Sulastri DJ. M.Pd</td>					
								<td width='' class='center'>Rp 10.000.000,-</td>					
								<td width='' class='center'>Rp 1.500.000,-</td>					
								<td width='' class='center'>Pembanguan Ruangan Kepala Sekolah</td>					
								<td width='15%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>Pembangunan</td>
								<td width='' class='center'>Dra. Hj. Sulastri DJ. M.Pd</td>					
								<td width='' class='center'>Rp 8.500.000,-</td>					
								<td width='' class='center'>Rp 2.500.000,-</td>						
								<td width='' class='center'>Pembanguan Masjid</td>					
								<td width='' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>Pembiayaan Mengikuti Workshop</td>
								<td width='' class='center'>Drs. H. Susanto DJ. M.Pd</td>					
								<td width='' class='center'>Rp 7.000.000,-</td>					
								<td width='' class='center'>Rp 2.000.000,-</td>					
								<td width='' class='center'>Mengikuti Workshop Teknologi Informasi dan Komunikasi</td>					
								<td width='' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>Pembangunan</td>
								<td width='' class='center'>Dra. H. Susanto DJ. M.Pd</td>					
								<td width='' class='center'>Rp 6.000.000,-</td>					
								<td width='' class='center'>Rp 3.500.000,-</td>					
								<td width='' class='center'>Pembangunan Lapangan Olah Raga</td>					
								<td width='' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='center'>Pembangunan</td>
								<td width='' class='center'>Drs. Aceng Fikri</td>					
								<td width='' class='center'>Rp 4.000.000,-</td>					
								<td width='' class='center'>Rp 2.500.000,-</td>					
								<td width='' class='center'>Pembanguan Masjid</td>					
								<td width='' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>							
						</tbody>
					</table>
					<div class="footer">
					
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
