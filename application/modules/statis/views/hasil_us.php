<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekap Hasil Ujian Sekolah</h2>
       </div>
       <div class="tabletools">			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<th width=80px align='left'><span class="text">Tampilkan</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>2009/2010</option>
									<option value=''>2010/2011</option>
									<option value=''>2011/2012</option>
									<option value=''>2012/2013</option>
									<option value=''>2013/2014</option>
									<option value=''>2014/2015</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan='2'>No.</th>
								<th rowspan='2'>NIS</th>
								<th rowspan='2'>Nama Siswa</th>
								<th colspan='6'>Nilai</th>
								<th rowspan='2'>Jumlah Nilai</th>
								<th rowspan='2'>Rata-rata</th>
								<th colspan='2' rowspan='2'>Aksi</th>
							</tr>
							<tr>
								<th>Bahasa Indonesia</th>
								<th>Bahasa Inggris</th>
								<th>Matematika</th>
								<th>Biologi</th>
								<th>Kimia</th>
								<th>Fisika</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>11044252</td>
								<td width='' class='center'>Mifah Mizwar</td>
								<td width='' class='center'>10</td>
								<td width='' class='center'>10</td>
								<td width='' class='center'>10</td>
								<td width='' class='center'>10</td>
								<td width='' class='center'>10</td>
								<td width='' class='center'>10</td>
								<td width='' class='center'>40</td>
								<td width='' class='center'>10</td>
								<td width='100px' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>	
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>11044254</td>
								<td width='' class='center'>Budi Satria</td>
								<td width='' class='center'>9</td>
								<td width='' class='center'>9</td>
								<td width='' class='center'>9</td>
								<td width='' class='center'>9</td>
								<td width='' class='center'>9</td>
								<td width='' class='center'>9</td>
								<td width='' class='center'>54</td>
								<td width='' class='center'>9</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>						
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 2</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
							<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
							<span>
								<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
							</span>
							<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
