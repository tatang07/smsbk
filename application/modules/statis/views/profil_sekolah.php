<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
	<div class="box with-table">

		<div class="header">
			<h2>Profil Sekolah
				<div class="logo-sekolah">
					<img width="100px" src="<?php echo base_url("extras/sekolah/logo1.png"); ?>" alt="">
				</div>
			</h2>
		</div>

		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper" role="grid">    
					<table class="styled" >
						<thead>
							<tr>

							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' >Nama Sekolah: </td>
								<td >SMA NEGERI 1 Indramayu</td>
							
							</tr>
							<tr>
								<td width='' >Nomor Statistik Sekolah (NSS)</td>
								<td >301021816001</td>
							</tr>
							<tr>
								<td width='' >No Identitas Sekolah</td>
								<td >3</td>

							</tr>
							<tr>
								<td width='' >Status Sekolah</td>
								<td >Negeri</td>

							</tr>
							<tr>
								<td width='' >Tanggal Berdiri</td>
								<td >29 Agustus 1961</td>

							</tr>
							<tr>
								<td width='' >SK Pendirian</td>
								<td >135/SK/B/1961</td>

							</tr>
							<tr>
								<td width='' >Alamat</td>
								<td >Jl. Letjen MT. Haryono, Kec. SINDANG, Kab. KAB. INDRAMAYU, Provinsi. JAWA BARAT </td>

							</tr>
							<tr>
								<td width='' >Kode Pos</td>
								<td >45222</td>

							</tr>
							<tr>
								<td width='' >No Telepon</td>
								<td >0234272089</td>

							</tr>
							<tr>
								<td width='' >Fax</td>
								<td >0234275315</td>

							</tr>
							<tr>
								<td width='' >E-mail</td>
								<td >smanegeri1_sindang@yahoo.co.id</td>

							</tr>
							<tr>
								<td width='' >Website</td>
								<td >http://www.osissasi-b.blogspot.com</td>

							</tr>
							<tr>
								<td width='' >Luas Halaman</td>
								<td > 0 m2</td>

							</tr>
							<tr>
								<td width='' >Luas Tanah</td>
								<td >14521 m2</td>

							</tr>
							<tr>
								<td width='' >Luas Bangunan</td>
								<td >5000 m2</td>

							</tr>
							<tr>
								<td width='' >Luas Lapangan Olahraga</td>
								<td >0 m2</td>

							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="box with-table">
		<div class="header">
			<h2>Denah Sekolah</h2>
		</div>
		<div class="content">
			<img width="800px" src="<?php echo base_url("extras/sekolah/denah1.jpg"); ?>" alt="">
		</div>
	</div>

	<div class="box with-table">
		<div class="header">
			<h2>Kepala Sekolah</h2>
		</div>
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper" role="grid"> 
					<table class="styled" >
						<thead>
							<tr>

							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' >Nama</td>
								<td >Dra. Hj. SULASTRI DJ, M.Pd</td>
							</tr>
							<tr>
								<td width='' >NIP</td>
								<td >1955051119</td>
							</tr>
							<tr>
								<td width='' s>Pangkat/Golongan</td>
								<td >Pembi/IVb</td>

							</tr>
							<tr>
								<td width='' >Pendidikan</td>
								<td >S2</td>

							</tr>
							<tr>
								<td width='' >No SK Pengangkatan</td>
								<td >824/Kep.284-BKD/2007</td>

							</tr>
							<tr>
								<td width='' >Tanggal Pengangkatan</td>
								<td >27 November 2007</td>

							</tr>
							<tr>
								<td width='' >TMT</td>
								<td >27 November 2007</td>

							</tr>
							<tr>
								<td width='' >Kode Pos</td>
								<td >45222</td>

							</tr>

						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>
