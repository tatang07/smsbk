<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekapitulasi Penghasilan Orang Tua</h2>
       </div>
	   <div class="box">
	   <div class="content" style="height: 250px;">
						<table class=chart data-type=pie data-donut=0>
							<thead>
								<tr>
									<th></th>
									<th>< Rp.500.000</th>
									<th>Rp.500.000 - Rp. 1.000.000</th>
									<th>Rp.1.000.000 - Rp.2.000.000</th>
									<th>Rp.2.000.000 - Rp.5.000.000</th>
									<th>> Rp.5.000.000</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th>< Rp.500.000</th>
									<td>10</td>
								</tr>	
								<tr>
									<th>Rp.500.000 - Rp. 1.000.000</th>
									<td>18</td>
								</tr>
								<tr>
									<th>Rp.1.000.000 - Rp.2.000.000</th>
									<td>64</td>
								</tr>
								<tr>
									<th>Rp.2.000.000 - Rp.5.000.000</th>
									<td>12</td>
								</tr>
								<tr>
									<th>> Rp.5.000.000</th>
									<td>3</td>
								</tr>
							</tbody>	
						</table>
					</div><!-- End of .content -->
				</div><!-- End of .box -->
	   
	   
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper" role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jenis Pekarjaan</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>< Rp.500.000</td>
								<td width='' class='center'>10</td>								
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>Rp.500.000 - Rp. 1.000.000</td>
								<td width='' class='center'>18</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>Rp.1.000.000 - Rp.2.000.000</td>
								<td width='' class='center'>64</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>Rp.2.000.000 - Rp.5.000.000</td>
								<td width='' class='center'>12</td>
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='center'> > Rp.5.000.000</td>
								<td width='' class='center'>3</td>
							</tr>
							<tr>
								<td width='' class='center'> </td>
								<td width='' class='center'> Jumlah</td>
								<td width='' class='center'>107</td>
							</tr>						
						</tbody>
					</table>
					
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
