<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Supervisi</h2>
       </div>
       <div class="tabletools">
			<div class="right">
				<a href=""><i class="icon-plus"></i> Tambah Supervisi</a> 
			  	<a href=""><i class="icon-print"></i> Cetak Ke File</a> 
			  <br/><br/>
            </div>
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Index Prestasi</th>
								<th>Waktu</th>
								<th>Nama Guru</th>
								<th>Mata Pelajaran</th>
								<th>Materi</th>
								<th>Kelas</th>
								<th>Term</th>
								<th>Aksi</th>
						
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='left'>1</td>
								<td width='' class='left'>2.75</td>
								<td width='' class='left'>2015-08-03 00:00:00</td>
								<td width='' class='left'>Diana Susanti, S.Si.</td>
								<td width='' class='left'>BIO310 - Biologi</td>
								<td width='' class='left'>Biologi Sebagai Sains</td>
								<td width='' class='left'>X-1</td>
								<td width='' class='left'>Semester Genap</td>
							
								<td width='100px' class='left'>
									<a href="<?php echo base_url('statis/det_supervisi');?>" class="button small grey tooltip" data-gravity="s"><i class="icon"></i>Detail</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							
							</tr>
							<tr>
								<td width='' class='left'>2</td>
								<td width='' class='left'>2.63</td>
								<td width='' class='left'>2015-08-26 00:00:00</td>
								<td width='' class='left'>Saptarini Kismawati, SS</td>
								<td width='' class='left'>SOS332 - Sosiologi</td>
								<td width='' class='left'>Sosiologi</td>
								<td width='' class='left'>X-1</td>
								<td width='' class='left'>Semester Ganjil</td>
							
								<td width='' class='left'>
									<a href="<?php echo base_url('statis/det_supervisi');?>" class="button small grey tooltip" data-gravity="s"><i class="icon"></i>Detail</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							
							</tr>
							<tr>
								<td width='' class='left'>3</td>
								<td width='' class='left'>3.45</td>
								<td width='' class='left'>2015-03-23 00:00:00</td>
								<td width='' class='left'>Dr Toni MT</td>
								<td width='' class='left'>BIO310 - Biologi</td>
								<td width='' class='left'>Struktur Sel</td>
								<td width='' class='left'>XII-IPA-2</td>
								<td width='' class='left'>Semester Genap</td>
							
								<td width='' class='left'>
									<a href="<?php echo base_url('statis/det_supervisi');?>" class="button small grey tooltip" data-gravity="s"><i class="icon"></i>Detail</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							
							</tr>
							<tr>
								<td width='' class='left'>4</td>
								<td width='' class='left'>2.45</td>
								<td width='' class='left'>2015-02-13 00:00:00</td>
								<td width='' class='left'>Dr Iip Ar MT</td>
								<td width='' class='left'>MTK310 - Matematika</td>
								<td width='' class='left'>Persamaan Lingkaran</td>
								<td width='' class='left'>XII-IPA-3</td>
								<td width='' class='left'>Semester Genap</td>
							
								<td width='' class='left'>
									<a href="<?php echo base_url('statis/det_supervisi');?>" class="button small grey tooltip" data-gravity="s"><i class="icon"></i>Detail</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							
							</tr>
							<tr>
								<td width='' class='left'>5</td>
								<td width='' class='left'>2.45</td>
								<td width='' class='left'>2015-01-16 00:00:00</td>
								<td width='' class='left'>Dr Yaya R, MT</td>
								<td width='' class='left'>TIK310 - Teknologi</td>
								<td width='' class='left'>Pengolah Kata</td>
								<td width='' class='left'>X-3</td>
								<td width='' class='left'>Semester Genap</td>
							
								<td width='' class='left'>
									<a href="<?php echo base_url('statis/det_supervisi');?>" class="button small grey tooltip" data-gravity="s"><i class="icon"></i>Detail</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							
							</tr>
							
							
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 5</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
