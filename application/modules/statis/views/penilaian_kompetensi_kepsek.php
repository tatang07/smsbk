<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar Penilaian Kompetensi Kepala Sekolah</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=80px align='left'><span class="text">Kepala Sekolah</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>Dra. Hj. Sulastri DJ. M.Pd</option>
									<option value=''>Drs. Imam Permana Hadi</option>
									<option value=''>Drs. H. Susanto DJ. M.Pd</option>
									<option value=''>Drs. Aceng Fikri</option>									
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>							
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Program Penilaian Penilai</th>
								<th>Penilai</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>Januari/2007 - Desember / 2007</td>					
								<td width='' class='center'>Dr. Bona Simanjuntak Msc</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>Januari/2008 - Desember / 2008</td>					
								<td width='' class='center'>Dra. Hj. Sulastri DJ. M.Pd</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>Januari/2009 - Desember / 2009</td>					
								<td width='' class='center'>Dra. Hj. Sulastri DJ. M.Pd</td>					
								<td width='20%' class='center'>								
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>								   </td>	
							</tr>																				
						</tbody>
					</table>
					<div class="footer">
					
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
