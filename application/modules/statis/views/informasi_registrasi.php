<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Informasi Registrasi Tahun Ajaran 2014 / 2015</h2>
       </div>
       <div class="tabletools">
			<div class="right">			  	
				<div class="right">
					<a href=""><i class="icon-edit"></i>Ubah Informasi</a> 
					<br/><br/>
				</div>
				<div class="right">
					<a href=""><i class="icon-print"></i>Cetak ke file</a> 
					<br/><br/>
				</div>
            </div>
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='left'>Registrasi dapat dilakukan pada tanggal 27 September 2014.</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
        </div>
    </div>										
</div>