<h1 class="grid_12">Jadwal</h1>
<div class="grid_12">
	<div class="box with-table">

		<div class="box tabbedBox">

			<div class="header">
				<h2>Jadwal Guru</h2>
				<ul>
					<li><a href="#t1-c1">Senin</a></li>
					<li><a href="#t1-c2">Selasa</a></li>
					<li><a href="#t1-c3">Rabu</a></li>
					<li><a href="#t1-c4">Kamis</a></li>
					<li><a href="#t1-c5">Jum'at</a></li>
					<li><a href="#t1-c6">Sabtu</a></li>
				</ul>
			</div><!-- End of .header -->

			<div class="content tabbed">

				<div id="t1-c1">
					<div class="tabletools">
						<div class="dataTables_filter">
							<form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
								<table>
									<tr>
										<th width=1px align='left'><span class="text">Guru:</span></th>
										<td width=100px align='left'>
											<select>
												<option value=''>Dr. Hasan</option>
												<option value=''>Dr. Toni</option>
												<option value=''>Iip S.Kom</option>
												<option value=''>Siswo Handoko M.T</option>
												<option value=''>Ghifari A S.T</option>
												<option value=''>Dr. Arif</option>
												<option value=''>Dr. Miftah Mizwar</option>
												<option value=''>Dr. Sutrisno</option>
											</select>
										</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>
											<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
										</td>
									</tr>
								</table>
							</form>
						</div>
						</div>
						
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">    
								<table class="styled" >
									<thead>
										<tr>
											<th>Waktu</th>
											<th>Ruang</th>
											<th>Mata Pelajaran</th>
											<th>Kelas</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width='30px' class='center'>07.00-08.30</td>
											<td width='100px' class='center'>R-X-1</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>X-1</td>
										</tr>
										<tr>
											<td width='30px' class='center'>08.30-10.00</td>
											<td width='100px' class='center'>R-XI-IPA-1</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>XI-IPA-1</td>
										</tr>
										<tr>
											<td width='30px' class='center'>10.00-10.30</td>
											<td width='100px' class='center'></td>
											<td width='200px'>ISTIRAHAT</td>
											<td width='200px'></td>
										</tr>
										<tr>
											<td width='30px' class='center'>10.30-12.00</td>
											<td width='100px' class='center'>R-XII-IPA-2</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>XII-IPA-2</td>
										</tr>
										<tr>
											<td width='30px' class='center'>12.00-12.30</td>
											<td width='100px' class='center'></td>
											<td width='200px'>ISTIRAHAT</td>
											<td width='200px'></td>
										</tr>
										<tr>
											<td width='30px' class='center'>12.30-14.00</td>
											<td width='100px' class='center'>R-X-3</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>X-3</td>
										</tr>

									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Mata Pelajaran: 4</div>
									<div class="dataTables_paginate paging_full_numbers">
										<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
										<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
											<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
										</span>
										<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>

										<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="t1-c2">
					<div class="tabletools">
						<div class="dataTables_filter">
							<form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
								<table>
									<tr>
										<th width=1px align='left'><span class="text">Guru:</span></th>
										<td width=100px align='left'>
											<select>
												<option value=''>Dr. Hasan</option>
												<option value=''>Dr. Toni</option>
												<option value=''>Iip S.Kom</option>
												<option value=''>Siswo Handoko M.T</option>
												<option value=''>Ghifari A S.T</option>
												<option value=''>Dr. Arif</option>
												<option value=''>Dr. Miftah Mizwar</option>
												<option value=''>Dr. Sutrisno</option>
											</select>
										</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>
											<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
										</td>
									</tr>
								</table>
							</form>
						</div>
						</div>
						
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">    
								<table class="styled" >
									<thead>
										<tr>
											<th>Waktu</th>
											<th>Ruang</th>
											<th>Mata Pelajaran</th>
											<th>Kelas</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width='30px' class='center'>07.00-08.30</td>
											<td width='100px' class='center'>R-X-1</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>X-1</td>
										</tr>
										<tr>
											<td width='30px' class='center'>08.30-10.00</td>
											<td width='100px' class='center'>R-XI-IPA-1</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>XI-IPA-1</td>
										</tr>
										<tr>
											<td width='30px' class='center'>10.00-10.30</td>
											<td width='100px' class='center'></td>
											<td width='200px'>ISTIRAHAT</td>
											<td width='200px'></td>
										</tr>
										<tr>
											<td width='30px' class='center'>10.30-12.00</td>
											<td width='100px' class='center'>R-XII-IPA-2</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>XII-IPA-2</td>
										</tr>
										<tr>
											<td width='30px' class='center'>12.00-12.30</td>
											<td width='100px' class='center'></td>
											<td width='200px'>ISTIRAHAT</td>
											<td width='200px'></td>
										</tr>
										<tr>
											<td width='30px' class='center'>12.30-14.00</td>
											<td width='100px' class='center'>R-X-3</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>X-3</td>
										</tr>

									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Mata Pelajaran: 4</div>
									<div class="dataTables_paginate paging_full_numbers">
										<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
										<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
											<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
										</span>
										<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>

										<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div id="t1-c3">
					<div class="tabletools">
						<div class="dataTables_filter">
							<form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
								<table>
									<tr>
										<th width=1px align='left'><span class="text">Guru:</span></th>
										<td width=100px align='left'>
											<select>
												<option value=''>Dr. Hasan</option>
												<option value=''>Dr. Toni</option>
												<option value=''>Iip S.Kom</option>
												<option value=''>Siswo Handoko M.T</option>
												<option value=''>Ghifari A S.T</option>
												<option value=''>Dr. Arif</option>
												<option value=''>Dr. Miftah Mizwar</option>
												<option value=''>Dr. Sutrisno</option>
											</select>
										</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>
											<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
										</td>
									</tr>
								</table>
							</form>
						</div>
						</div>
						
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">    
								<table class="styled" >
									<thead>
										<tr>
											<th>Waktu</th>
											<th>Ruang</th>
											<th>Mata Pelajaran</th>
											<th>Kelas</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width='30px' class='center'>07.00-08.30</td>
											<td width='100px' class='center'>R-X-1</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>X-1</td>
										</tr>
										<tr>
											<td width='30px' class='center'>08.30-10.00</td>
											<td width='100px' class='center'>R-XI-IPA-1</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>XI-IPA-1</td>
										</tr>
										<tr>
											<td width='30px' class='center'>10.00-10.30</td>
											<td width='100px' class='center'></td>
											<td width='200px'>ISTIRAHAT</td>
											<td width='200px'></td>
										</tr>
										<tr>
											<td width='30px' class='center'>10.30-12.00</td>
											<td width='100px' class='center'>R-XII-IPA-2</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>XII-IPA-2</td>
										</tr>
										<tr>
											<td width='30px' class='center'>12.00-12.30</td>
											<td width='100px' class='center'></td>
											<td width='200px'>ISTIRAHAT</td>
											<td width='200px'></td>
										</tr>
										<tr>
											<td width='30px' class='center'>12.30-14.00</td>
											<td width='100px' class='center'>R-X-3</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>X-3</td>
										</tr>

									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Mata Pelajaran: 4</div>
									<div class="dataTables_paginate paging_full_numbers">
										<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
										<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
											<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
										</span>
										<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>

										<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div id="t1-c4">
					<div class="tabletools">
						<div class="dataTables_filter">
							<form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
								<table>
									<tr>
										<th width=1px align='left'><span class="text">Guru:</span></th>
										<td width=100px align='left'>
											<select>
												<option value=''>Dr. Hasan</option>
												<option value=''>Dr. Toni</option>
												<option value=''>Iip S.Kom</option>
												<option value=''>Siswo Handoko M.T</option>
												<option value=''>Ghifari A S.T</option>
												<option value=''>Dr. Arif</option>
												<option value=''>Dr. Miftah Mizwar</option>
												<option value=''>Dr. Sutrisno</option>
											</select>
										</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>
											<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
										</td>
									</tr>
								</table>
							</form>
						</div>
						</div>
						
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">    
								<table class="styled" >
									<thead>
										<tr>
											<th>Waktu</th>
											<th>Ruang</th>
											<th>Mata Pelajaran</th>
											<th>Kelas</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width='30px' class='center'>07.00-08.30</td>
											<td width='100px' class='center'>R-X-1</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>X-1</td>
										</tr>
										<tr>
											<td width='30px' class='center'>08.30-10.00</td>
											<td width='100px' class='center'>R-XI-IPA-1</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>XI-IPA-1</td>
										</tr>
										<tr>
											<td width='30px' class='center'>10.00-10.30</td>
											<td width='100px' class='center'></td>
											<td width='200px'>ISTIRAHAT</td>
											<td width='200px'></td>
										</tr>
										<tr>
											<td width='30px' class='center'>10.30-12.00</td>
											<td width='100px' class='center'>R-XII-IPA-2</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>XII-IPA-2</td>
										</tr>
										<tr>
											<td width='30px' class='center'>12.00-12.30</td>
											<td width='100px' class='center'></td>
											<td width='200px'>ISTIRAHAT</td>
											<td width='200px'></td>
										</tr>
										<tr>
											<td width='30px' class='center'>12.30-14.00</td>
											<td width='100px' class='center'>R-X-3</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>X-3</td>
										</tr>

									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Mata Pelajaran: 4</div>
									<div class="dataTables_paginate paging_full_numbers">
										<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
										<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
											<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
										</span>
										<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>

										<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div id="t1-c5">
					<div class="tabletools">
						<div class="dataTables_filter">
							<form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
								<table>
									<tr>
										<th width=1px align='left'><span class="text">Guru:</span></th>
										<td width=100px align='left'>
											<select>
												<option value=''>Dr. Hasan</option>
												<option value=''>Dr. Toni</option>
												<option value=''>Iip S.Kom</option>
												<option value=''>Siswo Handoko M.T</option>
												<option value=''>Ghifari A S.T</option>
												<option value=''>Dr. Arif</option>
												<option value=''>Dr. Miftah Mizwar</option>
												<option value=''>Dr. Sutrisno</option>
											</select>
										</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>
											<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
										</td>
									</tr>
								</table>
							</form>
						</div>
						</div>
						
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">    
								<table class="styled" >
									<thead>
										<tr>
											<th>Waktu</th>
											<th>Ruang</th>
											<th>Mata Pelajaran</th>
											<th>Kelas</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width='30px' class='center'>07.00-08.30</td>
											<td width='100px' class='center'>R-X-1</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>X-1</td>
										</tr>
										<tr>
											<td width='30px' class='center'>08.30-10.00</td>
											<td width='100px' class='center'>R-XI-IPA-1</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>XI-IPA-1</td>
										</tr>
										<tr>
											<td width='30px' class='center'>10.00-10.30</td>
											<td width='100px' class='center'></td>
											<td width='200px'>ISTIRAHAT</td>
											<td width='200px'></td>
										</tr>
										<tr>
											<td width='30px' class='center'>10.30-12.00</td>
											<td width='100px' class='center'>R-XII-IPA-2</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>XII-IPA-2</td>
										</tr>
										<tr>
											<td width='30px' class='center'>12.00-12.30</td>
											<td width='100px' class='center'></td>
											<td width='200px'>ISTIRAHAT</td>
											<td width='200px'></td>
										</tr>
										<tr>
											<td width='30px' class='center'>12.30-14.00</td>
											<td width='100px' class='center'>R-X-3</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>X-3</td>
										</tr>

									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Mata Pelajaran: 4</div>
									<div class="dataTables_paginate paging_full_numbers">
										<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
										<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
											<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
										</span>
										<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>

										<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div id="t1-c6">
					<div class="tabletools">
						<div class="dataTables_filter">
							<form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
								<table>
									<tr>
										<th width=1px align='left'><span class="text">Guru:</span></th>
										<td width=100px align='left'>
											<select>
												<option value=''>Dr. Hasan</option>
												<option value=''>Dr. Toni</option>
												<option value=''>Iip S.Kom</option>
												<option value=''>Siswo Handoko M.T</option>
												<option value=''>Ghifari A S.T</option>
												<option value=''>Dr. Arif</option>
												<option value=''>Dr. Miftah Mizwar</option>
												<option value=''>Dr. Sutrisno</option>
											</select>
										</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>
											<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
										</td>
									</tr>
								</table>
							</form>
						</div>
						</div>
						
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">    
								<table class="styled" >
									<thead>
										<tr>
											<th>Waktu</th>
											<th>Ruang</th>
											<th>Mata Pelajaran</th>
											<th>Kelas</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td width='30px' class='center'>07.00-08.30</td>
											<td width='100px' class='center'>R-X-1</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>X-1</td>
										</tr>
										<tr>
											<td width='30px' class='center'>08.30-10.00</td>
											<td width='100px' class='center'>R-XI-IPA-1</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>XI-IPA-1</td>
										</tr>
										<tr>
											<td width='30px' class='center'>10.00-10.30</td>
											<td width='100px' class='center'></td>
											<td width='200px'>ISTIRAHAT</td>
											<td width='200px'></td>
										</tr>
										<tr>
											<td width='30px' class='center'>10.30-12.00</td>
											<td width='100px' class='center'>R-XII-IPA-2</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>XII-IPA-2</td>
										</tr>
										<tr>
											<td width='30px' class='center'>12.00-12.30</td>
											<td width='100px' class='center'></td>
											<td width='200px'>ISTIRAHAT</td>
											<td width='200px'></td>
										</tr>
										<tr>
											<td width='30px' class='center'>12.30-14.00</td>
											<td width='100px' class='center'>R-X-3</td>
											<td width='200px'>A003-Matematika</td>
											<td width='200px'>X-3</td>
										</tr>

									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Mata Pelajaran: 4</div>
									<div class="dataTables_paginate paging_full_numbers">
										<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
										<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
											<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
										</span>
										<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>

										<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div><!-- End of .content -->

			</div><!-- End of .box -->
		</div>
	</div>
