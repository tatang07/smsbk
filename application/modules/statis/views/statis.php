<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class statis extends Simple_Controller {
    	
		public function mod($view="contoh"){			
			render($view);
		}
		
		public function rpp(){			
			render('rpp');
		}
		
		public function daftar_organisasi_siswa(){			
			render('daftar_organisasi_siswa');
		}
		
		public function struktur_organisasi_siswa(){			
			render('struktur_organisasi_siswa');
		}
		
		public function susunan_pengurus(){			
			render('susunan_pengurus');
		}
		
		public function kegiatan(){
			render('kegiatan');
		}
		
		public function p_ortu(){			
			render('p_ortu');
		}
		
		public function silabus(){			
			render('silabus');
		}
		
		public function ketersediaan_silabus(){			
			render('ketersediaan_silabus');
		}
		
		public function struktur_kurikulum(){			
			render('struktur_kurikulum');
		}
		
		public function agenda_kelas(){			
			render('agenda_kelas');
		}
		
		public function kehadiran_guru(){			
			render('kehadiran_guru');
		}
		public function jadwal_ekskul(){			
			render('jadwal_ekskul');
		}		
		
		public function ekskul_peserta(){			
			render('ekskul_peserta');
		}
		public function rks(){			
			render('rks');
		}
		public function ekskul_prestasi(){			
			render('ekskul_prestasi');
		}
		public function jenis_ekstrakurikuler(){			
			render('jenis_ekstrakurikuler');
		}
		public function det_je(){			
			render('det_je');
		}
		public function pengembangan_diri(){
			render('pengembangan_diri');
		}
		public function peserta_pengembangan_diri(){
			render('peserta_pengembangan_diri');
		}
		public function penanggungjawab_pengembangan_diri(){
			render('penanggungjawab_pengembangan_diri');
		}
		public function jadwal_pengembangan_diri(){
			render('jadwal_pengembangan_diri');
		}
		public function fasilitas_pengembangan_diri(){	
			render('fasilitas_pengembangan_diri');
		}
		public function prestasi_pengembangan_diri(){
			render('prestasi_pengembangan_diri');
		}

		public function fasilitas_ekstrakurikuler(){	
			render('fasilitas_ekstrakurikuler');
		}
		
		public function rekap_hasil_ortu(){
			render('rekap_hasil_ortu');
		}

		public function proporsi_gender(){			
			render('proporsi_gender');
		}

		public function pps(){			
			render('pps');
		}
		public function det_pps(){			
			render('det_pps');
		}
		public function struktur_organisasi_sekolah(){			
			render('struktur_organisasi_sekolah');
		}
		
		public function pps_hobi(){			
			render('pps_hobi');
		}

		public function pps_agama(){			
			render('pps_agama');
		}
		
		public function ptk_kualifikasi_pendidikan(){
			render('ptk_kualifikasi_pendidikan');
		}
		
		public function ptk_penugasan_dan_penentuan_beban_mengajar(){
			render('ptk_penugasan_dan_penentuan_beban_mengajar');
		}
		public function pekerjaan_ortu(){			
			render('pekerjaan_ortu');
		}
		public function identitas_tenaga_pendidik(){			
			render('identitas_tenaga_pendidik');
		}
		public function det_identitas_tenaga_pendidik(){			
			render('det_identitas_tenaga_pendidik');
		}
		public function identitas_tenaga_kependidikan(){			
			render('identitas_tenaga_kependidikan');
		}
		public function det_identitas_tenaga_kependidikan(){			
			render('det_identitas_tenaga_kependidikan');
		}

		public function kualifikasi_pangkat(){			
			render('kualifikasi_pangkat');
		}
		public function gaji(){			
			render('gaji');
		}		

		public function jumlah_siswa(){			
			render('jumlah_siswa');
		}
		
		public function kompetensi_kepsek(){			
			render('kompetensi_kepsek');
		}
		
		public function jadwal(){			
			render('jadwal');
		}		
		public function persyaratan_menjadi_kepala_sekolah(){			
			render('persyaratan_menjadi_kepala_sekolah');
		}		
		public function kebijakan_sekolah(){			
			render('kebijakan_sekolah');
		}

		public function pertemuan_kepsek(){			
			render('pertemuan_kepsek');
		}
		public function program_kerja_tahunan(){
			render('program_kerja_tahunan');
		}
		public function kebijakan_nasional(){
			render('kebijakan_nasional');
		}

		public function jadwal_kelas(){			
			render('jadwal_kelas');
		}
		public function jadwal_guru(){			
			render('jadwal_guru');
		}
		public function kompetensi_guru(){			
			render('kompetensi_guru');
		}
		public function identitas_kepsek(){
			render('identitas_kepsek');
		}
		
		public function visi_misi(){
			render('visi_misi');
		}
		
		public function grafik_penerima_beasiswa(){
			render('grafik_penerima_beasiswa');
		}
		
		public function rekap_penerima_keringanan_pembayaran(){
			render('rekap_penerima_keringanan_pembayaran');
		}
		
		public function rekap_tunggakan_dspb(){
			render('rekap_tunggakan_dspb');
		}

		public function penilaian_kompetensi_kepsek(){
			render('penilaian_kompetensi_kepsek');
		}
		public function kalender_akademik(){
			render('kalender_akademik');
		}
		public function laporan_dana_sumbangan(){
			render('laporan_dana_sumbangan');
		}		
		public function rekap_pembayaran_dspb(){
			render('rekap_pembayaran_dspb');
		}		
		public function rekap_keluar_masuk(){
			render('rekap_keluar_masuk');
		}
		public function DP3(){			
			render('DP3');
		}
		public function rincian_minggu_efektif(){			
			render('rincian_minggu_efektif');
		}
		
		public function rekap_penerima_beasiswa(){			
			render('rekap_penerima_beasiswa');
		}
		
		public function rapbs(){			
			render('rapbs');
		}
		
		public function pps_mutasi_siswa(){			
			render('pps_mutasi_siswa');
		}
		
		public function pembukuan(){			
			render('pembukuan');
		}
		
		public function penilaian_kinerja_guru(){			
			render('penilaian_kinerja_guru');
		}
		
		public function detail_penilaian_kinerja_guru(){			
			render('detail_penilaian_kinerja_guru');
		}
		
		public function angka_melanjutkan(){			
			render('angka_melanjutkan');
		}
		public function keadaan_peserta_didik(){			
			render('keadaan_peserta_didik');
		}

		public function supervisi(){			
			render('supervisi');
		}
		public function det_supervisi(){			
			render('det_supervisi');
		}

		public function DP3_guru(){			
			render('DP3_guru');
		}
		public function DP3_guru_detail(){			
			render('DP3_guru_detail');
		}

		public function profil_sekolah(){			
			render('profil_sekolah');
		}		
		public function pps_bpbk(){			
			render('pps_bpbk');
		}	
		public function angka_melanjutkan_detail(){			
			render('angka_melanjutkan_detail');
		}
		
		public function evaluasi_ujian_sekolah(){			
			render('evaluasi_ujian_sekolah');
		}
		
		public function evaluasi_ujian_nasional(){			
			render('evaluasi_ujian_nasional');
		}
		
		public function evaluasi_komponen_penilaian(){			
			render('evaluasi_komponen_penilaian');
		}
		
		public function evaluasi_kkm(){			
			render('evaluasi_kkm');
		}		
		public function evaluasi_detail_evaluasi_kelas(){			
			render('evaluasi_detail_evaluasi_kelas');
		}
		public function DP3_detail(){			
			render('DP3_detail');
		}
		public function nilai_unas(){			
			render('nilai_unas');
		}
		public function buku_rapor(){			
			render('buku_rapor');
		}
		public function rekap_lulusan(){
			render('rekap_lulusan');
		}
		public function evaluasi_kelas(){
			render('evaluasi_kelas');
		}
		public function penilaian_deskriptif(){
			render('penilaian_deskriptif');
		}
		public function pdftj(){
			render('pdftj');
		}
		public function jobdesc(){
			render('jobdesc');
		}
		public function daftar_calon_siswa(){
			render('daftar_calon_siswa');
		}
		public function pengkelasan_siswa_baru(){
			render('pengkelasan_siswa_baru');
		}
		public function hasil_un(){
			render('hasil_un');
		}
		public function hasil_us(){
			render('hasil_us');
		}

		public function informasi_daftar(){
			render('informasi_daftar');
		}
		public function informasi_umum(){
			render('informasi_umum');
		}

		public function form_daftar_ppdb(){
			render('form_daftar_ppdb');
		}
		
		public function informasi_registrasi(){
			render('informasi_registrasi');
		}
		
		public function dokumen_kurikulum(){
			render('dokumen_kurikulum');
		}

		public function program(){
			render('program');
		}

		public function PPDB_info_umum(){
			render('PPDB_info_umum');
		}
		
		public function PPDB_info_pendaftaran(){
			render('PPDB_info_pendaftaran');
		}

		public function PPDB_form_pendaftaran(){
			render('PPDB_form_pendaftaran');
		}

		public function PPDB_daftar_calon_siswa(){
			render('PPDB_daftar_calon_siswa');
		}

		public function PPDB_hasil_penerimaan_siswa(){
			render('PPDB_hasil_penerimaan_siswa');
		}

		public function PPDB_regestrasi_calon_siswa(){
			render('PPDB_regestrasi_calon_siswa');
		}

		public function PPDB_pengkelasan_siswa_baru(){
			render('PPDB_pengkelasan_siswa_baru');
		}

		public function PPDB_informasi_registrasi(){
			render('PPDB_informasi_registrasi');
		}
		
		public function pps_siswa_berprestasi(){
			render('pps_siswa_berprestasi');
		}
}
