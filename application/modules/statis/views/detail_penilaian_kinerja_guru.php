<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
	
       <div class="header">
            <h2>Detail Penilaian Kinerja Guru</h2>
       </div>
	   
	   <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-print"></i>Cetak File</a> 
			  	<a href=""><i class="icon-plus"></i>Kompetisi Guru</a> 
			  <br/><br/>
            </div>
            </div>
			
        <div class="content">
		<table width="100%">
	<tbody><tr>
		<td align="right"><b>JANGKA WAKTU PENILAIAN</b></td>
	</tr>
	<tr>
		<td align="right">BULAN,Desember s/d Desember		2014		</td>
	</tr>
</tbody></table>
<div class="header">
<h2>YANG DINILAI</h2>
</div>
<table class="Design">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<tbody>
<tr>
	<td>a. Nama</td><td>ACHMAD SUHADI, SPd</td>
</tr>
<tr class="even">
	<td>b. NIP</td><td>131286228</td>
</tr>	
<tr>
	<td>c. Pangkat,golongan ruang</td><td>Pembi, IVa</td>
</tr>
</table>
<div class="header">
<h2>PEJABAT PENILAI</h2>
</div>
<table class="Design">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<tbody>
<tr class="even">
	<td>a. Nama</td> <td>ACHMAD SUHADI, SPd</td>
</tr>
<tr>
	<td>b. NIP</td><td>131286228</td>
</tr>
<tr class="even">
	<td>c. Pangkat, golongan ruang</td><td>Pembi, IVa </td>
</tr>
</tbody>
					</table>
					<div class="header">
<h2>PENILAIAN</h2>
</div>
<table class="Design">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<tbody>
						<tr class="even">
		<td rowspan="2" align="center" valign="center">KOMPETENSI INTI</td>
		<td colspan="2" align="center">NILAI</td>
		<td rowspan="2" align="center" valign="center">KETERANGAN</td>
	</tr>
	<tr>
		<td align="center">ANGKA</td>		
		<td align="center">SEBUTAN</td>		
	</tr>
				<tr class="even">
			<td><b>
				Pedagogik</b>
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
				<tr>
					<td>Menguasai karakteristik peserta didik dari aspek fisik, moral, sosial, kultural, emosional, dan intelektual</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr class="even">
					<td>Menguasai teori belajar dan prinsip-prinsip pembelajaran yang mendidik</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr>
					<td>Mengembangkan kurikulum yang terkait dengan mata pelajaran yang diampu</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr class="even">
					<td>Menyelenggarakan pembelajaran yang mendidik</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr>
					<td>Memanfaatkan teknologi informasi dan komunikasi untuk kepentingan pembelajaran</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr class="even">
					<td>Memfasilitasi pengembangan potensi peserta didik untuk mengaktulisasikan berbagai potensi yang dimiliki</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr>
					<td>berkomunikasi secara efektif, empatik, dan santun dengan pesertad didik</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr class="even">
					<td>menyelenggarakan penilaian evaluasi proses dan hasil belajar</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr>
					<td>Memanfaatkan hasil penilaian dan evaluasi untk kepentingan pembelajaran</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr class="even">
					<td>Melakukan tindakan reflektif untuk peningkatan kualitas pembelajaran.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
					<tr>
			<td><b>
				Kepribadian</b>
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
					<td>Bertindak sesuai dengan norma agama, hukum, sosial dan kebudayaan nasional Indonesia.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr>
					<td>Menampilkan diri sebagai pribadi yang jujur, berkahlak mulia, dan teladan abgi peserta didik dan masyarakat.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr class="even">
					<td>Menampilkan diri sebagai pribadi yang mantap, stabil, dewasa, arif dan berwibawa.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr>
					<td>Menunjukkan etos kerja, tanggung jawab yang tinggi, rasa bangga menjadi guru, dan rasa percaya diri.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr class="even">
					<td>Menjunjung tinggi kode etik profesi guru.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
					<tr class="even">
			<td><b>
				Sosial</b>
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
					<td>Bersikap inklusif, bertindak objektif, serta tidak diskriminatif karena pertimbangan jenis kelamin, agama, ras, kondisi fisik, latar belakang keluarga, dan status sosial ekonomi.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr>
					<td>Berkomunikasi secara efektif,empatif,dan santun dengan sesama pendidik, tenaga kependidikan, orang tua, dan masyarakat.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr class="even">
					<td>Beradaptasi di tempat bertugas di seluruh wilayah Republik Indonesia yang memiliki keragaman sosial budaya.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr>
					<td>Berkomunikasi dengan komunikasi profesi sendiri dan profesi lain secara lisan dan tulisan atau bentuk lain.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
					<tr>
			<td><b>
				Profesional</b>
			</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
				</tr>
				<tr class="even">
				</tr>
				<tr>
					<td>Menguasai materi, struktur, konsep, dan pola pikir keilmuan yang mendukung mata pelajaran yang diampu.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr class="even">
					<td>Menguasai standar kompetensi dan kompetensi dasar mata pelajaran yang diampu.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr>
					<td>Mengembangkan materi pembelajaran yang diampu secara kreatif</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr class="even">
					<td>Mengembangkan keprofesionalan secara bekelanjutan dengan melakukan tindakan reflektif</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr>
					<td>Memanfaatkan teknologi informasi dan komunikasi untuk mengembangkan diri.</td>
			<td>0 </td>
			
			<td align="center">
				kurang</td>
			<td></td>
					</tr>
				<tr class="even">
		<td align="center"><b>Jumlah</b></td>
		<td>0</td>
		<td>-</td>
		<td>-</td>
	</tr>
	<tr>
		<td align="center"><b>Rata-rata</b></td>
		<td>0</td>
		<td align="center">kurang		</td>
		<td>-</td>
	</tr>
	</tbody></table>
				<div class="header">
<h2>Kesimpulan</h2>
</div>
<table class="Design">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<tbody><tr>
						<td>Tingkatkan lagi kinerjanya</td></tr>
						</tbody>
						</table>
				</div>
				</div>
				</div>
            </div>
        </div>        
    </div>
 </div>

