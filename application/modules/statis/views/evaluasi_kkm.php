<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>KKM Mata Pelajaran</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-plus"></i>Tambah</a> 
			  <br/><br/>
            </div>
            
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=50px align='left'><span class="text">Kelas</span></th>
							<td width=100px align='left'>
								<select>
								<option>X - 1</option>
								<option>X - 2</option>
								<option>X - 3</option>
								<option>X - 4</option>
								<option>X - 5</option>
								<option>X - 6</option>
								<option>X - 7</option>
								<option>XI IPA - 1</option>
								<option>XI IPA - 2</option>
								<option>XI IPA - 3</option>
								<option>XI IPA - 4</option>
								<option>XI IPS - 1</option>
								<option>XI IPS - 2</option>
								<option>XI IPS - 3</option>
								<option>XII IPA - 1</option>
								<option>XII IPA - 2</option>
								<option>XII IPA - 3</option>
								<option>XII IPA - 4</option>
								<option>XII IPS - 1</option>
								<option>XII IPS - 2</option>
								<option>XII IPS - 3</option>
								</select>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode Matpel</th>
								<th>Matpel</th>
								<th>Guru</th>
								<th>KKM</th>
								<th>Dokumen KKM</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='100px'>A001</td>
								<td width='300px'>Pendidikan Agama dan Budi Pekerti</td>
								<td width='100px'>Dr. Toni</td>
								<td width='50px' class='center'>70</td>
								<td width='100px' class='center'><a href="#"><i class="icon-download"></i> Download</a></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width=''>A002</td>
								<td width=''>Pendidikan Pancasila dan Kewarganegaraan</td>
								<td width=''>Dr. Hisyam</td>
								<td width='' class='center'>70</td>
								<td width='' class='center'><a href="#"><i class="icon-download"></i> Download</a></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width=''>A003</td>
								<td width=''>Matematika</td>
								<td width=''>Dr. Nurhasan</td>
								<td width='' class='center'>70</td>
								<td width='' class='center'></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width=''>A004</td>
								<td width=''>Bahasa Indonesia</td>
								<td width=''>Dr. Iip</td>
								<td width='' class='center'>70</td>
								<td width='' class='center'><a href="#"><i class="icon-download"></i> Download</a></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width=''>A005</td>
								<td width=''>Bahasa inggris</td>
								<td width=''>Dr. Handoko</td>
								<td width='' class='center'>70</td>
								<td width='' class='center'><a href="#"><i class="icon-download"></i> Download</a></td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 5</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
