<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Pembebasan</h2> <?php $g=get_jenjang_sekolah(); //echo $g; ?>
       </div>
	   <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pembiayaan/rekap_dspb/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			<div class="dataTables_filter">                
					<table>
						<tr>
							<th width=70px align='left'><span class="text">Golongan</span></th>
							<td width=50px align='left'>
								<?php if($g==4){ ?>
								<select>
									<option value=''>X</option>
									<option value=''>XI</option>
									<option value=''>XII</option>
								</select>
								<?php }elseif($g==3){?>
								<select>
									<option value=''>VII</option>
									<option value=''>VIII</option>
									<option value=''>IX</option>
								</select>
								<?php }elseif($g==2){?>
								<select>
									<option value=''>I</option>
									<option value=''>II</option>
									<option value=''>III</option>
									<option value=''>IV</option>
									<option value=''>V</option>
									<option value=''>VI</option>
								</select>
								<?php } ?>					
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
							
							</td>
						</tr>
					</table>               
            </div>
       </div>            
        </div>
        <div class="content" style="border: 1px solid #bbb">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<?php if($g==4){?>
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Aksi</th>
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td width='20px' class='center'>1</td>
								<td width='' class='center'>124432124</td>								
								<td width='' class='center'>Kunto Aji</td>
								<td width='100px' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>123451123</td>								
								<td width='' class='center'>Aji Saka</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>							
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>124689821</td>								
								<td width='' class='center'>Asep Pradana</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>	
						</tbody>
					</table>
					<?php }elseif($g==3){?>
							<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Aksi</th>
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td width='20px' class='center'>1</td>
								<td width='' class='center'>123354677</td>								
								<td width='' class='center'> Rudi Pradana</td>
								<td width='100px' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>124689821</td>								
								<td width='' class='center'>Asep Budi</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>							
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>124683452</td>								
								<td width='' class='center'>Siti Sarah</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>	
						</tbody>
					</table>
					
					<?php }elseif($g==2){ ?>
							<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Aksi</th>								
							</tr>
						</thead>
						
						<tbody>
							<tr>
								<td width='20px' class='center'>1</td>
								<td width='' class='center'>124683452</td>								
								<td width='' class='center'>Alexcis</td>
								<td width='100px' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>124612352</td>								
								<td width='' class='center'>Bobi</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>							
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>124612352</td>								
								<td width='' class='center'>Ken Turi</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>12400252</td>								
								<td width='' class='center'>Novi Setiawan</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='center'>12345672</td>								
								<td width='' class='center'>Sarah</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>		
							<tr>
								<td width='' class='center'>6</td>
								<td width='' class='center'>124681123</td>								
								<td width='' class='center'>Saep</td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit</a>
									<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>								
						</tbody>
					</table>					
					<?php }?>
				</div>
            </div>
        </div>        
    </div>
 </div>
