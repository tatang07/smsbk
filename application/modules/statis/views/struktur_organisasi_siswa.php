<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Struktur Organisasi Siswa</h2>
       </div>
       <div class="tabletools">
			<div class="right">			  	
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                
					<table>
						<tr>													
							<th width=100px align='left'><span class="text">Kata Kunci:</span></th>
							<td width=100px align='left'><input type='text'></td>
							<td>&nbsp;&nbsp;&nbsp;&nbsp;</td>
							<th width=100px align='left'><span class="text">Organisasi</span></th>
							<td width=100px align='left'>
											<select>
												<option value=''>OSIS</option>
												<option value=''>Pramuka</option>
											</select>
										</td>
							<td><a class="button " data-gravity="s">Cari</a></td>
						</tr>												
					</table>                
            </div>         			
        </div>			
    </div>										
</div>
<div class="grid_12">
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">   
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jabatan</th>								
								<th>Aksi</th>				
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>								
								<td width='' class='center'>Ketua Umum</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> delete</a>
								</td>
							</tr>						
							<tr>
								<td width='' class='center'>2</td>								
								<td width='' class='center'>Sekretaris</td>								
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> delete</a>
								</td>
							</tr>
						</tbody>
					</table>					
				</div>
            </div>
        </div>        
 </div>
					<td><a class="button " data-gravity="s">Home</a></td>
					<td><a class="button " data-gravity="s">Tambah Struktur</a></td>
					<td><a class="button " data-gravity="s">Cetak Ke File</a></td>
    </div>