<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
       	<div class="box">
				
					<div class="header">
						<h2>Kompetensi Kepala Sekolah</h2>
					</div>
					
					<div class="accordion toggle">
					
						<h3><a href="#">Kepribadian</a></h3>
						<div>
							<ul>
							
								<li>Berakhlak mulia, mengembangkan budaya dan tradisi akhlak mulia, dan menjadi teladan akhlak mulia bagi komunitas di Sekolah/Madrasah</li>
								<li>Memiliki integritas kepribadian sebagai pemimpin</li>
								<li>Memiliki keinginan yang kuat dalam pengembangan diri sebagai kepala sekolah/madrasah</li>
								<li>Bersikap terbuka dalam melaksanakan tugas pokok dan fungsi</li>
								<li>Mengendalikan diri dalam menghadapi masalah dalam pekerjaan sebagai kepala sekolah/madrasah</li>
								<li>memiliki bakat dan minat jabatan sebagai pemimpin pendidikan</li>
							</ul>
						</div>
						
						<h3><a href="#">Manajerial</a></h3>
						<div>
							<ul>
							
								<li>Menyusun perencanaan sekolah/madrasah untuk berbagai tingkatan perencanaan</li>
								<li>Mengembangkan organisasi sekolah/madrasah sesuai dengan kebutuhan</li>
								<li>memimpin sekolah/madrasah dalam rangka pendayagunaan sumberdaya sekolah/madrasah secara optimal</li>
								<li>Mengelola perubahan dan pengembangan sekolah/madrasah menuju organisasi pembelajar yang efektif</li>
								<li>Menciptakan budaya dan iklim sekolah/marasah yang kondusif dan inovatif bagi pembelajaran peserta didik</li>
								<li>Mengelola guru dan staf dalam rangka pendayagunaan sumberdaya manusia secara optimal</li>
								<li>Mengelola sarana dan prasarana sekolah/madrasah dalam rangka pendayagunaan secara optimal</li>
								<li>Mengelola hubungan sekolah/madrasah dan masyarakat dalam rangka pencarian dukungan ide, sumber belajar, dan pembiayaan sekolah/madrasah</li>
								<li>Mengelola peserta didik dalam rangka penerimaan peserta didik baru, dan penempatan dan pengembangan kapasitas peserta didik</li>
								<li>Mengelola pengembangan kurikulum dan kegiatan pembelajaran sesuai dengan arah dan tujuan pendidikan nasional</li>
								<li>Mengelola keuangan sekolah/madrasah sesuai dengan prinsip pengelolaan yang akuntabel, transparan, dan efesien</li>
								<li>Mengelola ketatausahaan sekolah/madrasah dalam mendukung pencapaian tujuan sekolah/madrasah</li>
								<li>Mengelola unit layanan khusus sekolah/madrasah dalam mendukung kegiatan pembelajaran dan kegiatan peserta didik di sekolah/madrasah</li>
								<li>Mengelola sistem informasi sekolah/madrasah dalam mendukung penyususnan program dan pengambilan keputusan</li>
								<li>Memanfaatkan kemajuan teknologi informasi bagi peningkatan pembelajaran dan manajemen sekolah/madrasah</li>
								<li>Melakukan monitoring, evaluasi, dan pelaporan pelaksanaan program kegiatan sekolah/madrasah dengan prosedur yang tepat, serta merencanakan tindak lanjutnya</li>

							</ul>
						</div>
						
						<h3><a href="#">Kewirausahaan</a></h3>
						<div>
							<ul>
							
								<li>Menciptakan inovasi yang berguna bagi pengembangan sekolah/madrasah
								<li>Bekerja keras untuk mencapai keberhasilan sekolah/madrasah sebagai organisasi pembelajar yang efektif</li>
								<li>Memiliki motivasi untuk sukses dalam melaksanakan tugas pokok dan fungsinya sebagai pemimpin sekolah/madrasah</li>
								<li>Pantang menyerah dan selalu mencari solusi terbaik dalam menghadapi kendala yang dihadipi sekolah/madrasah</li>
								<li>Memiliki naluri kewirausahaan dalam mengelola kegiatan produksi/jasa sekolah/madrasah sebagai sumber belajar peserta didik</li>


							</ul>
						</div>
						
						<h3><a href="#">Supervisi</a></h3>
						<div>
							<ul>
							
								<li>Merencanakan program supervisi akademik dalam rangka peningkatan profesionalisme guru</li>
								<li>Melaksanakan supervisi akademik terhadap guru dengan menggunakan pendekatan dan tekhnik supervisi yang tepat</li>
								<li>Menindaklanjuti hasil supervisi akademik terhadap guru dalam rangka peningkatan progfesionalisme guru</li>


							</ul>
						</div>
						
						
						<h3><a href="#">Sosial</a></h3>
						<div>
							<ul>
							
								<li>Bekerjasama dengan pihak lain untuk kepentingan sekolah/madrasah</li>
								<li>Berpartisipasi dalam kegiatan sosial kemasyarakatan</li>
								<li>Memiliki kepekaan sosial terhadap orang atau kelompok lain</li>

							</ul>
						</div>
						
					</div><!-- End of .content -->
					
				</div><!-- End of .box -->		
 </div>
