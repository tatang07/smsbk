<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Hobi, Minat dan Bakat Siswa</h2>
       </div>
	   <div class="box">
	   <div class="content" style="height: 250px;">
			<table class=chart data-type=pie data-donut=0>
				<thead>
					<tr>
						<th></th>
						<th>Hobi</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<th>Belajar</th>
						<td>10</td>
					</tr>
					<tr>
						<th>Programming</th>
						<td>34</td>
					</tr>
					<tr>
						<th>Lintas Alam</th>
						<td>64</td>
					</tr>
					<tr>
						<th>Berkemah</th>
						<td>12</td>
					</tr>
					<tr>
						<th>Bermain Musik</th>
						<td>44</td>
					</tr>
				</tbody>	
			</table>
		</div><!-- End of .content -->
		</div><!-- End of .box -->
					
        <div class="content" style="border-top:1px solid #bbb">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Hobi, Minat dan Bakat Siswa</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='30px' class='center'>1</td>
								<td width='500px'>Belajar</td>
								<td width='50px' class='center'>10</td>
							</tr>
							<tr>
								<td width='30px' class='center'>2</td>
								<td width='500px'>Programming</td>
								<td width='50px' class='center'>34</td>
							</tr>
							<tr>
								<td width='30px' class='center'>3</td>
								<td width='500px'>Lintas Alam</td>
								<td width='50px' class='center'>64</td>
							</tr>
							<tr>
								<td width='30px' class='center'>4</td>
								<td width='500px'>Berkemah</td>
								<td width='50px' class='center'>12</td>
							</tr>
							<tr>
								<td width='30px' class='center'>5</td>
								<td width='500px'>Bermain Musik</td>
								<td width='50px' class='center'>44</td>
							</tr>
							<tr>
								<td width='30px' class='center'></td>
								<td width='500px'>Jumlah</td>
								<td width='50px' class='center'>164</td>
							</tr>
						</tbody>
					</table>
					
				</div>
            </div>
        </div>        
    </div>
 </div>
