<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Organisasi Siswa</h2>
       </div>
       <div class="tabletools">
			<div class="right">			  	
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                
					<table>
						<tr>													
							<th width=100px align='left'><span class="text">Kata Kunci:</span></th>
							<td width=100px align='left'><input type='text'></td>	
						<td><a class="button " data-gravity="s">Cari</a></td>
						</tr>												
					</table>                
            </div>         			
        </div>			
    </div>										
</div>
<div class="grid_12">
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">   
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Organisasi</th>
								<th>Inisial</th>								
								<th>Sekretariat</th>
								<th>Aksi</th>						
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>								
								<td width='' class='center'>Organisasi Intra Sekolah</td>								
								<td width='' class='center'>OSIS</td>								
								<td width='' class='center'>R34S</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> delete</a>
								</td>
							</tr>						
							<tr>
								<td width='' class='center'>2</td>								
								<td width='' class='center'>Pasukan Pengibar Bendera Pusaka</td>
								<td width='' class='center'>Paskibraka</td>								
								<td width='' class='center'>R33S</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> delete</a>
								</td>
							</tr>
						</tbody>
					</table>					
				</div>
            </div>
        </div>        
 </div>
					<td><a class="button " data-gravity="s">Home</a></td>
					<td><a class="button " data-gravity="s">Tambah Organisasi</a></td>
					<td><a class="button " data-gravity="s">Cetak Ke File</a></td>
    </div>