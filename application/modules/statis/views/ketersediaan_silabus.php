<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Ketersediaan Silabus</h2>
       </div>
       <div class="tabletools">
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=80px align='left'><span class="text">Jenis Kurikulum</span></th>
							<td width=200px align='left'>
								<select>
									<option value=''>Kurikulum 2013</option>
									<option value=''>Kurikulum KSTP</option>
								</select>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=3>No.</th>
								<th rowspan=3>Kode</th>
								<th rowspan=3>Mata Pelajaran</th>
								<th colspan=7>Ketersediaan Silabus</th>
							</tr>
							<tr>
								<th>X</td>
								<th colspan=3>XI</th>
								<th colspan=3>XII</th>
							</tr>
							<tr>
								<th>INTI</th>
								<th>IPA</th>
								<th>IPS</th>
								<th>BAHASA</th>
								<th>IPA</th>
								<th>IPS</th>
								<th>BAHASA</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='' class='center'>A001</td>
								<td>Pendidikan Agama dan Budi Pekerti</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width='' class='center'>A002</td>
								<td>Pendidikan Pancasila dan Kewarganegaraan</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width='' class='center'>A003</td>
								<td>Matematika</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width='' class='center'>A004</td>
								<td>Bahasa Indonesia</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
							</tr>
							<tr>
								<td width='' class='center'>5</td>
								<td width='' class='center'>A005</td>
								<td>Bahasa Inggris</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
							</tr>
							<tr>
								<td width='' class='center'>6</td>
								<td width='' class='center'>A006</td>
								<td>Sejarah Indonesia</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
							</tr>
							<tr>
								<td width='' class='center'>7</td>
								<td width='' class='center'>B001</td>
								<td>Kesenian</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
							</tr>
							<tr>
								<td width='' class='center'>8</td>
								<td width='' class='center'>B002</td>
								<td>Pendidikan Jasmani dan Kesehatan</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
							</tr>
							<tr>
								<td width='' class='center'>9</td>
								<td width='' class='center'>B003</td>
								<td>Prakarya dan Kewirausahaan</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
							</tr>
							<tr>
								<td width='' class='center'>10</td>
								<td width='' class='center'>C101</td>
								<td>Fisika</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
							</tr>
							<tr>
								<td width='' class='center'>11</td>
								<td width='' class='center'>C102</td>
								<td>Biologi</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
							</tr>
							<tr>
								<td width='' class='center'>12</td>
								<td width='' class='center'>C103</td>
								<td>Kimia</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
							</tr>
							<tr>
								<td width='' class='center'>13</td>
								<td width='' class='center'>C201</td>
								<td>Sejarah Dunia</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'><font color='red'>Tidak Ada</font></td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
							</tr>
							<tr>
								<td width='' class='center'>14</td>
								<td width='' class='center'>C202</td>
								<td>Geografi</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
							</tr>
							<tr>
								<td width='' class='center'>15</td>
								<td width='' class='center'>C203</td>
								<td>Ekonomi</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
							</tr>
							<tr>
								<td width='' class='center'>16</td>
								<td width='' class='center'>C204</td>
								<td>Sosiologi</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'><font color='red'>Tidak Ada</font></td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
							</tr>
							<tr>
								<td width='' class='center'>17</td>
								<td width='' class='center'>C301</td>
								<td>Antropologi</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'><font color='red'>Tidak Ada</font></td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'><font color='red'>Tidak Ada</font></td>
								<td width='' class='center'></td>
							</tr>
							<tr>
								<td width='' class='center'>18</td>
								<td width='' class='center'>C302</td>
								<td>Bahasa dan Sastra Inggris</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'><font color='red'>Tidak Ada</font></td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'><font color='red'>Tidak Ada</font></td>
								<td width='' class='center'></td>
							</tr>
							<tr>
								<td width='' class='center'>19</td>
								<td width='' class='center'>C303</td>
								<td>Bahasa Jerman</td>
								<td width='' class='center'>Ada</td>
								<td width='' class='center'></td>
								<td width='' class='center'><font color='red'>Tidak Ada</font></td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'><font color='red'>Tidak Ada</font></td>
								<td width='' class='center'></td>
							</tr>
							<tr>
								<td width='' class='center'>20</td>
								<td width='' class='center'>C304</td>
								<td>Bahasa Arab</td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'><font color='red'>Tidak Ada</font></td>
								<td width='' class='center'></td>
								<td width='' class='center'></td>
								<td width='' class='center'><font color='red'>Tidak Ada</font></td>
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 20</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
