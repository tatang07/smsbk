<h1 class="grid_12">Pendidik Dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Profil Guru</a>
       </div>
       <div class="tabletools">
	
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' >Nama Lengkap: </td>
								<td >ACHMAD SUHADI, SPD</td>
							</tr>
							<tr>
								<td width='' >NIP</td>
								<td >131286228</td>
							</tr>
							<tr>
								<td width='' >NUPTK</td>
								<td >6362736638200003</td>

							</tr>
							<tr>
								<td width='' >Kode Guru</td>
								<td >14</td>

							</tr>
							<tr>
								<td width='' >Status Sertifikasi</td>
								<td >Terse</td>

							</tr>
								<tr>
								<td width='' >Status Pegawai</td>
								<td >Tetap</td>

							</tr>
								<tr>
								<td width='' >Status Pernikahan</td>
								<td >Kawin</td>

							</tr>
								<tr>
								<td width='' >Golongan Darah</td>
								<td >A</td>

							</tr>
								<tr>
								<td width='' >Jenis Kelamin</td>
								<td >Laki-Laki</td>

							</tr>
								<tr>
								<td width='' >Tempat, Tanggal lahir</td>
								<td >Bandung, 30 Oktober 1958 </td>

							</tr>
								<tr>
								<td width='' >Agama</td>
								<td >Islam</td>

							</tr>
								<tr>
								<td width='' >Alamat</td>
								<td >Kec. INDRAMAYU, Kab. ACEH BARAT, Provinsi. BALI </td>

							</tr>
								</tr>
								<tr>
								<td width='' >No Handphone</td>
								<td > 08971273187</td>

							</tr>
								</tr>
								<tr>
								<td width='' >Tanggal Diangkat</td>
								<td >1 Maret 1983</td>

							</tr>
								</tr>
								<tr>
								<td width='' >Pangkat/Golongan</td>
								<td >Pembi/IVa</td>

							</tr>
							</tr>
								<tr>
								<td width='' >Gaji</td>
								<td >Rp 3000000</td>

							</tr>
							</tr>
								<tr>
								<td width='' >Pendidikan Terakhir</td>
								<td >TK</td>

							</tr>
						</tbody>
				</table>
				
				
				<div class="header">
					<h2>Mata Pelajaran Yang Diajarkan</a>
				</div>
					<table class="styled" >
						<thead>
							<tr>
								<th> No</th>
								<th> Kode Mata Pelajaran</th>
								<th> Mata Pelajaran</th>
								<th> Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>M201</td>
								<td>Matematika</td>
								<td class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>M202</td>
								<td>Kimia</td>
								<td class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
							<tr>
								<td>3</td>
								<td>M203</td>
								<td>Biologi</td>
								<td class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
						
						</tbody>
					</table>
					
				<div class="header">
				<h2>Riwayat Pendidikan</a>
				</div>
					<table class="styled" >
						<thead>
							<tr>
								<th> No</th>
								<th> Jenjang</th>
								<th> Nama Lembaga</th>
								<th> Program Studi</th>
								<th> Dari</th>
								<th> Sampai</th>
								<th> Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>SMA</td>
								<td>SMA Negeri Lampung</td>
								<td>SD</td>
								<td>1974</td>
								<td>1980</td>
								<td class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
						
						</tbody>
					</table>
					
			<div class="header">
				<h2>Riwayat Jabatan</a>
			</div>
					<table class="styled" >
						<thead>
							<tr>
								<th> No</th>
								<th> Jabatan </th>
								<th> No SK</th>
								<th> Tanggal SK</th>
								<th> Keterangan</th>
								<th> Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Guru</td>
								<td>1234</td>
								<td>10/12/1994</td>
								<td>Sempat bekerja di PT.Sanjaya</td>
								<td class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
						</tbody>
					</table>
					
				<div class="header">
					<h2>Penataran</a>
				</div>
					<table class="styled" >
						<thead>
							<tr>
								<th> No</th>
								<th> Penataran </th>
								<th> Tahun</th>
								<th> Lokasi</th>
								<th> Keterangan</th>
								<th> Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Guru</td>
								<td>1999</td>
								<td>Lampung</td>
								<td>sering Pentaran di banyak tempat</td>
								<td class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="header">
					<h2>Prestasi</a>
					</div>
					<table class="styled" >
						<thead>
							<tr>
								<th> No</th>
								<th> Prestasi </th>
								<th> Tahun</th>
								<th> Lokasi</th>
								
								<th> Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Juara Umum Catur</td>
								<td>1999</td>
								<td>Lampung</td>
								<td class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
						</tbody>
					</table>
					
					<div class="header">
					<h2>Pengalaman Lain</a>
					</div>
					<table class="styled" >
						<thead>
							<tr>
								<th> No</th>
								<th> Pengalaman </th>
								<th> Dari</th>
								<th> Sampai</th>
								<th> Lokasi</th>
								
								<th> Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>1</td>
								<td>Guru Les</td>
								<td>1987</td>
								<td>1988</td>
								<td>Lampung</td>
								<td class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Delete</a>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="footer">
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
