<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Komponen Evaluasi Kelas</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i class="icon-plus"></i>Tambah</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=50px align='left'><span class="text">Kelas</span></th>
							<td width=100px align='left'>
								<select>
								<option>X - 1</option>
								<option>X - 2</option>
								<option>X - 3</option>
								<option>X - 4</option>
								<option>X - 5</option>
								<option>X - 6</option>
								<option>X - 7</option>
								<option>XI IPA - 1</option>
								<option>XI IPA - 2</option>
								<option>XI IPA - 3</option>
								<option>XI IPA - 4</option>
								<option>XI IPS - 1</option>
								<option>XI IPS - 2</option>
								<option>XI IPS - 3</option>
								<option>XII IPA - 1</option>
								<option>XII IPA - 2</option>
								<option>XII IPA - 3</option>
								<option>XII IPA - 4</option>
								<option>XII IPS - 1</option>
								<option>XII IPS - 2</option>
								<option>XII IPS - 3</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<th width=80px align='left'><span class="text">Mata Pelajaran</span></th>
							<td width=100px align='left'>
								<select>
								<option>A001 Bahasa Inggris X</option>
								<option>A002 Penjas X</option>
								<option>A003 Bahasa Indonesia X</option>
								<option>A004 Matematika X</option>
								<option>B001 Matematika XI IPA</option>
								<option>B002 Kimia XI IPA</option>
								<option>B003 Biologi XI IPA</option>
								<option>B004 FIsika XI IPA</option>
								<option>B005 Sosiologi XI IPS</option>
								<option>B006 Geografi XI IPS</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>
							<th width=50px align='left'><span class="text">Komponen</span></th>
							<td width=100px align='left'>
								<select>
								<option>Pengetahuan</option>
								<option>Sikap</option>
								<option>Keterampilan</option>
								</select>
							</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<td>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Penilaian</th>
								<th>Bobot (%)</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td width='' class='center'>1</td>
								<td width='400px'>Tugas</td>
								<td width='100px'>20</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>2</td>
								<td width=''>Ulangan Harian</td>
								<td width=''>30</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>3</td>
								<td width=''>UTS</td>
								<td width=''>25</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<tr>
								<td width='' class='center'>4</td>
								<td width=''>UAS</td>
								<td width=''>25</td>
								<td width='' class='center'>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 4</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
