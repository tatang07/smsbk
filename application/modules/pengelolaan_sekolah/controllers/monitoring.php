<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class monitoring extends CI_Controller {

	public function index()
	{	
		$this->load->model('pengelolaan_sekolah/m_monitoring');
		$data['monitoring'] = $this->m_monitoring->get_data()->result_array();
		$data['component']="pengelolaan_sekolah";
		render('monitoring/datalist',$data);
	}

	
	public function load_add()
	{	
		$data['component']="pengelolaan_sekolah";
		render('monitoring/tambah',$data);
	}

	public function submit(){
		$this->load->model('pengelolaan_sekolah/m_monitoring');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('jenis_kegiatan', 'jenis Kegiatan', 'required');
		$this->form_validation->set_rules('waktu_mulai', 'Waktu Mulai', 'required');
		$this->form_validation->set_rules('waktu_akhir', 'Waktu Akhir', 'required');
		$this->form_validation->set_rules('petugas', 'Petugas', 'required');
		$this->form_validation->set_rules('target', 'Target', 'required');
			
		if($this->form_validation->run() == TRUE){
			$data['id_sekolah'] = get_id_sekolah();
			$data['id_tahun_ajaran'] = get_id_tahun_ajaran();
			$data['jenis_kegiatan'] = $this->input->post('jenis_kegiatan');
			$data['waktu_mulai'] = tanggal_db($this->input->post('waktu_mulai'));
			$data['waktu_akhir'] = tanggal_db($this->input->post('waktu_akhir'));
			$data['petugas'] = $this->input->post('petugas');
			$data['target'] = $this->input->post('target');

			$this->m_monitoring->submit($data);
			set_success_message('Data Berhasil Ditambah!');
			redirect(site_url('pengelolaan_sekolah/monitoring'));
		}else{
			set_error_message('Terjadi Kesalahan Pengisian Data!');
			redirect(site_url('pengelolaan_sekolah/monitoring'));
		}
		
	}

	public function load_edit($id){
		$this->load->model('pengelolaan_sekolah/m_monitoring');
		$data['data_edit'] = $this->m_monitoring->get_data_by_id($id)->result_array();

		$data['component']="pengelolaan_sekolah";
		render('monitoring/edit',$data);
	}

	public function submit_edit(){
		$this->load->model('pengelolaan_sekolah/m_monitoring');
		$id = $this->input->post('id_sekolah_monitoring');

		$data['jenis_kegiatan'] = $this->input->post('jenis_kegiatan');
		$data['waktu_mulai'] = $this->input->post('waktu_mulai');
		$data['waktu_akhir'] = $this->input->post('waktu_akhir');
		$data['petugas'] = $this->input->post('petugas');
		$data['target'] = $this->input->post('target');

		$this->m_monitoring->update($data, $id);
		set_success_message('Data Berhasil Diupdate!');
		redirect(site_url('pengelolaan_sekolah/monitoring'));
		
	}

	public function delete($id){
		$this->load->model('pengelolaan_sekolah/m_monitoring');
		$this->m_monitoring->delete($id);
		set_success_message('Data Berhasil Dihapus!');
		redirect(site_url('pengelolaan_sekolah/monitoring'));		
	}

	public function search(){
		$this->load->model('pengelolaan_sekolah/m_monitoring');

		$key = $this->input->post('key');
		$data['monitoring'] = $this->m_monitoring->search($key);
		render('monitoring/datalist',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */