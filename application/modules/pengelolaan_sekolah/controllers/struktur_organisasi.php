<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class struktur_organisasi extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('pengelolaan_sekolah/struktur_organisasi/home');
		}
		
		public function home(){
			$data['gambar'] = "";
			$data['component']="pengelolaan_sekolah";
			render("struktur_organisasi/struktur_organisasi", $data);
		}
	
		public function tambah(){
			$data['gambar'] = "";
			$data['component']="pengelolaan_sekolah";
			render("struktur_organisasi/form_tambah", $data);
		}
		
		
		public function submit_post(){
			
			// $this->form_validation->set_rules('id_sekolah', 'Sekolah', 'required');
				
				$id_sekolah = get_id_sekolah();
				
				$config['upload_path'] = './extras/sekolah/';
				$config['allowed_types'] = 'jpg';
				$config['max_size']	= '10000';
				$config['file_name'] = 'sos_'.$id_sekolah;
				$config['overwrite'] = true;

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$error = array('error' => $this->upload->display_errors());
					set_error_message('Terjadi Kesalahan Pengisian Data!');
					redirect("pengelolaan_sekolah/struktur_organisasi/home/");
				}
				else
				{
					$lokasi_file = $this->upload->data();
					set_success_message('Data Berhasil Ditambah!');
					redirect("pengelolaan_sekolah/struktur_organisasi/home/");
				}
				
				
		}

}
