<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class review_proses extends CI_Controller {

	public function index()
	{	
		$this->load->model('pengelolaan_sekolah/m_review_proses');
		$data['program'] = $this->m_review_proses->get_program()->result_array();
		$data['component']="pengelolaan_sekolah";
		render('review_proses/datalist',$data);
	}

	
	public function load_add()
	{	
		$this->load->model('pengelolaan_sekolah/m_review_proses');
		$data['component']="pengelolaan_sekolah";
		$data['program'] = $this->m_review_proses->get_program()->result_array();
		render('review_proses/tambah',$data);
	}

	public function submit(){
		$this->load->model('pengelolaan_sekolah/m_review_proses');
		$this->load->library('form_validation');
		$this->form_validation->set_rules('program', 'Program', 'required');
		$this->form_validation->set_rules('jenis_kegiatan', 'Jenis Kegiatan', 'required');
		$this->form_validation->set_rules('waktu_mulai', 'Waktu Mulai', 'required');
		$this->form_validation->set_rules('waktu_akhir', 'Waktu Akhir', 'required');
		$this->form_validation->set_rules('petugas', 'Petugas', 'required');
		$this->form_validation->set_rules('target', 'Target', 'required');
			
		if($this->form_validation->run() == TRUE){
			$data['id_sekolah'] = get_id_sekolah();
			$data['id_tahun_ajaran'] = get_id_tahun_ajaran();
			$data['id_sekolah_program_kerja'] = $this->input->post('program');
			$data['jenis_kegiatan'] = $this->input->post('jenis_kegiatan');
			$data['waktu_mulai'] = tanggal_db($this->input->post('waktu_mulai'));
			$data['waktu_akhir'] = tanggal_db($this->input->post('waktu_akhir'));
			$data['petugas'] = $this->input->post('petugas');
			$data['target'] = $this->input->post('target');

			//print_r($data);
			$this->m_review_proses->submit($data);
			set_success_message('Data Berhasil Ditambah!');
			redirect(site_url('pengelolaan_sekolah/review_proses'));
		}else{
			set_error_message('Terjadi Kesalahan Pengisian Data!');
			redirect(site_url('pengelolaan_sekolah/review_proses'));
		}
	}

	public function load_edit($id){
		$this->load->model('pengelolaan_sekolah/m_review_proses');
		$data['data_edit'] = $this->m_review_proses->get_data_by_id($id)->result_array();
		$data['program'] = $this->m_review_proses->get_program()->result_array();

		$data['component']="pengelolaan_sekolah";
		render('review_proses/edit',$data);
	}

	public function submit_edit(){
		$this->load->model('pengelolaan_sekolah/m_review_proses');
		$id = $this->input->post('id_sekolah_evaluasi_program_proses');

		$data['id_sekolah_program_kerja'] = $this->input->post('program');
		$data['jenis_kegiatan'] = $this->input->post('jenis_kegiatan');
		$data['waktu_mulai'] = $this->input->post('waktu_mulai');
		$data['waktu_akhir'] = $this->input->post('waktu_akhir');
		$data['petugas'] = $this->input->post('petugas');
		$data['target'] = $this->input->post('target');

		$this->m_review_proses->update($data, $id);
		set_success_message('Data Berhasil Diupdate!');
		redirect(site_url('pengelolaan_sekolah/review_proses'));
		
	}

	public function delete($id){
		$this->load->model('pengelolaan_sekolah/m_review_proses');
		$this->m_review_proses->delete($id);
		set_success_message('Data Berhasil Dihapus!');
		redirect(site_url('pengelolaan_sekolah/review_proses'));		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */