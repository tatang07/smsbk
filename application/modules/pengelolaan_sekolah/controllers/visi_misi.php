<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class visi_misi extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_visi_misi');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('pengelolaan_sekolah/visi_misi/home');
		}
		
		public function home(){
			
			
			$data['visi'] = $this->m_visi_misi->get_all_visi();
			$data['misi'] = $this->m_visi_misi->get_all_misi();
			
			$data['component']="pengelolaan_sekolah";
			render("visi_misi/visi_misi", $data);
		}
	
		public function tambah($judul){
			$judul = $this->uri->segment(4);
			if ($judul == "v"){
				$data['judul'] = 'Visi';
			}else{
				$data['judul'] = 'Misi';
			}
			
			$data['component']="pengelolaan_sekolah";
			render("visi_misi/form_tambah", $data);
		}
		
		public function edit_visi(){
			$id = $this->uri->segment(4);
			if ($id == NULL) {
				redirect('pengelolaan_sekolah/visi_misi/home');
			}
			
			$row = $this->m_visi_misi->get_visi_by($id);
			$data['id'] = $id;
			$data['visi'] = $row->visi;
			$data['urutan'] = $row->urutan;
			$data['judul'] = "Visi";
			
			$data['component']="pengelolaan_sekolah";
			render("visi_misi/form_edit", $data);
		}
		
		public function edit_misi(){
			$id = $this->uri->segment(4);
			if ($id == NULL) {
				redirect('pengelolaan_sekolah/visi_misi/home');
			}
			
			$row = $this->m_visi_misi->get_misi_by($id);
			$data['id'] = $id;
			$data['misi'] = $row->misi;
			$data['urutan'] = $row->urutan;
			$data['judul'] = "Misi";
			
			$data['component']="pengelolaan_sekolah";
			render("visi_misi/form_edit", $data);
		}
		
		public function delete_visi($id){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('pengelolaan_sekolah/visi_misi/home');
			}
			
			$this->m_visi_misi->delete_visi($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('pengelolaan_sekolah/visi_misi/home');
		}
		
		public function delete_misi($id){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('pengelolaan_sekolah/visi_misi/home');
			}
			
			$this->m_visi_misi->delete_misi($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('pengelolaan_sekolah/visi_misi/home');
		}
		
		public function submit_post(){
			$judul = $this->input->post('judul');
			if($judul == "Visi"){
				$this->form_validation->set_rules('Visi', 'Visi', 'required');
			}else{
				$this->form_validation->set_rules('Misi', 'Misi', 'required');
			}
			$this->form_validation->set_rules('urutan', 'Urutan', 'required');
			// $this->form_validation->set_rules('jumlah_jam', 'Jumlah Jam', 'required');
			
			if($this->form_validation->run() == TRUE){
			
				if($judul == "Visi"){
					$visi = $this->input->post('Visi');
				}else{
					$misi = $this->input->post('Misi');
				}
				
				$urutan = $this->input->post('urutan');
				$id = $this->input->post('id');
				$id_sekolah = $this->input->post('id_sekolah');
				
				$action = $this->input->post('action');
				if($action == 'update'){
					if($judul == "Visi"){
						$this->m_visi_misi->update_visi($id, $visi, $urutan);
					}else{
						$this->m_visi_misi->update_visi($id, $misi, $urutan);
					}
					set_success_message('Data Berhasil Diupdate!');
					redirect("pengelolaan_sekolah/visi_misi/home");
				}else{
					if($judul == "Visi"){
						$this->m_visi_misi->tambah_visi($id_sekolah, $visi, $urutan);
					}else{
						$this->m_visi_misi->tambah_misi($id_sekolah, $misi, $urutan);
					}
					
					set_success_message('Data Berhasil Ditambah!');
					redirect("pengelolaan_sekolah/visi_misi/home");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian Data!');
				redirect("pengelolaan_sekolah/visi_misi/home/");
			}
		}
		
}
