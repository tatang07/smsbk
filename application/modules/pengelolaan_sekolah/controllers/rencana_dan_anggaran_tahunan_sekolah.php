<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class rencana_dan_anggaran_tahunan_sekolah extends CI_Controller {

	public function index()
	{	
		$this->load->model('pengelolaan_sekolah/m_rencana_dan_anggaran_tahunan_sekolah');
		$data['ta'] = $this->m_rencana_dan_anggaran_tahunan_sekolah->get_tahun_ajaran();
		$data['component']="pengelolaan_sekolah";
		render('rencana_dan_anggaran_tahunan_sekolah/rencana_dan_anggaran_tahunan_sekolah',$data);
	}
	// public function load_add($id_tahun_ajaran)
	// {	
		// $this->load->model('kepemimpinan/m_program_kerja_tahunan');
		// $data['tabi'] = $this->m_program_kerja_tahunan->get_tahun_ajaran_by_id($id_tahun_ajaran);
		// $data['id_tahun_ajaran']=$id_tahun_ajaran;
		// render('program_kerja_tahunan/form_tambah',$data);
	// }
	// public function load_add_sub($id_sekolah_program_kerja)
	// {	
		// $this->load->model('kepemimpinan/m_program_kerja_tahunan');
		// $data['id_sekolah_program_kerja']=$id_sekolah_program_kerja;
		// render('program_kerja_tahunan/form_tambah_sub',$data);
	// }
	public function load_detail($id_sekolah_program_kerja)
	{	
		$this->load->model('pengelolaan_sekolah/m_rencana_dan_anggaran_tahunan_sekolah');
		$data['program_kerja'] = $this->m_rencana_dan_anggaran_tahunan_sekolah->get_data_by_id($id_sekolah_program_kerja);
		$data['sub'] = $this->m_rencana_dan_anggaran_tahunan_sekolah->get_data_sub($id_sekolah_program_kerja);
		$data['component']="pengelolaan_sekolah";
		render('rencana_dan_anggaran_tahunan_sekolah/detail',$data);
	}
	// public function load_edit($id_tahun_ajaran,$id_sekolah_program_kerja)
	// {	
		// $this->load->model('kepemimpinan/m_program_kerja_tahunan');
		// $data['tabi'] = $this->m_program_kerja_tahunan->get_tahun_ajaran_by_id($id_tahun_ajaran);
		// $data['data_edit'] = $this->m_program_kerja_tahunan->get_data_by_id($id_sekolah_program_kerja);
		// $data['id']=$id_sekolah_program_kerja;
		// render('program_kerja_tahunan/form_edit',$data);
	// }
	public function load_edit_sub($id_sekolah_sub_program_kerja,$id_sekolah_program_kerja)
	{	
		$this->load->model('pengelolaan_sekolah/m_rencana_dan_anggaran_tahunan_sekolah');
		$data['data_edit'] = $this->m_rencana_dan_anggaran_tahunan_sekolah->get_data_sub_by_id($id_sekolah_sub_program_kerja);
		$data['id']=$id_sekolah_sub_program_kerja;
		$data['id2']=$id_sekolah_program_kerja;
		$data['component']="pengelolaan_sekolah";
		render('rencana_dan_anggaran_tahunan_sekolah/form_edit_sub',$data);
	}
	// public function delete($id_sekolah_program_kerja){
			// $this->load->model('kepemimpinan/m_program_kerja_tahunan');
			// $this->m_program_kerja_tahunan->delete($id_sekolah_program_kerja);
			// redirect(site_url('kepemimpinan/program_kerja_tahunan/index/')) ;
	// }
	// public function delete_sub($id_sekolah_sub_program_kerja,$id_sekolah_program_kerja){
			// $id2=$id_sekolah_program_kerja;
			// $this->load->model('kepemimpinan/m_program_kerja_tahunan');
			// $this->m_program_kerja_tahunan->delete_sub($id_sekolah_sub_program_kerja);

			// redirect(site_url('kepemimpinan/program_kerja_tahunan/load_detail/'.$id2)) ;
	// }
	// public function submit_add(){
			// $this->load->model('kepemimpinan/m_program_kerja_tahunan');
			// $data['program_kerja'] = $this->input->post('program_kerja');
			// $data['tanggal_mulai'] = tanggal_db($this->input->post('periode_awal'));
			// $data['tanggal_akhir'] = tanggal_db($this->input->post('periode_akhir'));
			// $data['keterangan'] = $this->input->post('keterangan');
			// $data['id_tahun_ajaran'] = $this->input->post('id_tahun_ajaran');
			// $data['id_sekolah'] = get_id_sekolah();
			// $this->m_program_kerja_tahunan->add($data);
			// redirect(site_url('kepemimpinan/program_kerja_tahunan/index/')) ;
	// }
	// public function submit_add_sub(){
			// $this->load->model('kepemimpinan/m_program_kerja_tahunan');
			// $data['sub_program_kerja'] = $this->input->post('sub_program_kerja');
			// $data['tanggal_mulai'] = tanggal_db($this->input->post('periode_awal'));
			// $data['tanggal_akhir'] = tanggal_db($this->input->post('periode_akhir'));
			// $data['keterangan'] = $this->input->post('keterangan');
			// $data['id_sekolah_program_kerja'] = $this->input->post('id_sekolah_program_kerja');
			// $this->m_program_kerja_tahunan->add_sub($data);

			// redirect(site_url('kepemimpinan/program_kerja_tahunan/load_detail/'.$data['id_sekolah_program_kerja'])) ;
	// }
	// public function submit_edit(){
			// $this->load->model('kepemimpinan/m_program_kerja_tahunan');
			// $id=$this->input->post('id');
			// $data['program_kerja'] = $this->input->post('program_kerja');
			// $data['tanggal_mulai'] = tanggal_db($this->input->post('periode_awal'));
			// $data['tanggal_akhir'] = tanggal_db($this->input->post('periode_akhir'));
			// $data['keterangan'] = $this->input->post('keterangan');
	

			// $this->m_program_kerja_tahunan->edit($data,$id);
			// redirect(site_url('kepemimpinan/program_kerja_tahunan/index/')) ;
	// }
	public function submit_edit_sub(){
			$this->load->model('pengelolaan_sekolah/m_rencana_dan_anggaran_tahunan_sekolah');
			$id=$this->input->post('id');
			$id2=$this->input->post('id2');
			$data['sub_program_kerja'] = $this->input->post('sub_program_kerja');
			$data['tanggal_mulai'] = tanggal_db($this->input->post('periode_awal'));
			$data['tanggal_akhir'] = tanggal_db($this->input->post('periode_akhir'));
			$data['alokasi_dana'] = $this->input->post('alokasi_dana');
			$data['keterangan'] = $this->input->post('keterangan');
		

			$this->m_rencana_dan_anggaran_tahunan_sekolah->edit_sub($data,$id);
			redirect(site_url('pengelolaan_sekolah/rencana_dan_anggaran_tahunan_sekolah/load_detail/'.$id2)) ;
	}

		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */