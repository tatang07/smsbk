<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class kalender_akademik extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('t_kalender_akademik');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('pengelolaan_sekolah/kalender_akademik/home');
		}
		
		public function home(){
			
			
			$data['kalender_akademik'] = $this->t_kalender_akademik->get_all_kalender_akademik();
			$data['tahun_ajaran'] = $this->t_kalender_akademik->get_all_tahun_ajaran();
			$data['component']="pengelolaan_sekolah";
			render("kalender_akademik/kalender_akademik", $data);
		}
	
		public function tambah(){
			
			$data['term'] = $this->t_kalender_akademik->get_all_term();;
			$data['component']="pengelolaan_sekolah";
			render("kalender_akademik/form_tambah", $data);
		}
		
		public function edit(){
			$id = $this->uri->segment(4);
			if ($id == NULL) {
				redirect('pengelolaan_sekolah/kalender_akademik/home');
			}
			
			$data['term'] = $this->t_kalender_akademik->get_all_term();
			
			$row = $this->t_kalender_akademik->get_kalender_akademik_by($id);
			
			$data['id_kalender_akademik'] = $id;
			$data['id_sekolah'] = $row->id_sekolah;
			$data['id_term'] = $row->id_term;
			$data['kegiatan'] = $row->kegiatan;
			$data['tanggal_mulai'] = tanggal_view($row->tanggal_mulai);
			$data['tanggal_selesai'] = tanggal_view($row->tanggal_selesai);
			$data['keterangan'] = $row->keterangan;
			$data['component']="pengelolaan_sekolah";
			render("kalender_akademik/form_edit", $data);
		}
		
		public function delete($id){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('pengelolaan_sekolah/kalender_akademik/home');
			}
			
			$this->t_kalender_akademik->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('pengelolaan_sekolah/kalender_akademik/home');
		}
		
		public function submit_post(){
			
			$this->form_validation->set_rules('kegiatan', 'Kegiatan', 'required');
			$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'required');
			$this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'required');
			$this->form_validation->set_rules('keterangan', 'Keterangan', 'required');
			$this->form_validation->set_rules('id_term', 'Term', 'required');
			$this->form_validation->set_rules('id_sekolah', 'Sekolah', 'required');
				
			if($this->form_validation->run() == TRUE){
			
				$id_kalender_akademik = $this->input->post('id_kalender_akademik');
				$id_sekolah = $this->input->post('id_sekolah');
				$id_term = $this->input->post('id_term');
				$kegiatan = $this->input->post('kegiatan');
				$tanggal_mulai = tanggal_db($this->input->post('tanggal_mulai'));
				$tanggal_selesai = tanggal_db($this->input->post('tanggal_selesai'));
				$keterangan = $this->input->post('keterangan');
				
				$action = $this->input->post('action');
				if($action == 'update'){
					
					$this->t_kalender_akademik->update($id_kalender_akademik, $id_sekolah, $id_term, $kegiatan, $tanggal_mulai, $tanggal_selesai, $keterangan);
					
					set_success_message('Data Berhasil Diupdate!');
					redirect("pengelolaan_sekolah/kalender_akademik/home");
				}else{
					
					$this->t_kalender_akademik->tambah($id_sekolah, $id_term, $kegiatan, $tanggal_mulai, $tanggal_selesai, $keterangan);
					
					set_success_message('Data Berhasil Ditambah!');
					redirect("pengelolaan_sekolah/kalender_akademik/home");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian Data!');
				redirect("pengelolaan_sekolah/kalender_akademik/home/");
			}
		}

		function print_pdf($statuspil){
			$data['kalender_akademik'] = $this->t_kalender_akademik->get_all_kalender_akademik();
			$data['tahun_ajaran'] = $this->t_kalender_akademik->get_all_tahun_ajaran();
			
			$data['statuspil'] = $statuspil;
			print_pdf($this->load->view('kalender_akademik/kalender_akademik_pdf', $data, true), "A4");
		}
}
