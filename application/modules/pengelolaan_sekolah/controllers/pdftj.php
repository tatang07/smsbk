<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class pdftj extends Simple_Controller {
    	 public function index(){
			redirect('pengelolaan_sekolah/pdftj/datalist');
	    }

    	public $myconf = array(
			//Nama controller
			"name" 			=> "pengelolaan_sekolah/pdftj",
			"component"		=> "pengelolaan_sekolah",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Pengelolaan Sekolah",
			"subtitle"		=> "Posisi, Distribusi Kewenangan, Fungsi dan Tanggung Jawab",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "m_pdftj",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"jabatan"=>"Posisi jabatan",
								"kewenangan"=>"Kewenangan",
								"fungsi"=>"Fungsi",
								"tanggung_jawab"=>"Tanggung Jawab",
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(								
								//"lokasi_file_lambang"=>"file",
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"jabatan"
							), 
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "pdftj/datalist",
								//"view_file_datatable"	=> "pdftj/datatable",
								//"view_file_datatable_action"	=> "pdftj/datatable_action",
								"field_list"			=> array("jabatan", "kewenangan", "fungsi", "tanggung_jawab"),
								"field_sort"			=> array("jabatan", "kewenangan", "fungsi", "tanggung_jawab"),
								"field_filter"			=> array("jabatan", "kewenangan", "fungsi", "tanggung_jawab"),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(),
								"field_comparator"		=> array("id_sekolah"=>"="),
								"field_operator"		=> array("id_sekolah"=>"AND"),
								//"field_align"			=> array(),
								"field_size"			=> array(),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								// "custom_action_link"	=> array('Detail'=>'pengelolaan_sekolah/pdftj/detail'),
								"custom_add_link"		=> array('label'=>'Add','href'=>'pengelolaan_sekolah/pdftj/add' ),
								"custom_edit_link"		=> array('label'=>'Edit','href'=>'pengelolaan_sekolah/pdftj/edit'),
						
								//"custom_delete_link"	=> array(),
								//"custom_link"			=> array("program"=>"program/datalist","Siswa"=>"Siswa/datalist"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								// "view_file"				=> "pengelolaan_sekolah/program/detail",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("jabatan", "kewenangan", "fungsi", "tanggung_jawab"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								// "redirect_link"			=> "pengelolaan_sekolah/program"
							),
			//Konfigurasi tampilan edit
			/*"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								// "enable"				=> TRUE,
								// "model_name"			=> "",
								// "table"					=> array(),
								"view_file"				=> "ekskul_jenis/edit",
								"view_file_form_input"	=> "ekskul_jenis/form_input",
								"field_list"			=> array("nama_ekstra_kurikuler", "pelatih", "proker", "tujuan","Foto"),
								// "custom_action"			=> "",
								// "msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								// "msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> "pengelolaan_sekolah"
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);
		// public function detail($id){
			
			// $this->load->model('pengelolaan_sekolah/m_siswa_program_pembinaan');
			// $data['program'] = $this->m_siswa_program_pembinaan->select_isi_by_id($id)->result();
			// $data['siswa'] = $this->m_siswa_program_pembinaan->select_detail($id)->result();
			// render('pdftj/detail',$data);				
			
		// }
		
		// public function load_add(){	
			// $this->load->model('pengelolaan_sekolah/m_siswa_program_pembinaan');
			// $id_sekolah= get_id_sekolah();			
			// $data['tingkat_kelas']=0;
			// $jenjang_sekolah = get_jenjang_sekolah();
			// $data['siswa'] = $this->m_siswa_program_pembinaan->select_kelas($jenjang_sekolah);
			// render('pdftj/form_tambah',$data);
		// }

		public function pre_process($data){
			//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
			//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
						
			$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			// $list_program = get_list("m_program","id_program","program");
			$data['conf']['data_list']['subtitle'] = "Posisi, Distribusi Kewenangan, Fungsi dan Tanggung Jawab";
			
			$data['conf']['data_add']['subtitle'] = "Tambah";
			$data['conf']['data_edit']['subtitle'] = "Edit";
			
			$data['conf']['data_delete']['redirect_link'] = "pengelolaan_sekolah/pdftj/datalist/";
			$data['conf']['data_add']['redirect_link']    = "pengelolaan_sekolah/pdftj/datalist/";
			$data['conf']['data_edit']['redirect_link']   = "pengelolaan_sekolah/pdftj/datalist/";
			
			return $data;
		}
		
		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
			$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			// $_POST['filter']['id_program'] = $id_program;
			$_POST['filter']['id_sekolah'] = get_id_sekolah();
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															//'id_program'=>$id_program
															'id_sekolah'=>get_id_sekolah() 
														);
			
			//Mengganti link add yang ada di kanan atas form datalist
			$data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'pengelolaan_sekolah/pdftj/add/');			
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
		
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'pengelolaan_sekolah/pdftj/edit');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'pengelolaan_sekolah/pdftj/delete');
			
			return $data;
		}
		
		public function before_datatable($data){
			//Method ini hanya akan dieksekusi di awal method datatable
			//Mengambil id_pihak yang diposting dari form datalist
			$filter = $this->input->post("filter");			
			// $id_program = $filter['id_program'];
			//Mengkustomisasi link tombol edit dan delete
			
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'pengelolaan_sekolah/pdftj/edit/');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'pengelolaan_sekolah/pdftj/delete/');
			
			return $data;
		}
		
				
		public function before_render_add($data){
			if(!$this->input->post()){
				$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
				//mengisi nilai untuk id_ di form input
				$_POST['item']['id_program'] = $id_program;
			}
			return $data;
		}
		
		public function before_add($data){
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
				$data['item']['id_sekolah'] = get_id_sekolah();
			}
			return $data;
		}
				
		public function before_edit($data){
			if(!$this->input->post()){
				$data['id'] = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 0;
			}
			return $data;
		}
		
		public function before_delete($data){
			$data['id'] = $this->uri->rsegment(3);
			return $data;
		}
}
