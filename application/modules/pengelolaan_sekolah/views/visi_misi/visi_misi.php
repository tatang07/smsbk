<h1 class="grid_12">Pengelolaan Sekolah</h1>
<div class="grid_12">
	<div class="box with-table">
		<div class="header">
			<h2>Visi dan Misi</h2>
		</div>
	</div>
	<div class="box with-table">

		<div class="header">
			<h2>Visi</h2>
		</div>

		<div class="content">
			<div class="tabletools">
				<div class="right">
					<a href="<?php echo base_url('pengelolaan_sekolah/visi_misi/tambah/v'); ?>"><i class="icon-plus"></i>Tambah</a> 
				  <br/><br/>
				</div>
			</div>
			
			<div id="datatable" url="">
				<div class="dataTables_wrapper" role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Visi</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; ?>
							<?php if($visi){ ?>
							<?php foreach($visi as $v): ?>
								<tr>
									<td width='30px' class='center'><?php echo $no; ?></td>
									<td width='600px'><?php echo $v->visi; ?></td>
									
									<td width='' class='center'>
									<a href="<?php echo base_url('pengelolaan_sekolah/visi_misi/edit_visi/'.$v->id_sekolah_visi); ?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
									<a href="<?php echo base_url('pengelolaan_sekolah/visi_misi/delete_visi/'.$v->id_sekolah_visi); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
								<?php $no++; ?>
							<?php endforeach; ?>
							</tbody>
							</table>
							<?php }else{ ?>			
									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Data Tidak Ditemukan!</div>
								</div>
							<?php } ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="box with-table">
		<div class="header">
			<h2>Misi</h2>
		</div>
		<div class="content">
			<div class="tabletools">
				<div class="right">
					<a href="<?php echo base_url('pengelolaan_sekolah/visi_misi/tambah/m'); ?>"><i class="icon-plus"></i>Tambah</a> 
				  <br/><br/>
				</div>
			</div>
			<div id="datatable" url="">
				<div class="dataTables_wrapper" role="grid"> 
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Misi</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; ?>
							<?php if($misi){ ?>
							<?php foreach($misi as $m): ?>
								<tr>
									<td width='30px' class='center'><?php echo $no; ?></td>
									<td width='600px'><?php echo $m->misi; ?></td>
									
									<td width='' class='center'>
									<a href="<?php echo base_url('pengelolaan_sekolah/visi_misi/edit_misi/'.$m->id_sekolah_misi); ?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
									<a href="<?php echo base_url('pengelolaan_sekolah/visi_misi/delete_misi/'.$m->id_sekolah_misi); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
								<?php $no++; ?>
							<?php endforeach; ?>
							</tbody>
							</table>
							<?php }else{ ?>			
									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Data Tidak Ditemukan!</div>
								</div>
							<?php } ?>
				</div>
			</div>
		</div>
	</div>

</div>
