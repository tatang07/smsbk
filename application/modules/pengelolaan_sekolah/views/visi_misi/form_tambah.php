	<h1 class="grid_12">Pengelolaan Sekolah</h1>
			
			<form action="<?php echo base_url('pengelolaan_sekolah/visi_misi/submit_post'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah <?php echo $judul; ?></legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong><?php echo $judul; ?></strong>
						</label>
						<div>
							<textarea rows=5 name="<?php echo $judul; ?>" id="f1_textarea_grow"></textarea>
							<input type="hidden" name="id" value="<?php echo '0'; ?>" />
							<input type="hidden" name="id_sekolah" value="<?php echo get_id_sekolah(); ?>" />
							<input type="hidden" name="action" value="add" />
							<input type="hidden" name="judul" value="<?php echo $judul; ?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Urutan</strong>
						</label>
						<div>
							<input type="text" name="urutan" value="" />
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pengelolaan_sekolah/visi_misi/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		