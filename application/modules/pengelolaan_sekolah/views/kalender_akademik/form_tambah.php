	<h1 class="grid_12">Pengelolaan Sekolah</h1>
			
			<form action="<?php echo base_url('pengelolaan_sekolah/kalender_akademik/submit_post'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Kalender Akademik</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kegiatan</strong>
						</label>
						<div>
							<input type="text" name="kegiatan" value="" />
							<input type="hidden" name="id_kalender_akademik" value="<?php echo '0'; ?>" />
							<input type="hidden" name="id_sekolah" value="<?php echo get_id_sekolah(); ?>" />
							<input type="hidden" name="action" value="add" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Tanggal Mulai</strong>
						</label>
						<div>
							<input type="date" name="tanggal_mulai" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Tanggal Selesai</strong>
						</label>
						<div>
							<input type="date" name="tanggal_selesai" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Term</strong>
						</label>
						<div>
							<select name="id_term" data-placeholder="pilih term">
							<?php foreach($term as $t): ?>
									<option value="<?php echo $t->id_term; ?>"><?php echo $t->term; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="" />
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pengelolaan_sekolah/kalender_akademik/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
