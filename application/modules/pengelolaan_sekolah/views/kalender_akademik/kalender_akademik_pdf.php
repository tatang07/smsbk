<h1 class="grid_12">Pengelolaan Sekolah</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <?php $tahun =''; foreach($tahun_ajaran as $q): ?>
				<?php if($q->id_tahun_ajaran == $statuspil){ ?>
					<?php $tahun = $q->tahun_awal.'/'.$q->tahun_akhir; ?>
				<?php } ?>
			<?php endforeach; ?>
            <h2>Kalender Akademik Tahun Ajaran <?php echo $tahun; ?></h2>
       </div>
      
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kegiatan</th>
								<th>Tanggal Mulai</th>
								<th>Tanggal Selesai</th>
								<th>Penanggung Jawab</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; ?>
							<?php if($kalender_akademik){ ?>
							<?php foreach($kalender_akademik as $k): ?>
								<?php if($k->id_tahun_ajaran == $statuspil){ ?>
								<tr>
									<td width='50pc' class='center'><?php echo $no; ?></td>
									<td width='300px' class='center'><?php echo $k->kegiatan; ?></td>
									<td width='150px' class='center'><?php echo $k->tanggal_mulai; ?></td>					
									<td width='150px' class='center'><?php echo $k->tanggal_selesai; ?></td>					
									<td width='200px' class='center'><?php echo $k->keterangan; ?></td>					
								</tr>
							<?php $no++; } ?>
							<?php endforeach; ?>
								</tbody>
							</table>
							<?php }else{ ?>			
									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Data Tidak Ditemukan!</div>
								</div>
							<?php } ?>
				</div>
            </div>
        </div>        
    </div>
 </div>
