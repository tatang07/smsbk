

<?php 
	$statuspil = 0;
	if(isset($_POST['id_tahun_ajaran'])){
	$a = $_POST['id_tahun_ajaran'];
	$statuspil = $a; } ?>
	
<h1 class="grid_12">Pengelolaan Sekolah</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
			<?php $tahun =''; foreach($tahun_ajaran as $q): ?>
				<?php if($q->id_tahun_ajaran == $statuspil){ ?>
					<?php $tahun = $q->tahun_awal.'/'.$q->tahun_akhir; ?>
				<?php } ?>
			<?php endforeach; ?>
            <h2>Kalender Akademik Tahun Ajaran <?php echo $tahun; ?></h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pengelolaan_sekolah/kalender_akademik/print_pdf/'.$statuspil); ?>"><i class="icon-print"></i>Cetak</a> 
			  	<a href="<?php echo base_url('pengelolaan_sekolah/kalender_akademik/tambah/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br/><br/>
            </div>
			
           <div class="dataTables_filter">
				<form action="" name= "myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Tahun Ajaran</span></th>
							<td width=200px align='left'>
								<select name="id_tahun_ajaran" id="id_tahun_ajaran" onchange="submitform();">
									<option value="0">-Pilih Tahun Ajaran-</option>
								<?php foreach($tahun_ajaran as $q): ?>
									
									<?php if($q->id_tahun_ajaran == $statuspil){ ?>
										<option selected value='<?php echo $q->id_tahun_ajaran; ?>' data-status-pilihan="<?php echo $q->id_tahun_ajaran; ?>"><?php echo $q->tahun_awal.'/'.$q->tahun_akhir; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $q->id_tahun_ajaran; ?>' data-status-pilihan="<?php echo $q->id_tahun_ajaran; ?>"><?php echo $q->tahun_awal.'/'.$q->tahun_akhir; ?></option>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kegiatan</th>
								<th>Tanggal Mulai</th>
								<th>Tanggal Selesai</th>
								<th>Penanggung Jawab</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; ?>
							<?php if($kalender_akademik){ ?>
							<?php foreach($kalender_akademik as $k): ?>
								<?php if($k->id_tahun_ajaran == $statuspil){ ?>
								<tr>
									<td width='50pc' class='center'><?php echo $no; ?></td>
									<td width='300px' class='center'><?php echo $k->kegiatan; ?></td>
									<td width='150px' class='center'><?php echo $k->tanggal_mulai; ?></td>					
									<td width='150px' class='center'><?php echo $k->tanggal_selesai; ?></td>					
									<td width='200px' class='center'><?php echo $k->keterangan; ?></td>					
									<td width='150px' class='center'>								
										<a href="<?php echo base_url('pengelolaan_sekolah/kalender_akademik/edit/'.$k->id_kalender_akademik); ?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Ubah</a>
										<a href="<?php echo base_url('pengelolaan_sekolah/kalender_akademik/delete/'.$k->id_kalender_akademik); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php $no++; } ?>
							<?php endforeach; ?>
								</tbody>
							</table>
							<?php }else{ ?>			
									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Data Tidak Ditemukan!</div>
								</div>
							<?php } ?>
				</div>
            </div>
        </div>        
    </div>
 </div>


<script>
	
	function submitform()
	{
	  document.myform.submit();
	}
</script>