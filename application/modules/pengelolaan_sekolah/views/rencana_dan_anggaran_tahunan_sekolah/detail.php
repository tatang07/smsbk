<?php  
	$statuspil = 0;
	if(isset($_POST['id_tahun_ajaran'])){
	$a = $_POST['id_tahun_ajaran'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Pengelolaan Sekolah</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Sub Program Kerja Tahunan</h2>
       </div>
	
       <div class="tabletools">
			<div class="right"><?php foreach($program_kerja as $p){ ?>
			  	
            </div>
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=200px align='left'><span class="text"><span><?php echo $p['program_kerja']; ?></span></th>
						</tr>
					</table>
				</form>
			</div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Sub Program Kerja</th>
								<th >Periode</th>
								<th >Total Alokasi Dana</th>
								<th >Realisasi</th>
								<th >Keterangan</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
							<?php $no=1; ?>
							<?php foreach($sub as $d):?>
								<tr>
									<td class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['sub_program_kerja']?>
									</td class='center'>
									<td class='center'>
									<?php  echo $d['tanggal_mulai'].' s/d '.$d['tanggal_akhir']?>
									</td>
									<td class='center'>
									<?php  echo $d['alokasi_dana'];?>
									</td>
									<td class='center'>
									<?php  echo $d['realisasi'];?>
									</td>
									<td class='center'>
									<?php  echo $d['keterangan'];?>
									</td>
									<td class='center'>
									<a href="<?php echo site_url('pengelolaan_sekolah/rencana_dan_anggaran_tahunan_sekolah/load_edit_sub/'.$d['id_sekolah_sub_program_kerja'].'/'.$p['id_sekolah_program_kerja'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i>Alokasi Dana</a>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
						<?php }?>
					</table>
					
					<div class="footer">
						<div class="dataTables_info"></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>
