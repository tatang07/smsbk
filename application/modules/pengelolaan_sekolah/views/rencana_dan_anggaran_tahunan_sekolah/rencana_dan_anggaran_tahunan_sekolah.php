<?php  
	$statuspil = 0;
	if(isset($_POST['id_tahun_ajaran'])){
	$a = $_POST['id_tahun_ajaran'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Pengelolaan Sekolah</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rencana dan Anggaran Tahunan Sekolah</h2>
			<?php //echo $statuspil ?>
       </div>
	
       <div class="tabletools">
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Tahun Ajaran</span></th>
							<td width=200px align='left'>
								<select name="id_tahun_ajaran" id="id_tahun_ajaran" onchange="submitform();">
									<option value="0">-Tahun Ajaran-</option>
									<?php foreach($ta as $q): ?>
										
										<?php if($q->id_tahun_ajaran == $statuspil){ ?>
											<option selected value='<?php echo $q->id_tahun_ajaran; ?>' data-status-pilihan="<?php echo $q->id_tahun_ajaran; ?>"><?php echo $q->tahun_awal; ?>-<?php echo $q->tahun_akhir; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q->id_tahun_ajaran; ?>' data-status-pilihan="<?php echo $q->id_tahun_ajaran; ?>"><?php echo $q->tahun_awal; ?>-<?php echo $q->tahun_akhir; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Program Kerja</th>
								<th >Periode</th>
								<th >Realisasi</th>
								<th >Keterangan</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
							<?php 
								$this->load->model('pengelolaan_sekolah/m_rencana_dan_anggaran_tahunan_sekolah');
								$id_sekolah=get_id_sekolah();
								$data['isi'] = $this->m_rencana_dan_anggaran_tahunan_sekolah->get_data($statuspil,$id_sekolah);
								$isi=$data['isi'];

								//$x = $this->m_rencana_dan_anggaran_tahunan_sekolah->getJumlahAlokasi()->row();
								//$alokasi = $x->alokasi_dana;

								//$y = $this->m_rencana_dan_anggaran_tahunan_sekolah->getJumlahrealisasi()->row();
								//$realisasi = $y->pendanaan;

								//$jum_realisasi = ($realisasi / $alokasi) * 100;
								
							?>
							<?php $no=1; ?>
							

							<?php foreach($isi as $d):?>
								<?php 
									$x = $this->m_rencana_dan_anggaran_tahunan_sekolah->getJumlahAlokasi($d['id_sekolah_program_kerja'])->row_array();
									$y = $this->m_rencana_dan_anggaran_tahunan_sekolah->getJumlahrealisasi($d['id_sekolah_program_kerja'])->row_array();
									
									$realisasi = ( $y['pendanaan'] / $x['alokasi_dana']) * 100;
									//echo $realisasi;
								?>
									<?php //print_r($y); ?>
								<tr>
									<td class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['program_kerja']?>
									</td class='center'>
									<td class='center'>
									<?php  echo tanggal_indonesia($d['tanggal_mulai']).' s/d '.tanggal_indonesia($d['tanggal_akhir'])?>
									</td>
									<td class='center'>
									<?php  echo number_format($realisasi, 2).' %';?>
									</td>
									<td class='center'>
									<?php  echo $d['keterangan'];?>
									</td>
									<td class='center'>
									<a href="<?php echo site_url('pengelolaan_sekolah/rencana_dan_anggaran_tahunan_sekolah/load_detail/'.$d['id_sekolah_program_kerja'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-share-alt"></i> detail</a>
									
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>