<h1 class="grid_12">Pengelolaan Sekolah</h1>
<form action="<?php echo base_url('pengelolaan_sekolah/monitoring/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
	<fieldset>
		<legend>Tambah Monitoring</legend>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Jenis Kegiatan</strong>
			</label>
			<div>
				<input type="text" name="jenis_kegiatan" value="" />
			</div>
		</div>

		<div class="row">
			<label for="f1_normal_input">
				<strong>Waktu Mulai</strong>
			</label>
			<div>
				<input type="date" name="waktu_mulai" value="" />
				
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Waktu Akhir</strong>
			</label>
			<div>
				<input type="date" name="waktu_akhir" value="" />
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Petugas</strong>
			</label>
			<div>
				<input type="text" name="petugas" value="" />
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Target</strong>
			</label>
			<div>
				<input type="text" name="target" value="" />
				
			</div>
		</div>		
	</fieldset><!-- End of fieldset -->
	
	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" name=send />
			 <a href="<?php echo base_url('pengelolaan_sekolah/monitoring/'); ?>"> <input value="Batal" type="button"></a>
		</div>
		<div class="right">
		</div>
	</div><!-- End of .actions -->
</form><!-- End of .box -->