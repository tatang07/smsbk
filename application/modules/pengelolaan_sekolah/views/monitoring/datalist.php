<h1 class="grid_12">Pengelolaan Sekolah</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Monitoring</h2>
       </div>
	
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pengelolaan_sekolah/monitoring/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('pengelolaan_sekolah/monitoring/search/'); ?>" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th align="left"><span class="text">Pencarian</span></th>
								<td align="left"><input type="text" name="key" id="key"></td>
								<td width="10px"></td>
							
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jenis Kegiatan</th>
								<th>Waktu</th>
								<th>Petugas</th>
								<th>Target</th>
								<th>Aksi</th>
							</tr>
						</thead>
				
						<tbody>
							<?php $i=1;?>
							<?php foreach($monitoring as $m){ ?>
								<tr>
									<td width='' class='center'><?php echo $i ?></td>
									<td width='' class='center'><?php echo $m['jenis_kegiatan']; ?></td>
									<td width='' class='center'>
										<?php 
											if($m['waktu_mulai'] && $m['waktu_akhir']){
												echo tanggal_indonesia($m['waktu_mulai']).' - '.tanggal_indonesia($m['waktu_akhir']);								
											}else{
												echo "";
											}
										?>
									</td>
									<td width='' class='center'><?php echo $m['petugas']; ?></td>
									<td width='' class='center'><?php echo $m['target']; ?></td>
									<td width='' class='center'>
										<a href="<?php echo site_url('pengelolaan_sekolah/monitoring/load_edit/'.$m['id_sekolah_monitoring'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
										<a href="<?php echo site_url('pengelolaan_sekolah/monitoring/delete/'.$m['id_sekolah_monitoring'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php $i++; ?>
							<?php } ?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info"></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>