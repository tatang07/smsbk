<?php $myconf= $conf['data_list']?>
<?php if(count($myconf['field_sum'])>0):?>
	<?php $sum_item = $myconf['field_sum'] ?>
	<?php $sum_data = array() ?>
	<?php
		foreach($alldata as $det){
			foreach($sum_item as $item){
				if(isset($det[$item])){
					$valitem = $det[$item];
					if(isset($sum_data[$item])){
						$sum_data[$item] += $valitem;
					}else{
						$sum_data[$item]= $valitem;
					}
				}
			}
		}
	?>
<?php endif ?>
		<?php $wdtno = isset($myconf['field_size']['no_col'])?$myconf['field_size']['no_col']:"10px"?>
		<div class="dataTables_wrapper"  role="grid">    
            <table class="styled" >
                <thead>
                    <tr>
						<th rowspan=2>No.</th>
						<?php foreach($myconf['field_list'] as $key): ?>  
							<?php $label = isset($conf['data_label'][$key])?$conf['data_label'][$key]:"" ?>
							<?php if($label=='Fasilitas'){ ?>
								<th <?php echo in_array($key, $myconf['field_sort'])?sortby($key):"" ?> rowspan=2><?php echo $label?></th>
							<?php } ?>
						<?php endforeach ?>
		
						
						<?php if($myconf["enable_action"]):?>
							
						<?php endif ?>
                    </tr>
					<tr>
						<?php foreach($myconf['field_list'] as $key): ?>  
							<?php $label = isset($conf['data_label'][$key])?$conf['data_label'][$key]:"" ?>
							<?php if($label!='Fasilitas'){ ?>
								<th <?php echo in_array($key, $myconf['field_sort'])?sortby($key):"" ?>><?php echo $label?></th>
							<?php } ?>
						<?php endforeach ?>
						<th rowspan=2>Aksi</th>
					</tr>
                </thead>
                <tbody>
                    <?php $i=$offset+1 ?>
                    <?php foreach ($list as $a): ?>	
                        <tr>
                            <td width='<?php echo $wdtno ?>' class='center'><?php echo $i++ ?></td>
							<?php foreach($myconf['field_list'] as $key): ?> 
								<?php $size = isset($myconf['field_size'][$key])?" width='".$myconf['field_size'][$key]."' ":"" ?>
								<?php $align = isset($myconf['field_align'][$key])?" style='text-align:".$myconf['field_align'][$key]."' ":"" ?>
								<?php $val = $a[$key] ?>
								<?php $val = isset($conf['data_type'][$key]) && $conf['data_type'][$key]=="date"? tanggal_view($val):$val ?>
								<?php if(in_array($key,$myconf["field_separated"])):?>
									<?php $val = str_replace(";","<br/>",$val)?>
								<?php endif ?>
								<td <?php echo $size?> <?php echo $align?> >
									<?php $dtype= isset($conf['data_type'][$key])?$conf['data_type'][$key]:""?>
									<?php $dtype= is_array($dtype)?"":$dtype?>
									<?php if(isset($conf[$dtype]['list'])): ?>
										<?php $newtype = $conf[$dtype] ?>
										<a href="javascript:openPopup('<?php echo $key ?>detailpopup<?php echo $a[$myconf['id_model']] ?>')"> <?php echo $val ?></a>
										<?php $titletype = isset($newtype['title'])?$newtype['title']:""?>
										<div style="display: none;" id="<?php echo $key ?>detailpopup<?php echo $a[$myconf['id_model']] ?>" title="<?php echo $titletype?>">
											<table>
											<?php foreach($newtype['list'] as $det): ?> 
												<tr>
													<?php $labeldet = isset($conf['data_label'][$det])?$conf['data_label'][$det]:"" ?>
													<td><?php echo $labeldet ?></td>
													<?php $isi = $a[$det] ?>
													<?php $isi = isset($conf['data_type'][$det]) && $conf['data_type'][$det]=="date"? tanggal_view($isi):$isi ?>
													<td>:&nbsp;&nbsp;&nbsp;</td>
														<?php if(in_array($det,$myconf["field_separated"])):?>
															<?php $isi = str_replace(";","<br/>",$isi)?>
														<?php endif ?>
													<td><?php echo $isi?></td>
												</tr>
											<?php endforeach ?>
											</table>
										</div>
									<?php else:?>
										<?php echo $val ?>
									<?php endif ?>
								</td>
                            <?php endforeach ?>
							<?php if($myconf["enable_action"]):?>
								<?php $this->load->view($myconf['view_file_datatable_action'],array('detail'=>$a)) ?>
							<?php endif ?>
                        </tr>
                    <?php endforeach ?>
					<?php if(count($myconf['field_sum'])>0):?>
						<tr>
							<td colspan=2 style='text-align:center'>Total</td>
							<?php $idn = 1 ?>
							<?php foreach($myconf['field_list'] as $key): ?>
								<?php if($idn>1):?>
									<?php $nilai = isset($sum_data[$key])?$sum_data[$key]:"&nbsp;" ?>
									<td style='text-align:center'><?php echo $nilai ?></td>
								<?php endif ?>
								<?php $idn++ ?>
							<?php endforeach ?>
						</tr>
					<?php endif ?>
                </tbody>
            </table>
            <?php $this->load->view("elements/pagination") ?>
            
        </div>
		
		
<?php if(count($myconf['field_summary'])>0):?>
	<?php $rekap_item = $myconf['field_summary'] ?>
	<?php $rekap_data = array() ?>
	<?php
		foreach($alldata as $det){
			foreach($rekap_item as $item){
				if(isset($det[$item])){
					$valitem = $det[$item];
					if(isset($rekap_data[$item][$valitem])){
						$rekap_data[$item][$valitem] = $rekap_data[$item][$valitem] +1;
					}else{
						$rekap_data[$item][$valitem] = 1;
					}
				}
			}
		}
	?>
	<br>&nbsp;&nbsp;
	<div class="grid_12 profile">
		<div class="details grid_12">

			<section>
				
					<table>
					<?php foreach($rekap_item as $rek):?>
						<?php $rek_title = isset($conf['data_label'][$rek])?$conf['data_label'][$rek]:""?>
						<tr>
							<td><b>Rekap <?php echo $rek_title ?></b></td>
							<td width=2px>&nbsp;&nbsp;</td>
							<td></td>
						</tr>    
						<?php if(isset($rekap_data[$rek])):?>
							<?php foreach($rekap_data[$rek] as $lblrek=>$detrek):?>
								<tr>
									<td><b><?php echo $lblrek ?></b></td>
									<td width=2px>:&nbsp;&nbsp;</td>
									<td><?php echo $detrek ?></td>
								</tr>
							<?php endforeach ?>
						<?php endif ?>
						<tr><td>&nbsp;</td></tr>
					<?php endforeach ?>
					</table>
				
			</section>
		</div>
	</div>
	<div>&nbsp;<br>&nbsp;&nbsp;</div>
<?php endif?>