<h1 class="grid_12">Pengelolaan Sekolah</h1>
<form action="<?php echo base_url('pengelolaan_sekolah/review_proses/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
	<fieldset>
		<legend>Edit Review dan Evaluasi Terhadap Proses Pelaksanaan Program</legend>
		<?php foreach($data_edit as $d){ ?>
			<div class="row">
				<label for="f1_normal_input">
					<strong>Program</strong>
				</label>
				<div>
					<select name="program">
						<?php foreach($program as $p){ ?>
							<?php if($p['id_sekolah_program_kerja'] == $d['id_sekolah_program_kerja']) { ?>
								<option selected value="<?php echo $p['id_sekolah_program_kerja'] ?>"> <?php echo $p['program_kerja']; ?> </option>>
							<?php }else{ ?>
								<option value="<?php echo $p['id_sekolah_program_kerja'] ?>"> <?php echo $p['program_kerja']; ?> </option>>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="row">
				<label for="f1_normal_input">
					<strong>Jenis Kegiatan</strong>
				</label>
				<div>
					<input type="text" name="jenis_kegiatan" value="<?php echo $d['jenis_kegiatan'] ?>" />
					<input type="hidden" name="id_sekolah_evaluasi_program_proses" value="<?php echo $d['id_sekolah_evaluasi_program_proses'] ?>" />
				</div>
			</div>

			<div class="row">
				<label for="f1_normal_input">
					<strong>Waktu Mulai</strong>
				</label>
				<div>
					<input type="date" name="waktu_mulai" value="<?php echo tanggal_view($d['waktu_mulai']) ?>" />
					
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Waktu Akhir</strong>
				</label>
				<div>
					<input type="date" name="waktu_akhir" value="<?php echo tanggal_view($d['waktu_akhir']) ?>" />
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Petugas</strong>
				</label>
				<div>
					<input type="text" name="petugas" value="<?php echo $d['petugas'] ?>" />
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Target</strong>
				</label>
				<div>
					<input type="text" name="target" value="<?php echo $d['target'] ?>" />
					
				</div>
			</div>
		<?php } ?>		
	</fieldset><!-- End of fieldset -->
	
	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" name=send />
			 <a href="<?php echo base_url('pengelolaan_sekolah/review_proses/'); ?>"> <input value="Batal" type="button"></a>
		</div>
		<div class="right">
		</div>
	</div><!-- End of .actions -->
</form><!-- End of .box -->