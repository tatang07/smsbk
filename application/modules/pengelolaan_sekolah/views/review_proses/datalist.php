<?php  
	if(isset($_POST['id_sekolah_program_kerja'])){
		$a = $_POST['id_sekolah_program_kerja'];
		$statuspil = $a;
	}else{
		$statuspil = "0";
	}
?>

<h1 class="grid_12">Pengelolaan Sekolah</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Review dan Evaluasi Terhadap Proses Pelaksanaan Program</h2>
       </div>
	
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pengelolaan_sekolah/review_proses/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th align="left"><span class="text">Pencarian</span></th>
								<td align="left">
									<select name="id_sekolah_program_kerja" id="id_sekolah_program_kerja" onchange="submitform();">
										<option value="0">-- Pilih Program --</option>
										<?php foreach($program as $p): ?>
											<?php if($p['id_sekolah_program_kerja'] == $statuspil){ ?>
												<option selected value='<?php echo $p['id_sekolah_program_kerja']; ?>' data-status-pilihan="<?php echo $p['id_sekolah_program_kerja']; ?>"><?php echo $p['program_kerja']; ?></option>
											<?php }else{ ?>
												<option value='<?php echo $p['id_sekolah_program_kerja']; ?>' data-status-pilihan="<?php echo $p['id_sekolah_program_kerja']; ?>"><?php echo $p['program_kerja']; ?></option>
											<?php } ?>
										<?php endforeach; ?>
									</select>
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Program</th>
								<th>Jenis Kegiatan</th>
								<th>Waktu</th>
								<th>Petugas</th>
								<th>Target</th>
								<th>Aksi</th>
							</tr>
						</thead>
				
						<tbody>
							<?php
								$this->load->model('pengelolaan_sekolah/m_review_proses');
								$id_sekolah_program_kerja = $statuspil;

								if($statuspil== "0"){
									$data['review'] = $this->m_review_proses->get_all()->result_array();
								}else{
									$data['review'] = $this->m_review_proses->get_data($id_sekolah_program_kerja)->result_array();									
								}

								$review = $data['review'];
							?>

							<?php $i=1;?>
							<?php foreach($review as $m){ ?>
								<tr>
									<td width='' class='center'><?php echo $i ?></td>
									<td width='' class='center'><?php echo $m['program_kerja']; ?></td>
									<td width='' class='center'><?php echo $m['jenis_kegiatan']; ?></td>
									<td width='' class='center'>
										<?php 
											if($m['waktu_mulai'] && $m['waktu_akhir']){
												echo tanggal_indonesia($m['waktu_mulai']).' - '.tanggal_indonesia($m['waktu_akhir']);								
											}else{
												echo "";
											}
										?>
									</td>
									<td width='' class='center'><?php echo $m['petugas']; ?></td>
									<td width='' class='center'><?php echo $m['target']; ?></td>
									<td width='' class='center'>
										<a href="<?php echo site_url('pengelolaan_sekolah/review_proses/load_edit/'.$m['id_sekolah_evaluasi_program_proses'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
										<a href="<?php echo site_url('pengelolaan_sekolah/review_proses/delete/'.$m['id_sekolah_evaluasi_program_proses'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php $i++; ?>
							<?php } ?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info"></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>