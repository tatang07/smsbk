<?php
class m_monitoring extends MY_Model {
    function get_data(){
    	$this->db->select('*');
		$this->db->from('t_sekolah_monitoring');
		$this->db->where('id_sekolah', get_id_sekolah());
		$this->db->where('id_tahun_ajaran', get_id_tahun_ajaran());
		return $this->db->get();
    }

    function get_data_by_id($id_sekolah_monitoring){
    	$this->db->select('*');
		$this->db->from('t_sekolah_monitoring');
		$this->db->where('id_sekolah_monitoring', $id_sekolah_monitoring);
		return $this->db->get();
    }

    function search($key){
        $query = $this->db->query("SELECT * FROM t_sekolah_monitoring where jenis_kegiatan like '%$key%' ");
        return $query->result_array();
    }

    function submit($data){
    	$this->db->insert('t_sekolah_monitoring',$data);
    }

    function update($data, $id){
    	$this->db->where('id_sekolah_monitoring',$id);
		$this->db->update('t_sekolah_monitoring',$data);
    }

    function delete($id){
    	$this->db->where('id_sekolah_monitoring', $id);
		$this->db->delete('t_sekolah_monitoring');
    }
}
	