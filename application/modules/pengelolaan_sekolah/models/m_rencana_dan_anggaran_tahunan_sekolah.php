<?php
class m_rencana_dan_anggaran_tahunan_sekolah extends MY_Model {
	
	public function get_tahun_ajaran(){
			$query = $this->db
						->from('m_tahun_ajaran')
						->order_by('id_tahun_ajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
	}
	
	// public function get_tahun_ajaran_by_id($id){
			// $query = $this->db
						// ->from('m_tahun_ajaran')
						// ->where('id_tahun_ajaran',$id)
						// ->get();
			// if($query->num_rows() > 0){
				// foreach($query->result() as $data){
					// $result[] = $data;
				// }	return $result;
			// }
	// }
	function get_data($id_tahun_ajaran,$id_sekolah){
			$this->db->select('*');
			$this->db->from('t_sekolah_program_kerja');
			$this->db->where('id_tahun_ajaran',$id_tahun_ajaran);
			$this->db->where('id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}
	function get_data_by_id($id){
			$this->db->select('*');
			$this->db->from('t_sekolah_program_kerja');
			$this->db->where('id_sekolah_program_kerja',$id);
			
			return $this->db->get()->result_array();
	}
	function get_data_sub_by_id($id){
			$this->db->select('*');
			$this->db->from('t_sekolah_sub_program_kerja');
			$this->db->where('id_sekolah_sub_program_kerja',$id);
			
			return $this->db->get()->result_array();
	}
	function get_data_sub($id){
			$this->db->select('*');
			$this->db->from('t_sekolah_sub_program_kerja');
			$this->db->where('id_sekolah_program_kerja',$id);
			
			return $this->db->get()->result_array();
	}
	// function add($data){
			// $this->db->insert('t_sekolah_program_kerja',$data);
	// }
	// function add_sub($data){
			// $this->db->insert('t_sekolah_sub_program_kerja',$data);
	// }
	// function edit($data,$id=0){
			// $this->db->where('id_sekolah_program_kerja',$id);
			// $this->db->update('t_sekolah_program_kerja',$data);
	// }
	function edit_sub($data,$id=0){
			$this->db->where('id_sekolah_sub_program_kerja',$id);
			$this->db->update('t_sekolah_sub_program_kerja',$data);
	}
	// function delete($id_sekolah_program_kerja){
			// $this->db->where('id_sekolah_program_kerja', $id_sekolah_program_kerja);
			// $this->db->delete('t_sekolah_program_kerja');
	// }
	// function delete_sub($id_sekolah_sub_program_kerja){
			// $this->db->where('id_sekolah_sub_program_kerja', $id_sekolah_sub_program_kerja);
			// $this->db->delete('t_sekolah_sub_program_kerja');
	// }

	function getJumlahAlokasi($id){
		$this->db->select_sum('alokasi_dana');
		$this->db->from('t_sekolah_sub_program_kerja');
		$this->db->where('id_sekolah_program_kerja', $id);
		$this->db->group_by('id_sekolah_program_kerja');
		return $this->db->get();
	}

	function getJumlahRealisasi(){
		$this->db->select_sum('pendanaan');
		$this->db->from('t_sekolah_sub_program_kerja');
		$this->db->group_by('id_sekolah_program_kerja');
		return $this->db->get();
	}

}