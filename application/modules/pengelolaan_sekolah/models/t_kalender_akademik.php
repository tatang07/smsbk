<?php
class t_kalender_akademik extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_all_kalender_akademik(){
			$query = $this->db
						->select('*')
						->from('t_kalender_akademik k')
						->join('m_term t', 'k.id_term = t.id_term')
						->where('k.id_sekolah', get_id_sekolah())
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_tahun_ajaran(){
			$query = $this->db
						->from('m_tahun_ajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_term(){
			$query = $this->db
						->from('m_term')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_kalender_akademik_by($id){
			$query = $this->db->query("SELECT * FROM t_kalender_akademik where id_kalender_akademik = '$id';");
			return $query->row();
		}
		
		public function update($id_kalender_akademik, $id_sekolah, $id_term, $kegiatan, $tanggal_mulai, $tanggal_selesai, $keterangan){
			$data=array(
				'id_sekolah'=> $id_sekolah,
				'id_term'=> $id_term,
				'kegiatan'=> $kegiatan,
				'tanggal_mulai'=> $tanggal_mulai,
				'tanggal_selesai'=> $tanggal_selesai,
				'keterangan'=> $keterangan
			);
			
			$this->db->where('id_kalender_akademik', $id_kalender_akademik);
			$this->db->update('t_kalender_akademik', $data);
		}
		
		public function tambah($id_sekolah, $id_term, $kegiatan, $tanggal_mulai, $tanggal_selesai, $keterangan){
			$data=array(
				'id_sekolah'=> $id_sekolah,
				'id_term'=> $id_term,
				'kegiatan'=> $kegiatan,
				'tanggal_mulai'=> $tanggal_mulai,
				'tanggal_selesai'=> $tanggal_selesai,
				'keterangan'=> $keterangan
			);
			$this->db->insert('t_kalender_akademik', $data);
		}
		
		public function delete($id){
			$this->db->delete('t_kalender_akademik', array('id_kalender_akademik'=>$id));
		}
		
}