<?php
class m_visi_misi extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_all_visi(){
			$query = $this->db
						->from('m_sekolah_visi')
						->where('id_sekolah', get_id_sekolah())
						->order_by('urutan asc')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_misi(){
			$query = $this->db
						->from('m_sekolah_misi')
						->where('id_sekolah', get_id_sekolah())
						->order_by('urutan asc')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_visi_by($id){
			$query = $this->db->query("SELECT * FROM m_sekolah_visi where id_sekolah_visi = '$id';");
			return $query->row();
		}
		
		public function get_misi_by($id){
			$query = $this->db->query("SELECT * FROM m_sekolah_misi where id_sekolah_misi = '$id';");
			return $query->row();
		}
		
		public function update_visi($id_visi, $visi, $urutan){
			$data=array(
				'visi'=> $visi,
				'urutan'=> $urutan
			);
			
			$this->db->where('id_sekolah_visi', $id_visi);
			$this->db->update('m_sekolah_visi', $data);
		}
		
		public function update_misi($id_misi, $misi, $urutan){
			$data=array(
				'misi'=> $misi,
				'urutan'=> $urutan
			);
			
			$this->db->where('id_sekolah_misi', $id_misi);
			$this->db->update('m_sekolah_misi', $data);
		}
		
		public function tambah_visi($id_sekolah, $visi, $urutan){
			$data=array(
				'id_sekolah'=> $id_sekolah,
				'visi'=> $visi,
				'urutan'=> $urutan
			);
			$this->db->insert('m_sekolah_visi', $data);
		}
		
		public function tambah_misi($id_sekolah, $misi, $urutan){
			$data=array(
				'id_sekolah'=> $id_sekolah,
				'misi'=> $misi,
				'urutan'=> $urutan
			);
			$this->db->insert('m_sekolah_misi', $data);
		}
		public function delete_visi($id){
			$this->db->delete('m_sekolah_visi', array('id_sekolah_visi'=>$id));
		}
		
		public function delete_misi($id){
			$this->db->delete('m_sekolah_misi', array('id_sekolah_misi'=>$id));
		}
		
}