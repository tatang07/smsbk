<?php
class m_review_proses extends MY_Model {
    function get_program(){
        $this->db->select('*');
        $this->db->from('t_sekolah_program_kerja');
        return $this->db->get();
    }

    function get_data($id_sekolah_program_kerja){
    	$this->db->select('*');
		$this->db->from('t_sekolah_evaluasi_program_proses sm');
        $this->db->join('t_sekolah_program_kerja sp', 'sm.id_sekolah_program_kerja = sp.id_sekolah_program_kerja');
		$this->db->where('sm.id_sekolah_program_kerja', $id_sekolah_program_kerja);
		return $this->db->get();
    }

    function get_all(){
        $this->db->select('*');
        $this->db->from('t_sekolah_evaluasi_program_proses sm');
        $this->db->join('t_sekolah_program_kerja sp', 'sm.id_sekolah_program_kerja = sp.id_sekolah_program_kerja');
        return $this->db->get();
    }

    function get_data_by_id($id){
    	$this->db->select('*');
		$this->db->from('t_sekolah_evaluasi_program_proses');
		$this->db->where('id_sekolah_evaluasi_program_proses', $id);
		return $this->db->get();
    }

    function submit($data){
    	$this->db->insert('t_sekolah_evaluasi_program_proses',$data);
    }

    function update($data, $id){
    	$this->db->where('id_sekolah_evaluasi_program_proses',$id);
		$this->db->update('t_sekolah_evaluasi_program_proses',$data);
    }

    function delete($id){
    	$this->db->where('id_sekolah_evaluasi_program_proses', $id);
		$this->db->delete('t_sekolah_evaluasi_program_proses');
    }
}
	