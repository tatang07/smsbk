<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class rpp_tematik extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('t_rpp_tematik');
			$this->load->helper('download');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kurikulum/rpp_tematik/home');
		}
		
		public function home(){
			
			$data['tematik'] = $this->t_rpp_tematik->get_all_tematik();
			$data['tingkat'] = $this->t_rpp_tematik->get_all_tingkat();
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/rpp_tematik/home";
			$config["total_rows"] = $this->t_rpp_tematik->count_datalist(0);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->t_rpp_tematik->get_datalist(0, $config["per_page"], $page);
			$data['first_tingkat'] = 0;
			$data['first_tematik'] = 0;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['stat'] = 'home';
			$data['jumlah'] = $this->t_rpp_tematik->count_datalist(0);
			
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			render("rpp_tematik/rpp_tematik", $data);
		}
		
		public function search(){
			
			$id_tingkat = $this->input->post('tingkat');
			$id_tematik = $this->input->post('tematik');
			
			$data['tematik'] = $this->t_rpp_tematik->get_all_tematik();
			$data['tingkat'] = $this->t_rpp_tematik->get_all_tingkat();
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/rpp_tematik/home";
			$config["total_rows"] = $this->t_rpp_tematik->count_datalist($id_tematik);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->t_rpp_tematik->get_datalist($id_tematik, $config["per_page"], $page);
			$data['first_tingkat'] = $id_tingkat;
			$data['first_tematik'] = $id_tematik;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['stat'] = 'search';
			$data['jumlah'] = $this->t_rpp_tematik->count_datalist($id_tematik);
			
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			render("rpp_tematik/rpp_tematik", $data);
		}
		
		public function add(){
			$data['term'] = $this->t_rpp_tematik->get_all_term();
			$data['tematik'] = $this->t_rpp_tematik->get_all_tematik();
			
			$data['component'] = 'kurikulum';
			render("rpp_tematik/form_tambah", $data);
		}
		
		public function submit(){
			$this->form_validation->set_rules('nama_rpp_tematik', 'Kode Pelajaran', 'required');
			$this->form_validation->set_rules('pertemuan_ke', 'Pelajaran', 'required');
			$this->form_validation->set_rules('tematik', ' Guru Matpel Rombel', 'required');
			$this->form_validation->set_rules('term', ' Guru Matpel Rombel', 'required');
			
			if($this->form_validation->run() == TRUE){
				$nama_rpp_tematik = $this->input->post('nama_rpp_tematik');
				$pertemuan_ke = $this->input->post('pertemuan_ke');
				$tematik = $this->input->post('tematik');
				$term = $this->input->post('term');
				
				$config['upload_path'] = './extras/rpp_tematik/';
				$config['allowed_types'] = 'pdf|doc|docx|xls|txt';
				$config['max_size']	= '10000';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$error = array('error' => $this->upload->display_errors());
					set_error_message($error);
					redirect("kurikulum/rpp_tematik/home/");
				}
				else
				{
					$lokasi_file = $this->upload->data();
				}
			
				$action = $this->input->post('action');
				if($action == 'add'){
					$this->t_rpp_tematik->insert($nama_rpp_tematik, $pertemuan_ke, $lokasi_file['full_path'], $term, $tematik);
					set_success_message('Data Berhasil Ditambah!');
					redirect("kurikulum/rpp_tematik/home/");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian!');
				redirect("kurikulum/rpp_tematik/home/e");
			}
		}
		
		public function delete(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/rpp_tematik/home');
			}
			
			$this->t_rpp_tematik->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('kurikulum/rpp_tematik/home/d');
		}
		
		public function download(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/rpp_tematik/home');
			}
			$row = $this->t_rpp_tematik->get_rpp_tematik_by($id);
			$file = $row->lokasi_file;
			$data = file_get_contents($file);
			$filename = basename($file);;
			
			force_download($filename, $data);
		}
		
}
