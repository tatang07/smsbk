<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class jadwal_pelajaran extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_jadwal_pelajaran');
			$this->load->helper('download');
			$this->load->helper('file');
			$this->load->library('csvimport');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kurikulum/jadwal_pelajaran/home');
		}
		
		public function home(){
			
			$id_sekolah = get_id_sekolah();
			
			$first_rombel = $this->m_jadwal_pelajaran->get_first_rombel();
			
			if($first_rombel){
				$id_rombel = $first_rombel->id_rombel;
			}else{
				$id_rombel = 0;
			}
			
			$first_guru = $this->m_jadwal_pelajaran->get_first_guru();
			
			if($first_guru){
				$id_guru = $first_guru->id_guru;
			}else{
				$id_guru = 0;
			}
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/jadwal_pelajaran/home";
			$config["total_rows"] = $this->m_jadwal_pelajaran->count_datalist($id_sekolah);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->m_jadwal_pelajaran->get_datalist($id_sekolah, $config["per_page"], $page);
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			
			$data['id_rombel'] = $id_rombel;
			$data['id_guru'] = $id_guru;
			
			$data['stat'] = 'home';
			$data['jumlah'] = $this->m_jadwal_pelajaran->count_datalist($id_sekolah);
			
			$data['gid'] = $gid = get_usergroup();
			
			$last = $this->m_jadwal_pelajaran->get_last_jadwal_pelajaran();
			if($gid==9){
				redirect('kurikulum/jadwal_pelajaran/jadwal_guru/'.$last->id_jadwal_pelajaran.'/'.get_id_personal());
			}
			$data['component'] = 'kurikulum';
			render("jadwal_pelajaran/jadwal_pelajaran", $data);
		}
		
		public function jadwal_kelas(){
			$id_sekolah = get_id_sekolah();
			$id_jadwal_pelajaran = $this->uri->segment(4);
			$id_rombel = $this->uri->segment(5);
			
			if(isset($_POST['id_jadwal_pelajaran'])){
				$id_jadwal_pelajaran = $_POST['id_jadwal_pelajaran'];
			}
			if(isset($_POST['id_rombel'])){
				$id_rombel = $_POST['id_rombel'];
			}
			
			$data['rombel'] = $this->m_jadwal_pelajaran->get_all_rombel();
			$data['first_rombel'] = $id_rombel;
			$data['id_jadwal_pelajaran'] = $id_jadwal_pelajaran;
			
			$data['datalist'] = $this->m_jadwal_pelajaran->get_jadwal_pelajaran_detail_kelas($id_jadwal_pelajaran, $id_rombel, $id_sekolah);
			
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			
			render("jadwal_pelajaran/jadwal_kelas", $data);
		}
		
		public function jadwal_Guru(){
			$id_sekolah = get_id_sekolah();
			$id_jadwal_pelajaran = $this->uri->segment(4);
			$id_guru = $this->uri->segment(5);
			
			if(isset($_POST['id_jadwal_pelajaran'])){
				$id_jadwal_pelajaran = $_POST['id_jadwal_pelajaran'];
			}
			if(isset($_POST['id_guru'])){
				$id_guru = $_POST['id_guru'];
			}
			
			$data['guru'] = $this->m_jadwal_pelajaran->get_all_guru();
			$data['id_jadwal_pelajaran'] = $id_jadwal_pelajaran;

			$data['first_guru'] = $id_guru;
			
			$data['datalist'] = $this->m_jadwal_pelajaran->get_jadwal_pelajaran_detail_guru($id_jadwal_pelajaran, $id_guru, $id_sekolah);
			
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			render("jadwal_pelajaran/jadwal_guru", $data);
		}
		
		public function delete(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/jadwal_pelajaran/home');
			}
			
			$this->m_jadwal_pelajaran->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('kurikulum/jadwal_pelajaran/home/d');
		}
		
		public function import(){
			$data['component'] = 'kurikulum';
			render("jadwal_pelajaran/form_import");
		}
		
		public function download(){
			
			$file = './extras/csv/format-import-jadwal.csv';
			$data = file_get_contents($file);
			$filename = basename($file);;
			
			force_download($filename, $data);
		}
		
		public function submit_import(){
			
			$this->form_validation->set_rules('nama', 'Nama Jadwal Pelajaran', 'required');
			
			if($this->form_validation->run() == TRUE){
				$nama = $this->input->post('nama');
				$id_sekolah = get_id_sekolah();
				$status_aktif = 1;
				
				
				
				
				$config['upload_path'] = './extras/dokumen_kurikulum/';
				$config['allowed_types'] = 'csv';
				$config['max_size'] = '1000';
		 
				$this->load->library('upload', $config);
		 
		 
				// If upload failed, display error
				if (!$this->upload->do_upload()) {
					$data['error'] = $this->upload->display_errors();
		 
					$this->load->view('csvindex', $data);
				} else {
					$file_data = $this->upload->data();
					$file_path =  './extras/dokumen_kurikulum/'.$file_data['file_name'];
					
					$status=0;
					
					if ($this->csvimport->get_array($file_path)) {
						$csv_array = $this->csvimport->get_array($file_path);
						$temp = $this->csvimport->get_array($file_path);
					
						$error = array();
						
						foreach ($temp as $index => $row) {
							//ngecek nama hari
							$hari = $row['Hari'];
							if(ucwords($hari)=='SENIN' || ucwords($hari)=='SELASA' || ucwords($hari)=='RABU' || ucwords($hari)=='KAMIS' || ucwords($hari)=='JUMAT' || ucwords($hari)=='SABTU' || ucwords($hari)=='MINGGU'){
								$error[$index] = array('Hari' => '');
							}else{
								$error[$index] = array('Hari' => ucwords($hari));
							}
							
							//ngecek kode ruang ada ngga
							$ruang_belajar = $this->m_jadwal_pelajaran->get_ruang_belajar_by($row['Kode Ruang'], get_id_sekolah());
							if(!$ruang_belajar){
								$id_ruang_belajar = $this->m_jadwal_pelajaran->insert_ruang_belajar($row['Kode Ruang'], $row['Kode Ruang'], get_id_sekolah());
								// $error[$index] += array('Kode Ruang' =>$row['Kode Ruang']);
							}else{
								$error[$index] += array('Kode Ruang' =>'');
							}
							
							//ngecek kode guru, kelas, kode mata pelajaran
							$guru_matpel_rombel = $this->m_jadwal_pelajaran->get_guru_matpel_rombel_by($row['Kode Guru'], $row['Kode Mata Pelajaran'], $row['Kelas']);
							if(!$guru_matpel_rombel){
								$error[$index] += array('Kode Guru' =>$row['Kode Guru']);
								$error[$index] += array('Kelas' =>$row['Kelas']);
								$error[$index] += array('Kode Mata Pelajaran' =>$row['Kode Mata Pelajaran']);
							}else{
								$error[$index] += array('Kode Guru' => '');
								$error[$index] += array('Kelas' => '');
								$error[$index] += array('Kode Mata Pelajaran' => '');
							}
						}
						$status=0;
						foreach($error as $index => $e){
							if($e['Hari']=='' && $e['Kode Ruang']=='' && $e['Kode Guru']==''){
								$status++;
							}
						}
						
						$er['error'] = $error;
						if($status != count($csv_array)){
							render("jadwal_pelajaran/list_error", $er);
						}else{
							
							$this->m_jadwal_pelajaran->insert_jadwal($nama, $status_aktif, $id_sekolah);
							
							foreach ($csv_array as $row) {

								$mulai = $row['Mulai'];
								$selesai = $row['Akhir'];
								$kode_ruang = $row['Kode Ruang'];
								$kode_guru = $row['Kode Guru'];
								$rombel = $row['Kelas'];
								$kode_pelajaran = $row['Kode Mata Pelajaran'];
								
								
								$data['hari'] = $row['Hari'];
								$baris = $this->m_jadwal_pelajaran->get_last_jadwal_pelajaran();
								$data['id_jadwal_pelajaran'] = $baris->id_jadwal_pelajaran;
								
								$guru_matpel_rombel = $this->m_jadwal_pelajaran->get_guru_matpel_rombel_by($kode_guru, $kode_pelajaran, $rombel);
								$ruang_belajar = $this->m_jadwal_pelajaran->get_ruang_belajar_by($kode_ruang, $id_sekolah);
								// $jam_pelajaran = $this->m_jadwal_pelajaran->get_jam_pelajaran_by($mulai, $selesai, $id_sekolah);
								
									foreach($guru_matpel_rombel as $a){
										foreach($ruang_belajar as $b){
										// foreach($jam_pelajaran as $c){
											$data['id_guru_matpel_rombel'] = $a->id_guru_matpel_rombel;
											$data['id_ruang_belajar'] = $b->id_ruang_belajar;
											$data['mulai'] = $mulai;
											$data['selesai'] = $selesai;
											// $data['id_jam_pelajaran'] = $c->id_jam_pelajaran;
											
											$this->m_jadwal_pelajaran->save($data);
										// }
										}
									}
							}
							set_success_message('Data Berhasil Diimport');
							redirect("kurikulum/jadwal_pelajaran/");
						}
						
					} else{
						set_error_message('Terjadi Kesalahan Saat Upload File!');
						redirect("kurikulum/jadwal_pelajaran/");
					}
				}
			}else{
				set_error_message('Terjadi Kesalahan Pengisian!');
				redirect("kurikulum/jadwal_pelajaran/");
			}
		}
		
		// public function coba(){
			// $pelajaran = $this->m_jadwal_pelajaran->get_all_pelajaran();
			
			// $temp = array();
			// foreach($pelajaran as $p){
				// $temp[$p->id_pelajaran] = array();
				// $guru_mapel = $this->m_jadwal_pelajaran->get_kelompok_guru($p->id_pelajaran);
				// if($guru_mapel){
					// foreach($guru_mapel as $gmp){
						// $temp[$p->id_pelajaran][$gmp->nama] = array();
					// }
				// }
			// }
			
			// $this->mydebug($temp);
		// }
		
		public function export(){
			
			$tingkat_kelas = $this->m_jadwal_pelajaran->get_all_tingkat(get_jenjang_sekolah());
			$rombel_peminatan = $this->m_jadwal_pelajaran->get_all_rombel_peminatan();
			$rombel = $this->m_jadwal_pelajaran->get_all_rombel();
			$peminatan = $this->m_jadwal_pelajaran->get_all_peminatan();
			$ruang_belajar = $this->m_jadwal_pelajaran->get_all_ruang_belajar();
			$guru = $this->m_jadwal_pelajaran->get_all_guru();
			$guru_matpel_rombel = $this->m_jadwal_pelajaran->get_all_guru_matpel_rombel();
			$kelompok_pelajaran = $this->m_jadwal_pelajaran->get_kelompok_pelajaran();
			$kelompok = $this->m_jadwal_pelajaran->get_all_kelompok_pelajaran();
			$pelajaran = $this->m_jadwal_pelajaran->get_all_pelajaran();
			$pelajaran_by = $this->m_jadwal_pelajaran->get_pelajaran_by(1);
			// $kelompok_guru = $this->m_jadwal_pelajaran->get_kelompok_guru();
			
			//temporary untuk kelas dan guru
			$temp = array();
			foreach($pelajaran as $p){
				$temp[$p->id_pelajaran] = array();
				$guru_mapel = $this->m_jadwal_pelajaran->get_kelompok_guru($p->id_pelajaran);
				if($guru_mapel){
					foreach($guru_mapel as $gmp){
						$temp[$p->id_pelajaran][$gmp->nama] = array();
						$o=0;
						foreach($guru_mapel as $gm){
							if($gm->nama == $gmp->nama){
								$temp[$p->id_pelajaran][$gmp->nama][$o] = $gm->rombel;
								$o++;
							}
						}
					}
				}
			}
			
	
			$data = array();
			$data['kelas'] = array();
			$data['ruang'] = array();
			$data['guru'] = array();
			$data['mapel'] = array();
			
			foreach($tingkat_kelas as $t){
				$data['kelas'][$t->tingkat_kelas] = array();
				
				$data['kelas'][$t->tingkat_kelas]['data'] = array();
				
				$data['kelas'][$t->tingkat_kelas]['kelas'] = array();
				
				$i=0;
				foreach($rombel_peminatan as $r){
					if($t->id_tingkat_kelas == $r->id_tingkat_kelas){
						$data['kelas'][$t->tingkat_kelas]['kelas'][$i] = array();
						$data['kelas'][$t->tingkat_kelas]['kelas'][$i]['kelas'] = $r->rombel;
						$data['kelas'][$t->tingkat_kelas]['kelas'][$i]['peminatan'] = $r->peminatan_singkatan;
						$i++;
					}
				}
			}
			
			$i=0;
			if($ruang_belajar){
				foreach($ruang_belajar as $r){
					$data['ruang'][$i] = array();
					$data['ruang'][$i]['kode'] = $r->kode_ruang_belajar;
					$data['ruang'][$i]['nama_ruang'] = $r->ruang_belajar;
					$i++;
				}
			}
			
			$i=0;
			foreach($guru as $g){
				$data['guru'][$i] = array();
				$data['guru'][$i]['nama'] = $g->nama;
				$data['guru'][$i]['kode'] = $g->kode_guru;
				$data['guru'][$i]['jam_ngajar'] = 0;
				$i++;
			}
			
			$i=0;
			foreach($kelompok as $k){
				if($k->status_pilihan==0){
					$data['mapel'][$i] = array();
					$data['mapel'][$i]['kategori_mapel'] = $k->kelompok_pelajaran;
					foreach($tingkat_kelas as $ti){
						$data['mapel'][$i]['kelas'] = $ti->tingkat_kelas;
						$data['mapel'][$i]['jurusan'] = 'all';
						$data['mapel'][$i]['mapel'] = array();
						$j=0;
						foreach($kelompok_pelajaran as $p){
							
							if($p->id_kelompok_pelajaran == $k->id_kelompok_pelajaran && $p->id_tingkat_kelas==$ti->id_tingkat_kelas){
								$data['mapel'][$i]['mapel'][$j] = array();
								$data['mapel'][$i]['mapel'][$j]['kode'] = $p->kode_pelajaran;
								$data['mapel'][$i]['mapel'][$j]['mapel'] = $p->pelajaran;
								$data['mapel'][$i]['mapel'][$j]['jam_minggu'] = $p->jumlah_jam;
								$data['mapel'][$i]['mapel'][$j]['guru'] = array();
								$c=0;
								foreach($temp as $t => $val){
									
									if($t == $p->id_pelajaran){
										if($val){
											$data['mapel'][$i]['mapel'][$j]['guru'][$c] = array();
											foreach($val as $guru => $v){
												$data['mapel'][$i]['mapel'][$j]['guru'][$c]['guru'] = $guru;
												$data['mapel'][$i]['mapel'][$j]['guru'][$c]['kelas'] = $v;
												
												$c++;
											}
										}
									}
								}
								$j++;
							}
						}
					$i++;
					}
				}else{
					//mapel untuk kelompok peminatan c
					foreach($peminatan as $p){
						foreach($tingkat_kelas as $t){
							$data['mapel'][$i]['kategori_mapel'] = $k->kelompok_pelajaran;
							$data['mapel'][$i]['kelas'] = $t->tingkat_kelas;
							$data['mapel'][$i]['jurusan'] = $p->peminatan_singkatan;
							$data['mapel'][$i]['mapel'] = array();
							$g=0;
							foreach($kelompok_pelajaran as $l){
								if($l->id_peminatan==$p->id_peminatan && $l->id_tingkat_kelas==$t->id_tingkat_kelas){
									$data['mapel'][$i]['mapel'][$g]['kode'] = $l->kode_pelajaran;
									$data['mapel'][$i]['mapel'][$g]['mapel'] = $l->pelajaran;
									$data['mapel'][$i]['mapel'][$g]['jam_minggu'] = $l->jumlah_jam;
									$data['mapel'][$i]['mapel'][$g]['guru'] = array();
									
									$c=0;
									foreach($temp as $te => $val){
										
										if($te == $l->id_pelajaran){
											$data['mapel'][$i]['mapel'][$g]['guru'][$c] = array();
											foreach($val as $guru => $v){
												$data['mapel'][$i]['mapel'][$g]['guru'][$c]['guru'] = $guru;
												$data['mapel'][$i]['mapel'][$g]['guru'][$c]['kelas'] = $v;
												
												$c++;
											}
										}
									}
									
									$g++;
								}
							}
							$i++;
						}
					}
				}
			}
		
			// $vPath = './extras/sekolah';
			// mkdir($vPath);
			// write_file("$vPath/export.json", );
			// echo $vPath;
			
			// $this->mydebug($data);
			
			
			force_download('export_jadwal.smsbk', json_encode($data, JSON_PRETTY_PRINT));
			$data['component'] = 'kurikulum';
			render("jadwal_pelajaran/form_import");
		}
		
}
