<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class kalender_akademik extends Simple_Controller {
		public function index(){
			redirect('kurikulum/kalender_akademik/datalist');
	    }
    	public $myconf = array(
			//Nama controller
			"name" 			=> "kurikulum/kalender_akademik",
			"component"		=> "kurikulum",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "KALENDER AKADEMIK",
			"subtitle"		=> "",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "t_kalender_akademik",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"kegiatan"=>"Kegiatan",
								"tanggal_mulai"=>"Tanggal Mulai",					
								"tanggal_selesai"=>"Tanggal Selesai",
								"keterangan"=>"Keterangan"
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								//"tanggal"=>"date",
								//"id_pihak_komunikasi"=>"hidden",
								"id_kalender_akademik"=>"hidden",
						
								"tanggal_mulai"=>"date",
								"tanggal_selesai"=>"date",
								"id_term"=>"hidden",
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"keterangan","kegiatan","tanggal_mulai"
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								"id_kalender_akademik"
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("kegiatan","tanggal_mulai","tanggal_selesai","keterangan"),
								"field_sort"			=> array("kegiatan","tanggal_mulai","tanggal_selesai","keterangan"),
								"field_filter"			=> array("kegiatan","tanggal_mulai","tanggal_selesai","keterangan"),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(),
								//"field_comparator"		=> array("id_siswa"=>"=","id_sekolah"=>"="),
								//"field_operator"		=> array("id_siswa"=>"AND","id_sekolah"=>"AND"),
								//"field_align"			=> array(),
								"field_size"			=> array("action_col"=>"150"),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array('detail'=>'kurikulum/kalender_akademik/detail'),
								"custom_add_link"		=> array('label'=>'Add','href'=>'kurikulum/kalender_akademik/add' ),
								//"custom_edit_link"		=> array(),
								//"custom_delete_link"	=> array(),
								//"custom_link"			=> array("Kurikulum"=>"kurikulum/datalist","Siswa"=>"Siswa/datalist"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("kegiatan","tanggal_mulai","tanggal_selesai","keterangan"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								//"redirect_link"			=> ""
							),
			//Konfigurasi tampilan edit
			/*"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"view_file"				=> "simple/edit",
								"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array(),
								"custom_action"			=> "",
								"msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								"msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);
	

		public function pre_process($data){
			//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
			//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
			
			
			$id_kalender_akademik = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			//$list_pihak_komunikasi = get_list("m_siswa","id_siswa","nama");
			$data['conf']['data_list']['subtitle'] ="Kalender Akademik";
			$data['conf']['data_add']['subtitle'] = "Kalender Akademik";
			$data['conf']['data_edit']['subtitle'] = "Kalender Akademik";
			
			$data['conf']['data_delete']['redirect_link'] = "kurikulum/kalender_akademik/datalist/";
			$data['conf']['data_add']['redirect_link'] = "kurikulum/kalender_akademik/datalist/";
			$data['conf']['data_edit']['redirect_link'] = "kurikulum/kalender_akademik/datalist/";
			
			return $data;
		}
		
		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
			//$id_siswa = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			//$_POST['filter']['id_siswa'] = $id_siswa;
			//$_POST['filter']['id_sekolah'] = get_id_sekolah();
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															//'id_siswa'=>$id_siswa, 
															//'id_sekolah'=>get_id_sekolah() 
														);
			
			//Mengganti link add yang ada di kanan atas form datalist
			$data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'kurikulum/kalender_akademik/add/');			
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'kurikulum/kalender_akademik/edit/' );
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'kurikulum/kalender_akademik/delete/' );
			
			return $data;
		}
		
		public function before_datatable($data){
			//Method ini hanya akan dieksekusi di awal method datatable
			//Mengambil id_pihak yang diposting dari form datalist
			$filter = $this->input->post("filter");
			
			//$id_siswa = $filter['id_sekolah'];
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'kurikulum/edit/' );
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'kurikulum/delete/' );
			
			return $data;
		}
		
				
		public function before_render_add($data){
			if(!$this->input->post()){
				//$id_siswa = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
				//mengisi nilai untuk id_pihak_komunikasi di form input
				//$_POST['item']['id_siswa'] = $id_siswa; //name='item[id_pihak_kom..]'
			}
			return $data;
		}
		
		public function before_add($data){
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
				//$data['item']['id_sekolah'] = get_id_sekolah();
			}
			return $data;
		}
				
		public function before_edit($data){
			if(!$this->input->post()){
				$data['id'] = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 0;
			}
			return $data;
		}
		
		public function before_delete($data){
			$data['id'] = $this->uri->rsegment(3);
			return $data;
		}
		
	

}
