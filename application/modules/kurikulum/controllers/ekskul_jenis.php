<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class ekskul_jenis extends Simple_Controller {
    	 public function index(){
			redirect('kurikulum/ekskul_jenis/datalist');
	    }

    	public $myconf = array(
			//Nama controller
			"name" 			=> "kurikulum/ekskul_jenis",
			"component"		=> "kurikulum",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Kurikulum",
			"subtitle"		=> "Jenis Ekstrakurikuler",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "t_ekstra_kurikuler",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"jenis"=>"Jenis",
								"nama_ekstra_kurikuler"=>"Nama Ekstra Kurikuler",
								"tujuan"=>"Tujuan",
								"pelatih"=>"Penanggung Jawab",
								"proker"=>"Proker",
								"lokasi_file_lambang"=>"Foto",
								"tentang"=>"Deskripsi",
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								// "tentang"=>"textarea",
								"lokasi_file_lambang"=>"file",
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"nama_ekstra_kurikuler"
							), 
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "ekskul_jenis/datalist",
								//"view_file_datatable"	=> "ekskul_jenis/datatable",
								"view_file_datatable_action"	=> "ekskul_jenis/datatable_action",
								"field_list"			=> array("nama_ekstra_kurikuler", "pelatih", "proker", "tujuan"),
								"field_sort"			=> array("nama_ekstra_kurikuler", "pelatih", "proker", "tujuan"),
								"field_filter"			=> array("nama_ekstra_kurikuler", "pelatih", "proker", "tujuan"),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(
																"id_ekstra_kurikuler"=>array("label"=>"Ekstra Kurikuler","table"=>"m_ekstra_kurikuler", 
																							"table_id"=>"id_ekstra_kurikuler", "table_label"=>"nama_ekstra_kurikuler"),
															),
								"field_comparator"		=> array("jenis"=>"=","id_sekolah"=>'='),
								"field_operator"		=> array("jenis"=>"AND","id_sekolah"=>'AND'),
								//"field_align"			=> array(),
								"field_size"			=> array(),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array('Detail'=>'kurikulum/ekskul_jenis/detail'),
								"custom_add_link"		=> array('label'=>'Add','href'=>'kurikulum/ekskul_jenis/add' ),
								"custom_edit_link"		=> array('label'=>'Edit','href'=>'kurikulum/ekskul_jenis/edit'),
						
								//"custom_delete_link"	=> array(),
								//"custom_link"			=> array("program"=>"program/datalist","Siswa"=>"Siswa/datalist"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								// "view_file"				=> "kurikulum/program/detail",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("nama_ekstra_kurikuler", "tentang","pelatih", "proker", "tujuan","lokasi_file_lambang"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								// "redirect_link"			=> "kurikulum/program"
							),
			//Konfigurasi tampilan edit
			/*"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								// "enable"				=> TRUE,
								// "model_name"			=> "",
								// "table"					=> array(),
								"view_file"				=> "ekskul_jenis/edit",
								"view_file_form_input"	=> "ekskul_jenis/form_input",
								"field_list"			=> array("nama_ekstra_kurikuler", "pelatih", "proker", "tujuan","Foto"),
								// "custom_action"			=> "",
								// "msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								// "msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> "kurikulum"
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),*/
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> false,
								"enable_xls"			=> false,
								// "view_pdf"				=> "simple/laporan_datalist",
								// "view_xls"				=> "simple/laporan_datalist",
								// "field_size"			=> array(),
								// "orientation"			=> "p",
								// "special_number"		=> "",
								// "enable_header"			=> TRUE,
								// "enable_footer"			=> TRUE,
							)
		);
		public function detail($id){
		//print_r($id);
		$this->load->model('kurikulum/t_ekstra_kurikuler');
		$data['data'] = $this->t_ekstra_kurikuler->select_isi_by_id($id)->result();
		
		$data['component'] = 'kurikulum';
		render('ekskul_jenis/detail',$data);
		}
			
		
		public function pre_process($data){
			//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
			//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
						
			$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			// $list_program = get_list("m_program","id_program","program");
			$data['conf']['data_list']['subtitle'] = "Data Jenis Ekstra Kurikuler";
			
			$data['conf']['data_add']['subtitle'] = "Tambah";
			$data['conf']['data_edit']['subtitle'] = "Edit";
			
			$data['conf']['data_delete']['redirect_link'] = "kurikulum/ekskul_jenis/datalist/";
			$data['conf']['data_add']['redirect_link']    = "kurikulum/ekskul_jenis/datalist/";
			$data['conf']['data_edit']['redirect_link']   = "kurikulum/ekskul_jenis/datalist/";
			
			return $data;
		}
		
		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
			$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			$_POST['filter']['jenis'] = 1;
			$_POST['filter']['id_sekolah'] = get_id_sekolah();

			$gid = get_usergroup();
			if($gid == 9){
				$data['conf']['data_list']['enable_action']=false;
				$data['conf']['data_add']['enable']=false;
				$data['conf']['data_edit']['enable']=false;
				$data['conf']['data_delete']['enable']=false;
			}
			
			
			
			$data['conf']['data_list']['field_filter_dropdown']['id_ekstra_kurikuler']['filter'] = array('jenis'=>1, 'id_sekolah'=>get_id_sekolah());
			// $_POST['filter']['id_program'] = $id_program;
			//$_POST['filter']['id_sekolah'] = get_id_sekolah();
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															//'id_program'=>$id_program
															//'id_sekolah'=>get_id_sekolah() 
														);
			
			//Mengganti link add yang ada di kanan atas form datalist
			$data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'kurikulum/ekskul_jenis/add/');			
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
		
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'kurikulum/ekskul_jenis/edit');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'kurikulum/ekskul_jenis/delete');
			
			return $data;
		}
		
		public function before_datatable($data){
			//Method ini hanya akan dieksekusi di awal method datatable
			//Mengambil id_pihak yang diposting dari form datalist
			$_POST['filter']['jenis'] = 1;
			$_POST['filter']['id_sekolah'] = get_id_sekolah();
			$filter = $this->input->post("filter");			
			// $id_program = $filter['id_program'];
			//Mengkustomisasi link tombol edit dan delete
			//print_r($_POST);
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'kurikulum/ekskul_jenis/edit/');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'kurikulum/ekskul_jenis/delete/');
			
			return $data;
		}
		
				
		public function before_render_add($data){
			if(!$this->input->post()){
				$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
				//mengisi nilai untuk id_ di form input
				$_POST['item']['id_program'] = $id_program;
			}
			return $data;
		}
		
		public function before_add($data){
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
				$data['item']['id_sekolah'] = get_id_sekolah();
				$data['item']['jenis'] = 1;
				
				// print_r($_POST['item');
				if($_FILES["item"]["error"]["lokasi_file_lambang"]==0){
					$fname = date("YmdHisu");
					$src = 'extras/ekstra_kurikuler/'. $fname.".jpg";
					move_uploaded_file($_FILES["item"]["tmp_name"]["lokasi_file_lambang"],$src);
					$data['item']['lokasi_file_lambang'] = $fname.".jpg";
					
					$config['image_library'] = 'gd2';
					$config['source_image'] = $src;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 200;
					$config['height'] = 200;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
				}
			}
			return $data;
		}
				
		public function before_edit($data){
			if(!$this->input->post()){
				$data['id'] = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 0;
				
			}else{
				if($_FILES["item"]["error"]["lokasi_file_lambang"]==0){
					$fname = date("YmdHisu");
					$src = 'extras/ekstra_kurikuler/'. $fname.".jpg";
					move_uploaded_file($_FILES["item"]["tmp_name"]["lokasi_file_lambang"],$src);
					$data['item']['lokasi_file_lambang'] = $fname.".jpg";
					
					$config['image_library'] = 'gd2';
					$config['source_image'] = $src;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 200;
					$config['height'] = 200;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
				}
			}
			return $data;
		}
		
		public function before_delete($data){
			$data['id'] = $this->uri->rsegment(3);
			return $data;
		}
}
