<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class presensi extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('t_presensi');
		}
		
		public function index(){
			redirect('kurikulum/presensi/home');
		}
		
		public function home(){
			
			$data['pelajaran'] = $this->t_presensi->get_all_pelajaran();
			$first_pelajaran = $this->t_presensi->get_first_pelajaran();
			$id_pelajaran = $first_pelajaran->id_pelajaran;
			$data['rombel'] = $this->t_presensi->get_all_rombel();
			$first_rombel = $this->t_presensi->get_first_rombel();
			if($first_rombel){
				$id_rombel = $first_rombel->id_rombel;
			}else{
				$id_rombel = 0;
			}
			$id_guru_matpel_rombel = 0;
			
			$id_agenda_kelas = 1;
			$data['list'] = $this->t_presensi->get_datalist($id_agenda_kelas);
			$data['jenis_absen'] = get_list("r_jenis_absen","id_jenis_absen", "jenis_absen");
			$data['siswa'] = $this->t_presensi->get_all_siswa();
			$data['rekap_absen'] = $this->t_presensi->get_rekap_absen($id_guru_matpel_rombel);
			$data['first_rombel'] = $id_rombel;
			$data['first_pelajaran'] = $id_pelajaran;
			$data['stat'] = 'home';
			$data['jumlah'] = $this->t_presensi->count_datalist($id_agenda_kelas);
			
			render("presensi/presensi", $data);
		}
		
		public function search(){
			
			$id_rombel = $this->input->post('rombel');
			$id_pelajaran = $this->input->post('pelajaran');
			
			$row = $this->t_presensi->get_guru_matpel_rombel($id_pelajaran, $id_rombel);
			if($row){
				$id_guru_matpel_rombel = $row->id_guru_matpel_rombel;
			}else{
				$id_guru_matpel_rombel = 0;
			}
			$id_term = get_id_term();
			$id_agenda_kelas = 1;
			
			$data['jenis_absen'] = get_list("r_jenis_absen","id_jenis_absen", "jenis_absen");
			$data['siswa'] = $this->t_presensi->get_all_siswa();
			$data['rekap_absen'] = $this->t_presensi->get_rekap_absen($id_guru_matpel_rombel);
			$data['id_guru_matpel_rombel'] = $id_guru_matpel_rombel;
			
			$data['pelajaran'] = $this->t_presensi->get_all_pelajaran();
			$data['rombel'] = $this->t_presensi->get_all_rombel();
			
			$data['first_rombel'] = $id_rombel;
			$data['first_pelajaran'] = $id_pelajaran;
			$data['stat'] = 'search';
			
			// $this->mydebug($data['jenis_absen']);
			render("presensi/presensi", $data);
		}
}
