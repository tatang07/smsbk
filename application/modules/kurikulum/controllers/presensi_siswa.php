<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class presensi_siswa extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_presensi_siswa');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kurikulum/presensi_siswa/home');
		}
		
		public function home(){
			
			$data['siswa'] = $this->m_presensi_siswa->get_all_siswa();
			$data['pelajaran'] = $this->m_presensi_siswa->get_all_pelajaran();
			$data['absen'] = $this->m_presensi_siswa->get_all_absen();
			$data['rombel'] = $this->m_presensi_siswa->get_all_rombel();
			
			$id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
			$id_gmr = isset($_POST['id_gmr'.$id_rombel])?$_POST['id_gmr'.$id_rombel]:0;
			$data["id_rombel"] = $id_rombel;
			$data["id_gmr"] = $id_gmr;
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/presensi_siswa/home";
			$config["total_rows"] = $this->m_presensi_siswa->count_datalist($id_gmr);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->m_presensi_siswa->get_datalist($id_gmr, $config["per_page"], $page);
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['status'] = 'home';
			$data['jumlah'] = $this->m_presensi_siswa->count_datalist($id_gmr);
			
			$data['component'] = 'kurikulum';
			render("presensi_siswa/presensi_siswa", $data);
		}
		
		public function search(){
			// echo 'it works!';
			
			$data['siswa'] = $this->m_presensi_siswa->get_all_siswa();
			$data['pelajaran'] = $this->m_presensi_siswa->get_all_pelajaran();
			$data['absen'] = $this->m_presensi_siswa->get_all_absen();
			$data['rombel'] = $this->m_presensi_siswa->get_all_rombel();
			
			$id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
			$id_gmr = isset($_POST['id_gmr'.$id_rombel])?$_POST['id_gmr'.$id_rombel]:0;
			$data["id_rombel"] = $id_rombel;
			$data["id_gmr"] = $id_gmr;
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/presensi_siswa/home";
			$config["total_rows"] = $this->m_presensi_siswa->count_datalist($id_gmr);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->m_presensi_siswa->get_datalist($id_gmr, $config["per_page"], $page);
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['status'] = 'search';
			$data['jumlah'] = $this->m_presensi_siswa->count_datalist($id_gmr);
			
			$data['component'] = 'kurikulum';
			render("presensi_siswa/presensi_siswa", $data);
		}
		
		public function add(){
			$data['siswa'] = $this->m_presensi_siswa->get_all_siswa();
			$data['guru'] = $this->m_presensi_siswa->get_all_guru();
			$data['pelajaran'] = $this->m_presensi_siswa->get_all_pelajaran();
			$data['rombel'] = $this->m_presensi_siswa->get_all_rombel();
			
			$data['component'] = 'kurikulum';
			render("presensi_siswa/form_tambah", $data);
		}
		
		public function submit(){
			
		}
		
}
