<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class jadwal_pengembangan_diri extends Simple_Controller {
    	
		
		public function index(){	
			$this->load->model('m_jadwal_pd');
			$data['data'] = $this->m_jadwal_pd->select_isi()->result_array();
			$data['list_pengembangan_diri'] = $this->m_jadwal_pd->getList()->result_array();
			$data['component'] = 'kurikulum';
			$data['gid'] = $gid = get_usergroup();
			render('pengembangan_diri_jadwal/jadwal_pengembangan_diri',$data);
		}
		
		public function tambah_data(){
			$this->load->model('m_jadwal_pd');
			$data['list_pengembangan_diri'] = $this->m_jadwal_pd->getList()->result_array();
			$data['component'] = 'kurikulum';
			render('pengembangan_diri_jadwal/tambah_jadwal_pengembangan_diri',$data);
		}
		public function simpan_tambah_data(){
			$this->load->model('m_jadwal_pd');
			$data['id_ekstra_kurikuler'] = $this->input->post('id_ekstra_kurikuler');
			$data['hari'] = $this->input->post('hari');
			$data['jam_mulai'] = $this->input->post('jam_mulai');
			$data['jam_selesai'] = $this->input->post('jam_selesai');
			$data['tempat'] = $this->input->post('tempat');
			$this->m_jadwal_pd->tambah_data($data);
			set_success_message('Data Berhasil Ditambah!');
			redirect(site_url('kurikulum/jadwal_pengembangan_diri/index/'));
		}
		
		public function simpan_edit_data(){
			$this->load->model('m_jadwal_pd');
			$id_ekstra_kurikuler = $this->input->post('id_ekstra_kurikuler');
			$id = $this->input->post('id');
			$hari = $this->input->post('hari');
			$jam_mulai = $this->input->post('jam_mulai');
			$jam_selesai = $this->input->post('jam_selesai');
			$tempat = $this->input->post('tempat');
			
			$this->m_jadwal_pd->update_data($id, $hari, $jam_mulai, $jam_selesai, $tempat, $id_ekstra_kurikuler);
			set_success_message('Data Berhasil Diupdate!');
			redirect(site_url('kurikulum/jadwal_pengembangan_diri/index/'));
		}
		
		public function edit_data($id){
			$this->load->model('m_jadwal_pd');
			$data['list_pengembangan_diri'] = $this->m_jadwal_pd->getList()->result_array();
			$data['id'] = $id;
			$data['data'] = $this->m_jadwal_pd->getDataById($id)->result_array();
			$data['component'] = 'kurikulum';
			// echo $data['$id'];
			render('pengembangan_diri_jadwal/edit_jadwal_pengembangan_diri',$data);
		}
		public function delete($id){
			$this->load->model('m_jadwal_pd');
			$this->m_jadwal_pd->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect(site_url('kurikulum/jadwal_pengembangan_diri/index/'));
		}

}
