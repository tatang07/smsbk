<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class konversi_nilai extends Simple_Controller {
    	public function index(){
			redirect('kurikulum/konversi_nilai/datalist');
	    }

    	public $myconf = array(
			//Nama controller
			"name" 			=> "kurikulum/konversi_nilai",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Konversi Nilai",
			"subtitle"		=> "",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "m_konversi_nilai",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"nilai_huruf"=>"Nilai Huruf",
								"nilai_min"=>"Nilai Minimum",					
								"nilai_max"=>"Nilai Maksimum",
								"nilai_angka"=>"Nilai Angka",
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								"id_konversi_nilai"=>"hidden"
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"nilai_huruf","nilai_min","nilai_max","nilai_angka"
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								"nilai_huruf","nilai_min","nilai_max","nilai_angka"
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("nilai_huruf","nilai_min","nilai_max","nilai_angka"),
								"field_sort"			=> array("nilai_huruf","nilai_min","nilai_max","nilai_angka"),
								"field_filter"			=> array(),
								"field_filter_hidden"	=> array("id_komponen_nilai"),
								"field_filter_dropdown"	=> array(),
								"field_comparator"		=> array(),
								"field_operator"		=> array(),
								//"field_align"			=> array(),
								"field_size"			=> array(),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array(),
								"custom_add_link"		=> array('label'=>'Tambah','href'=>'kurikulum/konversi_nilai/add' ),
								"custom_edit_link"		=> array('label'=>'Edit','href'=>'kurikulum/konversi_nilai/edit'),
								"custom_delete_link"	=> array('label'=>'Delete','href'=>'kurikulum/konversi_nilai/delete'),
								//"custom_link"			=> array("Kurikulum"=>"kurikulum/datalist","Siswa"=>"Siswa/datalist"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("id_konversi_nilai","nilai_huruf","nilai_min","nilai_max","nilai_angka"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								"redirect_link"			=> "kurikulum/konversi_nilai/datalist"
							),
			//Konfigurasi tampilan edit
			"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/edit",
								//"view_file_form_input"	=> "simple/form_input",
								//"field_list"			=> array(),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> "kurikulum/konversi_nilai/datalist"
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								//"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> "kurikulum/konversi_nilai/datalist"
							),
			//Konfigurasi tampilan export
			/*"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);	
		
		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
			$id_komponen = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			$_POST['filter']['id_komponen_nilai'] = $id_komponen;
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															'id_komponen_nilai'=>$id_komponen
														);
			
			//Mengganti link add yang ada di kanan atas form datalist
			$data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'kurikulum/konversi_nilai/add/'.$id_komponen );			
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
			//$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'kurikulum/konversi_nilai/edit/'.$id_komponen );
			//$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'kurikulum/konversi_nilai/delete/'.$id_komponen );
			
			return $data;
		}

		public function before_add($data){
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
				$id_komponen = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
				//mengisi nilai untuk id_komponen_nilai di form input
				$data['item']['id_komponen_nilai'] = $id_komponen; 
			}
			return $data;
		}
}
