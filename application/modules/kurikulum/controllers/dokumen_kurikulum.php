<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class dokumen_kurikulum extends Simple_Controller {
	public function __construct(){
		parent::__construct();
		// untuk load model
		$this->load->model('m_dokumen_kurikulum');
		$this->load->library('form_validation');
		$this->load->helper('download');
	}
	
	public function index(){
		redirect('kurikulum/dokumen_kurikulum/datalist');
	}

	public function datalist(){
		$data['kurikulum'] = $this->m_dokumen_kurikulum->get_all_kurikulum();
		$data['alldata'] = $this->m_dokumen_kurikulum->get_all_data();
		$data['jumlah'] = $this->m_dokumen_kurikulum->get_all_data_count();
		
		$data['nama'] = "";
		$data['dokumen'] = "";
		 
		$data['gid'] = $gid = get_usergroup();
		
		$data['component'] = 'kurikulum';
		render("dokumen_kurikulum/datalist", $data);
	}
	
	public function search(){
		$nama = $this->input->post('nama');
		$dokumen = $this->input->post('dokumen');
		$data['kurikulum'] = $this->m_dokumen_kurikulum->get_all_kurikulum();
		$data['alldata'] = $this->m_dokumen_kurikulum->get_data_search($nama, $dokumen);
		$data['jumlah'] = $this->m_dokumen_kurikulum->get_data_search_count($nama, $dokumen);
		
		$data['nama'] = $nama;
		$data['dokumen'] = $dokumen;
		 
		 $data['gid'] = $gid = get_usergroup();
		 
		 $data['component'] = 'kurikulum';
		render("dokumen_kurikulum/datalist", $data);
	}
	
	public function download($id, $dokumen){
		if ($id == NULL){
			redirect('kurikulum/dokumen_kurikulum/');
		}
		
		if($dokumen == 'silabus'){
			$row = $this->m_dokumen_kurikulum->get_silabus_by($id);
			$file = $row->lokasi_file;
		}elseif($dokumen == 'rpp'){
			$row = $this->m_dokumen_kurikulum->get_rpp_by($id);
			$file = $row->lokasi_file;
		}else{
			$row = $this->m_dokumen_kurikulum->get_file_by($id);
			$file = $row->lokasi_file;
		}
		
		$data = file_get_contents($file);
		$filename = basename($file);;
		
		force_download($filename, $data);
	}
	
	public function delete($id, $dokumen){
		if($dokumen == 'silabus'){
			$this->m_dokumen_kurikulum->delete_silabus($id);
		}elseif($dokumen == 'rpp'){
			$this->m_dokumen_kurikulum->delete_rpp($id);
		}else{
			$this->m_dokumen_kurikulum->delete_file($id);
		}
		set_success_message('Data Berhasil Dihapus!');
		redirect('kurikulum/dokumen_kurikulum');
	}
}
