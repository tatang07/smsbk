<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class ketersediaan_rpp extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('t_rpp');
			$this->load->helper('download');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kurikulum/ketersediaan_rpp/home');
		}
		
		public function home(){
			
			$data['pelajaran'] = $this->t_rpp->get_all_pelajaran();
			$first_pelajaran = $this->t_rpp->get_first_pelajaran();
			$id_pelajaran = $first_pelajaran->id_pelajaran;
			$data['tingkat'] = $this->t_rpp->get_all_tingkat();
			$data['kurikulum'] = $this->t_rpp->get_all_kurikulum();
			$data['spil'] = 0;
			$data['sting'] = 0;
			$first_rombel = $this->t_rpp->get_first_rombel();
			if($first_rombel){
				$id_rombel = $first_rombel->id_rombel;
			}else{
				$id_rombel = 0;
			}
			
			$id_term = get_id_term();
			
			// $id_tingkat_kelas = isset($_POST['id_tingkat_kelas'])?$_POST['id_tingkat_kelas']:0;
			// $id_pelajaran = isset($_POST['id_pelajaran'.$id_tingkat_kelas])?$_POST['id_pelajaran'.$id_tingkat_kelas]:0;
			// $data["id_tingkat_kelas"] = $id_tingkat_kelas;
			// $data["id_pelajaran"] = $id_tingkat_kelas;
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/ketersediaan_rpp/home";
			$config["total_rows"] = $this->t_rpp->count_datalist($id_pelajaran, $id_term);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->t_rpp->get_datalist_ketersediaan($id_pelajaran, $id_term, $config["per_page"], $page);
			$data['first_rombel'] = $id_rombel;
			$data['first_pelajaran'] = $id_pelajaran;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['stat'] = 'home';
			$data['jumlah'] = $this->t_rpp->count_datalist_ketersediaan($id_pelajaran, $id_term);
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			render("ketersediaan_rpp/ketersediaan_rpp", $data);
		}
		
		public function search(){
			
			$id_pelajaran = $this->input->post('pelajaran');
			$id_tingkat = $this->input->post('tingkat');
			$id_kurikulum = $this->input->post('kurikulum');
			
			$data["spil"] = $id_kurikulum;
			$data["sting"] = $id_tingkat;
			// $data["id_tingkat"] = $id_rombel;
			// $data["id_pelajaran"] = $id_gmr;
			// $row = $this->t_rpp->get_gmr_by($id_gmr);
			// $id_pelajaran = $row->id_pelajaran;
			
			$data['pelajaran'] = $this->t_rpp->get_all_pelajaran();
			$data['tingkat'] = $this->t_rpp->get_all_tingkat();
			$data['kurikulum'] = $this->t_rpp->get_all_kurikulum();
			
			$id_term = get_id_term();
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/ketersediaan_rpp/home";
			$config["total_rows"] = $this->t_rpp->count_datalist($id_pelajaran, $id_term);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->t_rpp->get_datalist_ketersediaan($id_pelajaran, $id_term, $config["per_page"], $page);
			$data['first_pelajaran'] = $id_pelajaran;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['stat'] = 'search';
			$data['jumlah'] = $this->t_rpp->count_datalist_ketersediaan($id_pelajaran, $id_term);
			
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			// print_r ($data['list']);
			render("ketersediaan_rpp/ketersediaan_rpp", $data);
		}
				
}
