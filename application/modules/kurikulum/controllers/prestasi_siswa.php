<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class prestasi_siswa extends CI_Controller {

	public function index()
	{	
		$this->load->model('kurikulum/m_prestasi_siswa');
		$data['ekskul'] = $this->m_prestasi_siswa->get_ekskul();
		$data['isi'] = $this->m_prestasi_siswa->get_data();
		
		$data['gid'] = $gid = get_usergroup();
		
		$data['component'] = 'kurikulum';
		render('prestasi_siswa/prestasi_siswa',$data);
	}
	public function load_add()
	{	
		$this->load->model('kurikulum/m_prestasi_siswa');
		$id_sekolah= get_id_sekolah();
	$data['tingkat_kelas']=0;
		$jenjang_sekolah = get_jenjang_sekolah();
		$data['siswa'] = $this->m_prestasi_siswa->select_siswa($id_sekolah);
		$data['ekskul'] = $this->m_prestasi_siswa->get_ekskul();
		$data['tingkat_wilayah'] = $this->m_prestasi_siswa->select_tingkat_wilayah();
		
		$data['component'] = 'kurikulum';
		render('prestasi_siswa/form_tambah',$data);
	}
	
	public function get_siswa($id_ekskul){
		$this->load->model('kurikulum/m_prestasi_siswa');
		$siswa = $this->m_prestasi_siswa->get_siswa($id_ekskul);
		
		$output = "";
		foreach($siswa as $m){
			$output .= "<option value='".$m['id_siswa']."'>".$m['nama']."</option>";
		}
			
		echo $output;
	}
		
	public function delete($id){
			$this->load->model('kurikulum/m_prestasi_siswa');
			$this->m_prestasi_siswa->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect(site_url('kurikulum/prestasi_siswa/index/')) ;
	}
	
	public function submit_add(){
		$this->load->library('form_validation');
		
		$this->form_validation->set_rules('ekskul', 'Ekstra Kurikuler', 'required');
		$this->form_validation->set_rules('id_siswa', 'Siswa', 'required');
		$this->form_validation->set_rules('perlombaan', 'Perlombaan', 'required');
		$this->form_validation->set_rules('kategori', 'Kategori', 'required');
		$this->form_validation->set_rules('id_tingkat_wilayah', 'Tingkat Wilayah', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('prestasi', 'Prestasi', 'required');
		
		if($this->form_validation->run() == TRUE){
			$this->load->model('kurikulum/m_prestasi_siswa');
			$data['id_siswa'] = $this->input->post('id_siswa');
			$data['id_ekstra_kurikuler'] = $this->input->post('ekskul');
			$data['prestasi'] = $this->input->post('prestasi');
			$data['kategori'] = $this->input->post('kategori');
			$data['perlombaan'] = $this->input->post('perlombaan');
			$data['tanggal'] = tanggal_db($this->input->post('tanggal'));
			$data['id_tingkat_wilayah'] = $this->input->post('id_tingkat_wilayah');
			set_success_message('Data Berhasil Ditambah!');
			$this->m_prestasi_siswa->add($data);
			redirect(site_url('kurikulum/prestasi_siswa/index/')) ;
			
		}else{
			set_error_message('Terjadi Kesalahan Pengisian! Data harus lengkap!');
			redirect("kurikulum/prestasi_siswa/index");
		}
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('pps/m_bp_bk');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_bp_bk->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_bp_bk->select_kelas($jenjang_sekolah);
			
			$data['component'] = 'kurikulum';
			render('bp_bk/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('pps/m_bp_bk');
			$id=$this->input->post('id');
			$data['id_siswa'] = $this->input->post('id_siswa');
			$data['tanggal_catatan'] = tanggal_db($this->input->post('tanggal'));
			$data['keterangan'] = $this->input->post('keterangan');


			$this->m_bp_bk->edit($data,$id);
			redirect(site_url('pps/bp_bk/index/')) ;
	}
	public function search(){
		$this->load->model('kurikulum/m_prestasi_siswa');
		$nama = $this->input->post('nama');
		$id_ekstra_kurikuler = $this->input->post('id_ekstra_kurikuler');
		$id_sekolah = get_id_sekolah();
		$data['ekskul'] = $this->m_prestasi_siswa->get_ekskul();
		
		$data['id_ekstra_kurikuler']=$this->input->post('id_ekstra_kurikuler');
		
		$data['isi'] = $this->m_prestasi_siswa->get_data_search($id_ekstra_kurikuler,$nama);
		
		$data['gid'] = $gid = get_usergroup();
		
		$data['component'] = 'kurikulum';
		render('prestasi_siswa/prestasi_siswa',$data);
		
	}
	
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */