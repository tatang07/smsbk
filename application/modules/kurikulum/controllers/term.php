<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class term extends Simple_Controller {
    	public function index(){
			redirect('kurikulum/term/datalist');
	    }

    	public $myconf = array(
			//Nama controller
			"name" 			=> "term",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Term",
			"subtitle"		=> "",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_tahun_ajaran"),
			"model_name"	=> "m_term",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"term"=>"Term"
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								"id_term"=>"hidden",
								"id_tahun_ajaran"=>"hidden"
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"term"
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								"term"
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("term"),
								"field_sort"			=> array("term"),
								"field_filter"			=> array("term"),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(),
								"field_comparator"		=> array("id_tahun_ajaran"=>"="),
								"field_operator"		=> array(),
								//"field_align"			=> array(),
								"field_size"			=> array(),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_add_link"		=> array('label'=>'Tambah','href'=>'kurikulum/term/add' ),
								"custom_edit_link"		=> array('label'=>'Edit','href'=>'kurikulum/term/edit'),
								"custom_delete_link"	=> array('label'=>'Delete','href'=>'kurikulum/term/delete'),
								// "custom_link"			=> array("Term"=>"kurikulum/term/datalist"),
								//"custom_link"			=> array("Kurikulum"=>"kurikulum/datalist","Siswa"=>"Siswa/datalist"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("id_term", "term", "id_tahun_ajaran"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								"redirect_link"			=> "kurikulum/term/datalist"
							),
			//Konfigurasi tampilan edit
			"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/edit",
								//"view_file_form_input"	=> "simple/form_input",
								//"field_list"			=> array(),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> "kurikulum/term/datalist"
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								//"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> "kurikulum/term/datalist"
							),
			//Konfigurasi tampilan export
			/*"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);	
}
