<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class silabus extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_silabus');
			$this->load->helper('download');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kurikulum/silabus/home');
		}
		
		public function home(){
		
			$data['pelajaran'] = $this->m_silabus->get_all_pelajaran();
			$first_pelajaran = $this->m_silabus->get_first_pelajaran();
			$id_pelajaran = $first_pelajaran->id_pelajaran;
			$data['kurikulum'] = $this->m_silabus->get_all_kurikulum();
			$data['tingkat'] = $this->m_silabus->get_all_tingkat(get_jenjang_sekolah());
			$first_kurikulum = $this->m_silabus->get_first_kurikulum();
			if($first_kurikulum){
				$id_kurikulum = $first_kurikulum->id_kurikulum;
			}else{
				$id_kurikulum = 0;
			}
			
			$id_sekolah = get_id_sekolah();
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/silabus/home";
			$config["total_rows"] = $this->m_silabus->count_datalist($id_kurikulum, $id_pelajaran, $id_sekolah);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->m_silabus->get_datalist($id_kurikulum, $id_pelajaran, $id_sekolah, $config["per_page"], $page);
			$data['first_kurikulum'] = $id_kurikulum;
			$data['first_pelajaran'] = 0;
			$data['spil'] = 0;
			$data['sting'] = 0;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['stat'] = 'home';
			$data['jumlah'] = $this->m_silabus->count_datalist($id_kurikulum, $id_pelajaran, $id_sekolah);
			$data['component'] = 'kurikulum';
			$data['gid'] = $gid = get_usergroup();
			
			render("silabus/silabus", $data);
		}
		
		public function search(){
		
			$id_kurikulum = $this->input->post('kurikulum');
			$id_pelajaran = $this->input->post('pelajaran');
			$id_tingkat = $this->input->post('tingkat');
			
			$data['pelajaran'] = $this->m_silabus->get_all_pelajaran();
			$data['kurikulum'] = $this->m_silabus->get_all_kurikulum();
			$data['tingkat'] = $this->m_silabus->get_all_tingkat(get_jenjang_sekolah());
			
			$row = $this->m_silabus->get_kurikulum_by($id_kurikulum);
			if($row){
				$id_kurikulum = $row->id_kurikulum;
			}else{
				$id_kurikulum = 0;
			}
			
			$row = $this->m_silabus->get_pelajaran_by($id_pelajaran);
			if($row){
			$id_pelajaran = $row->id_pelajaran;
			}
			
			$id_sekolah = get_id_sekolah();
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/silabus/home";
			$config["total_rows"] = $this->m_silabus->count_datalist($id_kurikulum, $id_pelajaran, $id_sekolah);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->m_silabus->get_datalist($id_kurikulum, $id_pelajaran, $id_sekolah, $config["per_page"], $page);
			$data['first_kurikulum'] = $id_kurikulum;
			$data['first_pelajaran'] = $id_pelajaran;
			$data['first_tingkat'] = $id_tingkat;
			$data['spil'] = $id_kurikulum;
			$data['sting'] = $id_tingkat;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['stat'] = 'search';
			$data['jumlah'] = $this->m_silabus->count_datalist($id_kurikulum, $id_pelajaran, $id_sekolah);
			
			$data['component'] = 'kurikulum';
			$data['gid'] = $gid = get_usergroup();
			
			render("silabus/silabus", $data);
		}
		
		public function add(){
			$data['kelompok_pelajaran'] = $this->m_silabus->get_all_kelompok_pelajaran();
			$data['pelajaran'] = $this->m_silabus->get_all_pelajaran();
			
			$data['component'] = 'kurikulum';
			render("silabus/form_tambah", $data);
		}
		
		public function get_pelajaran($id_kelompok_pelajaran){
			$mapel = $this->m_silabus->get_pelajaran_by_kelompok($id_kelompok_pelajaran);
			
			$output = "";
			foreach($mapel as $m){
				$tingkat_kelas = $this->m_silabus->get_all_tingkat(get_jenjang_sekolah());
				foreach($tingkat_kelas as $g){
					if($m->id_tingkat_kelas == $g->id_tingkat_kelas){
						// $row = $this->t_rpp->get_guru_matpel_rombel($g->id_guru, $m->id_pelajaran, $id_rombel);
						// $id_guru_matpel_rombel = $row->id_guru_matpel_rombel;
						
						$output .= "<option value='".$m->id_pelajaran."'>".$m->pelajaran." | ".$g->tingkat_kelas."</option>";
					}
				}
			}
				
			echo $output;
		}
		
		public function submit(){
			$this->form_validation->set_rules('nama_silabus', 'Nama Silabusn', 'required');
			$this->form_validation->set_rules('id_pelajaran', 'Pelajaran', 'required');
			$this->form_validation->set_rules('id_kelompok_pelajaran', 'Term', 'required');
			
			if($this->form_validation->run() == TRUE){
				$nama_silabus = $this->input->post('nama_silabus');
				$id_pelajaran = $this->input->post('id_pelajaran');
				$id_kelompok_pelajaran = $this->input->post('id_kelompok_pelajaran');
				$id_sekolah = get_id_sekolah();
				
				$config['upload_path'] = './extras/silabus/';
				$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|txt';
				$config['max_size']	= '100000000';
				$new_name = date('YmdHisu');
				$config['file_name'] = $new_name;

				$this->load->library('upload', $config);
				print_r($_FILES);
				
				if ( ! $this->upload->do_upload())
				{
					$error = array('error' => $this->upload->display_errors());
				}
				else
				{
					$lokasi_file = $this->upload->data();
					
				}
				
				
				$action = $this->input->post('action');
				if($action == 'add'){
					$row = $this->m_silabus->get_kelompok_pelajaran_by($id_kelompok_pelajaran);
					$id_kurikulum = $row->id_kurikulum;
					$this->m_silabus->insert($nama_silabus, base_url("extras/silabus/".$lokasi_file['file_name']), $id_pelajaran, $id_sekolah, $id_kurikulum);
					set_success_message('Data Berhasil Ditambah!');
					redirect("kurikulum/silabus/home/");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian!');
				redirect("kurikulum/silabus/home/e");
			}
		}
		
		public function delete(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/silabus/home');
			}
			
			$this->m_silabus->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('kurikulum/silabus/home/');
		}
		
		public function download(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/silabus/home');
			}
			$row = $this->m_silabus->get_silabus_by($id);
			$file = $row->lokasi_file;
			$data = file_get_contents($file);
			$filename = basename($file);;
			
			force_download($filename, $data);
		}
		
}
