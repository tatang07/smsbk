<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class komponen_nilai extends Simple_Controller {
    	 public function index(){
			redirect('kurikulum/komponen_nilai/datalist');
	    }

    	public $myconf = array(
			//Nama controller
			"name" 			=> "kurikulum/komponen_nilai",
			"component"		=> "komponen_nilai",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Komponen Nilai",
			"subtitle"		=> "",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_kurikulum"),
			"model_name"	=> "m_komponen_nilai",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"komponen_nilai"=>"Komponen Nilai",								
								"nilai_min"=>"Nilai Minimal",								
								"max"=>"Nilai Maksimal",								
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								"id_komponen_nilai"=>"hidden",
								"id_kurikulum"=>"hidden"
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"komponen_nilai",
								"nilai_min",
								"max"
							), 
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								"komponen_nilai"
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("komponen_nilai","nilai_min","max"),
								"field_sort"			=> array("komponen_nilai","nilai_min","max"),
								"field_filter"			=> array("komponen_nilai","nilai_min","max"),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(),
								//"field_comparator"		=> array("id_program"=>"=","id_kurikulum"=>"="),
								//"field_operator"		=> array("id_program"=>"AND","id_kurikulum"=>"AND"),
								//"field_align"			=> array(),
								"field_size"			=> array(),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array(
																"Konversi Nilai"=>"kurikulum/konversi_nilai/datalist"
															),
								"custom_add_link"		=> array('label'=>'Add','href'=>'kurikulum/komponen_nilai/add' ),
								"custom_edit_link"		=> array('label'=>'Edit','href'=>'kurikulum/komponen_nilai/edit'),
								//"custom_delete_link"	=> array(),
								//"custom_link"			=> array("program"=>"program/datalist","Siswa"=>"Siswa/datalist"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								// "view_file"				=> "kurikulum/program/detail",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("komponen_nilai","nilai_min","max","id_komponen_nilai","id_kurikulum"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								// "redirect_link"			=> "kurikulum/program"
							),
			//Konfigurasi tampilan edit
			/*"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								// "enable"				=> TRUE,
								// "model_name"			=> "",
								// "table"					=> array(),
								// "view_file"				=> "simple/edit",
								// "view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("program","id_program","id_kurikulum"),
								// "custom_action"			=> "",
								// "msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								// "msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> "kurikulum"
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);
		
			

		public function pre_process($data){
			//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
			//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
						
			$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			// $list_program = get_list("m_program","id_program","program");
			$data['conf']['data_list']['subtitle'] = "Data Komponen Nilai";
			$data['conf']['data_add']['subtitle'] = "Tambah";
			$data['conf']['data_edit']['subtitle'] = "Edit";
			
			$data['conf']['data_delete']['redirect_link'] = "kurikulum/komponen_nilai/datalist/".$this->uri->rsegment(3);
			$data['conf']['data_add']['redirect_link']    = "kurikulum/komponen_nilai/datalist/".$this->uri->rsegment(3);
			$data['conf']['data_edit']['redirect_link']   = "kurikulum/komponen_nilai/datalist/".$this->uri->rsegment(3);
			
			return $data;
		}
		
		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_kurikulum
			$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			// $_POST['filter']['id_program'] = $id_program;
			//$_POST['filter']['id_kurikulum'] = get_id_kurikulum();
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															//'id_program'=>$id_program
															//'id_kurikulum'=>get_id_kurikulum() 
														);
			
			//Mengganti link add yang ada di kanan atas form datalist
			$data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'kurikulum/komponen_nilai/add/'.$this->uri->rsegment(3));			
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'kurikulum/komponen_nilai/edit/');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'kurikulum/komponen_nilai/delete/');
			
			return $data;
		}
		
		public function before_datatable($data){
			//Method ini hanya akan dieksekusi di awal method datatable
			//Mengambil id_pihak yang diposting dari form datalist
			$filter = $this->input->post("filter");			
			// $id_program = $filter['id_program'];
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'kurikulum/komponen_nilai/edit/');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'kurikulum/komponen_nilai/delete/');
			
			return $data;
		}
		
				
		public function before_render_add($data){
			if(!$this->input->post()){
				$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
				//mengisi nilai untuk id_ di form input
				$_POST['item']['id_program'] = $id_program;
			}
			return $data;
		}
		
		public function before_add($data){
			if($this->input->post()){
				//menambahkan satu field id_kurikulum sebelum data disimpan ke db
				$data['item']['id_kurikulum'] = $this->uri->rsegment(3);
			}
			return $data;
		}
				
		public function before_edit($data){
			if(!$this->input->post()){
				$data['id'] = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 0;
			}
			return $data;
		}
		
		public function before_delete($data){
			$data['id'] = $this->uri->rsegment(3);
			return $data;
		}
}
