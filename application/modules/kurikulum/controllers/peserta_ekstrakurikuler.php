<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class Peserta_ekstrakurikuler extends Simple_Controller {

	public $myconf = array(
		//Nama controller
		"name" 			=> "kurikulum/peserta_ekstrakurikuler",
		"component"		=> "kurikulum",
		//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
		//dan subtitile general ini akan ter-replace
		"title"			=> "Kurikulum",
		// "subtitle"		=> "",
		//"view_file_index"=> "simple/index",
		//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
		//tidak ditentukan, maka akan digunakan model ini
		//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
		//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
		"model_name"	=> "m_peserta_ekstrakurikuler",
		//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
		//Format: nama_field=>"Labelnya"
		"data_label"	=> array(
							"nis"=>"NIS",
							"nama"=>"Nama",
							"rombel"=>"Kelas"
						),
		//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
		//Tipe default jika tidak didefinisikan: String, 
		//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
		//Format: nama_field => tipe_data_nya
		// "data_type"		=> array(
		// 					"id_kondisi"=>array("r_kondisi","id_kondisi","kondisi"),
		// 					"id_jenis_sarana_prasarana"=>"hidden",
		// 					"id_sekolah"=>"hidden"
		// 				),
		//Field-field yang harus diisi
		// "data_mandatory"=> array(
		// 					"kode_sarana_prasarana","nama_sarana_prasarana","penanggung_jawab","id_kondisi","daya_tampung"
		// 				),
		//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
		// "data_unique"	=> array(
		// 					"id_sarana_prasarana","kode_sarana_prasarana"
		// 				),
		//Konfigurasi tampilan datalist
		"data_list"		=> array(
							//"title"			=> "simple",
							//"subtitle"		=> "simple",
							//"model_name"			=> "",
							//"table"					=> array(),
							//"view_file"				=> "simple/datalist",
							//"view_file_datatable"	=> "simple/datatable",
							//"view_file_datatable_action"	=> "simple/datatable_action",
							"field_list"			=> array("nis","nama","rombel"),
							"field_sort"			=> array("nis","nama","rombel"),
							"field_filter"			=> array("s.nis","s.nama","s.rombel"),
							"field_filter_hidden"	=> array(),
							"field_filter_dropdown"	=> array("e.id_ekstra_kurikuler"=>array("label"=>"Ekstra Kurikuler","table"=>"m_ekstra_kurikuler", "table_id"=>"id_ekstra_kurikuler", "table_label"=>"nama_ekstra_kurikuler")),
							"field_comparator"		=> array("e.jenis"=>"=","s.id_sekolah"=>'='),
							"field_operator"		=> array("e.jenis"=>"AND","s.id_sekolah"=>'AND'),
							//"field_align"			=> array(),
							"field_size"			=> array('action_col' => "10%"),
							//"field_separated"		=> array(),
							//"field_sum"				=> array(),
							//"field_summary"			=> array(),
							//"enable_action"			=> TRUE,
							// "custom_action_link"	=> array("Siswa" => "pps/penentuan_kelas/siswa"),
							"custom_add_link"		=> array('label'=>'Tambah','href'=>'kurikulum/peserta_ekstrakurikuler/tambah_peserta' ),
							//"custom_edit_link"		=> array(),
							//"custom_delete_link"	=> array(),
							//"custom_link"			=> array("Kurikulum"=>"kurikulum/datalist","Siswa"=>"Siswa/datalist"),
						),
		//Konfigurasi tampilan add				
		// "data_add"		=> array(
							//"title"			=> "simple",
							//"subtitle"		=> "simple",
							//"enable"				=> TRUE,
							//"model_name"			=> "",
							//"table"					=> array(),
							//"view_file"				=> "simple/add",
							//"view_file_form_input"	=> "simple/form_input",
							// "field_list"			=> array("id_ekstra_kurikuler","rombel","id_siswa"),
							//"custom_action"			=> "",
							//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
							//"msg_on_success"		=> "Data berhasil disimpan.",
							//"redirect_link"			=> ""
						// ),
		//Konfigurasi tampilan edit
		// "data_edit"		=> array(
							//"title"			=> "simple",
							//"subtitle"		=> "simple",
							// "enable"				=> TRUE,
							// "model_name"			=> "",
							// "table"					=> array(),
							// "view_file"				=> "simple/edit",
							// "view_file_form_input"	=> "simple/form_input",
							// "field_list"			=> array(),
							// "custom_action"			=> "",
							// "msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
							// "msg_on_success"		=> "Data berhasil diperbaharui.",
							// "redirect_link"			=> ""
						// ),
		//Konfigurasi tampilan delete
		/*"data_delete"	=> array(
							"enable"				=> TRUE,
							"model_name"			=> "",
							"table"					=> array(),
							"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
							"msg_on_success"		=> "Data berhasil dihapus.",
							"redirect_link"			=> ""
						),
		//Konfigurasi tampilan export
		"data_export"	=> array(
							"enable_pdf"			=> TRUE,
							"enable_xls"			=> TRUE,
							"view_pdf"				=> "simple/laporan_datalist",
							"view_xls"				=> "simple/laporan_datalist",
							"field_size"			=> array(),
							"orientation"			=> "p",
							"special_number"		=> "",
							"enable_header"			=> TRUE,
							"enable_footer"			=> TRUE,
						)*/
	);

	function index()
	{
		redirect('kurikulum/peserta_ekstrakurikuler/datalist');
	}

	public function pre_process($data){
		//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
		//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
		
		
		// $id_sapras = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
		// $list_sarana_prasarana = get_list("r_jenis_sarana_prasarana","id_jenis_sarana_prasarana","jenis_sarana_prasarana");
		$data['conf']['data_list']['subtitle'] = 'Data Peserta Kegiatan Ekstrakurikuler';
		// $data['conf']['data_add']['subtitle'] = $list_sarana_prasarana[$id_sapras];
		// $data['conf']['data_edit']['subtitle'] = $list_sarana_prasarana[$id_sapras];
		
		// $data['conf']['data_delete']['redirect_link'] = "sarana_prasarana/datalist/$id_sapras";
		// $data['conf']['data_add']['redirect_link'] = "sarana_prasarana/datalist/$id_sapras";
		// $data['conf']['data_edit']['redirect_link'] = "sarana_prasarana/datalist/$id_sapras";
		
		return $data;
	}
	
	public function before_datalist($data){
		//Method ini hanya akan dieksekusi di awal method datalist
		//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
		// $id_sapras = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;

		// $_POST['filter']['id_jenis_sarana_prasarana'] = $id_sapras;
		$_POST['filter']['s.id_sekolah'] = get_id_sekolah();
		//$_POST['filter']['id_tahun_ajaran'] = get_id_tahun_ajaran();
		
		$data['conf']['data_list']['field_filter_dropdown']['e.id_ekstra_kurikuler']['filter'] = array('jenis'=>1, 'id_sekolah'=>get_id_sekolah());
		
		$gid = get_usergroup();
			if($gid == 9){
				$data['conf']['data_list']['enable_action']=false;
				$data['conf']['data_add']['enable']=false;
				$data['conf']['data_edit']['enable']=false;
				$data['conf']['data_delete']['enable']=false;
			}
			
		// //Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
		// //mengupdate datatable baik ketika search ataupun pindah page
		// $data['conf']['data_list']['field_filter_hidden'] = array(
		// 												'id_tahun_ajaran'=>2, 
		// 												'id_sekolah'=>get_id_sekolah() 
		// 											);
		
		// //Mengganti link add yang ada di kanan atas form datalist
		// $data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'sarana_prasarana/add/'.$id_sapras );			
		
		// //Memanipulasi parameter page
		// $page = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 1;
		// $data['page'] = $page;
		// //Mengkustomisasi link tombol edit dan delete
		// $data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'sarana_prasarana/edit/'.$id_sapras );
		// $data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'sarana_prasarana/delete/'.$id_sapras );
		
		return $data;
	}
	
	public function before_datatable($data){
		//Method ini hanya akan dieksekusi di awal method datatable
		//Mengambil id_pihak yang diposting dari form datalist
		// $filter = $this->input->post("filter");
		$_POST['filter']['s.id_sekolah'] = get_id_sekolah();
		//$_POST['filter']['id_tahun_ajaran'] = get_id_tahun_ajaran();
		
		// $id_sapras = $filter['id_jenis_sarana_prasarana'];
		// //Mengkustomisasi link tombol edit dan delete
		// $data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'sarana_prasarana/edit/'.$id_sapras );
		// $data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'sarana_prasarana/delete/'.$id_sapras );
		
		return $data;
	}
	
			
	public function before_render_add($data){
		// $data['conf']['data_type'] = array(
		// 	"id_tingkat_kelas" => array('list' => $this->m_peserta_ekstrakurikuler->get_tingkat_kelas(false, true)),
		// 	"id_kurikulum" => array('m_kurikulum', 'id_kurikulum', 'nama_kurikulum'),
		// 	"id_guru" => array('list' => $this->m_peserta_ekstrakurikuler->get_guru(false, true))
		// 	);
		// if(!$this->input->post()){
		// 	$id_sapras = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
		// 	//mengisi nilai untuk id_pihak_komunikasi di form input
		// 	$_POST['item']['id_jenis_sarana_prasarana'] = $id_sapras; //name='item[id_pihak_kom..]'
		// }
		return $data;
	}
	
	public function before_add($data){
		// if($this->input->post()){
		// 	//menambahkan satu field id_sekolah sebelum data disimpan ke db
		// 	$data['item']['id_sekolah'] = get_id_sekolah();
		// }
		if($this->input->post()){
			$data['item']['id_tahun_ajaran'] = get_id_tahun_ajaran();
		}

		return $data;
	}
			
	public function before_edit($data){
		// if(!$this->input->post()){
		// 	$data['id'] = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 0;
		// }
		return $data;
	}
	
	public function before_delete($data){
		// $data['id'] = $this->uri->rsegment(4);
		return $data;
	}

	function tambah_peserta($id_ekskul = false, $id_rombel = false)
	{
		$this->load->model('m_peserta_ekstrakurikuler');
		$data['ekskul'] = $this->m_peserta_ekstrakurikuler->get_ekskul(array('jenis'=>1, 'id_sekolah'=>get_id_sekolah()), true);
		$data['rombel'] = $this->m_peserta_ekstrakurikuler->get_rombel(true);
		$data['id_rombel'] = $id_rombel ? $id_rombel : false;
		$data['id_ekskul'] = $id_ekskul ? $id_ekskul : false;

		$data['siswa'] = array();
		if($id_rombel){
			if($siswa = $this->m_peserta_ekstrakurikuler->get_siswa_rombel($id_rombel, get_id_tahun_ajaran())){
				foreach ($siswa as $value) {
					$data['siswa'][$value['id_siswa']] = $value['nama'];
				}
			}
		}

		$data['siswa_ekskul'] = array();
		if($id_ekskul){
			if($siswa_ekskul = $this->m_peserta_ekstrakurikuler->get_siswa_ekskul_rombel($id_ekskul, $id_rombel)) {
				foreach ($siswa_ekskul as $value) {
					$data['siswa_ekskul'][$value['id_siswa']] = $value['nama'];
				}
			}
		}

		// jika form disubmit
		if($post = $this->input->post('siswa')){
			$this->m_peserta_ekstrakurikuler->update_peserta_ekskul($id_ekskul, $post);
			set_success_message('Data Berhasil Ditambah!');
			redirect('kurikulum/peserta_ekstrakurikuler/datalist');
		}
		
		$data['component'] = 'kurikulum';
		render('ekskul_peserta/form_peserta_ekstrakurikuler', $data);
	}

}
