<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class silabus_tematik extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_silabus_tematik');
			$this->load->helper('download');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kurikulum/silabus_tematik/home');
		}
		
		public function home(){
		
			$data['tematik'] = $this->m_silabus_tematik->get_all_tematik();
			$data['tingkat'] = $this->m_silabus_tematik->get_all_tingkat(get_jenjang_sekolah());
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/silabus_tematik/home";
			$config["total_rows"] = $this->m_silabus_tematik->count_datalist();
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->m_silabus_tematik->get_datalist($config["per_page"], $page);
			
			$data['first_tematik'] = 0;
			$data['sting'] = 0;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['stat'] = 'home';
			$data['jumlah'] = $this->m_silabus_tematik->count_datalist();
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			render("silabus_tematik/silabus_tematik", $data);
		}
		
		public function search(){
		
			$id_tematik = $this->input->post('tematik');
			$id_tingkat = $this->input->post('tingkat');
			
			$data['tematik'] = $this->m_silabus_tematik->get_all_tematik();
			$data['tingkat'] = $this->m_silabus_tematik->get_all_tingkat(get_jenjang_sekolah());
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/silabus_tematik/home";
			$config["total_rows"] = $this->m_silabus_tematik->count_datalist();
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->m_silabus_tematik->get_datalist($config["per_page"], $page);
			$data['first_tematik'] = $id_tematik;
			$data['first_tingkat'] = $id_tingkat;
			$data['sting'] = $id_tingkat;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['stat'] = 'search';
			$data['jumlah'] = $this->m_silabus_tematik->count_datalist();
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			render("silabus_tematik/silabus_tematik", $data);
		}
		
		public function add(){
			$data['tematik'] = $this->m_silabus_tematik->get_all_tematik();
			
			$data['component'] = 'kurikulum';
			render("silabus_tematik/form_tambah", $data);
		}
		
		public function submit(){
			$this->form_validation->set_rules('nama_silabus_tematik', 'Nama Silabus', 'required');
			$this->form_validation->set_rules('id_pelajaran_tematik', 'Pelajaran Tematik', 'required');
			
			if($this->form_validation->run() == TRUE){
				$nama_silabus_tematik = $this->input->post('nama_silabus_tematik');
				$id_pelajaran_tematik = $this->input->post('id_pelajaran_tematik');
				$id_sekolah = get_id_sekolah();
				
				$config['upload_path'] = './extras/silabus_tematik/';
				$config['allowed_types'] = 'pdf|doc|docx|xls|txt';
				$config['max_size']	= '10000';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$error = array('error' => $this->upload->display_errors());
					set_error_message($error);
					redirect("kurikulum/silabus_tematik/home");
				}
				else
				{
					$lokasi_file = $this->upload->data();
				}
				
				
				$action = $this->input->post('action');
				if($action == 'add'){
					$this->m_silabus_tematik->insert($nama_silabus_tematik, $id_pelajaran_tematik, $id_sekolah, base_url("extras/silabus_tematik/".$lokasi_file['file_name']));
					set_success_message('Data Berhasil Ditambah!');
					redirect("kurikulum/silabus_tematik/home/");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian!');
				redirect("kurikulum/silabus_tematik/home");
			}
		}
		
		public function delete(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/silabus_tematik/home');
			}
			
			$this->m_silabus_tematik->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('kurikulum/silabus_tematik/home/');
		}
		
		public function download(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/silabus_tematik/home');
			}
			$row = $this->m_silabus_tematik->get_silabus_tematik_by($id);
			$file = $row->lokasi_file;
			$data = file_get_contents($file);
			$filename = basename($file);;
			
			force_download($filename, $data);
		}
		
}
