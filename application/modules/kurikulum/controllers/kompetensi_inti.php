<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class kompetensi_inti extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_kompetensi_inti');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kurikulum/kompetensi_inti/home');
		}
		
		public function home(){
			
			$cek = $this->uri->segment(4);
						
			$data['kompetensi_inti'] = $this->m_kompetensi_inti->get_all_kompetensi();
			$data['pelajaran'] = $this->m_kompetensi_inti->get_all_pelajaran();
			$data['datalist'] = null;
			$data['tingkat_kelas'] = $this->m_kompetensi_inti->get_all_tingkat(get_jenjang_sekolah());
			$data['id_tingkat_kelas'] = 0;
			$data['id_pelajaran'] = 0;
			$data['cek'] = $cek;
			$data['kur'] = $this->m_kompetensi_inti->get_all_kurikulum();
			$data['count'] = $this->m_kompetensi_inti->count_tingkat(get_jenjang_sekolah());
			
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			render("kompetensi_inti/kompetensi_inti", $data);
		}
		
		public function tambah(){
			$data['tingkat_kelas'] = $this->m_kompetensi_inti->get_all_tingkat(get_jenjang_sekolah());
			$data['kurikulum'] = $this->m_kompetensi_inti->get_all_kurikulum();
			
			$data['component'] = 'kurikulum';
			render("kompetensi_inti/form_tambah", $data);
		}
		
		
		public function edit(){
			$id = $this->uri->segment(4);
			if ($id == NULL) {
				redirect('kurikulum/kompetensi_inti/home');
			}
			
			$row = $this->m_kompetensi_inti->get_kompetensi_inti_by($id);
			$data['id_kompetensi_inti'] = $id;
			$data['kode_kompetensi_inti'] = $row->kode_kompetensi_inti;
			$data['kompetensi_inti'] = $row->kompetensi_inti;
			$data['id_kurikulum'] = $row->id_kurikulum;
			$data['kurikulum'] = $this->m_kompetensi_inti->get_all_kurikulum();
			$data['id_tingkat_kelas'] = $row->id_tingkat_kelas;
			$data['tingkat_kelas'] = $this->m_kompetensi_inti->get_all_tingkat(get_jenjang_sekolah());
			
			$data['component'] = 'kurikulum';
			render("kompetensi_inti/form_edit", $data);
		}
		
		public function delete($id){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/kompetensi_inti/home');
			}
			
			$this->m_kompetensi_inti->delete($id);
			set_success_message('Data Berhasil Dihapus');
			redirect('kurikulum/kompetensi_inti/home');
		}
		
		public function submit_post(){
			$this->form_validation->set_rules('kode_kompetensi_inti', 'Kode Kompetensi Inti', 'required');
			$this->form_validation->set_rules('kompetensi_inti', 'Kompetensi Inti', 'required');
			$this->form_validation->set_rules('id_kurikulum', 'Id Kurikulum', 'required');
			$this->form_validation->set_rules('id_tingkat_kelas', 'Id Tingkat Kelas', 'required');
			
			if($this->form_validation->run() == TRUE){
				$kode_kompetensi_inti = $this->input->post('kode_kompetensi_inti');
				$kompetensi_inti = $this->input->post('kompetensi_inti');
				$id_kurikulum = $this->input->post('id_kurikulum');
				$id_tingkat_kelas = $this->input->post('id_tingkat_kelas');
				$id_kompetensi_inti = $this->input->post('id_kompetensi_inti');
				
				$action = $this->input->post('action');
				if($action == 'update'){
					$this->m_kompetensi_inti->update($id_kompetensi_inti, $kode_kompetensi_inti, $kompetensi_inti, $id_kurikulum, $id_tingkat_kelas);
					set_success_message('Data Berhasil Diupdate');
					redirect("kurikulum/kompetensi_inti/home/");
				}elseif($action == 'add'){
					$data['kode_kompetensi_inti'] = $kode_kompetensi_inti;
					$data['kompetensi_inti'] = $kompetensi_inti;
					$data['id_kurikulum'] = $id_kurikulum;
					$data['id_tingkat_kelas'] = $id_tingkat_kelas;
					
					$this->m_kompetensi_inti->save($data);
					set_success_message('Data Berhasil Disimpan');
					redirect("kurikulum/kompetensi_inti/home/");
				}
			}
			else{
				set_error_message('Terjadi kesalahan pengisian, silahkan tambah data lagi.');
				redirect("kurikulum/kompetensi_inti/home/");
			}
		}
		
		// ----------- controller untuk Standar Kompetensi ----------- //
		
		public function search_sk(){
			
			$id = $this->uri->segment(4);
			
			if($id==0){
				$id_tingkat_kelas = $this->input->post('id_tingkat_kelas');
				$id_pelajaran = $this->input->post('id_pelajaran'.$id_tingkat_kelas);
			}else{
				$id_tingkat_kelas = $this->uri->segment(4);
				$id_pelajaran = $this->uri->segment(5);
			}
			if($id_pelajaran == -1 && $id_tingkat_kelas == -1){
				$data['datalist'] = $this->m_kompetensi_inti->get_all_data_sk();
			}else{
				$data['datalist'] = $this->m_kompetensi_inti->get_data_sk($id_tingkat_kelas, $id_pelajaran);
			}
						
			$data['kompetensi_inti'] = $this->m_kompetensi_inti->get_all_kompetensi();
			$data['pelajaran'] = $this->m_kompetensi_inti->get_all_pelajaran();
			$data['tingkat_kelas'] = $this->m_kompetensi_inti->get_all_tingkat(get_jenjang_sekolah());
			$data['id_tingkat_kelas'] = $id_tingkat_kelas;
			$data['id_pelajaran'] = $id_pelajaran;
			$data['cek'] = 1;
			$data['kur'] = $this->m_kompetensi_inti->get_all_kurikulum();
			$data['count'] = $this->m_kompetensi_inti->count_tingkat(get_jenjang_sekolah());
			
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			// print_r($data['datalist']);
			// echo $id_tingkat_kelas;
			// echo $id_pelajaran;
			// print_r($_POST);
			render("kompetensi_inti/kompetensi_inti", $data);
		}
		
		public function delete_sk($id){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/kompetensi_inti/home/1');
			}
			
			$this->m_kompetensi_inti->delete_sk($id);
			set_success_message('Data Berhasil Dihapus');
			redirect('kurikulum/kompetensi_inti/home/1');
		}
		
		public function tambah_sk(){
			$data['pelajaran'] = $this->m_kompetensi_inti->get_all_pelajaran();
			$data['action'] = "add";
			$data['standar_kompetensi'] = "";
			$data['id_standar_kompetensi'] = 0;
			$data['id_pelajaran'] = 0;
			
			$data['component'] = 'kurikulum';
			render("kompetensi_inti/form_tambah_sk", $data);
		}
		
		public function edit_sk(){
			$id = $this->uri->segment(4);
			if ($id == NULL) {
				redirect('kurikulum/kompetensi_inti/home/1');
			}
			
			$data['pelajaran'] = $this->m_kompetensi_inti->get_all_pelajaran();
			$data['action'] = "update";
			
			$row = $this->m_kompetensi_inti->get_standar_kompetensi_by($id);
			
			$data['id_standar_kompetensi'] = $id;
			$data['standar_kompetensi'] = $row->standar_kompetensi;
			$data['id_pelajaran'] = $row->id_pelajaran;
		
			$data['component'] = 'kurikulum';
			render("kompetensi_inti/form_tambah_sk", $data);
		}
		
		public function submit_sk(){
			$id_tingkat_kelas = $this->input->post('id_tingkat_kelas');
			
			$this->form_validation->set_rules('standar_kompetensi', 'Standar Kompetensi', 'required');
			$this->form_validation->set_rules('id_pelajaran'.$id_tingkat_kelas, 'Pelajaran', 'required');

			if($this->form_validation->run() == TRUE){
				$id_standar_kompetensi = $this->input->post('id_standar_kompetensi');
				$standar_kompetensi = $this->input->post('standar_kompetensi');
				$id_pelajaran = $this->input->post('id_pelajaran'.$id_tingkat_kelas);
				
				$action = $this->input->post('action');
				
				if($action == 'update'){
					$this->m_kompetensi_inti->update_sk($id_standar_kompetensi, $standar_kompetensi, $id_pelajaran);
					set_success_message('Data Berhasil Diupdate');
					redirect("kurikulum/kompetensi_inti/home/1");
				}elseif($action == 'add'){
					$this->m_kompetensi_inti->add_sk($standar_kompetensi, $id_pelajaran);
					set_success_message('Data Berhasil ditambah');
					redirect("kurikulum/kompetensi_inti/home/1");
				}
			}
			else{
				set_error_message('Terjadi kesalahan pengisian, silahkan tambah data lagi.');
				redirect("kurikulum/kompetensi_inti/home/1");
			}
		}
		
		// ----------- controller untuk Kompetensi Dasar ----------- //
		public function detail_sk(){
			$id = $this->uri->segment(4);
			if ($id == NULL) {
				redirect('kurikulum/kompetensi_inti/home');
			}
			
			$row = $this->m_kompetensi_inti->get_standar_kompetensi_by($id);
			
			$data['standar_kompetensi'] = $row->standar_kompetensi;
			$id_pelajaran = $row->id_pelajaran;
		
			$row = $this->m_kompetensi_inti->get_pelajaran_by($id_pelajaran);
			$data['tingkat_kelas'] = $row->tingkat_kelas;
			$data['kode_pelajaran'] = $row->kode_pelajaran;
			$data['pelajaran'] = $row->pelajaran;
			$data['id_pelajaran'] = $row->id_pelajaran;
			$data['id_tingkat_kelas'] = $row->id_tingkat_kelas;
			
			$data['data'] = $this->m_kompetensi_inti->get_kompetensi_dasar_by($id);
			$data['id'] = $id;
			
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			render("kompetensi_inti/kompetensi_dasar", $data);
		}
		
		public function tambah_kd(){
			$id = $this->uri->segment(4);
			$data['standar_kompetensi'] = $this->m_kompetensi_inti->get_all_standar_kompetensi();
			$data['action'] = "add";
			$data['kompetensi_dasar'] = "";
			$data['id'] = $id;
			$data['id_kompetensi_dasar'] = 0;
			$data['id_standar_kompetensi'] = 0;
			
			$data['component'] = 'kurikulum';
			render("kompetensi_inti/form_tambah_kd", $data);
		}
		
		public function edit_kd(){
			$id_sk = $this->uri->segment(4);
			$id = $this->uri->segment(5);
			if ($id == NULL) {
				redirect('kurikulum/kompetensi_inti/home');
			}
			
			$data['standar_kompetensi'] = $this->m_kompetensi_inti->get_all_standar_kompetensi();
			$data['action'] = "update";
			
			$row = $this->m_kompetensi_inti->get_kd_by($id);
			
			$data['id_kompetensi_dasar'] = $id;
			$data['id'] = $id_sk;
			$data['kompetensi_dasar'] = $row->kompetensi_dasar;
			$data['id_standar_kompetensi'] = $row->id_standar_kompetensi;
			
			$data['component'] = 'kurikulum';
			render("kompetensi_inti/form_tambah_kd", $data);
		}
		
		public function delete_kd($id){
			$id_sk = $this->uri->segment(4);
			$id = $this->uri->segment(5);
			if ($id == NULL){
				redirect('kurikulum/kompetensi_inti/home');
			}
			
			$this->m_kompetensi_inti->delete_kd($id);
			set_success_message('Data Berhasil Dihapus');
			redirect('kurikulum/kompetensi_inti/detail_sk/'.$id_sk);
		}
		
		public function submit_kd(){
			$this->form_validation->set_rules('kompetensi_dasar', 'Kompetensi Dasar', 'required');
			$this->form_validation->set_rules('id_standar_kompetensi', 'Standar Kompetensi', 'required');
			
			if($this->form_validation->run() == TRUE){

				$id = $this->input->post('id');
				$id_standar_kompetensi = $this->input->post('id_standar_kompetensi');
				$kompetensi_dasar = $this->input->post('kompetensi_dasar');
				$id_kompetensi_dasar = $this->input->post('id_kompetensi_dasar');
				
				$action = $this->input->post('action');
				
				if($action == 'update'){
					$this->m_kompetensi_inti->update_kd($id_kompetensi_dasar, $kompetensi_dasar, $id_standar_kompetensi);
					set_success_message('Data Berhasil Diupdate');
					redirect("kurikulum/kompetensi_inti/detail_sk/".$id);
				}elseif($action == 'add'){
					$this->m_kompetensi_inti->add_kd($kompetensi_dasar, $id_standar_kompetensi);
					set_success_message('Data Berhasil ditambah');
					redirect("kurikulum/kompetensi_inti/detail_sk/".$id);
				}
			}
			else{
				set_error_message('Terjadi kesalahan pengisian, silahkan tambah data lagi.');
				redirect("kurikulum/kompetensi_inti/detail_sk/".$id);
			}
		}
}
