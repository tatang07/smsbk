<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class presensi_guru extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_presensi_guru');
			$this->load->library('form_validation');
			$this->load->library('pagination');
		}
		
		public function index(){
			redirect('kurikulum/presensi_guru/home');
		}
		
		public function home(){
			
			$data['gid'] = $gid = get_usergroup();
			
			$data['guru'] = $this->m_presensi_guru->get_all_guru();
			$first = $this->m_presensi_guru->get_first_guru();
			if($first){
			$id_guru = $first->id_guru;
			}else{
				$id_guru = 0;
			}
			
			
			if(get_id_personal()){
				$id_guru = get_id_personal();
			}
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/presensi_guru/home";
			$config["total_rows"] = $this->m_presensi_guru->count_datalist($id_guru);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->m_presensi_guru->get_datalist($id_guru, $config["per_page"], $page);
			$data['first'] = $id_guru;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['status'] = 'home';
			$data['jumlah'] = $this->m_presensi_guru->count_datalist($id_guru);
			
			$data['component'] = 'kurikulum';
			render("presensi_guru/presensi_guru", $data);
		}
		
		public function search(){
			
			$id = $this->input->post('guru');
			
			$row = $this->m_presensi_guru->get_guru_by($id);
			if($row){
			$id_guru = $row->id_guru;
			}else{
				$id_guru = 0;
			}
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/presensi_guru/home";
			$config["total_rows"] = $this->m_presensi_guru->count_datalist($id_guru);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			
			$data['guru'] = $this->m_presensi_guru->get_all_guru();
			$data['list'] = $this->m_presensi_guru->get_datalist($id_guru, $config["per_page"], $page);
			$data['first'] = $id_guru;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['jumlah'] = $this->m_presensi_guru->count_datalist($id_guru);
			$data['status'] = 'search';
			
			$data['component'] = 'kurikulum';
			render("presensi_guru/presensi_guru", $data);
		}
		
}
