<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class agenda_kelas extends Simple_Controller {
	
	public function __construct(){
		parent::__construct();
		// untuk load model
		$this->load->model('t_agenda_kelas');
		$this->load->library('form_validation');
		$this->load->helper('url');
		$this->load->helper('date');
	}
	
	//menampilkan tampilan agenda_kelas/
	public function index()
	{
		$data['pelajaran'] = $this->t_agenda_kelas->get_all_pelajaran();
		$data['rombel'] = $this->t_agenda_kelas->get_all_rombel();
		
		//panel1 = harian
		$data['agenda_harian'] = $this->t_agenda_kelas->get_agenda_kelas_harian();
		//panel2 = pelajaran
		$data['agenda_kelas'] = $this->t_agenda_kelas->get_all_agenda_kelas();
		$data['aksi']="home";
		
		$id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
		$id_gmr = isset($_POST['id_gmr'.$id_rombel])?$_POST['id_gmr'.$id_rombel]:0;
		$data["id_rombel"] = $id_rombel;
		$data["id_gmr"] = $id_gmr;
		$data['gid'] = $gid = get_usergroup();
		
		$data['component'] = 'kurikulum';
		
		render("agenda_kelas/agenda_kelas", $data);
	}

	//menampilkan tampilan agenda_kelas/
	public function search()
	{
		$data['pelajaran'] = $this->t_agenda_kelas->get_all_pelajaran();
		$data['rombel'] = $this->t_agenda_kelas->get_all_rombel();
		
		$parameter = $_POST;

		
		$id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
		$id_gmr = isset($_POST['id_gmr'.$id_rombel])?$_POST['id_gmr'.$id_rombel]:0;
		$data["id_rombel"] = $id_rombel;
		$data["id_gmr"] = $id_gmr;


		//panel1 = harian
		if(isset($_POST['cariharian'])){//form normal
			$data['first_rombel'] = $parameter['id_rombel'];
			if($_POST['tanggal'] == ''){
				$parameter['tanggal'] ='';
				$data['agenda_harian'] = $this->t_agenda_kelas->get_search_harian($parameter);
			}else{
				$parameter['tanggal'] = DateTime::createFromFormat('d/m/Y', $_POST['tanggal'])->format('Y-m-d');
				$data['agenda_harian'] = $this->t_agenda_kelas->get_search_harian($parameter);
			}
			//$data['km_rombel'] = $this->t_agenda_kelas->get_km_rombel($id_rombel);
		}else{
			$data['agenda_harian'] = '';
		}

		//panel2 = pelajaran
		if(isset($_POST['caripelajaran'])){//form normal
			$data['first_rombel'] = $id_rombel;
			$data['first_pelajaran'] = $id_gmr;
			$param['id_rombel']=$id_rombel;
			if($id_gmr == 0){
				$data['agenda_kelas'] = $this->t_agenda_kelas->get_search_pelajaran_all($param);
			}else{
				$param['id_gmr']=$id_gmr;
				$data['agenda_kelas'] = $this->t_agenda_kelas->get_search_pelajaran($param);
			}

			//$data['km_rombel'] = $this->t_agenda_kelas->get_km_rombel($id_rombel);
		}else{
			$data['agenda_kelas'] = '';
		}

		//panel2 = pelajaran
		if(isset($_POST['cariKM'])){//form normal
			$param['id_rombel']=$id_rombel;
			$data['siswa'] = $this->t_agenda_kelas->get_siswa_by_rombel($id_rombel);
		}

		if(isset($_POST['gantiKM'])){
			$param['id_rombel']=$id_rombel;
			$data['siswa'] = $this->t_agenda_kelas->get_siswa_by_rombel($id_rombel);
		}

		if(isset($_POST['pilihKM'])){
			$param['id_rombel']=$id_rombel;
			$temp['id_siswa'] = $_POST['id_km_kelas'];
			$temp['id_rombel'] = $id_rombel;

			$this->t_agenda_kelas->set_km_kelas($temp,$id_rombel);
		}
		$data['aksi']="search";
		$data['gid'] = $gid = get_usergroup();


		if($this->t_agenda_kelas->get_km_kelas($id_rombel)){
			$data['km'] = $this->t_agenda_kelas->get_km_kelas($id_rombel);
		}else{
			$data['km'] = "Belum ada KM";
		}

		$data['component'] = 'kurikulum';
		
		render("agenda_kelas/agenda_kelas", $data);
	}

	//menampilkan form untuk menambah, form add, get
	public function add($id_rombel = false, $id_gmr = false, $id_agenda_kelas = false)
	{
		// $id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
		// $id_gmr = isset($_POST['id_gmr'.$id_rombel])?$_POST['id_gmr'.$id_rombel]:0;
		// if(! $id_agenda_kelas) show_404();
		
		$data["id_rombel"] = $id_rombel;
		$data["id_agenda_kelas"] = $id_agenda_kelas;
		$data["id_gmr"] = $id_gmr;
		
		$data['siswa'] = array();
		if($id_rombel){
			$this->load->model('pps/m_penentuan_kelas');
			$data['siswa'] = $this->m_penentuan_kelas->get_siswa_rombel($id_rombel, get_id_tahun_ajaran());
		}
		
		$data['siswa_absen'] = array();
		$data['tanggal'] = "";
		$data['materi'] = "";
		$data['proses_belajar'] = "";
			
		if($id_agenda_kelas){
			$data['siswa_absen'] = $this->t_agenda_kelas->get_siswa_absen($id_agenda_kelas);
			$row = $this->t_agenda_kelas->get_agenda_kelas_by($id_agenda_kelas);
			$data['tanggal'] = $row->tanggal;
			$data['materi'] = $row->materi;
			$data['proses_belajar'] = $row->proses_belajar;
		}
			
		$data['pelajaran'] = $this->t_agenda_kelas->get_pelajaran_rombel($id_rombel);
		// print_r($data['gmr']);
		$data['pelajaran_dropdown'] = array('- pilih -');
		if($data['pelajaran']){
			foreach($data['pelajaran'] as $pel){
				$data['pelajaran_dropdown'][$pel->id_guru_matpel_rombel] = $pel->kode_pelajaran.' - '.$pel->pelajaran;
			}
		}
		
		$data['jenis_absen'] = $this->t_agenda_kelas->get_jenis_absen();
		$data['count_jenis_absen'] = $this->t_agenda_kelas->count_jenis_absen();
		$data['rombel'] = $this->t_agenda_kelas->get_all_rombel();
		$data['guru'] = $this->t_agenda_kelas->get_all_guru();
		$data['term'] = $this->t_agenda_kelas->get_all_term();
		$data['agenda'] = $this->t_agenda_kelas->get_all_guru_matpel_rombel();
		$data['agenda_kelas'] = $this->t_agenda_kelas->get_all_agenda_kelas();
		
		// print_r($data['siswa_absen']);
		//view form here
		
		// jika form disubmit
		if($post = $this->input->post()){
			date_default_timezone_set('Asia/Jakarta');
			$data_agenda = array(
				'tanggal' => tanggal_db($this->input->post('tanggal')),
				'materi' => $this->input->post('materi'),
				'proses_belajar' => $this->input->post('proses_belajar'),
				'id_guru_matpel_rombel' => $this->input->post('id_gmr'),
				'id_term' => get_id_term(),
				'waktu_rekap' => date('H:I:s', now())
			);
			
			$data_absen = $this->input->post('absen');
			
			if($id_agenda_kelas==false){
				$id_agenda_kelas = $this->t_agenda_kelas->set_agenda_kelas($data_agenda);
				set_success_message('Data Berhasil Ditambah!');
				
			}else{
				$data_agenda = array(
					'tanggal' => tanggal_db($this->input->post('tanggal')),
					'materi' => $this->input->post('materi'),
					'proses_belajar' => $this->input->post('proses_belajar'),
					'id_guru_matpel_rombel' => $this->input->post('id_gmr'),
					'id_term' => get_id_term(),
					'id_agenda_kelas' => $id_agenda_kelas
				);
				$this->t_agenda_kelas->update_agenda_kelas($data_agenda);
				$this->t_agenda_kelas->delete_absen($id_agenda_kelas);
			
				set_success_message('Data Berhasil Diupdate!');
			}
			
			foreach($data_absen as $id_s=>$id_j){
				$data = array(
					'id_jenis_absen' => $id_j,
					'id_agenda_kelas' => $id_agenda_kelas,
					'id_siswa' => $id_s
				);
				$this->t_agenda_kelas->set_absen($data);
			}
			// $id_agenda_kelas = 
			// $this->t_agenda_kelas->update_rombel_siswa($id_rombel, $id_rombel_asal, $post);
			redirect('kurikulum/agenda_kelas/add/'.$id_rombel.'/'.$id_gmr.'/'.$id_agenda_kelas);
		}
		
		$data['component'] = 'kurikulum';
		render("agenda_kelas/form_tambah", $data);
	}

	//menyimpan data dari form, post
	public function submit()
	{
		if(isset($_POST['simpan'])){
			
			//tambah guru matpel rombel dulu
			$guru_matpel_rombel['id_guru'] = $_POST['guru'];
			$guru_matpel_rombel['id_rombel'] = $_POST['rombel'];
			$guru_matpel_rombel['id_pelajaran'] = $_POST['pelajaran'];
			$this->t_agenda_kelas->set_guru_matpel_rombel($guru_matpel_rombel);
			//get last added
			$insert_id = $this->db->insert_id();

			//tambah agenda kelas


			//versi semuanya
			// $agenda_kelas['id_term'] = $_POST['id_term'];
			$agenda_kelas['materi'] = $_POST['materi'];
			$agenda_kelas['proses_belajar'] = $_POST['proses_belajar'];
			$agenda_kelas['tanggal'] = DateTime::createFromFormat('d/m/Y', $_POST['tanggal'])->format('Y-m-d');
			$agenda_kelas['id_guru_matpel_rombel'] = $insert_id;

			$this->t_agenda_kelas->set_agenda_kelas($agenda_kelas);

			redirect('kurikulum/agenda_kelas', 'refresh');
		}else{
			redirect('kurikulum/agenda_kelas/add', 'refresh');
		}
	}

	//menampilkan form edit, get
	public function edit($id = NULL)
	{	
		if(isset($id)){
			//selecting by_id
			$data['agenda_kelas'] = $this->t_agenda_kelas->get_agenda_kelas_by_id($id);
			$data['term'] = $this->t_agenda_kelas->get_all_term();
			$data['agenda'] = $this->t_agenda_kelas->get_all_guru_matpel_rombel();

			$data['pelajaran'] = $this->t_agenda_kelas->get_all_pelajaran();
			$data['rombel'] = $this->t_agenda_kelas->get_all_rombel();
			$data['guru'] = $this->t_agenda_kelas->get_all_guru();
			
			$data['guru_matpel_rombel'] = $this->t_agenda_kelas->get_all_agenda_kelas_rombel($id);

			//view form here
			$data['component'] = 'kurikulum';
			render("agenda_kelas/form_edit", $data);
		}else{
			redirect('kurikulum/agenda_kelas', 'refresh');
		}
	}

	//mengupdate data di database, post
	public function update($id = NULL)
	{
		if(isset($_POST['simpan'])){	
			$data = $_POST;
			$data['id_agenda_kelas'] = $id;
			$data['tanggal'] = DateTime::createFromFormat('d/m/Y', $data['tanggal'])->format('Y-m-d');
			unset($data['simpan']);
			unset($data['id_guru']);
			unset($data['id_pelajaran']);
			unset($data['id_rombel']);
			$this->t_agenda_kelas->update_agenda_kelas($data);

			unset($data);

			$data_query['agenda_kelas'] = $this->t_agenda_kelas->get_agenda_kelas_by_id($id);

			$data['id_guru_matpel_rombel'] = $data_query['agenda_kelas']->id_guru_matpel_rombel;
			$data['id_guru'] = $_POST['id_guru'];
			$data['id_pelajaran'] = $_POST['id_pelajaran'];
			$data['id_rombel'] = $_POST['id_rombel'];
			$this->t_agenda_kelas->update_guru_matpel_rombel($data);

			//redirect here
	        redirect('kurikulum/agenda_kelas', 'refresh');
		}else{
			redirect('kurikulum/agenda_kelas/edit/'.$id, 'refresh');
		}
	}

	//menghapus data, get
	public function delete($id)
	{
        $this->t_agenda_kelas->delete_agenda_kelas($id);
        // redirect here
		set_success_message('Data Berhasil Dihapus!');
        redirect('kurikulum/agenda_kelas', 'refresh');
	}
}
