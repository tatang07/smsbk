<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class struktur_kurikulum extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_struktur_kurikulum');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kurikulum/struktur_kurikulum/home');
		}
		
		public function home(){

			$id_kur = isset($_POST['id_kurikulum'])?$_POST['id_kurikulum']:get_id_kurikulum();
			
			$data['kompetensi_inti'] = $this->m_struktur_kurikulum->get_all_kompetensi();
			$data['tingkat_kelas'] = $this->m_struktur_kurikulum->get_all_tingkat(get_jenjang_sekolah());
			$data['jenjang_sekolah'] = get_jenjang_sekolah();
			$data['kur'] = $this->m_struktur_kurikulum->get_all_kurikulum();
			$data['count'] = $this->m_struktur_kurikulum->count_tingkat(get_jenjang_sekolah());
			$data['pelajaran'] = $this->m_struktur_kurikulum->get_all_pelajaran($id_kur);
			$data['pelajaran_group'] = $this->m_struktur_kurikulum->get_all_pelajaran_group_by($id_kur);
			$data['kelompok'] = $this->m_struktur_kurikulum->get_all_kelompok_pelajaran($id_kur);
			$data['last'] = $this->m_struktur_kurikulum->get_last_kelompok_pelajaran();
			$data['peminatan'] = $this->m_struktur_kurikulum->get_all_peminatan();
			$data['beban'] = $this->m_struktur_kurikulum->get_all_beban_belajar();
			
			$data['kelasc'] = $this->m_struktur_kurikulum->get_all_pelajaran_by_peminatan($id_kur);
			
			$p = $this->m_struktur_kurikulum->get_all_pelajaran_group_by($id_kur);
			$pelajaran = array();
			
			foreach($data['kelompok'] as $k){
				$pelajaran[$k->id_kelompok_pelajaran] = array();
				foreach($p as $pel){
					if($k->id_kelompok_pelajaran == $pel->id_kelompok_pelajaran){
						$pelajaran[$k->id_kelompok_pelajaran][$pel->pelajaran] = array();
						foreach($data['tingkat_kelas'] as $t){
						$pelajaran[$k->id_kelompok_pelajaran][$pel->pelajaran] += array($t->tingkat_kelas => 0);
						}
					}
				}
			}
			
			$p = $this->m_struktur_kurikulum->get_all_pelajaran_group_by($id_kur);
			foreach($data['kelompok'] as $k){
				foreach($p as $pel){
					if($k->id_kelompok_pelajaran == $pel->id_kelompok_pelajaran){
						foreach($data['tingkat_kelas'] as $t){
							foreach($data['pelajaran'] as $pl){
								if($pl->id_tingkat_kelas == $t->id_tingkat_kelas && $pl->id_kelompok_pelajaran == $k->id_kelompok_pelajaran && $pel->pelajaran == $pl->pelajaran){
									$pelajaran[$k->id_kelompok_pelajaran][$pel->pelajaran][$t->tingkat_kelas] = $pl->jumlah_jam;
								}
							}
						}
					}
				}
			}
			
			// foreach($data['tingkat_kelas'] as $t){
				// foreach($data['pelajaran'] as $pel){
					// if(isset($pelajaran[$pel->pelajaran])){
						// $pelajaran[$pel->pelajaran] += array($t->tingkat_kelas => 0);
					// }else{
						// $pelajaran[$pel->pelajaran] = array($t->tingkat_kelas => 0);
					// }
				// }
			// }
			// foreach($data['tingkat_kelas'] as $t){
				// foreach($data['pelajaran'] as $pel){
					// if($pel->id_tingkat_kelas == $t->id_tingkat_kelas){
						// $pelajaran[$pel->pelajaran][$t->tingkat_kelas] = $pel->jumlah_jam;
					// }
				// }
			// }
			$data['alokasi_pelajaran'] = $pelajaran;
			
			$kelasc = array();
			foreach($data['peminatan'] as $peminatan){
				$kelasc[$peminatan->peminatan] = array();
				$kelasc[$peminatan->peminatan]['kelompok'] = $peminatan->id_kelompok_pelajaran;
				$kelasc[$peminatan->peminatan]['pel'] = array();
			}
			
			if($data['kelasc']){
				foreach($data['kelasc'] as $c){
						// $alokasi = $this->m_struktur_kurikulum->get_pelajaran_by($c->id_pelajaran);
					$peminatan = (array) $c;
					$peminatan += array('alokasi' => '');
					$kelasc[$c->peminatan]['pel'][] = $peminatan;
				}
			}
			$data['mapel_pilihan'] = $kelasc;
			// $this->mydebug($peminatan);
			// $this->mydebug($kelasc);
			// $this->mydebug($data['alokasi_pelajaran']);
			// $this->mydebug($data['mapel_pilihan']);
			$data['component'] = 'kurikulum';
			// foreach($data['mapel_pilihan'] as $pem => $value_minat){
				// $this->mydebug($value_minat['pel']);
				// echo $value_minat['kelompok'];
			// }
			
			$data['gid'] = $gid = get_usergroup();
			
			// foreach($data['alokasi_pelajaran'] as $p => $pj){
			// foreach($pj as $np=>$kl){
			// foreach($kl as $kls=>$jum){
				// echo $jum.'<br>';
			// }
			// }
			// }
			render("struktur_kurikulum/struktur_kurikulum", $data);
		}
		
		public function tambah_pel(){
			$data['tingkat_kelas'] = $this->m_struktur_kurikulum->get_all_tingkat(get_jenjang_sekolah());
			$data['kurikulum'] = $this->m_struktur_kurikulum->get_all_kurikulum();
			$data['peminatan_ktsp'] = $this->m_struktur_kurikulum->get_all_peminatan_ktsp();
			$data['peminatan_2013'] = $this->m_struktur_kurikulum->get_all_peminatan_2013();
			$data['kelompok_pelajaran'] = $this->m_struktur_kurikulum->get_all_kelompok_pelajaran_semua();
			$data['kualifikasi_all'] = array();
			if($kualifikasi_all = $this->m_struktur_kurikulum->get_all_kualifikasi_guru())
				foreach ($kualifikasi_all as $kualifikasi) {
					$data['kualifikasi_all'][$kualifikasi['id_kualifikasi_guru']] = $kualifikasi['kualifikasi_guru'];
				}
			$data['kualifikasi_baru'] = array();


			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";
			// exit;
			
			$data['component'] = 'kurikulum';
			render("struktur_kurikulum/form_tambah_pel", $data);
		}
		
		public function submit_post_pel(){
			$this->form_validation->set_rules('kode_pelajaran', 'Kode Pelajaran', 'required');
			$this->form_validation->set_rules('pelajaran', 'Pelajaran', 'required');
			$this->form_validation->set_rules('id_kelompok_pelajaran', 'Id Kelompok Pelajaran', 'required');
			$this->form_validation->set_rules('id_peminatan', 'Id Peminatan', 'required');
			$this->form_validation->set_rules('jumlah_jam', 'Jumlah Jam', 'required');
			$post = $this->input->post('kualifikasi');
			
			if($this->form_validation->run() == TRUE){
				$kode_pelajaran = $this->input->post('kode_pelajaran');
				$id_pelajaran = $this->input->post('id_pelajaran');
				$pelajaran = $this->input->post('pelajaran');
				$id_kelompok_pelajaran = $this->input->post('id_kelompok_pelajaran');
				$id_kualifikasi_guru = -1;
				$id_peminatan = $this->input->post('id_peminatan');
				$id_tingkat_kelas = $this->input->post('id_tingkat_kelas');
				$jumlah_jam = $this->input->post('jumlah_jam');
				$id_alokasi_waktu_per_minggu = $this->input->post('id_alokasi_waktu_per_minggu');
				
				$status = $this->m_struktur_kurikulum->get_kelompok_pelajaran_by($id_kelompok_pelajaran);
				
				$action = $this->input->post('action');
				if($action == 'add'){
					$s =1;
					if($status->status_pilihan == 0){
						$id_peminatan = 0;
					}
					for($i=0;$i<count($jumlah_jam);$i++){
						if($kode_pelajaran[$i]!="" && $jumlah_jam[$i]!= ""){
							$data['kode_pelajaran'] = $kode_pelajaran[$i];
							$data['pelajaran'] = $pelajaran;
							$data['jumlah_jam'] = $jumlah_jam[$i];
							$data['id_tingkat_kelas'] = $id_tingkat_kelas[$i];
							$data['id_kelompok_pelajaran'] = $id_kelompok_pelajaran;
							$data['id_kualifikasi_guru'] = $id_kualifikasi_guru;
							$data['id_peminatan'] = $id_peminatan;
							if(get_id_sekolah() != 0){
								$data['id_sekolah'] = get_id_sekolah();
							}

							$this->m_struktur_kurikulum->save($data);
							$s=0;
						}
					}
					if($s == 1){
						set_error_message('Masukkan jumlah jam pelajaran di tingkat kelas!');
					}else{
						set_success_message('Data Berhasil Ditambah!');
						
					}

					$pel = $this->m_struktur_kurikulum->get_last_pelajaran_arr();
					
					$this->m_struktur_kurikulum->insert_kualifikasi($pel[0]['id_pelajaran'], $post);
					
					if($this->m_struktur_kurikulum->get_pelajaran_kualifikasi_by($pel[0]['id_pelajaran']-1) != ''){
						$this->m_struktur_kurikulum->insert_kualifikasi($pel[0]['id_pelajaran']-1, $post);
						echo"a";
						if($this->m_struktur_kurikulum->get_pelajaran_kualifikasi_by($pel[0]['id_pelajaran']-2) != ''){
							$this->m_struktur_kurikulum->insert_kualifikasi($pel[0]['id_pelajaran']-2, $post);
							echo "b";
						}
					}
					

					redirect("kurikulum/struktur_kurikulum/home/");
				}
			}
			else{
				set_success_message('Error saat pengisian, silahkan coba lagi');
				redirect("kurikulum/struktur_kurikulum/home");
			}
		}
}
