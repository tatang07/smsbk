<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class kur_program extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_kur_program');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kurikulum/kur_program/home');
		}
		
		public function home(){
			
			$id_kur = isset($_POST['id_kurikulum'])?$_POST['id_kurikulum']:get_id_kurikulum();
			
			$data['kompetensi_inti'] = $this->m_kur_program->get_all_kompetensi();
			$data['tingkat_kelas'] = $this->m_kur_program->get_all_tingkat(get_jenjang_sekolah());
			$data['kur'] = $this->m_kur_program->get_all_kurikulum();
			$data['count'] = $this->m_kur_program->count_tingkat(get_jenjang_sekolah());
			$data['pelajaran'] = $this->m_kur_program->get_all_pelajaran($id_kur);
			$data['pelajaran_group'] = $this->m_kur_program->get_all_pelajaran_group_by($id_kur);
			$data['kelompok'] = $this->m_kur_program->get_all_kelompok_pelajaran($id_kur);
			$data['last'] = $this->m_kur_program->get_last_kelompok_pelajaran();
			$data['peminatan'] = $this->m_kur_program->get_all_peminatan();
			$data['beban'] = $this->m_kur_program->get_all_beban_belajar();
			
			$data['kelasc'] = $this->m_kur_program->get_all_pelajaran_by_peminatan($id_kur);
			
			$p = $this->m_kur_program->get_all_pelajaran_group_by($id_kur);
			// echo '<pre>';
			// print_r($p);
			// echo '</pre>';
			// exit();
			$pelajaran = array();
			
			if($data['kelompok']){
			foreach($data['kelompok'] as $k){
				$pelajaran[$k->id_kelompok_pelajaran] = array();
				foreach($p as $pel){
					if($k->id_kelompok_pelajaran == $pel->id_kelompok_pelajaran){
						$pelajaran[$k->id_kelompok_pelajaran][$pel->pelajaran] = array();
						foreach($data['tingkat_kelas'] as $t){
						$pelajaran[$k->id_kelompok_pelajaran][$pel->pelajaran] += array($t->tingkat_kelas => 0);
						}
					}
				}
			}
			}
			
			$p = $this->m_kur_program->get_all_pelajaran_group_by($id_kur);
			if($data['kelompok']){
			foreach($data['kelompok'] as $k){
				foreach($p as $pel){
					if($k->id_kelompok_pelajaran == $pel->id_kelompok_pelajaran){
						foreach($data['tingkat_kelas'] as $t){
							foreach($data['pelajaran'] as $pl){
								if($pl->id_tingkat_kelas == $t->id_tingkat_kelas && $pl->id_kelompok_pelajaran == $k->id_kelompok_pelajaran && $pel->pelajaran == $pl->pelajaran){
									$pelajaran[$k->id_kelompok_pelajaran][$pel->pelajaran][$t->tingkat_kelas] = $pl->jumlah_jam;
								}
							}
						}
					}
				}
			}
			}
			
			// foreach($p as $pel){
				// $pelajaran[$pel->pelajaran] = array();
			// }
			
			// // foreach($data['tingkat_kelas'] as $t){
				// // foreach($data['pelajaran'] as $pel){
					// // $pelajaran[$pel->pelajaran] += array($t->tingkat_kelas => 0);
				// // }
			// // }
			// foreach($data['tingkat_kelas'] as $t){
				// foreach($data['pelajaran'] as $pel){
					// if($pel->id_tingkat_kelas == $t->id_tingkat_kelas){
						// $pelajaran[$pel->pelajaran][$t->tingkat_kelas] = $pel->jumlah_jam;
					// }
				// }
			// }
			$data['alokasi_pelajaran'] = $pelajaran;
			// $this->mydebug($pelajaran);
			
			
			$kelasc = array();
			if($data['kelompok']){
			foreach($data['peminatan'] as $peminatan){
				$kelasc[$peminatan->peminatan] = array();
				$kelasc[$peminatan->peminatan]['kurikulum'] = $peminatan->id_kurikulum;
				$kelasc[$peminatan->peminatan]['pel'] = array();
			}
			}
			
			if($data['kelompok']){
			foreach($data['kelasc'] as $c){
					// $alokasi = $this->m_struktur_kurikulum->get_pelajaran_by($c->id_pelajaran);
				$peminatan = (array) $c;
				$peminatan += array('alokasi' => '');
				$kelasc[$c->peminatan]['pel'][] = $peminatan;
			}
			}
			// $this->mydebug($kelasc);
			$data['mapel_pilihan'] = $kelasc;
			
			// foreach($kelasc as $pem => $value_minat){
				// foreach($value_minat['pel'] as $p){
					// $this->mydebug($p);
				// }
			// }
			$data['component'] = 'kurikulum';
			render("kur_program/kur_program", $data);
		}
		
		function print_pdf($id_kur){
			$data['kompetensi_inti'] = $this->m_kur_program->get_all_kompetensi();
			$data['tingkat_kelas'] = $this->m_kur_program->get_all_tingkat(get_jenjang_sekolah());
			$data['kur'] = $this->m_kur_program->get_all_kurikulum();
			$data['count'] = $this->m_kur_program->count_tingkat(get_jenjang_sekolah());
			$data['pelajaran'] = $this->m_kur_program->get_all_pelajaran($id_kur);
			$data['pelajaran_group'] = $this->m_kur_program->get_all_pelajaran_group_by($id_kur);
			$data['kelompok'] = $this->m_kur_program->get_all_kelompok_pelajaran($id_kur);
			$data['last'] = $this->m_kur_program->get_last_kelompok_pelajaran();
			$data['peminatan'] = $this->m_kur_program->get_all_peminatan();
			$data['beban'] = $this->m_kur_program->get_all_beban_belajar();
			
			$data['kelasc'] = $this->m_kur_program->get_all_pelajaran_by_peminatan($id_kur);
			
			$p = $this->m_kur_program->get_all_pelajaran_group_by($id_kur);
			$pelajaran = array();
			
			foreach($data['kelompok'] as $k){
				$pelajaran[$k->id_kelompok_pelajaran] = array();
				foreach($p as $pel){
					if($k->id_kelompok_pelajaran == $pel->id_kelompok_pelajaran){
						$pelajaran[$k->id_kelompok_pelajaran][$pel->pelajaran] = array();
						foreach($data['tingkat_kelas'] as $t){
						$pelajaran[$k->id_kelompok_pelajaran][$pel->pelajaran] += array($t->tingkat_kelas => 0);
						}
					}
				}
			}
			
			$p = $this->m_kur_program->get_all_pelajaran_group_by($id_kur);
			foreach($data['kelompok'] as $k){
				foreach($p as $pel){
					if($k->id_kelompok_pelajaran == $pel->id_kelompok_pelajaran){
						foreach($data['tingkat_kelas'] as $t){
							foreach($data['pelajaran'] as $pl){
								if($pl->id_tingkat_kelas == $t->id_tingkat_kelas && $pl->id_kelompok_pelajaran == $k->id_kelompok_pelajaran && $pel->pelajaran == $pl->pelajaran){
									$pelajaran[$k->id_kelompok_pelajaran][$pel->pelajaran][$t->tingkat_kelas] = $pl->jumlah_jam;
								}
							}
						}
					}
				}
			}
			
			// foreach($p as $pel){
				// $pelajaran[$pel->pelajaran] = array();
			// }
			
			// foreach($data['tingkat_kelas'] as $t){
				// foreach($data['pelajaran'] as $pel){
					// $pelajaran[$pel->pelajaran] += array($t->tingkat_kelas => 0);
				// }
			// }
			// foreach($data['tingkat_kelas'] as $t){
				// foreach($data['pelajaran'] as $pel){
					// if($pel->id_tingkat_kelas == $t->id_tingkat_kelas){
						// $pelajaran[$pel->pelajaran][$t->tingkat_kelas] = $pel->jumlah_jam;
					// }
				// }
			// }
			$data['alokasi_pelajaran'] = $pelajaran;
			// $this->mydebug($pelajaran);
			
			
			$kelasc = array();
			foreach($data['peminatan'] as $peminatan){
				$kelasc[$peminatan->peminatan] = array();
			}
			
			foreach($data['kelasc'] as $c){
					// $alokasi = $this->m_struktur_kurikulum->get_pelajaran_by($c->id_pelajaran);
				$peminatan = (array) $c;
				$peminatan += array('alokasi' => '');
				$kelasc[$c->peminatan][] = $peminatan;
			}
			// $this->mydebug($kelasc);
			$data['mapel_pilihan'] = $kelasc;
			$data['statuspil'] = $id_kur;
			print_pdf($this->load->view('kur_program/kur_program_pdf', $data, true), "A4");
		}
}
