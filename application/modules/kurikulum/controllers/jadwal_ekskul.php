<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class jadwal_ekskul extends Simple_Controller {
    	
		
		public function index(){	
			$this->load->model('m_jadwal_ekskul');
			$data['data'] = $this->m_jadwal_ekskul->select_isi()->result_array();
			$data['list_ekskul'] = $this->m_jadwal_ekskul->getList()->result_array();
			$data['component'] = 'kurikulum';
			$data['gid'] = $gid = get_usergroup();
			render('ekskul_jadwal/jadwal_ekskul',$data);
		}
		
		public function tambah_data(){
			$this->load->model('m_jadwal_ekskul');
			$data['list_ekskul'] = $this->m_jadwal_ekskul->getList()->result_array();
			$data['component'] = 'kurikulum';
			render('ekskul_jadwal/tambah_jadwal_ekskul',$data);
		}
		public function simpan_tambah_data(){
			$this->load->model('m_jadwal_ekskul');
			$data['id_ekstra_kurikuler'] = $this->input->post('id_ekstra_kurikuler');
			$data['hari'] = $this->input->post('hari');
			$data['jam_mulai'] = $this->input->post('jam_mulai');
			$data['jam_selesai'] = $this->input->post('jam_selesai');
			$data['tempat'] = $this->input->post('tempat');
			$this->m_jadwal_ekskul->tambah_data($data);
			set_success_message('Data Berhasil Ditambah!');
			redirect(site_url('kurikulum/jadwal_ekskul/index/'));
		}
		
		public function simpan_edit_data(){
			$this->load->model('m_jadwal_ekskul');
			$id_ekstra_kurikuler = $this->input->post('id_ekstra_kurikuler');
			$id = $this->input->post('id');
			$hari = $this->input->post('hari');
			$jam_mulai = $this->input->post('jam_mulai');
			$jam_selesai = $this->input->post('jam_selesai');
			$tempat = $this->input->post('tempat');
			
			$this->m_jadwal_ekskul->update_data($id, $hari, $jam_mulai, $jam_selesai, $tempat, $id_ekstra_kurikuler);
			set_success_message('Data Berhasil Diupdate!');
			redirect(site_url('kurikulum/jadwal_ekskul/index/'));
		}
		
		public function edit_data($id){
			$this->load->model('m_jadwal_ekskul');
			$data['list_ekskul'] = $this->m_jadwal_ekskul->getList()->result_array();
			$data['id'] = $id;
			$data['data'] = $this->m_jadwal_ekskul->getDataById($id)->result_array();
			$data['component'] = 'kurikulum';
			// echo $data['$id'];
			render('ekskul_jadwal/edit_jadwal_ekskul',$data);
		}
		public function delete($id){
			$this->load->model('m_jadwal_ekskul');
			$this->m_jadwal_ekskul->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect(site_url('kurikulum/jadwal_ekskul/index/'));
		}

}
