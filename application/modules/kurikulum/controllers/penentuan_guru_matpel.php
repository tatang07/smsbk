<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class penentuan_guru_matpel extends CI_Controller {
	public function index()
	{	
		$this->load->model('kurikulum/m_penentuan_guru_matpel');
		$data['tingkat_kelas'] = get_tingkat_kelas();
		$data['id_tingkat_kelas']=0;
		$data['component'] = 'kurikulum';
		render('penentuan_guru_matpel/datalist', $data);
	}

	public function tampil($id_rombel = 0, $id_tingkat_kelas = 0){
		$this->load->model('kurikulum/m_penentuan_guru_matpel');

		$data['tingkat_kelas'] = get_tingkat_kelas();
		$send = $this->input->post('send');

		if($send=="Tampilkan"){
			$id_tingkat_kelas = $this->input->post('tingkat_kelas');
			$id_rombel = $this->input->post('kelas');
		}
		
		$data['rombels'] = $this->getRombel($id_tingkat_kelas, $id_rombel, true);

		$data['mapel'] =  $this->m_penentuan_guru_matpel->getData($id_rombel)->result_array();
		$data['id_rombel'] = $id_rombel;
		$data['id_tingkat_kelas'] = $id_tingkat_kelas;
		$data['component'] = 'kurikulum';
		render('penentuan_guru_matpel/datalist', $data);
	}

	public function delete($id_guru_matpel_rombel,  $id_rombel, $id_tingkat_kelas){
		$this->load->model('kurikulum/m_penentuan_guru_matpel');
		$this->m_penentuan_guru_matpel->delete($id_guru_matpel_rombel);
		$data['id_tingkat_kelas'] = $id_tingkat_kelas;

		$this->tampil($id_rombel, $id_tingkat_kelas);
	}

	public function getRombel($id_tingkat_kelas, $id_rombel = false, $return = false){
		$this->load->model('kurikulum/m_penentuan_guru_matpel');
		$id_tahun_ajaran = get_id_tahun_ajaran();
		$data['rombel'] = $this->m_penentuan_guru_matpel->getRombel($id_tingkat_kelas, $id_tahun_ajaran)->result();

		$output = "";
		foreach($data['rombel'] as $r){
			if($id_rombel && $r->id_rombel == $id_rombel){
				$output .= "<option selected='selected' value='".$r->id_rombel."'>".$r->rombel."</option>";
			}else{
				$output .= "<option value='".$r->id_rombel."'>".$r->rombel."</option>";				
			}
		}
		
		if($return === false)
			echo $output;
		else
			return $output;
	}

	public function tampil_data(){
		$this->load->model('kurikulum/m_penentuan_guru_matpel');
		$id_tingkat_kelas = $this->input->post('tingkat_kelas');

		$data['id_kurilukum'] =  $this->m_penentuan_guru_matpel->getIdKurikulum($id_tingkat_kelas)->row();
		$kurikulum = $data['id_kurilukum'];

		$data['matpel'] = $this->m_penentuan_guru_matpel->getDataMatpel($kurikulum->id_kurikulum, $id_tingkat_kelas)->result_array();
		$data['id_rombel'] = $this->input->post('kelas');
		$data['guru'] = $this->m_penentuan_guru_matpel->getGuru()->result_array();

		$data['component'] = 'kurikulum';
		render('penentuan_guru_matpel/penentuan_guru_matpel', $data);
	}

	public function tambah($id_rombel, $id_tingkat_kelas){
		$this->load->model('kurikulum/m_penentuan_guru_matpel');
		
		$data['id_kurilukum'] =  $this->m_penentuan_guru_matpel->getIdKurikulum($id_rombel)->row();
		$kurikulum = $data['id_kurilukum'];
		$id_sekolah = get_id_sekolah();
		//echo $kurikulum->id_kurikulum;

		$data['mapel'] = $this->m_penentuan_guru_matpel->getDataNonGuru($id_rombel, $kurikulum->id_kurikulum, $id_tingkat_kelas);
		$data['guru'] = $this->m_penentuan_guru_matpel->getGuru($id_sekolah)->result_array();
		$data['kelompok_pelajaran'] = $this->m_penentuan_guru_matpel->getKelompokPelajaran($kurikulum->id_kurikulum)->result_array();
		$data['peminatan'] = $this->m_penentuan_guru_matpel->getpeminatan()->result_array();

		$kurikulum = $this->m_penentuan_guru_matpel->getKurikulum($kurikulum->id_kurikulum)->result_array();
		$tahunajaran = $this->m_penentuan_guru_matpel->getTahunAjaran()->result_array();
		$rombel = $this->m_penentuan_guru_matpel->getRombelId($this->uri->rsegment(3))->result_array();
		$peminatan = $this->m_penentuan_guru_matpel->getPeminatanId($this->uri->rsegment(3))->result_array();
		
		$data['kurikulum'] = $kurikulum[0]['nama_kurikulum'];
		$data['tahun_ajaran'] = $tahunajaran[0]['tahun_awal'].'/'.$tahunajaran[0]['tahun_akhir'];
		$data['id_rombel'] = $this->uri->rsegment(3);
		if($rombel){
		$data['namarombel'] = $rombel[0]['rombel'];
		}else{
		$data['namarombel'] = '-';
		}
		if($peminatan){
		$data['namapeminatan'] = $peminatan[0]['peminatan'];
		}else{
		$data['namapeminatan'] = '-';
		}
		$data['id_tingkat_kelas'] = $this->uri->rsegment(4);

		// echo '<pre>';
		// print_r($rombel);
		// echo '</pre>';
		$data['component'] = 'kurikulum';
		render('penentuan_guru_matpel/tambah', $data);		
	}

	public function simpan(){
		$this->load->model('kurikulum/m_penentuan_guru_matpel');
		$data['id_rombel'] = $this->input->post('id_rombel');
		$data['id_pelajaran'] = $this->input->post('id_pelajaran');
		$data['id_guru'] = $this->input->post('id_guru');

		$x = array();
		for($i=0;$i<count($data['id_rombel']);$i++){
			if($data['id_guru'][$i] != '--- Pilih Guru ---'){
				$x[] = array(
						'id_rombel' => $data['id_rombel'][$i],
						'id_pelajaran' => $data['id_pelajaran'][$i],
						'id_guru' => $data['id_guru'][$i]
					);
			}
		}

		$id_r = $this->input->post('idr');
		$id_t = $this->input->post('idk');

		$this->m_penentuan_guru_matpel->simpan($x);
		$this->tampil($id_r, $id_t);
		//redirect(site_url('kurikulum/penentuan_guru_matpel/tampil'));
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */