<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class daftar_pelajaran extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_daftar_pelajaran');
			$this->load->library("pagination");
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kurikulum/daftar_pelajaran/home');
		}
		
		public function home(){
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/daftar_pelajaran/home";
			$config["total_rows"] = $this->m_daftar_pelajaran->count_pelajaran();
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			$data['jumlah'] = $this->m_daftar_pelajaran->count_pelajaran();
			$data['tingkat_kelas'] = $this->m_daftar_pelajaran->get_all_tingkat(get_jenjang_sekolah());
			$data['kur'] = $this->m_daftar_pelajaran->get_all_kurikulum();
			$data['count'] = $this->m_daftar_pelajaran->count_tingkat(get_jenjang_sekolah());
			$data['pelajaran'] = $this->m_daftar_pelajaran->get_all_pelajaran($config["per_page"], $page);
			$data['kelompok'] = $this->m_daftar_pelajaran->get_all_kelompok_pelajaran();
			$data['peminatan'] = $this->m_daftar_pelajaran->get_all_peminatan();
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			
			$data['status_pilihan'] = 0;
			$data['jenjang'] = get_jenjang_sekolah();
			$data['status_kurikulum'] = 2;
			
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			$data['posisi'] = 'home';
			render("daftar_pelajaran/daftar_pelajaran", $data);
		}
	
		public function edit(){
			$id = $this->uri->segment(4);
			if ($id == NULL) {
				redirect('kurikulum/daftar_pelajaran/home');
			}
			
			$row = $this->m_daftar_pelajaran->get_pelajaran_by($id);
			$data['id_pelajaran'] = $id;
			$data['kode_pelajaran'] = $row->kode_pelajaran;
			$data['pelajaran'] = $row->pelajaran;
			$data['id_kelompok_pelajaran'] = $row->id_kelompok_pelajaran;
			$data['id_kualifikasi_guru'] = $row->id_kualifikasi_guru;
			$data['id_peminatan'] = $row->id_peminatan;
			$data['id_tingkat_kelas'] = $row->id_tingkat_kelas;
			$data['jumlah_jam'] = $row->jumlah_jam;
			$data['tingkat_kelas'] = $this->m_daftar_pelajaran->get_all_tingkat(get_jenjang_sekolah());
			$data['kurikulum'] = $this->m_daftar_pelajaran->get_all_kurikulum();
			$data['peminatan'] = $this->m_daftar_pelajaran->get_all_peminatan();
			$data['kelompok_pelajaran'] = $this->m_daftar_pelajaran->get_all_kelompok_pelajaran();
			$data['kualifikasi_guru'] = $this->m_daftar_pelajaran->get_all_kualifikasi_guru();
			
			$status = $this->m_daftar_pelajaran->get_kelompok_pelajaran_by($row->id_kelompok_pelajaran);
			$data['status'] = $status->status_pilihan;
			 // $this->mydebug($data['alokasi']);
			$data['component'] = 'kurikulum';
			render("daftar_pelajaran/form_edit", $data);
		}
		
		public function delete($id){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/daftar_pelajaran/home');
			}
			
			$this->m_daftar_pelajaran->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('kurikulum/daftar_pelajaran/home');
		}
		
		public function search(){
			// echo 'it works!';
			$id_kurikulum = $this->input->post('id_kurikulum');
			$id_tingkat_kelas = $this->input->post('id_tingkat_kelas');
			$id_kelompok_pelajaran = $this->input->post('id_kelompok_pelajaran');
			$id_peminatan = $this->input->post('id_peminatan');
			
			$kelompok = $this->m_daftar_pelajaran->get_kelompok_pelajaran_by($id_kelompok_pelajaran);
			if($kelompok){ $status = $kelompok->status_pilihan; }else{$status=0;}
			
			$data['links'] = "";
			$data['page'] = 0;
			
			$data['id_kurikulum'] = $id_kurikulum;
			$data['id_tingkat_kelas'] = $id_tingkat_kelas;
			$data['id_kelompok_pelajaran'] = $id_kelompok_pelajaran;
			$data['id_peminatan'] = $id_peminatan;
			
			$row = $this->m_daftar_pelajaran->get_kelompok_pelajaran_by($id_kelompok_pelajaran);
			if($row){ $data['status_pilihan'] = $row->status_pilihan;}else{$data['status_pilihan']=0;}
			
			// print_r($row);
			$data['tingkat_kelas'] = $this->m_daftar_pelajaran->get_all_tingkat(get_jenjang_sekolah());
			$data['kur'] = $this->m_daftar_pelajaran->get_all_kurikulum();
			$data['count'] = $this->m_daftar_pelajaran->count_tingkat(get_jenjang_sekolah());
			$data['kelompok'] = $this->m_daftar_pelajaran->get_all_kelompok_pelajaran();
			$data['peminatan'] = $this->m_daftar_pelajaran->get_all_peminatan();
			if($status == 1){
				$data['pelajaran'] = $this->m_daftar_pelajaran->get_pelajaran($id_tingkat_kelas, $id_kelompok_pelajaran, $id_peminatan, $id_kurikulum);
				$data['jumlah'] = $this->m_daftar_pelajaran->get_count_pelajaran($id_tingkat_kelas, $id_kelompok_pelajaran, $id_peminatan, $id_kurikulum);
			}else{
				$data['pelajaran'] = $this->m_daftar_pelajaran->get_pelajaran($id_tingkat_kelas, $id_kelompok_pelajaran, 0, $id_kurikulum);
				$data['jumlah'] = $this->m_daftar_pelajaran->get_count_pelajaran($id_tingkat_kelas, $id_kelompok_pelajaran, 0, $id_kurikulum);
			}
			
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			$data['posisi'] = 'search';
			render("daftar_pelajaran/daftar_pelajaran", $data);
		}
		
		public function submit_post(){
			$this->form_validation->set_rules('kode_pelajaran', 'Kode Pelajaran', 'required');
			$this->form_validation->set_rules('pelajaran', 'Pelajaran', 'required');
			$this->form_validation->set_rules('jumlah_jam', 'Jumlah Jam', 'required');
			
			if($this->form_validation->run() == TRUE){
				$id_pelajaran = $this->input->post('id_pelajaran');
				$kode_pelajaran = $this->input->post('kode_pelajaran');
				$pelajaran = $this->input->post('pelajaran');
				$jumlah_jam = $this->input->post('jumlah_jam');
				$id_kelompok_pelajaran = $this->input->post('id_kelompok_pelajaran');
				$id_tingkat_kelas = $this->input->post('id_tingkat_kelas');
				$id_kualifikasi_guru = $this->input->post('id_kualifikasi_guru');
				$id_peminatan = $this->input->post('id_peminatan');
				
				$status = $this->m_daftar_pelajaran->get_kelompok_pelajaran_by($id_kelompok_pelajaran);
				if($status->status_pilihan == 0){
					$id_peminatan = 0;
				}
				
				$action = $this->input->post('action');
				if($action == 'update'){
					$this->m_daftar_pelajaran->update($id_pelajaran, $kode_pelajaran, $pelajaran, $jumlah_jam, $id_tingkat_kelas, $id_kelompok_pelajaran, $id_kualifikasi_guru, $id_peminatan);
					set_success_message('Data Berhasil Diupdate!');
					redirect("kurikulum/daftar_pelajaran/home");
				}
			}
			else{
				redirect("kurikulum/daftar_pelajaran/home/e");
			}
		}
		
}
