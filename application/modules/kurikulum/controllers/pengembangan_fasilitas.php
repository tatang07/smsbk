<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class pengembangan_fasilitas extends Simple_Controller {
    	 public function index(){
			redirect('kurikulum/pengembangan_fasilitas/datalist');
	    }

    	public $myconf = array(
			//Nama controller
			"name" 			=> "kurikulum/pengembangan_fasilitas",
			"component"		=> "kurikulum",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Kurikulum",
			"subtitle"		=> "Fasilitas Pengembangan Diri",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "t_ekskul_fasilitas",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"ekstra_kurikuler_fasilitas"=>"Fasilitas",
								"jumlah_baik"=>"Jumlah Baik",
								"jumlah_rusak_ringan"=>"Jumlah Rusak Ringan",
								"jumlah_rusak_berat"=>"Jumlah Rusak Berat",
								"id_ekstra_kurikuler"=>"Pengembangan Diri",
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								"id_ekstra_kurikuler_fasilitas"=>"hidden",
								"id_ekstra_kurikuler"=>array("m_ekstra_kurikuler","id_ekstra_kurikuler","nama_ekstra_kurikuler")
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"ekstra_kurikuler_fasilitas"
							), 
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								"id_ekstra_kurikuler_fasilitas"
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "pengembangan_fasilitas/datalist",
								//"view_file_datatable"	=> "pengembangan_fasilitas/datatable",
								"view_file_datatable_action"	=> "ekskul_fasilitas/datatable_action",
								"field_list"			=> array("ekstra_kurikuler_fasilitas", "jumlah_baik", "jumlah_rusak_ringan", "jumlah_rusak_berat"),
								"field_sort"			=> array("ekstra_kurikuler_fasilitas", "jumlah_baik", "jumlah_rusak_ringan", "jumlah_rusak_berat"),
								"field_filter"			=> array(),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(
																"id_ekstra_kurikuler"=>array("label"=>"Pengembangan Diri","table"=>"m_ekstra_kurikuler", "table_id"=>"id_ekstra_kurikuler", "table_label"=>"nama_ekstra_kurikuler"),
															),
								//"field_comparator"		=> array("id_program"=>"=","id_sekolah"=>"="),
								//"field_operator"		=> array("id_program"=>"AND","id_sekolah"=>"AND"),
								//"field_align"			=> array(),
								"field_size"			=> array(),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								// "custom_action_link"	=> array("kurikulum/program"),
								"custom_add_link"		=> array('label'=>'Add','href'=>'kurikulum/ekskul_fasilitas/add' ),
								"custom_edit_link"		=> array('label'=>'Edit','href'=>'kurikulum/ekskul_fasilitas/edit'),
								//"custom_delete_link"	=> array(),
								//"custom_link"			=> array("program"=>"program/datalist","Siswa"=>"Siswa/datalist"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								// "view_file"				=> "kurikulum/program/detail",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("ekstra_kurikuler_fasilitas", "jumlah_baik", "jumlah_rusak_ringan", "jumlah_rusak_berat", "id_ekstra_kurikuler"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								// "redirect_link"			=> "kurikulum/program"
							),
			//Konfigurasi tampilan edit
			/*"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								// "enable"				=> TRUE,
								// "model_name"			=> "",
								// "table"					=> array(),
								// "view_file"				=> "simple/edit",
								// "view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("program","id_program","id_sekolah"),
								// "custom_action"			=> "",
								// "msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								// "msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> "kurikulum"
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);
		
			

		public function pre_process($data){
			//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
			//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
						
			$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			// $list_program = get_list("m_program","id_program","program");
			$data['conf']['data_list']['subtitle'] = "Data Fasilitas Pengembangan Diri";
			$data['conf']['data_add']['subtitle'] = "Tambah";
			$data['conf']['data_edit']['subtitle'] = "Edit";
			
			$data['conf']['data_delete']['redirect_link'] = "kurikulum/pengembangan_fasilitas/datalist/";
			$data['conf']['data_add']['redirect_link']    = "kurikulum/pengembangan_fasilitas/datalist/";
			$data['conf']['data_edit']['redirect_link']   = "kurikulum/pengembangan_fasilitas/datalist/";
			
			return $data;
		}
		
		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
			$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			// $_POST['filter']['id_program'] = $id_program;
			//$_POST['filter']['id_sekolah'] = get_id_sekolah();
			// $_POST['filter']['e.jenis'] = 0;
			
			$data['conf']['data_list']['field_filter_dropdown']['id_ekstra_kurikuler']['filter'] = array('jenis'=>2, 'id_sekolah'=>get_id_sekolah());
			
			$gid = get_usergroup();
			if($gid == 9){
				$data['conf']['data_list']['enable_action']=false;
				$data['conf']['data_add']['enable']=false;
				$data['conf']['data_edit']['enable']=false;
				$data['conf']['data_delete']['enable']=false;
			}
			
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															//'id_program'=>$id_program
															//'id_sekolah'=>get_id_sekolah() 
														);
			
			//Mengganti link add yang ada di kanan atas form datalist
			$data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'kurikulum/pengembangan_fasilitas/add/');			
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'kurikulum/pengembangan_fasilitas/edit');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'kurikulum/pengembangan_fasilitas/delete');
			
			return $data;
		}
		
		public function before_datatable($data){
			//Method ini hanya akan dieksekusi di awal method datatable
			//Mengambil id_pihak yang diposting dari form datalist
			$_POST['filter']['e.jenis'] = 2;
			// $filter = $this->input->post("filter");			
			// $id_program = $filter['id_program'];
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'kurikulum/pengembangan_fasilitas/edit/');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'kurikulum/pengembangan_fasilitas/delete/');
			
			return $data;
		}
		
				
		public function before_render_add($data){
			$data['conf']['data_type']['id_ekstra_kurikuler']['list'] = get_list_filter("m_ekstra_kurikuler","id_ekstra_kurikuler",
								array("nama_ekstra_kurikuler"),
								array('jenis'=>2, 'id_sekolah'=>get_id_sekolah()));
								
			return $data;
		}
		
		public function before_add($data){
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
				//$data['item']['id_sekolah'] = get_id_sekolah();
			}
			return $data;
		}
			
		public function before_render_edit($data){
			
				$data['conf']['data_type']['id_ekstra_kurikuler']['list'] = get_list_filter("m_ekstra_kurikuler","id_ekstra_kurikuler",
								array("nama_ekstra_kurikuler"),
								array('jenis'=>2, 'id_sekolah'=>get_id_sekolah()));
			
			return $data;
		}
		
		public function before_edit($data){
			if(!$this->input->post()){
				$data['id'] = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 0;
			}
			return $data;
		}
		
		public function before_delete($data){
			$data['id'] = $this->uri->rsegment(3);
			return $data;
		}
}
