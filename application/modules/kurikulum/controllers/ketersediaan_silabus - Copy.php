<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class ketersediaan_silabus extends CI_Controller {

	public function index()
	{	
		$this->load->model('kurikulum/m_ketersediaan_silabus');
		$data['data'] = $this->m_ketersediaan_silabus->select_data()->result_array();	//kur 2013
		$data['data2'] = $this->m_ketersediaan_silabus->select_data2()->result_array();	//ktsp
		$data['pelajaran'] = $this->m_ketersediaan_silabus->select_pelajaran()->result_array();
		$data['kelas'] = $this->m_ketersediaan_silabus->select_kelas()->result_array();
		$data['program'] = $this->m_ketersediaan_silabus->select_program()->result_array();
		$data['silabus'] = $this->m_ketersediaan_silabus->select_silabus()->result_array();
		$data['kur'] = $this->m_ketersediaan_silabus->get_all_kurikulum();
		
		$silabus=array();
		foreach($data['pelajaran'] as $pel){
			if($pel['id_peminatan']!=0){
			foreach($data['kelas'] as $kel){
				foreach($data['program'] as $pro) {
				$silabus[$pel['kode_pelajaran']."|".$pel['pelajaran']][$kel['id_tingkat_kelas']][$pro['id_program']] = 'Tidak ada';
				}
			}
			}elseif($pel['id_peminatan']==0){
			foreach($data['kelas'] as $kel){
				if($kel['id_tingkat_kelas']==10){
					foreach($data['program'] as $pro) {
					$silabus[$pel['kode_pelajaran']."|".$pel['pelajaran']][$kel['id_tingkat_kelas']][$pro['id_program']] = 'Tidak Ada';		//Default untuk kelas 10 yg IPA IPS BAHASA 
					}
				}else{
					foreach($data['program'] as $pro) {
					$silabus[$pel['kode_pelajaran']."|".$pel['pelajaran']][$kel['id_tingkat_kelas']][$pro['id_program']] = '&nbsp';		//Default untuk kelas 10 yg IPA IPS BAHASA 
					}
				}
			}
			}
			
		}	
		
		//ktsp
		$silabus2=array();
		foreach($data['pelajaran'] as $pel){
			if($pel['id_peminatan']!=0){
			foreach($data['kelas'] as $kel){
				foreach($data['program'] as $pro) {
				$silabus2[$pel['kode_pelajaran']."|".$pel['pelajaran']][$kel['id_tingkat_kelas']][$pro['id_program']] = 'Tidak ada';
				}
			}
			}elseif($pel['id_peminatan']==0){
			foreach($data['kelas'] as $kel){
				if($kel['id_tingkat_kelas']==10){
					foreach($data['program'] as $pro) {
					$silabus2[$pel['kode_pelajaran']."|".$pel['pelajaran']][$kel['id_tingkat_kelas']][$pro['id_program']] = 'Tidak Ada';		//Default untuk kelas 10 yg IPA IPS BAHASA 
					}
				}else{
					foreach($data['program'] as $pro) {
					$silabus2[$pel['kode_pelajaran']."|".$pel['pelajaran']][$kel['id_tingkat_kelas']][$pro['id_program']] = '&nbsp';		//Default untuk kelas 10 yg IPA IPS BAHASA 
					}
				}
			}
			}
			
		}	
		
		
		$i=1;
		foreach($data['data'] as $d){
			if($d['id_peminatan']!=0){
			$silabus[$d['kode_pelajaran']."|".$d['pelajaran']][$d['id_tingkat_kelas']][$d['id_program']]='Ada';			//set ada untuk yg bukan peminatan 0
			}elseif($d['id_peminatan']==0){
				if($d['id_tingkat_kelas']==10){
				// Set ada untuk kelas X yg IPA IPS BAHASA klw ada
				$silabus[$d['kode_pelajaran']."|".$d['pelajaran']][$d['id_tingkat_kelas']][1]='Ada';
				$silabus[$d['kode_pelajaran']."|".$d['pelajaran']][$d['id_tingkat_kelas']][2]='Ada';
				$silabus[$d['kode_pelajaran']."|".$d['pelajaran']][$d['id_tingkat_kelas']][3]='Ada';
				}
			}
		}
		foreach($data['data2'] as $d){
			if($d['id_peminatan']!=0){
			$silabus2[$d['kode_pelajaran']."|".$d['pelajaran']][$d['id_tingkat_kelas']][$d['id_program']]='Ada';			//set ada untuk yg bukan peminatan 0
			}elseif($d['id_peminatan']==0){
				if($d['id_tingkat_kelas']==10){
				// Set ada untuk kelas X yg IPA IPS BAHASA klw ada
				$silabus2[$d['kode_pelajaran']."|".$d['pelajaran']][$d['id_tingkat_kelas']][1]='Ada';
				$silabus2[$d['kode_pelajaran']."|".$d['pelajaran']][$d['id_tingkat_kelas']][2]='Ada';
				$silabus2[$d['kode_pelajaran']."|".$d['pelajaran']][$d['id_tingkat_kelas']][3]='Ada';
				}
			}
		}
	
		$data['hasil']=$silabus;
		$data['hasil2']=$silabus2;
		
		$data['component'] = 'kurikulum';
		render('ketersediaan_silabus/ketersediaan_silabus',$data);
	}
	
	
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */