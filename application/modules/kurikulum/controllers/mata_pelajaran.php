<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class mata_pelajaran extends Simple_Controller {
    	 public function index(){
			redirect('kurikulum/mata_pelajaran/datalist');
	    }
		
    	public $myconf = array(
			//Nama controller
			"name" 			=> "kurikulum/mata_pelajaran",
			"component"		=> "kurikulum",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Kurikulum Sekolah",
			"subtitle"		=> "Mata Pelajaran",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "m_mata_pelajaran",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"kode_mata_pelajaran"=>"Kode Mata Pelajaran",
								"id_pelajaran"=>"Pelajaran",						
								"jumlah_jam"=>"Jumlah Jam",						
								"id_program"=>"Program",						
								"id_tingkat_kelas"=>"Tingkat Kelas",					
								"pelajaran"=>"Pelajaran",						
								"program"=>"Program",						
								"tingkat_kelas"=>"Tingkat Kelas"						
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								"id_pelajaran"=>array("m_pelajaran","id_pelajaran","pelajaran"),					
								"id_program"=>array("m_program","id_program","program"),				
								"id_tingkat_kelas"=>array("m_tingkat_kelas","id_tingkat_kelas","tingkat_kelas"),
								"id_kurikulum"=>"hidden",
								"id_sekolah"=>"hidden",
								"id_mata_pelajaran"=>"hidden"
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"kode_mata_pelajaran","id_pelajaran","jumlah_jam","id_program","id_tingkat_kelas"
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								"id_mata_pelajaran","kode_mata_pelajaran"
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("kode_mata_pelajaran","pelajaran","jumlah_jam","program","tingkat_kelas"),
								"field_sort"			=> array("kode_mata_pelajaran","pelajaran","jumlah_jam","program","tingkat_kelas"),
								"field_filter"			=> array("kode_mata_pelajaran","pelajaran","jumlah_jam","program","tingkat_kelas"),
								"field_filter_dropdown"	=> array(
																"id_pelajaran"=>array("label"=>"Pelajaran","table"=>"m_pelajaran", "table_id"=>"id_pelajaran", "table_label"=>"pelajaran"),
															),
								"field_filter_hidden"	=> array(),
								"field_comparator"		=> array("id_mata_pelajaran"=>"=","id_pelajaran"=>"="),
								"field_operator"		=> array("id_mata_pelajaran"=>"AND","id_pelajaran"=>"AND"),
								//"field_align"			=> array(),
								"field_size"			=> array(),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								//"custom_action_link"	=> array(),
								//"custom_add_link"		=> array('label'=>'Add','href'=>'school/add' ),
								//"custom_edit_link"		=> array(),
								//"custom_delete_link"	=> array(),
								//"custom_link"			=> array("Kurikulum"=>"kurikulum/datalist","Siswa"=>"Siswa/datalist"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("kode_mata_pelajaran","id_pelajaran","jumlah_jam","id_program","id_tingkat_kelas"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								//"redirect_link"			=> ""
							),
			//Konfigurasi tampilan edit
			/*"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"view_file"				=> "simple/edit",
								"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array(),
								"custom_action"			=> "",
								"msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								"msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);
		
}
