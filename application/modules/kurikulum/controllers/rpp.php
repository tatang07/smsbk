<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class rpp extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('t_rpp');
			$this->load->helper('download');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kurikulum/rpp/home');
		}
		
		public function home(){
			
			$data['pelajaran'] = $this->t_rpp->get_all_pelajaran();
			$first_pelajaran = $this->t_rpp->get_first_pelajaran();
			$id_pelajaran = $first_pelajaran->id_pelajaran;
			$data['rombel'] = $this->t_rpp->get_all_rombel();
			$first_rombel = $this->t_rpp->get_first_rombel();
			if($first_rombel){
				$id_rombel = $first_rombel->id_rombel;
			}else{
				$id_rombel = 0;
			}
			
			$id_term = get_id_term();
			
			$id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
			$id_gmr = isset($_POST['id_gmr'.$id_rombel])?$_POST['id_gmr'.$id_rombel]:0;
			$data["id_rombel"] = $id_rombel;
			$data["id_gmr"] = $id_gmr;
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/rpp/home";
			$config["total_rows"] = $this->t_rpp->count_datalist($id_gmr, $id_term);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->t_rpp->get_datalist($id_gmr, $id_term, $config["per_page"], $page);
			$data['first_rombel'] = $id_rombel;
			$data['first_pelajaran'] = $id_pelajaran;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['stat'] = 'home';
			$data['jumlah'] = $this->t_rpp->count_datalist($id_gmr, $id_term);
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			render("rpp/rpp", $data);
		}
		
		public function search(){
			
			$id_rombel = $this->input->post('id_rombel');
			$id_gmr = $this->input->post('id_gmr');
			
			$id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
			$id_gmr = isset($_POST['id_gmr'.$id_rombel])?$_POST['id_gmr'.$id_rombel]:0;
			$data["id_rombel"] = $id_rombel;
			$data["id_gmr"] = $id_gmr;
			$row = $this->t_rpp->get_gmr_by($id_gmr);
			if($row){
			$id_pelajaran = $row->id_pelajaran;
			}else{
			$id_pelajaran = 0;
			}
			
			$data['pelajaran'] = $this->t_rpp->get_all_pelajaran();
			$data['rombel'] = $this->t_rpp->get_all_rombel();
			
			$id_term = get_id_term();
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/rpp/home";
			$config["total_rows"] = $this->t_rpp->count_datalist($id_gmr, $id_term);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->t_rpp->get_datalist($id_gmr, $id_term, $config["per_page"], $page);
			$data['first_rombel'] = $id_rombel;
			$data['first_pelajaran'] = $id_pelajaran;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['stat'] = 'search';
			$data['jumlah'] = $this->t_rpp->count_datalist($id_gmr, $id_term);
			
			$data['gid'] = $gid = get_usergroup();
			
			$data['component'] = 'kurikulum';
			render("rpp/rpp", $data);
		}
		
		public function add(){
			$data['term'] = $this->t_rpp->get_all_term();
			$data['guru'] = $this->t_rpp->get_all_guru();
			$data['rombel'] = $this->t_rpp->get_all_rombel();
			$data['guru_matpel_rombel'] = $this->t_rpp->get_all_guru_matpel_rombel();
			
			$data['component'] = 'kurikulum';
			render("rpp/form_tambah", $data);
		}
		
		public function get_mapel_guru($id_rombel){
			$mapel = $this->t_rpp->get_matpel_rombel($id_rombel);
			
			$output = "";
			foreach($mapel as $m){
				$guru = $this->t_rpp->get_guru_matpel($m->id_pelajaran, $id_rombel);
				foreach($guru as $g){
					if($m->id_guru == $g->id_guru){
						$row = $this->t_rpp->get_guru_matpel_rombel($g->id_guru, $m->id_pelajaran, $id_rombel);
						$id_guru_matpel_rombel = $row->id_guru_matpel_rombel;
						
						$output .= "<option value='".$id_guru_matpel_rombel."'>".$g->nama." | ".$m->pelajaran."</option>";
					}
				}
			}
				
			echo $output;
		}
		
		public function submit(){
			$this->form_validation->set_rules('nama_rpp', 'Kode Pelajaran', 'required');
			$this->form_validation->set_rules('pertemuan_ke', 'Pelajaran', 'required');
			$this->form_validation->set_rules('id_guru_matpel_rombel', ' Guru Matpel Rombel', 'required');
			
			if($this->form_validation->run() == TRUE){
				$nama_rpp = $this->input->post('nama_rpp');
				$pertemuan_ke = $this->input->post('pertemuan_ke');
				$id_guru_matpel_rombel = $this->input->post('id_guru_matpel_rombel');
				$id_term = get_id_term();
				
				$config['upload_path'] = './extras/rpp/';
				$config['allowed_types'] = 'pdf|doc|docx|xls|txt';
				$config['max_size']	= '10000';

				$this->load->library('upload', $config);

				if ( ! $this->upload->do_upload())
				{
					$error = array('error' => $this->upload->display_errors());

				}
				else
				{
					$lokasi_file = $this->upload->data();
				}
			
				$action = $this->input->post('action');
				if($action == 'add'){
					$this->t_rpp->insert($nama_rpp, $pertemuan_ke, $lokasi_file['full_path'], $id_guru_matpel_rombel, $id_term);
					set_success_message('Data Berhasil Ditambah!');
					redirect("kurikulum/rpp/home/");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian!');
				redirect("kurikulum/rpp/home/e");
			}
		}
		
		public function delete(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/rpp/home');
			}
			
			$this->t_rpp->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('kurikulum/rpp/home/d');
		}
		
		public function download(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/rpp/home');
			}
			$row = $this->t_rpp->get_rpp_by($id);
			$file = $row->lokasi_file;
			$data = file_get_contents($file);
			$filename = basename($file);;
			
			force_download($filename, $data);
		}
		
}
