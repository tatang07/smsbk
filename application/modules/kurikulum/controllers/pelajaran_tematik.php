<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class pelajaran_tematik extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_pelajaran_tematik');
			$this->load->helper('download');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kurikulum/pelajaran_tematik/home');
		}
		
		public function home(){
		
			$data['tingkat'] = $this->m_pelajaran_tematik->get_all_tingkat(get_jenjang_sekolah());
			
			$config = array();
			$config["base_url"] = base_url() . "kurikulum/pelajaran_tematik/home";
			$config["total_rows"] = $this->m_pelajaran_tematik->count_datalist();
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->m_pelajaran_tematik->get_datalist($config["per_page"], $page);
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['jumlah'] = $this->m_pelajaran_tematik->count_datalist();
			$data['component'] = 'kurikulum';
			render("pelajaran_tematik/pelajaran_tematik", $data);
		}
		
		public function add(){
			$data['tingkat'] = $this->m_pelajaran_tematik->get_all_tingkat(get_jenjang_sekolah());
			$data['id'] = 0;
			$data['kode'] = '';
			$data['tema'] = '';
			$data['alokasi'] = '';
			$data['tingkat_kelas'] = '';
			
			$data['action'] = 'add';
			
			$data['component'] = 'kurikulum';
			render("pelajaran_tematik/form_tambah", $data);
		}
		
		public function edit($id){
			$data['tingkat'] = $this->m_pelajaran_tematik->get_all_tingkat(get_jenjang_sekolah());
			$row = $this->m_pelajaran_tematik->get_tematik_by($id);
			$data['id'] = $id;
			$data['kode'] = $row->kode_pelajaran_tematik;
			$data['tema'] = $row->pelajaran_tematik;
			$data['alokasi'] = $row->alokasi_waktu;
			$data['tingkat_kelas'] = $row->id_tingkat_kelas;
			
			$data['action'] = 'edit';
			
			$data['component'] = 'kurikulum';
			render("pelajaran_tematik/form_tambah", $data);
		}
		
		public function submit(){
			$this->form_validation->set_rules('id', 'Id', 'required');
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('tema', 'Tema', 'required');
			$this->form_validation->set_rules('alokasi_waktu', 'Alokasi', 'required');
			$this->form_validation->set_rules('id_tingkat_kelas', 'Tingkat', 'required');
			
			if($this->form_validation->run() == TRUE){
				$id = $this->input->post('id');
				$kode = $this->input->post('kode');
				$tema = $this->input->post('tema');
				$alokasi = $this->input->post('alokasi_waktu');
				$tingkat = $this->input->post('id_tingkat_kelas');
				$id_sekolah = get_id_sekolah();
				
				$action = $this->input->post('action');
				if($action == 'add'){
					$this->m_pelajaran_tematik->insert($kode, $tema, $alokasi, $tingkat, $id_sekolah);
					set_success_message('Data Berhasil Ditambah!');
					redirect("kurikulum/pelajaran_tematik/home/");
				}else{
					$this->m_pelajaran_tematik->update($id, $kode, $tema, $alokasi, $tingkat, $id_sekolah);
					set_success_message('Data Berhasil Diupdate!');
					redirect("kurikulum/pelajaran_tematik/home/");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian!');
				redirect("kurikulum/pelajaran_tematik/home");
			}
		}
		
		public function delete(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kurikulum/pelajaran_tematik/home');
			}
			
			$this->m_pelajaran_tematik->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('kurikulum/pelajaran_tematik/home/');
		}
		
}
