<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class ketersediaan_silabus_tematik extends CI_Controller {

	public function index()
	{	
		$this->load->model('kurikulum/m_ketersediaan_silabus_tematik');
		$id_sekolah=get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		$data['jenis_kelas']= $this->m_ketersediaan_silabus_tematik->get_jenis_kelas($jenjang_sekolah)->result_array();
		$data['pelajaran']= $this->m_ketersediaan_silabus_tematik->select_pelajaran()->result_array();
		$data['silabus'] = $this->m_ketersediaan_silabus_tematik->get_silabus($id_sekolah)->result_array();
		
		$skr=array();
		foreach($data['silabus'] as $sil){
				$skr[$sil['pelajaran_tematik']][$sil['id_tingkat_kelas']]=1;
		}

		$data['hasil']=$skr;
		render('ketersediaan_silabus_tematik/ketersediaan_silabus_tematik',$data);
	}
	
	
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */