<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class ketersediaan_silabus extends CI_Controller {

	public function index()
	{	
		$this->load->model('kurikulum/m_ketersediaan_silabus');
		$data['kurtilas'] = $this->m_ketersediaan_silabus->select_data()->result_array();	//kur 2013
		$data['ktsp'] = $this->m_ketersediaan_silabus->select_data2()->result_array();	//ktsp
		$jenjang_sekolah= get_jenjang_sekolah();
		$data['jenis_kelas']= $this->m_ketersediaan_silabus->get_jenis_kelas($jenjang_sekolah)->result_array();
		if($jenjang_sekolah==4){
		$data['jenis_peminatan']= $this->m_ketersediaan_silabus->get_jenis_peminatan_sma()->result_array();
		$data['pelajaran']= $this->m_ketersediaan_silabus->select_pelajaran_sma()->result_array();
		$data['pelajaran2']= $this->m_ketersediaan_silabus->select_pelajaran_sma2()->result_array();
		}elseif($jenjang_sekolah==3){
		$data['jenis_peminatan']= $this->m_ketersediaan_silabus->get_jenis_peminatan_smp()->result_array();
		$data['pelajaran']= $this->m_ketersediaan_silabus->select_pelajaran_smp()->result_array();
		$data['pelajaran2']= $this->m_ketersediaan_silabus->select_pelajaran_smp2()->result_array();
		}elseif($jenjang_sekolah==2){
		$data['jenis_peminatan']= $this->m_ketersediaan_silabus->get_jenis_peminatan_sd()->result_array();
		$data['pelajaran']= $this->m_ketersediaan_silabus->select_pelajaran_sd()->result_array();
		$data['pelajaran2']= $this->m_ketersediaan_silabus->select_pelajaran_sd2()->result_array();
		}
		
		$data['kur'] = $this->m_ketersediaan_silabus->get_all_kurikulum();
		$id_sekolah=get_id_sekolah();
		
		$data['silabus_kurtilas'] = $this->m_ketersediaan_silabus->get_silabus_kurtilas($id_sekolah)->result_array();
		$data['silabus_ktsp'] = $this->m_ketersediaan_silabus->get_silabus_ktsp($id_sekolah)->result_array();
		
	
		$skr=array();
		foreach($data['silabus_kurtilas'] as $sil){
				$skr[$sil['pelajaran']][$sil['id_tingkat_kelas']][$sil['id_peminatan']]=1;
		}
		
		$skt=array();
		foreach($data['silabus_ktsp'] as $sil){
				$skt[$sil['pelajaran']][$sil['id_tingkat_kelas']][$sil['id_peminatan']]=1;
		}
		
		$data['hasil']=$skr;
		$data['hasil2']=$skt;
	
		
		$data['silabus_ktsp'] = $this->m_ketersediaan_silabus->get_silabus_ktsp($id_sekolah)->result_array();
		
		$data['component'] = 'kurikulum';
		
		render('ketersediaan_silabus/ketersediaan_silabus',$data);
	}
	
	
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */