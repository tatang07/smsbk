<?php
class m_jadwal_pelajaran extends MY_Model {
    public function __construct(){
		parent::__construct();
	}

		public $table = "m_jadwal_pelajaran_detail";
		
		public function get_datalist($id_sekolah, $limit, $start){

			$this->db->limit($limit, $start);
			$this->db->select('*');
			$this->db->from('m_jadwal_pelajaran rp'); 
			
			$this->db->where('rp.status_aktif',1);
			$this->db->where('rp.id_sekolah',$id_sekolah);
			
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_datalist($id_sekolah){

			$this->db->select('*');
			$this->db->from('m_jadwal_pelajaran rp'); 
			
			$this->db->where('rp.status_aktif',1);
			$this->db->where('rp.id_sekolah',$id_sekolah);
			$query = $this->db->get();
					
			return $query->num_rows();
		}
		
		public function get_all_rombel(){
			
			$this->db->where('id_sekolah',get_id_sekolah());
			$this->db->where('id_kurikulum',get_id_kurikulum());
			$this->db->where('id_tahun_ajaran',get_id_tahun_ajaran());
			$query = $this->db->get('m_rombel');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_pelajaran(){
			$this->db->select('*');
			$this->db->from('m_pelajaran'); 
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_pelajaran_by($id){
			$this->db->select('*');
			$this->db->from('m_pelajaran'); 
			
			$this->db->where('id_peminatan =', $id);
			$this->db->group_by('pelajaran');
			// $this->db->group_by('jumlah_jam');
			
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_peminatan(){
			$this->db->select('*');
			$this->db->from('m_peminatan'); 
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_rombel_peminatan(){
			$this->db->select('*');
			$this->db->where('id_sekolah',get_id_sekolah()); 
			$this->db->where('id_tahun_ajaran',get_id_tahun_ajaran()); 
			$this->db->where('id_kurikulum',get_id_kurikulum()); 
			$this->db->from('m_rombel r'); 
			
			// $this->db->join('m_program p', 'r.id_program=p.id_program', 'left');
			$this->db->join('m_peminatan mp', 'mp.id_peminatan=r.id_program', 'left');
			
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_ruang_belajar(){
			$this->db->select('*');
			$this->db->from('m_ruang_belajar'); 
			$this->db->where('id_sekolah',get_id_sekolah());
			$query = $this->db->get();
			
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_rombel_by($id){
			$query = $this->db->query("SELECT * FROM m_rombel WHERE id_rombel='$id'");
			return $query->row();
		}
		
		public function get_rombel_by_nama($id){
			$query = $this->db->query("SELECT * FROM m_rombel WHERE rombel='$id'");
			return $query->row();
		}
		
		public function get_first_rombel(){
			$query = $this->db->query("SELECT * FROM m_rombel ORDER BY id_rombel ASC LIMIT 1;");
			return $query->row();
		}
		
		public function get_all_guru(){
			$this->db->select('*');
			$this->db->from('m_guru'); 
			$this->db->where('id_sekolah',get_id_sekolah());
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_kelompok_pelajaran(){
			$this->db->select('*');
			$this->db->from('m_kelompok_pelajaran'); 
			$this->db->where('id_kurikulum',get_id_kurikulum());
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_guru_matpel_rombel(){
			$this->db->select('*');
			$this->db->from('t_guru_matpel_rombel'); 
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_guru_matpel_rombel_by_rombel(){
			$this->db->select('*');
			$this->db->from('t_guru_matpel_rombel'); 
			$this->db->group_by('id_rombel'); 
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		
		public function get_kelompok_pelajaran(){
			$this->db->select('*');
			$this->db->from('m_kelompok_pelajaran kp'); 
			
			$this->db->join('m_pelajaran m', 'kp.id_kelompok_pelajaran=m.id_kelompok_pelajaran', 'left');
			
			$this->db->where('kp.id_kurikulum',get_id_kurikulum());
			
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_kelompok_guru($id){
			$query = $this->db
						->from('t_guru_matpel_rombel gm')
						->where('g.id_sekolah',get_id_sekolah())
						->where('gm.id_pelajaran',$id)
						->join('m_rombel r', 'gm.id_rombel=r.id_rombel')
						->join('m_guru g', 'gm.id_guru=g.id_guru')
						->get();
			
			if($query->num_rows() > 0)
				return $query->result();
				
			return false;
		}
		
		public function get_all_tingkat($jenjang){
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_guru_by($id){
			$query = $this->db->query("SELECT * FROM m_guru WHERE id_guru='$id'");
			return $query->row();
		}
		
		public function get_first_guru(){
			$query = $this->db->query("SELECT * FROM m_guru ORDER BY id_guru ASC LIMIT 1;");
			return $query->row();
		}
		
		public function get_jadwal_pelajaran_detail_kelas($id, $id_rombel, $id_sekolah){
			$this->db->select('*');
			$this->db->from('m_jadwal_pelajaran_detail jp'); 
			
			$this->db->join('m_ruang_belajar rb', 'rb.id_ruang_belajar=jp.id_ruang_belajar', 'left');
			// $this->db->join('m_jam_pelajaran j', 'j.id_jam_pelajaran=jp.id_jam_pelajaran', 'left');
			$this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=jp.id_guru_matpel_rombel', 'left');
			$this->db->join('m_pelajaran m', 'gm.id_pelajaran=m.id_pelajaran', 'left');
			$this->db->join('m_guru g', 'gm.id_guru=g.id_guru', 'left');
			
			$this->db->where('jp.id_jadwal_pelajaran',$id);
			$this->db->where('gm.id_rombel',$id_rombel);
			// $this->db->where('j.id_sekolah',$id_sekolah);
			$this->db->where('rb.id_sekolah',$id_sekolah);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_jadwal_pelajaran_detail_guru($id, $id_guru, $id_sekolah){
			$this->db->select('*');
			$this->db->from('m_jadwal_pelajaran_detail jp'); 
			
			$this->db->join('m_ruang_belajar rb', 'rb.id_ruang_belajar=jp.id_ruang_belajar', 'left');
			// $this->db->join('m_jam_pelajaran j', 'j.id_jam_pelajaran=jp.id_jam_pelajaran', 'left');
			$this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=jp.id_guru_matpel_rombel', 'left');
			$this->db->join('m_pelajaran m', 'gm.id_pelajaran=m.id_pelajaran', 'left');
			$this->db->join('m_rombel r', 'gm.id_rombel=r.id_rombel', 'left');
			
			$this->db->where('jp.id_jadwal_pelajaran',$id);
			$this->db->where('gm.id_guru',$id_guru);
			// $this->db->where('j.id_sekolah',$id_sekolah);
			$this->db->where('rb.id_sekolah',$id_sekolah);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_guru_matpel_rombel_by($kode_guru, $kode_pelajaran, $rombel){
			$this->db->select('gm.id_guru_matpel_rombel, r.rombel, m.kode_pelajaran, g.kode_guru');
			$this->db->from('t_guru_matpel_rombel gm'); 
			$this->db->join('m_pelajaran m', 'm.id_pelajaran=gm.id_pelajaran'); 
			$this->db->join('m_guru g', 'g.id_guru=gm.id_guru'); 
			$this->db->join('m_rombel r', 'r.id_rombel=gm.id_rombel'); 
			
			$this->db->where('g.kode_guru',$kode_guru);
			$this->db->where('m.kode_pelajaran',$kode_pelajaran);
			$this->db->where('r.rombel',$rombel);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_ruang_belajar_by($kode_ruang, $id_sekolah){
			$this->db->select('*');
			$this->db->from('m_ruang_belajar'); 
			
			$this->db->where('kode_ruang_belajar',$kode_ruang);
			$this->db->where('id_sekolah',$id_sekolah);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_jam_pelajaran_by($mulai, $selesai, $id_sekolah){
			$this->db->select('*');
			$this->db->from('m_jam_pelajaran'); 
			
			$this->db->where('mulai',$mulai);
			$this->db->where('selesai',$selesai);
			$this->db->where('id_sekolah',$id_sekolah);
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_last_jadwal_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_jadwal_pelajaran ORDER BY id_jadwal_pelajaran DESC LIMIT 1;");
			return $query->row();
		}
		
		public function insert_jadwal($nama, $status_aktif, $id_sekolah){
			$this->db->query("INSERT INTO m_jadwal_pelajaran(nama, status_aktif, tanggal_dibuat, id_sekolah) VALUES ('$nama', '$status_aktif', now(), '$id_sekolah');");
		}
		
		public function insert_ruang_belajar($ruang, $kode, $id_sekolah){
			$this->db->query("INSERT INTO m_ruang_belajar(ruang_belajar, kode_ruang_belajar, id_sekolah) VALUES ('$ruang', '$kode', '$id_sekolah');");
			$last_id = $this->db->insert_id();
			return $last_id;
		}
		
		public function delete($id){
			$this->db->delete('m_jadwal_pelajaran_detail', array('id_jadwal_pelajaran'=>$id));
			$this->db->delete('m_jadwal_pelajaran', array('id_jadwal_pelajaran'=>$id));
		}
}