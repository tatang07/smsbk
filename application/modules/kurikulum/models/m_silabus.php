<?php
class m_silabus extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_datalist($id_kurikulum, $id_pelajaran, $id_sekolah, $limit, $start){

			$this->db->limit($limit, $start);
			$this->db->select('*');
			$this->db->from('m_silabus'); 
			
			$this->db->where('id_kurikulum',$id_kurikulum);
			$this->db->where('id_pelajaran',$id_pelajaran);
			$this->db->where('id_sekolah',$id_sekolah);
			// $this->db->group_by('s.nis');         
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_datalist($id_kurikulum, $id_pelajaran, $id_sekolah){

			$this->db->select('*');
			$this->db->from('m_silabus'); 
			
			$this->db->where('id_kurikulum',$id_kurikulum);
			$this->db->where('id_pelajaran',$id_pelajaran);
			$this->db->where('id_sekolah',$id_sekolah);
			$query = $this->db->get();
					
					
			return $query->num_rows();
			
		}
		
		public function get_all_tingkat($jenjang){
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_kurikulum(){
			$query = $this->db->get('m_kurikulum');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_kurikulum_by($id){
			$query = $this->db->query("SELECT * FROM m_kurikulum WHERE id_kurikulum='$id'");
			return $query->row();
		}
		
		public function get_kelompok_pelajaran_by($id){
			$query = $this->db->query("SELECT * FROM m_kelompok_pelajaran WHERE id_kelompok_pelajaran='$id'");
			return $query->row();
		}
		
		public function get_silabus_by($id){
			$query = $this->db->query("SELECT * FROM m_silabus WHERE id_silabus='$id'");
			return $query->row();
		}
		
		public function get_first_kurikulum(){
			$query = $this->db->query("SELECT * FROM m_kurikulum ORDER BY id_kurikulum DESC LIMIT 1;");
			return $query->row();
		}
		
		public function get_all_pelajaran(){
			$query = $this->db
						->from('m_pelajaran p')
						->join('m_kelompok_pelajaran kp', 'kp.id_kelompok_pelajaran=p.id_kelompok_pelajaran')
						->join('m_tingkat_kelas t', 't.id_tingkat_kelas=p.id_tingkat_kelas')
						->where('t.id_jenjang_sekolah', get_jenjang_sekolah())
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_kelompok_pelajaran(){
			$query = $this->db->get('m_kelompok_pelajaran');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_pelajaran_by($id){
			$query = $this->db->query("SELECT * FROM m_pelajaran WHERE id_pelajaran='$id'");
			return $query->row();
		}
		
		public function get_pelajaran_by_kelompok($id){
			$this->db->where('id_kelompok_pelajaran',$id);
			$this->db->order_by('id_tingkat_kelas asc');
			$query = $this->db->get('m_pelajaran');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_first_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_pelajaran ORDER BY id_pelajaran ASC LIMIT 1;");
			return $query->row();
		}
		
		public function delete($id){
			$this->db->delete('m_silabus', array('id_silabus'=>$id));
		}
		
		public function insert($nama_silabus, $lokasi_file, $id_pelajaran, $id_sekolah, $id_term){
			$this->db->query("INSERT INTO m_silabus(nama_silabus, lokasi_file, id_pelajaran, id_sekolah, id_kurikulum) VALUES ('$nama_silabus', '$lokasi_file', '$id_pelajaran', '$id_sekolah', '$id_term')");
		}
}