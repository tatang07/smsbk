<?php
class m_dokumen_kurikulum extends MY_Model {
    public function __construct(){
		parent::__construct();
	}

	public function get_all_kurikulum(){
		$query = $this->db->query('SELECT nama_kurikulum from m_kurikulum');
		return $query->result();
	}	

	public function get_all_data(){
		$id=get_id_sekolah();
		$query = $this->db->query("SELECT id_dokumen_kurikulum as id, nama_dokumen as nama_doc, NULL as kode_pelajaran, NULL as pelajaran, NULL as lokasi, 'lainnya' as jenis_dokumen FROM m_dokumen_kurikulum where id_sekolah='$id' UNION
								SELECT m_silabus.id_silabus as id, m_silabus.nama_silabus as nama_doc, m_pelajaran.kode_pelajaran as kode_pelajaran, m_pelajaran.pelajaran as pelajaran, m_silabus.lokasi_file as lokasi, 'silabus' as jenis_dokumen FROM m_silabus LEFT JOIN m_pelajaran ON m_silabus.id_pelajaran = m_pelajaran.id_pelajaran where m_silabus.id_sekolah='$id' UNION
								SELECT t_rpp.id_rpp as id, t_rpp.nama_rpp as nama_doc, m_pelajaran.kode_pelajaran as kode_pelajaran, m_pelajaran.pelajaran as pelajaran, t_rpp.lokasi_file as lokasi, 'rpp' as jenis_dokumen FROM t_rpp LEFT JOIN t_guru_matpel_rombel ON t_guru_matpel_rombel.id_guru_matpel_rombel=t_rpp.id_guru_matpel_rombel LEFT JOIN m_pelajaran ON t_guru_matpel_rombel.id_pelajaran=m_pelajaran.id_pelajaran LEFT JOIN m_guru ON t_guru_matpel_rombel.id_guru=m_guru.id_guru where m_guru.id_sekolah='$id'");
		return $query->result();
	}
	
	public function get_all_data_count(){
		$id=get_id_sekolah();
		$query = $this->db->query("SELECT id_dokumen_kurikulum as id, nama_dokumen as nama_doc, NULL as kode_pelajaran, NULL as pelajaran, NULL as lokasi, 'lainnya' as jenis_dokumen FROM m_dokumen_kurikulum where id_sekolah='$id' UNION
								SELECT m_silabus.id_silabus as id, m_silabus.nama_silabus as nama_doc, m_pelajaran.kode_pelajaran as kode_pelajaran, m_pelajaran.pelajaran as pelajaran, m_silabus.lokasi_file as lokasi, 'silabus' as jenis_dokumen FROM m_silabus LEFT JOIN m_pelajaran ON m_silabus.id_pelajaran = m_pelajaran.id_pelajaran where m_silabus.id_sekolah='$id' UNION
								SELECT t_rpp.id_rpp as id, t_rpp.nama_rpp as nama_doc, m_pelajaran.kode_pelajaran as kode_pelajaran, m_pelajaran.pelajaran as pelajaran, t_rpp.lokasi_file as lokasi, 'rpp' as jenis_dokumen FROM t_rpp LEFT JOIN t_guru_matpel_rombel ON t_guru_matpel_rombel.id_guru_matpel_rombel=t_rpp.id_guru_matpel_rombel LEFT JOIN m_pelajaran ON t_guru_matpel_rombel.id_pelajaran=m_pelajaran.id_pelajaran LEFT JOIN m_guru ON t_guru_matpel_rombel.id_guru=m_guru.id_guru where m_guru.id_sekolah='$id'");
		return $query->num_rows();
	}
	
	public function get_data_search($nama, $dokumen){
		$id=get_id_sekolah();
		$query = $this->db->query("SELECT id_dokumen_kurikulum as id, nama_dokumen as nama_doc, NULL as kode_pelajaran, NULL as pelajaran, NULL as lokasi, 'lainnya' as jenis_dokumen FROM m_dokumen_kurikulum where nama_dokumen like '%$nama%' and 'lainnya' = '$dokumen' and id_sekolah='$id' UNION
								SELECT m_silabus.id_silabus as id, m_silabus.nama_silabus as nama_doc, m_pelajaran.kode_pelajaran as kode_pelajaran, m_pelajaran.pelajaran as pelajaran, m_silabus.lokasi_file as lokasi, 'silabus' as jenis_dokumen FROM m_silabus LEFT JOIN m_pelajaran ON m_silabus.id_pelajaran = m_pelajaran.id_pelajaran where (m_silabus.nama_silabus like '%$nama%' or m_pelajaran.pelajaran like '%$nama%') and 'silabus' = '$dokumen' and m_silabus.id_sekolah='$id' UNION
								SELECT t_rpp.id_rpp as id, t_rpp.nama_rpp as nama_doc, m_pelajaran.kode_pelajaran as kode_pelajaran, m_pelajaran.pelajaran as pelajaran, t_rpp.lokasi_file as lokasi, 'rpp' as jenis_dokumen FROM t_rpp LEFT JOIN t_guru_matpel_rombel ON t_guru_matpel_rombel.id_guru_matpel_rombel=t_rpp.id_guru_matpel_rombel LEFT JOIN m_pelajaran ON t_guru_matpel_rombel.id_pelajaran=m_pelajaran.id_pelajaran LEFT JOIN m_guru ON t_guru_matpel_rombel.id_guru=m_guru.id_guru where (t_rpp.nama_rpp like '%$nama%' or m_pelajaran.pelajaran like '%$nama%') and 'rpp' = '$dokumen' and m_guru.id_sekolah='$id'");
		
		return $query->result();
	}
	
	
	public function get_data_search_count($nama, $dokumen){
		$id=get_id_sekolah();
		$query = $this->db->query("SELECT id_dokumen_kurikulum as id, nama_dokumen as nama_doc, NULL as kode_pelajaran, NULL as pelajaran, NULL as lokasi, 'lainnya' as jenis_dokumen FROM m_dokumen_kurikulum where nama_dokumen like '%$nama%' and 'lainnya' = '$dokumen' and id_sekolah='$id' UNION
								SELECT m_silabus.id_silabus as id, m_silabus.nama_silabus as nama_doc, m_pelajaran.kode_pelajaran as kode_pelajaran, m_pelajaran.pelajaran as pelajaran, m_silabus.lokasi_file as lokasi, 'silabus' as jenis_dokumen FROM m_silabus LEFT JOIN m_pelajaran ON m_silabus.id_pelajaran = m_pelajaran.id_pelajaran where (m_silabus.nama_silabus like '%$nama%' or m_pelajaran.pelajaran like '%$nama%') and 'silabus' = '$dokumen' and m_silabus.id_sekolah='$id' UNION
								SELECT t_rpp.id_rpp as id, t_rpp.nama_rpp as nama_doc, m_pelajaran.kode_pelajaran as kode_pelajaran, m_pelajaran.pelajaran as pelajaran, t_rpp.lokasi_file as lokasi, 'rpp' as jenis_dokumen FROM t_rpp LEFT JOIN t_guru_matpel_rombel ON t_guru_matpel_rombel.id_guru_matpel_rombel=t_rpp.id_guru_matpel_rombel LEFT JOIN m_pelajaran ON t_guru_matpel_rombel.id_pelajaran=m_pelajaran.id_pelajaran LEFT JOIN m_guru ON t_guru_matpel_rombel.id_guru=m_guru.id_guru where (t_rpp.nama_rpp like '%$nama%' or m_pelajaran.pelajaran like '%$nama%') and 'rpp' = '$dokumen' and m_guru.id_sekolah='$id'");
		return $query->num_rows();
	}
	
	public function get_file_by($id){
		$query = $this->db->query("SELECT * FROM m_dokumen_kurikulum WHERE id_dokumen_kurikulum='$id'");
		return $query->row();
	}
	
	public function get_rpp_by($id){
		$query = $this->db->query("SELECT * FROM t_rpp WHERE id_rpp='$id'");
		return $query->row();
	}
	
	public function get_silabus_by($id){
		$query = $this->db->query("SELECT * FROM m_silabus WHERE id_silabus='$id'");
		return $query->row();
	}
	
	public function delete_rpp($id){
		$this->db->delete('t_rpp', array('id_rpp'=>$id));
	}
	
	public function delete_silabus($id){
		$this->db->delete('m_silabus', array('id_silabus'=>$id));
	}
	
	public function delete_file($id){
			$this->db->delete('m_dokumen_kurikulum', array('id_dokumen_kurikulum'=>$id));
		}
}