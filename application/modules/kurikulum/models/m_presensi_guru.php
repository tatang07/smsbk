<?php
class m_presensi_guru extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_datalist($id, $limit, $start){
			$this->db->limit($limit, $start);
			$this->db->select('*, count(a.materi) as pertemuan');
			$this->db->from('t_guru_matpel_rombel mr'); 
			$this->db->join('m_pelajaran p', 'p.id_pelajaran=mr.id_pelajaran', 'left');
			$this->db->join('m_guru g', 'g.id_guru=mr.id_guru', 'left');
			$this->db->join('m_rombel r', 'r.id_rombel=mr.id_rombel', 'left');
			$this->db->join('t_agenda_kelas a', 'a.id_guru_matpel_rombel=mr.id_guru_matpel_rombel', 'left');
			$this->db->where('g.id_guru',$id);
			$this->db->group_by('p.kode_pelajaran');         
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_datalist($id){
			$this->db->select('*, count(a.materi) as pertemuan');
			$this->db->from('t_guru_matpel_rombel mr'); 
			$this->db->join('m_pelajaran p', 'p.id_pelajaran=mr.id_pelajaran', 'left');
			$this->db->join('m_guru g', 'g.id_guru=mr.id_guru', 'left');
			$this->db->join('m_rombel r', 'r.id_rombel=mr.id_rombel', 'left');
			$this->db->join('t_agenda_kelas a', 'a.id_guru_matpel_rombel=mr.id_guru_matpel_rombel', 'left');
			$this->db->where('g.id_guru',$id);
			$this->db->group_by('p.kode_pelajaran');         
			$query = $this->db->get();
					
			return $query->num_rows();
			
		}
		
		public function get_all_guru(){
			$this->db->from('m_guru'); 
			$this->db->where('id_sekolah',get_id_sekolah());
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				 return $query->result();
			}
			
			return false;
		}
		
		public function get_guru_by($id){
			$query = $this->db->query("SELECT * FROM m_guru WHERE id_guru='$id'");
			return $query->row();
		}
		
		public function get_first_guru(){
			$query = $this->db->query("SELECT * FROM m_guru ORDER BY id_guru ASC LIMIT 1;");
			return $query->row();
		}
		
		public function get_all_rombel(){
			$query = $this->db->get('m_rombel');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_guru_matpel_rombel(){
			$query = $this->db->get('m_rombel');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_agenda_kelas(){
			$query = $this->db->get('t_agenda_kelas');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_pelajaran(){
			$query = $this->db->get('m_pelajaran');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
}