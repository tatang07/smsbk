<?php
class m_pelajaran extends MY_Model {
    public $table = "m_pelajaran";
    public $id = "id_pelajaran";
	
	//join key
	public $join_to = array(
							"m_kelompok_pelajaran b"=>"b.id_kelompok_pelajaran=m_pelajaran.id_kelompok_pelajaran",
							"r_kualifikasi_guru c"=>"c.id_kualifikasi_guru=t_kualifikasi_guru_pelajaran.id_kualifikasi_guru",
					  );
	
	//join field
	public $join_fields = array(
							"b.kelompok_pelajaran kelompok_pelajaran",
							"c.kualifikasi_guru kualifikasi_guru",
						  );
	
}