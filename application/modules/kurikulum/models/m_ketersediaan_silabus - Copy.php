<?php
class m_ketersediaan_silabus extends MY_Model {

	function insert_isi($data){
			$this->db->insert($this->table, $data);
	}
	// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_peminatan as id_program,id_peminatan FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran group by p.id_pelajaran ,id_tingkat_kelas, id_peminatan
	function select_data(){
	
			$this->db->select('id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas, id_peminatan as id_program,id_peminatan'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_silabus s');
			//$this->db->join('m_mata_pelajaran mp', 'mp.id_mata_pelajaran = s.id_mata_pelajaran');
			$this->db->join('m_pelajaran p', 's.id_pelajaran = p.id_pelajaran');
			$this->db->where('s.id_kurikulum', 2);
			$this->db->group_by("p.id_pelajaran ,id_tingkat_kelas, id_peminatan,pelajaran");
			return $this->db->get();
		}
	function select_data2(){
	
			$this->db->select('id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas, id_peminatan as id_program,id_peminatan'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_silabus s');
			//$this->db->join('m_mata_pelajaran mp', 'mp.id_mata_pelajaran = s.id_mata_pelajaran');
			$this->db->join('m_pelajaran p', 's.id_pelajaran = p.id_pelajaran');
			$this->db->where('s.id_kurikulum', 1);
			$this->db->group_by("p.id_pelajaran ,id_tingkat_kelas, id_peminatan,pelajaran");
			return $this->db->get();
		}
	function select_pelajaran(){
	
			$this->db->select('*'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_pelajaran');
			$this->db->group_by("pelajaran");
			return $this->db->get();
	}
	function select_silabus(){
	
			$this->db->select('*'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_silabus');
			return $this->db->get();
	}
	function select_program(){
	
			/*$this->db->select('*'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_program');*/
			$this->db->select('id_peminatan as id_program, peminatan_singkatan as program'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_peminatan');
			return $this->db->get();
	}
	function select_kelas(){
	
			$this->db->select('*'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_tingkat_kelas');
			$this->db->where(array('id_jenjang_sekolah'=>get_jenjang_sekolah()));
			return $this->db->get();
	}
	
	public function get_all_kurikulum(){
			$query = $this->db
						->from('m_kurikulum')
						->order_by('id_kurikulum desc')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
	}
	// function get_jumlah_jam($id_pel,$pel){
			// $this->db->select('*');
			// $this->db->from('m_alokasi_waktu_per_minggu awpm');
		
			// $this->db->where_in('awpm.id_tingkat_kelas', $pel );
			// $this->db->where('awpm.id_pelajaran',$id_pel);
			// return $this->db->get()->result_array();
	// }
	
	// function hitung_beban(){
			// $this->db->select('*');
			// $this->db->from('m_alokasi_waktu_per_minggu awpm');
			// $this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_pelajaran = awpm.id_pelajaran');
			// return $this->db->get()->result_array();
	// }

}