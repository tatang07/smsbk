<?php
class m_kompetensi_inti extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public $table = "m_kompetensi_inti";
		
		public function get_all_kompetensi(){
			$query = $this->db->get('m_kompetensi_inti');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_tingkat($jenjang){
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_tingkat($jenjang){
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			return $query->num_rows();
		}
		
		public function get_all_kurikulum(){
			$query = $this->db
						->from('m_kurikulum')
						->order_by('id_kurikulum desc')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_last_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_pelajaran ORDER BY id_pelajaran DESC LIMIT 1;");
			return $query->row();
		}
		
		public function get_all_peminatan(){
			$query = $this->db->get('m_peminatan');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_pelajaran(){
			$query = $this->db
						->from('m_pelajaran p')
						->join('m_kelompok_pelajaran kp', 'kp.id_kelompok_pelajaran=p.id_kelompok_pelajaran')
						// ->order_by('id_kurikulum desc')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_kompetensi_inti_by($id){
			$query = $this->db->query("SELECT * FROM m_kompetensi_inti WHERE id_kompetensi_inti='$id'");
			return $query->row();
		}
		
		public function update($id_kompetensi_inti, $kode_kompetensi_inti, $kompetensi_inti, $id_kurikulum, $id_tingkat_kelas){
			$data=array(
				'kode_kompetensi_inti'=> $kode_kompetensi_inti,
				'kompetensi_inti'=> $kompetensi_inti,
				'id_kurikulum'=> $id_kurikulum,
				'id_tingkat_kelas'=> $id_tingkat_kelas
			);
			
			$this->db->where('id_kompetensi_inti', $id_kompetensi_inti);
			$this->db->update('m_kompetensi_inti', $data);
		}
		
		public function delete($id){
			$this->db->delete('m_kompetensi_inti', array('id_kompetensi_inti'=>$id));
		}
		
		// --- proses untuk Standar Kompetensi --- //
		
		public function get_all_data_sk(){
			$query = $this->db
						->from('m_standar_kompetensi s')
						->join('m_pelajaran p', 'p.id_pelajaran = s.id_pelajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_data_sk($id_tingkat_kelas, $id_pelajaran){
			if($id_tingkat_kelas == -1){
				$query = $this->db
							->from('m_standar_kompetensi s')
							->join('m_pelajaran p', 'p.id_pelajaran = s.id_pelajaran')
							->where('p.id_pelajaran', $id_pelajaran)
							->get();
			}elseif($id_pelajaran == -1){
				$query = $this->db
							->from('m_standar_kompetensi s')
							->join('m_pelajaran p', 'p.id_pelajaran = s.id_pelajaran')
							->where('p.id_tingkat_kelas', $id_tingkat_kelas)
							->get();
			}else{
				$query = $this->db
							->from('m_standar_kompetensi s')
							->join('m_pelajaran p', 'p.id_pelajaran = s.id_pelajaran')
							->where('p.id_tingkat_kelas', $id_tingkat_kelas)
							->where('p.id_pelajaran', $id_pelajaran)
							->get();
			}
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function delete_sk($id){
			$this->db->delete('m_kompetensi_dasar', array('id_standar_kompetensi'=>$id));
			$this->db->delete('m_standar_kompetensi', array('id_standar_kompetensi'=>$id));
		}
		
		public function add_sk($standar_kompetensi, $id_pelajaran){
			$data=array(
				'standar_kompetensi'=> $standar_kompetensi,
				'id_pelajaran'=> $id_pelajaran
			);
			
			$this->db->insert('m_standar_kompetensi', $data);
		}
		
		public function update_sk($id_standar_kompetensi, $standar_kompetensi, $id_pelajaran){
			$data=array(
				'standar_kompetensi'=> $standar_kompetensi,
				'id_pelajaran'=> $id_pelajaran
			);
			
			$this->db->where('id_standar_kompetensi', $id_standar_kompetensi);
			$this->db->update('m_standar_kompetensi', $data);
		}
		
		public function get_standar_kompetensi_by($id){
			$query = $this->db->query("SELECT * FROM m_standar_kompetensi WHERE id_standar_kompetensi='$id'");
			return $query->row();
		}
		
		// --- proses untuk Kompetensi Dasar --- //
		
		public function get_all_standar_kompetensi(){
			$query = $this->db->get('m_standar_kompetensi');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_pelajaran_by($id){
			$query = $this->db
						->from('m_pelajaran p')
						->join('m_tingkat_kelas t', 'p.id_tingkat_kelas = t.id_tingkat_kelas')
						->where('p.id_pelajaran', $id)
						->get();
			return $query->row();
		}
		
		public function get_kompetensi_dasar_by($id){
			$query = $this->db
						->from('m_kompetensi_dasar')
						->where('id_standar_kompetensi', $id)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_kd_by($id){
			$query = $this->db->query("SELECT * FROM m_kompetensi_dasar WHERE id_kompetensi_dasar='$id'");
			return $query->row();
		}
		
		public function delete_kd($id){
			$this->db->delete('m_kompetensi_dasar', array('id_kompetensi_dasar'=>$id));
		}
		
		public function add_kd($kompetensi_dasar, $id_standar_kompetensi){
			$data=array(
				'kompetensi_dasar'=> $kompetensi_dasar,
				'id_standar_kompetensi'=> $id_standar_kompetensi
			);
			
			$this->db->insert('m_kompetensi_dasar', $data);
		}
		
		public function update_kd($id_kompetensi_dasar, $kompetensi_dasar, $id_standar_kompetensi){
			$data=array(
				'kompetensi_dasar'=> $kompetensi_dasar,
				'id_standar_kompetensi'=> $id_standar_kompetensi
			);
			
			$this->db->where('id_kompetensi_dasar', $id_kompetensi_dasar);
			$this->db->update('m_kompetensi_dasar', $data);
		}
}