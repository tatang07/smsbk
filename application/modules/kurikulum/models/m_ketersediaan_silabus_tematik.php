<?php
class m_ketersediaan_silabus_tematik extends MY_Model {
	
	function select_data(){
	
			$this->db->select('id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas, id_peminatan as id_program,id_peminatan'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_silabus s');
			$this->db->join('m_pelajaran p', 's.id_pelajaran = p.id_pelajaran');
			$this->db->where('s.id_kurikulum', 2);
			$this->db->group_by("p.id_pelajaran ,id_tingkat_kelas, id_peminatan,pelajaran");
			return $this->db->get();
		}
	function select_data2(){
	
			$this->db->select('id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas, id_peminatan as id_program,id_peminatan'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_silabus s');
			$this->db->join('m_pelajaran p', 's.id_pelajaran = p.id_pelajaran');
			$this->db->where('s.id_kurikulum', 1);
			$this->db->group_by("p.id_pelajaran ,id_tingkat_kelas, id_peminatan,pelajaran");
			return $this->db->get();
		}
	
	function get_jenis_kelas($id){
	
			$this->db->select('*'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_tingkat_kelas');
			$this->db->where('id_jenjang_sekolah', $id);
			return $this->db->get();
		}
	
	function select_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_pelajaran_tematik GROUP BY pelajaran_tematik");
			return $query;
	}

	function get_silabus($id_sekolah){
			$query = $this->db->query("SELECT * FROM m_silabus_tematik as s JOIN m_pelajaran_tematik as p ON s.id_pelajaran_tematik=p.id_pelajaran_tematik WHERE p.id_sekolah=$id_sekolah");
			return $query;
	}
	
}