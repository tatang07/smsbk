<?php
class t_ekstra_kurikuler_peserta extends MY_Model {
    public $table = "t_ekstra_kurikuler_peserta";
    public $id = "id_ekstra_kurikuler_peserta";

    //create your model here
    public function get_all_ekstra_kurikuler_peserta(){
    	$query = $this->db->query(
					"SELECT
					t_ekstra_kurikuler_peserta.id_siswa AS id_siswa,
					t_ekstra_kurikuler_peserta.id_ekstra_kurikuler AS id_ekstra_kurikuler,
					m_rombel.rombel AS rombel,
					m_siswa.nama AS nama,
					m_siswa.nis AS nis,
					m_ekstra_kurikuler.nama_ekstra_kurikuler AS nama_ekstra_kurikuler
					from ((((
					`m_siswa` join `t_ekstra_kurikuler_peserta` on((`t_ekstra_kurikuler_peserta`.`id_siswa` = `m_siswa`.`id_siswa`))) 
					join `m_ekstra_kurikuler` on((`m_ekstra_kurikuler`.`id_ekstra_kurikuler` = `t_ekstra_kurikuler_peserta`.`id_ekstra_kurikuler`))) 
					join `t_rombel_detail` on((`m_siswa`.`id_siswa` = `t_rombel_detail`.`id_siswa`))) 
					join `m_rombel` on((`m_rombel`.`id_rombel` = `t_rombel_detail`.`id_rombel`)));"
				);

		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
    }

    public function get_search_ekstra_kurikuler_peserta($data){
    	$query = $this->db->query(
					"SELECT
					t_ekstra_kurikuler_peserta.id_siswa AS id_siswa,
					t_ekstra_kurikuler_peserta.id_ekstra_kurikuler AS id_ekstra_kurikuler,
					m_rombel.rombel AS rombel,
					m_siswa.nama AS nama,
					m_siswa.nis AS nis,
					m_ekstra_kurikuler.nama_ekstra_kurikuler AS nama_ekstra_kurikuler
					from ((((
					`m_siswa` join `t_ekstra_kurikuler_peserta` on((`t_ekstra_kurikuler_peserta`.`id_siswa` = `m_siswa`.`id_siswa`))) 
					join `m_ekstra_kurikuler` on((`m_ekstra_kurikuler`.`id_ekstra_kurikuler` = `t_ekstra_kurikuler_peserta`.`id_ekstra_kurikuler`))) 
					join `t_rombel_detail` on((`m_siswa`.`id_siswa` = `t_rombel_detail`.`id_siswa`))) 
					join `m_rombel` on((`m_rombel`.`id_rombel` = `t_rombel_detail`.`id_rombel`)))
    				where 
    				t_ekstra_kurikuler_peserta.id_ekstra_kurikuler = $data[id_ekstra_kurikuler];"
				);

		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
    }

	public function get_ekstra_kurikuler_peserta_by_id($id){
		$query = $this->db->query
			("SELECT * FROM t_ekstra_kurikuler_peserta WHERE id_ekstra_kurikuler_peserta='$id'");

		return $query->row();
	}
  
    public function set_ekstra_kurikuler_peserta($data){
		$this->db->insert('t_ekstra_kurikuler_peserta', $data);
	}

    public function update_ekstra_kurikuler_peserta($data){
		$this->db->where('id_ekstra_kurikuler_peserta', $data['id_ekstra_kurikuler_peserta']);
		$this->db->update('t_ekstra_kurikuler_peserta', $data); 
	}

	public function delete_ekstra_kurikuler_peserta($id){
		$this->db->where('id_ekstra_kurikuler_peserta', $id);
		$this->db->delete('t_ekstra_kurikuler_peserta');
	}

    public function get_all_rombel(){
		$query = $this->db->get('m_rombel');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
		
	public function get_rombel_by($id){
		$query = $this->db->query
			("SELECT * FROM m_rombel WHERE id_rombel='$id'");
		return $query->row();
	}
	
	public function get_first_rombel(){
		$query = $this->db->query
			("SELECT * FROM m_rombel ORDER BY id_rombel ASC LIMIT 1;");
		return $query->row();
	}

	public function get_all_ekstra_kurikuler(){
		$query = $this->db->get('m_ekstra_kurikuler');

		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_ekstra_kurikuler_by($id){
		$query = $this->db->query
			("SELECT * FROM m_ekstra_kurikuler WHERE id_ekstra_kurikuler='$id'");
		return $query->row();
	}
	
	public function get_first_ekstra_kurikuler(){
		$query = $this->db->query
			("SELECT * FROM m_ekstra_kurikuler ORDER BY id_ekstra_kurikuler ASC LIMIT 1;");
		return $query->row();
	}

    public function get_all_guru(){
		$query = $this->db->get('m_guru');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}

    public function get_all_term(){
		$query = $this->db->get('m_term');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}

    public function set_guru_matpel_rombel($data){
		$this->db->insert('t_guru_matpel_rombel', $data);

		return $this->db->insert_id();
	}

}