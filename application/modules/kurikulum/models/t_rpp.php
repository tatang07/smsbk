<?php
class t_rpp extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_datalist($id_gmr, $id_term, $limit, $start){

			$this->db->limit($limit, $start);
			$this->db->select('*');
			$this->db->from('t_rpp rp'); 
			
			$this->db->join('m_term t', 't.id_term=rp.id_term', 'left');
			$this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=rp.id_guru_matpel_rombel', 'left');
			
			$this->db->where('rp.id_term',$id_term);
			$this->db->where('gm.id_guru_matpel_rombel',$id_gmr);
			
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_datalist_ketersediaan($id_gmr, $id_term, $limit, $start){

			$this->db->limit($limit, $start);
			$this->db->select('*, count(gm.id_guru_matpel_rombel) as jumlah');
			$this->db->from('t_rpp rp'); 
			
			$this->db->join('m_term t', 't.id_term=rp.id_term', 'left');
			$this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=rp.id_guru_matpel_rombel', 'left');
			$this->db->join('m_pelajaran p', 'p.id_pelajaran=gm.id_pelajaran', 'left');
			$this->db->join('m_rombel r', 'r.id_rombel=gm.id_rombel', 'left');
			$this->db->join('m_guru g', 'g.id_guru=gm.id_guru', 'left');
			
			$this->db->where('rp.id_term',$id_term);
			$this->db->where('gm.id_pelajaran',$id_gmr);
			$this->db->group_by('gm.id_guru, gm.id_rombel');
			
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_datalist_ketersediaan($id_gmr, $id_term){

			$this->db->select('*');
			$this->db->from('t_rpp rp'); 
			
			$this->db->join('m_term t', 't.id_term=rp.id_term', 'left');
			$this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=rp.id_guru_matpel_rombel', 'left');
			$this->db->join('m_pelajaran p', 'p.id_pelajaran=gm.id_pelajaran', 'left');
			$this->db->join('m_guru g', 'g.id_guru=gm.id_guru', 'left');
			
			$this->db->where('rp.id_term',$id_term);
			$this->db->where('gm.id_pelajaran',$id_gmr);
			$this->db->group_by('gm.id_guru, gm.id_rombel');
			
			$query = $this->db->get();
					
					
			return $query->num_rows();
			
		}
		
		public function count_datalist($id_gmr, $id_term){

			$this->db->select('*');
			$this->db->from('t_rpp rp'); 
			
			$this->db->join('m_term t', 't.id_term=rp.id_term', 'left');
			$this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=rp.id_guru_matpel_rombel', 'left');
			
			$this->db->where('rp.id_term',$id_term);
			$this->db->where('gm.id_guru_matpel_rombel',$id_gmr);
			
			$query = $this->db->get();
					
					
			return $query->num_rows();
			
		}
		
		public function get_all_rombel(){
			
			$this->db->where('id_sekolah',get_id_sekolah());
			$this->db->where('id_tahun_ajaran',get_id_tahun_ajaran());
			$this->db->order_by('rombel','desc');
			$query = $this->db->get('m_rombel');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_rombel_by($id){
			$query = $this->db->query("SELECT * FROM m_rombel WHERE id_rombel='$id'");
			return $query->row();
		}
		
		public function get_first_rombel(){
			$query = $this->db->query("SELECT * FROM m_rombel ORDER BY id_rombel ASC LIMIT 1;");
			return $query->row();
		}
		
		public function get_all_agenda_kelas(){
			$query = $this->db->get('t_agenda_kelas');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_term(){
			$query = $this->db->get('m_term');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_kurikulum(){
			$query = $this->db->get('m_kurikulum');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_tingkat(){
			$this->db->join('r_jenjang_sekolah j', 'j.id_jenjang_sekolah=k.id_jenjang_sekolah');
			$this->db->where('j.id_jenjang_sekolah', get_jenjang_sekolah());
			$query = $this->db->get('m_tingkat_kelas k');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_guru_matpel_rombel(){
			$query = $this->db->get('t_guru_matpel_rombel');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_guru(){
			$query = $this->db->get('m_guru');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_guru_matpel($id, $id_rombel){
			$this->db->where('id_pelajaran',$id);
			$this->db->where('id_rombel',$id_rombel);
			$this->db->join('m_guru p', 'p.id_guru=g.id_guru', 'left');
			$query = $this->db->get('t_guru_matpel_rombel g');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_matpel_rombel($id){
			$this->db->where('id_rombel',$id);
			$this->db->join('m_pelajaran p', 'p.id_pelajaran=g.id_pelajaran', 'left');
			$query = $this->db->get('t_guru_matpel_rombel g');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_guru_matpel_rombel($id_guru, $id_pelajaran, $id_rombel){
			$query = $this->db->query("SELECT * FROM t_guru_matpel_rombel WHERE id_guru = '$id_guru' and id_pelajaran='$id_pelajaran' and id_rombel = '$id_rombel'");
			return $query->row();
		}
		
		public function get_all_pelajaran(){
			$query = $this->db->get('m_pelajaran');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_pelajaran_by($id){
			$query = $this->db->query("SELECT * FROM m_pelajaran WHERE id_pelajaran='$id'");
			return $query->row();
		}
		
		public function get_gmr_by($id){
			$query = $this->db->query("SELECT * FROM t_guru_matpel_rombel WHERE id_guru_matpel_rombel='$id'");
			return $query->row();
		}
		
		public function get_rpp_by($id){
			$query = $this->db->query("SELECT * FROM t_rpp WHERE id_rpp='$id'");
			return $query->row();
		}
		
		public function get_first_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_pelajaran ORDER BY id_pelajaran ASC LIMIT 1;");
			return $query->row();
		}
		
		public function delete($id){
			$this->db->delete('t_rpp', array('id_rpp'=>$id));
		}
		
		public function insert($nama_rpp, $pertemuan_ke, $lokasi_file, $id_guru_matpel_rombel, $id_term){
			$this->db->query("INSERT INTO t_rpp(nama_rpp, pertemuan_ke, lokasi_file, id_guru_matpel_rombel, id_term) VALUES ('$nama_rpp', '$pertemuan_ke', '$lokasi_file', '$id_guru_matpel_rombel', '$id_term');");
		}
}