<?php
	class m_penentuan_guru_matpel extends MY_Model {
		function getData($id_rombel){
			$this->db->select('*'); 
			$this->db->from('t_guru_matpel_rombel tmr');
			$this->db->join('m_rombel r', 'r.id_rombel = tmr.id_rombel');
			$this->db->join('m_pelajaran p', 'p.id_pelajaran = tmr.id_pelajaran');
			$this->db->join('m_guru g', 'g.id_guru = tmr.id_guru');
			$this->db->where('tmr.id_rombel', $id_rombel);
			$this->db->order_by('tmr.id_guru_matpel_rombel');
			return $this->db->get();
		}

		function getDataMatpel($id_kurikulum, $id_tingkat_kelas){
			$this->db->select('*'); 
			$this->db->from('m_pelajaran p');
			$this->db->join('m_kelompok_pelajaran k', 'p.id_kelompok_pelajaran = k.id_kelompok_pelajaran');
			$this->db->where('k.id_kurikulum', $id_kurikulum);
			$this->db->where('p.id_tingkat_kelas', $id_tingkat_kelas);
			return $this->db->get();
		}

		function getDataNonGuru($id_rombel, $id_kurikulum, $id_tingkat_kelas){
			$allData = $this ->getData($id_rombel)->result_array();

			$mapel = array();
			if($allData){
				foreach($allData as $value){
					$mapel[] = $value['id_pelajaran'];
				}
			}

			$this->db->from('m_pelajaran p');
			$this->db->join('m_kelompok_pelajaran k', 'p.id_kelompok_pelajaran = k.id_kelompok_pelajaran');
			if($allData) 
				$this->db->where_not_in('p.id_pelajaran', $mapel);
				$this->db->where('k.id_kurikulum', $id_kurikulum);
				$this->db->where('p.id_tingkat_kelas', $id_tingkat_kelas);
			$result = $this->db->get();

			if($result->num_rows() > 0)
				return $result->result_array();

			return false;
		}

		function delete($id){
			$this->db->where('id_guru_matpel_rombel', $id);
			$this->db->delete('t_guru_matpel_rombel');
		}

		function getTingkatKelas(){
			$this->db->select('*'); 
			$this->db->from('m_tingkat_kelas');
			return $this->db->get();
		}

		function getRombel($id_tingkat_kelas, $id_tahun_ajaran){
			$this->db->select('*'); 
			$this->db->from('m_rombel');
			$this->db->where('id_tingkat_kelas', $id_tingkat_kelas);
			$this->db->where('id_tahun_ajaran', $id_tahun_ajaran);
			$this->db->where('id_sekolah', get_id_sekolah());
			$this->db->order_by('rombel', 'asc');
			return $this->db->get();
		}

		function getIdKurikulum($id_rombel){
			$this->db->select('id_kurikulum'); 
			$this->db->from('m_rombel');
			$this->db->where('id_rombel', $id_rombel);
			return $this->db->get();
		}

		function getGuru($id){
			$this->db->select('*'); 
			$this->db->from('m_guru');
			$this->db->where('id_sekolah', $id);
			return $this->db->get();
		}

		function getKelompokPelajaran($id){
			$this->db->select('*'); 
			$this->db->from('m_kelompok_pelajaran');
			$this->db->where('id_kurikulum', $id);
			return $this->db->get();
		}
		
		function getPeminatan(){
			$this->db->select('*'); 
			$this->db->from('m_peminatan');
			return $this->db->get();
		}
		
		function getKurikulum($id){
			$this->db->select('*'); 
			$this->db->from('m_kurikulum');
			$this->db->where('id_kurikulum', $id);
			return $this->db->get();
		}
		
		function getTahunAjaran(){
			$this->db->select('*'); 
			$this->db->from('m_tahun_ajaran');
			$this->db->where('id_tahun_ajaran', get_id_tahun_ajaran());
			return $this->db->get();
		}
		
		function getRombelId($id){
			$this->db->select('*'); 
			$this->db->from('m_rombel r');
			// $this->db->join('m_peminatan p', 'p.id_peminatan=r.id_program');
			$this->db->where('id_rombel', $id);
			return $this->db->get();
		}
		
		function getPeminatanId($id){
			$this->db->select('*'); 
			$this->db->from('m_rombel r');
			$this->db->join('m_peminatan p', 'p.id_peminatan=r.id_program');
			$this->db->where('id_rombel', $id);
			return $this->db->get();
		}
		
		function simpan($data){
			$this->db->insert_batch('t_guru_matpel_rombel',$data);
		}
	}
?>