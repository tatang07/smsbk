<?php
class t_agenda_kelas extends MY_Model {
    public $table = "t_agenda_kelas";
    public $id = "id_agenda_kelas";

    //create your model here
    public function get_all_agenda_kelas(){
    	$query = $this->db->get('t_agenda_kelas');
		$this->db->join('t_guru_matpel_rombel g', 'g.id_guru_matpel_rombel=a.id_guru_matpel_rombel', 'left');
		$query = $this->db->get('t_agenda_kelas a');
		
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
    }

	public function get_agenda_kelas_by_id($id){
		$query = $this->db->query
			("SELECT * FROM t_agenda_kelas WHERE id_agenda_kelas='$id'");

		return $query->row();
	}
  
    public function set_agenda_kelas($data){
		$this->db->insert('t_agenda_kelas', $data);
		return $this->db->insert_id();
	}
	
	public function set_absen($data){
		$this->db->insert('t_siswa_absen', $data);
	}
	
    public function update_agenda_kelas($data){
		$this->db->where('id_agenda_kelas', $data['id_agenda_kelas']);
		$this->db->update('t_agenda_kelas', $data); 
	}

    public function update_guru_matpel_rombel($data){
		$this->db->where('id_guru_matpel_rombel', $data['id_guru_matpel_rombel']);
		$this->db->update('t_guru_matpel_rombel', $data); 
	}

	public function delete_agenda_kelas($id){
		$this->db->where('id_agenda_kelas', $id);
		$this->db->delete('t_siswa_absen');
		
		$this->db->where('id_agenda_kelas', $id);
		$this->db->delete('t_agenda_kelas');
		
	}

	public function get_guru_matpel($id){
		$this->db->where('id_guru',$id);
		$this->db->join('m_pelajaran p', 'p.id_pelajaran=g.id_pelajaran', 'left');
		$query = $this->db->get('t_guru_matpel_rombel g');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_matpel_rombel($id){
		$this->db->where('id_pelajaran',$id);
		$this->db->join('m_rombel r', 'r.id_rombel=g.id_rombel', 'left');
		$query = $this->db->get('t_guru_matpel_rombel g');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_all_siswa(){
		$query = $this->db->get('m_siswa');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}

	public function get_guru_matpel_rombel($id_guru, $id_pelajaran, $id_rombel){
		$query = $this->db->query("SELECT * FROM t_guru_matpel_rombel WHERE id_guru = '$id_guru' and id_pelajaran='$id_pelajaran' and id_rombel = '$id_rombel'");
		return $query->row();
	}
	
    public function get_all_rombel(){
		$query = $this->db
						->from('m_rombel gm')
						->join('m_tingkat_kelas p', 'gm.id_tingkat_kelas=p.id_tingkat_kelas')
						->where('p.id_jenjang_sekolah', get_jenjang_sekolah())
						->where('gm.id_sekolah', get_id_sekolah())
						->get();
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
		
	public function get_rombel_by($id){
		$query = $this->db->query
			("SELECT * FROM m_rombel WHERE id_rombel='$id'");
		return $query->row();
	}
	
	public function get_first_rombel(){
		$query = $this->db->query
			("SELECT * FROM m_rombel ORDER BY id_rombel ASC LIMIT 1;");
		return $query->row();
	}

	public function get_all_pelajaran(){
		$query = $this->db->get('m_pelajaran');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_pelajaran_by($id){
		$query = $this->db->query
			("SELECT * FROM m_pelajaran WHERE id_pelajaran='$id'");
		return $query->row();
	}
	
	public function get_first_pelajaran(){
		$query = $this->db->query
			("SELECT * FROM m_pelajaran ORDER BY id_pelajaran ASC LIMIT 1;");
		return $query->row();
	}

    public function get_all_guru(){
		$query = $this->db->get('m_guru');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}

    public function get_all_term(){
		$query = $this->db->get('m_term');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}

    public function set_guru_matpel_rombel($data){
		$this->db->insert('t_guru_matpel_rombel', $data);

		return $this->db->insert_id();
	}

    public function get_search_pelajaran($data){
		$query = $this->db->query(
				"SELECT
				t_guru_matpel_rombel.id_guru AS id_guru,
				t_guru_matpel_rombel.id_pelajaran AS id_pelajaran,
				t_guru_matpel_rombel.id_rombel AS id_rombel,
				m_rombel.rombel AS rombel,
				m_guru.nama AS nama,
				m_pelajaran.kode_pelajaran AS kode_pelajaran,
				m_pelajaran.pelajaran AS pelajaran,
				m_guru.nip AS nip,
				t_agenda_kelas.id_guru_matpel_rombel,
				t_agenda_kelas.proses_belajar,
				t_agenda_kelas.materi,
				t_agenda_kelas.tanggal,
				t_agenda_kelas.id_agenda_kelas,
				t_agenda_kelas.id_term
				FROM
				(((t_guru_matpel_rombel
				JOIN m_rombel ON ((t_guru_matpel_rombel.id_rombel = m_rombel.id_rombel)))
				JOIN m_pelajaran ON ((t_guru_matpel_rombel.id_pelajaran = m_pelajaran.id_pelajaran)))
				JOIN m_guru ON ((t_guru_matpel_rombel.id_guru = m_guru.id_guru)))
				INNER JOIN t_agenda_kelas ON t_agenda_kelas.id_guru_matpel_rombel = t_guru_matpel_rombel.id_guru_matpel_rombel
				WHERE
				t_guru_matpel_rombel.id_guru_matpel_rombel = $data[id_gmr] AND t_guru_matpel_rombel.id_rombel = $data[id_rombel]
				;"
			);

		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}

	public function get_search_pelajaran_all($data){
		$query = $this->db->query(
				"SELECT
				t_guru_matpel_rombel.id_guru AS id_guru,
				t_guru_matpel_rombel.id_pelajaran AS id_pelajaran,
				t_guru_matpel_rombel.id_rombel AS id_rombel,
				m_rombel.rombel AS rombel,
				m_guru.nama AS nama,
				m_pelajaran.kode_pelajaran AS kode_pelajaran,
				m_pelajaran.pelajaran AS pelajaran,
				m_guru.nip AS nip,
				t_agenda_kelas.id_guru_matpel_rombel,
				t_agenda_kelas.proses_belajar,
				t_agenda_kelas.materi,
				t_agenda_kelas.tanggal,
				t_agenda_kelas.id_agenda_kelas,
				t_agenda_kelas.id_term
				FROM
				(((t_guru_matpel_rombel
				JOIN m_rombel ON ((t_guru_matpel_rombel.id_rombel = m_rombel.id_rombel)))
				JOIN m_pelajaran ON ((t_guru_matpel_rombel.id_pelajaran = m_pelajaran.id_pelajaran)))
				JOIN m_guru ON ((t_guru_matpel_rombel.id_guru = m_guru.id_guru)))
				INNER JOIN t_agenda_kelas ON t_agenda_kelas.id_guru_matpel_rombel = t_guru_matpel_rombel.id_guru_matpel_rombel
				WHERE t_guru_matpel_rombel.id_rombel = $data[id_rombel]"
			);

		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}

	//get agenda kelas with dengan tabel guru matpel rombel
	public function get_all_agenda_kelas_rombel($id){
		$query = $this->db->query(
				"SELECT
				t_guru_matpel_rombel.id_guru AS id_guru,
				t_guru_matpel_rombel.id_pelajaran AS id_pelajaran,
				t_guru_matpel_rombel.id_rombel AS id_rombel,
				m_rombel.rombel AS rombel,
				m_guru.nama AS nama,
				m_pelajaran.kode_pelajaran AS kode_pelajaran,
				m_pelajaran.pelajaran AS pelajaran,
				m_guru.nip AS nip,
				t_agenda_kelas.id_guru_matpel_rombel,
				t_agenda_kelas.proses_belajar,
				t_agenda_kelas.materi,
				t_agenda_kelas.tanggal,
				t_agenda_kelas.id_agenda_kelas,
				t_agenda_kelas.id_term
				FROM
				(((t_guru_matpel_rombel
				JOIN m_rombel ON ((t_guru_matpel_rombel.id_rombel = m_rombel.id_rombel)))
				JOIN m_pelajaran ON ((t_guru_matpel_rombel.id_pelajaran = m_pelajaran.id_pelajaran)))
				JOIN m_guru ON ((t_guru_matpel_rombel.id_guru = m_guru.id_guru)))
				INNER JOIN t_agenda_kelas ON t_agenda_kelas.id_guru_matpel_rombel = t_guru_matpel_rombel.id_guru_matpel_rombel
				WHERE
				t_guru_matpel_rombel.id_guru_matpel_rombel = t_agenda_kelas.id_guru_matpel_rombel
				and
				t_agenda_kelas.id_agenda_kelas = $id
				;"
			);

		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}

    public function get_search_harian($data){
    	if($data['tanggal'] == ''){
    		$data['tanggal'] = '%%';
			$query = $this->db->query(
				"SELECT
				t_guru_matpel_rombel.id_guru AS id_guru,
				t_guru_matpel_rombel.id_pelajaran AS id_pelajaran,
				t_guru_matpel_rombel.id_rombel AS id_rombel,
				m_rombel.rombel AS rombel,
				m_guru.nama AS nama,
				m_pelajaran.kode_pelajaran AS kode_pelajaran,
				m_pelajaran.pelajaran AS pelajaran,
				m_guru.nip AS nip,
				t_agenda_kelas.id_guru_matpel_rombel,
				t_agenda_kelas.proses_belajar,
				t_agenda_kelas.materi,
				t_agenda_kelas.tanggal,
				t_agenda_kelas.id_agenda_kelas,
				t_agenda_kelas.id_term
				FROM
				(((t_guru_matpel_rombel
				JOIN m_rombel ON ((t_guru_matpel_rombel.id_rombel = m_rombel.id_rombel)))
				JOIN m_pelajaran ON ((t_guru_matpel_rombel.id_pelajaran = m_pelajaran.id_pelajaran)))
				JOIN m_guru ON ((t_guru_matpel_rombel.id_guru = m_guru.id_guru)))
				INNER JOIN t_agenda_kelas ON t_agenda_kelas.id_guru_matpel_rombel = t_guru_matpel_rombel.id_guru_matpel_rombel
				WHERE
				t_guru_matpel_rombel.id_rombel = $data[id_rombel] 
				AND
				t_agenda_kelas.tanggal LIKE '$data[tanggal]'
				;"
			);
    	}else{
			$query = $this->db->query(
				"SELECT
				t_guru_matpel_rombel.id_guru AS id_guru,
				t_guru_matpel_rombel.id_pelajaran AS id_pelajaran,
				t_guru_matpel_rombel.id_rombel AS id_rombel,
				m_rombel.rombel AS rombel,
				m_guru.nama AS nama,
				m_pelajaran.kode_pelajaran AS kode_pelajaran,
				m_pelajaran.pelajaran AS pelajaran,
				m_guru.nip AS nip,
				t_agenda_kelas.id_guru_matpel_rombel,
				t_agenda_kelas.proses_belajar,
				t_agenda_kelas.materi,
				t_agenda_kelas.tanggal,
				t_agenda_kelas.id_agenda_kelas,
				t_agenda_kelas.id_term
				FROM
				(((t_guru_matpel_rombel
				JOIN m_rombel ON ((t_guru_matpel_rombel.id_rombel = m_rombel.id_rombel)))
				JOIN m_pelajaran ON ((t_guru_matpel_rombel.id_pelajaran = m_pelajaran.id_pelajaran)))
				JOIN m_guru ON ((t_guru_matpel_rombel.id_guru = m_guru.id_guru)))
				INNER JOIN t_agenda_kelas ON t_agenda_kelas.id_guru_matpel_rombel = t_guru_matpel_rombel.id_guru_matpel_rombel
				WHERE
				t_guru_matpel_rombel.id_rombel = $data[id_rombel] 
				AND
				t_agenda_kelas.tanggal = '$data[tanggal]'
				;"
			);
		}
		

		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}

	public function get_all_guru_matpel_rombel(){
		$query = $this->db->query
			("SELECT 
				t_guru_matpel_rombel.id_guru, 
				t_guru_matpel_rombel.id_pelajaran, 
				t_guru_matpel_rombel.id_rombel,
				t_guru_matpel_rombel.id_guru_matpel_rombel,
				m_rombel.rombel,
				m_guru.nama,
				m_pelajaran.kode_pelajaran,
				m_pelajaran.pelajaran,
				m_guru.nip
				FROM
				t_guru_matpel_rombel
				INNER JOIN m_rombel ON t_guru_matpel_rombel.id_rombel = m_rombel.id_rombel
				INNER JOIN m_pelajaran ON t_guru_matpel_rombel.id_pelajaran = m_pelajaran.id_pelajaran
				INNER JOIN m_guru ON t_guru_matpel_rombel.id_guru = m_guru.id_guru;
			");
		
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}

	public function get_agenda_kelas_harian(){
		$query = $this->db->query
			("SELECT
				t_guru_matpel_rombel.id_guru AS id_guru,
				t_guru_matpel_rombel.id_pelajaran AS id_pelajaran,
				t_guru_matpel_rombel.id_rombel AS id_rombel,
				m_rombel.rombel AS rombel,
				m_guru.nama AS nama,
				m_pelajaran.kode_pelajaran AS kode_pelajaran,
				m_pelajaran.pelajaran AS pelajaran,
				m_guru.nip AS nip,
				t_agenda_kelas.id_guru_matpel_rombel,
				t_agenda_kelas.proses_belajar,
				t_agenda_kelas.materi,
				t_agenda_kelas.tanggal,
				t_agenda_kelas.id_agenda_kelas,
				t_agenda_kelas.id_term
				FROM
				(((t_guru_matpel_rombel
				JOIN m_rombel ON ((t_guru_matpel_rombel.id_rombel = m_rombel.id_rombel)))
				JOIN m_pelajaran ON ((t_guru_matpel_rombel.id_pelajaran = m_pelajaran.id_pelajaran)))
				JOIN m_guru ON ((t_guru_matpel_rombel.id_guru = m_guru.id_guru)))
				INNER JOIN t_agenda_kelas ON t_agenda_kelas.id_guru_matpel_rombel = t_guru_matpel_rombel.id_guru_matpel_rombel;
			");
		
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	//edited by hasan
	public function get_jenis_absen(){
		$query = $this->db->get('r_jenis_absen');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_siswa(){
		$query = $this->db->get('v_siswa');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_pelajaran_rombel($id){
		$query = $this->db
						->from('t_guru_matpel_rombel gm')
						->join('m_pelajaran p', 'gm.id_pelajaran=p.id_pelajaran')
						->where('gm.id_rombel', $id)
						->get();
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_siswa_absen($id){
		$query = $this->db
						->from('t_siswa_absen sa')
						->where('sa.id_agenda_kelas', $id)
						->get();
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_agenda_kelas_by($id){
		$query = $this->db->query
			("SELECT * FROM t_agenda_kelas WHERE id_agenda_kelas='$id'");
		return $query->row();
	}
	
	public function count_jenis_absen(){
			$query = $this->db
						->from('r_jenis_absen')
						->get();
			return $query->num_rows();
		}
		
		public function delete_absen($id){
		$this->db->where('id_agenda_kelas', $id);
		$this->db->delete('t_siswa_absen');
	}

	public function get_km_kelas($id_rombel){
			
		$query = $this->db->query
			("SELECT nama FROM m_siswa JOIN t_km_pelajaran ON (
                                (t_km_pelajaran.id_siswa = m_siswa.id_siswa and t_km_pelajaran.id_rombel = $id_rombel)
                                )
			");
		
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}else{
				return false;
			}
			
	}

	public function get_siswa_by_rombel($id){
		$query = $this->db->query
			("SELECT * FROM m_siswa JOIN t_rombel_detail ON (
                                (t_rombel_detail.id_siswa = m_siswa.id_siswa and t_rombel_detail.id_rombel = $id)
                                )
			");
		
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}

	public function set_km_kelas($data,$id){
		$this->db->where('id_rombel', $id);
		$this->db->delete('t_km_pelajaran');

		$this->db->insert('t_km_pelajaran', $data);

		return $this->db->insert_id();
	}
/*
	public function get_km_rombel($id_rombel, $id_gmr){
		"SELECT
		t_guru_matpel_rombel.id_guru AS id_guru,
		t_guru_matpel_rombel.id_pelajaran AS id_pelajaran,
		t_guru_matpel_rombel.id_rombel AS id_rombel,
		m_rombel.rombel AS rombel,
		m_guru.nama AS nama,
		m_pelajaran.kode_pelajaran AS kode_pelajaran,
		m_pelajaran.pelajaran AS pelajaran,
		m_guru.nip AS nip,
		t_agenda_kelas.id_guru_matpel_rombel,
		t_agenda_kelas.proses_belajar,
		t_agenda_kelas.materi,
		t_agenda_kelas.tanggal,
		t_agenda_kelas.id_agenda_kelas,
		t_agenda_kelas.id_term
		FROM
		(((t_guru_matpel_rombel
		JOIN m_rombel ON ((t_guru_matpel_rombel.id_rombel = m_rombel.id_rombel)))
		JOIN m_pelajaran ON ((t_guru_matpel_rombel.id_pelajaran = m_pelajaran.id_pelajaran)))
		JOIN m_guru ON ((t_guru_matpel_rombel.id_guru = m_guru.id_guru)))
		INNER JOIN t_agenda_kelas ON t_agenda_kelas.id_guru_matpel_rombel = t_guru_matpel_rombel.id_guru_matpel_rombel
		WHERE
		t_guru_matpel_rombel.id_guru_matpel_rombel = $data[id_gmr] AND t_guru_matpel_rombel.id_rombel = $data[id_rombel]
		;"
	}
	*/
}