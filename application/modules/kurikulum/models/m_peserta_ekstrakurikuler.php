<?php

class M_peserta_ekstrakurikuler extends MY_Model {

    public $table = "t_ekstra_kurikuler_peserta";
    public $id = "id_ekstra_kurikuler_peserta";
	
	// join ON 
	public $join_to = array(
			"m_ekstra_kurikuler e" => "t_ekstra_kurikuler_peserta.id_ekstra_kurikuler = e.id_ekstra_kurikuler",
			"v_siswa s" => "t_ekstra_kurikuler_peserta.id_siswa = s.id_siswa"
		 );

	public $join_fields = array("nis", "nama_ekstra_kurikuler", "nama", "rombel");

	function get($id){
		$this->db->from($this->table);

		foreach ($this->join_to as $field => $on) {
			$this->db->join($field, $on);
		}

		return $this->db->where($this->id, $id)
						->get()->row_array();
	}

	function get_rombel($for_dropdown = false)
	{
		$result = $this->db
					->from('t_rombel_detail rd')
					->join('m_rombel r', 'r.id_rombel = rd.id_rombel')
					// ->where('id_tahun_ajaran', get_id_tahun_ajaran()-1)
					->where('id_tahun_ajaran', get_id_tahun_ajaran()) //ceritanya current tahun ajaran = 2
					->where('id_sekolah', get_id_sekolah()) //ceritanya current tahun ajaran = 2
					->group_by('rd.id_rombel')
					->get();

		if($result->num_rows() > 0){
			if($for_dropdown){
				$data = array(0 => '-- pilih kelas --');
				foreach ($result->result_array() as $value) {
					$data[$value['id_rombel']] = $value['rombel'];
				}
				return $data;
			} else
				return $result->result_array();
		}

		return false;
	}

	function get_siswa_rombel($id_rombel = false, $id_tahun_ajaran = false)
	{
		$this->db->from('t_rombel_detail rd')
				->join('m_siswa s', 's.id_siswa = rd.id_siswa')
				->join('m_rombel r', 'r.id_rombel = rd.id_rombel');

		if($id_rombel)
			$this->db->where('r.id_rombel', $id_rombel);

		if($id_tahun_ajaran)
			$this->db->where('r.id_tahun_ajaran', $id_tahun_ajaran);

		$data = $this->db->get();

		if($data->num_rows() > 0)
			return $data->result_array();

		return false;
	}

	// get siswa yang belum punya rombel terdahulu
	function get_siswa_ekskul_rombel($id_ekskul, $id_rombel)
	{
		$this->db->from('t_ekstra_kurikuler_peserta p')
				->join('m_siswa s', 's.id_siswa = p.id_siswa')
				->join('m_ekstra_kurikuler e', 'e.id_ekstra_kurikuler = p.id_ekstra_kurikuler')
				->join('t_rombel_detail rd', 'rd.id_siswa = s.id_siswa')
				->join('m_rombel r', 'r.id_rombel = rd.id_rombel')
				->where('e.id_ekstra_kurikuler', $id_ekskul)
				->where('r.id_rombel', $id_rombel)
				->where('id_tahun_ajaran', get_id_tahun_ajaran());

		$data = $this->db->get();

		if($data->num_rows() > 0)
			return $data->result_array();

		return false;
	}

	// dropdown function
	function get_ekskul($where = array(), $for_dropdown = false)
	{
		$this->db->from('m_ekstra_kurikuler');
		if(!empty($where))
			$this->db->where($where);

		$this->db->where('id_sekolah', get_id_sekolah());
		$result = $this->db->get();

		if($result->num_rows() > 0){
			if($for_dropdown){
				$data = array(0 => '-- pilih ekstrakurikuler --');
				foreach ($result->result_array() as $value) {
					$data[$value['id_ekstra_kurikuler']] = $value['nama_ekstra_kurikuler'];
				}
				return $data;
			} else
				return $result->result_array();
		}

		return false;
	}

	function get_tingkat_kelas($where = array(), $for_dropdown = false)
	{
		$this->db->from('m_tingkat_kelas');
		if(!empty($where))
			$this->db->where($where);

		$this->db->where('id_jenjang_sekolah', get_jenjang_sekolah());
		$result = $this->db->get();

		if($result->num_rows() > 0){
			if($for_dropdown){
				$data = array(0 => '-- pilih tingkat kelas --');
				foreach ($result->result_array() as $value) {
					$data[$value['id_tingkat_kelas']] = $value['tingkat_kelas'];
				}
				return $data;
			} else
				return $result->result_array();
		}

		return false;
	}

	function update_peserta_ekskul($id_ekskul, $siswa){
		// delete from kelas asal first
		$this->db->where('id_ekstra_kurikuler', $id_ekskul)
				->where_in('id_siswa', $siswa)
				->delete('t_ekstra_kurikuler_peserta');

		// create new then
		$data = array();
		foreach ($siswa as $id_siswa) {
			$data[] = array(
				'id_siswa' => $id_siswa,
				'id_ekstra_kurikuler' => $id_ekskul,
				'status_aktif' => 1
				);
		}
		return $this->db->insert_batch('t_ekstra_kurikuler_peserta', $data);
	}
}