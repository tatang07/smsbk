<?php
class m_rekap_guru extends MY_Model {
    public $table = "t_guru_matpel_rombel";
    public $id = "id_guru_matpel_rombel";
	
	                 // join                    ON 
	public $join_to = array(
			"m_pelajaran p"=>"p.id_pelajaran=t_guru_matpel_rombel.id_pelajaran",
			"m_guru g"=>"g.id_guru=t_guru_matpel_rombel.id_guru",
			"m_rombel r"=>"r.id_rombel=t_guru_matpel_rombel.id_rombel",
			"t_agenda_kelas a"=>"a.id_guru_matpel_rombel=t_guru_matpel_rombel.id_guru_matpel_rombel"
		);
	public $join_fields = array("kode_pelajaran","nama","pelajaran","rombel", "count(materi)");
	
	// function getdetail($id=0){
		// $this->db->select('*');
		// $this->db->from('m_guru g'); 
		// $this->db->join('r_golongan_darah gd', 'gd.id_golongan_darah=g.id_golongan_darah', 'left');
		// $this->db->join('r_status_pernikahan sp', 'sp.id_status_pernikahan=g.id_status_pernikahan', 'left');
		// $this->db->join('r_agama a', 'a.id_agama=g.id_agama', 'left');
		// $this->db->join('r_status_pegawai rsp', 'rsp.id_status_pegawai=g.id_status_pegawai', 'left');
		// $this->db->join('r_kecamatan k', 'k.id_kecamatan=g.id_kecamatan', 'left');
		// $this->db->join('r_jenjang_pendidikan jp', 'jp.id_jenjang_pendidikan=g.id_jenjang_pendidikan', 'left');
		// $this->db->join('r_pangkat_golongan pg', 'pg.id_pangkat_golongan=g.id_pangkat_golongan', 'left');
		// $this->db->join('m_sekolah s', 's.id_sekolah=g.id_sekolah', 'left');
		// $this->db->where('g.id_guru',$id);
		// $this->db->order_by('g.nip','asc');         
		// $query = $this->db->get();
		
		// if($query->num_rows() != 0)
		// {
			// return $query->result_array();
		// }
		// else
		// {
			// return false;
		// }
	
	// }
}