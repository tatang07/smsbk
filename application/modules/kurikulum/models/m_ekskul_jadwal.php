<?php
class m_ekskul_jadwal extends MY_Model {
    public $table = "m_ekstra_kurikuler_jadwal";
    public $id = "id_ekstra_kurikuler_jadwal";
	
		// join ON 
	public $join_to = array(
			"m_ekstra_kurikuler e" => "m_ekstra_kurikuler_jadwal.id_ekstra_kurikuler = e.id_ekstra_kurikuler"
		 );

	public $join_fields = array("jenis");
}