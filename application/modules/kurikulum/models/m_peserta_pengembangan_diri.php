<?php
class m_peserta_pengembangan_diri extends MY_Model {
    public $table = "t_ekstra_kurikuler_peserta";
    public $id = "id_ekstra_kurikuler_peserta";
	
	
	//select s.nis,s.nama,tk.tingkat_kelas,p.program from t_ekstra_kurikuler_peserta as ekp join m_ekstra_kurikuler as ek on ekp.id_ekstra_kurikuler = ek.id_ekstra_kurikuler join m_siswa as s on ekp.id_siswa = s.id_siswa join t_rombel_detail as rd on s.id_siswa = rd.id_siswa join m_rombel as r on r.id_rombel =  rd.id_rombel join m_tingkat_kelas as tk on tk.id_tingkat_kelas = r.id_tingkat_kelas join m_program as p on r.id_program = p.id_program join m_peminatan as pem on p.id_peminatan = pem.id_peminatan
	
	
	//                  join                    ON 
	public $join_to = array(
			"m_ekstra_kurikuler ek"=>"t_ekstra_kurikuler_peserta.id_ekstra_kurikuler=ek.id_ekstra_kurikuler",
			"m_siswa s"=>"t_ekstra_kurikuler_peserta.id_siswa=s.id_siswa",
			"t_rombel_detail rd"=>"s.id_siswa=rd.id_siswa",
			"m_rombel r"=>"r.id_rombel=rd.id_rombel",
			"m_tingkat_kelas tk"=>"tk.id_tingkat_kelas=r.id_tingkat_kelas",
			"m_program p"=>"r.id_program=p.id_program",
			"m_peminatan pem"=>"p.id_peminatan=pem.id_peminatan"
			
		 );
	public $join_fields = array("nama","tingkat_kelas","peminatan_singkatan","nama_ekstra_kurikuler","nis");
	
	function get_list_pengembangan_diri(){
			$this->db->select('*'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_ekstra_kurikuler');
			//$this->db->join('m_mata_pelajaran mp', 'mp.id_mata_pelajaran = s.id_mata_pelajaran');
		
			$this->db->where('jenis', 2);
			// $this->db->group_by("p.id_pelajaran ,id_tingkat_kelas, id_peminatan");
			$hasil=$this->db->get()->result();
			$output = array();
			foreach ($hasil as $h){
				$output[$h->id_ekstra_kurikuler] = $h->nama_ekstra_kurikuler; 
			}
			return $output;
	}
	function getList(){
			$this->db->select('*'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_ekstra_kurikuler');
			//$this->db->join('m_mata_pelajaran mp', 'mp.id_mata_pelajaran = s.id_mata_pelajaran');
		
			$this->db->where('jenis', 2);
			// $this->db->group_by("p.id_pelajaran ,id_tingkat_kelas, id_peminatan");
			$hasil=$this->db->get()->result();
			return $hasil;
	}
	
	function getData($nim){
			$this->db->select('id_siswa,nama');
			$this->db->from('m_siswa');
			$this->db->where('nis', $nim);
			$hasil=$this->db->get()->row();
			return $hasil;
	}
	

	
	function insertData($data){
	
			$this->db->insert($this->table, $data);
	}
	
}