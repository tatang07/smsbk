<?php
class m_pelajaran_tematik extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_datalist($limit, $start){

			$this->db->limit($limit, $start);
			$this->db->select('*');
			$this->db->from('m_pelajaran_tematik'); 
			
			$this->db->where('id_sekolah',get_id_sekolah());
			// $this->db->group_by('s.nis');         
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_tematik_by($id){
			$query = $this->db->query("SELECT * FROM m_pelajaran_tematik WHERE id_pelajaran_tematik='$id'");
			return $query->row();
		}
		
		public function count_datalist(){

			$this->db->select('*');
			$this->db->from('m_pelajaran_tematik'); 
			
			$this->db->where('id_sekolah',get_id_sekolah());
			$query = $this->db->get();
					
					
			return $query->num_rows();
			
		}
		
		public function get_all_tingkat($jenjang){
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function delete($id){
			$this->db->delete('m_pelajaran_tematik', array('id_pelajaran_tematik'=>$id));
		}
		
		public function insert($kode, $tema, $alokasi, $tingkat, $id){
			$this->db->query("INSERT INTO m_pelajaran_tematik(kode_pelajaran_tematik, pelajaran_tematik, alokasi_waktu, id_tingkat_kelas, id_sekolah) VALUES ('$kode', '$tema', '$alokasi', '$tingkat', '$id')");
		}
		
		public function update($id, $kode, $tema, $alokasi, $tingkat, $id_sekolah){
			$data=array(
				'kode_pelajaran_tematik'=> $kode,
				'pelajaran_tematik'=> $tema,
				'alokasi_waktu'=> $alokasi,
				'id_tingkat_kelas'=> $tingkat,
				'id_sekolah'=> $id_sekolah
			);
			
			$this->db->where('id_pelajaran_tematik', $id);
			$this->db->update('m_pelajaran_tematik', $data);
		}
}