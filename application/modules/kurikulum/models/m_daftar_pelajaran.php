<?php
class m_daftar_pelajaran extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_all_kompetensi(){
			$query = $this->db->get('m_kompetensi_inti');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_tingkat($jenjang){
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_tingkat($jenjang){
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			return $query->num_rows();
		}
		
		public function get_all_kurikulum(){
			$query = $this->db
						->from('m_kurikulum')
						->order_by('id_kurikulum desc')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_pelajaran($limit, $start){
			$this->db->limit($limit, $start);
			$query = $this->db
						->from('m_pelajaran p')
						->join('m_tingkat_kelas t', 't.id_tingkat_kelas=p.id_tingkat_kelas')
						->where('t.id_jenjang_sekolah', get_jenjang_sekolah())
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_pelajaran(){
			 $query = $this->db
						->from('m_pelajaran p')
						->join('m_tingkat_kelas t', 't.id_tingkat_kelas=p.id_tingkat_kelas')
						->where('t.id_jenjang_sekolah', get_jenjang_sekolah())
						->get();
			return $query->num_rows();
		}
		
		public function get_all_pelajaran_group_by(){
			$query = $this->db
						->from('m_pelajaran')
						->group_by('pelajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_pelajaran($tingkat, $kelompok, $peminatan, $kurikulum){
			$sekolah = get_id_sekolah();
			$where = "(p.id_sekolah = 0 or p.id_sekolah = $sekolah)";
			$query = $this->db
						->from('m_pelajaran p')
						->join('m_kelompok_pelajaran kp', 'kp.id_kelompok_pelajaran=p.id_kelompok_pelajaran')
						->where('p.id_tingkat_kelas', $tingkat)
						->where('p.id_kelompok_pelajaran', $kelompok)
						->where('p.id_peminatan', $peminatan)
						->where($where)
						->where('kp.id_kurikulum', $kurikulum)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_count_pelajaran($tingkat, $kelompok, $peminatan, $kurikulum){
			$query = $this->db
						->from('m_pelajaran p')
						->join('m_kelompok_pelajaran kp', 'kp.id_kelompok_pelajaran=p.id_kelompok_pelajaran')
						->where('p.id_tingkat_kelas', $tingkat)
						->where('p.id_kelompok_pelajaran', $kelompok)
						->where('p.id_peminatan', $peminatan)
						->where('kp.id_kurikulum', $kurikulum)
						->get();
			return $query->num_rows();
		}
		
		public function get_all_pelajaran_by_peminatan(){
			$query = $this->db
						->from('m_pelajaran p')
						->where('p.id_peminatan >=', 1)
						->join('m_peminatan pm', 'pm.id_peminatan = p.id_peminatan')
						->join('m_kelompok_pelajaran kp', 'kp.id_kelompok_pelajaran = p.id_kelompok_pelajaran')
						->join('t_kualifikasi_guru_pelajaran kgp', 'kgp.id_pelajaran = p.id_pelajaran')
						->join('r_kualifikasi_guru kg', 'kg.id_kualifikasi_guru = kgp.id_kualifikasi_guru')
						->order_by('p.id_peminatan')
						->get();
			
			if($query->num_rows() > 0)
				return $query->result();
				
			return false;
		}
		
		public function get_all_kelompok_pelajaran(){
			$query = $this->db->get('m_kelompok_pelajaran');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_kelompok_pelajaran_by($id){
			$query = $this->db->query("SELECT * FROM m_kelompok_pelajaran where id_kelompok_pelajaran = '$id';");
			return $query->row();
		}
		
		public function get_last_kelompok_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_kelompok_pelajaran ORDER BY id_kelompok_pelajaran DESC LIMIT 1;");
			return $query->row();
		}
		
		public function get_last_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_pelajaran ORDER BY id_pelajaran DESC LIMIT 1;");
			return $query->row();
		}
		
		public function get_all_peminatan(){
			$query = $this->db
						->from('m_peminatan p')
						->join('m_kelompok_pelajaran kp', 'kp.id_kelompok_pelajaran = p.id_kelompok_pelajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_kualifikasi_guru(){
			$query = $this->db->get('r_kualifikasi_guru');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_alokasi_waktu_per_minggu(){
			$query = $this->db->get('m_alokasi_waktu_per_minggu');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_alokasi_waktu_per_minggu($id_pelajaran){
			$query = $this->db
						->where('id_pelajaran', $id_pelajaran)
						->join('m_tingkat_kelas t', 't.id_tingkat_kelas = m_alokasi_waktu_per_minggu.id_tingkat_kelas')
						->order_by('t.id_tingkat_kelas', 'asc')
						->get('m_alokasi_waktu_per_minggu');
			
			if($query->num_rows() > 0)
				return $query->result_array();
			
			return false;
		}
		
		public function get_pelajaran_by($id){
			$query = $this->db->query("SELECT * FROM m_pelajaran WHERE id_pelajaran='$id'");
			return $query->row();
		}
		
		public function get_all_beban_belajar(){
			$query = $this->db->get('m_beban_belajar');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_kompetensi_inti_by($id){
			$query = $this->db->query("SELECT * FROM m_kompetensi_inti WHERE id_kompetensi_inti='$id'");
			return $query->row();
		}
		
		public function update($id_pelajaran, $kode_pelajaran, $pelajaran, $jumlah_jam, $id_tingkat_kelas, $id_kelompok_pelajaran, $id_kualifikasi_guru, $id_peminatan){
			$data=array(
				'kode_pelajaran'=> $kode_pelajaran,
				'pelajaran'=> $pelajaran,
				'jumlah_jam'=> $jumlah_jam,
				'id_tingkat_kelas'=> $id_tingkat_kelas,
				'id_kelompok_pelajaran'=> $id_kelompok_pelajaran,
				'id_kualifikasi_guru'=> $id_kualifikasi_guru,
				'id_peminatan'=> $id_peminatan
			);
			
			$this->db->where('id_pelajaran', $id_pelajaran);
			$this->db->update('m_pelajaran', $data);
		}
		
		public function delete($id){
			$this->db->delete('m_pelajaran', array('id_pelajaran'=>$id));
		}
		
}