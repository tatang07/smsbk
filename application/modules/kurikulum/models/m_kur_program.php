<?php
class m_kur_program extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_all_kompetensi(){
			$query = $this->db->get('m_kompetensi_inti');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_tingkat($jenjang){
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_tingkat($jenjang){
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			return $query->num_rows();
		}
		
		public function get_all_kurikulum(){
			$query = $this->db
						->from('m_kurikulum')
						->order_by('id_kurikulum desc')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_pelajaran($id_kur){
			$sekolah = get_id_sekolah();
			$where = "(p.id_sekolah = 0 or p.id_sekolah = $sekolah)";
			
			$query = $this->db
						->from('m_pelajaran p')
						->join('m_tingkat_kelas t', 't.id_tingkat_kelas=p.id_tingkat_kelas')
						->join('m_kelompok_pelajaran k', 'k.id_kelompok_pelajaran=p.id_kelompok_pelajaran')
						->where('t.id_jenjang_sekolah', get_jenjang_sekolah())
						->where('k.id_kurikulum', $id_kur)
						->where($where)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		
		public function get_all_pelajaran_group_by($id_kur){
			$sekolah = get_id_sekolah();
			$where = "(p.id_sekolah = 0 or p.id_sekolah = $sekolah)";
			
			$query = $this->db
						->from('m_pelajaran p')
						->join('m_tingkat_kelas t', 't.id_tingkat_kelas=p.id_tingkat_kelas')
						->join('m_kelompok_pelajaran k', 'k.id_kelompok_pelajaran=p.id_kelompok_pelajaran')
						->where('t.id_jenjang_sekolah', get_jenjang_sekolah())
						->where('k.id_kurikulum', $id_kur)
						->where($where)
						->group_by('pelajaran')
						->group_by('k.id_kelompok_pelajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_pelajaran_by_peminatan($id_kur){
			$sekolah = get_id_sekolah();
			$where = "(p.id_sekolah = 0 or p.id_sekolah = $sekolah)";
			
			/*
		SELECT
		p.pelajaran,
		pm.peminatan
		FROM
		((((m_pelajaran p
		JOIN m_peminatan pm ON ((pm.id_peminatan = p.id_peminatan)))
		JOIN m_kelompok_pelajaran kp ON ((kp.id_kelompok_pelajaran = p.id_kelompok_pelajaran)))
		JOIN t_kualifikasi_guru_pelajaran kgp ON ((kgp.id_pelajaran = p.id_pelajaran)))
         JOIN r_kualifikasi_guru kg ON ((kg.id_kualifikasi_guru = kgp.id_kualifikasi_guru)))
		WHERE
		(p.id_sekolah = 0 or p.id_sekolah=1) and p.id_peminatan >= 1
        group by p.pelajaran
        order by p.id_peminatan
        ;
			*/

			$query = $this->db
						->select('p.pelajaran, pm.peminatan')
						->from('m_pelajaran p')
						->where('p.id_peminatan >=', 1)
						->where($where)
						->join('m_peminatan pm', 'pm.id_peminatan = p.id_peminatan')
						->join('m_kelompok_pelajaran kp', 'kp.id_kelompok_pelajaran = p.id_kelompok_pelajaran')
						->join('t_kualifikasi_guru_pelajaran kgp', 'kgp.id_pelajaran = p.id_pelajaran')
						->join('r_kualifikasi_guru kg', 'kg.id_kualifikasi_guru = kgp.id_kualifikasi_guru')
						->where('kp.id_kurikulum', $id_kur)
						->order_by('p.id_peminatan')
						->group_by('p.pelajaran')
						->get();
			
			if($query->num_rows() > 0)
				return $query->result();
				
			return false;
		}
		
		public function get_all_kelompok_pelajaran($id){
			$this->db->where('id_kurikulum', $id);
			$query = $this->db->get('m_kelompok_pelajaran');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_kelompok_pelajaran_by($id){
			$query = $this->db->query("SELECT * FROM m_kelompok_pelajaran where id_kelompok_pelajaran = '$id';");
			return $query->row();
		}
		
		public function get_last_kelompok_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_kelompok_pelajaran ORDER BY id_kelompok_pelajaran DESC LIMIT 1;");
			return $query->row();
		}
		
		public function get_last_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_pelajaran ORDER BY id_pelajaran DESC LIMIT 1;");
			return $query->row();
		}
		
		public function get_all_peminatan(){
			$query = $this->db
						->from('m_peminatan p')
						->join('m_kelompok_pelajaran kp', 'kp.id_kelompok_pelajaran=p.id_kelompok_pelajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_kualifikasi_guru(){
			$query = $this->db->get('r_kualifikasi_guru');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_pelajaran_by($id){
			$query = $this->db->query("SELECT * FROM m_pelajaran WHERE id_pelajaran='$id'");
			return $query->row();
		}
		
		public function get_all_beban_belajar(){
			$query = $this->db->get('m_beban_belajar');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
}