<?php
class m_struktur_kurikulum extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public $table = "m_pelajaran";
		
		public function get_all_kompetensi(){
			$query = $this->db->get('m_kompetensi_inti');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_tingkat($jenjang){
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_tingkat($jenjang){
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			return $query->num_rows();
		}
		
		public function get_all_kurikulum(){
			$query = $this->db
						->from('m_kurikulum')
						->order_by('id_kurikulum desc')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_pelajaran($id_kur){
			$sekolah = get_id_sekolah();
			$where = "(p.id_sekolah = 0 or p.id_sekolah = $sekolah)";
			
			$query = $this->db
						->from('m_pelajaran p')
						->join('m_tingkat_kelas t', 't.id_tingkat_kelas=p.id_tingkat_kelas')
						->join('m_kelompok_pelajaran k', 'k.id_kelompok_pelajaran=p.id_kelompok_pelajaran')
						->where('t.id_jenjang_sekolah', get_jenjang_sekolah())
						->where('k.id_kurikulum', $id_kur)
						->where($where)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_pelajaran_group_by($id_kur){
			$sekolah = get_id_sekolah();
			$where = "(p.id_sekolah = 0 or p.id_sekolah = $sekolah)";
			
			$query = $this->db
						->from('m_pelajaran p')
						->join('m_tingkat_kelas t', 't.id_tingkat_kelas=p.id_tingkat_kelas')
						->join('m_kelompok_pelajaran k', 'k.id_kelompok_pelajaran=p.id_kelompok_pelajaran')
						->where('t.id_jenjang_sekolah', get_jenjang_sekolah())
						->where('k.id_kurikulum', $id_kur)
						->where($where)
						->group_by('p.pelajaran')
						->group_by('k.id_kelompok_pelajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_pelajaran_by_peminatan($id_kur){
			$sekolah = get_id_sekolah();
			$where = "(p.id_sekolah = 0 or p.id_sekolah = $sekolah)";
			
			$query = $this->db
						->select('p.pelajaran, pm.peminatan')
						->from('m_pelajaran p, t_kualifikasi_guru_pelajaran kgp')
						->join('m_tingkat_kelas t', 't.id_tingkat_kelas=p.id_tingkat_kelas')
						->join('m_peminatan pm', 'pm.id_peminatan = p.id_peminatan')
						->join('m_kelompok_pelajaran kp', 'kp.id_kelompok_pelajaran = p.id_kelompok_pelajaran')
						->join('r_kualifikasi_guru kg', 'kg.id_kualifikasi_guru = kgp.id_kualifikasi_guru')
						->order_by('p.id_peminatan')
						->group_by('p.pelajaran')
						->where('p.id_peminatan >=', 1)
						->where('t.id_jenjang_sekolah', get_jenjang_sekolah())
						->where('kp.id_kurikulum', $id_kur)
						->where($where)
						->get();
			
			if($query->num_rows() > 0)
				return $query->result();
				
			return false;
		}
		
		public function get_all_kelompok_pelajaran_semua(){
			$query = $this->db->get('m_kelompok_pelajaran');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_kelompok_pelajaran($id){
			$this->db->where('id_kurikulum', $id);
			$query = $this->db->get('m_kelompok_pelajaran');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_kelompok_pelajaran_by($id){
			$query = $this->db->query("SELECT * FROM m_kelompok_pelajaran where id_kelompok_pelajaran = '$id';");
			return $query->row();
		}
		
		public function get_last_kelompok_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_kelompok_pelajaran ORDER BY id_kelompok_pelajaran DESC LIMIT 1;");
			return $query->row();
		}
		
		public function get_last_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_pelajaran ORDER BY id_pelajaran DESC LIMIT 1;");
			return $query->row();
		}

		public function get_last_pelajaran_arr(){
			$query = $this->db->query("SELECT * FROM m_pelajaran ORDER BY id_pelajaran DESC LIMIT 1;");
			return $query->result_array();
		}
		
		public function get_all_peminatan(){
			$query = $this->db->get('m_peminatan');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}

		public function get_all_peminatan_ktsp(){
			$this->db->where('id_kelompok_pelajaran', '4'); //karena pilihan ktsp itu kelompok pelajaran 4
			$query = $this->db->get('m_peminatan');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}

		public function get_all_peminatan_2013(){			
			$this->db->where('id_kelompok_pelajaran', '3'); //karena pilihan 2013 itu kelompok pelajaran 3
			$query = $this->db->get('m_peminatan');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_kualifikasi_guru(){
			$query = $this->db->get('r_kualifikasi_guru');
			if($query->num_rows() > 0){
				return $query->result_array();
			}
		}

		function insert_kualifikasi($id_pelajaran, $kualifikasi){

			// delete all from current kelas 
			$this->db->where('id_pelajaran', $id_pelajaran)
					->delete('t_kualifikasi_guru_pelajaran');

			// create new then
			$data = array();
			foreach ($kualifikasi as $id_kualifikasi_guru) {
				$data[] = array(
					'id_kualifikasi_guru' => $id_kualifikasi_guru,
					'id_pelajaran' => $id_pelajaran
					);
			}
			$this->db->insert_batch('t_kualifikasi_guru_pelajaran', $data);
		}
		
		public function get_pelajaran_by($id){
			$query = $this->db->query("SELECT id_pelajaran, pelajaran FROM m_pelajaran WHERE id_pelajaran='$id'");
			return $query->row();
		}

		

		public function get_pelajaran_kualifikasi_by($id){
			$query = $this->db->query("SELECT id_kualifikasi_pelajaran FROM t_kualifikasi_guru_pelajaran WHERE id_pelajaran='$id'");
			return $query->result_array();
		}
		
		public function get_all_beban_belajar(){
			$query = $this->db->get('m_beban_belajar');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function insert_pel($kode_pelajaran, $pelajaran, $jumlah_jam, $id_tingkat_kelas, $id_kelompok_pelajaran, $id_kualifikasi_guru, $id_peminatan){
			$this->db->query("INSERT INTO m_pelajaran VALUES ('', '$kode_pelajaran', '$pelajaran', '$jumlah_jam', '$id_tingkat_kelas', '$id_kelompok_pelajaran', '$id_kualifikasi_guru', '$id_peminatan')");	
		}
}