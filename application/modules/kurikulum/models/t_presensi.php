<?php
class t_presensi extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_datalist($id_agenda_kelas){

			$this->db->select('*');
			$this->db->from('t_siswa_absen sa'); 
			
			$this->db->join('r_jenis_absen ja', 'sa.id_jenis_absen=ja.id_jenis_absen', 'left');
			// $this->db->join('t_agenda_kelas ak', 'sa.id_agenda_kelas=ak.id_agenda_kelas', 'left');
			// $this->db->join('m_siswa s', 'sa.id_siswa=s.id_siswa', 'left');
			// $this->db->join('m_term t', 't.id_term=ak.id_term', 'left');
			// $this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=ak.id_guru_matpel_rombel', 'left');
			// $this->db->join('m_pelajaran m', 'gm.id_pelajaran=m.id_pelajaran', 'left');
			// $this->db->join('m_rombel r', 'gm.id_rombel=r.id_rombel', 'left');
			
			// $this->db->where('r.id_rombel',$id_rombel);
			// $this->db->where('m.id_pelajaran',$id_pelajaran);
			$this->db->where('sa.id_agenda_kelas',$id_agenda_kelas);
			// $this->db->group_by('s.nis');         
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_datalist($id_agenda_kelas){

			$this->db->select('*');
			$this->db->from('t_siswa_absen sa'); 
			
			$this->db->join('r_jenis_absen ja', 'sa.id_jenis_absen=ja.id_jenis_absen', 'left');
			// $this->db->join('t_agenda_kelas ak', 'sa.id_agenda_kelas=ak.id_agenda_kelas', 'left');
			// $this->db->join('m_siswa s', 'sa.id_siswa=s.id_siswa', 'left');
			// $this->db->join('m_term t', 't.id_term=ak.id_term', 'left');
			// $this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=ak.id_guru_matpel_rombel', 'left');
			// $this->db->join('m_pelajaran m', 'gm.id_pelajaran=m.id_pelajaran', 'left');
			// $this->db->join('m_rombel r', 'gm.id_rombel=r.id_rombel', 'left');
			
			// $this->db->where('r.id_rombel',$id_rombel);
			// $this->db->where('m.id_pelajaran',$id_pelajaran);
			$this->db->where('sa.id_agenda_kelas',$id_agenda_kelas);
			// // $this->db->group_by('s.nis');         
			$query = $this->db->get();
					
					
			return $query->num_rows();
			
		}
		
		public function get_rekap_absen($id){
			$this->db->where('id_guru_matpel_rombel',$id);
			$query = $this->db->get('v_siswa_absen_rekap');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[$data->nis]['jml'][$data->id_jenis_absen] = $data->jumlah;
					$result[$data->nis]['nama'] = $data->nama;
				}	
				return $result;
			}
		}
		
		public function get_all_rombel(){
			$query = $this->db->get('m_rombel');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_siswa(){
			$query = $this->db->get('m_siswa');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_jenis_absen(){
			$query = $this->db->get('r_jenis_absen');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_siswa_absen(){
			$query = $this->db->get('t_siswa_absen');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_rombel_by($id){
			$query = $this->db->query("SELECT * FROM m_rombel WHERE id_rombel='$id'");
			return $query->row();
		}
		
		public function get_first_rombel(){
			$query = $this->db->query("SELECT * FROM m_rombel ORDER BY id_rombel ASC LIMIT 1;");
			return $query->row();
		}
		
		public function get_all_agenda_kelas(){
			$query = $this->db->get('t_agenda_kelas');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_agenda_kelas_by($id, $id_term){
			$this->db->select('*');
			$this->db->from('t_agenda_kelas'); 
			$this->db->where('id_guru_matpel_rombel',$id);
			$this->db->where('id_term',$id_term);
			$query = $this->db->get();

			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_term(){
			$query = $this->db->get('m_term');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_guru_matpel_rombel(){
			$query = $this->db->get('t_guru_matpel_rombel');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_guru(){
			$query = $this->db->get('m_guru');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_guru_matpel_rombel($id_pelajaran, $id_rombel){
			$query = $this->db->query("SELECT * FROM t_guru_matpel_rombel WHERE id_pelajaran='$id_pelajaran' and id_rombel = '$id_rombel'");
			return $query->row();
		}
		
		public function get_all_pelajaran(){
			$query = $this->db->get('m_pelajaran');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_pelajaran_by($id){
			$query = $this->db->query("SELECT * FROM m_pelajaran WHERE id_pelajaran='$id'");
			return $query->row();
		}
		
		public function get_absen_by($id){
			$query = $this->db->query("SELECT * FROM t_siswa_absen WHERE id_siswa_absen='$id'");
			return $query->row();
		}
		
		public function get_first_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_pelajaran ORDER BY id_pelajaran ASC LIMIT 1;");
			return $query->row();
		}
}