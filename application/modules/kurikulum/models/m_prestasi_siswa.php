<?php
class m_prestasi_siswa extends MY_Model {
	
	public function get_ekskul(){
			$query = $this->db
						->from('m_ekstra_kurikuler')
						->order_by('id_ekstra_kurikuler')
						->where('jenis',1)
						->where('id_sekolah',get_id_sekolah())
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
	}
	
	public function get_data_search($ekskul, $nama){
		$query = $this->db->query("SELECT s.nama,s.nis,prestasi,id_prestasi_siswa,kategori,perlombaan,tingkat_wilayah,tanggal FROM t_prestasi_siswa as ps join m_siswa as s on s.id_siswa=ps.id_siswa join r_tingkat_wilayah as tw on ps.id_tingkat_wilayah =tw.id_tingkat_wilayah where s.nama like '%$nama%' and ps.id_ekstra_kurikuler = '$ekskul' ");
		return $query->result_array();
	}
	
	function get_data(){
		$query = $this->db->query("SELECT s.nama,s.nis,prestasi,id_prestasi_siswa,kategori,perlombaan,tingkat_wilayah,tanggal, ps.id_ekstra_kurikuler FROM t_prestasi_siswa as ps join m_siswa as s on s.id_siswa=ps.id_siswa join r_tingkat_wilayah as tw on ps.id_tingkat_wilayah =tw.id_tingkat_wilayah join m_ekstra_kurikuler as ek on ek.id_ekstra_kurikuler=ps.id_ekstra_kurikuler where ek.jenis=1");
		return $query->result_array();
	}
	
	function select_tingkat_wilayah(){
			$this->db->select('*');
			$this->db->from('r_tingkat_wilayah');
			
			return $this->db->get()->result_array();
	}
	
	function select_siswa($id_sekolah){
			$this->db->select('*');
			$this->db->from('t_ekstra_kurikuler_peserta ekp');
			$this->db->join('m_siswa s','s.id_siswa = ekp.id_siswa');
			$this->db->join('m_ekstra_kurikuler ek', 'ek.id_ekstra_kurikuler = ekp.id_ekstra_kurikuler');
			$this->db->where('ek.id_sekolah',$id_sekolah);
			$this->db->where('ekp.status_aktif',1);
			return $this->db->get()->result_array();
	}
	
	function get_siswa($id_ekskul){
			$this->db->select('*');
			$this->db->from('t_ekstra_kurikuler_peserta ekp');
			$this->db->join('v_siswa s','s.id_siswa = ekp.id_siswa');
			$this->db->join('m_ekstra_kurikuler ek', 'ek.id_ekstra_kurikuler = ekp.id_ekstra_kurikuler');
			$this->db->where('ek.id_sekolah',get_id_sekolah());
			$this->db->where('ekp.id_ekstra_kurikuler',$id_ekskul);
			$this->db->where('ekp.status_aktif',1);
			return $this->db->get()->result_array();
	}
	
	function add($data){
			$this->db->insert('t_prestasi_siswa',$data);
	}
	
	function edit($data,$id=0){
			$this->db->where('id_siswa_catatan_bk',$id);
			$this->db->update('t_siswa_catatan_bk',$data);
	}
	
	function delete($id){
			$this->db->where('id_prestasi_siswa', $id);
			$this->db->delete('t_prestasi_siswa');
	}
	
}