<?php
class t_rpp_tematik extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_datalist($id, $limit, $start){
			$id_term=get_id_term();
			$this->db->limit($limit, $start);
			$this->db->select('*');
			$this->db->from('t_rpp_tematik rp'); 
			
			$this->db->join('m_term t', 't.id_term=rp.id_term', 'left');
			$this->db->join('m_pelajaran_tematik p', 'p.id_pelajaran_tematik=rp.id_pelajaran_tematik', 'left');
			
			$this->db->where('rp.id_pelajaran_tematik',$id);
			$this->db->where('rp.id_term',$id_term);
			$this->db->where('p.id_sekolah',get_id_sekolah());
			
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_datalist($id){
			$id_term=get_id_term();
			$this->db->select('*');
			$this->db->from('t_rpp_tematik rp'); 
			
			$this->db->join('m_term t', 't.id_term=rp.id_term', 'left');
			$this->db->join('m_pelajaran_tematik p', 'p.id_pelajaran_tematik=rp.id_pelajaran_tematik', 'left');
			
			$this->db->where('rp.id_pelajaran_tematik',$id);
			$this->db->where('rp.id_term',$id_term);
			$this->db->where('p.id_sekolah',get_id_sekolah());
			
			$query = $this->db->get();
					
			return $query->num_rows();
		}
		
		public function get_all_tingkat(){
			$jenjang = get_jenjang_sekolah();
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_tematik(){
			$query = $this->db
						->from('m_pelajaran_tematik')
						->where('id_sekolah', get_id_sekolah())
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_term(){
			$query = $this->db->get('m_term');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_rpp_tematik_by($id){
			$query = $this->db->query("SELECT * FROM t_rpp_tematik WHERE id_rpp_tematik='$id'");
			return $query->row();
		}
		
		public function delete($id){
			$this->db->delete('t_rpp_tematik', array('id_rpp_tematik'=>$id));
		}
		
		public function insert($nama_rpp_tematik, $pertemuan_ke, $lokasi_file, $tematik, $id_term){
			$this->db->query("INSERT INTO t_rpp_tematik(nama_rpp_tematik, pertemuan_ke, lokasi_file, id_pelajaran_tematik, id_term) VALUES ('$nama_rpp_tematik', '$pertemuan_ke', '$lokasi_file', '$tematik', '$id_term');");
		}
}