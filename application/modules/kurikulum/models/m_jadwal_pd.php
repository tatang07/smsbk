<?php
class m_jadwal_pd extends MY_Model {

	function insert_isi($data){
			$this->db->insert($this->table, $data);
	}

	function select_isi(){
	
			$this->db->select('*'); 
			$this->db->from('m_ekstra_kurikuler_jadwal ekj');
			$this->db->join('m_ekstra_kurikuler ek', 'ek.id_ekstra_kurikuler = ekj.id_ekstra_kurikuler');
			$this->db->where('ek.jenis', 2);
			$this->db->where('ek.id_sekolah', get_id_sekolah());
			// $this->db->group_by("p.id_pelajaran ,id_tingkat_kelas, id_peminatan");
			return $this->db->get();
	}
	function getList(){
			$this->db->select('*'); 
			$this->db->from('m_ekstra_kurikuler');
			$this->db->where('jenis', 2);
			$this->db->where('id_sekolah', get_id_sekolah());
			return $this->db->get();
	}
	
	function getDataById($id){
			$this->db->select('*'); 
			$this->db->from('m_ekstra_kurikuler_jadwal');
			$this->db->where('id_ekstra_kurikuler_jadwal',$id);
			return $this->db->get();
	}
	function tambah_data($data){
		$this->db->insert('m_ekstra_kurikuler_jadwal',$data);
	}
	
	public function update_data($id, $hari, $jam_mulai, $jam_selesai, $tempat, $id_ekstra_kurikuler){
		$data=array(
			'hari'=> $hari,
			'jam_mulai'=> $jam_mulai,
			'jam_selesai'=> $jam_selesai,
			'tempat'=> $tempat,
			'id_ekstra_kurikuler'=> $id_ekstra_kurikuler
		);
		
		$this->db->where('id_ekstra_kurikuler_jadwal', $id);
		$this->db->update('m_ekstra_kurikuler_jadwal', $data);
	}
		
	function delete($id){
		$this->db->where('id_ekstra_kurikuler_jadwal', $id);
		$this->db->delete('m_ekstra_kurikuler_jadwal');
	}
}