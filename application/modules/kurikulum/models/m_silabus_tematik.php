<?php
class m_silabus_tematik extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_datalist($limit, $start){
			$id_sekolah=get_id_sekolah();
			$this->db->limit($limit, $start);
			$this->db->select('*');
			$this->db->from('m_silabus_tematik'); 
			
			$this->db->where('id_sekolah',$id_sekolah);
			// $this->db->group_by('s.nis');         
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_datalist(){
			$id_sekolah=get_id_sekolah();
			
			$this->db->select('*');
			$this->db->from('m_silabus_tematik'); 
			
			$this->db->where('id_sekolah',$id_sekolah);
			$query = $this->db->get();
					
					
			return $query->num_rows();
			
		}
		
		public function get_all_tingkat($jenjang){
			$query = $this->db
						->from('m_tingkat_kelas')
						->where('id_jenjang_sekolah', $jenjang)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_silabus_tematik_by($id){
			$query = $this->db->query("SELECT * FROM m_silabus_tematik WHERE id_silabus_tematik='$id'");
			return $query->row();
		}
		
		public function get_all_tematik(){
			$query = $this->db
						->from('m_pelajaran_tematik')
						->where('id_sekolah', get_id_sekolah())
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function delete($id){
			$this->db->delete('m_silabus_tematik', array('id_silabus_tematik'=>$id));
		}
		
		public function insert($nama_silabus_tematik, $id, $id_sekolah, $lokasi_file){
			$this->db->query("INSERT INTO m_silabus_tematik(nama_silabus_tematik, id_pelajaran_tematik, id_sekolah, lokasi_file) VALUES ('$nama_silabus_tematik', '$id', '$id_sekolah', '$lokasi_file')");
		}
}