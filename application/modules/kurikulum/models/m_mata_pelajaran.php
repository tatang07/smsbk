<?php
class m_mata_pelajaran extends MY_Model {
    public $table = "m_mata_pelajaran";
    public $id = "id_mata_pelajaran";
	
	//join key
	public $join_to = array(
							"m_program a"=>"a.id_program=m_mata_pelajaran.id_program",
							"m_pelajaran b"=>"b.id_pelajaran=m_mata_pelajaran.id_pelajaran",
							"m_tingkat_kelas c"=>"c.id_tingkat_kelas=m_mata_pelajaran.id_tingkat_kelas"
					  );
	
	//join field
	public $join_fields = array(
							"a.program program",
							"b.pelajaran pelajaran",
							"c.tingkat_kelas tingkat_kelas"
						  );
}