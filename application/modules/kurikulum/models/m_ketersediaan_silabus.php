<?php
class m_ketersediaan_silabus extends MY_Model {

	// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_peminatan as id_program,id_peminatan FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran group by p.id_pelajaran ,id_tingkat_kelas, id_peminatan
	
	function select_data(){
	
			$this->db->select('id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas, id_peminatan as id_program,id_peminatan'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_silabus s');
			$this->db->join('m_pelajaran p', 's.id_pelajaran = p.id_pelajaran');
			$this->db->where('s.id_kurikulum', 2);
			$this->db->group_by("p.id_pelajaran ,id_tingkat_kelas, id_peminatan,pelajaran");
			return $this->db->get();
		}
	function select_data2(){
	
			$this->db->select('id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas, id_peminatan as id_program,id_peminatan'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_silabus s');
			$this->db->join('m_pelajaran p', 's.id_pelajaran = p.id_pelajaran');
			$this->db->where('s.id_kurikulum', 1);
			$this->db->group_by("p.id_pelajaran ,id_tingkat_kelas, id_peminatan,pelajaran");
			return $this->db->get();
		}
	
	function get_jenis_kelas($id){
	
			$this->db->select('*'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_tingkat_kelas');
			$this->db->where('id_jenjang_sekolah', $id);
			return $this->db->get();
		}
	
	function get_jenis_peminatan_sma(){
			$query = $this->db->query("SELECT *,count(id_peminatan) as banyak_peminatan FROM m_pelajaran WHERE id_tingkat_kelas=10 OR id_tingkat_kelas=11 OR id_tingkat_kelas=12 GROUP BY id_peminatan");
			return $query;
	}
	function get_jenis_peminatan_smp(){
			$query = $this->db->query("SELECT *,count(id_peminatan) as banyak_peminatan FROM m_pelajaran WHERE id_tingkat_kelas=7 OR id_tingkat_kelas=8 OR id_tingkat_kelas=9 GROUP BY id_peminatan");
			return $query;
	}
	function get_jenis_peminatan_sd(){
			$query = $this->db->query("SELECT *,count(id_peminatan) as banyak_peminatan FROM m_pelajaran WHERE id_tingkat_kelas=1 OR id_tingkat_kelas=2 OR id_tingkat_kelas=3 OR id_tingkat_kelas=4 OR id_tingkat_kelas=5 OR id_tingkat_kelas=6 GROUP BY id_peminatan");
			return $query;
	}

	function get_colspan($id_tingkat_kelas, $id_kurikulum){
			$query = $this->db->query("SELECT p.*, k.* FROM m_pelajaran p left join m_kelompok_pelajaran k on p.id_kelompok_pelajaran=k.id_kelompok_pelajaran WHERE p.id_tingkat_kelas=$id_tingkat_kelas and k.id_kurikulum=$id_kurikulum GROUP BY p.id_peminatan");
			return $query;
	}
	function get_nama_peminatan($id_peminatan){
			$query = $this->db->query("SELECT peminatan_singkatan FROM m_peminatan WHERE id_peminatan=$id_peminatan ");
			return $query;
	}
	function select_pelajaran_sma(){
			$query = $this->db->query("SELECT p.*, k.* FROM m_pelajaran p left join m_kelompok_pelajaran k on p.id_kelompok_pelajaran=k.id_kelompok_pelajaran WHERE p.id_tingkat_kelas=10 OR p.id_tingkat_kelas=11 OR p.id_tingkat_kelas=12 GROUP BY pelajaran " );
			return $query;
	}
	function select_pelajaran_sma2(){
			$query = $this->db->query("SELECT p.*, k.* FROM m_pelajaran p left join m_kelompok_pelajaran k on p.id_kelompok_pelajaran=k.id_kelompok_pelajaran WHERE p.id_tingkat_kelas=10 OR p.id_tingkat_kelas=11 OR p.id_tingkat_kelas=12  " );
			return $query;
	}
	function select_pelajaran_smp(){
			$query = $this->db->query("SELECT p.*, k.* FROM m_pelajaran p left join m_kelompok_pelajaran k on p.id_kelompok_pelajaran=k.id_kelompok_pelajaran WHERE p.id_tingkat_kelas=7 OR p.id_tingkat_kelas=8 OR p.id_tingkat_kelas=9 GROUP BY pelajaran");
			return $query;
	}
	function select_pelajaran_smp2(){
			$query = $this->db->query("SELECT p.*, k.* FROM m_pelajaran p left join m_kelompok_pelajaran k on p.id_kelompok_pelajaran=k.id_kelompok_pelajaran WHERE p.id_tingkat_kelas=7 OR p.id_tingkat_kelas=8 OR p.id_tingkat_kelas=9 ");
			return $query;
	}
	function select_pelajaran_sd(){
			$query = $this->db->query("SELECT p.*, k.* FROM m_pelajaran p left join m_kelompok_pelajaran k on p.id_kelompok_pelajaran=k.id_kelompok_pelajaran WHERE p.id_tingkat_kelas=1 OR p.id_tingkat_kelas=2 OR p.id_tingkat_kelas=3 OR p.id_tingkat_kelas=4 OR p.id_tingkat_kelas=5 OR p.id_tingkat_kelas=6 GROUP BY pelajaran");
			return $query;
	}
	function select_pelajaran_sd2(){
			$query = $this->db->query("SELECT p.*, k.* FROM m_pelajaran p left join m_kelompok_pelajaran k on p.id_kelompok_pelajaran=k.id_kelompok_pelajaran WHERE p.id_tingkat_kelas=1 OR p.id_tingkat_kelas=2 OR p.id_tingkat_kelas=3 OR p.id_tingkat_kelas=4 OR p.id_tingkat_kelas=5 OR p.id_tingkat_kelas=6 ");
			return $query;
	}
	
	function get_silabus_kurtilas($id_sekolah){
			$query = $this->db->query("SELECT * FROM m_silabus as s JOIN m_pelajaran as p ON s.id_pelajaran=p.id_pelajaran WHERE s.id_sekolah=$id_sekolah AND s.id_kurikulum=2");
			return $query;
	}
	
	function get_silabus_ktsp($id_sekolah){
			$query = $this->db->query("SELECT * FROM m_silabus as s JOIN m_pelajaran as p ON s.id_pelajaran=p.id_pelajaran WHERE s.id_sekolah=$id_sekolah AND s.id_kurikulum=1");
			return $query;
	}
	public function get_all_kurikulum(){
			$query = $this->db
						->from('m_kurikulum')
						->order_by('id_kurikulum desc')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
	}


}