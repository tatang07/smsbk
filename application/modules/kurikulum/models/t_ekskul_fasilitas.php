<?php
class t_ekskul_fasilitas extends MY_Model {
    public $table = "t_ekstra_kurikuler_fasilitas";
    public $id = "id_ekstra_kurikuler_fasilitas";
	
	// join ON 
	public $join_to = array(
			"m_ekstra_kurikuler e" => "t_ekstra_kurikuler_fasilitas.id_ekstra_kurikuler = e.id_ekstra_kurikuler"
		 );

	public $join_fields = array("jenis");
}