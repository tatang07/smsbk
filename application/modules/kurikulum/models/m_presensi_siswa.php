<?php
class m_presensi_siswa extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_datalist($id_gmr, $limit, $start){

			$this->db->limit($limit, $start);
			$this->db->select('*, count(ja.id_jenis_absen) as jumlah_absen');
			$this->db->from('m_siswa s'); 
			
			$this->db->join('t_siswa_absen sa', 'sa.id_siswa=s.id_siswa', 'left');
			$this->db->join('r_jenis_absen ja', 'sa.id_jenis_absen=ja.id_jenis_absen', 'left');
			$this->db->join('t_agenda_kelas ak', 'sa.id_agenda_kelas=ak.id_agenda_kelas', 'left');
			$this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=ak.id_guru_matpel_rombel', 'left');
			
			$this->db->where('gm.id_guru_matpel_rombel',$id_gmr);
			$this->db->group_by('s.nis');         
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_datalist($id_gmr){

			$this->db->select('*, count(sa.id_siswa_absen) as jumlah_absen');
			$this->db->from('m_siswa s'); 
			
			$this->db->join('t_siswa_absen sa', 'sa.id_siswa=s.id_siswa', 'left');
			$this->db->join('r_jenis_absen ja', 'sa.id_jenis_absen=ja.id_jenis_absen', 'left');
			$this->db->join('t_agenda_kelas ak', 'sa.id_agenda_kelas=ak.id_agenda_kelas', 'left');
			$this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=ak.id_guru_matpel_rombel', 'left');
			
			$this->db->where('gm.id_guru_matpel_rombel',$id_gmr);
			$this->db->group_by('s.nis');         
			$query = $this->db->get();
					
			return $query->num_rows();
			
		}
		
		public function get_all_guru(){
			$query = $this->db->get('m_guru');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_siswa(){
			$query = $this->db->get('m_siswa');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_absen(){
			$query = $this->db->get('t_siswa_absen');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_siswa_by($id){
			$query = $this->db->query("SELECT * FROM m_siswa WHERE id_siswa='$id'");
			return $query->row();
		}
		
		public function get_first_siswa(){
			$query = $this->db->query("SELECT * FROM m_siswa ORDER BY id_siswa ASC LIMIT 1;");
			return $query->row();
		}
		
		public function get_all_rombel(){
			$query = $this->db->get('m_rombel');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_rombel_by($id){
			$query = $this->db->query("SELECT * FROM m_rombel WHERE id_rombel='$id'");
			return $query->row();
		}
		
		public function get_first_rombel(){
			$query = $this->db->query("SELECT * FROM m_rombel ORDER BY id_rombel ASC LIMIT 1;");
			return $query->row();
		}
		
		public function get_all_siswa_absen(){
			$query = $this->db->get('t_siswa_absen');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_agenda_kelas(){
			$query = $this->db->get('t_agenda_kelas');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_pelajaran(){
			$query = $this->db->get('m_pelajaran');
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_pelajaran_by($id){
			$query = $this->db->query("SELECT * FROM m_pelajaran WHERE id_pelajaran='$id'");
			return $query->row();
		}
		
		public function get_first_pelajaran(){
			$query = $this->db->query("SELECT * FROM m_pelajaran ORDER BY id_pelajaran ASC LIMIT 1;");
			return $query->row();
		}
		
}