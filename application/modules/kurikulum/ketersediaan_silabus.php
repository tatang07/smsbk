<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class ketersediaan_silabus extends CI_Controller {

	public function index()
	{	
		$this->load->model('kurikulum/m_ketersediaan_silabus');
		$data['data'] = $this->m_ketersediaan_silabus->select_data()->result_array();
		$data['pelajaran'] = $this->m_ketersediaan_silabus->select_pelajaran()->result_array();
		$data['kelas'] = $this->m_ketersediaan_silabus->select_kelas()->result_array();
		$data['program'] = $this->m_ketersediaan_silabus->select_program()->result_array();
		
		$silabus=array();
		foreach($data['pelajaran'] as $pel){
			foreach($data['kelas'] as $kel){
				foreach($data['program'] as $pro) {
				$silabus[$pel['kode_pelajaran']."|".$pel['pelajaran']][$kel['id_tingkat_kelas']][$pro['id_program']] = 'Tidak ada';
				}
			}
		}
		//print_r ($silabus);
		
		foreach($data['data'] as $d){
			$silabus[$d['kode_pelajaran']."|".$d['pelajaran']][$d['id_tingkat_kelas']][$d['id_program']]='Ada';
		}
		
		// print_r ($silabus);
		$data['hasil']=$silabus;
		render('ketersediaan_silabus/ketersediaan_silabus',$data);
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */