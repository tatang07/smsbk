
<?php 
	$statuspil = 0;
	if(isset($_POST['id_kurikulum'])){
	$a = $_POST['id_kurikulum'];
	$statuspil = $a; } ?>

<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Daftar Pelajaran</h2>
		</div>
		<div class="tabletools">
			
			<div class="dataTables_filter">
				<form action="" name= "myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=150px align='left'><span class="text"><span>Jenis Kurikulum</span></th>
							<td width=200px align='left'>
								<select name="id_kurikulum" id="id_kurikulum" onchange="submitform();">
									<option value="0">-Pilih Kurikulum-</option>
								<?php foreach($kur as $q): ?>
									
									<?php if($q->id_kurikulum == $statuspil){ ?>
										<option selected value='<?php echo $q->id_kurikulum; ?>' data-status-pilihan="<?php echo $q->id_kurikulum; ?>"><?php echo $q->nama_kurikulum; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $q->id_kurikulum; ?>' data-status-pilihan="<?php echo $q->id_kurikulum; ?>"><?php echo $q->nama_kurikulum; ?></option>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
				
				<form action="<?php echo base_url('kurikulum/daftar_pelajaran/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<input type="hidden" name="id_kurikulum" value="<?php echo $statuspil; ?>">
						<tr>
							<th width=100px align='left'><span class="text"><span>Tingkat Kelas</span></th>
							<td width=200px align='left'>
								<select name="id_tingkat_kelas">
									<option value="0">-Pilih Kelas-</option>
								<?php foreach($tingkat_kelas as $t): ?>
									<?php if($t->id_tingkat_kelas == $id_tingkat_kelas){ ?>
										<option selected value="<?php echo $t->id_tingkat_kelas; ?>"><?php echo $t->tingkat_kelas; ?></option>
									<?php }else{ ?>
										<option value="<?php echo $t->id_tingkat_kelas; ?>"><?php echo $t->tingkat_kelas; ?></option>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
						</tr>
						<tr>
							<th width=100px align='left'><span class="text"><span>Kelompok Pelajaran</span></th>
							<td width=200px align='left'>
								<select name="id_kelompok_pelajaran" data-placeholder="Pilih Kelompok Pelajaran" id="id_kelompok_pelajaran">
								<option value="0">-Pilih Kelompok Pelajaran-</option>
								<?php foreach($kelompok as $t): ?>
									<?php if($t->id_kurikulum == $statuspil){ ?>
										<?php if($jenjang == 4){ ?>
											<?php if($t->id_kelompok_pelajaran == $id_kelompok_pelajaran){ ?>
												<option selected value="<?php echo $t->id_kelompok_pelajaran; ?>" data-status-pilihan="<?php echo $t->status_pilihan; ?>"><?php echo $t->kelompok_pelajaran; ?></option>
											<?php }else{ ?>
												<option value="<?php echo $t->id_kelompok_pelajaran; ?>" data-status-pilihan="<?php echo $t->status_pilihan; ?>"><?php echo $t->kelompok_pelajaran; ?></option>
											<?php } ?>
										<?php }else{ ?>
											
												<?php if($t->id_kelompok_pelajaran == $id_kelompok_pelajaran){ ?>
													<option selected value="<?php echo $t->id_kelompok_pelajaran; ?>" data-status-pilihan="<?php echo $t->status_pilihan; ?>"><?php echo $t->kelompok_pelajaran; ?></option>
												<?php }else{ ?>
													<option value="<?php echo $t->id_kelompok_pelajaran; ?>" data-status-pilihan="<?php echo $t->status_pilihan; ?>"><?php echo $t->kelompok_pelajaran; ?></option>
											
											<?php } ?>
										<?php } ?>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
						</tr>
						
						<tr id="field-peminatan" <?php if($status_pilihan == 0){ ?> style="display:none;" <?php } ?>>
							<th width=100px align='left'><span class="text"><span>Peminatan</span></th>
							<td width=200px align='left'>
								<select name="id_peminatan">
								<?php foreach($peminatan as $t): ?>
									<?php if($t->id_kurikulum == $statuspil){ ?>
										<?php if($t->id_peminatan == $id_peminatan){ ?>
											<option selected value="<?php echo $t->id_peminatan; ?>"><?php echo $t->peminatan; ?></option>
										<?php }else{ ?>
											<option value="<?php echo $t->id_peminatan; ?>"><?php echo $t->peminatan; ?></option>
										<?php } ?>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
						</tr>
						<tr>
							<td width=150px align='left'><input type="submit" name=send class="button grey tooltip" value="cari" /></td>
						</tr>
					</table>
				</form>
			</div>

		</div>
	</div>
	
	<?php if($posisi=='search'){ ?>
	<div class="box with-table">
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode Pelajaran</th>
								<th>Mata Pelajaran</th>
								<th>Jumlah Jam/Minggu</th>
								<?php if($gid == 1 || $gid == 2 || $gid == 3){ ?>
									<th>Aksi</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php $no=$page+1; ?>
							<?php if($pelajaran){ ?>
							<?php foreach($pelajaran as $p): ?>
							<tr>
								<td width="50px" class= 'center'><?php echo $no; ?></td>
								<td width="100px" class= 'center'><?php echo $p->kode_pelajaran; ?></td>
								<td width="400px" class= 'left'><?php echo $p->pelajaran; ?></td>
								<td width="100px" class= 'center'><?php echo $p->jumlah_jam; ?></td>
								<?php if(($gid == 1 || ($gid == 2 && $p->id_sekolah != 0) || ($gid == 3 && $p->id_sekolah != 0))){ ?>
								<td width="" class= 'center'>
									<a href="<?php echo base_url('kurikulum/daftar_pelajaran/edit/'.$p->id_pelajaran); ?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a href="<?php echo base_url('kurikulum/daftar_pelajaran/delete/'.$p->id_pelajaran); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Delete</a>
								</td>
								<?php }else{ ?>
									<td width="" class= 'center'>-</td>
								<?php } ?>
									
							</tr>
							<?php $no++; ?>
							<?php endforeach; } ?>
						</tbody>
					</table>
					<div class="footer">
					<div class="dataTables_info">Jumlah Data: <?php echo $jumlah; ?></div>
					<div class="dataTables_paginate paging_full_numbers">
						<p><?php if($links){echo $links;} ?></p>
					</div>
				</div>
				</div>
			</div>
		</div>        
	</div>
	<?php } ?>
</div>

<script>
	$('#id_kelompok_pelajaran').on('change', function(){
		if($(this).find("option:selected").data('status-pilihan') == 1){
			$('#field-peminatan').show();
		} else {
			$('#field-peminatan').hide();
		}
	});
</script>


<script>
	
	function submitform()
	{
	  document.myform.submit();
	}
</script>