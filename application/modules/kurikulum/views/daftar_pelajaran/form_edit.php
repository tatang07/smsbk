	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/daftar_pelajaran/submit_post'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Edit Daftar Pelajaran</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kode Pelajaran</strong>
						</label>
						<div>
							<input type="text" name="kode_pelajaran" value="<?php echo $kode_pelajaran; ?>" />
							<input type="hidden" name="id_pelajaran" value="<?php echo $id_pelajaran; ?>" />
							<input type="hidden" name="action" value="update" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Pelajaran</strong>
						</label>
						<div>
							<input type="text" name="pelajaran" value="<?php echo $pelajaran; ?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_input">
							<strong>Jumlah Jam</strong>
						</label>
						<div>
							<input type="text" name="jumlah_jam" value="<?php echo $jumlah_jam; ?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Tingkat Kelas</strong>
						</label>
						<div>
							<select name="id_tingkat_kelas" data-placeholder="Choose a Name">
							<?php foreach($tingkat_kelas as $t): ?>
								<?php if($t->id_tingkat_kelas == $id_tingkat_kelas){ ?>
									<option selected value="<?php echo $t->id_tingkat_kelas; ?>"><?php echo $t->tingkat_kelas; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $t->id_tingkat_kelas; ?>"><?php echo $t->tingkat_kelas; ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Kelompok Pelajaran</strong>
						</label>
						<div>
							<select name="id_kelompok_pelajaran" data-placeholder="Choose a Name" id="id_kelompok_pelajaran">
							<?php foreach($kelompok_pelajaran as $k): ?>
								<?php if($k->id_kelompok_pelajaran == $id_kelompok_pelajaran){ ?>
									<option selected value="<?php echo $k->id_kelompok_pelajaran; ?>" data-status-pilihan="<?php echo $k->status_pilihan; ?>"><?php echo $k->kelompok_pelajaran; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $k->id_kelompok_pelajaran; ?>" data-status-pilihan="<?php echo $k->status_pilihan; ?>"><?php echo $k->kelompok_pelajaran; ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Kualifikasi Guru</strong>
						</label>
						<div>
							<select name="id_kualifikasi_guru" data-placeholder="Choose a Name">
							<?php foreach($kualifikasi_guru as $k): ?>
								<?php if($k->id_kualifikasi_guru == $id_kualifikasi_guru){ ?>
									<option selected value="<?php echo $k->id_kualifikasi_guru; ?>"><?php echo $k->kualifikasi_guru; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $k->id_kualifikasi_guru; ?>"><?php echo $k->kualifikasi_guru; ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row" id="field-peminatan" style="display:none;">
						<label for="f2_select2">
							<strong>Peminatan</strong>
						</label>
						<div>
							<select name="id_peminatan" data-placeholder="Choose a Name">
							<?php foreach($peminatan as $k): ?>
								<?php if($k->id_peminatan == $id_peminatan){ ?>
									<option selected value="<?php echo $k->id_peminatan; ?>"><?php echo $k->peminatan; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $k->id_peminatan; ?>"><?php echo $k->peminatan; ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/daftar_pelajaran/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->


<script>
		$('#id_kelompok_pelajaran').on('change', function(){
			if($(this).find("option:selected").data('status-pilihan') == 1){
				$('#field-peminatan').show();
			} else {
				$('#field-peminatan').hide();
			}
		});
		</script>