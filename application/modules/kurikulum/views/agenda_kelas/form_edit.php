	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/agenda_kelas/update/'.$agenda_kelas->id_agenda_kelas); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Kehadiran Siswa</legend>
					
					<div class="row">
						<label>
							<strong>Tanggal</strong>
						</label>
						<div>
							<input type="date" name='tanggal' 
								value="<?php echo mdate('%d/%m/%Y', strtotime($agenda_kelas->tanggal)); ?>" 
								required>
						</div>
					</div>
					
					<div class="row">
						<label>
							<strong>Materi</strong>
						</label>
						<div>
							<input type='text' name='materi' 
								value='<?php echo $agenda_kelas->materi ?>' 
								required>
						</div>
					</div>

					<div class="row">
						<label>
							<strong>Proses Belajar</strong>
						</label>
						<div>
							<input type='text' name='proses_belajar' 
								value='<?php echo $agenda_kelas->proses_belajar ?>' 
								required>
						</div>
					</div>
						
					<div class="row">
						<label>
							<strong>Pelajaran</strong>
						</label>
						<div>
							<select name="id_pelajaran" data-placeholder="Pilih Pelajaran">
							<?php foreach($pelajaran as $p): ?>
									<option value="<?php echo $p->id_pelajaran; ?>"
										<?php 
											if($p->id_pelajaran == $guru_matpel_rombel[0]->id_pelajaran){
												echo 'selected';
											}
										 ?>
										>
										<?php echo $p->kode_pelajaran.' - '.$p->pelajaran; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>

					<div class="row">
						<label>
							<strong>Guru</strong>
						</label>
						<div>
							<select name="id_guru" data-placeholder="Pilih Guru">
							<?php foreach($guru as $p): ?>
									<option value="<?php echo $p->id_guru; ?>"
										<?php 
											if($p->id_guru == $guru_matpel_rombel[0]->id_guru){
												echo 'selected';
											}
										 ?>
										>
										<?php echo $p->nama ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>

					<div class="row">
						<label>
							<strong>Kelas</strong>
						</label>
						<div>
							<select name="id_rombel" data-placeholder="Pilih Kelas">
							<?php foreach($rombel as $p): ?>
									<option value="<?php echo $p->id_rombel; ?>"
										<?php 
											if($p->id_rombel == $guru_matpel_rombel[0]->id_rombel){
												echo 'selected';
											}
										 ?>
										>
										<?php echo $p->rombel; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
				</fieldset><!-- End of fieldset -->
					<div class="actions">
						<div class="left">
							<input type="submit" value="simpan" name=simpan />
							 <a href="<?php echo base_url('kurikulum/agenda_kelas/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		