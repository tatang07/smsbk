<?php $rombel = array("0"=>"-Pilih-")+get_rombel() ?>
<?php $pelajaran = get_pelajaran_guru_matpel_rombel() ?>	
	
	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Kehadiran Siswa</legend>
					<!-- ini kenapa gak berfungsi ya id_term gak muncul? 
						<?php echo 'asdf'.get_id_term(); ?>
					-->

					<div class="row">
						<label for="f2_select2">
							<strong>Kelas</strong>
						</label>
						<div>
							<?php echo simple_form_dropdown_clear("id_rombel",$rombel,$id_rombel," id='idrombel' onChange=rombelChanged(this.value)",null) ?>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Mata Pelajaran</strong>
						</label>
						<div>
							<?php $mapel =  array("0"=>"-Pilih-") ?>
							<?php echo simple_form_dropdown_clear("id_gmr",$pelajaran_dropdown,$id_gmr,"",null) ?>
						</div>
					</div>

					<div class="row">
						<label>
							<strong>Tanggal</strong>
						</label>
						<div>
							<input type="date" name='tanggal' value=<?php if($tanggal==''){echo date('d/m/Y');}else{echo date("d/m/Y", strtotime($tanggal));}?>>
						</div>
					</div>
					
					<div class="row">
						<label>
							<strong>Materi</strong>
						</label>
						<div>
							<textarea name='materi'><?php echo $materi;?></textarea>
							<!-- <input type='text' name='materi' required> -->
						</div>
					</div>

					<div class="row">
						<label>
							<strong>Proses Belajar</strong>
						</label>
						<div>
							<textarea name='proses_belajar'><?php echo $proses_belajar;?></textarea>
							<!-- <input type='text' name='proses_belajar' required> -->
						</div>
					</div>
				</fieldset><!-- End of fieldset -->
				
				<?php if($id_rombel && $siswa): ?>
				<fieldset>
					<legend>Siswa</legend>
					<table class="styled" >
						<thead>
							<tr>
								<th>No. <?php if($this->uri->segment(6)){echo 'ada';}else{echo 'tidak ada';}; ?></th>
								<th>NIS</th>
								<th>Nama</th>
								<?php if($id_gmr): ?>
								<th colspan=<?php echo $count_jenis_absen?>>Kehadiran</th>
								<?php endif; ?>
							</tr>
							
						</thead>
						<tbody>
							<?php $no=1; foreach($siswa as $s): ?>
								<tr>
									<td width="50px" class="center"><?php echo $no?></td>
									<td width="100px" class="center"><?php echo $s['nis'] ?></td>
									<td width="300px" class=""><?php echo $s['nama'] ?></td>
									<?php if($id_gmr): ?>
									<?php foreach($jenis_absen as $j): ?>
										<td width="50px" class="center"><div><input type="radio" name="absen[<?php echo $s['id_siswa'];?>]" value="<?php echo $j->id_jenis_absen;?>" <?php if($this->uri->segment(6)){ if($siswa_absen){ foreach($siswa_absen as $sa){ if($sa->id_siswa == $s['id_siswa'] && $sa->id_jenis_absen == $j->id_jenis_absen){ echo "checked"; } } } }else{ if($j->id_jenis_absen==1){ echo 'checked';} } ?> /><label><?php echo $j->jenis_absen; ?></label></div></td>
									<?php endforeach; ?>
									<?php endif; ?>
								</tr>
								<?php $no++; endforeach; ?>
						</tbody>
					</table>
				</fieldset><!-- End of fieldset -->
				<?php endif; ?>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="simpan" name=simpan />
							 <a href="<?php echo base_url('kurikulum/agenda_kelas/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
<!--		
<script>
$(function(){
	$(document).on('change', '#guru', function(){
		var id_guru = $(this).val();
		$.post("<?php echo site_url('kurikulum/agenda_kelas/get_mapel_guru'); ?>/" + id_guru, function(data){
			$("#pelajaran").html(data);
			$("#pelajaran").trigger("chosen:updated");
		});
	});

	$(document).on('change', '#pelajaran', function(){
		var id_pelajaran = $(this).val();
		$.post("<?php echo site_url('kurikulum/agenda_kelas/get_mapel_rombel'); ?>/" + id_pelajaran, function(data){
			$("#rombel").html(data);
			$("#rombel").trigger("chosen:updated");
		});
	});
	
	$(document).on('change', '#rombel', function(){
		var id_rombel = $(this).val();
		$.post("<?php echo site_url('kurikulum/agenda_kelas/get_siswa_rombel'); ?>/" + id_rombel, function(data){
			$("#siswa").html(data);
			$("#siswa").trigger("chosen:updated");
		});
	});
});
</script>
-->

<script type="text/javascript">
	$(function(){
		$('#id_rombel').chosen().change(function(){
			window.location = "<?php echo site_url('kurikulum/agenda_kelas/add').'/'; ?>" + $(this).val();
		});
		$('#id_gmr').chosen().change(function(){
			window.location = "<?php echo site_url('kurikulum/agenda_kelas/add/'.$this->uri->segment(4)).'/'; ?>" + $(this).val();
		});
	});
</script>