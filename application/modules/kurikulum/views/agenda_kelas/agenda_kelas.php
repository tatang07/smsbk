<?php if($gid == 9){
		$rombel = array("0"=>"-Pilih-")+get_rombel_guru_matpel_rombel_by_guru(get_id_personal());
	}else{
		$rombel = array("0"=>"-Pilih-")+get_rombel();
	}
		?>
<?php $pelajaran = get_pelajaran_guru_matpel_rombel() ?>

<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
	<div class="box with-table">
		
		<div class="tabletools">
		<table><tr>
			<th width=100px align='left'><span class="text">Kelas</span></th>
			
			<td width=150px align='left'>
				<?php echo simple_form_dropdown_clear("id_rombel",$rombel,$id_rombel," id='idrombel' onChange=rombelChanged(this.value)",null)
				?>
			</td>
		</tr></table>

		</div>
	</div>
		<div class="box tabbedBox">

			<div class="header">
				<h2>Agenda Kelas</h2>
				<ul>
					<li><a href="#t1-c1">Harian</a></li>
					<li><a href="#t1-c2">Mata Pelajaran</a></li>
					
				</ul>
			</div><!-- End of .header -->

			<div class="content tabbed">

				<div id="t1-c1">
					<div class="tabletools">
						<div class="right">
							<a href="<?php echo site_url('kurikulum/agenda_kelas/add'); ?>"><i class="icon-plus"></i>Tambah</a> 
							<br/><br/>
						</div>
						<div class="dataTables_filter">
							<form action="<?php echo base_url('kurikulum/agenda_kelas/search'); ?>"
								id="formPencarian" name="formPencarian" method="post">
								<table>
									<tr>
										<th width=30px align='left'><span class="text">Tanggal</span></th>
										<td width=100px align='left'>
											<input type="hidden" id="rombel_1" name="id_rombel" value="<?php echo $id_rombel; ?>" />
											<input type="date" name="tanggal" id='f3_date' />
										</td>					
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<td width=1px>
											<button type="submit" original-title='Cari' class="button grey tooltip" name=cariharian>
												<i class='icon-search'></i>
											</button>
										</td>
									</tr>
								</table>
							</form>
							<?php if($id_rombel > 0){
								?>
							<form action="<?php echo base_url('kurikulum/agenda_kelas/search#gantiKM'); ?>"
								id="formKM" name="formKM" method="post">
								<table>
									<tr>
										<th width=30px align='left'><span class="text">KM</span></th>
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<th>:</th>
										<td width=1px>&nbsp;</td>
										<?php if($km != "Belum ada KM"){ ?>
											<th width=100px align='left'>
											<span class="text"><?php foreach ($km as $k) {
											echo $k->nama;
											}  ?></span>
										</th>	
										<?php }else{?>
										<th width=100px align='left'>
											<span class="text"><?php echo $km; ?></span>
										</th>
										<?php }?>
										<td width=19px></td>
										<input type="hidden" id="rombel_1" name="id_rombel" value="<?php echo $id_rombel; ?>" />					
										<td width=100px>
											<button type="submit" class="button grey tooltip" name="gantiKM">
												<?php if($km != "Belum ada KM"){ ?>
													<b>Ganti KM</b>
												<?php }else{?>
													<b>Pilih KM</b>
												<?php }?>

											</button>
										</td>
									</tr>
								</table>
							</form>
							<?php } ?>
						
						</div>
					</div>
						
						
						
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">
								<table class="styled" >
									<thead>
										<tr>
											<th>No.</th>
											<th>Kode Mata Pelajaran</th>
											<th>Mata Pelajaran</th>
											<th>Materi</th>
											<th>Proses Belajar</th>
											<?php if($gid == 2 || $gid == 3 || $gid == 4 || $gid == 9){ ?>
												<th>Aksi</th>
											<?php } ?>
										</tr>
									</thead>
									
									<?php if($aksi=='search'){ ?>
									
									<tbody>
										<?php $no = 1; ?>
										<?php if($agenda_harian){ ?>
											<?php foreach($agenda_harian as $ah): ?>
												<?php  //if($ah->id_siswa == $first){ ?>
												<tr>
													<td width='30px' class='center'><?php echo $no; ?></td>
													<td width='100px' class='center'><?php echo $ah->kode_pelajaran ?></td>
													<td width='200px'><?php echo $ah->pelajaran; ?></td>
													<td width='200px'><?php echo $ah->materi; ?></td>
													<td width='100px'><?php echo $ah->proses_belajar; ?></td>
													<?php if($gid == 2 || $gid == 3 || $gid == 4 || ($gid == 9 && $ah->id_guru == get_id_personal())){ ?>
													<td class='center'>
														<a href="<?php echo site_url('kurikulum/agenda_kelas/add/'.$first_rombel.'/'.$ah->id_guru_matpel_rombel.'/'.$ah->id_agenda_kelas); ?>"
															class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
														<a href="<?php echo site_url('kurikulum/agenda_kelas/delete/'.$ah->id_agenda_kelas); ?>"
															onClick="if (confirm('Anda yakin akan menghapus?')) commentDelete(1); return false"
													        class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
													</td>
													<?php } ?>
												</tr>
												<?php $no++; ?>
												<?php //} ?>
											<?php endforeach; ?>
										<?php } ?>
									</tbody>
									<?php }else{ $no=0;?>
									<tbody>
										<tr><td colspan=5>Pilih Kelas dan Tanggal yang ingin dicari</td></tr>
									</tbody>
									<?php } ?>
								</table>
								<div class="footer">
									<?php if($no-1 == 0){ ?>
										<div class="dataTables_info">
											Data tidak ditemukan!
										</div>
									<?php }else {?>
										<?php if($aksi=='search'){ ?>
										<div class="dataTables_info">Jumlah Data: <?php echo $no-1; ?></div>
									<?php }} ?>
									<div class="dataTables_paginate paging_full_numbers">
										<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
										<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
											<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
										</span>
										<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>

										<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
									</div>
								</div>
							</div>
						</div>
					</div>

					<div id="t1-c2">
						<div class="tabletools">
							<div class="right">
								<a href="<?php echo site_url('kurikulum/agenda_kelas/add'); ?>"><i class="icon-plus"></i>Tambah</a> 
								<br/><br/>
							</div>
						<div class="dataTables_filter">
							<form action="<?php echo base_url('kurikulum/agenda_kelas/search#t1-c2'); ?>"
								id="formPencarian" name="formPencarian" method="post">
								<table>
									<tr>
										<input type="hidden" id="rombel_2" name="id_rombel" value="<?php echo $id_rombel; ?>" />
										<th width=80px align='left'><span class="text">Mata Pelajaran</span></th>
										<td width=250px align='left'>
											<?php foreach($rombel as $id=>$det):?>
												<?php $mapel =  array("0"=>"-Pilih-") ?>
												<?php $mapel += isset($pelajaran[$id])?$pelajaran[$id]:array() ?>
												<div id='mapel<?php echo $id?>' <?php echo $id_rombel!=$id?"style='display:none'":""?> class='listmapel'>					
													<?php echo simple_form_dropdown_clear("id_gmr".$id,$mapel,$id_gmr,"",null) ?>
												</div>	
											<?php endforeach ?>
										</td>
										<td>&nbsp;</td>
										<td>&nbsp;</td>
										<td>
										<input type="submit" class="button grey tooltip" value="cari" name=caripelajaran />
										<!-- <a href="search" class="button grey tooltip"><i class='icon-search'></i></a> -->
										</td>
									</tr>
								</table>
							</form>
							<?php if($id_rombel > 0){
								?>
							<form action="<?php echo base_url('kurikulum/agenda_kelas/search#gantiKM'); ?>"
								id="formKM" name="formKM" method="post">
								<table>
									<tr>
										<th width=30px align='left'><span class="text">KM</span></th>
										<td width=1px>&nbsp;</td>
										<td width=1px>&nbsp;</td>
										<th>:</th>
										<td width=1px>&nbsp;</td>
										<?php if($km != "Belum ada KM"){ ?>
											<th width=100px align='left'>
											<span class="text"><?php foreach ($km as $k) {
											echo $k->nama;
											}  ?></span>
										</th>	
										<?php }else{?>
										<th width=100px align='left'>
											<span class="text"><?php echo $km; ?></span>
										</th>
										<?php }?>
										<td width=19px></td>
										<input type="hidden" id="rombel_1" name="id_rombel" value="<?php echo $id_rombel; ?>" />					
										<td width=100px>
											<button type="submit" class="button grey tooltip" name="gantiKM">
												<?php if($km != "Belum ada KM"){ ?>
													<b>Ganti KM</b>
												<?php }else{?>
													<b>Pilih KM</b>
												<?php }?>

											</button>
										</td>
									</tr>
								</table>
							</form>
							<?php } ?>
						</div>
					</div>

						
						
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">    
								<table class="styled" >
									<thead>
										<tr>
											<th>No.</th>
											<th>Tanggal</th>
											<th>Materi</th>
											<th>Proses Belajar</th>
											<?php if($gid == 2 || $gid == 3 || $gid == 4 || $gid == 9){ ?>
												<th>Aksi</th>
											<?php } ?>
										</tr>
									</thead>
									<?php if($aksi=='search'){ ?>
									<tbody>
										<?php $no = 1; ?>
										<?php if($agenda_kelas){ ?>
											<?php foreach($agenda_kelas as $ak): ?>
												<?php  //if($ak->id_siswa == $first){ ?>
												<tr>
													<td width="50px" class="center"><?php echo $no; ?></td>
												<td width="100px" class="center"><?php echo $ak->tanggal; ?></td>
														<td width="400px" class=""><?php echo $ak->materi; ?></td>
													<td width="100px" class="center"><?php echo $ak->proses_belajar; ?></td>
													<!-- addlink please -->
													<?php if($gid == 2 || $gid == 3 || $gid == 4 || ($gid == 9 && $ak->id_guru == get_id_personal())){ ?>
													<td class='center'>
														<a href="<?php echo site_url('kurikulum/agenda_kelas/add/'.$first_rombel.'/'.$ak->id_guru_matpel_rombel.'/'.$ak->id_agenda_kelas); ?>"
															class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
														<a href="<?php echo site_url('kurikulum/agenda_kelas/delete/'.$ak->id_agenda_kelas); ?>"
															onClick="if (confirm('Anda yakin akan menghapus?')) commentDelete(1); return false"
													        class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
													</td>
													<?php } ?>
												</tr>
												<?php $no++; ?>
												<?php //} ?>
											<?php endforeach; ?>
										<?php } ?>
									</tbody>
									<?php }else{ ?>
									<tbody>
										<tr><td colspan=5>Pilih Kelas dan Pelajaran yang ingin dicari</td></tr>
									</tbody>
									<?php } ?>
								</table>
								<div class="footer">
									<?php if($no-1 == 0){ ?>
										<div class="dataTables_info">
											Data tidak ditemukan!
										</div>
									<?php }else {?>
										<div class="dataTables_info">Jumlah Data: <?php echo $no-1; ?></div>
									<?php } ?>
									<div class="dataTables_paginate paging_full_numbers">
										<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
										<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
											<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
										</span>
										<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>

										<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
									</div>
								</div>
							</div>
						</div>
					</div>


					<!-- GANTI KM -->
					<div id="gantiKM">
						<form action="<?php echo base_url('kurikulum/agenda_kelas/search#t1-c1'); ?>"
								id="formKM" name="formKM" method="post">
								<table>
									<tr>
										<th width=100px align='left'><span class="text">KM Saat ini</span></th>
										<th> : </th>
										<?php if($km != "Belum ada KM"){ ?>
											<th width=100px align='left'>
											<span class="text"><?php foreach ($km as $k) {
											echo $k->nama;
											}  ?></span>
										</th>	
										<?php }else{?>
										<th width=100px align='left'>
											<span class="text"><?php echo $km; ?></span>
										</th>
										<?php }?>
										
									</tr>
									<tr>
										<th width=30px align='left'><span class="text">KM Baru</span></th>
										<th> : </th>
										<td width=150px align='left'>
											<select name="id_km_kelas" data-placeholder="-- Pilih KM Kelas --" id="id_km_kelas">
												<option value="0">-Pilih KM Kelas-</option>
												<?php foreach($siswa as $k): ?>
												
													<option value="<?php echo $k->id_siswa; ?>" data-status-pilihan="<?php echo $k->id_siswa; ?>"><?php echo $k->nama; ?></option>
													
												<?php endforeach; ?>
											</select>
										</td>
										<input type="hidden" id="rombel_1" name="id_rombel" value="<?php echo $id_rombel; ?>" />
										<td width=100px>
											<button type="submit" class="button grey tooltip" name="pilihKM">
												<b>Pilih</b>
											</button>
										</td>
									</tr>
								</table>
							</form>
					</div>

					<!-- akhir ganti KM -->

				</div>
				<!-- End of .content -->



			</div><!-- End of .box -->
	</div>

	
	<script type="text/javascript">		
		function rombelChanged(id){
			$('.listmapel').hide();
			$('.listkomp').hide();
			if(id>0){
				$('#mapel'+id).show();				
				$('#komp'+id).show();				
				$('#rombel_1').val(id);				
				$('#rombel_2').val(id);				
			}
		}
	</script>