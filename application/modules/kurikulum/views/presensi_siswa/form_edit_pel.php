	<h1 class="grid_12">Struktur Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/struktur_kurikulum/submit_post_pel'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Edit Pelajaran <?php echo $pelajaran; ?></legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kode Pelajaran</strong>
						</label>
						<div>
							<input type="text" name="kode_pelajaran" value="<?php echo $kode_pelajaran; ?>" />
							<input type="hidden" name="id_pelajaran" value="<?php echo $id_pelajaran; ?>" />
							<input type="hidden" name="action" value="update" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Pelajaran</strong>
						</label>
						<div>
							<input type="text" name="pelajaran" value="<?php echo $pelajaran; ?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Kelompok Pelajaran</strong>
						</label>
						<div>
							<select name="id_kelompok_pelajaran" data-placeholder="Choose a Name">
							<?php foreach($kelompok_pelajaran as $k): ?>
								<?php if($k->id_kelompok_pelajaran == $id_kelompok_pelajaran){ ?>
									<option selected value="<?php echo $k->id_kelompok_pelajaran; ?>"><?php echo $k->kelompok_pelajaran; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $k->id_kelompok_pelajaran; ?>"><?php echo $k->kelompok_pelajaran; ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Kualifikasi Guru</strong>
						</label>
						<div>
							<select name="id_kualifikasi_guru" data-placeholder="Choose a Name">
							<?php foreach($kualifikasi_guru as $t): ?>
								<?php if($t->id_kualifikasi_guru == $id_kualifikasi_guru){ ?>
									<option selected value="<?php echo $t->id_kualifikasi_guru; ?>"><?php echo $t->kualifikasi_guru; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $t->id_kualifikasi_guru; ?>"><?php echo $t->kualifikasi_guru; ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					<?php if($id_peminatan != 0){ ?>
					<div class="row">
						<label for="f2_select3">
							<strong>Peminatan</strong>
						</label>
						<div>
							<select name="id_peminatan" data-placeholder="Choose a Name">
							<?php foreach($peminatan as $t): ?>
								<?php if($t->id_peminatan == $id_peminatan){ ?>
									<option selected value="<?php echo $t->id_peminatan; ?>"><?php echo $t->peminatan; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $t->id_peminatan; ?>"><?php echo $t->peminatan; ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					<?php }else{ ?>
						<input type="hidden" name="id_peminatan" value="0" />
					<?php } ?>
						
				</fieldset><!-- End of fieldset -->
				<fieldset>
					<legend>Edit Alokasi Waktu per Minggu untuk Pelajaran <?php echo $pelajaran; ?></legend>
					<?php foreach($alokasi as $p): ?>
					<div class="row">
						<label for="f1_normal_input">
							<strong><?php echo $p['tingkat_kelas']; ?></strong>
						</label>
						<div>
							<input type="text" name="jumlah_jam[]" value="<?php echo $p['jumlah_jam']; ?>" />
							<input type="hidden" name="id_tingkat_kelas[]" value="<?php echo $p['tingkat_kelas']; ?>" />
							<input type="hidden" name="id_alokasi_waktu_per_minggu[]" value="<?php echo $p['id_alokasi_waktu_per_minggu']; ?>" />
						</div>
					</div>
					<?php endforeach; ?>
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/struktur_kurikulum/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		