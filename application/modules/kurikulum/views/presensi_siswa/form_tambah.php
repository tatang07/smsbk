	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/struktur_kurikulum/submit_post'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Kehadiran Siswa</legend>
					
					<div class="row">
						<label for="f1_select">
							<strong>Kelas</strong>
						</label>
						<div>
							<select name="id_rombel" data-placeholder="Pilih Kelas">
							<?php foreach($rombel as $r): ?>
									<option value="<?php echo $r->id_rombel; ?>"><?php echo $r->rombel; ?></option>
							<?php endforeach; ?>
							</select>
							<input type="hidden" name="action" value="add" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select">
							<strong>Pelajaran</strong>
						</label>
						<div>
							<select name="id_pelajaran" data-placeholder="Pilih Pelajaran">
							<?php foreach($pelajaran as $p): ?>
									<option value="<?php echo $p->id_pelajaran; ?>"><?php echo $p->pelajaran; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f3_select">
							<strong>Guru</strong>
						</label>
						<div>
							<select name="id_guru" data-placeholder="Pilih Guru">
							<?php foreach($guru as $p): ?>
									<option value="<?php echo $p->id_guru; ?>"><?php echo $p->nama; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
						
				</fieldset><!-- End of fieldset -->
				
				<fieldset>
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama Siswa</th>
								<th>Kehadiran</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; ?>
							<?php foreach($siswa as $s): ?>
								<tr>
									<td width = "30px" class = "center"><?php echo $no; ?></td>
									<td width = "100px" class = "center"><?php echo $s->nis; ?></td>
									<td width = "400px" class = ""><?php echo $s->nama; ?></td>
									<td width = "100px" class = "center"><input type="checkbox" name = "kehadiran[]" >Hadir</td>
								</tr>
							<?php $no++; ?>
							<?php endforeach; ?>
						</tbody>
					</table>
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/presensi_siswa/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		