	<h1 class="grid_12">Struktur Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/struktur_kurikulum/submit_post'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Edit Kompetensi Inti</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kode Kompetensi Inti</strong>
						</label>
						<div>
							<input type="text" name="kode_kompetensi_inti" value="<?php echo $kode_kompetensi_inti; ?>" />
							<input type="hidden" name="id_kompetensi_inti" value="<?php echo $id_kompetensi_inti; ?>" />
							<input type="hidden" name="action" value="update" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Kompetensi Inti</strong>
						</label>
						<div>
							<textarea class="nogrow" rows=5 name="kompetensi_inti" placeholder="<?php echo $kompetensi_inti; ?>"></textarea>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Kurikulum</strong>
						</label>
						<div>
							<select name="id_kurikulum" data-placeholder="Choose a Name">
							<?php foreach($kurikulum as $k): ?>
								<?php if($k->id_kurikulum == $id_kurikulum){ ?>
									<option selected value="<?php echo $k->id_kurikulum; ?>"><?php echo $k->nama_kurikulum; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $k->id_kurikulum; ?>"><?php echo $k->nama_kurikulum; ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Tingkat Kelas</strong>
						</label>
						<div>
							<select name="id_tingkat_kelas" data-placeholder="Choose a Name">
							<?php foreach($tingkat_kelas as $t): ?>
								<?php if($t->id_tingkat_kelas == $id_tingkat_kelas){ ?>
									<option selected value="<?php echo $t->id_tingkat_kelas; ?>"><?php echo $t->tingkat_kelas; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $t->id_tingkat_kelas; ?>"><?php echo $t->tingkat_kelas; ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
						
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/struktur_kurikulum/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		