	<h1 class="grid_12">Struktur Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/struktur_kurikulum/submit_post_pel'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Pelajaran</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kode Pelajaran</strong>
						</label>
						<div>
							<input type="text" name="kode_pelajaran" value="" />
							<input type="hidden" name="action" value="add" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Pelajaran</strong>
						</label>
						<div>
							<input type="text" name="pelajaran" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Kelompok Pelajaran</strong>
						</label>
						<div>
							<select name="id_kelompok_pelajaran" data-placeholder="Choose a Name" id="id_kelompok_pelajaran">
							
							<?php foreach($kelompok_pelajaran as $k): ?>
								<option value="<?php echo $k->id_kelompok_pelajaran; ?>" data-status-pilihan="<?php echo $k->status_pilihan; ?>"><?php echo $k->kelompok_pelajaran; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Kualifikasi Guru</strong>
						</label>
						<div>
							<select name="id_kualifikasi_guru" data-placeholder="Choose a Name">
							<?php foreach($kualifikasi_guru as $t): ?>
									<option value="<?php echo $t->id_kualifikasi_guru; ?>"><?php echo $t->kualifikasi_guru; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row" id="field-peminatan" style="display:none;">
						<label for="f2_select3">
							<strong>Peminatan</strong>
						</label>
						<div>
							<select name="id_peminatan" data-placeholder="Choose a Name">
							<?php foreach($peminatan as $t): ?>
									<option value="<?php echo $t->id_peminatan; ?>"><?php echo $t->peminatan; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
						
				</fieldset><!-- End of fieldset -->
				<fieldset>
					<legend>Edit Alokasi Waktu per Minggu untuk Pelajaran</legend>
					<?php foreach($tingkat_kelas as $p): ?>
					<div class="row">
						<label for="f1_normal_input">
							<strong><?php echo $p->tingkat_kelas; ?></strong>
						</label>
						<div>
							<input type="text" name="jumlah_jam[]" value="" />
							<input type="hidden" name="id_tingkat_kelas[]" value="<?php echo $p->id_tingkat_kelas; ?>" />
						</div>
					</div>
					<?php endforeach; ?>
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/struktur_kurikulum/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		
		<script>
		$('#id_kelompok_pelajaran').on('change', function(){
			if($(this).find("option:selected").data('status-pilihan') == 1){
				$('#field-peminatan').show();
			} else {
				$('#field-peminatan').hide();
			}
		});
		</script>