<?php 
	$statuspiltingkat = $sting;
	
	if(isset($_POST['tingkat'])){
	$a = $_POST['tingkat'];
	$statuspiltingkat = $a; }	?>

<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Silabus Tematik</h2>
		</div>
		<div class="tabletools">
			<div class="right">
			<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
			  	<a href="<?php echo base_url('kurikulum/silabus_tematik/add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php } ?>
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
					<table>
						<tr>
				<form action="" name= "myform" method="post" enctype="multipart/form-data">
							
							<th width=100px align='left'><span class="text">Tingkat Kelas</span></th>
							<td width=200px align='left'>
								<select name="tingkat" id="tingkat" onchange="submitform();">
									<option>-- Pilih Tingkat Kelas --</option>
								<?php foreach($tingkat as $p): ?>
										<?php if($p->id_tingkat_kelas == $statuspiltingkat ){ ?>
											<option selected value='<?php echo $p->id_tingkat_kelas; ?>'><?php echo $p->tingkat_kelas; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $p->id_tingkat_kelas; ?>'><?php echo $p->tingkat_kelas; ?></option>
										<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
				</form>

				<form action="<?php echo base_url('kurikulum/silabus_tematik/search'); ?>" method="post" enctype="multipart/form-data">
							<th width=100px align='left'><span class="text">Pelajaran Tematik</span></th>
							<td width=200px align='left'>
							<input type="hidden" name="tingkat" value="<?php echo $statuspiltingkat; ?>">
								<select name="tematik">
									<option>-- Pilih Pelajaran Tematik--</option>
								<?php foreach($tematik as $p): ?>
									<?php if($p->id_tingkat_kelas == $statuspiltingkat ){ ?>
										<?php if($p->id_pelajaran_tematik == $first_tematik ){ ?>
											<option selected value='<?php echo $p->id_pelajaran_tematik; ?>'><?php echo $p->pelajaran_tematik; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $p->id_pelajaran_tematik; ?>'><?php echo $p->pelajaran_tematik; ?></option>
										<?php } ?>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
							<input type="submit" name=send class="button grey tooltip" value="cari" />
							<!-- <a href="search" class="button grey tooltip"><i class='icon-search'></i></a> -->
							</td>
						</tr>
				</form>
					</table>
			</div>

		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>silabus_tematik</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						
						<?php if($stat == 'search'){ ?>
							<?php $no = $page + 1; ?>
							<?php if($list){ ?>
								<?php foreach($list as $l): ?>
									
										<tr>
											<td width="50px" class="center"><?php echo $no; ?></td>
											<td width="300px" class="center"><?php echo $l->nama_silabus_tematik; ?></td>
											<td width="150px" class="center">
												<a href="<?php echo base_url('kurikulum/silabus_tematik/download/'.$l->id_silabus_tematik); ?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-print"></i> Download</a>
												<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
													<a href="<?php echo base_url('kurikulum/silabus_tematik/delete/'.$l->id_silabus_tematik); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
												<?php } ?>
											</td>
										</tr>
										<?php $no++; ?>
								<?php endforeach; ?>
							
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $jumlah; ?></div>
						<div class="dataTables_paginate paging_full_numbers">
							<p><?php if($links){echo $links; } ?></p>
						</div>
					</div>
							<?php }else{ ?>
								</tbody>
								</table>
								<div class="footer">Data tidak ditemukan!
								</div>
							<?php } ?>
						<?php }else{ ?>
							</tbody>
								</table>
								<div class="footer">Pilih Tingkat Kelas dan Pelajaran Tematik yang Ingin di Cari!
								</div>
						<?php } ?>
				</div>
			</div>
		</div>        
	</div>
</div>

<script>
	
	function submitform()
	{
	  document.myform.submit();
	}
</script>