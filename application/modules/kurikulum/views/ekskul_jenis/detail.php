<h1 class="grid_12">Ekstrakulikuler</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								
							</tr>
						</thead>
						<tbody>
							
							<tr>
								<td width='' class='left'>Nama Ekstrakurikuler: </td>
								<td ><?php foreach ($data as $d) {?>
									<?php echo $d->nama_ekstra_kurikuler; ?>
									<?php }?></h2></td>
								<td rowspan="4"  class='center'><?php foreach ($data as $d) {?>
									<img src="<?php echo base_url('extras/ekstra_kurikuler/'.$d->lokasi_file_lambang); ?>" width="100px">
									<?php }?></h2></td>
							</tr>
							<tr>
								<td width='' class='left'>Tujuan</td>
								<td ><?php foreach ($data as $d) {?>
									<?php echo $d->tujuan; ?>
									<?php }?></h2></td>
							</tr>
							<tr>
								<td class='left'>Penanggung Jawab</td>
								<td ><?php foreach ($data as $d) {?>
									<?php echo $d->pelatih; ?>
									<?php }?></td>

							</tr>
							<tr>
								<td class='left'>Proker</td>
								<td ><?php foreach ($data as $d) {?>
									<?php echo $d->proker; ?>
									<?php }?></td>

							</tr>
							<tr>
								<td class='center'></td>
								<td  width='500px'></td>

							</tr>
						</tbody>
					</table>
					<div class="footer">
						 <a href="<?php echo base_url('kurikulum/ekskul_jenis/datalist/'); ?>"> <input value="Kembali" type="button"></a>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
