	
	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/rpp/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Rencana Pelaksanaan Pembelajaran</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama RPP</strong>
						</label>
						<div>
							<input type="text" name="nama_rpp" value="" />
							<input type="hidden" name="action" value="add" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Pertemuan Ke</strong>
						</label>
						<div>
							<input type="text" name="pertemuan_ke" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Kelas</strong>
						</label>
						<div>
							<select name="id_rombel" id="rombel" data-placeholder="-- Pilih Kelas --">
								<option>-- Pilih Kelas --</option>
							<?php foreach($rombel as $t): ?>
								<option value="<?php echo $t->id_rombel; ?>"><?php echo $t->rombel; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Mata Pelajaran</strong>
						</label>
						<div>
							<select name="id_guru_matpel_rombel" class="chosen-select" id="pelajaran" data-placeholder="-- Pilih Pelajaran --">
							<option>-- Pilih Pelajaran --</option>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>File</strong>
						</label>
						<div>
							<input type="file" name="userfile" value="" />
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/rpp/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		
<script>
$(function(){
	$(document).on('change', '#rombel', function(){
		var id_rombel = $(this).val();
		$.post("<?php echo site_url('kurikulum/rpp/get_mapel_guru'); ?>/" + id_rombel, function(data){
			$("#pelajaran").html(data);
			$("#pelajaran").trigger("chosen:updated");
		});
	});
});
</script>