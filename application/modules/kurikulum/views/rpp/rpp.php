<?php $rombel = array("0"=>"-Pilih-")+get_rombel() ?>
<?php $pelajaran = get_pelajaran_guru_matpel_rombel() ?>

<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Rencana Pelaksanaan Pembelajaran</h2>
		</div>
		<div class="tabletools">
			<div class="right">
				<?php if($gid == 2 || $gid == 3 || $gid == 4 || $gid == 9){ ?>
					<a href="<?php echo base_url('kurikulum/rpp/add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php } ?>
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('kurikulum/rpp/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text">Kelas</span></th>
							<td width=150px align='left'>
								<?php echo simple_form_dropdown_clear("id_rombel",$rombel,$id_rombel," id='idrombel' onChange=rombelChanged(this.value)",null) ?>
							</td>
							<td>&nbsp;</td>
							<th width=80px align='left'><span class="text">Mata Pelajaran</span></th>
							<td width=250px align='left'>
								<?php foreach($rombel as $id=>$det):?>
									<?php $mapel =  array("0"=>"-Pilih-") ?>
									<?php $mapel += isset($pelajaran[$id])?$pelajaran[$id]:array() ?>
									<div id='mapel<?php echo $id?>' <?php echo $id_rombel!=$id?"style='display:none'":""?> class='listmapel'>					
										<?php echo simple_form_dropdown_clear("id_gmr".$id,$mapel,$id_gmr,"",null) ?>
									</div>	
								<?php endforeach ?>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
							<input type="submit" name=send class="button grey tooltip" value="cari" />
							<!-- <a href="search" class="button grey tooltip"><i class='icon-search'></i></a> -->
							</td>
						</tr>
					</table>
				</form>
			</div>

		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>RPP</th>
								<th>Semester</th>
								<th>Pertemuan</th>
								<?php if($gid == 2 || $gid == 3 || $gid == 4 || $gid == 9){ ?>
									<th>Aksi</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
						
						<?php if($stat == 'search'){ ?>
							<?php $no = $page + 1; ?>
							<?php if($list){ ?>
								<?php foreach($list as $l): ?>
									
										<tr>
											<td width="50px" class="center"><?php echo $no; ?></td>
											<td width="300px" class="center"><?php echo $l->nama_rpp; ?></td>
											<td width="100px" class="center"><?php echo $l->term; ?></td>
											<td width="100px" class="center"><?php echo $l->pertemuan_ke; ?></td>
											<?php if($gid == 2 || $gid == 3 || $gid == 4 || ($gid == 9 && $l->id_guru == get_id_personal())){ ?>
											<td width="150px" class="center">
												<a href="<?php echo base_url('kurikulum/rpp/download/'.$l->id_rpp); ?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-print"></i> Download</a>
												<a href="<?php echo base_url('kurikulum/rpp/delete/'.$l->id_rpp); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
											</td>
											<?php } ?>
										</tr>
										<?php $no++; ?>
								<?php endforeach; ?>
							
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $jumlah; ?></div>
						<div class="dataTables_paginate paging_full_numbers">
							<p><?php if($links){echo $links; } ?></p>
						</div>
					</div>
							<?php }else{ ?>
								</tbody>
								</table>
								<div class="footer">Data tidak ditemukan!
								</div>
							<?php } ?>
						<?php }else{ ?>
							</tbody>
								</table>
								<div class="footer">Pilih Kelas dan Mata Pelajaran yang Ingin di Cari!
								</div>
						<?php } ?>
				</div>
			</div>
		</div>        
	</div>
</div>

	<script type="text/javascript">		
		function rombelChanged(id){
			$('.listmapel').hide();
			$('.listkomp').hide();
			if(id>0){
				$('#mapel'+id).show();				
				$('#komp'+id).show();				
			}
		}
	</script>