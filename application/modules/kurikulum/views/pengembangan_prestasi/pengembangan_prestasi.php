<?php  
	$statuspil = 0;
	if(isset($_POST['id_ekstra_kurikuler'])){
	$a = $_POST['id_ekstra_kurikuler'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Data Prestasi Pengembangan Diri</h2>
			
       </div>
	
       <div class="tabletools">
			<div class="right">
			<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
				<a href="<?php echo base_url('kurikulum/pengembangan_prestasi/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			<?php } ?>
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('kurikulum/pengembangan_prestasi/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="40px" align="left"><span class="text">Nama</span></th>
								<td align="left"><input type="text" name="nama" id="nama"></td>
								<td width="10px"></td>
								<th width="110px" align="left"><span class="text">Pengembangan Diri</span></th>
								<td width="200px" align="left">
								<select name="id_ekstra_kurikuler" id="id_ekstra_kurikuler" onchange="submitform();">
									<option value="0">-Jenis Pengembangan Diri-</option>
									<?php foreach($ekskul as $q): ?>
										
										<?php if($q->id_ekstra_kurikuler == $statuspil){ ?>
											<option selected value='<?php echo $q->id_ekstra_kurikuler; ?>' data-status-pilihan="<?php echo $q->id_ekstra_kurikuler; ?>"><?php echo $q->nama_ekstra_kurikuler; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q->id_ekstra_kurikuler; ?>' data-status-pilihan="<?php echo $q->id_ekstra_kurikuler; ?>"><?php echo $q->nama_ekstra_kurikuler; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
								</td>
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Nis</th>
								<th >Nama</th>
								<th >Prestasi</th>
								<th >Kategori</th>
								<th >Perlombaan</th>
								<th >Tingkat</th>
								<th >Tanggal</th>
								<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
									<th>Aksi</th>
								<?php } ?>
							</tr>
						</thead>
				
						<tbody>
						
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
								<?php //print_r($isi)?>
								<tr>
									<td class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['nis']?>
									</td class='center'>
									<td class='center'>
									<?php  echo $d['nama'];?>
									</td>
									<td class='center'>
									<?php  echo $d['prestasi'];?>
									</td>
									<td class='center'>
									<?php  echo $d['kategori'];?>
									</td>
									<td class='center'>
									<?php  echo $d['perlombaan'];?>
									</td>
									<td class='center'>
									<?php  echo $d['tingkat_wilayah'];?>
									</td>
									<td class='center'>
									<?php  echo $d['tanggal'];?>
									</td>
									<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
										<td class='center'>
										<a href="<?php echo site_url('kurikulum/pengembangan_prestasi/delete/'.$d['id_prestasi_siswa'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
										</td>
									<?php } ?>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>