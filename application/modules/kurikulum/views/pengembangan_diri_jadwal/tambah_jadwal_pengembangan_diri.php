
<h1 class="grid_12">Pengembangan Diri</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>TAMBAH JADWAL PENGEMBANGAN DIRI</h2>
       </div>
		
		<form action="<?php echo base_url('kurikulum/jadwal_pengembangan_diri/simpan_tambah_data'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenis Pengembangan Diri</strong>
						</label>
						<div>
							<select name="id_ekstra_kurikuler">
							<?php foreach ($list_pengembangan_diri as $pd): ?>
							  <option value="<?php echo $pd['id_ekstra_kurikuler'];?>"><?php echo $pd['nama_ekstra_kurikuler'];?></option>
							<?php endforeach ?>
							</select> 
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Hari:</strong>
						</label>
						<div>
							<select name="hari">
							  <option value="Senin">Senin</option>
							  <option value="Selasa">Selasa</option>
							  <option value="Rabu">Rabu</option>
							  <option value="Kamis">Kamis</option>
							  <option value="Jumat">Jumat</option>
							  <option value="Sabtu">Sabtu</option>
							  <option value="Minggu">Minggu</option>
							</select> 
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jam Mulai :</strong>
						</label>
						<div>
							<input type="time" name="jam_mulai" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jam Selesai :</strong>
						</label>
						<div>
							<input type="time" name="jam_selesai" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tempat :</strong>
						</label>
						<div>
							<input type="text" name="tempat" value="" />
						</div>
					</div>
					
				<fieldset>
				<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							<a href="<?php echo base_url('kurikulum/jadwal_pengembangan_diri/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						
				</div>
		</form>
    </div>
 </div>
