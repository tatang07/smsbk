<?php 
	$statuspiltingkat = 0;
	
	if(isset($_POST['tingkat'])){
	$a = $_POST['tingkat'];
	$statuspiltingkat = $a; }	?>

<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Silabus</h2>
		</div>
		<div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('kurikulum/pelajaran_tematik/add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
					<table>
						<tr>
				<form action="" name= "myform" method="post" enctype="multipart/form-data">
							
							<th width=100px align='left'><span class="text">Tingkat Kelas</span></th>
							<td width=200px align='left'>
								<select name="tingkat" id="tingkat" onchange="submitform();">
									<option>-- Pilih Tingkat Kelas --</option>
								<?php foreach($tingkat as $p): ?>
										<?php if($p->id_tingkat_kelas == $statuspiltingkat ){ ?>
											<option selected value='<?php echo $p->id_tingkat_kelas; ?>'><?php echo $p->tingkat_kelas; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $p->id_tingkat_kelas; ?>'><?php echo $p->tingkat_kelas; ?></option>
										<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
							</tr>
				</form>

					</table>
			</div>

		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode Pelajaran Tematik</th>
								<th>Pelajaran Tematik</th>
								<th>Alokasi Waktu</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						
						<?php if($statuspiltingkat != 0){ ?>
							<?php $no = $page + 1; ?>
							<?php if($list){ ?>
								<?php foreach($list as $l): ?>
									
										<tr>
											<td width="50px" class="center"><?php echo $no; ?></td>
											<td width="150px" class="center"><?php echo $l->kode_pelajaran_tematik; ?></td>
											<td width="300px" class="center"><?php echo $l->pelajaran_tematik; ?></td>
											<td width="150px" class="center"><?php echo $l->alokasi_waktu; ?></td>
											<td width="150px" class="center">
												<a href="<?php echo base_url('kurikulum/pelajaran_tematik/edit/'.$l->id_pelajaran_tematik); ?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-edit"></i> Edit</a>
												<a href="<?php echo base_url('kurikulum/pelajaran_tematik/delete/'.$l->id_pelajaran_tematik); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
										<?php $no++; ?>
								<?php endforeach; ?>
							
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $jumlah; ?></div>
						<div class="dataTables_paginate paging_full_numbers">
							<p><?php if($links){echo $links; } ?></p>
						</div>
					</div>
							<?php }else{ ?>
								</tbody>
								</table>
								<div class="footer">Data tidak ditemukan!
								</div>
							<?php } ?>
						<?php }else{ ?>
							</tbody>
								</table>
								<div class="footer">Pilih Tingkat Kelas yang Ingin di Cari!
								</div>
						<?php } ?>
				</div>
			</div>
		</div>        
	</div>
</div>

<script>
	
	function submitform()
	{
	  document.myform.submit();
	}
</script>