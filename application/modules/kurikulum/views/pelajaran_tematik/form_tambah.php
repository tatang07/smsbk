	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/pelajaran_tematik/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>pelajaran_tematik</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kode Pelajaran Tematik</strong>
						</label>
						<div>
							<input type="text" name="kode" value="<?php echo $kode; ?>" />
							<input type="hidden" name="id" value="<?php echo $id; ?>" />
							<input type="hidden" name="action" value="<?php echo $action; ?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Pelajaran Tematik</strong>
						</label>
						<div>
							<input type="text" name="tema" value="<?php echo $tema; ?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Alokasi Waktu</strong>
						</label>
						<div>
							<input type="text" name="alokasi_waktu" value="<?php echo $alokasi; ?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Tingkat Kelas</strong>
						</label>
						<div>
							<select name="id_tingkat_kelas" data-placeholder="-- Pilih Tingkat Kelas --">
								<option>-- Pilih Tingkat Kelas --</option>
							<?php foreach($tingkat as $t): ?>
							<?php if($t->id_tingkat_kelas == $tingkat_kelas){ ?>
								<option selected value="<?php echo $t->id_tingkat_kelas; ?>"><?php echo $t->tingkat_kelas; ?></option>
							<?php }else{ ?>
								<option value="<?php echo $t->id_tingkat_kelas; ?>"><?php echo $t->tingkat_kelas; ?></option>
							<?php } ?>
							
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/pelajaran_tematik/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
