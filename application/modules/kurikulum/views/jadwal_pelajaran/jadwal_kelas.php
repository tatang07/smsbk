<?php 
	$statuspil = 0;
	if(isset($_POST['id_rombel'])){
	$a = $_POST['id_rombel'];
	$statuspil = $a; } ?>
	
<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="tabletools">
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('kurikulum/jadwal_pelajaran/jadwal_kelas/'); ?>" method="post" enctype="multipart/form-data" name= "myform">
					<table>
						<tr>
							<th width=50px align='left'><span class="text">Kelas</span></th>
							<td width=200px align='left'>
								<input type="hidden" name="id_jadwal_pelajaran" value="<?php echo $id_jadwal_pelajaran; ?>">
								<select name="id_rombel" data-placeholder="-- Pilih Kelas --" id="id_rombel" onchange="submitform();">
									<option value=0>-- Pilih Kelas --</option>
								<?php foreach($rombel as $r): ?>
										<?php if($r->id_rombel==$statuspil){ ?>
										<option selected value='<?php echo $r->id_rombel; ?>'><?php echo $r->rombel; ?></option>
										<?php }else{ ?>
										<option value='<?php echo $r->id_rombel; ?>'><?php echo $r->rombel; ?></option>
										<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							
							<td>
							<input type="submit" name=send class="button grey tooltip" value="cari" />
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		
	</div>
	<div class="box with-table tabbedBox" id="field1">
		
		<div class="header">
			<h2>Jadwal Kelas</h2>
			<?php $hari = array('SENIN', 'SELASA', 'RABU', 'KAMIS', 'JUMAT', 'SABTU'); ?>
			<ul>
				<?php for($i=0;$i<6;$i++){ ?>
					<li><a href="#<?php echo $hari[$i]; ?>"><?php echo $hari[$i]; ?></a></li>
				<?php } ?>
			</ul>
		</div><!-- End of .header -->
		
		<div class="content tabbed">
			<?php for($i=0;$i<6;$i++){ ?>
			<div id="<?php echo $hari[$i]; ?>">
				
				<div id="datatable" url="">
					<div class="dataTables_wrapper"  role="grid">    
						<table class="styled" >
							<thead>
								<tr>
									<th>Waktu</th>
									<th>Ruang</th>
									<th>Mata Pelajaran</th>
									<th>Guru</th>
								</tr>
							</thead>
							<tbody>
								<?php if($datalist){ ?>
								<?php foreach($datalist as $d): ?>
									<?php if($d->id_rombel==$statuspil){ $ha=ucwords($d->hari); if($ha == $hari[$i]){ ?>
									<tr>
										<td width="150px" class="center"><?php echo $d->mulai." - ".$d->selesai; ?></td>
										<td width="100px" class="center"><?php echo $d->ruang_belajar; ?></td>
										<td width="300px" class=""><?php echo $d->pelajaran; ?></td>
										<td width="200px" class=""><?php echo $d->nama; ?></td>
									</tr>
								<?php } } endforeach; ?>
							</tbody>
						</table>
								<?php }else{ ?>
										</tbody>
									</table>
									<div class="footer">Data tidak ditemukan!</div>
								<?php } ?>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>        
	</div>
	
	<a href="<?php echo site_url('kurikulum/jadwal_pelajaran/');?>"><button class="block">Kembali</button></a>

</div>


<script>
	
	function submitform()
	{
	  document.myform.submit();
	}
</script>