<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Jadwal Pelajaran</h2>
		</div>
		<div class="tabletools">
			<div class="right">
			<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
			  	<a href="<?php echo base_url('kurikulum/jadwal_pelajaran/export/'); ?>"><i class="icon-print"></i>Export Bahan</a> 
			  	<a href="<?php echo base_url('kurikulum/jadwal_pelajaran/import/'); ?>"><i class="icon-plus"></i>Import</a> 
			<?php } ?>
			  <br><br>
            </div>
			
		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jadwal</th>
								<?php if($gid == 2 || $gid == 3 || $gid == 4 || $gid){ ?>
									<th>Aksi</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
						
						<?php if($stat == 'home'){ ?>
							<?php $no = $page + 1; ?>
							<?php if($list){ ?>
								<?php foreach($list as $l): ?>
									
										<tr>
											<td width="50px" class="center"><?php echo $no; ?></td>
											<td width="300px" class="center"><?php echo $l->nama; ?></td>
											<?php if($gid == 2 || $gid == 3 || $gid == 4 || $gid){ ?>
												<td width="150px" class="center">
													<a href="<?php echo base_url('kurikulum/jadwal_pelajaran/jadwal_kelas/'.$l->id_jadwal_pelajaran.'/'.$id_rombel); ?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-print"></i> Jadwal Kelas</a>
													<a href="<?php echo base_url('kurikulum/jadwal_pelajaran/jadwal_guru/'.$l->id_jadwal_pelajaran.'/'.$id_guru); ?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-print"></i> Jadwal Guru</a>
													<a href="<?php echo base_url('kurikulum/jadwal_pelajaran/delete/'.$l->id_jadwal_pelajaran); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
												</td>
											<?php } ?>
										</tr>
										<?php $no++; ?>
								<?php endforeach; ?>
							
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $jumlah; ?></div>
						<div class="dataTables_paginate paging_full_numbers">
							<p><?php if($links){echo $links; } ?></p>
						</div>
					</div>
							<?php }else{ ?>
								</tbody>
								</table>
								<div class="footer">Data tidak ditemukan!
								</div>
							<?php } ?>
						<?php }else{ ?>
							</tbody>
								</table>
								<div class="footer">Pilih Kelas dan Mata Pelajaran yang Ingin di Cari!
								</div>
						<?php } ?>
				</div>
			</div>
		</div>        
	</div>
</div>