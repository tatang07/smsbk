<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>List Error Saat Import Jadwal Pelajaran</h2>
		</div>
		
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Baris</th>
								<th>Keterangan</th>
							</tr>
						</thead>
						<tbody>
						
						
								<?php $no=1; foreach($error as $index => $e): ?>
									
										<tr>
											<td width="30px" class="center"><?php echo $no; ?></td>
											<td width="100px" class="center"><?php echo $index+1 ?></td>
											<td width="500px" class="">
											<?php 
											//ngecek eror hari
											if($e['Hari']!=''){
												$hari=' Nama hari salah, seharusnya bukan '.$e['Hari'];
											}else{
												$hari='';
											}
											
											//ngecek eror ruang
											if($e['Kode Ruang']!=''){
												$ruang=' kode ruang '.$e['Kode Ruang'].' tidak ada dalam database sistem, silahkan diganti';
											}else{
												$ruang='';
											}
											
											//ngecek eror guru matpel rombel
											if($e['Kode Guru']!=''){
												$gmr=' Kode guru, kelas, dan mata pelajaran tidak saling berhubungan';
											}else{
												$gmr='';
											}
											
											$baris=$index+1; 
											if($hari=='' && $ruang=='' && $gmr==''){
												echo "Baris ke ".$baris." Sudah Benar";
											}else{
												echo 'Error : <ul>';
												if($hari!=''){ echo '<li>'.$hari.'</li>'; }
												if($ruang!=''){ echo '<li>'.$ruang.'</li>'; }
												if($gmr!=''){ echo '<li>'.$gmr.'</li>'; }
												echo '</ul>';
											}

											?>
											
											</td>
										</tr>
										<?php $no++; ?>
								<?php endforeach; ?>
							
						</tbody>
					</table>
					
					
				</div>
			</div>
		</div>    
	</div>
		<a href="<?php echo site_url('kurikulum/jadwal_pelajaran/import');?>"><button class="block">Kembali Import Jadwal</button></a>
</div>