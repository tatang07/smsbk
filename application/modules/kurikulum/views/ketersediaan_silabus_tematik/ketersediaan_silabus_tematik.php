
<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Ketersediaan Silabus Tematik</h2>
		
       </div>
	
       <div class="tabletools">
            
        </div>

        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
				
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=2>No.</th>
								<th rowspan=2>Tema</th>
								<th colspan=6>Tingkat</th>
							</tr>
						
							<tr>
								<th> 1 </th>
								<th> 2 </th>
								<th> 3 </th>
								<th> 4 </th>
								<th> 5 </th>
								<th> 6 </th>
							</tr>
						</thead>
						
						<tbody>
							<?php $n=0;?>
							<?php $no=1; foreach ($pelajaran as $p):?>
								<tr>
									<td>
										<?php echo $no; $no++; ?>
									</td>
									<td>
										<?php echo $p['pelajaran_tematik']; ?>
									</td>
									<!-- NGECEK MATA PELAJARAN ITU ADA SILABUS NYA GA-->
									<?php $i=0;?>
									
									<?php foreach($jenis_kelas as $jk) :?>
												<?php
													if(isset($hasil[$p['pelajaran_tematik']][$jk['id_tingkat_kelas']])){
														echo "<td>ada</td>";
													}else{
														echo "<td>- </td>";
													} 
												?>
										<?php $i++;?>
									<?php endforeach?>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>

			
					
					<div class="footer">
						<div class="dataTables_info"><?php echo "Jumlah Data :"; echo $no-1;?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>