<?php 
	$statuspil = 0;
	$statuspem = 0;
	if(isset($_POST['id_kurikulum'])){
	$a = $_POST['id_kurikulum'];
	$statuspil = $a; 
	$statuspem = $statuspil+1; } ?>
	
	<h1 class="grid_12">Kurikulum</h1>
		
			<form action="" name= "myform" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>
				<legend>Kurikulum</legend>
				<div class="row">
					<label for="f2_select2">
						<strong>Jenis Kurikulum</strong>
					</label>
					<div>
						<select name="id_kurikulum" id="id_kurikulum" onchange="submitform();">
						<option value="0">-Pilih Kurikulum-</option>
						<?php foreach($kurikulum as $q): ?>
							
							<?php if($q->id_kurikulum == $statuspil){ ?>
								<option selected value='<?php echo $q->id_kurikulum; ?>' data-status-pilihan="<?php echo $q->id_kurikulum; ?>"><?php echo $q->nama_kurikulum; ?></option>
							<?php }else{ ?>
								<option value='<?php echo $q->id_kurikulum; ?>' data-status-pilihan="<?php echo $q->id_kurikulum; ?>"><?php echo $q->nama_kurikulum; ?></option>
							<?php } ?>
						<?php endforeach; ?>
						</select>
					</div>
			</fieldset>
			</form>
			
			<form action="<?php echo base_url('kurikulum/struktur_kurikulum/submit_post_pel'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Pelajaran</legend>
	
					<div class="row">
						<label for="f1_textarea">
							<strong>Pelajaran</strong>
						</label>
						<div>
							<input type="text" name="pelajaran" value="" />
							<input type="hidden" name="action" value="add" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Kelompok Pelajaran</strong>
						</label>
						<div>
							<select name="id_kelompok_pelajaran" data-placeholder="-- Pilih Kelompok Pelajaran --" id="id_kelompok_pelajaran">
							<option value="0">-Pilih Kelompok Pelajaran-</option>
							<?php foreach($kelompok_pelajaran as $k): ?>
							<?php if($k->id_kurikulum == $statuspil){ ?>
								<option value="<?php echo $k->id_kelompok_pelajaran; ?>" data-status-pilihan="<?php echo $k->status_pilihan; ?>"><?php echo $k->kelompok_pelajaran; ?></option>
							<?php } endforeach; ?>
							</select>
						</div>
					</div>
					
					<fieldset>
						<?php echo simple_form_dropdown_dual('kualifikasi[]', $kualifikasi_all, $kualifikasi_baru, "", array('label'=>'Klasifikasi Guru')); ?>

					</fieldset>
					
					<div class="row" id="field-peminatan" style="display:none;">
						<label for="f2_select3">
							<strong>Peminatan</strong>
						</label>
						<div>
							<select name="id_peminatan" data-placeholder="Choose a Name">
							<?php
								if ($statuspem == 3) {
									foreach($peminatan_2013 as $t):
							?>
										<option value="<?php echo $t->id_peminatan; ?>"><?php echo $t->peminatan; ?></option>
							<?php
									endforeach;
								}else{
									foreach($peminatan_ktsp as $t):
							?>
										<option value="<?php echo $t->id_peminatan; ?>"><?php echo $t->peminatan; ?></option>
							<?php
									endforeach;
								}
							?>
							</select>
						</div>
					</div>
						
				</fieldset><!-- End of fieldset -->
				<fieldset>
					<legend>Kode Pelajaran dan Alokasi Waktu per Minggu untuk Pelajaran</legend>
				</fieldset><!-- End of fieldset -->
				<fieldset>
					<?php foreach($tingkat_kelas as $p): ?>
					<legend>Kelas <?php echo $p->tingkat_kelas; ?></legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kode Pelajaran</strong>
						</label>
						<div>
							<input type="text" name="kode_pelajaran[]" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Alokasi Waktu</strong>
						</label>
						<div>
							<input type="text" name="jumlah_jam[]" value="" />
							<input type="hidden" name="id_tingkat_kelas[]" value="<?php echo $p->id_tingkat_kelas; ?>" />
						</div>
					</div>
					<?php endforeach; ?>
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/struktur_kurikulum/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		
		<script>
		$('#id_kelompok_pelajaran').on('change', function(){
			if($(this).find("option:selected").data('status-pilihan') == 1){
				$('#field-peminatan').show();
			} else {
				$('#field-peminatan').hide();
			}
		});
		</script>
		
		<script>
			
			function submitform()
			{
			  document.myform.submit();
			}
		</script>