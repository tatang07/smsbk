	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/silabus/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Silabus</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama Silabus</strong>
						</label>
						<div>
							<input type="text" name="nama_silabus" value="" />
							<input type="hidden" name="action" value="add" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Kelompok Pelajaran</strong>
						</label>
						<div>
						<?php $kurikulum = $this->uri->segment(4); ?>
							<select name="id_kelompok_pelajaran" id="kelompok_pelajaran" data-placeholder="-- Pilih Kelompok Pelajaran --">
								<option>-- Pilih Kelompok Pelajaran --</option>
							<?php foreach($kelompok_pelajaran as $t): ?>
							<?php if($t->id_kurikulum == $kurikulum){ ?>
								<option value="<?php echo $t->id_kelompok_pelajaran; ?>"><?php echo $t->kelompok_pelajaran; ?></option>
							<?php } endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Mata Pelajaran</strong>
						</label>
						<div>
							<select name="id_pelajaran" class="chosen-select" id="pelajaran" data-placeholder="-- Pilih Pelajaran --">
							<option>-- Pilih Pelajaran --</option>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>File</strong>
						</label>
						<div>
							<input type="file" name="userfile" value="" />
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/silabus/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		
<script>
$(function(){
	$(document).on('change', '#kelompok_pelajaran', function(){
		var id_kelompok_pelajaran = $(this).val();
		$.post("<?php echo site_url('kurikulum/silabus/get_pelajaran'); ?>/" + id_kelompok_pelajaran, function(data){
			$("#pelajaran").html(data);
			$("#pelajaran").trigger("chosen:updated");
		});
	});
});
</script>