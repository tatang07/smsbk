<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Rekap Kehadiran Guru</h2>
		</div>
		
		<?php if(!get_id_personal()){ ?>
		<div class="tabletools">
			
			<div class="dataTables_filter">
				<form action="search" method="post" enctype="multipart/form-data">
					<table>
					
						<tr>
							<th width=50px align='left'><span class="text">Guru</span></th>
							<td width=200px align='left'>
								<select name="guru">
								<?php  if(isset($guru) and count($guru)>0){ foreach($guru as $q): ?>
									<?php if($q->id_guru == $first ){ ?>
										<option selected value='<?php echo $q->id_guru; ?>'><?php echo $q->nama; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $q->id_guru; ?>'><?php echo $q->nama; ?></option>
									<?php } ?>
								<?php endforeach; } ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
							<input type="submit" name=send class="button grey tooltip" value="cari" />
							<!-- <a href="search" class="button grey tooltip"><i class='icon-search'></i></a> -->
							</td>
						</tr>
					</table>
				</form>
			</div>

		</div>
		<?php } ?>
		
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode.</th>
								<th>Mata Pelajaran</th>
								<th>Kelas</th>
								<th>Pertemuan</th>
							</tr>
						</thead>
						<tbody>
						<?php if(get_id_personal()){ ?>
							<?php $no = $page + 1; ?>
							<?php if($list){ ?>
								<?php foreach($list as $l): ?>
									<?php  if($l->id_guru == $first){ ?>
									<tr>
										<td width="50px" class="center"><?php echo $no; ?></td>
										<td width="100px" class="center"><?php echo $l->kode_pelajaran; ?></td>
										<td width="400px" class=""><?php echo $l->pelajaran; ?></td>
										<td width="100px" class="center"><?php echo $l->rombel; ?></td>
										<td width="100px" class="center"><?php echo $l->pertemuan; ?></td>
									</tr>
									<?php $no++; ?>
									<?php } ?>
								<?php endforeach; ?>
							
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $jumlah; ?></div>
						<div class="dataTables_paginate paging_full_numbers">
							<p><?php if($links){echo $links; } ?></p>
						</div>
					</div>
							<?php }else{ ?>
								</tbody>
								</table>
								<div class="footer">Data tidak ditemukan!
								</div>
							<?php } ?>
						<?php }else{ ?>
							</tbody>
								</table>
								<div class="footer">Pilih Nama Guru yang Ingin dicari!
								</div>
						<?php } ?>
				</div>
			</div>
		</div>        
	</div>
</div>