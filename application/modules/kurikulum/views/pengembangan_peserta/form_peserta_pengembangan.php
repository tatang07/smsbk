<h1 class="grid_12">Tambah Peserta Ekstrakurikuler</h1>
<form action="" class="grid_12 validate" novalidate="novalidate" method="post" id="valid" enctype="multipart/form-data">
	<fieldset>

		<?php echo simple_form_dropdown('id_ekstra_kurikuler', $ekskul, $id_ekskul, "", array('label'=>'Pengembangan Diri')); ?>
		<?php echo $id_ekskul ? simple_form_dropdown('id_rombel', $rombel, $id_rombel, "", array('label'=>'Kelas')) : ''; ?>

		<?php echo ($id_rombel && $id_ekskul)
					? simple_form_dropdown_dual('siswa[]', $siswa, $siswa_ekskul, "", array('label'=>'Siswa'))
					: ''; ?>

	</fieldset>
	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" />
			<a href="<?php echo site_url('kurikulum/peserta_pengembangan')?>"> <input type="button" value="Kembali" /></a>
		</div>
	</div>
</form>

<script>
	$(function(){
		$('#id_ekstra_kurikuler').chosen().change(function(){
			window.location = "<?php echo site_url('kurikulum/peserta_pengembangan/tambah_peserta').'/'; ?>" + $(this).val();
		});
		$('#id_rombel').chosen().change(function(){
			window.location = "<?php echo site_url('kurikulum/peserta_pengembangan/tambah_peserta/'.$this->uri->segment(4)).'/'; ?>" + $(this).val();
		});
	})
</script>