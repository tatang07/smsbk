<?php  
	$statuspil = 0;
	if(isset($_POST['id_kurikulum'])){
	$a = $_POST['id_kurikulum'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Ketersediaan Silabus</h2>
		
       </div>
	
       <div class="tabletools">
			
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Jenis Kurikulum</span></th>
							<td width=200px align='left'>
								<select name="id_kurikulum" id="id_kurikulum" onchange="submitform();">
									<option value="0">-Pilih Kurikulum-</option>
									<?php foreach($kur as $q): ?>
										
										<?php if($q->id_kurikulum == $statuspil){ ?>
											<option selected value='<?php echo $q->id_kurikulum; ?>' data-status-pilihan="<?php echo $q->id_kurikulum; ?>"><?php echo $q->nama_kurikulum; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q->id_kurikulum; ?>' data-status-pilihan="<?php echo $q->id_kurikulum; ?>"><?php echo $q->nama_kurikulum; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
            
        </div>
		
		
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<?php if($statuspil==2) { ?>
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=3>No.</th>
								<th rowspan=3>Mata Pelajaran</th>
								<th colspan=12>Ketersediaan Silabus</th>
							</tr>
							<tr>
								<?php $i=0;?>
								<?php foreach($jenis_kelas as $j): ?>
								<?php 
								$this->load->model('kurikulum/m_ketersediaan_silabus');
								$id_tingkat_kelas=$j['id_tingkat_kelas'];
								$colspan = $this->m_ketersediaan_silabus->get_colspan($id_tingkat_kelas, $statuspil)->result_array();
								$banyak_colspan[$i] = count($colspan);
								$cols[$i]=$colspan;
								
								?>
							
								<th colspan=<?php echo $banyak_colspan[$i]; $i++; ?>><?php echo $j['tingkat_kelas'] ?></td>		<!--NGEPRINT COLSPAN SAMA TINGKAT KELAS-->
								<?php endforeach; ?>
							</tr>
							<tr>
							<?php foreach($cols[0] as $a):?>
								<?php 
								$this->load->model('kurikulum/m_ketersediaan_silabus');
								$id_peminatan=$a['id_peminatan'];
								// echo $id_peminatan;
								if($id_peminatan==0){
									$nama_peminatan = array("0"=>array("peminatan_singkatan"=>"&nbsp"));
									
								}else{
								$nama_peminatan = $this->m_ketersediaan_silabus->get_nama_peminatan($id_peminatan)->result_array();
								}
								?>
								<?php //print_r ($nama_peminatan); ?>
									<?php foreach ($nama_peminatan as $np):?>
									<th><?php echo $np['peminatan_singkatan'] ?></th>
									<?php endforeach;?>
								
							<?php endforeach ?>
							<?php foreach($cols[1] as $a):?>
								<?php 
								$this->load->model('kurikulum/m_ketersediaan_silabus');
								$id_peminatan=$a['id_peminatan'];
								// echo $id_peminatan;
								if($id_peminatan==0){
									$nama_peminatan = array("0"=>array("peminatan_singkatan"=>"&nbsp"));
									
								}else{
								$nama_peminatan = $this->m_ketersediaan_silabus->get_nama_peminatan($id_peminatan)->result_array();
								}
								?>
								<?php //print_r ($nama_peminatan); ?>
									<?php foreach ($nama_peminatan as $np):?>
									<th><?php echo $np['peminatan_singkatan'] ?></th>
									<?php endforeach;?>
								
							<?php endforeach ?>
							<?php foreach($cols[2] as $a):?>
								<?php 
								$this->load->model('kurikulum/m_ketersediaan_silabus');
								$id_peminatan=$a['id_peminatan'];
								// echo $id_peminatan;
								if($id_peminatan==0){
									$nama_peminatan = array("0"=>array("peminatan_singkatan"=>"&nbsp"));
									
								}else{
								$nama_peminatan = $this->m_ketersediaan_silabus->get_nama_peminatan($id_peminatan)->result_array();
								}
								?>
								<?php //print_r ($nama_peminatan); ?>
									<?php foreach ($nama_peminatan as $np):?>
									<th><?php echo $np['peminatan_singkatan'] ?></th>
									<?php endforeach;?>
								
							<?php endforeach ?>
							</tr>
						</thead>
						<?php $n=count($jenis_kelas);?>
						<tbody>
							<?php $n=0;?>
							<?php $no=1; foreach ($pelajaran as $p):?>
							<?php if($p['id_kurikulum']==$statuspil){ ?>
								<tr>
									<td align="center">
										<?php echo $no; $no++; ?>
									</td>
									<td>
										<?php echo $p['pelajaran']; ?>
									</td>
									<!-- NGECEK MATA PELAJARAN ITU ADA SILABUS NYA GA-->
									<?php $i=0;?>
									
									<?php foreach($jenis_kelas as $jk) :?>
										
										<?php foreach($cols[$i] as $c):?>
												<?php
													if(isset($hasil[$p['pelajaran']][$jk['id_tingkat_kelas']][$c['id_peminatan']])){
														echo "<td>ada</td>";
													}else{
														echo "<td>- </td>";
													} 
												?>
											
											
										<?php endforeach ?>
										<?php $i++;?>
									<?php endforeach?>
								</tr>
							<?php } endforeach;?>
						</tbody>
					</table>

					<?php } ?>
					
					
					
					
					
					
					
					
					
					
					
					<?php if($statuspil==1) {?>
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=3>No.</th>
								<th rowspan=3>Mata Pelajaran</th>
								<th colspan=12>Ketersediaan Silabus</th>
							</tr>
							<tr>
								<?php $i=0;?>
								<?php foreach($jenis_kelas as $j): ?>
								<?php 
								$this->load->model('kurikulum/m_ketersediaan_silabus');
								$id_tingkat_kelas=$j['id_tingkat_kelas'];
								$colspan = $this->m_ketersediaan_silabus->get_colspan($id_tingkat_kelas, $statuspil)->result_array();
								// echo '<pre>';
								// print_r($colspan);
								// echo '</pre>';
								$banyak_colspan[$i] = count($colspan);
								$cols[$i]=$colspan;
								
								?>
							
								<th colspan=<?php echo $banyak_colspan[$i]; $i++; ?>><?php echo $j['tingkat_kelas'] ?></td>		<!--NGEPRINT COLSPAN SAMA TINGKAT KELAS-->
								<?php endforeach; ?>
							</tr>
							<tr>
							<?php foreach($cols[0] as $a):?>
								<?php 
								$this->load->model('kurikulum/m_ketersediaan_silabus');
								$id_peminatan=$a['id_peminatan'];
								// echo $id_peminatan;
								if($id_peminatan==0){
									$nama_peminatan = array("0"=>array("peminatan_singkatan"=>"&nbsp"));
									
								}else{
								$nama_peminatan = $this->m_ketersediaan_silabus->get_nama_peminatan($id_peminatan)->result_array();
								}
								?>
								<?php //print_r ($nama_peminatan); ?>
									<?php foreach ($nama_peminatan as $np):?>
									<th><?php echo $np['peminatan_singkatan'] ?></th>
									<?php endforeach;?>
								
							<?php endforeach ?>
							<?php foreach($cols[1] as $a):?>
								<?php 
								$this->load->model('kurikulum/m_ketersediaan_silabus');
								$id_peminatan=$a['id_peminatan'];
								// echo $id_peminatan;
								if($id_peminatan==0){
									$nama_peminatan = array("0"=>array("peminatan_singkatan"=>"&nbsp"));
									
								}else{
								$nama_peminatan = $this->m_ketersediaan_silabus->get_nama_peminatan($id_peminatan)->result_array();
								}
								?>
								<?php //print_r ($nama_peminatan); ?>
									<?php foreach ($nama_peminatan as $np):?>
									<th><?php echo $np['peminatan_singkatan'] ?></th>
									<?php endforeach;?>
								
							<?php endforeach ?>
							<?php foreach($cols[2] as $a):?>
								<?php 
								$this->load->model('kurikulum/m_ketersediaan_silabus');
								$id_peminatan=$a['id_peminatan'];
								// echo $id_peminatan;
								if($id_peminatan==0){
									$nama_peminatan = array("0"=>array("peminatan_singkatan"=>"&nbsp"));
									
								}else{
								$nama_peminatan = $this->m_ketersediaan_silabus->get_nama_peminatan($id_peminatan)->result_array();
								}
								?>
								<?php //print_r ($nama_peminatan); ?>
									<?php foreach ($nama_peminatan as $np):?>
									<th><?php echo $np['peminatan_singkatan'] ?></th>
									<?php endforeach;?>
								
							<?php endforeach ?>
							</tr>
						</thead>
						<?php $n=count($jenis_kelas);?>
						<tbody>
							<?php $n=0;?>
							<?php $no=1; foreach ($pelajaran as $p):?>
							<?php if($p['id_kurikulum']==$statuspil){ ?>
								<tr>
									<td align="center">
										<?php echo $no; $no++; ?>
									</td>
									<td>
										<?php echo $p['pelajaran']; ?>
									</td>
									<!-- NGECEK MATA PELAJARAN ITU ADA SILABUS NYA GA-->
									<?php $i=0;?>
									
									<?php foreach($jenis_kelas as $jk) :?>
										
										<?php foreach($cols[$i] as $c):?>
												<?php
													if(isset($hasil2[$p['pelajaran']][$jk['id_tingkat_kelas']][$c['id_peminatan']])){
														echo "<td>ada</td>";
													}else{
														echo "<td>- </td>";
													} 
												?>
											
											
										<?php endforeach ?>
										<?php $i++;?>
									<?php endforeach?>
								</tr>
							<?php } endforeach;?>
						</tbody>
					</table>
					
					
					
					
					
					<?php } ?>
					<div class="footer">
						<div class="dataTables_info">Pilih Kurikulum Yang Anda Cari</div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>