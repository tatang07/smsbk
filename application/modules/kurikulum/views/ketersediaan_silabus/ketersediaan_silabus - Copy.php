<?php  
	$statuspil = 0;
	if(isset($_POST['id_kurikulum'])){
	$a = $_POST['id_kurikulum'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Ketersediaan Silabus</h2>
			<?php //echo $statuspil ?>
       </div>
	
       <div class="tabletools">
			
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Jenis Kurikulum</span></th>
							<td width=200px align='left'>
								<select name="id_kurikulum" id="id_kurikulum" onchange="submitform();">
									<option value="0">-Pilih Kurikulum-</option>
									<?php foreach($kur as $q): ?>
										
										<?php if($q->id_kurikulum == $statuspil){ ?>
											<option selected value='<?php echo $q->id_kurikulum; ?>' data-status-pilihan="<?php echo $q->id_kurikulum; ?>"><?php echo $q->nama_kurikulum; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q->id_kurikulum; ?>' data-status-pilihan="<?php echo $q->id_kurikulum; ?>"><?php echo $q->nama_kurikulum; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<?php if($statuspil==2) {?>
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=3>No.</th>
								<!--<th rowspan=3>Kode</th>-->
								<th rowspan=3>Mata Pelajaran</th>
								<th colspan=9>Ketersediaan Silabus</th>
							</tr>
							<tr>
								<th colspan=3>X</td>
								<th colspan=3>XI</th>
								<th colspan=3>XII</th>
							</tr>
							<tr>
								<th>IPA</th>
								<th>IPS</th>
								<th>BAHASA</th>
								<th>IPA</th>
								<th>IPS</th>
								<th>BAHASA</th>
								<th>IPA</th>
								<th>IPS</th>
								<th>BAHASA</th>
							</tr>
						</thead>
				
						<tbody>
							<?php $no=1;?> 
							<?php foreach ($hasil as $pelajaran=>$p) {?>
							<?php $explode = explode('|',$pelajaran);?> 
							<tr>
								<td width='' class='center'><?php echo $no?></td>
								<!--<td width='' class='center'><?php //echo $explode[0] ?></td>-->
								<td><?php echo $explode[1] ?></td>
								<?php foreach($p as $p2=>$p3) {?>
									<?php foreach($p3 as $p4) {?>
									<td width='' class='center'><font color="black"><?php echo $p4 ?></font></td> 
									<?php } ?>
								<?php } ?>
								
							</tr>
							<?php $no=$no+1;?>
							<?php } ?>
					</table>
					<?php }elseif($statuspil==1) {?>
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=3>No.</th>
								<th rowspan=3>Kode</th>
								<th rowspan=3>Mata Pelajaran</th>
								<th colspan=9>Ketersediaan Silabus</th>
							</tr>
							<tr>
								<th colspan=3>X</td>
								<th colspan=3>XI</th>
								<th colspan=3>XII</th>
							</tr>
							<tr>
								<th>IPA</th>
								<th>IPS</th>
								<th>BAHASA</th>
								<th>IPA</th>
								<th>IPS</th>
								<th>BAHASA</th>
								<th>IPA</th>
								<th>IPS</th>
								<th>BAHASA</th>
							</tr>
						</thead>
				
						<tbody>
							<?php $no=1;?> 
							<?php foreach ($hasil2 as $pelajaran=>$p) {?>
							<?php $explode = explode('|',$pelajaran);?> 
							<tr>
								<td width='' class='center'><?php echo $no?></td>
								<td width='' class='center'><?php echo $explode[0] ?></td>
								<td><?php echo $explode[1] ?></td>
								<?php foreach($p as $p2=>$p3) {?>
									<?php foreach($p3 as $p4) {?>
									<td width='' class='center'><font color="black"><?php echo $p4 ?></font></td> 
									<?php } ?>
								<?php } ?>
								
							</tr>
							<?php $no=$no+1;?>
							<?php } ?>
					</table>
					<?php } ?>
					<div class="footer">
						<div class="dataTables_info">Pilih Kurikulum Yang Anda Cari</div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>