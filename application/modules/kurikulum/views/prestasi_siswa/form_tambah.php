	<h1 class="grid_12">Kurikulum</h1>
		
			<form action="<?php echo base_url('kurikulum/prestasi_siswa/submit_add'); ?>" method="post" name='myform' enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Daftar Prestasi Ekstra Kurikuler</legend><?php //echo($tingkat_kelas);?>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Ekstrakurikuler</strong>
						</label>
						<div>
						<select name="ekskul" id="ekskul"  data-placeholder="-- Pilih Ekstra Kurikuler --">
							<option value="">-- Pilih Ekstra Kurikuler --</option>
							<?php foreach($ekskul as $d):?>
								 <option value="<?php echo $d->id_ekstra_kurikuler; ?>"> <?php echo $d->nama_ekstra_kurikuler;?></option>
							 <?php endforeach ?>
						</select> 
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Siswa</strong>
						</label>
						<div>
							<select name="id_siswa" id="id_siswa" data-placeholder="-- Pilih Siswa --">
								<option value="">-- Pilih Siswa --</option>
							</select> 
					
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Prestasi</strong>
						</label>
						<div>
							<input type="text" name="prestasi" value="" />
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal</strong>
						</label>
						<div>
							<input type="date" name="tanggal" value="" />
							
						</div>
					</div>

					<div class="row">
						<label for="f1_normal_input">
							<strong>Kategori</strong>
						</label>
						<div>
							<input type="text" name="kategori" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Perlombaan</strong>
						</label>
						<div>
							<input type="text" name="perlombaan" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tingkat Kota</strong>
						</label>
						<div>
							<select name="id_tingkat_wilayah" data-placeholder="-- Pilih Tingkat Kota --">
							<option value="">-- Pilih Tingkat Kota --</option>
							<?php foreach($tingkat_wilayah as $d):?>
							  <option value="<?php echo $d['id_tingkat_wilayah']?>"><?php echo $d['tingkat_wilayah']?></option>
							 <?php endforeach ?>
							</select> 
					
						</div>
					</div>
					
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/prestasi_siswa/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		
<script>
$(function(){
	$(document).on('change', '#ekskul', function(){
		var id_ekskul = $(this).val();
		$.post("<?php echo site_url('kurikulum/prestasi_siswa/get_siswa'); ?>/" + id_ekskul, function(data){
			$("#id_siswa").html(data);
			$("#id_siswa").trigger("chosen:updated");
		});
	});
});
</script>