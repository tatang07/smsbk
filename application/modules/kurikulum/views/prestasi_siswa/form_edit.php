	<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>

			<form action="<?php echo base_url('pps/bp_bk/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Daftar Catatan BP/BK </legend>
				<?php foreach($data_edit as $t){?>
				<?php //echo $t['program_kerja']; ?>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kelas</strong>
						</label>
						<div>
							
							<select disabled name="kelas" id="kelas" class="b" >
							
							<?php foreach($kelas as $d):?>
								<?php if($tingkat_kelas==$d['id_tingkat_kelas']){?>
							  <option selected value="<?php echo $d['id_tingkat_kelas']?>"><?php echo $d['tingkat_kelas']?></option>
								<?php }else { ?>
								 <option value="<?php echo $d['id_tingkat_kelas']?>"><?php echo $d['tingkat_kelas']?></option>
								<?php } ?>
							 <?php endforeach ?>
							</select> 
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Siswa</strong>
						</label>
						<div>
						<input type="hidden" name="id_siswa" value="<?php echo $t['id_siswa'] ?>">
							<select disabled id="id_siswa" class="b" >
							<?php foreach($tabi as $d):?>
								<?php if($d['id_siswa'] == $t['id_siswa']){?>
							  <option selected value="<?php echo $d['id_siswa']?>"><?php echo $d['nama']?></option>
							<?php}else{ ?>
								  <option value="<?php echo $d['id_siswa']?>"><?php echo $d['nama']?></option>
							<?php } ?>
							 <?php endforeach ?>
							</select> 
							
						</div>
					</div>
			
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal </strong>
						</label>
						<div>
							<input type="hidden" name="id" value="<?php echo $t['id_siswa_catatan_bk']?>" />
							<input type="date" name="tanggal" id="tanggal" class="a" value="<?php echo $t['tanggal_catatan']?>" />
							<?php $tgl=$t['tanggal_catatan']?>
						</div>
					</div>

					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="keterangan" id="keterangan" class="a" value="<?php echo $t['keterangan']?>" />
							<?php $ket=$t['keterangan']?>
						</div>
					</div>
					
				<?php } ?>
				</fieldset>
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pps/bp_bk/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
