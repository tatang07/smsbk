
<?php 
	$statuspil = 0;
	if(isset($_POST['id_ekstra_kurikuler'])){
	$a = $_POST['id_ekstra_kurikuler'];
	$statuspil = $a; } ?>

<h1 class="grid_12">Ekstra Kurikuler</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Jadwal Program Ekstra Kurikuler</h2>
       </div>
	   <?php //print_r($data)?>
       <div class="tabletools">
			<div class="right">
			<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
			  	<a href="<?php echo site_url('kurikulum/jadwal_ekskul/tambah_data/')?>"><i class="icon-plus"></i>Tambah</a> 
			<?php } ?>
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form action="" name= "myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=80px align='left'><span class="text">Jenis Program</span></th>
							<td width=200px align='left'>
								<select name="id_ekstra_kurikuler" id="id_ekstra_kurikuler" onchange="submitform();">
									<option>-- Pilih Jenis Program --</option>
									<?php foreach ($list_ekskul as $pd): ?>
									<?php if($pd['id_ekstra_kurikuler']==$statuspil){ ?>
									  <option selected value="<?php echo $pd['id_ekstra_kurikuler'];?>"><?php echo $pd['nama_ekstra_kurikuler'];?></option>
									<?php }else{ ?>
									  <option value="<?php echo $pd['id_ekstra_kurikuler'];?>"><?php echo $pd['nama_ekstra_kurikuler'];?></option>
									<?php } endforeach ?>
								</select> 
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Hari</th>
								<th>Waktu</th>
								<th>Tempat</th>
								<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
									<th>Aksi</th>
								<?php } ?>
							</tr>
						</thead>
						
						<tbody>
							<?php $no=1;?>
							<?php foreach($data as $d):?>
							<?php if($d['id_ekstra_kurikuler']==$statuspil){ ?>
							<tr>
								<td width='' class='center'><?php echo $no; $no++; ?></td>
								<td width='' class='center'><?php echo $d['hari'];?></td>
								<td width='' class='center'><?php echo $d['jam_mulai'];?>-<?php echo $d['jam_selesai'];?></td>
								<td width='' class='center'><?php echo $d['tempat'];?></td>
								<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
									<td width='' class='center'>
									<a href="<?php echo site_url('kurikulum/jadwal_ekskul/edit_data/'.$d['id_ekstra_kurikuler_jadwal'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a href="<?php echo site_url('kurikulum/jadwal_ekskul/delete/'.$d['id_ekstra_kurikuler_jadwal'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a></td>	
								<?php } ?>
							</tr>
							<?php } endforeach?>		
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $no-1; ?></div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>

 
<script>
	
	function submitform()
	{
	  document.myform.submit();
	}
</script>
 