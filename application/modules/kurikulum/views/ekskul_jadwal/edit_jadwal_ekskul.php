
<h1 class="grid_12">Ekstra Kurikuler</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>TAMBAH JADWAL EKSTRA KURIKULER</h2>
       </div>
		
		<form action="<?php echo base_url('kurikulum/jadwal_ekskul/simpan_edit_data'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<?php foreach($data as $d):?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenis Ekstra Kurikuler</strong>
						</label>
						<div>
						<input type="hidden" name="id" value="<?php echo $id; ?>">
							<select name="id_ekstra_kurikuler">
							<?php foreach ($list_ekskul as $pd): ?>
							  <option value="<?php echo $pd['id_ekstra_kurikuler'];?>"><?php echo $pd['nama_ekstra_kurikuler'];?></option>
							<?php endforeach ?>
							</select> 
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Hari:</strong>
						</label>
						<div>
							<select name="hari">
								<?php if($d['hari']=="Senin"){?>
								<option selected value="Senin">Senin</option>
								<?php }else {?>
								<option value="Senin">Senin</option>
								<?php } ?>
								<?php if($d['hari']=="Selasa"){?>
								<option selected value="Selasa">Selasa</option>
								<?php }else {?>
								<option value="Selasa">Selasa</option>
								<?php } ?>
								<?php if($d['hari']=="Rabu"){?>
								<option selected value="Rabu">Rabu</option>
								<?php }else {?>
								<option value="Rabu">Rabu</option>
								<?php } ?>
								<?php if($d['hari']=="Kamis"){?>
								<option selected value="Kamis">Kamis</option>
								<?php }else {?>
								<option value="Kamis">Kamis</option>
								<?php } ?>
								<?php if($d['hari']=="Jumat"){?>
								<option selected value="Jumat">Jumat</option>
								<?php }else {?>
								<option value="Jumat">Jumat</option>
								<?php } ?>
								<?php if($d['hari']=="Sabtu"){?>
								<option selected value="Sabtu">Sabtu</option>
								<?php }else {?>
								<option value="Sabtu">Sabtu</option>
								<?php } ?>
								<?php if($d['hari']=="Minggu"){?>
								<option selected value="Minggu">Minggu</option>
								<?php }else {?>
								<option value="Minggu">Minggu</option>
								<?php } ?>
							</select> 
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jam Mulai :</strong>
						</label>
						<div>
							<input type="time" name="jam_mulai" value="<?php echo $d['jam_mulai'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jam Selesai :</strong>
						</label>
						<div>
							<input type="time" name="jam_selesai" value="<?php echo $d['jam_selesai'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tempat :</strong>
						</label>
						<div>
							<input type="text" name="tempat" value="<?php echo $d['tempat'] ?>" />
						</div>
					</div>
				<?php endforeach; ?>
				<fieldset>
				<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							<a href="<?php echo base_url('kurikulum/jadwal_ekskul/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						
				</div>
		</form>
    </div>
 </div>
