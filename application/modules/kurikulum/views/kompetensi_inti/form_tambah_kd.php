	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/kompetensi_inti/submit_kd'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
				<?php if($action == 'add'){?>
					<legend>Tambah Kompetensi Dasar</legend>
				<?php }else{ ?>
					<legend>Edit Kompetensi Dasar</legend>
				<?php } ?>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kompetensi Dasar</strong>
						</label>
						<div>
							<input type="text" name="kompetensi_dasar" value="<?php echo $kompetensi_dasar; ?>" />
							<input type="hidden" name="action" value="<?php echo $action; ?>" />
							<input type="hidden" name="id_kompetensi_dasar" value="<?php echo $id_kompetensi_dasar; ?>" />
							<input type="hidden" name="id" value="<?php echo $id; ?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Standar Kompetensi</strong>
						</label>
						<div>
							<select name="id_standar_kompetensi" data-placeholder="Pilih Standar Kompetensi">
							<?php foreach($standar_kompetensi as $t): ?>
								<?php if($t->id_standar_kompetensi == $id_standar_kompetensi){ ?>
									<option selected value="<?php echo $t->id_standar_kompetensi; ?>"><?php echo  word_limiter($t->standar_kompetensi, 10); ?></option>
								<?php }else{ ?>
									<option value="<?php echo $t->id_standar_kompetensi; ?>"><?php echo word_limiter($t->standar_kompetensi, 13); ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/kompetensi_inti/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		