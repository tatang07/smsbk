
<?php 
	$statuspil = $cek;
	if(isset($_POST['id_kurikulum'])){
	$a = $_POST['id_kurikulum'];
	$statuspil = $a; } ?>
	
<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">

	<div class="box with-table">
		
		<div class="tabletools">
			<div class="right">
				<?php if($statuspil == 2 && ($gid == 1 || $gid == 3)){ ?>
					<a href="<?php echo base_url('kurikulum/kompetensi_inti/tambah/'); ?>"><i class="icon-plus"></i>Tambah Kompetensi Inti</a>
				<?php } ?>
				<br/><br/>
			</div>
			
			<div class="dataTables_filter">
				<form action="" name= "myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Jenis Kurikulum</span></th>
							<td width=200px align='left'>
								<select name="id_kurikulum" id="id_kurikulum" onchange="submitform();">
									<option value="0">-Pilih Kurikulum-</option>
								<?php foreach($kur as $q): ?>
									
									<?php if($q->id_kurikulum == $statuspil){ ?>
										<option selected value='<?php echo $q->id_kurikulum; ?>' data-status-pilihan="<?php echo $q->id_kurikulum; ?>"><?php echo $q->nama_kurikulum; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $q->id_kurikulum; ?>' data-status-pilihan="<?php echo $q->id_kurikulum; ?>"><?php echo $q->nama_kurikulum; ?></option>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>

		</div>
	</div>
	
	<?php if($statuspil ==2){ ?>
		
	<div class="box with-table tabbedBox" id="field1">
		
		<div class="header">
			<h2>Kompetensi Inti</h2>
			<ul>
				<?php if($tingkat_kelas){foreach($tingkat_kelas as $t): ?>
					<li><a href="#<?php echo $t->tingkat_kelas; ?>"><?php echo $t->tingkat_kelas; ?></a></li>
				<?php endforeach; } ?>
			</ul>
		</div><!-- End of .header -->
		<div class="content tabbed">
			<?php foreach($tingkat_kelas as $t): ?>
			<div id="<?php echo $t->tingkat_kelas; ?>">
				<div id="datatable" url="">
					<div class="dataTables_wrapper"  role="grid">    
						<table class="styled" >
							<thead>
								<tr>
									<th>No.</th>
									<th>Kode</th>
									<th>Deskripsi</th>
									<?php if($gid == 1){ ?>
										<th>Aksi</th>
									<?php } ?>
								</tr>
							</thead>
							<tbody>
								<?php 
									if($kompetensi_inti){
									$no=1; foreach($kompetensi_inti as $d): 
										
										if($d->id_tingkat_kelas==$t->id_tingkat_kelas && $d->id_kurikulum == $statuspil){
								?>
								
								<tr>
									<td width='30px' class= 'center'><?php echo $no; ?></td>
									<td width='50px' class= 'center'><?php echo $d->kode_kompetensi_inti; ?></td>
									<td width="600px"><?php echo $d->kompetensi_inti; ?></td>
										<?php if($gid == 1){ ?>
											<td width="" class= 'center'>
											<a href="<?php echo base_url('kurikulum/kompetensi_inti/edit/'.$d->id_kompetensi_inti); ?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
											<a href="<?php echo base_url('kurikulum/kompetensi_inti/delete/'.$d->id_kompetensi_inti); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Delete</a></td>
										<?php } ?>
										
								</tr>
								<?php $no++; }
										endforeach; } ?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<?php endforeach; ?>
		</div>        
	</div>
	<?php }elseif($statuspil == 1){
		$dat['statuspil'] = $statuspil;
		$this->load->view('kompetensi_inti/standar_kompetensi', $dat);
		
	?>
		
	<?php }else{ ?>
		<div class="box with-table">
		 <div id="datatable" url="">
			<div class="dataTables_wrapper"  role="grid">
				<div class="content">
					<div class="footer">
						<div class="dataTables_info">Pilih Kurikulum Yang Anda Cari</div>
					</div>
				</div>
			</div>
		 </div>
		</div>
	<?php } ?>

</div>


<script>
	
	function submitform()
	{
	  document.myform.submit();
	}
</script>