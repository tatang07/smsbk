	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/kompetensi_inti/submit_post'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Kompetensi Inti</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kode Kompetensi Inti</strong>
						</label>
						<div>
							<input type="text" name="kode_kompetensi_inti" value="" />
							<input type="hidden" name="action" value="add" />
							<input type="hidden" name="id_kurikulum" value="2" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Kompetensi Inti</strong>
						</label>
						<div>
							<textarea class="nogrow" rows=5 name="kompetensi_inti" ></textarea>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Tingkat Kelas</strong>
						</label>
						<div>
							<select name="id_tingkat_kelas" data-placeholder="Choose a Name">
							<?php foreach($tingkat_kelas as $t): ?>
									<option value="<?php echo $t->id_tingkat_kelas; ?>"><?php echo $t->tingkat_kelas; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
						
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/kompetensi_inti/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		