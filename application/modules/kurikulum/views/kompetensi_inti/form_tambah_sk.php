	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/kompetensi_inti/submit_sk'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
				<?php if($action == 'add'){?>
					<legend>Tambah Standar Kompetensi</legend>
				<?php }else{ ?>
					<legend>Edit Standar Kompetensi</legend>
				<?php } ?>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Standar Kompetensi</strong>
						</label>
						<div>
							<input type="text" name="standar_kompetensi" value="<?php echo $standar_kompetensi; ?>" />
							<input type="hidden" name="action" value="<?php echo $action; ?>" />
							<input type="hidden" name="id_standar_kompetensi" value="<?php echo $id_standar_kompetensi; ?>" />
						</div>
					</div>
					
					<?php $tingkat_kelas =  array("0"=>"-Pilih Tingkat-")+get_tingkat_kelas() ?>								
					<?php $tingkat_matpel =  get_matpel_per_tingkat(1) ?>
													
					<?php
						$param['parent'] = $tingkat_kelas;	
						$param['child'] = $tingkat_matpel;		
						$param['parent_name'] = "tingkat";														
						$param['current_parent'] = $id_tingkat_kelas;								
						$param['current_child'] = 1;	
						$param['name'] = "id_pelajaran";	
					?>

					<div class="row">
						<label for="f2_select2">
							<strong>Tingkat</strong>
						</label>
						<div>
							<?php echo simple_form_dropdown_clear("id_tingkat_kelas",$tingkat_kelas,1," id='idtingkat' onChange=tingkatChanged(this.value)",null) ?>
						</div>
					</div>
					<div class="row">
						<label for="f2_select2">
							<strong>Mata Pelajaran</strong>
						</label>
						<div>
							<?php $this->load->view("html_template/linked_box_child",$param) ?>
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/kompetensi_inti/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		