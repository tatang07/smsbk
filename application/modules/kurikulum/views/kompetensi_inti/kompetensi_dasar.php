<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Kompetensi Dasar</h2>
		</div>
		<div class="tabletools">
			<div class="right">
				<?php if($gid == 1 || $gid == 2 || $gid == 3){ ?>
					<a href="<?php echo base_url('kurikulum/kompetensi_inti/tambah_kd/'.$id); ?>"><i class="icon-plus"></i>Tambah</a>
				<?php } ?>
				<br/><br/>
			</div>
			
			<div class="dataTables_filter">
				<table>
					<tr>
						<th width=200px align='left'><span class="text"><span>Tingkat</span></th>
						<th width=30px align='left'><span class="text"><span>:</span></th>
						<td width=400px align='left'><?php echo $tingkat_kelas; ?></td>
					</tr>
					<tr>
						<th width=200px align='left'><span class="text"><span>Mata Pelajaran</span></th>
						<th width=30px align='left'><span class="text"><span>:</span></th>
						<td width=400px align='left'><?php echo $kode_pelajaran.' - '.$pelajaran; ?></td>
					</tr>
					<tr>
						<th width=200px align='left'><span class="text"><span>Standar Kompetensi</span></th>
						<th width=30px align='left'><span class="text"><span>:</span></th>
						<td width=400px align='left'><?php echo $standar_kompetensi; ?></td>
					</tr>
				</table>
			</div>

		</div>
	</div>
	
	<div class="box with-table">
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kompetensi Dasar</th>
								<?php if($gid == 1 || $gid == 2 || $gid == 3){ ?>
									<th>Aksi</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php 
								if($data){
								$no=1; foreach($data as $d): 
							?>
							
							<tr>
								<td width='30px' class= 'center'><?php echo $no; ?></td>
								<td width="600px"><?php echo $d->kompetensi_dasar; ?></td>
								<?php if($gid == 1 || $gid == 2 || $gid == 3){ ?>
								<td width="150px" class= 'center'>
									<a href="<?php echo base_url('kurikulum/kompetensi_inti/edit_kd/'.$id.'/'.$d->id_kompetensi_dasar); ?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a href="<?php echo base_url('kurikulum/kompetensi_inti/delete_kd/'.$id.'/'.$d->id_kompetensi_dasar); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Delete</a>
								</td>
								<?php } ?>
									
							</tr>
							<?php $no++;
									endforeach; } ?>
						</tbody>
					</table>
				</div>
				
			</div>
		</div>        
	</div>
				<a href="<?php echo base_url('kurikulum/kompetensi_inti/search_sk/'.$id_tingkat_kelas.'/'.$id_pelajaran); ?>"> <input value="Kembali ke Standar Kompetensi" type="button"></a>
</div>