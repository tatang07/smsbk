
	<div class="box with-table">
		<div class="header">
			<h2>Standar Kompetensi</h2>
		</div>
		<div class="tabletools">
			<div class="right">
			<?php if($gid == 1 || $gid == 2 || $gid == 3){ ?>
				<a href="<?php echo base_url('kurikulum/kompetensi_inti/tambah_sk/'); ?>"><i class="icon-plus"></i>Tambah</a>
			<?php } ?>
				<br/><br/>
			</div>
			<div class="dataTables_filter">
				<form action="<?php echo base_url('kurikulum/kompetensi_inti/search_sk'); ?>" method="post" enctype="multipart/form-data">
					<?php $this->load->view("komponen/kelas_matpel"); ?>
				</form>
			</div>

		</div>
	</div>
	
	<div class="box with-table">
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Standar Kompetensi</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
								<?php 
									if($datalist){
									$no=1; foreach($datalist as $d): 		
								?>
								
								<tr>
									<td width='30px' class= 'center'><?php echo $no; ?></td>
									<td width="600px"><?php echo $d->standar_kompetensi; ?></td>
									<td width="" class= 'center'>
										<a href="<?php echo base_url('kurikulum/kompetensi_inti/detail_sk/'.$d->id_standar_kompetensi); ?>" class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Kompetensi Dasar</a>
										<?php if($gid == 1){ ?>
										<a href="<?php echo base_url('kurikulum/kompetensi_inti/edit_sk/'.$d->id_standar_kompetensi); ?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
										<a href="<?php echo base_url('kurikulum/kompetensi_inti/delete_sk/'.$d->id_standar_kompetensi); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Delete</a>
										<?php } ?>
									</td>
									
								</tr>
								<?php $no++;
										endforeach; }else{ ?>
										<tr>
											<td colspan=3>Data Tidak Ditemukan!</td>
										</tr>
										<?php } ?>
							</tbody>
						</tbody>
					</table>
					
				</div>
			</div>
		</div>
	</div>