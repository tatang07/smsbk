<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Penentuan Guru Mata Pelajaran</h2>
       </div>
       <div class="tabletools">
       		<div class="right">
       			<?php if($id_tingkat_kelas > 0){ ?>
			  		<a href="<?php echo base_url('kurikulum/penentuan_guru_matpel/tambah/'.$id_rombel.'/'.$id_tingkat_kelas) ?>"><i class="icon-plus"></i>Tambah</a> 
			  	<?php } ?>
			  <br><br>
            </div>
       		<div class="dataTables_filter">
				<form action="<?php echo base_url('kurikulum/penentuan_guru_matpel/tampil'); ?>" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Tingkat</span></th>
							<td width=200px align='left'>
								<select name="tingkat_kelas" id="tingkat_kelas" onchange="submitform();">
									<option value='0'>-Pilih Tingkat-</option>
									<?php foreach($tingkat_kelas as $idt=>$t): ?>
										<?php if($idt == $id_tingkat_kelas){ ?>
											<option selected value='<?php echo $idt; ?>' data-status-pilihan="<?php echo $idt; ?>"><?php echo $t; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $idt; ?>' data-status-pilihan="<?php echo $idt; ?>"><?php echo $t; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>

						<tr>
							<th width=100px align='left'><span class="text"><span>Kelas</span></th>
							<td width=200px align='left'>
								<select name="kelas" id="kelas" class="chosen-select">
									<?php if(isset($rombels)) echo $rombels; ?>
								</select>
							</td>
						</tr>

						<tr>
							<td colspan= '2' width=300px align='right'>
								<input type="hidden" value="id_tingkat_kelas">
								<input type="submit" value="Tampilkan" name=send />
							</td>
						</tr>
					</table>
				</form>
			</div>
        </div>

        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid"> 
					<table class="styled" >
						<thead>
							<tr>
								<th>No</th>
								<th>Mata Pelajaran</th>
								<th>Guru</th>
								<th>Aksi</th>
							</tr>
						</thead>
				
						<tbody>
							<?php if(isset($mapel)){ ?>
								<?php $i=1; ?>
							 		<?php foreach ($mapel as $m) { ?>
							 		<?php if($m['id_sekolah'] == get_id_sekolah()) { ?>
										<tr>
											<td width='7%' class='center'><?php echo $i; ?></td>
											<td width='' class='left'><?php echo $m['kode_pelajaran'].' - '.$m['pelajaran'] ?></td>
											<td width='' class='left'><?php echo $m['nama'] ?></td>	
											<td width='10%' align="center">
												<a href="<?php echo base_url('kurikulum/penentuan_guru_matpel/delete/'.$m['id_guru_matpel_rombel'].'/'.$id_rombel.'/'.$id_tingkat_kelas); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
											</td>																				
										</tr>
										<?php $i++; ?>
									<?php } } ?>
							<?php } ?>
						</tbody>
					</table>
					<div class="footer">

					</div>
				</div>
            </div>
        </div>  
    </div>
</div>

<script>
function generate_rombel(id_tingkat_kelas)
{
	$.post("<?php echo site_url('kurikulum/penentuan_guru_matpel/getRombel'); ?>/" + id_tingkat_kelas, function(data){
		$("#kelas").html(data);
		$("#kelas").trigger("chosen:updated");
	});
}

$(function(){
	$(document).on('change', '#tingkat_kelas', function(){
		generate_rombel($(this).val());
	});
});
</script>