<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
	<div class="box with-table">
		<div class="header">
       		<h2>Penentuan Guru Mata Pelajaran</h2>
		</div>
	    <div class="tabletools">
			<div class="dataTables_filter">
			<table>
				<tr>
					<th width=100px align='left'><span class="text"><span>Kurikulum</span></th>
					<td width=20px align='left'>:</td>
					<td width=200px align='left'> <?php echo $kurikulum; ?></td>
				</tr>
				<tr>
					<th width=100px align='left'><span class="text"><span>Tahun Ajaran</span></th>
					<td width=20px align='left'>:</td>
					<td width=200px align='left'> <?php echo $tahun_ajaran; ?></td>
				</tr>
				<tr>
					<th width=100px align='left'><span class="text"><span>Kelas</span></th>
					<td width=20px align='left'>:</td>
					<td width=200px align='left'> <?php echo $namarombel; ?></td>
				</tr>
				<tr>
					<th width=100px align='left'><span class="text"><span>Peminatan</span></th>
					<td width=20px align='left'>:</td>
					<td width=200px align='left'> <?php echo $namapeminatan; ?></td>
				</tr>
			</table>
			</div>
		</div>
	</div>
    <div class="box with-table">
       <div class="header">
       		<h2>Tambah Guru Mata Pelajaran kelas</h2>
       </div>

        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid"> 
					<table class="styled" >
						<thead>
							<tr>
								<th colspan=2>Mata Pelajaran</th>
								<th>Guru</th>
							</tr>
						</thead>
				
						<tbody>
							<form action="<?php echo base_url('kurikulum/penentuan_guru_matpel/simpan'); ?>" name="myform" method="post" enctype="multipart/form-data">
								<?php if(isset($id_rombel)){ ?>
									<?php $i=0; ?>
									<?php if($mapel){ ?>
										<?php foreach ($kelompok_pelajaran as $kp) { ?>
											<tr>
												<td colspan=3 width='' class='left'><b><?php echo $kp['kelompok_pelajaran'] ?></b></td>
											</tr>

											<?php if($kp['status_pilihan']==1) { ?>
												<?php foreach ($peminatan as $p) { ?>
													<?php if($p['id_kelompok_pelajaran']==$kp['id_kelompok_pelajaran']) { ?>
														<tr>
															<td colspan=3 width='' class='left'><?php echo $p['peminatan'] ?></td>
														</tr>
														<?php $no=1; foreach ($mapel as $m) { ?>
															<?php if($m['id_kelompok_pelajaran']==$kp['id_kelompok_pelajaran'] && $m['id_peminatan']==$p['id_peminatan']) { ?>
															<tr>
																<td width='50px' class='center'><?php echo $no++ ?></td>
																<td width='' class='left'><?php echo $m['pelajaran'] ?></td>
																<td width='' class='center'>
																	<select name="id_guru[<?php echo $i; ?>]">
																		<option selected><i>--- Pilih Guru ---</i></option>
																		<?php foreach ($guru as $g) {?>
																			<option value="<?php echo $g['id_guru'] ?>"> <?php echo $g['nama'] ?> </option>
																		<?php }?>
																	</select>
																</td>																					
															</tr>
															<input type="hidden" name="id_rombel[<?php echo $i; ?>]" value="<?php echo $id_rombel; ?>">
															<input type="hidden" name="id_pelajaran[<?php echo $i; ?>]" value="<?php echo $m['id_pelajaran'] ?>">
															<?php $i++; ?>
															<?php }?>
														<?php }?>
													<?php }?>
												<?php }?>
											<?php }else{ ?>
												<?php $no=1; foreach ($mapel as $m) { ?>
													<?php if($m['id_kelompok_pelajaran']==$kp['id_kelompok_pelajaran']) { ?>
														<tr>
															<td width='50px' class='center'><?php echo $no++ ?></td>
															<td width='' class='left'><?php echo $m['pelajaran'] ?></td>
															<td width='' class='center'>
																<select name="id_guru[<?php echo $i; ?>]">
																	<option selected><i>--- Pilih Guru ---</i></option>
																	<?php foreach ($guru as $g) {?>
																		<option value="<?php echo $g['id_guru'] ?>"> <?php echo $g['nama'] ?> </option>
																	<?php }?>
																</select>
															</td>																					
														</tr>
														<input type="hidden" name="id_rombel[<?php echo $i; ?>]" value="<?php echo $id_rombel; ?>">
														<input type="hidden" name="id_pelajaran[<?php echo $i; ?>]" value="<?php echo $m['id_pelajaran'] ?>">
														<?php $i++; ?>
													<?php }?>
												<?php } ?>
											<?php } ?>
										<?php } ?>
									<?php } ?>
								<?php } ?>
								<input name='idr' type="hidden" value="<?php echo $id_rombel ?>">
								<input name='idk' type="hidden" value="<?php echo $id_tingkat_kelas ?>">
						</tbody>
					</table>
								<div class="actions">
									<div class="left">
										<input type="submit" value="Simpan" name=send />
										<a href="<?php echo base_url('kurikulum/penentuan_guru_matpel/tampil/'. $id_rombel.'/'.$id_tingkat_kelas); ?>"> <input value="Batal" type="button"></a>
									</div>
								</div>
							</form>
					<div class="footer">

					</div>
				</div>
            </div>
        </div>  
    </div>
</div>

<script>
$(function(){
	$(document).on('change', '#tingkat_kelas', function(){
		var id_tingkat_kelas = $(this).val();
		$.post("<?php echo site_url('kurikulum/penentuan_guru_matpel/getRombel'); ?>/" + id_tingkat_kelas, function(data){
			$("#kelas").html(data);
			$("#kelas").trigger("chosen:updated");
		});
	});
});
</script>