		<?php $myconf= $conf['data_list']?>
        <p class='subjudul' align='center'>
			<b> <?php echo strtoupper($myconf['title']) ?>
			 <?php echo !empty($myconf['subtitle'])?"<br>".$myconf['subtitle']:"<br>"."Daftar ".$myconf['title'] ?>
			</b>
		</p>
		
		<p class='subjudul'>
			<?php foreach($myconf['field_filter_dropdown'] as $key=>$det): ?>
				<?php if(isset($det['filter'])):?>
					<?php $list_dropdown = get_list_filter($det['table'],$det['table_id'],array($det['table_label']),array($det['filter']=>$det['filter_value'])) ?>
				<?php elseif(isset($det['table'])):?>
					<?php $list_dropdown = get_list($det['table'],$det['table_id'],$det['table_label']) ?>
				<?php else:?>
					<?php $list_dropdown = $det['list']?>
				<?php endif?>
				<?php if(isset($filter[$key])):?>
					<?php if($filter[$key]>0):?>
						<?php echo isset($det['label'])?strtoupper($det['label'])." : ":"" ?><?php echo strtoupper($list_dropdown[$filter[$key]]) ?><br/>
					<?php endif ?>
				<?php endif ?>
			<?php endforeach ?>
		<?php echo empty($filter['keywords'])?"":strtoupper("Filter Pencarian: ".$filter['keywords'])."<br/>" ?>
		<br/></p><br/>
		<table class="tbl" border=1px>
                <thead>
                    <tr>
						<th style="width:5%">No.</th>
						<?php foreach($myconf['field_list'] as $key): ?>  
							<?php $label = isset($conf['data_label'][$key])?$conf['data_label'][$key]:"" ?>
							<th><?php echo $label?></th>
						<?php endforeach ?>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=1 ?>
                    <?php foreach ($alldata as $a): ?>	
                        <tr>
                            <td style='width:5%;text-align:center; vertical-align:top'><?php echo $i++ ?></td>
							<?php foreach($myconf['field_list'] as $key): ?> 
								<?php $size = isset($conf['data_export']['field_size'][$key])?" width:".$conf['data_export']['field_size'][$key]."; ":"" ?>
								<?php $align = isset($myconf['field_align'][$key])?" text-align:".$myconf['field_align'][$key]."; ":"" ?>
								<?php $mystyle = " style='".$size.$align." vertical-align:top;'";?>
								<?php $val = $a[$key] ?>
								<?php $val = isset($conf['data_type'][$key]) && $conf['data_type'][$key]=="date"? tanggal_view($val):$val ?>
								<?php if(in_array($key,$myconf["field_separated"])):?>
									<?php $val = str_replace(";","<br/>",$val)?>
								<?php endif ?>
								<td <?php echo $mystyle?> ><?php echo $val ?></td>
                            <?php endforeach ?>							
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>