<?php $myconf= $conf['data_list']?>
<h1 class="grid_12"><?php echo $myconf['title'] ?></h1>
<div class="grid_12">
	<div class="box with-table">
		<div class="header">
			<h2><?php echo !empty($myconf['subtitle'])?$myconf['subtitle']:"Daftar ".$myconf['title'] ?></h2>
		</div>
		<div class="tabletools">
			
			<div class="dataTables_filter">
				<form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<?php foreach($myconf['field_filter_dropdown'] as $key=>$det): ?>
								<?php if(isset($det['filter'])):?>
									<?php $list_dropdown = get_list_filter($det['table'],$det['table_id'],array($det['table_label']),array($det['filter']=>$det['filter_value'])) ?>
								<?php elseif(isset($det['table'])):?>
									<?php $list_dropdown = get_list($det['table'],$det['table_id'],$det['table_label']) ?>
								<?php else:?>
									<?php $list_dropdown = $det['list']?>
								<?php endif?>
								<?php $det['is_all'] = isset($det['is_all'])? $det['is_all']: TRUE ?>
								<?php if($det['is_all']):?>
									<?php $list_dropdown = array(''=>"-Semua-")+$list_dropdown?>								
								<?php endif ?>
								<td width=1px>&nbsp;</td>
								<th width=1px align='left'><span class="text"><?php echo isset($det['label'])?$det['label']:"&nbsp;" ?>:</span></th>
								<td width=150px>
									<select name='filter[<?php echo str_replace(".","__",$key) ?>]' style='width:100px'>
										<?php foreach($list_dropdown as $id=>$val):?>
											<option value='<?php echo $id?>'><?php echo $val ?></option>
										<?php endforeach ?>
									</select>
								</td>
							<?php endforeach ?>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
					<?php foreach($myconf['field_filter_hidden'] as $key=>$det): ?>
						<?php echo form_hidden('filter['.$key.']',$det)?>
					<?php endforeach ?>
					<input id='sorting' type=hidden name='sorting' value=''>
				</form>
			</div>

		</div>
		<div class="content">
			<?php $url = $this->uri->ruri_string() ?>
			<?php $url = str_replace("datalist","datatable",$url) ?>
			<div id="datatable" url="<?php echo site_url($conf['name']."/datatable")?>" >
				<?php $this->load->view($myconf['view_file_datatable']); ?>
			</div>
		</div>        
	</div>
</div>