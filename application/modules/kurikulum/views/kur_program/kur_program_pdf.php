<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
	
	<div class="box with-table">
		
		<div class="header">
			<h2>Program</h2>
		</div>
	
	</div>

	<?php $w=1; foreach($mapel_pilihan as $pemi => $val_minat): ?>
	
	<div class="box with-table">
		
		<div class="header">
			<h2><?php echo $pemi; ?></h2>
		</div>
		
		<div class="content">
				<?php $arr = array(); ?>
				<?php foreach($tingkat_kelas as $t): ?>
					<?php $arr[$t->tingkat_kelas] = 0; ?>
				<?php endforeach; ?>
									
				<div id="<?php echo $w; ?>">
				<div id="datatable" url="">
					<div class="dataTables_wrapper"  role="grid">    
						<table class="styled" border=1 >
							<thead>
								<tr>
									<th rowspan=2>No.</th>
									<th rowspan=2>Mata Pelajaran</th>
									<th colspan=<?php echo $count; ?>>Jam/Minggu</th>
								</tr>
								<tr>
									<?php foreach($tingkat_kelas as $t): ?>
									<th><?php echo $t->tingkat_kelas; ?></th>
									<?php endforeach; ?>
								</tr>
							</thead>
							<tbody>
								<?php foreach($kelompok as $kel): ?>
									<!--  tampilan untuk kelas A dan B -->
									<?php if($kel->status_pilihan != 1 && $kel->id_kurikulum == $statuspil){ ?>
										<tr>
										<?php $cols = 2 + $count; ?>
											<td colspan=<?php echo $cols; ?>><b><?php echo $kel->kelompok_pelajaran?></b></td>
										</tr>
										<?php $no = 1; ?>
										<?php foreach($pelajaran_group as $pel): ?>
										<?php if($kel->id_kelompok_pelajaran == $pel->id_kelompok_pelajaran){ ?>
										<tr>
											<td width='30px' class='center'><?php echo $no; ?></td>
											<td width='350px'><?php echo $pel->pelajaran; ?></td>
											<?php foreach($alokasi_pelajaran as $p => $pj): ?>
												<?php if($kel->id_kelompok_pelajaran == $p){ ?>
													<?php foreach($pj as $np => $kl): ?>
														<?php if($pel->pelajaran == $np){ ?>
															<?php foreach($kl as $kls => $jml): ?>
																<?php $arr[$kls] = $arr[$kls] + $jml; ?>
																<td width='50px' class='center'><?php echo $jml; ?></td>
																<?php endforeach; } ?>
													<?php endforeach; } ?>
											<?php endforeach; ?>
										</tr>
										<?php $no++; } ?>
										<?php endforeach; ?>
										<!--  tampilan untuk kelas C -->
									<?php }elseif($kel->status_pilihan == 1 && $kel->id_kurikulum == $statuspil){ ?>
										<tr>
										<?php $cols = 2 + $count; ?>
											<td colspan=<?php echo $cols; ?>><b><?php echo $kel->kelompok_pelajaran?></b></td>
										</tr>
										<?php foreach($mapel_pilihan as $pem => $value_minat): ?>
											
											<?php if($pem == $pemi){ ?>
											
											<?php $no =1; foreach($value_minat as $p): ?>
												
												<tr>
													<td width='' class='center'><?php echo $no; ?></td>
													<td><?php echo $p['pelajaran']; ?></td>
													<?php foreach($alokasi_pelajaran as $kpl => $pj): ?>
														<?php if($kel->id_kelompok_pelajaran == $kpl){ ?>
															<?php foreach($pj as $np => $kl): ?>
																<?php if($p['pelajaran'] == $np){ ?>
															<?php foreach($kl as $kls => $jum): ?>
																<?php $arr[$kls] = $arr[$kls] + $jml; ?>
																<td width='50px' class='center'><?php echo $jum; ?></td>
															<?php endforeach; } ?>
															<?php endforeach; } ?>
													<?php endforeach; ?>
												</tr>
												
											<?php $no++; endforeach; } ?>
										<?php endforeach; ?>
									<?php } ?>
									
								<?php endforeach; ?>
								<?php if($kel->id_kurikulum == $statuspil){ ?>
								
								<tr>
									<td width='' colspan=2><b>Jumlah Jam Pelajaran yang Harus Ditempuh per Minggu</b></td>
									<?php foreach($tingkat_kelas as $t): ?>
										<td width='' class='center'><b><?php echo $arr[$t->tingkat_kelas]; ?></b></td>
									<?php endforeach; ?>
								</tr>
								<?php }else{?>
								<tr>
									<td width='' colspan=5><b>Data Tidak Ditemukan!</b></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						
					</div>
				</div>
				</div>
			
		</div>
	</div>
		<?php endforeach; ?>
</div>