<?php 
	$statuspil = 0;
	if(isset($_POST['id_kurikulum'])){
	$a = $_POST['id_kurikulum'];
	$statuspil = $a; } ?>
	
<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
	
	<div class="box with-table">
		
		<div class="tabletools">
			
			<div class="dataTables_filter">
				<form action="#0" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Jenis Kurikulum</span></th>
							<td width=200px align='left'>
								<select name="id_kurikulum" id="id_kurikulum" onchange="submitform();">
									<option value="0">-Pilih Kurikulum-</option>
									<?php foreach($kur as $q): ?>
										
										<?php if($q->id_kurikulum == $statuspil){ ?>
											<option selected value='<?php echo $q->id_kurikulum; ?>' data-status-pilihan="<?php echo $q->id_kurikulum; ?>"><?php echo $q->nama_kurikulum; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q->id_kurikulum; ?>' data-status-pilihan="<?php echo $q->id_kurikulum; ?>"><?php echo $q->nama_kurikulum; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>

		</div>
	</div>
	
	<?php if($statuspil != 0){ ?>
	<div class="box with-table tabbedBox">
		
		<div class="header">
			<h2>Program</h2>
			<ul>
			<?php $s=1; foreach($mapel_pilihan as $pem => $value_minat): ?>
			<?php if($value_minat['kurikulum']==$statuspil){ ?>
				<li><a href="#<?php echo $s; ?>"><?php echo $pem; ?></a></li>
			<?php } ?>
			<?php $s++; endforeach; ?>
			</ul>
		</div>
		<div class="tabletools">
			<div class="right">
				<a href="<?php echo base_url('kurikulum/kur_program/print_pdf/'.$statuspil); ?>"><i class="icon-print"></i>Cetak ke file</a> 
				<br/><br/>
			</div>
		</div>
	
		<div class="content tabbed">
			<?php $w=1; foreach($mapel_pilihan as $pemi => $val_minat): ?>
				<?php $arr = array(); ?>
				<?php foreach($tingkat_kelas as $t): ?>
					<?php $arr[$t->tingkat_kelas] = 0; ?>
				<?php endforeach; ?>
									
				<div id="<?php echo $w; ?>">
				<div id="datatable" url="">
					<div class="dataTables_wrapper"  role="grid">    
						<table class="styled" >
							<thead>
								<tr>
									<th rowspan=2>No.</th>
									<th rowspan=2>Mata Pelajaran</th>
									<th colspan=<?php echo $count; ?>>Jam/Minggu</th>
								</tr>
								<tr>
									<?php foreach($tingkat_kelas as $t): ?>
									<th><?php echo $t->tingkat_kelas; ?></th>
									<?php endforeach; ?>
								</tr>
							</thead>
							<tbody>
								<?php $id_kel=0; foreach($kelompok as $kel): ?>
									<!--  tampilan untuk kelas A dan B -->
									<?php if($kel->status_pilihan != 1 && $kel->id_kurikulum == $statuspil){ ?>
										<tr>
										<?php $id_kel = $kel->id_kurikulum; $cols = 2 + $count; ?>
											<td colspan=<?php echo $cols; ?>><b><?php echo $kel->kelompok_pelajaran?></b></td>
										</tr>
										<?php $no = 1; ?>
										<?php foreach($pelajaran_group as $pel): ?>
										<?php if($kel->id_kelompok_pelajaran == $pel->id_kelompok_pelajaran){ ?>
										<tr>
											<td width='30px' class='center'><?php echo $no; ?></td>
											<td width='350px'><?php echo $pel->pelajaran; ?></td>
											<?php foreach($alokasi_pelajaran as $p => $pj): ?>
												<?php if($kel->id_kelompok_pelajaran == $p){ ?>
													<?php foreach($pj as $np => $kl): ?>
														<?php if($pel->pelajaran == $np){ ?>
															<?php foreach($kl as $kls => $jml): ?>
																<?php $arr[$kls] = $arr[$kls] + $jml; ?>
																<td width='50px' class='center'><?php echo $jml; ?></td>
																<?php endforeach; } ?>
													<?php endforeach; } ?>
											<?php endforeach; ?>
										</tr>
										<?php $no++; } ?>
										<?php endforeach; ?>
										<!--  tampilan untuk kelas C -->
									<?php }elseif($kel->status_pilihan == 1 && $kel->id_kurikulum == $statuspil){ ?>
										<tr>
										<?php $cols = 2 + $count; ?>
											<td colspan=<?php echo $cols; ?>><b><?php echo $kel->kelompok_pelajaran?></b></td>
										</tr>
										<?php foreach($mapel_pilihan as $pem => $value_minat): ?>
											
											<?php if($pem == $pemi){ ?>
											
											<?php $no =1; foreach($value_minat['pel'] as $p): ?>
												
												<tr>
													<td width='' class='center'><?php echo $no; ?></td>
													<td><?php echo $p['pelajaran']; ?></td>
													<?php foreach($alokasi_pelajaran as $kpl => $pj): ?>
														<?php if($kel->id_kelompok_pelajaran == $kpl){ ?>
															<?php foreach($pj as $np => $kl): ?>
																<?php if($p['pelajaran'] == $np){ ?>
															<?php foreach($kl as $kls => $jum): ?>
																<?php $arr[$kls] = $arr[$kls] + $jum; ?>
																<td width='50px' class='center'><?php echo $jum; ?></td>
															<?php endforeach; } ?>
															<?php endforeach; } ?>
													<?php endforeach; ?>
												</tr>
												
											<?php $no++; endforeach; } ?>
										<?php endforeach; ?>
									<?php } ?>
									
								<?php endforeach; ?>
								<?php if($id_kel == $statuspil){ ?>
								
								<tr>
									<td width='' colspan=2><b>Jumlah Jam Pelajaran yang Harus Ditempuh per Minggu</b></td>
									<?php foreach($tingkat_kelas as $t): ?>
										<td width='' class='center'><b><?php echo $arr[$t->tingkat_kelas]; ?></b></td>
									<?php endforeach; ?>
								</tr>
								<?php }else{?>
								<tr>
									<td width='' colspan=5><b>Data Tidak Ditemukan!</b></td>
								</tr>
								<?php } ?>
							</tbody>
						</table>
						
					</div>
				</div>
				</div>
			<?php $w++; endforeach; ?>
		</div>
	</div>
</div>

	<?php }else{ ?>
		<div class="box with-table">
		 <div id="datatable" url="">
			<div class="dataTables_wrapper"  role="grid">
				<div class="content">
					<div class="footer">
						<div class="dataTables_info">Pilih Kurikulum Yang Anda Cari</div>
					</div>
				</div>
			</div>
		 </div>
		</div>
	<?php } ?>

<script>
	
	function submitform()
	{
	  document.myform.submit();
	}
</script>