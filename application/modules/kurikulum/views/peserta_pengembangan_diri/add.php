	<h1 class="grid_12">Pengembangan Diri</h1>
			
			<form action="<?php echo site_url("kurikulum/peserta_pengembangan_diri/insertData"); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>NIM</strong>
						</label>
						<div>
							<input type="text" name="nim" value="" id="nim" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>NAMA</strong>
						</label>
						<div>
							<input disabled type="text" value="" id="nama" />
							<input type="hidden" value="" id="id_siswa" name="id_siswa"/>
						</div>
					</div>
					<div class="row">
						<label for="f2_select2">
							<strong>Pelajaran</strong>
						</label>
							<?php 
							$this->load->model('m_peserta_pengembangan_diri');
							$data['listdropdown'] = $this->m_peserta_pengembangan_diri->getList();
							$hasil=$data['listdropdown'];
							?>
						<div>
						
							 <select name="id_ekstra_kurikuler">
							 <?php foreach($hasil as $h) {?>
							  <option value="<?php echo $h->id_ekstra_kurikuler ?>"><?php echo $h->nama_ekstra_kurikuler ?></option>
							 <?php }?>
							 </select>
						
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Status</strong>
						
						</label>
						<div>
							 <select name="status">
							  <option value="1">Aktif</option>
							  <option value="2">Tidak Aktif</option>
							</select>
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/silabus/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
<script>
$(document).ready(function(){
  $("#nim").on("keyup",function(){
    $.get("<?php echo site_url("kurikulum/peserta_pengembangan_diri/cekData")?>/" + $(this).val(),function(data,status){
      var a = JSON.parse(data);
	  if(a){
		console.log(a);
		$("#nama").val(a.nama);
		$("#id_siswa").val(a.id_siswa);
	  }
	  
    });
  });
});
</script>