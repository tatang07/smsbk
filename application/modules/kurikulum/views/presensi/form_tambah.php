	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/rpp/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Rencana Pelaksanaan Pembelajaran</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama RPP</strong>
						</label>
						<div>
							<input type="text" name="nama_rpp" value="" />
							<input type="hidden" name="action" value="add" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Pertemuan Ke</strong>
						</label>
						<div>
							<input type="text" name="pertemuan_ke" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Guru</strong>
						</label>
						<div>
							<select name="id_guru" id="guru" data-placeholder="Choose a Name">
								<option>-- Pilih Guru --</option>
							<?php foreach($guru as $t): ?>
								<option value="<?php echo $t->id_guru; ?>"><?php echo $t->nama; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Mata Pelajaran</strong>
						</label>
						<div>
							<select name="id_pelajaran" class="chosen-select" id="pelajaran" data-placeholder="Choose a Name">
							<option>-- Pilih Pelajaran --</option>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Rombel</strong>
						</label>
						<div>
							<select name="id_rombel"  class="chosen-select" id="rombel" data-placeholder="Choose a Name">
							<option>-- Pilih Kelas --</option>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Term</strong>
						</label>
						<div>
							<select name="id_term" data-placeholder="Choose a Name">
							<?php foreach($term as $k): ?>
								<option value="<?php echo $k->id_term; ?>"><?php echo $k->term; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>					
					
					<div class="row">
						<label for="f1_textarea">
							<strong>File</strong>
						</label>
						<div>
							<input type="file" name="userfile" value="" />
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/rpp/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		
<script>
$(function(){
	$(document).on('change', '#guru', function(){
		var id_guru = $(this).val();
		$.post("<?php echo site_url('kurikulum/rpp/get_mapel_guru'); ?>/" + id_guru, function(data){
			$("#pelajaran").html(data);
			$("#pelajaran").trigger("chosen:updated");
		});
	});

	$(document).on('change', '#pelajaran', function(){
		var id_pelajaran = $(this).val();
		$.post("<?php echo site_url('kurikulum/rpp/get_mapel_rombel'); ?>/" + id_pelajaran, function(data){
			$("#rombel").html(data);
			$("#rombel").trigger("chosen:updated");
		});
	});
});
</script>