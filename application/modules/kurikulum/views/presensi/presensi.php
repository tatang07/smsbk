<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Presensi</h2>
		</div>
		<div class="tabletools">
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('kurikulum/presensi/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=50px align='left'><span class="text">Kelas</span></th>
							<td width=200px align='left'>
								<select name="rombel">
								<?php foreach($rombel as $r): ?>
									<?php if($r->id_rombel == $first_rombel ){ ?>
										<option selected value='<?php echo $r->id_rombel; ?>'><?php echo $r->rombel; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $r->id_rombel; ?>'><?php echo $r->rombel; ?></option>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<th width=100px align='left'><span class="text">Mata Pelajaran</span></th>
							<td width=200px align='left'>
								<select name="pelajaran">
								<?php foreach($pelajaran as $p): ?>
									<?php if($p->id_pelajaran == $first_pelajaran ){ ?>
										<option selected value='<?php echo $p->id_pelajaran; ?>'><?php echo $p->pelajaran; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $p->id_pelajaran; ?>'><?php echo $p->pelajaran; ?></option>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
							<input type="submit" name=send class="button grey tooltip" value="cari" />
							<!-- <a href="search" class="button grey tooltip"><i class='icon-search'></i></a> -->
							</td>
						</tr>
					</table>
				</form>
			</div>

		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<?php foreach($jenis_absen as $j):?>
									<th><?php echo $j; ?></th>
								<?php endforeach;?>
							</tr>
						</thead>
						<tbody>
						
						<?php if($stat == 'search'){ ?>
							<?php $no = 1; ?>
							<?php if($rekap_absen){ ?>
								<?php foreach($rekap_absen as $nis=>$jml): ?>
									<tr>
										<td width="50px" class="center"><?php echo $no; ?></td>
										<td width="300px" class="center"><?php echo $nis; ?></td>
										<td width="300px" class="center"><?php echo $rekap_absen[$nis]['nama']; ?></td>
										<?php foreach($jenis_absen as $id=>$val):?>
											<td width="100px" class="center"><?php echo isset($rekap_absen[$nis]['jml'][$id])? $rekap_absen[$nis]['jml'][$id]:0; ?></td>
										<?php endforeach;?>
									</tr>
								<?php endforeach; ?>
							
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $no; ?></div>
						
					</div>
							<?php }else{ ?>
								</tbody>
								</table>
								<div class="footer">Data tidak ditemukan!
								</div>
							<?php } ?>
						<?php }else{ ?>
							</tbody>
								</table>
								<div class="footer">Pilih Kelas dan Mata Pelajaran yang Ingin di Cari!
								</div>
						<?php } ?>
				</div>
			</div>
		</div>        
	</div>
</div>