	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="<?php echo base_url('kurikulum/rpp_tematik/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Rencana Pelaksanaan Pembelajaran</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama RPP Tematik</strong>
						</label>
						<div>
							<input type="text" name="nama_rpp_tematik" value="" />
							<input type="hidden" name="action" value="add" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Pertemuan Ke</strong>
						</label>
						<div>
							<input type="text" name="pertemuan_ke" value="" />
						</div>
					</div>
					
					<input type="hidden" name="term" value="<?php echo get_id_term(); ?>">
					
					<div class="row">
						<label for="f2_select2">
							<strong>Pelajaran Tematik</strong>
						</label>
						<div>
							<select name="tematik" class="chosen-select" id="tematik" data-placeholder="-- Pilih Pelajaran Tematik --">
							<option>-- Pilih Pelajaran Tematik --</option>
							<?php foreach($tematik as $t): ?>
								<option value="<?php echo $t->id_pelajaran_tematik; ?>"><?php echo $t->pelajaran_tematik; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>File</strong>
						</label>
						<div>
							<input type="file" name="userfile" value="" />
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kurikulum/rpp_tematik/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
	