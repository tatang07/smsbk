<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
	<div class="box with-table">
		<div class="header">
			<h2>Dokumen Kurikulum</h2>
		</div>
		<div class="tabletools">
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('kurikulum/dokumen_kurikulum/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="40px" align="left"><span class="text">Nama</span></th>
								<td align="left"><input type="text" name="nama" id="nama"></td>
								<td width="10px"></td>
								<th width="110px" align="left"><span class="text">Dokumen Kurikulum</span></th>
								<td width="200px" align="left">
									<select id="dokumen" name="dokumen" class="chzn-done" style="display: none;">
										<option value="0">-- Pilih Dokumen --</option>
										<option <?php if($dokumen=='silabus'){echo 'selected';} ?> value="silabus">Silabus</option>
										<option <?php if($dokumen=='rpp'){echo 'selected';} ?> value="rpp">RPP</option>
										<option <?php if($dokumen=='lainnya'){echo 'selected';} ?> value="lainnya">Lainnya</option>
									</select>
								</td>
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>
		</div>
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No</th>
								<th>Nama Dokumen</th>
								<th>Jenis Dokumen</th>
								<th>Mata Pelajaran</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1; ?>
							<?php if($nama=="" && $dokumen==""){ ?>
							<?php foreach ($alldata as $data): ?>
								<tr>
									<td width='' class='center'><?php echo $no; ?></td>
									<td width='' class='left'><?php echo $data->nama_doc; ?></td>
									<td width='' class='center'><?php echo $data->jenis_dokumen; ?></td>
									<td width='' class='center'><?php echo $data->kode_pelajaran .'-'. $data->pelajaran; ?></td>
									<td width='' class='center'>
										<a href="<?php echo base_url('kurikulum/dokumen_kurikulum/download/'.$data->id.'/'.$data->jenis_dokumen); ?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-print"></i> Download</a>
										<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
											<a href="<?php echo base_url('kurikulum/dokumen_kurikulum/delete/'.$data->id.'/'.$data->jenis_dokumen); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
										<?php } ?>
									</td>
								</tr>
							<?php $no++; ?>
							<?php endforeach; }else{ ?>
								<?php foreach ($alldata as $data): ?>
									<?php if($data->nama_doc == $nama || $data->jenis_dokumen == $dokumen){ ?>
								<tr>
									<td width='' class='center'><?php echo $no; ?></td>
									<td width='' class='left'><?php echo $data->nama_doc; ?></td>
									<td width='' class='center'><?php echo $data->jenis_dokumen; ?></td>
									<td width='' class='center'><?php echo $data->kode_pelajaran .'-'. $data->pelajaran; ?></td>
									<td width='' class='center'>
										<a href="<?php echo base_url('kurikulum/dokumen_kurikulum/download/'.$data->id.'/'.$data->jenis_dokumen); ?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-print"></i> Download</a>
										<a href="<?php echo base_url('kurikulum/dokumen_kurikulum/delete/'.$data->id.'/'.$data->jenis_dokumen); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php $no++; ?>
							<?php } endforeach; ?>
							<?php } ?>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $jumlah; ?></div>
					</div>
				</div>
			</div>
		</div>        
	</div>
</div>
