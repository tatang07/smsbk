<?php
class m_sarana_prasarana extends MY_Model {
    public $table = "m_sarana_prasarana";
    public $id = "id_sarana_prasarana";
	
	//join key
	public $join_to = array(
							"r_kondisi k"=>"k.id_kondisi=m_sarana_prasarana.id_kondisi",
					  );
	
	//join field
	public $join_fields = array(
							"k.kondisi kondisi",
						  );
}