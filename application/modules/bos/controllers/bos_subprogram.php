<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class bos_subprogram extends CI_Controller {

	public function index()
	{	
		$this->load->model('bos/m_bos_subprogram');
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();
		
		$data['isi'] = $this->m_bos_subprogram->get_data($id_sekolah,$id_tahun_ajaran);
		$data['program'] = $this->m_bos_subprogram->get_program($id_sekolah,$id_tahun_ajaran);
		$data['component']="Pembiayaan";
		render('bos_subprogram/bos_subprogram',$data);
	}
	public function load_add()
	{	
		$this->load->model('bos/m_bos_subprogram');
		$id_sekolah= get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();
		$data['program'] = $this->m_bos_subprogram->get_program($id_sekolah,$id_tahun_ajaran);
		// $data['guru'] = $this->m_bos_subprogram->select_guru($id_sekolah);
		$data['component']="pembiayaan";
		render('bos_subprogram/form_tambah',$data);
	}

	public function load_edit($id_bos_subprogram)
	{	
		$this->load->model('bos/m_bos_subprogram');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_bos_subprogram->get_data_edit($id_bos_subprogram);
	
		$data['component']="pembiayaan";
		render('bos_subprogram/form_edit',$data);
	}

	public function delete($id_bos_subprogram){
			$this->load->model('bos/m_bos_subprogram');
			$this->m_bos_subprogram->delete($id_bos_subprogram);
			redirect(site_url('bos/bos_subprogram/index/')) ;
	}

	public function submit_add(){
			$this->load->model('bos/m_bos_subprogram');
			$data['id_bos_program'] = $this->input->post('id_bos_program');
			$data['subprogram'] = $this->input->post('subprogram');
			$data['kode_subprogram'] = $this->input->post('kode_subprogram');
			
		
			$this->m_bos_subprogram->add($data);
			redirect(site_url('bos/bos_subprogram/index/')) ;
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('bos/m_bos_subprogram');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_bos_subprogram->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_bos_subprogram->select_kelas($jenjang_sekolah);
			render('bos_subprogram/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('bos/m_bos_subprogram');
			$id=$this->input->post('id');
			// $data['id_bos_program'] = $this->input->post('id_bos_program');
			$data['subprogram'] = $this->input->post('subprogram');
			$data['kode_subprogram'] = $this->input->post('kode_subprogram');
			$this->m_bos_subprogram->edit($data,$id);
			redirect(site_url('bos/bos_subprogram/index/')) ;
	}
	public function search(){
		$this->load->model('bos/m_bos_subprogram');
		// $nama = $this->input->post('nama');
		$id_program = $this->input->post('program');
		
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();
		
		$data['isi'] = $this->m_bos_subprogram->get_data_search($id_program,$id_sekolah,$id_tahun_ajaran);
		$data['program'] = $this->m_bos_subprogram->get_program($id_sekolah,$id_tahun_ajaran);
		$data['component']="pembiayaan";
		render('bos_subprogram/bos_subprogram',$data);
	}
	
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */