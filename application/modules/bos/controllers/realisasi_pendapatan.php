<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class realisasi_pendapatan extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('bos/m_realisasi_pendapatan');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('bos/realisasi_pendapatan/home');
		}
		
		public function home(){
			
			$data['list'] = $this->m_realisasi_pendapatan->get_datalist();
			$data['jumlah'] = $this->m_realisasi_pendapatan->count_datalist();
			$data['sumber_dana'] = $this->m_realisasi_pendapatan->get_sumber_dana();
			
			$data['component'] = 'Pembiayaan';
			render("realisasi_pendapatan/realisasi_pendapatan", $data);
		}
		
		public function tambah(){
			$data['sumber_dana'] = $this->m_realisasi_pendapatan->get_sumber_dana();
			$data['id'] = '';
			$data['id_sumber_dana'] = '';
			$data['tgl'] = '';
			$data['no_bukti'] = '';
			$data['uraian'] = '';
			$data['pajak'] = '';
			$data['jml_setelah_pajak'] = '';
			$data['ket'] = '';
			
			$data['action'] = 'add';
			$data['component'] = 'Pembiayaan';
			render("realisasi_pendapatan/form_tambah", $data);
		}
		
		public function edit($id){
			$row = $this->m_realisasi_pendapatan->get_realisasi_pendapatan_by($id);
			$data['sumber_dana'] = $this->m_realisasi_pendapatan->get_sumber_dana();
			$data['id'] = $id;
			$data['id_sumber_dana'] = $row->id_bos_sumber_dana;
			$data['tgl'] = tanggal_view($row->tanggal_transaksi);
			$data['no_bukti'] = $row->nomor_bukti;
			$data['uraian'] = $row->uraian;
			$data['pajak'] = $row->pajak;
			$data['jml_setelah_pajak'] = $row->jumlah_setelah_pajak;
			$data['ket'] = $row->keterangan;
			
			$data['action'] = 'edit';
			$data['component'] = 'Pembiayaan';
			render("realisasi_pendapatan/form_tambah", $data);
		}
		
		public function submit(){
			$this->form_validation->set_rules('id_sumber_dana', 'Sumber Dana', 'required');
			$this->form_validation->set_rules('tgl', 'Tanggal', 'required');
			$this->form_validation->set_rules('no_bukti', 'Nomor Bukti', 'required');
			$this->form_validation->set_rules('uraian', 'Uraian', 'required');
			$this->form_validation->set_rules('pajak', 'Pajak', 'required');
			$this->form_validation->set_rules('jml_setelah_pajak', 'Jumlah Setelah Pajak', 'required');
			$this->form_validation->set_rules('ket', 'Keterangan', 'required');
			
			if($this->form_validation->run() == TRUE){
				$id = $this->input->post('id');
				$id_sumber_dana = $this->input->post('id_sumber_dana');
				$tgl = tanggal_db($this->input->post('tgl'));
				$no_bukti = $this->input->post('no_bukti');
				$uraian = $this->input->post('uraian');
				$pajak = $this->input->post('pajak');
				$jml_setelah_pajak = $this->input->post('jml_setelah_pajak');
				$ket = $this->input->post('ket');
				
				$action = $this->input->post('action');
				if($action == 'add'){
					$this->m_realisasi_pendapatan->insert($id_sumber_dana, $tgl, $no_bukti, $uraian, $pajak, $jml_setelah_pajak, $ket);
					set_success_message('Data Berhasil Ditambah!');
					redirect("bos/realisasi_pendapatan/home/");
				}else{
					$this->m_realisasi_pendapatan->update($id, $id_sumber_dana, $tgl, $no_bukti, $uraian, $pajak, $jml_setelah_pajak, $ket);
					set_success_message('Data Berhasil Diupdate!');
					redirect("bos/realisasi_pendapatan/home/");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian!');
				redirect("bos/realisasi_pendapatan/home/");
			}
		}
		
		public function delete(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('bos/realisasi_pendapatan/home/');
			}
			
			$this->m_realisasi_pendapatan->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('bos/realisasi_pendapatan/home/');
		}
			
}
