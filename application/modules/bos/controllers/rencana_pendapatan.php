<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class rencana_pendapatan extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('bos/m_rencana_pendapatan');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('bos/rencana_pendapatan/home');
		}
		
		public function home(){
			
			$data['list'] = $this->m_rencana_pendapatan->get_datalist();
			$data['jumlah'] = $this->m_rencana_pendapatan->count_datalist();
			
			$data['component'] = 'Pembiayaan';
			render("rencana_pendapatan/rencana_pendapatan", $data);
		}
		
		public function tambah(){
			$data['sumber_dana'] = $this->m_rencana_pendapatan->get_sumber_dana();
			$data['id'] = '';
			$data['p1'] = '';
			$data['p2'] = '';
			$data['p3'] = '';
			$data['p4'] = '';
			$data['ket'] = '';
			$data['id_sumber_dana'] = '';
			
			$data['action'] = 'add';
			$data['component'] = 'Pembiayaan';
			render("rencana_pendapatan/form_tambah", $data);
		}
		
		public function edit($id){
			$row = $this->m_rencana_pendapatan->get_rencana_pendapatan_by($id);
			$data['sumber_dana'] = $this->m_rencana_pendapatan->get_sumber_dana();
			$data['id'] = $id;
			$data['p1'] = $row->periode_1;
			$data['p2'] = $row->periode_2;
			$data['p3'] = $row->periode_3;
			$data['p4'] = $row->periode_4;
			$data['ket'] = $row->keterangan;
			$data['id_sumber_dana'] = $row->id_bos_sumber_dana;
			
			$data['action'] = 'edit';
			$data['component'] = 'Pembiayaan';
			render("rencana_pendapatan/form_tambah", $data);
		}
		
		public function submit(){
			$this->form_validation->set_rules('id_sumber_dana', 'Sumber Dana', 'required');
			$this->form_validation->set_rules('p1', 'Periode 1', 'required');
			$this->form_validation->set_rules('p2', 'Periode 2', 'required');
			$this->form_validation->set_rules('p3', 'Periode 3', 'required');
			$this->form_validation->set_rules('p4', 'Periode 4', 'required');
			$this->form_validation->set_rules('ket', 'Keterangan', 'required');
			
			if($this->form_validation->run() == TRUE){
				$id = $this->input->post('id');
				$p1 = $this->input->post('p1');
				$p2 = $this->input->post('p2');
				$p3 = $this->input->post('p3');
				$p4 = $this->input->post('p4');
				$ket = $this->input->post('ket');
				$id_sumber_dana = $this->input->post('id_sumber_dana');
				
				$action = $this->input->post('action');
				if($action == 'add'){
					$this->m_rencana_pendapatan->insert($id_sumber_dana, $p1, $p2, $p3, $p4, $ket);
					set_success_message('Data Berhasil Ditambah!');
					redirect("bos/rencana_pendapatan/home/");
				}else{
					$this->m_rencana_pendapatan->update($id, $id_sumber_dana, $p1, $p2, $p3, $p4, $ket);
					set_success_message('Data Berhasil Diupdate!');
					redirect("bos/rencana_pendapatan/home/");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian!');
				redirect("bos/rencana_pendapatan/home/");
			}
		}
		
		public function delete(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('bos/rencana_pendapatan/home/');
			}
			
			$this->m_rencana_pendapatan->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('bos/rencana_pendapatan/home/');
		}
			
}
