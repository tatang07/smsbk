<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class realisasi_pengeluaran extends Simple_Controller {
	function index($id_bos_program = false, $id_bos_subprogram = false, $id_bos_kegiatan = false, $id_bos_sumber_dana = false){
		$this->load->model('bos/m_realisasi_pengeluaran');
		$id_sekolah=get_id_sekolah();
		$data['program'] = array('- Pilih Program -');
		$program = $this->m_realisasi_pengeluaran->get_program($id_sekolah)->result_array();
		if($program){
			foreach ($program as $pro) {
				$data['program'][$pro['id_bos_program']] = $pro['program'];
			}
		}

		$data['subprogram'] = array('- Pilih Sub Program -');
		if($id_bos_program){
			$subprogram = $this->m_realisasi_pengeluaran->get_sub_program($id_bos_program)->result_array();
			if($subprogram){
				foreach ($subprogram as $spro) {
					$data['subprogram'][$spro['id_bos_subprogram']] = $spro['subprogram'];
				}
			}
		}

		$data['kegiatan'] = array('- Pilih Kegiatan -');
		if($id_bos_subprogram){
			$kegiatan = $this->m_realisasi_pengeluaran->get_kegiatan($id_bos_subprogram)->result_array();
			if($kegiatan){
				foreach ($kegiatan as $keg) {
					$data['kegiatan'][$keg['id_bos_kegiatan']] = $keg['kode_kegiatan'];
				}
			}
		}

		$data['sumber_dana'] = array('- Pilih Kegiatan -');
		$sumber_dana = $this->m_realisasi_pengeluaran->get_sumber_dana()->result_array();	
		if($sumber_dana){
			foreach ($sumber_dana as $sd) {
				$data['sumber_dana'][$sd['id_bos_sumber_dana']] = $sd['bos_sumber_dana'];
			}
		}

		$data['id_bos_program'] = $id_bos_program;
		$data['id_bos_subprogram'] = $id_bos_subprogram;
		$data['id_bos_kegiatan'] = $id_bos_kegiatan;
		$data['id_bos_sumber_dana'] = $id_bos_sumber_dana;
		$data['component']="Pembiayaan";
		render('realisasi_pengeluaran/datalist', $data);
	}	

	function tampil(){
		$this->load->model('bos/m_realisasi_pengeluaran');

		$id_bos_program = $this->input->post('id_bos_program');
		$id_bos_subprogram = $this->input->post('id_bos_subprogram');
		$id_bos_kegiatan = $this->input->post('id_bos_kegiatan');
		$id_bos_sumber_dana = $this->input->post('id_bos_sumber_dana');
		$id_sekolah=get_id_sekolah();
		$data['program'] = array('- Pilih Program -');
		$program = $this->m_realisasi_pengeluaran->get_program($id_sekolah)->result_array();
		if($program){
			foreach ($program as $pro) {
				$data['program'][$pro['id_bos_program']] = $pro['program'];
			}
		}

		$data['subprogram'] = array('- Pilih Sub Program -');
		if($id_bos_program){
			$subprogram = $this->m_realisasi_pengeluaran->get_sub_program($id_bos_program)->result_array();
			if($subprogram){
				foreach ($subprogram as $spro) {
					$data['subprogram'][$spro['id_bos_subprogram']] = $spro['subprogram'];
				}
			}
		}

		$data['kegiatan'] = array('- Pilih Kegiatan -');
		if($id_bos_subprogram){
			$kegiatan = $this->m_realisasi_pengeluaran->get_kegiatan($id_bos_subprogram)->result_array();
			if($kegiatan){
				foreach ($kegiatan as $keg) {
					$data['kegiatan'][$keg['id_bos_kegiatan']] = $keg['kode_kegiatan'];
				}
			}
		}

		$data['sumber_dana'] = array('- Pilih Kegiatan -');
		$sumber_dana = $this->m_realisasi_pengeluaran->get_sumber_dana()->result_array();	
		if($sumber_dana){
			foreach ($sumber_dana as $sd) {
				$data['sumber_dana'][$sd['id_bos_sumber_dana']] = $sd['bos_sumber_dana'];
			}
		}

		$data['id_bos_program'] = $id_bos_program;
		$data['id_bos_subprogram'] = $id_bos_subprogram;
		$data['id_bos_kegiatan'] = $id_bos_kegiatan;
		$data['id_bos_sumber_dana'] = $id_bos_sumber_dana;

		$data['realisasi'] = $this->m_realisasi_pengeluaran->get_data($id_bos_kegiatan, $id_bos_sumber_dana)->result_array();
		
		render('realisasi_pengeluaran/datalist', $data);
	}	

	function tambah($id_bos_program = false, $id_bos_subprogram = false, $id_bos_kegiatan = false, $id_bos_sumber_dana = false){
		$this->load->model('bos/m_realisasi_pengeluaran');
		$id_sekolah=get_id_sekolah();
		$data['program'] = array('- Pilih Program -');
		$program = $this->m_realisasi_pengeluaran->get_program($id_sekolah)->result_array();
		if($program){
			foreach ($program as $pro) {
				$data['program'][$pro['id_bos_program']] = $pro['program'];
			}
		}

		$data['subprogram'] = array('- Pilih Sub Program -');
		if($id_bos_program){
			$subprogram = $this->m_realisasi_pengeluaran->get_sub_program($id_bos_program)->result_array();
			if($subprogram){
				foreach ($subprogram as $spro) {
					$data['subprogram'][$spro['id_bos_subprogram']] = $spro['subprogram'];
				}
			}
		}

		$data['kegiatan'] = array('- Pilih Kegiatan -');
		if($id_bos_subprogram){
			$kegiatan = $this->m_realisasi_pengeluaran->get_kegiatan($id_bos_subprogram)->result_array();
			if($kegiatan){
				foreach ($kegiatan as $keg) {
					$data['kegiatan'][$keg['id_bos_kegiatan']] = $keg['kode_kegiatan'];
				}
			}
		}

		$data['sumber_dana'] = array('- Pilih Sumber Dana -');
		$sumber_dana = $this->m_realisasi_pengeluaran->get_sumber_dana()->result_array();	
		if($sumber_dana){
			foreach ($sumber_dana as $sd) {
				$data['sumber_dana'][$sd['id_bos_sumber_dana']] = $sd['bos_sumber_dana'];
			}
		}

		$data['id_bos_program'] = $id_bos_program;
		$data['id_bos_subprogram'] = $id_bos_subprogram;
		$data['id_bos_kegiatan'] = $id_bos_kegiatan;
		$data['id_bos_sumber_dana'] = $id_bos_sumber_dana;

		render('realisasi_pengeluaran/form_tambah', $data);
	}

	function submit_add(){
		$this->load->model('bos/m_realisasi_pengeluaran');
		$data['id_bos_kegiatan'] = $this->input->post('id_bos_kegiatan');
		$data['id_bos_sumber_dana'] = $this->input->post('id_bos_sumber_dana');
		$data['tgl_transaksi'] = $this->input->post('tgl_transaksi');
		$data['nomor_bukti'] = $this->input->post('nomor_bukti');
		$data['uraian'] = $this->input->post('uraian');
		$data['pajak'] = $this->input->post('pajak');
		$data['jumlah_setelah_pajak'] = $this->input->post('jumlah_setelah_pajak');

		$this->m_realisasi_pengeluaran->submit_add($data);
		redirect('bos/realisasi_pengeluaran');
	}

	function load_edit($id_bos_realisasi_pengeluaran ,$id_bos_program, $id_bos_subprogram, $id_bos_sumber_dana){
		$this->load->model('bos/m_realisasi_pengeluaran');
		$id_sekolah=get_id_sekolah;
		$data['program'] = $this->m_realisasi_pengeluaran->get_program($id_sekolah)->result_array();
		$data['subprogram'] = $this->m_realisasi_pengeluaran->get_sub_program($id_bos_program)->result_array();
		$data['kegiatan'] = $this->m_realisasi_pengeluaran->get_kegiatan($id_bos_subprogram)->result_array();
		$data['sumber_dana'] = $this->m_realisasi_pengeluaran->get_sumber_dana()->result_array();	
		$data['data_edit'] = $this->m_realisasi_pengeluaran->get_data_by_id($id_bos_realisasi_pengeluaran)->result_array();
		render('realisasi_pengeluaran/form_edit', $data);
	}

	function submit_edit(){
		$this->load->model('bos/m_realisasi_pengeluaran');
		$id_bos_realisasi_pengeluaran = $this->input->post('id_bos_realisasi_pengeluaran');

		$data['tgl_transaksi'] = $this->input->post('tgl_transaksi');
		$data['nomor_bukti'] = $this->input->post('nomor_bukti');
		$data['uraian'] = $this->input->post('uraian');
		$data['pajak'] = $this->input->post('pajak');
		$data['jumlah_setelah_pajak'] = $this->input->post('jumlah_setelah_pajak');

		$this->m_realisasi_pengeluaran->submit_edit($id_bos_realisasi_pengeluaran, $data);
		redirect('bos/rencana_pengeluaran');
	}

	function delete($id_bos_realisasi_pengeluaran){
		$this->load->model('bos/m_realisasi_pengeluaran');
		$this->m_realisasi_pengeluaran->delete($id_bos_realisasi_pengeluaran);

		redirect('bos/realisasi_pengeluaran');
	}

}
