<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class bos_program extends CI_Controller {

	public function index()
	{	
		$this->load->model('bos/m_bos_program');
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();
		
		$data['isi'] = $this->m_bos_program->get_data($id_sekolah,$id_tahun_ajaran);
		$data['component']="Pembiayaan";
		
		render('bos_program/bos_program',$data);
	}
	public function load_add()
	{	
		$this->load->model('bos/m_bos_program');
		$id_sekolah= get_id_sekolah();
		// $jenjang_sekolah = get_jenjang_sekolah();
		// $data['guru'] = $this->m_bos_program->select_guru($id_sekolah);
		$data['component']="pembiayaan";
		render('bos_program/form_tambah',$data);
	}

	public function load_edit($id_bos_program)
	{	
		$this->load->model('bos/m_bos_program');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_bos_program->get_data_edit($id_bos_program);
	
		$data['component']="pembiayaan";
		render('bos_program/form_edit',$data);
	}

	public function delete($id_bos_program){
			$this->load->model('bos/m_bos_program');
			$this->m_bos_program->delete($id_bos_program);
			redirect(site_url('bos/bos_program/index/')) ;
	}

	public function submit_add(){
			$this->load->model('bos/m_bos_program');
			$data['kode_program'] = $this->input->post('kode_program');
			$data['program'] = $this->input->post('program');
			$data['id_sekolah'] = get_id_sekolah();
			$data['id_tahun_ajaran'] = get_id_tahun_ajaran();
		
		
			$this->m_bos_program->add($data);
			redirect(site_url('bos/bos_program/index/')) ;
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('bos/m_bos_program');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_bos_program->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_bos_program->select_kelas($jenjang_sekolah);
			render('bos_program/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('bos/m_bos_program');
			$id=$this->input->post('id');
			$data['program'] = $this->input->post('program');
			$data['kode_program'] = $this->input->post('kode_program');
			// $data['tempat'] = $this->input->post('tempat');
			// $data['tanggal_pelaksanaan'] = tanggal_db($this->input->post('tanggal_pelaksanaan'));
			// echo $this->input->post('id_guru');
			// echo $data['id_guru'];
		// print_r ($data);


			$this->m_bos_program->edit($data,$id);
			redirect(site_url('bos/bos_program/index/')) ;
	}
	public function search(){
		$this->load->model('bos/m_bos_program');
		$nama = $this->input->post('nama');
		// $status = $this->input->post('status');
		
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();
		$data['isi'] = $this->m_bos_program->get_data_search($nama,$id_sekolah,$id_tahun_ajaran);
	
		$data['component']="pembiayaan";
		render('bos_program/bos_program',$data);
	}
	
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */