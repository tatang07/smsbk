<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class sumber_dana extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('bos/m_sumber_dana');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('bos/sumber_dana/home');
		}
		
		public function home(){
			
			$data['list'] = $this->m_sumber_dana->get_datalist();
			$data['jumlah'] = $this->m_sumber_dana->count_datalist();
			
			$data['component'] = 'Pembiayaan';
			render("sumber_dana/sumber_dana", $data);
		}
		
		public function tambah(){
			$data['sumber_dana'] = $this->m_sumber_dana->get_sumber_dana();
			$data['id'] = '';
			$data['kode'] = '';
			$data['nama'] = '';
			$data['id_parent'] = '';
			
			$data['action'] = 'add';
			$data['component'] = 'Pembiayaan';
			render("sumber_dana/form_tambah", $data);
		}
		
		public function edit($id){
			$row = $this->m_sumber_dana->get_sumber_dana_by($id);
			$data['sumber_dana'] = $this->m_sumber_dana->get_sumber_dana();
			$data['id'] = $id;
			$data['kode'] = $row->kode_bos_sumber_dana;
			$data['nama'] = $row->bos_sumber_dana;
			$data['id_parent'] = $row->id_parent;
			
			$data['action'] = 'edit';
			$data['component'] = 'Pembiayaan';
			render("sumber_dana/form_tambah", $data);
		}
		
		public function submit(){
			$this->form_validation->set_rules('nama', 'Nama', 'required');
			$this->form_validation->set_rules('kode', 'Kode', 'required');
			$this->form_validation->set_rules('id_parent', 'Parent', 'required');
			
			if($this->form_validation->run() == TRUE){
				$id = $this->input->post('id');
				$nama = $this->input->post('nama');
				$kode = $this->input->post('kode');
				$id_parent = $this->input->post('id_parent');
				
				$action = $this->input->post('action');
				if($action == 'add'){
					$this->m_sumber_dana->insert($kode, $nama, $id_parent);
					set_success_message('Data Berhasil Ditambah!');
					redirect("bos/sumber_dana/home/");
				}else{
					$this->m_sumber_dana->update($id, $kode, $nama, $id_parent);
					set_success_message('Data Berhasil Diupdate!');
					redirect("bos/sumber_dana/home/");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian!');
				redirect("bos/sumber_dana/home/");
			}
		}
		
		public function delete(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('bos/sumber_dana/home/');
			}
			
			$this->m_sumber_dana->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('bos/sumber_dana/home/');
		}
			
}
