<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class bos_kegiatan extends CI_Controller {

	public function index($id_bos_program = false, $id_bos_subprogram = false)
	{	
		$this->load->model('bos/m_bos_kegiatan');
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();
		
		$data['program'] = array('- pilih program -');
		$program = $this->m_bos_kegiatan->get_program($id_sekolah,$id_tahun_ajaran);
		if($program){
			foreach ($program as $prog) {
				$data['program'][$prog['id_bos_program']] = $prog['program'];
			}
		}
		
		$data['subprogram'] = array('- pilih komponen subprogram -');
		if($id_bos_program){
			$subprogram = $this->m_bos_kegiatan->get_subprogram($id_bos_program,$id_sekolah,$id_tahun_ajaran);
			if($subprogram){
				foreach ($subprogram as $kur) {
					$data['subprogram'][$kur['id_bos_subprogram']] = $kur['subprogram'];
				}
			}
		}
		
		if($id_bos_program && $id_bos_subprogram){
		
		$data['isi'] = $this->m_bos_kegiatan->get_data($id_sekolah,$id_tahun_ajaran,$id_bos_subprogram);
		}
		
		$data["id_bos_program"] = $id_bos_program;
		$data["id_bos_subprogram"] = $id_bos_subprogram;
		
		// $data['program'] = $this->m_bos_kegiatan->get_program($id_sekolah,$id_tahun_ajaran);
		$data['component']="Pembiayaan";
		render('bos_kegiatan/bos_kegiatan',$data);
	}
	public function load_add()
	{	
		$this->load->model('bos/m_bos_kegiatan');
		$id_sekolah= get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();
		// $data['program'] = $this->m_bos_kegiatan->get_program($id_sekolah,$id_tahun_ajaran);
		// $data['guru'] = $this->m_bos_kegiatan->select_guru($id_sekolah);
		$data['component']="pembiayaan";
		render('bos_kegiatan/form_tambah',$data);
	}

	public function load_edit($id_bos_kegiatan)
	{	
		$this->load->model('bos/m_bos_kegiatan');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_bos_kegiatan->get_data_edit($id_bos_kegiatan);
	
		$data['component']="pembiayaan";
		render('bos_kegiatan/form_edit',$data);
	}

	public function delete($id_bos_kegiatan){
			$this->load->model('bos/m_bos_kegiatan');
			$this->m_bos_kegiatan->delete($id_bos_kegiatan);
			redirect(site_url('bos/bos_kegiatan/index/')) ;
	}

	public function submit_add(){
			$this->load->model('bos/m_bos_kegiatan');
			// $data['id_bos_program'] = $this->input->post('id_bos_program');
			$data['id_bos_subprogram'] = $this->input->post('id_bos_subprogram');
			$data['kegiatan'] = $this->input->post('kegiatan');
			$data['kode_kegiatan'] = $this->input->post('kode_kegiatan');
			$data['keterangan'] = $this->input->post('keterangan');
			$data['tgl_mulai'] = tanggal_db($this->input->post('tgl_mulai'));
			$data['tgl_akhir'] = tanggal_db($this->input->post('tgl_akhir'));
			
		
			$this->m_bos_kegiatan->add($data);
			redirect(site_url('bos/bos_kegiatan/index/')) ;
	}
	
	public function getSubprogram($id_bos_program){
			$this->load->model('bos/m_bos_kegiatan');
			$id_sekolah=get_id_sekolah();
			$id_tahun_ajaran = get_id_tahun_ajaran();
			$data['subprogram'] = $this->m_bos_kegiatan->get_subprogram($id_sekolah,$id_tahun_ajaran,$id_bos_program);
			$data;
	}

	public function submit_edit(){
			$this->load->model('bos/m_bos_kegiatan');
			$id=$this->input->post('id');
			// $data['id_bos_program'] = $this->input->post('id_bos_program');
			$data['id_bos_subprogram'] = $this->input->post('id_bos_subprogram');
			$data['kegiatan'] = $this->input->post('kegiatan');
			$data['kode_kegiatan'] = $this->input->post('kode_kegiatan');
			$data['keterangan'] = $this->input->post('keterangan');
			$data['tgl_mulai'] = tanggal_db($this->input->post('tgl_mulai'));
			$data['tgl_akhir'] = tanggal_db($this->input->post('tgl_akhir'));
			
			$this->m_bos_kegiatan->edit($data,$id);
			redirect(site_url('bos/bos_kegiatan/index/')) ;
	}
	public function search(){
		$this->load->model('bos/m_bos_kegiatan');
		// $nama = $this->input->post('nama');
		$id_program = $this->input->post('program');
		
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();
		
		$data['isi'] = $this->m_bos_kegiatan->get_data_search($id_program,$id_sekolah,$id_tahun_ajaran);
		$data['program'] = $this->m_bos_kegiatan->get_program($id_sekolah,$id_tahun_ajaran);
		$data['component']="pembiayaan";
		render('bos_kegiatan/bos_kegiatan',$data);
	}
	
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */