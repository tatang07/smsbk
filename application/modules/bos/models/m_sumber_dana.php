<?php
class m_sumber_dana extends MY_Model {
    public function __construct(){
		parent::__construct();
	}

		public $table = "m_bos_sumber_dana";
		
		public function get_sumber_dana(){

			$this->db->select('*');
			$this->db->from('m_bos_sumber_dana'); 
			
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_sumber_dana_by($id){
			$query = $this->db->query("SELECT * FROM m_bos_sumber_dana WHERE id_bos_sumber_dana='$id'");
			return $query->row();
		}
		
		public function get_datalist(){

			$this->db->select('*');
			$this->db->from('m_bos_sumber_dana'); 
			
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_datalist(){

			$this->db->select('*');
			$this->db->from('m_bos_sumber_dana'); 
			
			$query = $this->db->get();
					
			return $query->num_rows();
		}
		
		public function insert($kode, $nama, $id_parent){
			$this->db->query("INSERT INTO m_bos_sumber_dana(kode_bos_sumber_dana, bos_sumber_dana, id_parent) VALUES ('$kode', '$nama', '$id_parent');");
		}
		
		public function update($id, $kode, $nama, $id_parent){
			$data=array(
				'kode_bos_sumber_dana'=> $kode,
				'bos_sumber_dana'=> $nama,
				'id_parent'=> $id_parent
			);
			
			$this->db->where('id_bos_sumber_dana', $id);
			$this->db->update('m_bos_sumber_dana', $data);
		}
		
		public function delete($id){
			$this->db->delete('m_bos_sumber_dana', array('id_bos_sumber_dana'=>$id));
		}
}