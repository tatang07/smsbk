<?php
class m_bos_program extends MY_Model {
	
	public function get_data_search($nama,$id_sekolah,$id_tahun_ajaran){
		$query = $this->db->query("SELECT * FROM m_bos_program as bp where program  like '%$nama%' and id_sekolah=$id_sekolah and id_tahun_ajaran=$id_tahun_ajaran ");
		return $query->result_array();
	}
	public function get_data_search_default($nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM m_bos_program as bp join m_guru as g on g.id_guru = bp.id_guru where g.nama  like '%$nama%' and g.id_sekolah=$id_sekolah");
		return $query->result_array();
	}
	function get_data($id_sekolah,$id_tahun_ajaran){
			$this->db->select('*');
			$this->db->from('m_bos_program bp');
			// $this->db->join('m_guru g','g.id_guru = bp.id_guru');
			
			$this->db->where('id_sekolah',$id_sekolah);
			$this->db->where('id_tahun_ajaran',$id_tahun_ajaran);
			
			return $this->db->get()->result_array();
	}
	function get_siswa($id_sekolah,$id_tingkat_kelas){
			$this->db->select('*');
			$this->db->from('t_rombel_detail rd');
			$this->db->join('m_siswa s','s.id_siswa = rd.id_siswa');		
			$this->db->join('m_rombel r','r.id_rombel = rd.id_rombel');
			$this->db->where('s.id_sekolah',$id_sekolah);
			$this->db->where('r.id_tingkat_kelas',$id_tingkat_kelas);
			return $this->db->get()->result_array();
	}

	function get_data_edit($id_bos_program){
			$this->db->select('*');
			$this->db->from('m_bos_program bp');
			// $this->db->join('m_guru g','g.id_guru = bp.id_guru');
			// $this->db->join('r_jenjang_pendidikan jp','g.id_jenjang_pendidikan = jp.id_jenjang_pendidikan');
			$this->db->where('bp.id_bos_program',$id_bos_program);
			
			return $this->db->get()->result_array();
	}
	
	
	function select_guru($id_sekolah){
			$this->db->select('*');
			$this->db->from('m_guru');
			$this->db->where('id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}

	function add($data){
			$this->db->insert('m_bos_program',$data);
	}

	function edit($data,$id=0){
			$this->db->where('id_bos_program',$id);
			$this->db->update('m_bos_program',$data);
	}

	function delete($id_bos_program){
			$this->db->where('id_bos_program', $id_bos_program);
			$this->db->delete('m_bos_program');
	}

}