<?php
class m_realisasi_pengeluaran extends MY_Model {
	function get_data($id_bos_kegiatan, $id_bos_sumber_dana){
		$this->db->select('*');
		$this->db->from('t_bos_realisasi_pengeluaran brp');
		$this->db->join('m_bos_sumber_dana bsd','bsd.id_bos_sumber_dana = brp.id_bos_sumber_dana');
		$this->db->join('m_bos_kegiatan bk','bk.id_bos_kegiatan = brp.id_bos_kegiatan');
		$this->db->join('m_bos_subprogram bsp','bsp.id_bos_subprogram = bk.id_bos_subprogram');
		$this->db->join('m_bos_program bp','bp.id_bos_program = bsp.id_bos_program');
		$this->db->where('bk.id_bos_kegiatan', $id_bos_kegiatan);
		$this->db->where('bsd.id_bos_sumber_dana', $id_bos_sumber_dana);
		return $this->db->get();
	}

	function get_data_by_id($id_bos_realisasi_pengeluaran){
		$this->db->select('*');
		$this->db->from('t_bos_realisasi_pengeluaran brp');
		$this->db->join('m_bos_sumber_dana bsd','bsd.id_bos_sumber_dana = brp.id_bos_sumber_dana');
		$this->db->join('m_bos_kegiatan bk','bk.id_bos_kegiatan = brp.id_bos_kegiatan');
		$this->db->join('m_bos_subprogram bsp','bsp.id_bos_subprogram = bk.id_bos_subprogram');
		$this->db->join('m_bos_program bp','bp.id_bos_program = bsp.id_bos_program');
		$this->db->where('brp.id_bos_realisasi_pengeluaran', $id_bos_realisasi_pengeluaran);
		return $this->db->get();
	}

	function get_program($id){
		$this->db->select('*');
		$this->db->from('m_bos_program');
		$this->db->where('id_sekolah',$id);
		return $this->db->get();
	}

	function get_sub_program($id_bos_program){
		$this->db->select('*');
		$this->db->from('m_bos_subprogram');
		$this->db->where('id_bos_program', $id_bos_program);
		return $this->db->get();
	}

	function get_kegiatan($id_bos_subprogram){
		$this->db->select('*');
		$this->db->from('m_bos_kegiatan');
		$this->db->where('id_bos_subprogram', $id_bos_subprogram);
		return $this->db->get();
	}

	function get_sumber_dana(){
		$this->db->select('*');
		$this->db->from('m_bos_sumber_dana');
		return $this->db->get();
	}

	function submit_add($data){
		$this->db->insert('t_bos_realisasi_pengeluaran',$data);
	}
	
	function delete($id){
		$this->db->where('id_bos_realisasi_pengeluaran', $id);
		$this->db->delete('t_bos_realisasi_pengeluaran');
	}

	function submit_edit($id, $data){
		$this->db->where('id_bos_realisasi_pengeluaran',$id);
		$this->db->update('t_bos_realisasi_pengeluaran',$data);
	}
}