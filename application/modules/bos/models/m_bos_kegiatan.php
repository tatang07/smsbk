<?php
class m_bos_kegiatan extends MY_Model {
	
	public function get_data_search($id_program,$id_sekolah,$id_tahun_ajaran){
		$query = $this->db->query("SELECT * FROM m_bos_kegiatan as bk join m_bos_subprogram as bsp on bsp.id_bos_subprogram=bk.id_bos_subprogram join m_bos_program as bp on bp.id_bos_program=bsp.id_bos_program where bp.id_sekolah=$id_sekolah and bp.id_tahun_ajaran=$id_tahun_ajaran ");
		return $query->result_array();
	}
	public function get_data_search_default($nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM m_bos_program as bp join m_guru as g on g.id_guru = bp.id_guru where g.nama  like '%$nama%' and g.id_sekolah=$id_sekolah");
		return $query->result_array();
	}
	function get_data($id_sekolah,$id_tahun_ajaran,$id_bos_subprogram){
			$this->db->select('*');
			$this->db->from('m_bos_kegiatan bk');
			$this->db->join('m_bos_subprogram bsp','bsp.id_bos_subprogram = bk.id_bos_subprogram');
			$this->db->join('m_bos_program bp','bsp.id_bos_program = bp.id_bos_program');
			
			$this->db->where('bk.id_bos_subprogram',$id_bos_subprogram);
			$this->db->where('bp.id_sekolah',$id_sekolah);
			$this->db->where('bp.id_tahun_ajaran',$id_tahun_ajaran);
			
			return $this->db->get()->result_array();
	}
	function get_program($id_sekolah,$id_tahun_ajaran){
			$this->db->select('*');
			$this->db->from('m_bos_program bp');
			$this->db->where('bp.id_sekolah',$id_sekolah);
			$this->db->where('bp.id_tahun_ajaran',$id_tahun_ajaran);
			return $this->db->get()->result_array();
	}
	function get_subprogram($id_bos_program,$id_sekolah,$id_tahun_ajaran){
			$this->db->select('*');
			$this->db->from('m_bos_subprogram bsp');
			$this->db->join('m_bos_program bp','bsp.id_bos_program = bp.id_bos_program');
			$this->db->where('bp.id_sekolah',$id_sekolah);
			$this->db->where('bp.id_tahun_ajaran',$id_tahun_ajaran);
			$this->db->where('bp.id_bos_program',$id_bos_program);
			return $this->db->get()->result_array();
	}
	function get_data_edit($id_bos_kegiatan){
			$this->db->select('*');
			$this->db->from('m_bos_kegiatan bsp');
			// $this->db->join('m_bos_program bp','bp.id_bos_program = bsp.id_bos_program');
			// $this->db->join('r_jenjang_pendidikan jp','g.id_jenjang_pendidikan = jp.id_jenjang_pendidikan');
			$this->db->where('bsp.id_bos_kegiatan',$id_bos_kegiatan);
			
			return $this->db->get()->result_array();
	}
	
	
	function select_guru($id_sekolah){
			$this->db->select('*');
			$this->db->from('m_guru');
			$this->db->where('id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}

	function add($data){
			$this->db->insert('m_bos_kegiatan',$data);
	}

	function edit($data,$id=0){
			$this->db->where('id_bos_kegiatan',$id);
			$this->db->update('m_bos_kegiatan',$data);
	}

	function delete($id_bos_kegiatan){
			$this->db->where('id_bos_kegiatan', $id_bos_kegiatan);
			$this->db->delete('m_bos_kegiatan');
	}

}