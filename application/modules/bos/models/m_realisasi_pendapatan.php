<?php
class m_realisasi_pendapatan extends MY_Model {
    public function __construct(){
		parent::__construct();
	}

		public $table = "t_bos_realisasi_pendapatan";
		
		public function get_sumber_dana(){

			$this->db->select('*');
			$this->db->from('m_bos_sumber_dana'); 
			
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_realisasi_pendapatan_by($id){
			$query = $this->db->query("SELECT * FROM t_bos_realisasi_pendapatan WHERE id_bos_realisasi_pendapatan='$id'");
			return $query->row();
		}
		
		public function get_datalist(){

			$this->db->select('*');
			$this->db->from('t_bos_realisasi_pendapatan r'); 
			$this->db->join('m_bos_sumber_dana s', 'r.id_bos_sumber_dana=s.id_bos_sumber_dana'); 
			$this->db->where('r.id_sekolah', get_id_sekolah()); 
			$this->db->where('r.id_tahun_ajaran', get_id_tahun_ajaran()); 
			
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_datalist(){

			$this->db->select('*');
			$this->db->from('t_bos_realisasi_pendapatan r'); 
			$this->db->join('m_bos_sumber_dana s', 'r.id_bos_sumber_dana=s.id_bos_sumber_dana'); 
			$this->db->where('r.id_sekolah', get_id_sekolah()); 
			$this->db->where('r.id_tahun_ajaran', get_id_tahun_ajaran()); 
			
			$query = $this->db->get();
					
			return $query->num_rows();
		}
		
		public function insert($id_sumber_dana, $tgl, $no_bukti, $uraian, $pajak, $jml_setelah_pajak, $ket){
			$id_sekolah=get_id_sekolah();
			$id_tahun_ajaran=get_id_tahun_ajaran();
			$this->db->query("INSERT INTO t_bos_realisasi_pendapatan(id_bos_sumber_dana, tanggal_transaksi, nomor_bukti, uraian, pajak, jumlah_setelah_pajak, keterangan, id_sekolah, id_tahun_ajaran) VALUES ('$id_sumber_dana', '$tgl', '$no_bukti', '$uraian', '$pajak', '$jml_setelah_pajak', '$ket', '$id_sekolah', '$id_tahun_ajaran');");
		}
		
		public function update($id, $id_sumber_dana, $tgl, $no_bukti, $uraian, $pajak, $jml_setelah_pajak, $ket){
			$data=array(
				'id_bos_sumber_dana'=> $id_sumber_dana,
				'tanggal_transaksi'=> $tgl,
				'nomor_bukti'=> $no_bukti,
				'uraian'=> $uraian,
				'pajak'=> $pajak,
				'jumlah_setelah_pajak'=> $jml_setelah_pajak,
				'keterangan'=> $ket
			);
			
			$this->db->where('id_bos_realisasi_pendapatan', $id);
			$this->db->update('t_bos_realisasi_pendapatan', $data);
		}
		
		public function delete($id){
			$this->db->delete('t_bos_realisasi_pendapatan', array('id_bos_realisasi_pendapatan'=>$id));
		}
}