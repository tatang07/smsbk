<h1 class="grid_12">Pembiayaan</h1>
<form action="<?php echo base_url('bos/rencana_pengeluaran/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
	<?php foreach($data_edit as $de){?>
		
	<fieldset>
			<legend>Tambah Rencana Pengeluaran</legend>

			<div class="row">
				<label><strong>Kurikulum</strong></label>
				<div>
					<select name="id_bos_program" disabled>
						<?php foreach ($program as $prog) { ?>
							<?php if($de['id_bos_program'] == $prog['id_bos_program']){ ?>
								<option selected value="<?php echo $prog['id_bos_program']; ?>"><?php echo $prog['program'] ?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="row">
				<label><strong>Sub Program</strong></label>
				<div>
					<select name="id_bos_subprogram" disabled>
						<?php foreach ($subprogram as $sp) { ?>
							<?php if($de['id_bos_subprogram'] == $sp['id_bos_subprogram']){ ?>
								<option selected value="<?php echo $sp['id_bos_subprogram']; ?>"><?php echo $sp['subprogram'] ?></option>
							<?php }?>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="row">
				<label><strong>Kegiatan</strong></label>
				<div>
					<select name="id_bos_kegiatan" disabled>
						<?php foreach ($kegiatan as $k) { ?>
							<?php if($de['id_bos_kegiatan'] == $k['id_bos_kegiatan']){ ?>
								<option selected value="<?php echo $k['id_bos_kegiatan']; ?>"><?php echo $k['kegiatan'] ?></option>
							<?php }?>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="row">
				<label><strong>Sumber Dana</strong></label>
				<div>
					<select name="id_bos_sumber_dana" disabled>
						<?php foreach ($sumber_dana as $sd) { ?>
							<?php if($de['id_bos_sumber_dana'] == $sd['id_bos_sumber_dana']){ ?>
								<option selected value="<?php echo $sd['id_bos_sumber_dana']; ?>"><?php echo $sd['bos_sumber_dana'] ?></option>
							<?php }?>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="row">
				<label for="f1_normal_input">
					<strong>Periode 1</strong>
				</label>
				<div>
					<input type="text" name="p1" value="<?php echo $de['periode1'] ?>" />					
					<input type="hidden" name="id_bos_rencana_pengeluaran" value="<?php echo $de['id_bos_rencana_pengeluaran'] ?>" />					
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Periode 2</strong>
				</label>
				<div>
					<input type="text" name="p2" value="<?php echo $de['periode2'] ?>" />					
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Periode 3</strong>
				</label>
				<div>
					<input type="text" name="p3" value="<?php echo $de['periode3'] ?>" />					
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Periode 4</strong>
				</label>
				<div>
					<input type="text" name="p4" value="<?php echo $de['periode4'] ?>" />
				</div>
			</div>
			
			<div class="row">
				<label for="f1_textarea">
					<strong>Keterangan</strong>
				</label>
				<div>
					<input type="text" name="ket" value="<?php echo $de['keterangan'] ?>" />
				</div>
			</div>		
		</fieldset>
	<?php }?>

	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" name=send />
			 <a href="<?php echo base_url('bos/rencana_pengeluaran/'); ?>"> <input value="Batal" type="button"></a>
		</div>
		<div class="right">
		</div>
	</div>
</form>