<pre>
	<?php //print_r($rencana) ?>
</pre>

<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Rencana Pengeluaran</h2>
       </div>
       <div class="tabletools">
       		<div class="right">
			  	<a href="<?php echo base_url('bos/rencana_pengeluaran/tambah/') ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
       		<div class="dataTables_filter">
				<form action="<?php echo base_url('bos/rencana_pengeluaran/tampil'); ?>" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Program</span></th>
							<td width=200px align='left'>
								<?php echo simple_form_dropdown_clear("id_bos_program", $program, $id_bos_program, "", null) ?>
							</td>
						</tr>

						<tr>
							<th width=100px align='left'><span class="text"><span>Sub Program</span></th>
							<td width=200px align='left'>
								<?php echo simple_form_dropdown_clear("id_bos_subprogram", $subprogram, $id_bos_subprogram, "", null) ?>
							</td>
						</tr>

						<tr>
							<th width=100px align='left'><span class="text"><span>Sumber Dana</span></th>
							<td width=200px align='left'>
								<?php echo simple_form_dropdown_clear("id_bos_sumber_dana", $sumber_dana, $id_bos_sumber_dana, "", null) ?>
							</td>
						</tr>
						
						<tr>
							<td colspan= '2' width=300px align='right'>
								<input type="hidden" value="id_tingkat_kelas">
								<input type="submit" value="Tampilkan" name=send />
							</td>
						</tr>
					</table>
				</form>
			</div>
        </div>

        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid"> 
					<table class="styled" >
						<thead>
							<tr>
								<th>No</th>
								<th>Kode</th>
								<th>Kegiatan</th>
								<th>P1</th>
								<th>P2</th>
								<th>P3</th>
								<th>P4</th>
								<th>Jumlah</th>
								<th>Keterangan</th>
								<th>Aksi</th>
							</tr>
						</thead>
				
						<tbody>
							<?php if(isset($rencana)){ ?>
								<?php $i=1; ?>
							 		<?php foreach ($rencana as $r) { ?>
										<tr>
											<td width='7%' class='center'><?php echo $i; ?></td>
											<td width='' class='left'><?php echo $r['kegiatan'] ?></td>
											<td width='' class='left'><?php echo $r['kegiatan'] ?></td>
											<td width='8%' class='left'><?php echo $r['periode1'] ?></td>
											<td width='8%' class='left'><?php echo $r['periode2'] ?></td>
											<td width='8%' class='left'><?php echo $r['periode3'] ?></td>
											<td width='8%' class='left'><?php echo $r['periode4'] ?></td>

											<?php
												$jumlah = $r['periode1'] + $r['periode2'] + $r['periode3'] + $r['periode4'];
											?>

											<td width='8%' class='left'><?php echo $jumlah; ?></td>
											<td width='' class='left'><?php echo $r['keterangan'] ?></td>
											<td width='12%' align="center">
												<a href="<?php echo base_url('bos/rencana_pengeluaran/delete/'.$r['id_bos_rencana_pengeluaran']) ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
												<a href="<?php echo base_url('bos/rencana_pengeluaran/load_edit/'.$r['id_bos_rencana_pengeluaran'].'/'.$r['id_bos_program'].'/'.$r['id_bos_subprogram'].'/'.$r['id_bos_sumber_dana'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Edit </a>
											</td>																					
										</tr>
										<?php $i++; ?>
									<?php } ?>
							<?php } ?>
						</tbody>
					</table>
					<div class="footer">

					</div>
				</div>
            </div>
        </div>  
    </div>
</div>

<script type="text/javascript">		
	$(function(){
		$('#id_bos_program').chosen().change(function(){
			window.location = "<?php echo site_url('bos/rencana_pengeluaran/index'); ?>/" + $(this).val();
		})
		$('#id_bos_subprogram').chosen().change(function(){
			window.location = "<?php echo site_url('bos/rencana_pengeluaran/index/'.$this->uri->segment(4)); ?>/" + $(this).val();
		})
	})
</script>