<h1 class="grid_12">Pembiayaan</h1>
<form action="<?php echo base_url('bos/rencana_pengeluaran/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
	<fieldset>
		<legend>Tambah Rencana Pengeluaran</legend>

		<div class="row">
			<label><strong>Program</strong></label>
			<div>
				<?php echo simple_form_dropdown_clear("id_bos_program", $program, $id_bos_program, "", null) ?>
			</div>
		</div>
		
		<?php if ($id_bos_program): ?>

		<div class="row">
			<label><strong>Sub Program</strong></label>
			<div>
				<?php echo simple_form_dropdown_clear("id_bos_subprogram", $subprogram, $id_bos_subprogram, "", null) ?>
			</div>
		</div>

		<?php if ($id_bos_subprogram): ?>

		<div class="row">
			<label><strong>Kegiatan</strong></label>
			<div>
				<?php echo simple_form_dropdown_clear("id_bos_kegiatan", $kegiatan, $id_bos_kegiatan	, "", null) ?>
			</div>
		</div>

		<?php if ($id_bos_kegiatan): ?>

		<div class="row">
			<label for="f1_normal_input">
				<strong>Sumber Dana</strong>
			</label>
			<div>
				<select name="id_bos_sumber_dana" data-placeholder="-- Pilih Sumber Dana --">
					<option>-- Pilih Sumber Dana --</option>
				<?php foreach($sumber_dana as $sd): ?>
					<option value="<?php echo $sd['id_bos_sumber_dana']; ?>"><?php echo $sd['bos_sumber_dana']; ?></option>
				<?php endforeach; ?>
				</select>
			</div>
		</div>

		<div class="row">
			<label for="f1_normal_input">
				<strong>Periode 1</strong>
			</label>
			<div>
				<input type="text" name="p1" value="" />
				
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Periode 2</strong>
			</label>
			<div>
				<input type="text" name="p2" value="" />
				
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Periode 3</strong>
			</label>
			<div>
				<input type="text" name="p3" value="" />
				
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Periode 4</strong>
			</label>
			<div>
				<input type="text" name="p4" value="" />
				
			</div>
		</div>
		
		<div class="row">
			<label for="f1_textarea">
				<strong>Keterangan</strong>
			</label>
			<div>
				<input type="text" name="ket" value="" />
			</div>
		</div>
		
		<?php endif; ?>
		<?php endif; ?>
		<?php endif; ?>
		
	</fieldset><!-- End of fieldset -->
	
	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" name=send />
			 <a href="<?php echo base_url('bos/rencana_pengeluaran/'); ?>"> <input value="Batal" type="button"></a>
		</div>
		<div class="right">
		</div>
	</div><!-- End of .actions -->
</form><!-- End of .box -->

<script type="text/javascript">		
	$(function(){
		$('#id_bos_program').chosen().change(function(){
			window.location = "<?php echo site_url('bos/rencana_pengeluaran/tambah'); ?>/" + $(this).val();
		})
		$('#id_bos_subprogram').chosen().change(function(){
			window.location = "<?php echo site_url('bos/rencana_pengeluaran/tambah/'.$this->uri->segment(4)); ?>/" + $(this).val();
		})
		$('#id_bos_kegiatan').chosen().change(function(){
			window.location = "<?php echo site_url('bos/rencana_pengeluaran/tambah/'.$this->uri->segment(4) .'/'. $this->uri->segment(5) ); ?>/" + $(this).val();
		})
	})
</script>