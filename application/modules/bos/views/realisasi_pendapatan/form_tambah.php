	<h1 class="grid_12">Pembiayaan</h1>
			
			<form action="<?php echo base_url('bos/realisasi_pendapatan/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah realisasi Pendapatan</legend>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Sumber Dana</strong>
						</label>
						<div>
							<input type="hidden" name="id" value="<?php echo $id; ?>" />
							<input type="hidden" name="action" value="<?php echo $action; ?>" />
							
							<select name="id_sumber_dana" id="id_sumber_dana" data-placeholder="-- Pilih Sumber Dana --">
								<option value=''>-- Pilih Sumber Dana --</option>
							<?php foreach($sumber_dana as $t): ?>
								<?php if($t->id_bos_sumber_dana == $id_sumber_dana){ ?>
									<option selected value="<?php echo $t->id_bos_sumber_dana; ?>"><?php echo $t->kode_bos_sumber_dana.' - '.$t->bos_sumber_dana; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $t->id_bos_sumber_dana; ?>"><?php echo $t->kode_bos_sumber_dana.' - '.$t->bos_sumber_dana; ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Transaksi</strong>
						</label>
						<div>
							<input type="date" name="tgl" value="<?php echo $tgl; ?>" />
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nomor Bukti</strong>
						</label>
						<div>
							<input type="text" name="no_bukti" value="<?php echo $no_bukti; ?>" />
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Uraian</strong>
						</label>
						<div>
							<textarea name="uraian"><?php echo $uraian; ?></textarea>
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>pajak</strong>
						</label>
						<div>
							<input type="text" name="pajak" value="<?php echo $pajak; ?>" />
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Jumlah Setelah Dipotong Pajak</strong>
						</label>
						<div>
							<input type="text" name="jml_setelah_pajak" value="<?php echo $jml_setelah_pajak; ?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<textarea name="ket"><?php echo $ket; ?></textarea>
							
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('bos/realisasi_pendapatan/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		