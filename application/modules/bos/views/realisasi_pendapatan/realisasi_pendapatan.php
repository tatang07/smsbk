<?php 
	$statuspil = 0;
	if(isset($_POST['id_sumber_dana'])){
	$a = $_POST['id_sumber_dana'];
	$statuspil = $a; } ?>
	
<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Realisasi Pendapatan</h2>
		</div>
		<div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('bos/realisasi_pendapatan/tambah/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="" name= "myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Sumber Dana</span></th>
							<td width=200px align='left'>
								<select name="id_sumber_dana" id="id_sumber_dana" onchange="submitform();" data-placeholder="-- Pilih Sumber Dana --">
									<option value="0">-- Pilih Sumber Dana --</option>
								<?php foreach($sumber_dana as $q): ?>
									
									<?php if($q->id_bos_sumber_dana == $statuspil){ ?>
										<option selected value='<?php echo $q->id_bos_sumber_dana; ?>' data-status-pilihan="<?php echo $q->id_bos_sumber_dana; ?>"><?php echo $q->bos_sumber_dana; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $q->id_bos_sumber_dana; ?>' data-status-pilihan="<?php echo $q->id_bos_sumber_dana; ?>"><?php echo $q->bos_sumber_dana; ?></option>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
			
		</div>
	
		
		<?php if($statuspil !=0){ ?>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Tanggal Transaksi</th>
								<th>Nomor Bukti</th>
								<th>Uraian</th>
								<th>Pajak</th>
								<th>Jumlah Setelah Dipotong Pajak</th>
								<th>Keterangan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						
							<?php if($list){ ?>
								<?php $no=1; foreach($list as $l): ?>
								<?php if($l->id_bos_sumber_dana == $statuspil){ ?>
									
										<tr>
											<td width="50px" class="center"><?php echo $no; ?></td>
											<td width="100px" class="center"><?php echo tanggal_view($l->tanggal_transaksi); ?></td>
											<td width="100px" class="center"><?php echo $l->nomor_bukti; ?></td>
											<td width="300px" class="center"><?php echo $l->uraian; ?></td>
											<td width="100px" class="center"><?php echo $l->pajak; ?></td>
											<td width="100px" class="center"><?php echo $l->jumlah_setelah_pajak; ?></td>
											<td width="200px" class="center"><?php echo $l->keterangan; ?></td>
											<td width="150px" class="center">
												<a href="<?php echo base_url('bos/realisasi_pendapatan/edit/'.$l->id_bos_realisasi_pendapatan); ?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-edit"></i> Edit</a>
												<a href="<?php echo base_url('bos/realisasi_pendapatan/delete/'.$l->id_bos_realisasi_pendapatan); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
										<?php $no++; ?>
								<?php } endforeach; ?>
							
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $no-1; ?></div>
					</div>
							<?php }else{ ?>
								</tbody>
								</table>
								<div class="footer">Data tidak ditemukan!
								</div>
							<?php } ?>
				</div>
			</div>
		</div>
		
		<?php }else{ ?>
			<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">
					<div class="footer">
						<div class="dataTables_info">Pilih Sumber Dana Yang Anda Cari</div>
					</div>
				</div>
			</div>
			</div>
		<?php } ?>
	</div>
</div>


<script>
	
	function submitform()
	{
	  document.myform.submit();
	}
</script>