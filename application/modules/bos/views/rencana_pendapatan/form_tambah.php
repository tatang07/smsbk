	<h1 class="grid_12">Pembiayaan</h1>
			
			<form action="<?php echo base_url('bos/rencana_pendapatan/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Rencana Pendapatan</legend>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Sumber Dana</strong>
						</label>
						<div>
							<input type="hidden" name="id" value="<?php echo $id; ?>" />
							<input type="hidden" name="action" value="<?php echo $action; ?>" />
							
							<select name="id_sumber_dana" id="id_sumber_dana" data-placeholder="-- Pilih Sumber Dana --">
								<option value=''>-- Pilih Sumber Dana --</option>
							<?php foreach($sumber_dana as $t): ?>
								<?php if($t->id_bos_sumber_dana == $id_sumber_dana){ ?>
									<option selected value="<?php echo $t->id_bos_sumber_dana; ?>"><?php echo $t->kode_bos_sumber_dana.' - '.$t->bos_sumber_dana; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $t->id_bos_sumber_dana; ?>"><?php echo $t->kode_bos_sumber_dana.' - '.$t->bos_sumber_dana; ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Periode 1</strong>
						</label>
						<div>
							<input type="text" name="p1" value="<?php echo $p1; ?>" />
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Periode 2</strong>
						</label>
						<div>
							<input type="text" name="p2" value="<?php echo $p2; ?>" />
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Periode 3</strong>
						</label>
						<div>
							<input type="text" name="p3" value="<?php echo $p3; ?>" />
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Periode 4</strong>
						</label>
						<div>
							<input type="text" name="p4" value="<?php echo $p4; ?>" />
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="ket" value="<?php echo $ket; ?>" />
						</div>
					</div>
					
					
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('bos/rencana_pendapatan/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		