<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Rencana Pendapatan</h2>
		</div>
		<div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('bos/rencana_pendapatan/tambah/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Sumber Dana</th>
								<th>Periode 1</th>
								<th>Periode 2</th>
								<th>Periode 3</th>
								<th>Periode 4</th>
								<th>Total</th>
								<th>Keterangan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						
							<?php if($list){ ?>
								<?php $no=1; foreach($list as $l): ?>
									
										<tr>
											<td width="50px" class="center"><?php echo $no; ?></td>
											<td width="50px" class="center"><?php echo $l->kode_bos_sumber_dana; ?></td>
											<td width="200px" class="center"><?php echo $l->bos_sumber_dana; ?></td>
											<td width="100px" class="center"><?php echo $l->periode_1; ?></td>
											<td width="100px" class="center"><?php echo $l->periode_2; ?></td>
											<td width="100px" class="center"><?php echo $l->periode_3; ?></td>
											<td width="100px" class="center"><?php echo $l->periode_4; ?></td>
											<td width="100px" class="center"><?php echo $l->periode_1+$l->periode_2+$l->periode_3+$l->periode_4; ?></td>
											<td width="200px" class="center"><?php echo $l->keterangan; ?></td>
											<td width="150px" class="center">
												<a href="<?php echo base_url('bos/rencana_pendapatan/edit/'.$l->id_bos_rencana_pendapatan); ?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-edit"></i> Edit</a>
												<a href="<?php echo base_url('bos/rencana_pendapatan/delete/'.$l->id_bos_rencana_pendapatan); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
										<?php $no++; ?>
								<?php endforeach; ?>
							
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $jumlah; ?></div>
					</div>
							<?php }else{ ?>
								</tbody>
								</table>
								<div class="footer">Data tidak ditemukan!
								</div>
							<?php } ?>
				</div>
			</div>
		</div>        
	</div>
</div>