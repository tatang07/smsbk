<?php  
	$statuspil = 0;
	if(isset($_POST['program'])){
	$a = $_POST['program'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>BOS SubProgram</h2>
			<?php //echo $statuspil ?>
       </div>
	
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('bos/bos_subprogram/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('bos/bos_subprogram/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
							
								<th width="110px" align="left"><span class="text">Program</span></th>
								<td width="200px" align="left">
								<select name="program" id="program" onchange="submitform();">
									<option value="0">-Program-</option>
									<?php foreach($program as $d):?>
									<?php if($statuspil==$d['id_bos_program']){?>
									<option selected value="<?php echo $d['id_bos_program']?>"><?php echo $d['program']?></option>
									<?php }else {?>
									<option value="<?php echo $d['id_bos_program']?>"><?php echo $d['program']?></option>
									<?php } ?>
								<?php endforeach;?>
								</select>
								</td>
								<td width="10px"></td>
								<td width="1px">
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Kode</th>
								<th >Sub Program</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
						
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
							
								<tr>
									<td width ="50px" class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['kode_subprogram']?>
									</td class='center'>
									<td class='center'>
									<?php  echo $d['subprogram'];?>
									</td>
									
									<td width="150px" class='center'>
									<a href="<?php echo site_url('bos/bos_subprogram/load_edit/'.$d['id_bos_subprogram'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a href="<?php echo site_url('bos/bos_subprogram/delete/'.$d['id_bos_subprogram'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>