<h1 class="grid_12">Pembiayaan</h1>
<form action="<?php echo base_url('bos/realisasi_pengeluaran/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
	<?php foreach($data_edit as $de){?>
		
	<fieldset>
			<legend>Edit Realisasi Pengeluaran</legend>

			<div class="row">
				<label><strong>Program</strong></label>
				<div>
					<select name="id_bos_program" disabled>
						<?php foreach ($program as $prog) { ?>
							<?php if($de['id_bos_program'] == $prog['id_bos_program']){ ?>
								<option selected value="<?php echo $prog['id_bos_program']; ?>"><?php echo $prog['program'] ?></option>
							<?php } ?>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="row">
				<label><strong>Sub Program</strong></label>
				<div>
					<select name="id_bos_subprogram" disabled>
						<?php foreach ($subprogram as $sp) { ?>
							<?php if($de['id_bos_subprogram'] == $sp['id_bos_subprogram']){ ?>
								<option selected value="<?php echo $sp['id_bos_subprogram']; ?>"><?php echo $sp['subprogram'] ?></option>
							<?php }?>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="row">
				<label><strong>Kegiatan</strong></label>
				<div>
					<select name="id_bos_kegiatan" disabled>
						<?php foreach ($kegiatan as $k) { ?>
							<?php if($de['id_bos_kegiatan'] == $k['id_bos_kegiatan']){ ?>
								<option selected value="<?php echo $k['id_bos_kegiatan']; ?>"><?php echo $k['kegiatan'] ?></option>
							<?php }?>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="row">
				<label><strong>Sumber Dana</strong></label>
				<div>
					<select name="id_bos_sumber_dana" disabled>
						<?php foreach ($sumber_dana as $sd) { ?>
							<?php if($de['id_bos_sumber_dana'] == $sd['id_bos_sumber_dana']){ ?>
								<option selected value="<?php echo $sd['id_bos_sumber_dana']; ?>"><?php echo $sd['bos_sumber_dana'] ?></option>
							<?php }?>
						<?php } ?>
					</select>
				</div>
			</div>

			<div class="row">
				<label for="f1_normal_input">
					<strong>Tanggal Transaksi</strong>
				</label>
				<div>
					<input type="date" name="tgl_transaksi" value="<?php echo tanggal_view($de['tgl_transaksi']) ?>" />					
					<input type="hidden" name="id_bos_realisasi_pengeluaran" value="<?php echo $de['id_bos_realisasi_pengeluaran'] ?>" />					
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>No. Bukti</strong>
				</label>
				<div>
					<input type="text" name="nomor_bukti" value="<?php echo $de['nomor_bukti'] ?>" />					
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Uraian</strong>
				</label>
				<div>
					<input type="text" name="uraian" value="<?php echo $de['uraian'] ?>" />					
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Pajak</strong>
				</label>
				<div>
					<input type="text" name="pajak" value="<?php echo $de['pajak'] ?>" />
				</div>
			</div>
			
			<div class="row">
				<label for="f1_textarea">
					<strong>Jumlah Setelah Pajak</strong>
				</label>
				<div>
					<input type="text" name="jumlah_setelah_pajak" value="<?php echo $de['jumlah_setelah_pajak'] ?>" />
				</div>
			</div>		
		</fieldset>
	<?php }?>

	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" name=send />
			 <a href="<?php echo base_url('bos/realisasi_pengeluaran/'); ?>"> <input value="Batal" type="button"></a>
		</div>
		<div class="right">
		</div>
	</div>
</form>