<h1 class="grid_12">Pembiayaan</h1>
<form action="<?php echo base_url('bos/realisasi_pengeluaran/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
	<fieldset>
		<legend>Tambah Rencana Pengeluaran</legend>

		<div class="row">
			<label><strong>Program</strong></label>
			<div>
				<?php echo simple_form_dropdown_clear("id_bos_program", $program, $id_bos_program, "", null) ?>
			</div>
		</div>
		
		<?php if ($id_bos_program): ?>

		<div class="row">
			<label><strong>Sub Program</strong></label>
			<div>
				<?php echo simple_form_dropdown_clear("id_bos_subprogram", $subprogram, $id_bos_subprogram, "", null) ?>
			</div>
		</div>

		<?php if ($id_bos_subprogram): ?>

		<div class="row">
			<label><strong>Kegiatan</strong></label>
			<div>
				<?php echo simple_form_dropdown_clear("id_bos_kegiatan", $kegiatan, $id_bos_kegiatan, "", null) ?>
			</div>
		</div>

		<?php if ($id_bos_kegiatan): ?>

		<div class="row">
			<label><strong>Sumber Dana</strong></label>
			<div>
				<?php echo simple_form_dropdown_clear("id_bos_sumber_dana", $sumber_dana, $id_bos_sumber_dana, "", null) ?>
			</div>
		</div>

		<div class="row">
			<label for="f1_normal_input">
				<strong>Tanggal Transaksi</strong>
			</label>
			<div>
				<input type="date" name="tgl_transaksi" value="" />
				
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>No. Bukti</strong>
			</label>
			<div>
				<input type="text" name="nomor_bukti" value="" />
				
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Uraian</strong>
			</label>
			<div>
				<input type="text" name="uraian" value="" />
				
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Pajak</strong>
			</label>
			<div>
				<input type="text" name="pajak" value="" />
				
			</div>
		</div>
		
		<div class="row">
			<label for="f1_textarea">
				<strong>Jumlah Setelah Pajak</strong>
			</label>
			<div>
				<input type="text" name="jumlah_setelah_pajak" value="" />
			</div>
		</div>
		
		<?php endif; ?>
		<?php endif; ?>
		<?php endif; ?>
		
	</fieldset><!-- End of fieldset -->
	
	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" name=send />
			 <a href="<?php echo base_url('bos/realisasi_pengeluaran/'); ?>"> <input value="Batal" type="button"></a>
		</div>
		<div class="right">
		</div>
	</div><!-- End of .actions -->
</form><!-- End of .box -->

<script type="text/javascript">		
	$(function(){
		$('#id_bos_program').chosen().change(function(){
			window.location = "<?php echo site_url('bos/realisasi_pengeluaran/tambah'); ?>/" + $(this).val();
		})
		$('#id_bos_subprogram').chosen().change(function(){
			window.location = "<?php echo site_url('bos/realisasi_pengeluaran/tambah/'.$this->uri->segment(4)); ?>/" + $(this).val();
		})
		$('#id_bos_kegiatan').chosen().change(function(){
			window.location = "<?php echo site_url('bos/realisasi_pengeluaran/tambah/'.$this->uri->segment(4).'/'.$this->uri->segment(5)); ?>/" + $(this).val();
		})
	})
</script>