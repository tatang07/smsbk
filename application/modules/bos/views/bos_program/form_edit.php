	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			
			<form action="<?php echo base_url('bos/bos_program/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Sertifikasi</legend>
					<?php foreach($data_edit as $d):?>
	
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kode Program</strong>
						</label>
						<div>
							<input type="text" name="kode_program" value="<?php echo $d['kode_program']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Program</strong>
						</label>
						<div>
							<input type="text" name="program" value="<?php echo $d['program'];?>" />
							<input type="hidden" name="id" value="<?php echo $d['id_bos_program']?>" />
						</div>
					</div>
					
				<?php endforeach?>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('bos/bos_program/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		