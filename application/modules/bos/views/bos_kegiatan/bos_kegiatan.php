<?php  
	$statuspil = 0;
	if(isset($_POST['program'])){
	$a = $_POST['program'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>BOS Kegiatan</h2>
			<?php //echo $statuspil ?>
       </div>
	
       <div class="tabletools">
			<div class="right">
				<?php $id_bos_program = $this->uri->segment(4); $id_bos_subprogram = $this->uri->segment(5);
				//echo $id_bos_program; echo $id_bos_subprogram;
				?>
			  	<a href="<?php echo base_url('bos/bos_kegiatan/load_add/'.$id_bos_program.'/'.$id_bos_subprogram); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form id="formPencarian" name="formPencarian" method="post" >
					<table>
						<tr>
							<th align='left'><span class="text">Kurikulum</span></th>
							<td width=10px align='left'>
								<div id='mapel<?php echo $id?>' class='listmapel'>
									<?php echo simple_form_dropdown_clear("id_bos_program", $program, $id_bos_program, "", null) ?>
								</div>
							</td>
							<td width="30px"></td>
							<?php if ($id_bos_program): ?>
								<th align='left'><span class="text">Komponen Nilai</span></th>
								<td width=100px align='left'>
									<div id='mapel<?php echo $id?>' class='listmapel'>
										<?php echo simple_form_dropdown_clear("id_bos_subprogram", $subprogram, $id_bos_subprogram, "", null) ?>
									</div>
								</td>
							<?php endif ?>
						</tr>
					</table>
				</form>
			</div>

            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Kode</th>
								<th >Kegiatan</th>
								<th >Tanggal Mulai</th>
								<th >Tanggal Akhir</th>
								<th >Keterangan</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
						<?php if(isset($isi)){?>
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
							
								<tr>
									<td width ="50px" class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['kode_kegiatan']?>
									</td >
									<td class='center'>
									<?php  echo $d['kegiatan']?>
									</td class='center'>
									<td class='center'>
									<?php  echo tanggal_view($d['tgl_mulai']);?>
									</td>
									<td class='center'>
									<?php  echo tanggal_view($d['tgl_akhir']);?>
									</td>
									<td class='center'>
									<?php  echo $d['keterangan'];?>
									</td>
									<td width="150px" class='center'>
									<a href="<?php echo site_url('bos/bos_kegiatan/load_edit/'.$d['id_bos_kegiatan'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a href="<?php echo site_url('bos/bos_kegiatan/delete/'.$d['id_bos_kegiatan'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
						<?php }?>
					</table>
					
					<div class="footer">
						<?php if(isset($isi)){?>
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						<?php }else{?>
						<div class="dataTables_info">Jumlah Data : 0</div>
						<?php }?>
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>
<script type="text/javascript">		
	$(function(){
		$('#id_bos_program').chosen().change(function(){
			window.location = "<?php echo site_url('bos/bos_kegiatan/index'); ?>/" + $(this).val();
		})
		$('#id_bos_subprogram').chosen().change(function(){
			window.location = "<?php echo site_url('bos/bos_kegiatan/index/'.$this->uri->segment(4)); ?>/" + $(this).val();
		})
	})
</script>