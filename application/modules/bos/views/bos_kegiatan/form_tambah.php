	<h1 class="grid_12">Pembiayaan</h1>
			
			<form action="<?php echo base_url('bos/bos_kegiatan/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Kegiatan</legend><?php //echo($tingkat_kelas);?>
					<?php $jenjang_sekolah = get_jenjang_sekolah(); ?>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kode Kegiatan</strong>
						</label>
						<div>
							 <input type="text" name="kode_kegiatan" value="">
							
							 <input type="hidden" name="id_bos_subprogram" value="<?php echo $this->uri->segment(5);?>">
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kegiatan</strong>
						</label>
						<div>
							<input type="text" name="kegiatan" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Mulai</strong>
						</label>
						<div>
							<input type="date" name="tgl_mulai" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Akhir</strong>
						</label>
						<div>
							<input type="date" name="tgl_akhir" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="" />
						</div>
					</div>
					
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('bos/bos_kegiatan/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		