	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			
			<form action="<?php echo base_url('bos/bos_kegiatan/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Sertifikasi</legend>
					<?php foreach($data_edit as $d):?>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kode Kegiatan</strong>
						</label>
						<div>
							<input type="text" name="kode_kegiatan" value="<?php echo $d['kode_kegiatan']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kegiatan</strong>
						</label>
						<div>
							<input type="text" name="kegiatan" value="<?php echo $d['kegiatan']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Mulai</strong>
						</label>
						<div>
							<input type="date" name="tgl_mulai" value="<?php echo $d['tgl_mulai']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Akhir</strong>
						</label>
						<div>
							<input type="date" name="tgl_akhir" value="<?php echo $d['tgl_akhir']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="<?php echo $d['keterangan'];?>" />
							<input type="hidden" name="id" value="<?php echo $d['id_bos_kegiatan']?>" />
							<input type="hidden" name="id_bos_subprogram" value="<?php echo $d['id_bos_subprogram']?>" />
						</div>
					</div>
					
				<?php endforeach?>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('bos/bos_kegiatan/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		