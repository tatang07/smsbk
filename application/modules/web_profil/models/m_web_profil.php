<?php
class m_web_profil extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_data_sekolah(){
			$id = get_id_sekolah();
			$query = $this->db->query("SELECT * FROM m_web_sekolah WHERE id_sekolah='$id'");
			return $query->result_array();
		}
		
		public function cari_data_sekolah(){
			$id = get_id_sekolah();
			$query = $this->db->query("SELECT * FROM m_web_sekolah WHERE id_sekolah='$id'");
			return $query->num_rows();
		}
		
		function edit($data,$id=0){
			$this->db->where('id_sekolah',$id);
			$this->db->update('m_web_sekolah',$data);
		}
		
		function add($data){
			$this->db->insert('m_web_sekolah',$data);
		}
}