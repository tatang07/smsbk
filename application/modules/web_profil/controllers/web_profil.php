<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class web_profil extends CI_Controller {
    	
		
		public function index(){
		
			$this->load->model('m_web_profil');
			
			// $data['component'] = 'home';
			$id=get_id_sekolah();
			
		
			$data['sekolah'] = $this->m_web_profil->get_data_sekolah();
			
			$cari_id = $this->m_web_profil->cari_data_sekolah();
			
			if($cari_id==0){
				render('web_profil/add');
			}else{
				render('web_profil/edit', $data);
			}
		}
	
		public function submit_edit(){
			$this->load->model('m_web_profil');
			$id=$this->input->post('id');
			$data['tentang_sekolah'] = $this->input->post('tentang_sekolah');
			
			
			 $config['upload_path'] = './extras/banner_sekolah/';
			 $config['allowed_types'] = 'gif|jpg|png';
			 $config['max_size'] = '10000';
			 $config['max_width'] = '3000';
			 $config['max_height'] = '3000';
			$this->load->library('upload', $config);
 
			 if (!$this->upload->do_upload('banner')){
			 	$this->load->helper('form');
				$error = array('error'=>$this->upload->display_errors());
				// $this->load->view('web_profil/form_upload', $error);
				// print_r($error);
				// print_r($this->upload->data());
			 }
			  else {
				$this->load->helper('form');
				$upload_data = $this->upload->data();
				$data['banner']=$upload_data['file_name'];
				// echo $data['banner'];
				
				// $data = array('upload_data' => $upload_data);
				// $this->load->view('uploadsample/view_upload_success', $data);
			 }
			 
		
			
			$this->m_web_profil->edit($data,$id);
			
			redirect(site_url('web_profil/web_profil/index/')) ;
		}
		
		public function submit_add(){
			$this->load->model('m_web_profil');
			$data['id_sekolah']=$this->input->post('id');
			$data['tentang_sekolah'] = $this->input->post('tentang_sekolah');
			
			
			 $config['upload_path'] = './extras/banner_sekolah/';
			 $config['allowed_types'] = 'gif|jpg|png';
			 $config['max_size'] = '10000';
			 $config['max_width'] = '3000';
			 $config['max_height'] = '3000';
			$this->load->library('upload', $config);
 
			 if (!$this->upload->do_upload('banner')){
			 	$this->load->helper('form');
				$error = array('error'=>$this->upload->display_errors());
				// $this->load->view('web_profil/form_upload', $error);
				// print_r($this->upload->data());
			 }
			 else {
				$this->load->helper('form');
				$upload_data = $this->upload->data();
				$data['banner']=$upload_data['file_name'];
				// echo $data['banner'];
				
				// $data = array('upload_data' => $upload_data);
				// $this->load->view('uploadsample/view_upload_success', $data);
			 }
			 
			// print_r($data);
			
			$this->m_web_profil->add($data);
			
			redirect(site_url('web_profil/web_profil/index/')) ;
		}
}
