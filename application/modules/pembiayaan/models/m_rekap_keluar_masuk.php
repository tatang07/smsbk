<?php
	class m_rekap_keluar_masuk extends MY_Model {
		function getDataKeuangan($id_sekolah,$id_tahun_ajaran){
			$this->db->select('*'); 
			$this->db->from('t_keuangan_sirkulasi');
			$this->db->where('id_sekolah', $id_sekolah);
			$this->db->where('id_tahun_ajaran', $id_tahun_ajaran);
			return $this->db->get();
		}

		function getDataKeuangan_ById($id){
			$this->db->select('*'); 
			$this->db->from('t_keuangan_sirkulasi');
			$this->db->where('id_keuangan_sirkulasi', $id);
			return $this->db->get();
		}

		function getTahunAjaran(){
			$this->db->select('*'); 
			$this->db->from('m_tahun_ajaran');
			return $this->db->get();
		}

		function add($data){
			$this->db->insert('t_keuangan_sirkulasi',$data);
		}

		function edit($data, $id){
			$this->db->where('id_keuangan_sirkulasi', $id);
			$this->db->update('t_keuangan_sirkulasi', $data);
		}

		function delete($id){
			$this->db->where('id_keuangan_sirkulasi', $id);
			$this->db->delete('t_keuangan_sirkulasi');
		}
	}
?>