<?php
class m_rekap_penerima_keringanan_pembayaran extends MY_Model {
	
	public function get_tahun_ajaran(){
			$query = $this->db
						->from('m_tahun_ajaran')
						->order_by('id_tahun_ajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
	}
	
	public function get_tahun_ajaran_by_id($id){
			$query = $this->db
						->from('m_tahun_ajaran')
						->where('id_tahun_ajaran',$id)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
	}
	function get_data($id_tahun_ajaran,$id_sekolah){
			$this->db->select('*');
			$this->db->from('t_keuangan_keringan_siswa');
			$this->db->join('m_siswa s', 's.id_siswa = t_keuangan_keringan_siswa.id_siswa');
			$this->db->join('r_jenis_pembayaran', 'r_jenis_pembayaran.id_jenis_pembayaran = t_keuangan_keringan_siswa.id_jenis_pembayaran');
			$this->db->where('id_tahun_ajaran',$id_tahun_ajaran);
			$this->db->where('s.id_sekolah',$id_sekolah);
			
			
			return $this->db->get()->result_array();
	}
	function get_siswa(){
			$this->db->select('*');
			$this->db->from('m_siswa');
			
			return $this->db->get()->result_array();
	}
	function get_jenis_pembayaran($id){
			$this->db->select('*');
			$this->db->from('r_jenis_pembayaran');
			$this->db->where('id_sekolah',$id);
			
			return $this->db->get()->result_array();
	}
	function get_data_by_id($id){
			$this->db->select('*');
			$this->db->from('t_keuangan_keringan_siswa');
			$this->db->where('id_keuangan_keringan_siswa',$id);
			
			return $this->db->get()->result_array();
	}
	// function get_data_sub_by_id($id){
			// $this->db->select('*');
			// $this->db->from('t_sekolah_sub_program_kerja');
			// $this->db->where('id_sekolah_sub_program_kerja',$id);
			
			// return $this->db->get()->result_array();
	// }
	// function get_data_sub($id){
			// $this->db->select('*');
			// $this->db->from('t_sekolah_sub_program_kerja');
			// $this->db->where('id_sekolah_program_kerja',$id);
			
			// return $this->db->get()->result_array();
	// }
	function add($data){
			$this->db->insert('t_keuangan_keringan_siswa',$data);
	}
	// function add_sub($data){
			// $this->db->insert('t_keuangan_keringan_siswa',$data);
	// }
	function edit($data,$id=0){
			$this->db->where('id_keuangan_keringan_siswa',$id);
			$this->db->update('t_keuangan_keringan_siswa',$data);
	}
	// function edit_sub($data,$id=0){
			// $this->db->where('id_sekolah_sub_program_kerja',$id);
			// $this->db->update('t_sekolah_sub_program_kerja',$data);
	// }
	function delete($id_sekolah_program_kerja){
			$this->db->where('id_keuangan_keringan_siswa', $id_sekolah_program_kerja);
			$this->db->delete('t_keuangan_keringan_siswa');
	}
	// function delete_sub($id_sekolah_sub_program_kerja){
			// $this->db->where('id_sekolah_sub_program_kerja', $id_sekolah_sub_program_kerja);
			// $this->db->delete('t_sekolah_sub_program_kerja');
	// }

}