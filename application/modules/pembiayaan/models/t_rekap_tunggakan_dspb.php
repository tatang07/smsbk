<?php
class t_rekap_tunggakan_dspb extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_all_rekap(){
			$query = $this->db
						->select('*')
						->from('t_keuangan_pembayaran_siswa k')
						->join('m_siswa s', 's.id_siswa = k.id_siswa')
						->join('m_tahun_ajaran t', 't.id_tahun_ajaran = k.id_tahun_ajaran')
						->join('r_jenis_pembayaran j', 'j.id_jenis_pembayaran = k.id_jenis_pembayaran')
						->where('j.id_sekolah', get_id_sekolah())
						->where('k.id_jenis_pembayaran', 1)
						->where('k.jumlah_dibayar >= k.tunggakan')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_siswa_rombel($id_rombel){

			$this->db->select('*, count(sa.id_siswa_absen) as jumlah_absen');
			$this->db->from('m_siswa s'); 
			
			$this->db->join('t_siswa_absen sa', 'sa.id_siswa=s.id_siswa', 'left');
			$this->db->join('r_jenis_absen ja', 'sa.id_jenis_absen=ja.id_jenis_absen', 'left');
			$this->db->join('t_agenda_kelas ak', 'sa.id_agenda_kelas=ak.id_agenda_kelas', 'left');
			$this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=ak.id_guru_matpel_rombel', 'left');
			$this->db->join('m_rombel r', 'gm.id_rombel=r.id_rombel', 'left');
			
			$this->db->where('r.id_rombel',$id_rombel);
			$this->db->group_by('s.nis');         
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
			
		}
		
		public function get_siswa_by($id){
			$sekolah = get_id_sekolah();
			$query = $this->db->query("SELECT * FROM m_siswa WHERE nis='$id' and id_sekolah = '$sekolah'");
			return $query->row();
		}
		
		public function get_all_rombel($id_sekolah){
			$query = $this->db
						->from('m_rombel')
						->where('id_sekolah',3)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_all_tahun_ajaran(){
			$query = $this->db
						->from('m_tahun_ajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function get_rombel_by($id){
			$query = $this->db->query("SELECT * FROM m_rombel WHERE id_rombel='$id'");
			return $query->row();
		}
		
		public function get_first_rombel(){
			$query = $this->db->query("SELECT * FROM m_rombel ORDER BY id_rombel ASC LIMIT 1;");
			return $query->row();
		}
		
		public function get_rekap_tunggakan_dspb_by($id){
			$query = $this->db->query("SELECT * FROM t_rekap_tunggakan_dspb where id_rekap_tunggakan_dspb = '$id';");
			return $query->row();
		}
		
		public function tambah($id_siswa, $id_tahun_ajaran, $id_jenis_pembayaran, $bulan, $tanggal, $jumlah_dibayar, $tunggakan){
			$data=array(
				'id_siswa'=> $id_siswa,
				'id_tahun_ajaran'=> $id_tahun_ajaran,
				'id_jenis_pembayaran'=> $id_jenis_pembayaran,
				'bulan'=> $bulan,
				'tanggal'=> $tanggal,
				'jumlah_dibayar'=> $jumlah_dibayar,
				'tunggakan'=> $tunggakan
			);
			$this->db->insert('t_keuangan_pembayaran_siswa', $data);
		}
		
}