<?php
class m_pembukuan extends MY_Model {
	
	public function get_tahun_ajaran(){
			$query = $this->db
						->from('m_tahun_ajaran')
						->order_by('id_tahun_ajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
	}
	
	public function get_tahun_ajaran_by_id($id){
			$query = $this->db
						->from('m_tahun_ajaran')
						->where('id_tahun_ajaran',$id)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
	}
	
	public function get_data($id_tahun_ajaran,$id_sekolah){
			$this->db->select('*');
			$this->db->from('t_keuangan_sirkulasi');
			$this->db->where('id_tahun_ajaran',$id_tahun_ajaran);
			$this->db->where('id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}	
}