<?php
	class m_dspt extends MY_Model {
		function getRombel(){
			$this->db->select('*'); 
			$this->db->from('m_rombel');
			return $this->db->get();
		}

		function getDataSiswa($id, $id_rombel){
			$this->db->select('*'); 
			$this->db->from('t_rombel_detail rd');
			$this->db->join('m_rombel r', 'rd.id_rombel = r.id_rombel');
			$this->db->join('m_siswa s', 'rd.id_siswa = s.id_siswa');
			$this->db->join('t_keuangan_pembayaran_siswa kps', 'rd.id_siswa = kps.id_siswa');
			$this->db->where('s.id_sekolah', $id);
			$this->db->where('rd.id_rombel', $id_rombel);
			return $this->db->get();
		}

		function getDataSiswa_byNis($nis){
			$this->db->select('*'); 
			$this->db->from('t_rombel_detail rd');
			$this->db->join('m_rombel r', 'rd.id_rombel = r.id_rombel');
			$this->db->join('m_siswa s', 'rd.id_siswa = s.id_siswa');
			$this->db->join('t_keuangan_pembayaran_siswa kps', 'rd.id_siswa = kps.id_siswa');
			$this->db->where('s.nis', $nis);
			return $this->db->get();
		}

		function getJumlahTunggakan($id){
			$this->db->select_sum('jumlah_dibayar');
			$this->db->from('t_keuangan_pembayaran_siswa_detail');
			$this->db->where('id_keuangan_pembayaran_siswa', $id);
			$this->db->group_by('id_keuangan_pembayaran_siswa');
			return $this->db->get();
		}

		function update($id, $data){
			$this->db->where('id_keuangan_pembayaran_siswa', $id);
			$this->db->update('t_keuangan_pembayaran_siswa', $data);
		}

		function detail($data){
			$this->db->insert('t_keuangan_pembayaran_siswa_detail',$data);
		}
	}
?>