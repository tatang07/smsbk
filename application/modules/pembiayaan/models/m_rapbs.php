<?php
	class m_rapbs extends MY_Model {
		function get_tahun_ajaran(){
			$query = $this->db
						->from('m_tahun_ajaran')
						->order_by('id_tahun_ajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}

		function get_data($id_tahun_ajaran,$id_sekolah){
			$this->db->select('*');
			$this->db->from('t_sekolah_program_kerja');
			$this->db->where('id_tahun_ajaran',$id_tahun_ajaran);
			$this->db->where('id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
		}

		function get_sub_proker($id_sekolah_program_kerja){
			$this->db->select('*');
			$this->db->from('t_sekolah_sub_program_kerja s');
			$this->db->join('t_sekolah_program_kerja p', 's.id_sekolah_program_kerja = p.id_sekolah_program_kerja');
			$this->db->where('s.id_sekolah_program_kerja',$id_sekolah_program_kerja);
			return $this->db->get();
		}

		function get_sub_proker_by_id($id_sekolah_sub_program_kerja){
			$this->db->select('*');
			$this->db->from('t_sekolah_sub_program_kerja s');
			$this->db->join('t_sekolah_program_kerja p', 's.id_sekolah_program_kerja = p.id_sekolah_program_kerja');
			$this->db->where('s.id_sekolah_sub_program_kerja',$id_sekolah_sub_program_kerja);
			return $this->db->get();
		}

		function update_sub_proker($id , $data){
			$this->db->where('id_sekolah_sub_program_kerja', $id);
			$this->db->update('t_sekolah_sub_program_kerja', $data);
		}

		function getJumlahAlokasi(){
			$this->db->select_sum('alokasi_dana');
			$this->db->from('t_sekolah_sub_program_kerja');
			$this->db->group_by('id_sekolah_program_kerja');
			return $this->db->get();
		}

		function getJumlahRealisasi(){
			$this->db->select_sum('pendanaan');
			$this->db->from('t_sekolah_sub_program_kerja');
			$this->db->group_by('id_sekolah_program_kerja');
			return $this->db->get();
		}
	}
?>