<?php
class m_dspt_penentuan_besaran extends MY_Model {
	
	public function get_data_search($status, $nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM t_keuangan_besaran as gsl join m_guru as g on g.id_guru = gsl.id_guru join r_jenjang_pendidikan as rp on rp.id_jenjang_pendidikan = g.id_jenjang_pendidikan where  g.nama  like '%$nama%' and gsl.status = '$status' and g.id_sekolah=$id_sekolah");
		return $query->result_array();
	}
	public function get_data_search_default($nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM t_keuangan_besaran as gsl join m_guru as g on g.id_guru = gsl.id_guru join r_jenjang_pendidikan as rp on rp.id_jenjang_pendidikan = g.id_jenjang_pendidikan where g.nama like '%$nama%' and g.id_sekolah=$id_sekolah");
		return $query->result_array();
	}
	function get_data($id_sekolah,$id_tahun_ajaran){
			$this->db->select('*');
			$this->db->from('t_keuangan_besaran kb');
			$this->db->join('m_tingkat_kelas tk','tk.id_tingkat_kelas = kb.id_tingkat_kelas');
			$this->db->where('kb.id_sekolah',$id_sekolah);
			$this->db->where('kb.id_tahun_ajaran',$id_tahun_ajaran);
			$this->db->where('kb.id_jenis_pembayaran',2);
			
			return $this->db->get()->result_array();
	}
	function get_siswa($id_sekolah,$id_tingkat_kelas){
			$this->db->select('*');
			$this->db->from('t_rombel_detail rd');
			$this->db->join('m_siswa s','s.id_siswa = rd.id_siswa');		
			$this->db->join('m_rombel r','r.id_rombel = rd.id_rombel');
			$this->db->where('s.id_sekolah',$id_sekolah);
			$this->db->where('r.id_tingkat_kelas',$id_tingkat_kelas);
			return $this->db->get()->result_array();
	}

	function get_data_edit($id_keuangan_besaran){
			$this->db->select('*');
			$this->db->from('t_keuangan_besaran kb');
			$this->db->join('m_tingkat_kelas tk','tk.id_tingkat_kelas = kb.id_tingkat_kelas');
			$this->db->where('kb.id_keuangan_besaran',$id_keuangan_besaran);
			return $this->db->get()->result_array();
	}
	
	
	function select_kelas($jenjang_sekolah){
			$this->db->select('*');
			$this->db->from('m_tingkat_kelas');
			$this->db->where('id_jenjang_sekolah',$jenjang_sekolah);			
			return $this->db->get()->result_array();
	}

	function add($data){
			$this->db->insert('t_keuangan_besaran',$data);
	}

	function edit($data,$id=0){
			$this->db->where('id_keuangan_besaran',$id);
			$this->db->update('t_keuangan_besaran',$data);
	}

	function delete($id_keuangan_besaran){
			$this->db->where('id_keuangan_besaran', $id_keuangan_besaran);
			$this->db->delete('t_keuangan_besaran');
	}

}