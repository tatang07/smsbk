<?php
	class t_keuangan_beasiswa extends MY_Model {
		function getBeasiswa($id_sekolah,$id_tahun_ajaran){
			$this->db->select('*'); 
			$this->db->from('t_keuangan_beasiswa kb');
			$this->db->join('m_siswa s', 's.id_siswa=kb.id_siswa', 'left');
			$this->db->join('r_pemberi_beasiswa pb', 'pb.id_pemberi_beasiswa=kb.id_pemberi_beasiswa', 'left');
			$this->db->where('s.id_sekolah', $id_sekolah);
			$this->db->where('kb.id_tahun_ajaran', $id_tahun_ajaran);
			return $this->db->get();
		}

		function getPemberiBeasiswa(){
			$id_sekolah = get_id_sekolah();
			$this->db->select('*'); 
			$this->db->from('r_pemberi_beasiswa');
			$this->db->where('id_sekolah', $id_sekolah);
			return $this->db->get();
		}
		
		function getSiswa_ByNis($nis){
			$this->db->select('*'); 
			$this->db->from('m_siswa');
			$this->db->where('nis', $nis);
			return $this->db->get();
		}
		
		function getPenerimaBeasiswa_ByIdPemberi($id, $id_tahun_ajaran){
			$this->db->select('*'); 
			$this->db->from('t_keuangan_beasiswa');
			$this->db->where('id_pemberi_beasiswa', $id);
			$this->db->where('id_tahun_ajaran', $id_tahun_ajaran);
			return $this->db->get();
		}
		
		function getPemberiBeasiswa_ById($id){
			$this->db->select('*'); 
			$this->db->from('r_pemberi_beasiswa');
			$this->db->where('id_pemberi_beasiswa', $id);
			return $this->db->get();
		}
		
		function getBeasiswa_ById($id){
			$this->db->select('*');
			$this->db->from('t_keuangan_beasiswa kb');
			$this->db->join('m_siswa s', 's.id_siswa=kb.id_siswa', 'left');
			$this->db->join('r_pemberi_beasiswa pb', 'pb.id_pemberi_beasiswa=kb.id_pemberi_beasiswa', 'left');
			$this->db->where('kb.id_keuangan_beasiswa', $id);
			return $this->db->get();
		}
		
		function getTahunAjaran(){
			$this->db->select('*'); 
			$this->db->from('m_tahun_ajaran');
			return $this->db->get();
		}

		function add($data){
			$this->db->insert('t_keuangan_beasiswa',$data);
		}

		function edit($data, $id){
			$this->db->where('id_keuangan_beasiswa', $id);
			$this->db->update('t_keuangan_beasiswa', $data);
		}

		function delete($id){
			$this->db->where('id_keuangan_beasiswa', $id);
			$this->db->delete('t_keuangan_beasiswa');
		}
		
		function hitungJumlah($id_tahun_ajaran){
			$pemberi_beasiswa = $this->getPemberiBeasiswa()->result_array();
			$data=array();
			foreach($pemberi_beasiswa as $pb):
				$data[]=$this->getPenerimaBeasiswa_ByIdPemberi($pb['id_pemberi_beasiswa'], $id_tahun_ajaran)->num_rows();
			endforeach;
			return $data;
		}
		
		function getData($nim){
			$this->db->select('id_siswa,nama');
			$this->db->from('m_siswa');
			$this->db->where('nis', $nim);
			$hasil=$this->db->get()->row();
			return $hasil;
		}
		
	}
?>