<?php
class m_dspb_pembebasan extends MY_Model {
	
	public function get_data_search($id_sekolah,$id_tahun_ajaran,$id_rombel){
		$query = $this->db->query("SELECT * FROM t_keuangan_pembebasan_siswa as kps join v_siswa as s on s.id_siswa = kps.id_siswa where  s.id_sekolah=$id_sekolah and s.id_tahun_ajaran = $id_tahun_ajaran and s.id_rombel=$id_rombel and id_jenis_pembayaran=1");
		return $query->result_array();
	}
	
	public function get_data_tagihan($id_sekolah,$id_tahun_ajaran,$id_rombel){
		$query = $this->db->query("SELECT * FROM m_rombel as r join t_keuangan_besaran as tk on r.id_tingkat_kelas = tk.id_tingkat_kelas where  r.id_sekolah=$id_sekolah and r.id_tahun_ajaran = $id_tahun_ajaran and r.id_rombel=$id_rombel and tk.id_jenis_pembayaran=1" );
		return $query->result_array();
	}
	public function get_data_search_default($nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM t_keuangan_besaran as gsl join m_guru as g on g.id_guru = gsl.id_guru join r_jenjang_pendidikan as rp on rp.id_jenjang_pendidikan = g.id_jenjang_pendidikan where g.nama like '%$nama%' and g.id_sekolah=$id_sekolah  and id_jenis_pembayaran=1");
		return $query->result_array();
	}
	function get_data($id_sekolah,$id_tahun_ajaran){
			$this->db->select('*');
			$this->db->from('t_keuangan_pembebasan_siswa kps');
			$this->db->join('v_siswa s','s.id_siswa = kps.id_siswa');
			$this->db->where('s.id_sekolah',$id_sekolah);
			$this->db->where('s.id_tahun_ajaran',$id_tahun_ajaran);
			$this->db->where('kps.id_jenis_pembayaran',1);
			
			return $this->db->get()->result_array();
	}
	
	function get_rombel($id_sekolah,$id_tahun_ajaran){
			$this->db->select('*');
			$this->db->from('m_rombel r');
			$this->db->where('r.id_sekolah',$id_sekolah);
			$this->db->where('r.id_tahun_ajaran',$id_tahun_ajaran);
			
			return $this->db->get()->result_array();
	}
	function get_siswa($id_sekolah,$id_tingkat_kelas){
			$this->db->select('*');
			$this->db->from('t_rombel_detail rd');
			$this->db->join('m_siswa s','s.id_siswa = rd.id_siswa');		
			$this->db->join('m_rombel r','r.id_rombel = rd.id_rombel');
			$this->db->where('s.id_sekolah',$id_sekolah);
			$this->db->where('r.id_tingkat_kelas',$id_tingkat_kelas);
			return $this->db->get()->result_array();
	}

	function get_data_edit($id_keuangan_besaran){
			$this->db->select('*');
			$this->db->from('t_keuangan_besaran kb');
			$this->db->join('m_tingkat_kelas tk','tk.id_tingkat_kelas = kb.id_tingkat_kelas');
			$this->db->where('kb.id_keuangan_besaran',$id_keuangan_besaran);
			return $this->db->get()->result_array();
	}
	
	
	function select_siswa($id_sekolah,$id_tahun_ajaran,$id_rombel){
			$this->db->select('*');
			$this->db->from('v_siswa');
			$this->db->where('id_sekolah',$id_sekolah);			
			$this->db->where('id_tahun_ajaran',$id_tahun_ajaran);			
			$this->db->where('id_rombel',$id_rombel);			
			return $this->db->get()->result_array();
	}

	function add($data){
			$this->db->insert('t_keuangan_pembebasan_siswa',$data);
	}

	function edit($data,$id=0){
			$this->db->where('id_keuangan_besaran',$id);
			$this->db->update('t_keuangan_besaran',$data);
	}

	function delete($id_keuangan_pembebasan_siswa){
			$this->db->where('id_keuangan_pembebasan_siswa', $id_keuangan_pembebasan_siswa);
			$this->db->delete('t_keuangan_pembebasan_siswa');
	}

}