<?php
class m_dspt_tunggakan extends MY_Model {
	
	public function get_data_search($id_sekolah,$id_tahun_ajaran,$id_rombel){
		$query = $this->db->query("SELECT * FROM t_keuangan_pembayaran_siswa as kps join v_siswa as s on s.id_siswa = kps.id_siswa where s.id_sekolah=$id_sekolah and s.id_tahun_ajaran = $id_tahun_ajaran and s.id_rombel=$id_rombel and kps.id_jenis_pembayaran=2 ");
		return $query->result_array();
	}
	
	public function get_data_tagihan($id_sekolah,$id_tahun_ajaran,$id_rombel){
		$query = $this->db->query("SELECT * FROM m_rombel as r join t_keuangan_besaran as tk on r.id_tingkat_kelas = tk.id_tingkat_kelas where  tk.id_sekolah=$id_sekolah and r.id_tahun_ajaran = $id_tahun_ajaran and r.id_rombel=$id_rombel and tk.id_jenis_pembayaran=2" );
		return $query->result_array();
	}

	function get_data($id_sekolah,$id_tahun_ajaran){
			$this->db->select('*');
			$this->db->from('t_keuangan_pembayaran_siswa kps');
			$this->db->join('v_siswa s','s.id_siswa = kps.id_siswa');
			$this->db->where('s.id_sekolah',$id_sekolah);
			$this->db->where('s.id_tahun_ajaran',$id_tahun_ajaran);
			
			return $this->db->get()->result_array();
	}
	
	function get_rombel($id_sekolah,$id_tahun_ajaran){
			$this->db->select('*');
			$this->db->from('m_rombel r');
			$this->db->where('r.id_sekolah',$id_sekolah);
			$this->db->where('r.id_tahun_ajaran',$id_tahun_ajaran);
			
			return $this->db->get()->result_array();
	}
	function get_siswa($id_sekolah,$id_tingkat_kelas){
			$this->db->select('*');
			$this->db->from('t_rombel_detail rd');
			$this->db->join('m_siswa s','s.id_siswa = rd.id_siswa');		
			$this->db->join('m_rombel r','r.id_rombel = rd.id_rombel');
			$this->db->where('s.id_sekolah',$id_sekolah);
			$this->db->where('r.id_tingkat_kelas',$id_tingkat_kelas);
			return $this->db->get()->result_array();
	}

	function get_data_edit($id_keuangan_pembayaran_siswa){
			$this->db->select('*');
			$this->db->from('t_keuangan_pembayaran_siswa kb');
			$this->db->join('m_siswa tk','tk.id_siswa = kb.id_siswa');
			$this->db->where('kb.id_keuangan_pembayaran_siswa',$id_keuangan_pembayaran_siswa);
			return $this->db->get()->result_array();
	}
	
	
	function select_kelas($jenjang_sekolah){
			$this->db->select('*');
			$this->db->from('m_tingkat_kelas');
			$this->db->where('id_jenjang_sekolah',$jenjang_sekolah);			
			return $this->db->get()->result_array();
	}

	function add($data){
			$this->db->insert('t_keuangan_pembayaran_siswa',$data);
	}

	function edit($data,$id=0){
			$this->db->where('id_keuangan_pembayaran_siswa',$id);
			$this->db->update('t_keuangan_pembayaran_siswa',$data);
	}

	function delete($id_keuangan_pembayaran_siswa){
			$this->db->where('id_keuangan_pembayaran_siswa', $id_keuangan_pembayaran_siswa);
			$this->db->delete('t_keuangan_pembayaran_siswa');
	}
	
	
	function select_siswa($id_sekolah,$id_tahun_ajaran,$id_rombel){
			$this->db->select('*');
			$this->db->from('v_siswa');
			$this->db->where('id_sekolah',$id_sekolah);			
			$this->db->where('id_tahun_ajaran',$id_tahun_ajaran);			
			$this->db->where('id_rombel',$id_rombel);			
			return $this->db->get()->result_array();
	}
	
	function get_rombel2($id_rombel){
			$this->db->select('*');
			$this->db->from('m_rombel r');
			$this->db->where('r.id_rombel',$id_rombel);
			
			return $this->db->get();
	}

}