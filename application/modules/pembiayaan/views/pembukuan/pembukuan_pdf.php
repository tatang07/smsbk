<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Pembukuan
            	
            </h2>
       </div>
      
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Tanggal</th>
								<th>Nama Transaksi</th>							
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; ?>
							<?php foreach($pembukuan as $k): ?>
								<tr>
									<td width='' class='center'><?php echo $no?></td>
									<td width='' class='center'><?php echo $k['tanggal_transaksi'] ?></td>
									<td width='' class='center'><?php echo $k['nama_transaksi'] ?></td>
									<td width='' class='center'><?php echo $k['jumlah_satuan'] ?></td>
								</tr>
							<?php $no++; ?>
							<?php endforeach; ?>
						</tbody>
					</table>
					<div class="footer">						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
