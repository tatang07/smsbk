<?php  
	$statuspil = 0;
	if(isset($_POST['id_tahun_ajaran'])){
	$a = $_POST['id_tahun_ajaran'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Pembukuan</h2>
			<?php //echo $statuspil ?>
       </div>
	
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pembiayaan/pembukuan/print_pdf/'.$statuspil); ?>"><i class="icon-print"></i>Cetak File</a>
			  	<a href="<?php echo base_url('pembiayaan/rekap_keluar_masuk'); ?>"><i class="icon"></i>Sirkulasi Keuangan</a> 
			  <br/><br/>
            </div>
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Tahun Ajaran</span></th>
							<td width=200px align='left'>
								<select name="id_tahun_ajaran" id="id_tahun_ajaran" onchange="submitform();">
									<option value="0">-Tahun Ajaran-</option>
									<?php foreach($ta as $q): ?>
										
										<?php if($q->id_tahun_ajaran == $statuspil){ ?>
											<option selected value='<?php echo $q->id_tahun_ajaran; ?>' data-status-pilihan="<?php echo $q->id_tahun_ajaran; ?>"><?php echo $q->tahun_awal; ?>-<?php echo $q->tahun_akhir; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q->id_tahun_ajaran; ?>' data-status-pilihan="<?php echo $q->id_tahun_ajaran; ?>"><?php echo $q->tahun_awal; ?>-<?php echo $q->tahun_akhir; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>           
        </div>
		<div class="header">
            <h2>Penerimaan</h2>			
       </div>
        <div class="content">
            <div id="datatable" url="">				
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Tanggal</th>
								<th >Uraian</th>
								<th >Jumlah</th>								
							</tr>
						</thead>
				
						<tbody>
							<?php 
								$this->load->model('pembiayaan/m_pembukuan');
								$id_sekolah = get_id_sekolah();
								$data['isi'] = $this->m_pembukuan->get_data($statuspil,$id_sekolah);
								$isi=$data['isi'];	
								$jumlah=0;		
							?>
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
								<?php //print_r($isi)?>
								<?php if ($d['kredit']==0){?>
								<tr>
									<td class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php echo $d['tanggal_transaksi']?>
									</td>
									<td width='' class='center'>
									<?php echo $d['nama_transaksi']?>
									</td class='center'>								
									<td class='center'>
									<?php echo number_format($d['harga_satuan'] * $d['jumlah_satuan'], 0, ',', '.') ?>
									</td class='center'>									
									<?php $jumlah=$jumlah+($d['harga_satuan'] * $d['jumlah_satuan'])?>
								</tr>
								<?php } ?>
							<?php endforeach;?>
						<td>Total</td>
						<td></td>
						<td></td>
						<td class='center'><?php echo number_format($jumlah, 0, ',', '.') ?></td>
						</tbody>
					</table>
					
				<div class="header">
					<h2>Pengeluaran</h2>			
			    </div>	
<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Tanggal</th>
								<th >Uraian</th>
								<th >Jumlah</th>								
							</tr>							
						</thead>				
						<tbody>						
							<?php 
								$this->load->model('pembiayaan/m_pembukuan');
								$id_sekolah=get_id_sekolah();
								$data['isi'] = $this->m_pembukuan->get_data($statuspil,$id_sekolah);
								$isi=$data['isi'];
								$jumlah=0;
							?>
							<?php $no=1; ?>							
							<?php foreach($isi as $d):?>								
							<?php if ($d['debit']==0){?>
								<tr>
									<td class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php echo $d['tanggal_transaksi']?>
									</td>
									<td width='' class='center'>
									<?php echo$d['nama_transaksi']?>
									</td class='center'>								
									<td class='center'>
									<?php echo number_format($d['harga_satuan'] * $d['jumlah_satuan'], 0, ',', '.')?>
									</td class='center'>									
									<?php $jumlah=$jumlah+($d['harga_satuan'] * $d['jumlah_satuan'])?>
								</tr>
							<?php } ?>	
							<?php endforeach;?>
							<td>Total</td>
							<td></td>
							<td></td>
							<td class='center'><?php echo number_format($jumlah, 0, ',', '.') ?></td>
						</tbody>						
					</table>
				</div>
            </div>
        </div>        
    </div>
</div>
<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>