<h1 class="grid_12">Pembiayaan</h1>
				<?php foreach($sub as $t){?>
					<?php $x = $t['id_sekolah_program_kerja']; ?>
	<form action="<?php echo base_url('pembiayaan/rapbs/submit/'.$id_sub.'/'.$x); ?>" method="post" enctype="multipart/form-data" class="grid_12">
		<fieldset>
			<legend>Rencana Anggaran Pendapatan dan Belanja Sekolah (RAPBS)</legend>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Program Kerja</strong>
						</label>
						<div>
							<input type="text" name="sub_program_kerja" value="<?php echo $t['sub_program_kerja'];?>" disabled/>
							<input type="hidden" name="id_sekolah_program_kerja" value="<?php echo $t['id_sekolah_program_kerja'];?>?>">
						</div>
					</div>
			
					<div class="row">
						<label for="f1_normal_input">
							<strong>Periode Awal</strong>
						</label>
						<div>
							<input type="date" name="periode_awal" value="<?php echo tanggal_view($t['tanggal_mulai']);?>" disabled/>
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Periode Akhir</strong>
						</label>
						<div>
							<input type="date" name="periode_akhir" value="<?php echo tanggal_view($t['tanggal_akhir']);?>" disabled/>
						
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Sumber Dana</strong>
						</label>
						<div>
							<input type="text" name="sumber_dana" value="<?php echo $t['sumber_dana'];?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Alokasi Dana</strong>
						</label>
						<div>
							<input type="text" name="alokasi_dana" value="<?php echo $t['alokasi_dana'];?>" disabled/>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Pendanaan</strong>
						</label>
						<div>
							<input type="text" name="pendanaan" value="<?php echo $t['pendanaan'];?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="<?php echo $t['keterangan'];?>" />
						</div>
					</div>
					
				<?php } ?>
		</fieldset>
		<div class="actions">
			<div class="left">
				<input type="submit" value="Simpan" name=send />
				 <a href="<?php echo base_url('pembiayaan/rapbs/load_detail/'.$id_sub); ?>"> <input value="Batal" type="button"></a>
			</div>
			<div class="right">
			</div>
		</div><!-- End of .actions -->
	</form><!-- End of .box -->
</div><!-- End of .grid_4 -->
