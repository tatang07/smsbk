<?php  
	$statuspil = 0;
	if(isset($_POST['id_tahun_ajaran'])){
	$a = $_POST['id_tahun_ajaran'];
	$statuspil = $a; } 
?>

<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rencana Anggaran Pendapatan dan Belanja Sekolah (RAPBS) </h2>
       </div>
       <div class="tabletools">
			 <div class="dataTables_filter">
                <form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Tahun Ajaran</span></th>
							<td width=200px align='left'>
								<select name="id_tahun_ajaran" id="id_tahun_ajaran" onchange="submitform();">
									<option value="0">-Tahun Ajaran-</option>
									<?php foreach($ta as $q): ?>
										
										<?php if($q->id_tahun_ajaran == $statuspil){ ?>
											<option selected value='<?php echo $q->id_tahun_ajaran; ?>' data-status-pilihan="<?php echo $q->id_tahun_ajaran; ?>"><?php echo $q->tahun_awal; ?>-<?php echo $q->tahun_akhir; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q->id_tahun_ajaran; ?>' data-status-pilihan="<?php echo $q->id_tahun_ajaran; ?>"><?php echo $q->tahun_awal; ?>-<?php echo $q->tahun_akhir; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
            </div>
        </div>

        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Program Kerja</th>
								<th>Periode</th>
								<th>Alokasi Dana</th>
								<th>Realisasi</th>
								<th>Keterangan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$this->load->model('pembiayaan/m_rapbs');
								$id_sekolah=get_id_sekolah();
								$data['rapbs'] = $this->m_rapbs->get_data($statuspil , $id_sekolah);

								$x = $this->m_rapbs->getJumlahAlokasi()->result_array();
								foreach($x as $alok){
									$alokasi = $alok['alokasi_dana'];
								}

								$y = $this->m_rapbs->getJumlahrealisasi()->result_array();
								foreach($y as $real){
									$realisasi = $real['pendanaan'];
								}

								$jum_realisasi = ($realisasi / $alokasi) * 100;
							?>

							<?php $i=1; ?>
							<?php foreach($data['rapbs'] as $r){ ?>
								<tr>
									<td class='center'> <?php echo $i; ?> </td>
									<td class='center'> <?php echo $r['program_kerja'] ?> </td>
									<td class='center'> <?php echo tanggal_indonesia($r['tanggal_mulai']).' - '.tanggal_indonesia($r['tanggal_akhir']) ?> </td>
									<td class='center'> <?php echo $alokasi; ?> </td>
									<td class='center'> <?php echo number_format($jum_realisasi, 2).' %'; ?> </td>
									<td class='center'> <?php echo $r['keterangan'] ?> </td>
									<td class='center'> 
										<a href="<?php echo site_url('pembiayaan/rapbs/load_detail/'.$r['id_sekolah_program_kerja'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-share-alt"></i> detail</a>
									</td>
								</tr>
							<?php $i++; ?>
							<?php } ?>
						</tbody>
					</table>
					<div class="footer">

					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>
