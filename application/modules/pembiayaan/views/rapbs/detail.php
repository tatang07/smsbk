<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rencana Anggaran Pendapatan dan Belanja Sekolah (RAPBS) </h2>
       </div>
       <div class="tabletools">
            <div class="dataTables_filter">
                <h2>Program Kerja     : <?php foreach($sub as $r) { echo $r['program_kerja'];} ?> </h2>
            </div>    
        </div>

        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Sub Program Kerja</th>
								<th>Periode</th>
								<th>Alokasi Dana</th>
								<th>Pendanaan</th>
								<th>Sumber Dana</th>
								<th>Keterangan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1; ?>
							<?php foreach($sub as $r){ ?>
								<tr>
									<td class='center'> <?php echo $i; ?> </td>
									<td class='center'> <?php echo $r['sub_program_kerja'] ?> </td>
									<td class='center'> <?php echo tanggal_view($r['tanggal_mulai']).' - '.tanggal_view($r['tanggal_akhir']) ?> </td>
									<td class='center'> <?php echo $r['alokasi_dana'] ?> </td>
									<td class='center'> <?php echo $r['pendanaan'] ?> </td>
									<td class='center'> <?php echo $r['sumber_dana'] ?> </td>
									<td class='center'> <?php echo $r['keterangan'] ?> </td>
									<td class='center'> 
										<a href="<?php echo site_url('pembiayaan/rapbs/edit_sub/'.$r['id_sekolah_sub_program_kerja'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-share-alt"></i> pendanaan</a>
									</td>
								</tr>
							<?php $i++; ?>
							<?php } ?>
						</tbody>
					</table>
					<div class="footer">

					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
