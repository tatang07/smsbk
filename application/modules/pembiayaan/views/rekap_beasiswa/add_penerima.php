<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Tambah Penerima Beasiswa</h2>
       </div>
		
		<form action="<?php echo base_url('pembiayaan/rekap_beasiswa/add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>			
				<div class="row">
					<label for="f1_normal_input">
						<strong>NIS</strong>
					</label>
					<div>
						<input id="nis" type="text" name="nis" value="" />
					</div>
				</div>
				<div class="row">
					<label for="f1_normal_input">
						<strong>Nama</strong>
					</label>
					<div>
						<input id="nama" disabled type="text" name="nama" value="" />
					</div>
				</div>
				<div class="row">
					<label for="f1_normal_input">
						<strong>Pemberi Beasiswa</strong>
					</label>
					<div>
						<select name="pemberi_beasiswa">
							<?php foreach($pemberi_beasiswa as $pb): ?>
								<option value='<?php echo $pb['id_pemberi_beasiswa']; ?>'><?php echo $pb['pemberi_beasiswa']?></option>
							<?php endforeach; ?>
						</select>
					</div>
				</div>
				<div class="row">
					<label for="f1_normal_input">
						<strong>Jumlah</strong>
					</label>
					<div>
						<input type="text" name="jumlah" value="" />
					</div>
				</div>
				<input type="hidden" name="id_tahun_ajaran" value="<?php echo $tahun_ajaran; ?>">	
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('pembiayaan/rekap_beasiswa'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>

 
<script>
$(document).ready(function(){
  $("#nis").on("keyup",function(){
    $.get("<?php echo site_url("pembiayaan/rekap_beasiswa/cekData")?>/" + $(this).val(),function(data,status){
      var a = JSON.parse(data);
	  if(a){
		console.log(a);
		$("#nama").val(a.nama);
	  }
	  
    });
  });
});
</script>