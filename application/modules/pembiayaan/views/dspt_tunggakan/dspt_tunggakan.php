<?php  
	$statuspil = 0;
	if(isset($_POST['id_rombel'])){
	$a = $_POST['id_rombel'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Tunggakan dspt</h2>
			<?php //echo $statuspil ?>
       </div>
	
       <div class="tabletools">
			<div class="right">
				<?php if($statuspil>0){?>
			  	<a href="<?php echo base_url('pembiayaan/dspt_tunggakan/load_add/'.$statuspil); ?>"><i class="icon-plus"></i>Tambah</a>
				<?php }?>
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('pembiayaan/dspt_tunggakan/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="110px" align="left"><span class="text">Kelas</span></th>
								<td width="200px" align="left">
								<select name="id_rombel" id="id_rombel" onchange="submitform();">
									
									<option value="0">-Pilih Kelas-</option>
									<?php foreach($rombel as $r):?>
									<?php if($statuspil==1){?>
									<option selected value="<?php echo $r['id_rombel'] ?>"><?php echo $r['rombel'] ?></option>
									<?php }else {?>
									<option value="<?php echo $r['id_rombel'] ?>"><?php echo $r['rombel'] ?></option>
									<?php } ?>
									<?php endforeach;?>
								</select>
								</td>
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
	
		<?php 
			$this->load->model('pembiayaan/m_dspt_tunggakan');
			$rombel=$this->m_dspt_tunggakan->get_rombel2($statuspil)->row();
			// print_r($rombel);
			
			// echo $rombel->id_tingkat_kelas;
		?>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Nis</th>
								<th >Nama</th>
								<th >Jumlah Tagihan</th>
								<th >Dibayar</th>
								<th >Tunggakan</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
							<?php if(isset($isi)){ ?>
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
							
								<tr>
									<td width='10px' class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['nis']?>
									</td class='center'>
									<td class='center'>
									<?php  echo $d['nama'];?>
									</td>
									
									<td class='center'>
									<?php foreach($tagihan as $t) {?>
										
										<?php if($t['id_tingkat_kelas']==$rombel->id_tingkat_kelas){?>
											<?php echo $t['besaran'];?>
										<?php }?>
									<?php }?>
									</td>
									<td class='center'>
									<?php  echo $d['jumlah_dibayar'];?>
									</td>
									<td class='center'>
									<?php  echo $d['tunggakan'];?>
									</td>
									<td width='100px' class='center'>
									<a href="<?php echo site_url('pembiayaan/dspt_tunggakan/load_edit/'.$d['id_keuangan_pembayaran_siswa'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a href="<?php echo site_url('pembiayaan/dspt_tunggakan/delete/'.$d['id_keuangan_pembayaran_siswa'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php endforeach;?>
							<?php } ?>
						</tbody>
					</table>
					
					<div class="footer">
						<?php if(isset($isi)){ ?>
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						<?php }else{?>
						<div class="dataTables_info">Jumlah Data : 0</div>
						<?php }?>
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>