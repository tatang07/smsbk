<?php  
	$statuspil = 0;
	if(isset($_POST['status'])){
	$a = $_POST['status'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Penentuan Besaran DSPB</h2>
			<?php //echo $statuspil ?>
       </div>
	
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pembiayaan/dspb_penentuan_besaran/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<!--<div class="dataTables_filter">
				<form action="<?php //echo base_url('pembiayaan/dspb_penentuan_besaran/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="40px" align="left"><span class="text">Nama</span></th>
								<td align="left"><input type="text" name="nama" id="nama"></td>
								<td width="10px"></td>
								<th width="110px" align="left"><span class="text">Status</span></th>
								<td width="200px" align="left">
								<select name="status" id="status" onchange="submitform();">
									<option value="0">-Status-</option>
									<?php //if($statuspil==1){?>
									<option selected value="1">Lulus</option>
									<?php //}else {?>
									<option value="1">Lulus</option>
									<?php //} ?>
									<?php //if($statuspil==2){?>
									<option selected value="2">Belum Lulus</option>
									<?php //}else {?>
									<option value="2">Belum Lulus</option>
									<?php //} ?>
									<?php //if($statuspil==3){?>
									<option selected value="3">Gagal</option>
									<?php //}else {?>
									<option value="3">Gagal</option>
									<?php //} ?>

								</select>
								</td>
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>-->

            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Tingkat</th>
								<th >Besaran</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
						
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
							
								<tr>
									<td width='10px' class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['tingkat_kelas']?>
									</td class='center'>
									<td class='center'>
									<?php  echo $d['besaran'];?>
									</td>
									
									<td width='100px' class='center'>
									<a href="<?php echo site_url('pembiayaan/dspb_penentuan_besaran/load_edit/'.$d['id_keuangan_besaran'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a href="<?php echo site_url('pembiayaan/dspb_penentuan_besaran/delete/'.$d['id_keuangan_besaran'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>