<?php
	if(isset($_POST['id_tahun_ajaran'])){
		$a = $_POST['id_tahun_ajaran'];
		$statuspil = $a;
	}else{
		$statuspil = 1;
	}
?>

<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Grafik Penerima Beasiswa Tahun Ajaran
	       		<?php foreach($tahun_ajaran as $ta): ?>
					<?php if($ta['id_tahun_ajaran'] == $statuspil){ ?>
	            		<?php echo $ta['tahun_awal'].'/'.$ta['tahun_akhir']; ?>	
					<?php } ?>
				<?php endforeach; ?>
			</h2>
       </div>
	   
	   <div class="tabletools">
       		<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Tahun Ajaran</span></th>
							<td width=200px align='left'>
								<select name="id_tahun_ajaran" id="id_tahun_ajaran" onchange="submitform();">
									<?php foreach($tahun_ajaran as $ta): ?>
										<?php if($ta['id_tahun_ajaran'] == $statuspil){ ?>
											<option selected value='<?php echo $ta['id_tahun_ajaran']; ?>' data-status-pilihan="<?php echo $ta['id_tahun_ajaran']; ?>"><?php echo $ta['tahun_awal'].'/'.$ta['tahun_akhir']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $ta['id_tahun_ajaran']; ?>' data-status-pilihan="<?php echo $ta['id_tahun_ajaran']; ?>"><?php echo $ta['tahun_awal'].'/'.$ta['tahun_akhir']; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
        </div>
	   
	   <?php
			$this->load->model('pembiayaan/t_keuangan_beasiswa');
			$id_tahun_ajaran = $statuspil;
			$id_sekolah = get_id_sekolah();
			$data['jumlah'] = $this->t_keuangan_beasiswa->hitungJumlah($id_tahun_ajaran);
			$data['pemberi'] = $this->t_keuangan_beasiswa->getPemberiBeasiswa()->result_array();
		?>
		
	   <div class="box">
				
					<div class="content" style="height: 350px;">
						<table class=chart data-type=bars>
							<thead>
								<tr>
									<th></th>
									<?php foreach ($data['pemberi'] as $pemberi):
										echo "<th>".$pemberi['pemberi_beasiswa']."</th>";
									endforeach; ?>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th>Jumlah Siswa</th>
									<?php
										foreach ($data['jumlah'] as $jumlah):
											echo "<td>".$jumlah."</td>";
										endforeach;
									?>
								</tr>
							</tbody>	
						</table>
					</div><!-- End of .content -->
					
				</div><!-- End of .box -->		
        <div class="content">
		
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<td rowspan=2 class='center' width='30px'><strong>No.</strong></td>
								<td rowspan=2><strong>Sumber</strong></td>
								<td rowspan=2 class='center' width='250px'><strong>Jumlah Siswa</strong></td>
							</tr>
						</thead>
						<tbody>
							<?php $no=1;?>
							<?php foreach ($data['pemberi'] as $pemberi):
										echo "<tr>";
											echo "<td width='30px' class='center'>".$no."</td>";
											echo "<td width='50px'>".$pemberi['pemberi_beasiswa']."</td>";
											echo "<td width='50px' class='center'>".$data['jumlah'][$no-1]."</td>";
										echo "</tr>";
										$no++;
								  endforeach; ?>
							<?php if($no==1){
								echo "<tr>";
									echo "<td colspan='3'>Tidak ada data!";
									echo "</td>";
								echo "</tr>";
							}?>
						</tbody>
					</table>
					
				</div>
            </div>
        </div>        
    </div>
 </div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>