<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekap Tunggakan DSPB</h2>
       </div>
		<div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pembiayaan/rekap_tunggakan_dspb/print_pdf/'.$first_rombel); ?>"><i class="icon-print"></i>Cetak File</a> 
			  	<a href="<?php echo base_url('pembiayaan/rekap_tunggakan_dspb/tambah/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
				<form action="search" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=50px align='left'><span class="text">Kelas</span></th>
							<td width=200px align='left'>
								<select name="rombel">
									<option value=''>-- Pilih Kelas --</option>
								<?php foreach($rombel as $r): ?>
									<?php if($r->id_rombel == $first_rombel ){ ?>
										<option selected value='<?php echo $r->id_rombel; ?>'><?php echo $r->rombel; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $r->id_rombel; ?>'><?php echo $r->rombel; ?></option>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
							<input type="submit" name=send class="button grey tooltip" value="cari" />
							<!-- <a href="search" class="button grey tooltip"><i class='icon-search'></i></a> -->
							</td>
						</tr>
					</table>
				</form>
			</div>   
		</div>
		
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">
				<?php if($first_rombel !=0){ ?>
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Jan</th>
								<th>Feb</th>
								<th>Mar</th>
								<th>Apr</th>
								<th>Mei</th>
								<th>Jun</th>
								<th>Jul</th>
								<th>Aug</th>
								<th>Sep</th>
								<th>Okt</th>
								<th>Nov</th>
								<th>Des</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1;?>
							<?php $temp = array('0','0','0','0','0','0','0','0','0','0','0','0',); ?>
							<?php if($siswa){ ?>
								<?php foreach($siswa as $s): ?>
								<?php if($rekap){ ?>
								<?php $j=0; foreach($rekap as $r): ?>
									<?php 
									if($s->id_siswa == $r->id_siswa){ ?>
									<?php 
										$temp[$r->bulan-1] = $r->bulan;
										$j++;
									} ?>
								<?php endforeach; } ?>
									<tr>
										<td width='50px' class='center'><?php echo $no; ?></td>
										<td width='100px'><?php echo $s->nis; ?></td>
										<td width='300px'><?php echo $s->nama; ?></td>
										<?php for($i=0;$i<12;$i++){ ?>
											<?php if($temp[$i] == $i+1){ ?>
												<td width='30px' class="lunas">L</td>
											<?php }else{ ?>
												<td width='30px' class="tunggak">T</td>
											<?php } ?>
										<?php } ?>
									</tr>
									<?php $no++; ?>
								<?php endforeach; ?>
							<?php } ?>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $no-1; ?></div>
					</div>
					<?php }else{ ?>
						<div class="footer">
							<div class="dataTables_info">Pilih Kelas yang Ingin Dicari</div>
						</div>
					<?php } ?>
				</div>
            </div>
        </div>        
    </div>
 </div>
