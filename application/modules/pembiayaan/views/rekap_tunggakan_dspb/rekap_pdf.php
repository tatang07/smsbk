<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekap Tunggakan DSPB</h2>
       </div>
		
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">
				<?php if($first_rombel !=0){ ?>
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Jan</th>
								<th>Feb</th>
								<th>Mar</th>
								<th>Apr</th>
								<th>Mei</th>
								<th>Jun</th>
								<th>Jul</th>
								<th>Aug</th>
								<th>Sep</th>
								<th>Okt</th>
								<th>Nov</th>
								<th>Des</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1;?>
							<?php if($siswa){ ?>
								<?php foreach($siswa as $s): ?>
								<?php if($rekap){ ?>
								<?php $temp = array('0','0','0','0','0','0','0','0','0','0','0','0',); $j=0; foreach($rekap as $r): ?>
									<?php 
									if($s->id_siswa == $r->id_siswa){ ?>
									<?php 
										$temp[$r->bulan-1] = $r->bulan;
										$j++;
									} ?>
								<?php endforeach; } ?>
									<tr>
										<td width='50px' class='center'><?php echo $no; ?></td>
										<td width='100px'><?php echo $s->nis; ?></td>
										<td width='300px'><?php echo $s->nama; ?></td>
										<?php for($i=0;$i<12;$i++){ ?>
											<?php if($temp[$i] == $i+1){ ?>
												<td width='30px' class="lunas">L</td>
											<?php }else{ ?>
												<td width='30px' class="tunggak">T</td>
											<?php } ?>
										<?php } ?>
									</tr>
									<?php $no++; ?>
								<?php endforeach; ?>
							<?php } ?>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $no-1; ?></div>
					</div>
					<?php }else{ ?>
						<div class="footer">
							<div class="dataTables_info">Pilih Kelas yang Ingin Dicari</div>
						</div>
					<?php } ?>
				</div>
            </div>
        </div>        
    </div>
 </div>
