	<h1 class="grid_12">Pembiayaan</h1>
			
			<form action="<?php echo base_url('pembiayaan/rekap_tunggakan_dspb/submit_post'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Rekap Tunggakan DSPB</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>NIS</strong>
						</label>
						<div>
							<input type="text" name="nis" value="" />
							<input type="hidden" name="id_jenis_pembayaran" value="<?php echo '1'; ?>" />
							<input type="hidden" name="action" value="add" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Tahun Ajaran</strong>
						</label>
						<div>
							<select name="id_tahun_ajaran" data-placeholder="pilih tahun ajaran">
							<?php foreach($tahun_ajaran as $t): ?>
									<option value="<?php echo $t->id_tahun_ajaran; ?>"><?php echo $t->tahun_awal.'/'.$t->tahun_akhir; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Tanggal</strong>
						</label>
						<div>
							<input type="date" name="tanggal" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Bulan</strong>
						</label>
						<div>
							<select name="bulan" data-placeholder="pilih bulan">
								<option value="1">Januari</option>
								<option value="2">Februari</option>
								<option value="3">Maret</option>
								<option value="4">April</option>
								<option value="5">Mei</option>
								<option value="6">Juni</option>
								<option value="7">Juli</option>
								<option value="8">Agustus</option>
								<option value="9">September</option>
								<option value="10">Oktober</option>
								<option value="11">November</option>
								<option value="12">Desember</option>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Jumlah Dibayar</strong>
						</label>
						<div>
							<input type="text" name="jumlah_dibayar" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Tunggakan</strong>
						</label>
						<div>
							<input type="text" name="tunggakan" value="" />
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pembiayaan/rekap_tunggakan_dspb/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
