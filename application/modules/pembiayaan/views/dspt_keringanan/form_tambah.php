	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			
			<form action="<?php echo base_url('pembiayaan/dspt_keringanan/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Keringanan DSPT</legend><?php //echo($tingkat_kelas);?>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama</strong>
						</label>
						<div>
							<select name="id_siswa" id="id_siswa" >
							<option value="0">-Pilih Siswa-</option>
							<?php foreach($siswa as $d):?>
							
							  <option value="<?php echo $d['id_siswa']?>"><?php echo $d['nama']?></option>
							
							 <?php endforeach ?>
							</select> 
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Besaran Keringanan</strong>
						</label>
						<div>
							<input type="text" name="jumlah" value="" />
							<input type="hidden" name="id_jenis_pembayaran" value="2" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Bulan</strong>
						</label>
						<div>
							<select name="bulan">
								<option value="0"> -Pilih Bulan- </option>
								<option value="1"> Januari </option>
								<option value="2"> Febuari </option>
								<option value="3"> Maret </option>
								<option value="4"> April </option>
								<option value="5"> Mei </option>
								<option value="6"> Juni </option>
								<option value="7"> Juli </option>
								<option value="8"> Agustus </option>
								<option value="9"> September </option>
								<option value="10"> Oktober </option>
								<option value="11"> November </option>
								<option value="12"> Desember </option>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="" />
						</div>
					</div>
					
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pembiayaan/dspt_keringanan/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		