	<h1 class="grid_12">Pembiayaan</h1>
			
			<form action="<?php echo base_url('pembiayaan/rekap_penerima_keringanan_pembayaran/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Program Kerja Tahun Ajaran <?php foreach($tabi as $t):?> <?php echo $t->tahun_awal.'-'.$t->tahun_akhir; ?> <?php endforeach;?></legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Siswa</strong>
						</label>
						<div>
						
							<input type="hidden" name="id_tahun_ajaran" value="<?php echo $id_tahun_ajaran?>" />
							 <select name="nama">
								<?php foreach ($siswa as $s):?>
								<option value="<?php echo $s['id_siswa']; ?>"><?php echo $s['nama'] ?></option>
								<?php endforeach;?>
							</select> 
						</div>
					</div>
					<?php //print_r($ta)?>
			
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenis Pembayaran</strong>
						</label>
						<div>
							<select name="jenis_pembayaran">
								<?php foreach ($jenis_pembayaran as $jp):?>
								<option value="<?php echo $jp['id_jenis_pembayaran']; ?>"><?php echo $jp['jenis_pembayaran'] ?></option>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Bulan</strong>
						</label>
						<div>
							 <select name ="bulan">
							  <option value="1">Januari</option>
							  <option value="2">Febuari</option>
							  <option value="3">Maret</option>
							  <option value="4">April</option>
							  <option value="5">Mei</option>
							  <option value="6">Juni</option>
							  <option value="7">Juli</option>
							  <option value="8">Agustus</option>
							  <option value="9">September</option>
							  <option value="10">Oktober</option>
							  <option value="11">November</option>
							  <option value="12">Agustus</option>
							</select> 
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jumlah</strong>
						</label>
						<div>
							<input type="text" name="jumlah" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="" />
						</div>
					</div>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pembiayaan/rekap_penerima_keringanan_pembayaran/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		
<script>
$(function(){
	$(document).on('change', '#kelompok_pelajaran', function(){
		var id_kelompok_pelajaran = $(this).val();
		$.post("<?php echo site_url('kurikulum/silabus/get_pelajaran'); ?>/" + id_kelompok_pelajaran, function(data){
			$("#pelajaran").html(data);
			$("#pelajaran").trigger("chosen:updated");
		});
	});
});
</script>