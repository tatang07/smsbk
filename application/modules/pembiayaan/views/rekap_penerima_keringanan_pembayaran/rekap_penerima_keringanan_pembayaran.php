<?php  
	$statuspil = 0;
	if(isset($_POST['id_tahun_ajaran'])){
	$a = $_POST['id_tahun_ajaran'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekap Penerima Keringanan Pembiayaan</h2>
			<?php //echo $statuspil ?>
       </div>
	
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pembiayaan/rekap_penerima_keringanan_pembayaran/load_add/'.$statuspil); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Tahun Ajaran</span></th>
							<td width=200px align='left'>
								<select name="id_tahun_ajaran" id="id_tahun_ajaran" onchange="submitform();">
									<option value="0">-Tahun Ajaran-</option>
									<?php foreach($ta as $q): ?>
										
										<?php if($q->id_tahun_ajaran == $statuspil){ ?>
											<option selected value='<?php echo $q->id_tahun_ajaran; ?>' data-status-pilihan="<?php echo $q->id_tahun_ajaran; ?>"><?php echo $q->tahun_awal; ?>-<?php echo $q->tahun_akhir; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q->id_tahun_ajaran; ?>' data-status-pilihan="<?php echo $q->id_tahun_ajaran; ?>"><?php echo $q->tahun_awal; ?>-<?php echo $q->tahun_akhir; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Bulan</th>
								<th >NIS</th>
								<th >Nama</th>
								<th >Jenis</th>
								<th >Jumlah</th>
								<th >Keterangan</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
							<?php 
								$this->load->model('kepemimpinan/m_rekap_penerima_keringanan_pembayaran');
								$id_sekolah=get_id_sekolah();
								$data['isi'] = $this->m_rekap_penerima_keringanan_pembayaran->get_data($statuspil,$id_sekolah);
								$isi=$data['isi'];
							?>
							<?php $no=1; ?>
			
							<?php foreach($isi as $d):?>
								<tr>
									<td class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['bulan']?>
									</td class='center'>
									<td class='center'>
									<?php  echo $d['nis']?>
									</td>
									<td class='center'>
									<?php  echo $d['nama']?>
									</td>
									<td class='center'>
									<?php  echo $d['jenis_pembayaran']?>
									</td>
									<td class='center'>
									<?php  echo $d['jumlah']?>
									</td>
									<td class='center'>
									<?php  echo $d['keterangan'];?>
									</td>
									<td class='center'>
									<a href="<?php echo site_url('pembiayaan/rekap_penerima_keringanan_pembayaran/load_edit/'.$statuspil.'/'.$d['id_keuangan_keringan_siswa'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
									<a href="<?php echo site_url('pembiayaan/rekap_penerima_keringanan_pembayaran/delete/'.$d['id_keuangan_keringan_siswa'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>