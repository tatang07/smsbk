	<h1 class="grid_12">Pembiayaan</h1>

			<form action="<?php echo base_url('pembiayaan/rekap_penerima_keringanan_pembayaran/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Program Kerja Tahun Ajaran <?php foreach($tabi as $t):?> <?php echo $t->tahun_awal.'-'.$t->tahun_akhir; ?> <?php endforeach;?></legend>
				<?php foreach($data_edit as $t){?>

					<div class="row">
						<label for="f1_normal_input">
							<strong>Siswa</strong>
						</label>
						<div>
						
							<input type="hidden" name="id_tahun_ajaran" value="<?php echo $id2; ?>" />
							<input type="hidden" name="id" value="<?php echo $id; ?>" />
							 <select name="nama">
								<?php foreach ($siswa as $s):?>
									<?php if ($t['id_siswa']==$s['id_siswa']){?>
									<option selected value="<?php echo $s['id_siswa']; ?>"><?php echo $s['nama'] ?></option>
									<?php }else{ ?>
									<option value="<?php echo $s['id_siswa']; ?>"><?php echo $s['nama'] ?></option>
									<?php } ?>
								<?php endforeach;?>
							</select> 
						</div>
					</div>
			<div class="row">
						<label for="f1_normal_input">
							<strong>Jenis Pembayaran</strong>
						</label>
						<div>
							<select name="jenis_pembayaran">
								<?php foreach ($jenis_pembayaran as $jp):?>
									<?php if($t['id_jenis_pembayaran']==$jp['id_jenis_pembayaran']){?>
									<option selected value="<?php echo $jp['id_jenis_pembayaran']; ?>"><?php echo $jp['jenis_pembayaran'] ?></option>
									<?php }else{?>
									<option value="<?php echo $jp['id_jenis_pembayaran']; ?>"><?php echo $jp['jenis_pembayaran'] ?></option>
									<?php }?>
								<?php endforeach;?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Bulan</strong>
						</label>
						<div>
							 <select name ="bulan">
							 <?php if($t['bulan']==1){?>
							  <option selected value="1">Januari</option>
							 <?php}else {?>
							 <option value="1">Januari</option>
							 <?php }?>
							   <?php if($t['bulan']==2){?>
							  <option selected value="2">Febuari</option>
							 <?php}else {?>
							 <option value="2">Febuari</option>
							 <?php }?>
							  <?php if($t['bulan']==3){?>
							  <option selected value="3">Maret</option>
							 <?php}else {?>
							 <option value="3">Maret</option>
							 <?php }?>
							  <?php if($t['bulan']==4){?>
							  <option selected value="4">April</option>
							 <?php}else {?>
							 <option value="4">April</option>
							 <?php }?>
							  <?php if($t['bulan']==5){?>
							  <option selected value="5">Mei</option>
							 <?php}else {?>
							 <option value="5">Mei</option>
							 <?php }?>
							  <?php if($t['bulan']==6){?>
							  <option selected value="6">Juni</option>
							 <?php}else {?>
							 <option value="6">Juni</option>
							 <?php }?>
							  <?php if($t['bulan']==7){?>
							  <option selected value="7">Juli</option>
							 <?php}else {?>
							 <option value="7">Juli</option>
							 <?php }?>
							  <?php if($t['bulan']==8){?>
							  <option selected value="8">Agustus</option>
							 <?php}else {?>
							 <option value="8">Agustus</option>
							 <?php }?>
							  <?php if($t['bulan']==9){?>
							  <option selected value="9">September</option>
							 <?php}else {?>
							 <option value="9">September</option>
							 <?php }?>
							  <?php if($t['bulan']==10){?>
							  <option selected value="10">Oktober</option>
							 <?php}else {?>
							 <option value="10">Oktober</option>
							 <?php }?>
							  <?php if($t['bulan']==11){?>
							  <option selected value="11">November</option>
							 <?php}else {?>
							 <option value="11">November</option>
							 <?php }?>
							  <?php if($t['bulan']==12){?>
							  <option selected value="12">Desember</option>
							 <?php}else {?>
							 <option value="12">Desember</option>
							 <?php }?>
							</select> 
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jumlah</strong>
						</label>
						<div>
							<input type="text" name="jumlah" value="<?php echo $t['jumlah']; ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="<?php echo $t['keterangan'];?>" />
						</div>
					</div>
					
				<?php } ?>
				</fieldset>
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pembiayaan/rekap_penerima_keringanan_pembayaran/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		