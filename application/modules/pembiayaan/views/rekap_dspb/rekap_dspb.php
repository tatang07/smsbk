<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekapitulasi Pembayaran DSPB</h2>
       </div>
	
       <div class="tabletools">
			<div class="right">
				<a href="<?php echo base_url('pembiayaan/rekap_dspb/print_pdf/'); ?>"><i class="icon-print"></i>Cetak</a>
			  	<a href="<?php echo base_url('pembiayaan/rekap_dspb/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>

       </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Bulan</th>
								<th >Jumlah</th>
							</tr>
						</thead>
				
						<tbody>

							<?php $no=1; ?>
			
							<?php foreach($isi as $d):?>
								<tr>
									<td class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php if($d['bulan']==1){?>
										<?php echo 'Januari'?>
									<?php }elseif($d['bulan']==2){?>
										<?php echo 'Febuari'?>
									<?php }elseif($d['bulan']==3){?>
										<?php echo 'Maret'?>
									<?php }elseif($d['bulan']==4){?>
										<?php echo 'April'?>
									<?php }elseif($d['bulan']==5){?>
										<?php echo 'Mei'?>
									<?php }elseif($d['bulan']==6){?>
										<?php echo 'Juni'?>
									<?php }elseif($d['bulan']==7){?>
										<?php echo 'Juli'?>	
									<?php }elseif($d['bulan']==8){?>
										<?php echo 'Agustus'?>
									<?php }elseif($d['bulan']==9){?>
										<?php echo 'September'?>
									<?php }elseif($d['bulan']==10){?>
										<?php echo 'Oktober'?>
									<?php }elseif($d['bulan']==11){?>
										<?php echo 'November'?>
									<?php }else{?>
										<?php echo 'Desember'?>	
									<?php } ?>
									</td>
									<td class='center'>
									<?php  echo $d['jumlah']?>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>