	<h1 class="grid_12">Pembiayaan</h1>
			
			<form action="<?php echo base_url('pembiayaan/rekap_dspb/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Program Kerja Tahun Ajaran</legend>
					
			
					<div class="row">
						<label for="f1_normal_input">
							<strong>NIM</strong>
						</label>
						<div>
							<input type="text" name="nim" value="" id="nim" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>NAMA</strong>
						</label>
						<div>
							<input disabled type="text" value="" id="nama" />
							<input type="hidden" value="" id="id_siswa" name="id_siswa"/>
						</div>
					</div>
				
					<div class="row">
						<label for="f1_normal_input">
							<strong>Bulan</strong>
						</label>
						<div>
							 <select name ="bulan">
							  <option value="1">Januari</option>
							  <option value="2">Febuari</option>
							  <option value="3">Maret</option>
							  <option value="4">April</option>
							  <option value="5">Mei</option>
							  <option value="6">Juni</option>
							  <option value="7">Juli</option>
							  <option value="8">Agustus</option>
							  <option value="9">September</option>
							  <option value="10">Oktober</option>
							  <option value="11">November</option>
							  <option value="12">Agustus</option>
							</select> 
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal</strong>
						</label>
						<div>
							<input type="date" name="tanggal" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jumlah Dibayar</strong>
						</label>
						<div>
							<input type="text" name="jumlah" value="" />
						</div>
					</div>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pembiayaan/rekap_penerima_keringanan_pembayaran/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		
<script>
$(document).ready(function(){
  $("#nim").on("keyup",function(){
    $.get("<?php echo site_url("pembiayaan/rekap_dspb/cekData")?>/" + $(this).val(),function(data,status){
      var a = JSON.parse(data);
	  if(a){
		console.log(a);
		$("#nama").val(a.nama);
		$("#id_siswa").val(a.id_siswa);
	  }
	  
    });
  });
});
</script>