<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Sumber Dana</h2>
		</div>
		<div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pembiayaan/sumber_dana/tambah/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kode</th>
								<th>Sumber Dana</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						
							<?php if($list){ ?>
								<?php $no=1; foreach($list as $l): ?>
									
										<tr>
											<td width="50px" class="center"><?php echo $no; ?></td>
											<td width="300px" class="center"><?php echo $l->kode_bos_sumber_dana; ?></td>
											<td width="300px" class="center"><?php echo $l->bos_sumber_dana; ?></td>
											<td width="150px" class="center">
												<a href="<?php echo base_url('pembiayaan/sumber_dana/edit/'.$l->id_bos_sumber_dana); ?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-edit"></i> Edit</a>
												<a href="<?php echo base_url('pembiayaan/sumber_dana/delete/'.$l->id_bos_sumber_dana); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
										<?php $no++; ?>
								<?php endforeach; ?>
							
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $jumlah; ?></div>
					</div>
							<?php }else{ ?>
								</tbody>
								</table>
								<div class="footer">Data tidak ditemukan!
								</div>
							<?php } ?>
				</div>
			</div>
		</div>        
	</div>
</div>