	<h1 class="grid_12">Pembiayaan</h1>
			
			<form action="<?php echo base_url('pembiayaan/sumber_dana/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Sumber Dana</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kode Sumber Dana</strong>
						</label>
						<div>
							<input type="text" name="kode" value="<?php echo $kode; ?>" />
							<input type="hidden" name="id" value="<?php echo $id; ?>" />
							<input type="hidden" name="action" value="add" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Nama Sumber Dana</strong>
						</label>
						<div>
							<input type="text" name="nama" value="<?php echo $nama; ?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Induk Sumber Dana</strong>
						</label>
						<div>
							<select name="id_parent" id="id_parent">
								<option value=0>-- Induk Sumber Dana --</option>
							<?php foreach($sumber_dana as $t): ?>
								<?php if($t->id_bos_sumber_dana == $id_parent){ ?>
									<option selected value="<?php echo $t->id_bos_sumber_dana; ?>"><?php echo $t->kode_bos_sumber_dana.' - '.$t->bos_sumber_dana; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $t->id_bos_sumber_dana; ?>"><?php echo $t->kode_bos_sumber_dana.' - '.$t->bos_sumber_dana; ?></option>
								<?php } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pembiayaan/sumber_dana/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		
<script>
// $(function(){
	// $(document).on('change', '#rombel', function(){
		// var id_rombel = $(this).val();
		// $.post("<?php echo site_url('pembiayaan/sumber_dana/get_mapel_guru'); ?>/" + id_rombel, function(data){
			// $("#pelajaran").html(data);
			// $("#pelajaran").trigger("chosen:updated");
		// });
	// });

	// // $(document).on('change', '#pelajaran', function(){
		// // var id_pelajaran = $(this).val();
		// // $.post("<?php echo site_url('pembiayaan/sumber_dana/get_mapel_rombel'); ?>/" + id_pelajaran, function(data){
			// // $("#rombel").html(data);
			// // $("#rombel").trigger("chosen:updated");
		// // });
	// // });
// });
</script>