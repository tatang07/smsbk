	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			<?php $id=$this->uri->segment(4)?>
			<form action="<?php echo base_url('pembiayaan/dspb_tunggakan/submit_edit/'.$id); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Edit Keringanan DSPT</legend><?php //echo($tingkat_kelas);?>
					<?php foreach($data_edit as $d):?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama</strong>
						</label>
						<div>
							<input disabled type="text" name="id_siswa" value="<?php echo $d['nama']?>" />
						
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jumlah Dibayar</strong>
						</label>
						<div>
							<input type="text" name="jumlah_dibayar" value="<?php echo $d['jumlah_dibayar'] ?>" />
							<input type="hidden" name="id_jenis_pembayaran" value="1" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Bulan</strong>
						</label>
						<div>
							<select name="bulan">
								<option value="0"> -Pilih Bulan- </option>
								<?php if($d['bulan']==1){?>
								<option selected value="1"> Januari </option>
								<?php }else{?>
								<option value="1"> Januari </option>
								<?php }?><?php if($d['bulan']==2){?>
								<option selected value="2"> Febuari </option>
								<?php }else{?>
								<option value="2"> Febuari </option>
								<?php }?><?php if($d['bulan']==3){?>
								<option selected value="3"> Maret </option>
								<?php }else{?>
								<option value="3"> Maret </option>
								<?php }?><?php if($d['bulan']==4){?>
								<option selected value="4"> April </option>
								<?php }else{?>
								<option value="4"> April </option>
								<?php }?><?php if($d['bulan']==5){?>
								<option selected value="5"> Mei </option>
								<?php }else{?>
								<option value="5"> Mei </option>
								<?php }?><?php if($d['bulan']==6){?>
								<option selected value="6"> Juni </option>
								<?php }else{?>
								<option value="6"> Juni </option>
								<?php }?><?php if($d['bulan']==7){?>
								<option selected value="7"> Juli </option>
								<?php }else{?>
								<option value="7"> Juli </option>
								<?php }?><?php if($d['bulan']==8){?>
								<option selected value="8"> Agustus </option>
								<?php }else{?>
								<option value="8"> Agustus </option>
								<?php }?><?php if($d['bulan']==9){?>
								<option selected value="9"> September </option>
								<?php }else{?>
								<option value="9"> September </option>
								<?php }?><?php if($d['bulan']==10){?>
								<option selected value="10"> Oktober </option>
								<?php }else{?>
								<option value="10"> Oktober </option>
								<?php }?><?php if($d['bulan']==11){?>
								<option selected value="11"> November </option>
								<?php }else{?>
								<option value="11"> November </option>
								<?php }?><?php if($d['bulan']==12){?>
								<option selected value="12"> Desember </option>
								<?php }else{?>
								<option value="12"> Desember </option>
								<?php }?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal</strong>
						</label>
						<div>
							<input type="date" name="tanggal" value="<?php echo $d['tanggal']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tunggakan</strong>
						</label>
						<div>
							<input type="text" name="tunggakan" value="<?php echo $d['tunggakan']?>" />
						</div>
					</div>
				<?php endforeach ?>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pembiayaan/dspb_tunggakan/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		