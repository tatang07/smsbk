<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Data Rekap Sirkulasi Keuangan Tahun Ajaran
            	<?php foreach($tahun_ajaran as $ta): ?>
					<?php if($ta['id_tahun_ajaran'] == $statuspil){ ?>
	            		<?php echo $ta['tahun_awal'].'/'.$ta['tahun_akhir']; ?>	
					<?php } ?>
				<?php endforeach; ?>
            </h2>
       </div>
      
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Transaksi</th>
								<th>Penanggung Jawab</th>
								<th>Debit</th>
								<th>Kredit</th>
								<th>Keterangan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; ?>
							<?php foreach($keuangan as $k): ?>
								<tr>
									<td width='' class='center'><?php echo $no?></td>
									<td width='' class='center'><?php echo $k['nama_transaksi'] ?></td>
									<td width='' class='center'><?php echo $k['penanggung_jawab'] ?></td>
									<td width='' class='center'><?php echo $k['debit'] ?></td>
									<td width='' class='center'><?php echo $k['kredit'] ?></td>
									<td width='' class='center'><?php echo $k['keterangan'] ?></td>	
								</tr>
							<?php $no++; ?>
							<?php endforeach; ?>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Data Tidak Ditemukan!</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
