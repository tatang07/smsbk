<?php  
	if(isset($_POST['id_tahun_ajaran'])){
		$a = $_POST['id_tahun_ajaran'];
		$statuspil = $a;
	}else{
		$statuspil = get_id_tahun_ajaran();
	}
?>
<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Data Rekap Sirkulasi Keuangan Tahun Ajaran
	       		<?php foreach($tahun_ajaran as $ta): ?>
					<?php if($ta['id_tahun_ajaran'] == $statuspil){ ?>
	            		<?php echo $ta['tahun_awal'].'/'.$ta['tahun_akhir']; ?>	
					<?php } ?>
				<?php endforeach; ?>
			</h2>
       </div>
	
       <div class="tabletools">
       		<div class="right">
			  	<a href="<?php echo base_url('pembiayaan/rekap_keluar_masuk/print_pdf/'.$statuspil); ?>"><i class="icon-print"></i>Cetak</a> 
			  	<a href="<?php echo base_url('pembiayaan/rekap_keluar_masuk/load_add/'.$statuspil); ?>"><i class="icon-plus"></i>Tambah Sirkulasi Keuangan</a> 
			  <br><br>
            </div>

			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Tahun Ajaran</span></th>
							<td width=200px align='left'>
								<select name="id_tahun_ajaran" id="id_tahun_ajaran" onchange="submitform();">
									<?php foreach($tahun_ajaran as $ta): ?>
										<?php if($ta['id_tahun_ajaran'] == $statuspil){ ?>
											<option selected value='<?php echo $ta['id_tahun_ajaran']; ?>' data-status-pilihan="<?php echo $ta['id_tahun_ajaran']; ?>"><?php echo $ta['tahun_awal'].'/'.$ta['tahun_akhir']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $ta['id_tahun_ajaran']; ?>' data-status-pilihan="<?php echo $ta['id_tahun_ajaran']; ?>"><?php echo $ta['tahun_awal'].'/'.$ta['tahun_akhir']; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
        </div>
        
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid"> 
					<?php
						$this->load->model('rekap_keluar_masuk/m_rekap_keluar_masuk');
						$id_tahun_ajaran = $statuspil;
						$id_sekolah = get_id_sekolah();
						$data['keuangan'] = $this->m_rekap_keluar_masuk->getDataKeuangan($id_sekolah,$id_tahun_ajaran)->result_array();
						$keuangan = $data['keuangan'];
					?>   

					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Transaksi</th>
								<th>Penanggung Jawab</th>
								<th>Debit</th>
								<th>Kredit</th>
								<th>Keterangan</th>
								<th>Aksi</th>
							</tr>
						</thead>
				
						<tbody>
							<?php $no=1;?> 
							<?php foreach ($keuangan as $k) {?>
							<tr>
								<td width='' class='center'><?php echo $no?></td>
								<td width='' class='center'><?php echo $k['nama_transaksi'] ?></td>
								<td width='' class='center'><?php echo $k['penanggung_jawab'] ?></td>
								<td width='' class='center'><?php echo $k['debit'] ?></td>
								<td width='' class='center'><?php echo $k['kredit'] ?></td>
								<td width='' class='center'><?php echo $k['keterangan'] ?></td>
								<td align="center">
									<a href="<?php echo base_url('pembiayaan/rekap_keluar_masuk/load_edit/'.$k['id_keuangan_sirkulasi']); ?>" class="button small grey tooltip" data-gravity="s"><i class="icon-edit"></i> Edit</a>
									<a href="<?php echo base_url('pembiayaan/rekap_keluar_masuk/delete/'.$k['id_keuangan_sirkulasi']); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
								</td>
							</tr>
							<?php $no=$no+1;?>
							<?php } ?>
					</table>
					<div class="footer">

					</div>
				</div>
            </div>
        </div>  
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>