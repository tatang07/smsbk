<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Tampah Pertemuan</h2>
       </div>
		
		<form action="<?php echo base_url('pembiayaan/rekap_keluar_masuk/edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>		
				<?php foreach ($keuangan as $k): ?>	
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama Transaksi</strong>
						</label>
						<div>
							<input type="text" name="nama_transaksi" value="<?php echo $k['nama_transaksi'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Transaksi</strong>
						</label>
						<div>
							<input type="date" name="tanggal_transaksi" value="<?php echo $k['tanggal_transaksi'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Penanggung Jawab</strong>
						</label>
						<div>
							<input type="text" name="penanggung_jawab" value="<?php echo $k['penanggung_jawab'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Harga Satuan</strong>
						</label>
						<div>
							<input type="text" name="harga_satuan" value="<?php echo $k['harga_satuan'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jumlah Satuan</strong>
						</label>
						<div>
							<input type="text" name="jumlah_satuan" value="<?php echo $k['jumlah_satuan'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Biaya Operasional</strong>
						</label>
						<div>
							<input type="text" name="biaya_operasional" value="<?php echo $k['biaya_operasional'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenis Transaksi</strong>
						</label>
						<div>
							<?php if($k['jenis_transaksi'] == 'debit'){?>
								<select name="jenis_transaksi">
									<option selected value="debit">debit</option>
									<option value="kredit">kredit</option>
								</select>
							<?php }else if($k['jenis_transaksi'] == 'kredit'){ ?>
								<select name="jenis_transaksi">
									<option value="debit">debit</option>
									<option selected value="kredit">kredit</option>
								</select>
							<?php } ?>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<textarea name="keterangan" rows="5"><?php echo $k['keterangan'] ?></textarea>
						</div>
					</div>
				<?php endforeach ?>
				<input type="hidden" name="id_keuangan_sirkulasi" value="<?php echo $id_keuangan_sirkulasi; ?>">		
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('pembiayaan/rekap_keluar_masuk'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>
