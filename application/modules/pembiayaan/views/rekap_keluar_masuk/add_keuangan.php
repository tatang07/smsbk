<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Tambah Data Sirkulasi Keuangan</h2>
       </div>
		
		<form action="<?php echo base_url('pembiayaan/rekap_keluar_masuk/add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>			
				<div class="row">
					<label for="f1_normal_input">
						<strong>Nama Transaksi</strong>
					</label>
					<div>
						<input type="text" name="nama_transaksi" value="" />
					</div>
				</div>
				<div class="row">
					<label for="f1_normal_input">
						<strong>Tanggal Transaksi</strong>
					</label>
					<div>
						<input type="date" name="tanggal_transaksi" value="" />
					</div>
				</div>
				<div class="row">
					<label for="f1_normal_input">
						<strong>Penanggung Jawab</strong>
					</label>
					<div>
						<input type="text" name="penanggung_jawab" value="" />
					</div>
				</div>
				<div class="row">
					<label for="f1_normal_input">
						<strong>Harga Satuan</strong>
					</label>
					<div>
						<input type="text" name="harga_satuan" value="" />
					</div>
				</div>
				<div class="row">
					<label for="f1_normal_input">
						<strong>Jumlah Satuan</strong>
					</label>
					<div>
						<input type="text" name="jumlah_satuan" value="" />
					</div>
				</div>
				<div class="row">
					<label for="f1_normal_input">
						<strong>Biaya Operasional</strong>
					</label>
					<div>
						<input type="text" name="biaya_operasional" value="" />
					</div>
				</div>
				<div class="row">
					<label for="f1_normal_input">
						<strong>Jenis Transaksi</strong>
					</label>
					<div>
						<select name="jenis_transaksi">
							<option value="debit">debit</option>
							<option value="kredit">kredit</option>
						</select>
					</div>
				</div>
				<div class="row">
					<label for="f1_normal_input">
						<strong>Keterangan</strong>
					</label>
					<div>
						<textarea name="keterangan" rows="5"></textarea>
					</div>
				</div>
				<input type="hidden" name="id_tahun_ajaran" value="<?php echo $tahun_ajaran; ?>">	
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('kepemimpinan/pertemuan'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>
