<?php  
	if(isset($_POST['id_rombel'])){
		$a = $_POST['id_rombel'];
		$statuspil = $a;
	}else{
		$statuspil = get_id_tahun_ajaran();
	}
?>

<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Rekap Dana Sumbangan Pendidikan Tahunan</h2>
       </div>
	
       <div class="tabletools">
       		<div class="right">
			  	<a href="<?php echo base_url('pembiayaan/dspt/load_bayar'); ?>"><i class="icon-plus"></i>Bayar</a> 
			  	<a href="<?php echo base_url('pembiayaan/dspt/print_pdf'); ?>"><i class="icon-print"></i>Cetak</a> 
			  <br><br>
            </div>

			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Kelas</span></th>
							<td width=200px align='left'>
								<select name="id_rombel" id="id_rombel" onchange="submitform();">
									<?php foreach($rombel as $r): ?>
										<?php if($r['id_rombel'] == $statuspil){ ?>
											<option selected value='<?php echo $r['id_rombel']; ?>' data-status-pilihan="<?php echo $r['id_rombel']; ?>"><?php echo $r['rombel']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $r['id_rombel']; ?>' data-status-pilihan="<?php echo $r['id_rombel']; ?>"><?php echo $r['rombel']; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
        </div>

        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid"> 
					<?php
						$this->load->model('pembiayaan/m_dspt');
						$id_rombel = $statuspil;
						$id_sekolah = get_id_sekolah();
						$data['dspt'] = $this->m_dspt->getDataSiswa($id_sekolah,$id_rombel)->result_array();
						$dspt = $data['dspt'];
					?> 
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Jumlah Bayar</th>
								<th>Tunggakan</th>
							</tr>
						</thead>
				
						<tbody>
							<?php $no=1;?> 
							<?php foreach ($dspt as $k) {?>
							<tr>
								<td width='' class='center'><?php echo $no?></td>
								<td width='' class='center'><?php echo $k['nis'] ?></td>
								<td width='' class='center'><?php echo $k['nama'] ?></td>
								<td width='' class='center'><?php echo $k['jumlah_dibayar'] ?></td>
								<td width='' class='center'><?php echo $k['tunggakan'] ?></td>
							</tr>
							<?php $no=$no+1;?>
							<?php } ?>
					</table>
					<div class="footer">

					</div>
				</div>
            </div>
        </div>  
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>