<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Pembayaran Dana Sumbangan Pendidikan Tahunan</h2>
       </div>
	

		<form action="<?php echo base_url('pembiayaan/dspt/bayar'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>			
				<div class="row">
					<label for="f1_normal_input">
						<strong>NIS</strong>
					</label>
					<div>
						<input type="text" name="nis" value="" />
					</div>
				</div>
				<div class="row">
					<label for="f1_normal_input">
						<strong>Tanggal</strong>
					</label>
					<div>
						<input type="date" name="tanggal" value="" />
					</div>
				</div>
				<div class="row">
					<label for="f1_normal_input">
						<strong>Jumlah Bayar</strong>
					</label>
					<div>
						<input type="text" name="jumlah_dibayar" value="" />
					</div>
				</div>
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('pembiayaan/dspt'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
</div>