<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class rekap_tunggakan_dspb extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
		
			$this->load->model('t_rekap_tunggakan_dspb');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('pembiayaan/rekap_tunggakan_dspb/home');
		}
		
		public function home(){
			
			$first_rombel = $this->t_rekap_tunggakan_dspb->get_first_rombel();
			if($first_rombel){
				$id_rombel = $first_rombel->id_rombel;
			}else{
				$id_rombel = 0;
			}
			
			$data['first_rombel'] = 0;
			$data['rekap'] = $this->t_rekap_tunggakan_dspb->get_all_rekap();
			$data['siswa'] = $this->t_rekap_tunggakan_dspb->get_siswa_rombel($id_rombel);
			$id_sekolah = get_id_sekolah();
			$data['rombel'] = $this->t_rekap_tunggakan_dspb->get_all_rombel($id_sekolah);
			$data['component'] = "Pembiayaan";
			render("rekap_tunggakan_dspb/rekap", $data);
		}
		
		public function search(){
			$id_rombel = $this->input->post('rombel');
			
			$data['first_rombel'] = $id_rombel;
			$data['rekap'] = $this->t_rekap_tunggakan_dspb->get_all_rekap();
			$data['siswa'] = $this->t_rekap_tunggakan_dspb->get_siswa_rombel($id_rombel);
			$id_sekolah = get_id_sekolah();
			$data['rombel'] = $this->t_rekap_tunggakan_dspb->get_all_rombel($id_sekolah);
			$data['component'] = "Pembiayaan";
			render("rekap_tunggakan_dspb/rekap", $data);
		}
		
		public function tambah(){
			
			$data['tahun_ajaran'] = $this->t_rekap_tunggakan_dspb->get_all_tahun_ajaran();;
			$data['component'] = "Pembiayaan";
			render("rekap_tunggakan_dspb/form_tambah", $data);
		}
		
		public function submit_post(){
			
			$this->form_validation->set_rules('nis', 'NIS', 'required');
			$this->form_validation->set_rules('id_tahun_ajaran', 'Tahun Ajaran', 'required');
			$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
				
			if($this->form_validation->run() == TRUE){
			
				$nis = $this->input->post('nis');
				$row = $this->t_rekap_tunggakan_dspb->get_siswa_by($nis);
				$id_siswa = $row->id_siswa;
				
				$id_tahun_ajaran = $this->input->post('id_tahun_ajaran');
				$id_jenis_pembayaran = $this->input->post('id_jenis_pembayaran');
				$bulan = $this->input->post('bulan');
				$tanggal = tanggal_db($this->input->post('tanggal'));
				$jumlah_dibayar = $this->input->post('jumlah_dibayar');
				$tunggakan = $this->input->post('tunggakan');
				
				$action = $this->input->post('action');
				if($action == 'add'){
					$this->t_rekap_tunggakan_dspb->tambah($id_siswa, $id_tahun_ajaran, $id_jenis_pembayaran, $bulan, $tanggal, $jumlah_dibayar, $tunggakan);
					
					set_success_message('Data Berhasil Ditambah!');
					redirect("pembiayaan/rekap_tunggakan_dspb/home");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian Data!');
				redirect("pembiayaan/rekap_tunggakan_dspb/home/");
			}
		}

		function print_pdf($id_rombel){
		
			$data['first_rombel'] = $id_rombel;
			$data['rekap'] = $this->t_rekap_tunggakan_dspb->get_all_rekap();
			$data['siswa'] = $this->t_rekap_tunggakan_dspb->get_siswa_rombel($id_rombel);
			$data['rombel'] = $this->t_rekap_tunggakan_dspb->get_all_rombel();
			
			print_pdf($this->load->view('rekap_tunggakan_dspb/rekap_pdf', $data, true), "A4");
		}
}
