<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class grafik_beasiswa extends Simple_Controller {
	public function index(){	
		$this->load->model('pembiayaan/t_keuangan_beasiswa');
		$data['tahun_ajaran'] = $this->t_keuangan_beasiswa->getTahunAjaran()->result_array();
		$data['component'] = "Pembiayaan";
		render('grafik_beasiswa/grafik_beasiswa', $data);
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */