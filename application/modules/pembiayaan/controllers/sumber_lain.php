<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class sumber_lain extends CI_Controller {

	public function index()
	{	
		$this->load->model('pembiayaan/m_sumber_lain');
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();		
		
		$data['isi'] = $this->m_sumber_lain->get_data($id_sekolah,$id_tahun_ajaran);
		$data['component']="Pembiayaan";
		render('sumber_lain/sumber_lain',$data);
	}
	public function load_add()
	{	
		$this->load->model('pembiayaan/m_sumber_lain');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		//$data['kelas'] = $this->m_sumber_lain->select_kelas($jenjang_sekolah);
		$data['component']="Pembiayaan";
		render('sumber_lain/form_tambah',$data);
	}

	public function load_edit($id_sumber_lain)
	{	
		$this->load->model('pembiayaan/m_sumber_lain');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_sumber_lain->get_data_edit($id_sumber_lain);		
		$data['component']="Pembiayaan";
		render('sumber_lain/form_edit',$data);
	}

	public function delete($id_sumber_dana){
			$this->load->model('pembiayaan/m_sumber_lain');
			$this->m_sumber_lain->delete($id_sumber_dana);
			redirect(site_url('pembiayaan/sumber_lain/index/')) ;
	}

	public function submit_add(){
			$this->load->model('pembiayaan/m_sumber_lain');
			//$data['id_tingkat_kelas'] = $this->input->post('id_tingkat_kelas');
			$data['id_sekolah'] = get_id_sekolah();
			$data['id_tahun_ajaran'] = get_id_tahun_ajaran();
			
			$data['besaran'] = $this->input->post('besaran');
			$data['sumber_dana'] = $this->input->post('sumber_dana');
			$data['keterangan'] = $this->input->post('keterangan');

		
			$this->m_sumber_lain->add($data);
			redirect(site_url('pembiayaan/sumber_lain/index/')) ;
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('pembiayaan/m_sumber_lain');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_sumber_lain->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_sumber_lain->select_kelas($jenjang_sekolah);
			render('sumber_lain/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('pembiayaan/m_sumber_lain');
			$id=$this->input->post('id');
			$data['besaran'] = $this->input->post('besaran');

			$this->m_sumber_lain->edit($data,$id);
			redirect(site_url('pembiayaan/sumber_lain/index/')) ;
	}
	public function search(){
		$this->load->model('pembiayaan/m_sumber_lain');
		$nama = $this->input->post('nama');
		$status = $this->input->post('status');
		
		$id_sekolah = get_id_sekolah();
		if($status==0){
		$data['isi'] = $this->m_sumber_lain->get_data_search_default($nama,$id_sekolah);
		}else{
		$data['isi'] = $this->m_sumber_lain->get_data_search($status,$nama,$id_sekolah);
		}
		$data['component']="Pembiayaan";
		render('sumber_lain/sumber_lain',$data);
	}
			
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */