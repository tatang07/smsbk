<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class rekap_keluar_masuk extends CI_Controller {
	public function index()
	{	
		$this->load->model('pembiayaan/m_rekap_keluar_masuk');
		$data['tahun_ajaran'] = $this->m_rekap_keluar_masuk->getTahunAjaran()->result_array();
		$data['component'] = "Pembiayaan";
		render('rekap_keluar_masuk/rekap_keluar_masuk', $data);
	}

	public function load_add($id){
		$data['tahun_ajaran'] = $id;
		$data['component'] = "Pembiayaan";
		render('rekap_keluar_masuk/add_keuangan', $data);
	}

	public function add(){
		$this->load->model('pembiayaan/m_rekap_keluar_masuk');
		$data['id_sekolah'] = get_id_sekolah();
		$data['id_tahun_ajaran'] = $this->input->post('id_tahun_ajaran');
		$data['nama_transaksi'] = $this->input->post('nama_transaksi');
		$data['tanggal_transaksi'] = tanggal_db($this->input->post('tanggal_transaksi'));
		$data['penanggung_jawab'] = $this->input->post('penanggung_jawab');
		$data['harga_satuan'] = $this->input->post('harga_satuan');
		$data['jumlah_satuan'] = $this->input->post('jumlah_satuan');
		$data['biaya_operasional'] = $this->input->post('biaya_operasional');
		$data['jenis_transaksi'] = $this->input->post('jenis_transaksi');
		$data['keterangan'] = $this->input->post('keterangan');

		if($data['jenis_transaksi'] == 'debit')
			$data['biaya_operasional'] = $data['biaya_operasional'] * -1;

		$jumlah_uang = ($data['harga_satuan'] * $data['jumlah_satuan']) + $data['biaya_operasional'];
		if($data['jenis_transaksi'] == 'debit'){
			$data['debit'] = $jumlah_uang;
			$data['kredit'] = '';
		}else if($data['jenis_transaksi'] == 'kredit'){
			$data['debit'] = '';
			$data['kredit'] = $jumlah_uang;
		}

		$this->m_rekap_keluar_masuk->add($data);
		redirect(site_url('pembiayaan/rekap_keluar_masuk'));
	}

	public function load_edit($id){
		$this->load->model('pembiayaan/m_rekap_keluar_masuk');
		$data['id_keuangan_sirkulasi'] = $id;
		$data['keuangan'] = $this->m_rekap_keluar_masuk->getDataKeuangan_ById($id)->result_array();
		$data['component'] = "Pembiayaan";
		render('rekap_keluar_masuk/edit_keuangan', $data);
	}

	public function edit(){
		$this->load->model('pembiayaan/m_rekap_keluar_masuk');
		$data['nama_transaksi'] = $this->input->post('nama_transaksi');
		$data['tanggal_transaksi'] = tanggal_db($this->input->post('tanggal_transaksi'));
		$data['penanggung_jawab'] = $this->input->post('penanggung_jawab');
		$data['harga_satuan'] = $this->input->post('harga_satuan');
		$data['jumlah_satuan'] = $this->input->post('jumlah_satuan');
		$data['biaya_operasional'] = $this->input->post('biaya_operasional');
		$data['jenis_transaksi'] = $this->input->post('jenis_transaksi');
		$data['keterangan'] = $this->input->post('keterangan');

		if($data['jenis_transaksi'] == 'debit')
			$data['biaya_operasional'] = $data['biaya_operasional'] * -1;

		$jumlah_uang = ($data['harga_satuan'] * $data['jumlah_satuan']) + $data['biaya_operasional'];
		if($data['jenis_transaksi'] == 'debit'){
			$data['debit'] = $jumlah_uang;
			$data['kredit'] = '';
		}else if($data['jenis_transaksi'] == 'kredit'){
			$data['debit'] = '';
			$data['kredit'] = $jumlah_uang;
		}

		$id_keuangan_sirkulasi = $this->input->post('id_keuangan_sirkulasi');
		$this->m_rekap_keluar_masuk->edit($data, $id_keuangan_sirkulasi);
		redirect(site_url('pembiayaan/rekap_keluar_masuk'));
	}

	public function delete($id_keuangan_sirkulasi){
		$this->load->model('pembiayaan/m_rekap_keluar_masuk');
		$this->m_rekap_keluar_masuk->delete($id_keuangan_sirkulasi);
		redirect(site_url('pembiayaan/rekap_keluar_masuk'));
	}

	public function print_pdf($id_tahun_ajaran){
		$this->load->model('pembiayaan/m_rekap_keluar_masuk');
		$id_sekolah = get_id_sekolah();
		$data['tahun_ajaran'] = $this->m_rekap_keluar_masuk->getTahunAjaran()->result_array();
		$data['statuspil'] = $id_tahun_ajaran;
		$data['keuangan'] = $this->m_rekap_keluar_masuk->getDataKeuangan($id_sekolah,$id_tahun_ajaran)->result_array();
		print_pdf($this->load->view('rekap_keluar_masuk/rekap_keluar_masuk_pdf', $data, true), "A4");
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */