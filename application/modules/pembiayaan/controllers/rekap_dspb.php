<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class rekap_dspb extends CI_Controller {

	public function index()
	{	
		$this->load->model('pembiayaan/m_rekap_dspb');
		
		$id_sekolah=get_id_sekolah();
		$data['isi'] = $this->m_rekap_dspb->get_data($id_sekolah);
		$data['component'] = "Pembiayaan";
		render('rekap_dspb/rekap_dspb',$data);
	}
	function print_pdf(){
		
		$this->load->model('pembiayaan/m_rekap_dspb');
		
		$id_sekolah=get_id_sekolah();
		$data['isi'] = $this->m_rekap_dspb->get_data($id_sekolah);	
		$data['statuspil'] = "iaiaia";
		print_pdf($this->load->view('rekap_dspb/rekap_dspb_pdf', $data, true), "A4");
	}
	public function load_add()
	{	
		$this->load->model('pembiayaan/m_rekap_dspb');
		$data['component'] = "Pembiayaan";
		render('rekap_dspb/form_tambah');
	}
		
	public function cekData($nim){
			$this->load->model('pembiayaan/m_rekap_dspb');
			$data = $this->m_rekap_dspb->getData($nim);
			echo json_encode ($data);
	}

	public function submit_add(){
			$this->load->model('pembiayaan/m_rekap_dspb');
			$data['id_siswa'] = $this->input->post('id_siswa');
			$data['tanggal'] = tanggal_db($this->input->post('tanggal'));
			$data['bulan'] = $this->input->post('bulan');
			$data['jumlah_dibayar'] = $this->input->post('jumlah');
	
			$this->m_rekap_dspb->add($data);
			redirect(site_url('pembiayaan/rekap_dspb/index/')) ;
	}


		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */