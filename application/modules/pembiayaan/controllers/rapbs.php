<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class rapbs extends CI_Controller {
	function index(){	
		$this->load->model('pembiayaan/m_rapbs');
		$data['ta'] = $this->m_rapbs->get_tahun_ajaran();
		$data['component'] = "Pembiayaan";
		render('rapbs/rapbs', $data);
	}

	function load_detail($id){
		$this->load->model('pembiayaan/m_rapbs');
		$data['sub'] = $this->m_rapbs->get_sub_proker($id)->result_array();
		$data['component'] = "Pembiayaan";
		render('rapbs/detail', $data);
	}

	function edit_sub($id){
		$this->load->model('pembiayaan/m_rapbs');
		$data['sub'] = $this->m_rapbs->get_sub_proker_by_id($id)->result_array();
		$data['id_sub'] = $id;
		$data['component'] = "Pembiayaan";
		render('rapbs/edit', $data);
	}

	function submit($id_sub, $id){
		$this->load->model('pembiayaan/m_rapbs');

		$data['pendanaan'] = $this->input->post('pendanaan');
		$data['sumber_dana'] = $this->input->post('sumber_dana');
		$data['keterangan'] = $this->input->post('keterangan');
		$this->m_rapbs->update_sub_proker($id_sub, $data);
		redirect(site_url('pembiayaan/rapbs/load_detail/'.$id));
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */