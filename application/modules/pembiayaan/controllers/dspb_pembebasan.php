<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class dspb_pembebasan extends CI_Controller {

	public function index()
	{	
		$this->load->model('pembiayaan/m_dspb_pembebasan');
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();		
		$data['rombel']=$this->m_dspb_pembebasan->get_rombel($id_sekolah,$id_tahun_ajaran);
		// $data['isi'] = $this->m_dspb_pembebasan->get_data($id_sekolah,$id_tahun_ajaran);
		$data['component']="Pembiayaan";
		render('dspb_pembebasan/dspb_pembebasan',$data);
	}
	public function load_add($id_rombel)
	{	
		$this->load->model('pembiayaan/m_dspb_pembebasan');
		$id_sekolah= get_id_sekolah();
		$id_tahun_ajaran=get_id_tahun_ajaran();
		
		$data['siswa'] = $this->m_dspb_pembebasan->select_siswa($id_sekolah,$id_tahun_ajaran,$id_rombel);
		$data['component']="Pembiayaan";
		render('dspb_pembebasan/form_tambah',$data);
	}

	public function load_edit($id_keuangan_besaran)
	{	
		$this->load->model('pembiayaan/m_dspb_pembebasan');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_dspb_pembebasan->get_data_edit($id_keuangan_besaran);		
		$data['component']="Pembiayaan";
		render('dspb_pembebasan/form_edit',$data);
	}

	public function delete($id_keuangan_pembebasan_siswa){
			$this->load->model('pembiayaan/m_dspb_pembebasan');
			$this->m_dspb_pembebasan->delete($id_keuangan_pembebasan_siswa);
			redirect(site_url('pembiayaan/dspb_pembebasan/index/')) ;
	}

	public function submit_add(){
			$this->load->model('pembiayaan/m_dspb_pembebasan');
			$data['id_siswa'] = $this->input->post('id_siswa');
			
			$data['id_jenis_pembayaran'] = $this->input->post('id_jenis_pembayaran');
			$data['id_tahun_ajaran'] = get_id_tahun_ajaran();
			
		
			$this->m_dspb_pembebasan->add($data);
			redirect(site_url('pembiayaan/dspb_pembebasan/index/')) ;
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('pembiayaan/m_dspb_pembebasan');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_dspb_pembebasan->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_dspb_pembebasan->select_kelas($jenjang_sekolah);
			render('dspb_pembebasan/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('pembiayaan/m_dspb_pembebasan');
			$id=$this->input->post('id');
			$data['besaran'] = $this->input->post('besaran');

			$this->m_dspb_pembebasan->edit($data,$id);
			redirect(site_url('pembiayaan/dspb_pembebasan/index/')) ;
	}
	public function search(){
		$this->load->model('pembiayaan/m_dspb_pembebasan');
		// $nama = $this->input->post('nama');
		$id_rombel = $this->input->post('id_rombel');
		
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();	
		$data['rombel']=$this->m_dspb_pembebasan->get_rombel($id_sekolah,$id_tahun_ajaran);
		$data['isi'] = $this->m_dspb_pembebasan->get_data_search($id_sekolah,$id_tahun_ajaran,$id_rombel);
		// $data['tagihan']=$this->m_dspb_pembebasan->get_data_tagihan($id_sekolah,$id_tahun_ajaran,$id_rombel);
		$data['component']="Pembiayaan";
		render('dspb_pembebasan/dspb_pembebasan',$data);
	}
			
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */