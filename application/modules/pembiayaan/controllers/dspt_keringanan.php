<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class dspt_keringanan extends CI_Controller {

	public function index()
	{	
		$this->load->model('pembiayaan/m_dspt_keringanan');
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();		
		$data['rombel']=$this->m_dspt_keringanan->get_rombel($id_sekolah,$id_tahun_ajaran);
		// $data['isi'] = $this->m_dspt_keringanan->get_data($id_sekolah,$id_tahun_ajaran);
		$data['component']="Pembiayaan";
		render('dspt_keringanan/dspt_keringanan',$data);
	}
	public function load_add($id_rombel)
	{	
		$this->load->model('pembiayaan/m_dspt_keringanan');
		$id_sekolah= get_id_sekolah();
		$id_tahun_ajaran=get_id_tahun_ajaran();
		
		$data['siswa'] = $this->m_dspt_keringanan->select_siswa($id_sekolah,$id_tahun_ajaran,$id_rombel);
		$data['component']="Pembiayaan";
		render('dspt_keringanan/form_tambah',$data);
	}

	public function load_edit($id_keuangan_besaran)
	{	
		$this->load->model('pembiayaan/m_dspt_keringanan');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_dspt_keringanan->get_data_edit($id_keuangan_besaran);		
		$data['component']="Pembiayaan";
		render('dspt_keringanan/form_edit',$data);
	}

	public function delete($id_keuangan_keringan_siswa){
			$this->load->model('pembiayaan/m_dspt_keringanan');
			$this->m_dspt_keringanan->delete($id_keuangan_keringan_siswa);
			redirect(site_url('pembiayaan/dspt_keringanan/index/')) ;
	}

	public function submit_add(){
			$this->load->model('pembiayaan/m_dspt_keringanan');
			$data['id_siswa'] = $this->input->post('id_siswa');
			$data['id_tahun_ajaran'] = get_id_tahun_ajaran();
			
			
			$data['id_jenis_pembayaran'] = $this->input->post('id_jenis_pembayaran');
			$data['jumlah'] = $this->input->post('jumlah');
			$data['bulan'] = $this->input->post('bulan');
			$data['keterangan'] = $this->input->post('keterangan');
		
			$this->m_dspt_keringanan->add($data);
			redirect(site_url('pembiayaan/dspt_keringanan/index/')) ;
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('pembiayaan/m_dspt_keringanan');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_dspt_keringanan->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_dspt_keringanan->select_kelas($jenjang_sekolah);
			render('dspt_keringanan/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('pembiayaan/m_dspt_keringanan');
			$id=$this->input->post('id');
			$data['besaran'] = $this->input->post('besaran');

			$this->m_dspt_keringanan->edit($data,$id);
			redirect(site_url('pembiayaan/dspt_keringanan/index/')) ;
	}
	public function search(){
		$this->load->model('pembiayaan/m_dspt_keringanan');
		// $nama = $this->input->post('nama');
		$id_rombel = $this->input->post('id_rombel');
		
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();	
		$data['rombel']=$this->m_dspt_keringanan->get_rombel($id_sekolah,$id_tahun_ajaran);
		$data['isi'] = $this->m_dspt_keringanan->get_data_search($id_sekolah,$id_tahun_ajaran,$id_rombel);
		$data['tagihan']=$this->m_dspt_keringanan->get_data_tagihan($id_sekolah,$id_tahun_ajaran,$id_rombel);
		$data['component']="Pembiayaan";
		render('dspt_keringanan/dspt_keringanan',$data);
	}
			
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */