<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class dspb_penentuan_besaran extends CI_Controller {

	public function index()
	{	
		$this->load->model('pembiayaan/m_dspb_penentuan_besaran');
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();		
		
		$data['isi'] = $this->m_dspb_penentuan_besaran->get_data($id_sekolah,$id_tahun_ajaran);
		$data['component']="Pembiayaan";
		render('dspb_penentuan_besaran/dspb_penentuan_besaran',$data);
	}
	public function load_add()
	{	
		$this->load->model('pembiayaan/m_dspb_penentuan_besaran');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		$data['kelas'] = $this->m_dspb_penentuan_besaran->select_kelas($jenjang_sekolah);
		$data['component']="Pembiayaan";
		render('dspb_penentuan_besaran/form_tambah',$data);
	}

	public function load_edit($id_keuangan_besaran)
	{	
		$this->load->model('pembiayaan/m_dspb_penentuan_besaran');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_dspb_penentuan_besaran->get_data_edit($id_keuangan_besaran);		
		$data['component']="Pembiayaan";
		render('dspb_penentuan_besaran/form_edit',$data);
	}

	public function delete($id_keuangan_besaran){
			$this->load->model('pembiayaan/m_dspb_penentuan_besaran');
			$this->m_dspb_penentuan_besaran->delete($id_keuangan_besaran);
			redirect(site_url('pembiayaan/dspb_penentuan_besaran/index/')) ;
	}

	public function submit_add(){
			$this->load->model('pembiayaan/m_dspb_penentuan_besaran');
			$data['id_tingkat_kelas'] = $this->input->post('id_tingkat_kelas');
			$data['id_sekolah'] = get_id_sekolah();
			$data['id_tahun_ajaran'] = get_id_tahun_ajaran();
			$data['besaran'] = $this->input->post('besaran');
			$data['id_jenis_pembayaran'] = $this->input->post('id_jenis_pembayaran');
		
			$this->m_dspb_penentuan_besaran->add($data);
			redirect(site_url('pembiayaan/dspb_penentuan_besaran/index/')) ;
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('pembiayaan/m_dspb_penentuan_besaran');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_dspb_penentuan_besaran->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_dspb_penentuan_besaran->select_kelas($jenjang_sekolah);
			render('dspb_penentuan_besaran/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('pembiayaan/m_dspb_penentuan_besaran');
			$id=$this->input->post('id');
			$data['besaran'] = $this->input->post('besaran');

			$this->m_dspb_penentuan_besaran->edit($data,$id);
			redirect(site_url('pembiayaan/dspb_penentuan_besaran/index/')) ;
	}
	public function search(){
		$this->load->model('pembiayaan/m_dspb_penentuan_besaran');
		$nama = $this->input->post('nama');
		$status = $this->input->post('status');
		
		$id_sekolah = get_id_sekolah();
		if($status==0){
		$data['isi'] = $this->m_dspb_penentuan_besaran->get_data_search_default($nama,$id_sekolah);
		}else{
		$data['isi'] = $this->m_dspb_penentuan_besaran->get_data_search($status,$nama,$id_sekolah);
		}
		$data['component']="Pembiayaan";
		render('dspb_penentuan_besaran/dspb_penentuan_besaran',$data);
	}
			
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */