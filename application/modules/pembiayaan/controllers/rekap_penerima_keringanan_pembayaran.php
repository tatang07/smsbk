<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class rekap_penerima_keringanan_pembayaran extends CI_Controller {

	public function index()
	{	
		$this->load->model('pembiayaan/m_rekap_penerima_keringanan_pembayaran');
		$data['ta'] = $this->m_rekap_penerima_keringanan_pembayaran->get_tahun_ajaran();
		$data['component'] = "Pembiayaan";
		render('rekap_penerima_keringanan_pembayaran/rekap_penerima_keringanan_pembayaran',$data);
	}
	public function load_add($id_tahun_ajaran)
	{	
		$this->load->model('pembiayaan/m_rekap_penerima_keringanan_pembayaran');
		$data['tabi'] = $this->m_rekap_penerima_keringanan_pembayaran->get_tahun_ajaran_by_id($id_tahun_ajaran);
		$data['siswa'] = $this->m_rekap_penerima_keringanan_pembayaran->get_siswa();
		$id_sekolah=get_id_sekolah();
		$data['jenis_pembayaran'] = $this->m_rekap_penerima_keringanan_pembayaran->get_jenis_pembayaran($id_sekolah);
		$data['id_tahun_ajaran']=$id_tahun_ajaran;
		$data['component'] = "Pembiayaan";
		render('rekap_penerima_keringanan_pembayaran/form_tambah',$data);
	}

	public function load_edit($id_tahun_ajaran,$id_sekolah_program_kerja)
	{	
		$this->load->model('pembiayaan/m_rekap_penerima_keringanan_pembayaran');
		$data['tabi'] = $this->m_rekap_penerima_keringanan_pembayaran->get_tahun_ajaran_by_id($id_tahun_ajaran);
		$data['data_edit'] = $this->m_rekap_penerima_keringanan_pembayaran->get_data_by_id($id_sekolah_program_kerja);
		$data['siswa'] = $this->m_rekap_penerima_keringanan_pembayaran->get_siswa();
		$id_sekolah=get_id_sekolah();
		$data['jenis_pembayaran'] = $this->m_rekap_penerima_keringanan_pembayaran->get_jenis_pembayaran($id_sekolah);
		$data['id']=$id_sekolah_program_kerja;
		$data['id2']=$id_tahun_ajaran;
		$data['component'] = "Pembiayaan";
		render('rekap_penerima_keringanan_pembayaran/form_edit',$data);
	}

	public function delete($id_sekolah_program_kerja){
			$this->load->model('pembiayaan/m_rekap_penerima_keringanan_pembayaran');
			$this->m_rekap_penerima_keringanan_pembayaran->delete($id_sekolah_program_kerja);
			redirect(site_url('pembiayaan/rekap_penerima_keringanan_pembayaran/index/')) ;
	}

	public function submit_add(){
			$this->load->model('pembiayaan/m_rekap_penerima_keringanan_pembayaran');
			$data['id_siswa'] = $this->input->post('nama');
			$data['id_jenis_pembayaran'] = $this->input->post('jenis_pembayaran');
			$data['bulan'] = $this->input->post('bulan');
			$data['jumlah'] = $this->input->post('jumlah');
			$data['keterangan'] = $this->input->post('keterangan');
			$data['id_tahun_ajaran'] = $this->input->post('id_tahun_ajaran');
	
			$this->m_rekap_penerima_keringanan_pembayaran->add($data);
			redirect(site_url('pembiayaan/rekap_penerima_keringanan_pembayaran/index/')) ;
	}

	public function submit_edit(){
			$this->load->model('pembiayaan/m_rekap_penerima_keringanan_pembayaran');
			$id=$this->input->post('id');
			$data['id_siswa'] = $this->input->post('nama');
			$data['id_jenis_pembayaran'] = $this->input->post('jenis_pembayaran');
			$data['bulan'] = $this->input->post('bulan');
			$data['jumlah'] = $this->input->post('jumlah');
			$data['keterangan'] = $this->input->post('keterangan');
			$data['id_tahun_ajaran'] = $this->input->post('id_tahun_ajaran');
			
			$this->m_rekap_penerima_keringanan_pembayaran->edit($data,$id);
			redirect(site_url('pembiayaan/rekap_penerima_keringanan_pembayaran/index/')) ;
	}


		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */