<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class dspt extends CI_Controller {
	public function index()
	{	
		$this->load->model('pembiayaan/m_dspt');
		$data['rombel'] = $this->m_dspt->getRombel()->result_array();
		$data['component'] = "Pembiayaan";
		render('dspt/dspt', $data);
	}

	public function load_bayar(){
		$data['component'] = "Pembiayaan";
		render('dspt/bayar');
	}

	public function bayar(){
		$this->load->model('pembiayaan/m_dspt');
		$data['tanggal'] = $this->input->post('tanggal');

		$nis = $this->input->post('nis');
		$siswa = $this->m_dspt->getDataSiswa_byNis($nis)->result_array();

		foreach($siswa as $s){
			$id = $s['id_keuangan_pembayaran_siswa'];
		}
		// echo $id;
		$detail['tanggal'] = $this->input->post('tanggal');
		$detail['jumlah_dibayar'] = $this->input->post('jumlah_dibayar');
		$detail['id_keuangan_pembayaran_siswa'] = $id;
		$this->m_dspt->detail($detail);

		$x = $this->m_dspt->getJumlahTunggakan($id)->result_array();
		foreach($x as $y){
			$jum =  $y['jumlah_dibayar'];
		}

		$data['jumlah_dibayar'] = $jum;
		$this->m_dspt->update($id, $data);

		print_r($data);
		redirect(site_url('pembiayaan/dspt '));
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */