<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class dspt_tunggakan extends CI_Controller {

	public function index()
	{	
		$this->load->model('pembiayaan/m_dspt_tunggakan');
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();		
		$data['rombel']=$this->m_dspt_tunggakan->get_rombel($id_sekolah,$id_tahun_ajaran);
		// $data['isi'] = $this->m_dspt_tunggakan->get_data($id_sekolah,$id_tahun_ajaran);
		$data['component']="Pembiayaan";
		render('dspt_tunggakan/dspt_tunggakan',$data);
	}
	public function load_add($id_rombel)
	{	
		$this->load->model('pembiayaan/m_dspt_tunggakan');
		
		$id_sekolah= get_id_sekolah();
		$id_tahun_ajaran=get_id_tahun_ajaran();
		$data['siswa'] = $this->m_dspt_tunggakan->select_siswa($id_sekolah,$id_tahun_ajaran,$id_rombel);
		$data['component']="Pembiayaan";

		render('dspt_tunggakan/form_tambah',$data);
	}

	public function load_edit($id_keuangan_pembayaran_siswa)
	{	
		$this->load->model('pembiayaan/m_dspt_tunggakan');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_dspt_tunggakan->get_data_edit($id_keuangan_pembayaran_siswa);		
		$data['component']="Pembiayaan";
		render('dspt_tunggakan/form_edit',$data);
	}

	public function delete($id_keuangan_besaran){
			$this->load->model('pembiayaan/m_dspt_tunggakan');
			$this->m_dspt_tunggakan->delete($id_keuangan_besaran);
			redirect(site_url('pembiayaan/dspt_tunggakan/index/')) ;
	}

	public function submit_add(){
			$this->load->model('pembiayaan/m_dspt_tunggakan');
			$data['id_siswa'] = $this->input->post('id_siswa');
			$data['id_tahun_ajaran'] = get_id_tahun_ajaran();
			
			
			$data['id_jenis_pembayaran'] = $this->input->post('id_jenis_pembayaran');
			$data['tanggal'] = $this->input->post('tanggal');
			$data['bulan'] = $this->input->post('bulan');
			$data['jumlah_dibayar'] = $this->input->post('jumlah_dibayar');
				
			$data['tunggakan'] = $this->input->post('tunggakan');
			$this->m_dspt_tunggakan->add($data);
			redirect(site_url('pembiayaan/dspt_tunggakan/index/')) ;
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('pembiayaan/m_dspt_tunggakan');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_dspt_tunggakan->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_dspt_tunggakan->select_kelas($jenjang_sekolah);
			render('dspt_tunggakan/form_tambah',$data);
	}

	public function submit_edit($id){
			$this->load->model('pembiayaan/m_dspt_tunggakan');
		
			$data['tanggal'] = tanggal_db($this->input->post('tanggal'));
			
			$data['bulan'] = $this->input->post('bulan');
			$data['jumlah_dibayar'] = $this->input->post('jumlah_dibayar');
			$data['tunggakan'] = $this->input->post('tunggakan');

			$this->m_dspt_tunggakan->edit($data,$id);
			redirect(site_url('pembiayaan/dspt_tunggakan/index/')) ;
	}
	public function search(){
		$this->load->model('pembiayaan/m_dspt_tunggakan');
		// $nama = $this->input->post('nama');
		$id_rombel = $this->input->post('id_rombel');
		
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();	
		$data['rombel']=$this->m_dspt_tunggakan->get_rombel($id_sekolah,$id_tahun_ajaran);
		$data['isi'] = $this->m_dspt_tunggakan->get_data_search($id_sekolah,$id_tahun_ajaran,$id_rombel);
		
		$data['tagihan']=$this->m_dspt_tunggakan->get_data_tagihan($id_sekolah,$id_tahun_ajaran,$id_rombel);
		$data['component']="Pembiayaan";
		render('dspt_tunggakan/dspt_tunggakan',$data);
	}
			
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */