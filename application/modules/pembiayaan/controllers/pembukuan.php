<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pembukuan extends CI_Controller {

	public function index(){	
		$this->load->model('pembiayaan/m_pembukuan');
		$data['ta'] = $this->m_pembukuan->get_tahun_ajaran();
		$data['component'] = "Pembiayaan";
		render('pembukuan/pembukuan',$data);
	}	
	
	function print_pdf($statuspil){
		$this->load->model('pembiayaan/m_pembukuan');
		$data['pembukuan'] = $this->m_pembukuan->get_data(1,1);
		$data['ta'] = $this->m_pembukuan->get_tahun_ajaran();
		
		$data['statuspil'] = $statuspil;
		print_pdf($this->load->view('pembukuan/pembukuan_pdf', $data, true), "A4");
		// print_r ($data['pembukuan']);
	}	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */