<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class rekap_beasiswa extends CI_Controller {
	public function index(){	
		$this->load->model('pembiayaan/t_keuangan_beasiswa');
		$data['tahun_ajaran'] = $this->t_keuangan_beasiswa->getTahunAjaran()->result_array();
		$data['component'] = "Pembiayaan";
		render('rekap_beasiswa/rekap_beasiswa', $data);
	}

	public function load_add($id){
		$data['tahun_ajaran'] = $id;
		$this->load->model('pembiayaan/t_keuangan_beasiswa');
		$data['pemberi_beasiswa'] = $this->t_keuangan_beasiswa->getPemberiBeasiswa()->result_array();
		$data['component'] = "Pembiayaan";
		render('rekap_beasiswa/add_penerima', $data);
	}

	public function add(){
		$this->load->model('pembiayaan/t_keuangan_beasiswa');		
		//id tahun ajaran
		$data['id_tahun_ajaran'] = $this->input->post('id_tahun_ajaran');
		
		//id siswa
		$nis = $this->input->post('nis');
		$hasil = $this->t_keuangan_beasiswa->getSiswa_ByNis($nis)->result_array();
		foreach($hasil as $h):
			$data['id_siswa'] = $h['id_siswa'];
		endforeach;
		//$data['nama'] = $this->input->post('nama');
		
		//id pemberi beasiswa
		$data['id_pemberi_beasiswa'] = $this->input->post('pemberi_beasiswa');
		
		//jumlah
		$data['jumlah'] = $this->input->post('jumlah');

		$this->t_keuangan_beasiswa->add($data);
		redirect(site_url('pembiayaan/rekap_beasiswa'));
	}

	public function load_edit($id){
		$this->load->model('pembiayaan/t_keuangan_beasiswa');
		$data['id_keuangan_beasiswa'] = $id;
		$data['beasiswa'] = $this->t_keuangan_beasiswa->getBeasiswa_ById($id)->result_array();
		$data['pemberi_beasiswa'] = $this->t_keuangan_beasiswa->getPemberiBeasiswa()->result_array();
		$data['component'] = "Pembiayaan";
		render('rekap_beasiswa/edit_penerima', $data);
	}

	public function edit(){
		$this->load->model('pembiayaan/t_keuangan_beasiswa');
		$id_keuangan_beasiswa = $this->input->post('id_keuangan_beasiswa');
		//id siswa
		$nis = $this->input->post('nis');
		$hasil = $this->t_keuangan_beasiswa->getSiswa_ByNis($nis)->result_array();
		foreach($hasil as $h):
			$data['id_siswa'] = $h['id_siswa'];
		endforeach;
		//$data['nama'] = $this->input->post('nama');
		
		//id pemberi beasiswa
		$data['id_pemberi_beasiswa'] = $this->input->post('pemberi_beasiswa');
		
		//jumlah
		$data['jumlah'] = $this->input->post('jumlah');

		$this->t_keuangan_beasiswa->edit($data, $id_keuangan_beasiswa);
		redirect(site_url('pembiayaan/rekap_beasiswa'));
	}

	public function delete($id_keuangan_beasiswa){
		$this->load->model('pembiayaan/t_keuangan_beasiswa');
		$this->t_keuangan_beasiswa->delete($id_keuangan_beasiswa);
		redirect(site_url('pembiayaan/rekap_beasiswa'));
	}
	
	public function cekData($nim){
		$this->load->model('pps/m_angka_melanjutkan');
		$data = $this->m_angka_melanjutkan->getData($nim);
		echo json_encode ($data);
	}
	
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */