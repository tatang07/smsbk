<?php
class m_berita extends MY_Model {
    public function __construct(){
			parent::__construct();
	}
	function add($data){
		$this->db->insert('m_berita',$data);
	}
	
	function get_berita($id){
		$this->db->select('judul_berita,berita,author,nama,tanggal,m.foto,id_berita');
		$this->db->from('m_berita as m');
		$this->db->join('m_guru as g', 'g.id_guru = m.author');
		$this->db->where('m.author',$id);
		return $this->db->get()->result_array();
	}
	
	function delete($id){
			$this->db->where('id_berita', $id);
			$this->db->delete('m_berita');
	}
	
	function get_data_edit($id){
		$this->db->select('*');
		$this->db->from('m_berita');
		$this->db->where('id_berita',$id);
		return $this->db->get()->result_array();
	}
	
	function edit($data,$id=0){
		$this->db->where('id_berita',$id);
		$this->db->update('m_berita',$data);
	}
}