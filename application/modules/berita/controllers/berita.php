<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class berita extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_berita');
			$this->load->helper('download');
			$this->load->library('form_validation');
		}
		
		public function index(){
			$id=get_id_personal();
			$data['berita']=$this->m_berita->get_berita($id);
			render("berita/berita", $data);
		}
		
		public function add_berita(){
			render("berita/add_berita", $data);
		}
		
		public function edit($id){
			$data['data_edit'] = $this->m_berita->get_data_edit($id);
			render("berita/edit_berita", $data);
		}
		
		public function delete($id){
			$this->m_berita->delete($id);
			redirect(site_url('berita/index/')) ;
		}
		
		public function submit_add(){
			$data['judul_berita'] = $this->input->post('judul_berita');
			$data['tanggal'] = tanggal_db($this->input->post('tanggal'));
			$data['author'] = $this->input->post('author');
			$data['berita'] = $this->input->post('berita');
			
			$config['upload_path'] = './extras/berita/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '10000';
			$config['max_width'] = '3000';
			$config['max_height'] = '3000';
			$this->load->library('upload', $config);
			
			if (!$this->upload->do_upload('foto')){
			 	$this->load->helper('form');
				$error = array('error'=>$this->upload->display_errors());
				// $this->load->view('home/form_upload', $error);
				// print_r($this->upload->data());
			 }
			 else {
				$this->load->helper('form');
				 $upload_data = $this->upload->data();
				 $data['foto']=$upload_data['file_name'];
				 echo $data['foto'];
			
				// $upload_data['judul'] = $judul;
				 // $data = array('upload_data' => $upload_data);
				 // $this->load->view('uploadsample/view_upload_success', $data);
			 }
			
			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";
			$this->m_berita->add($data);
			redirect(site_url('berita/index')) ;
		}
		
		public function submit_edit(){
			$data['judul_berita'] = $this->input->post('judul_berita');
			$data['tanggal'] = tanggal_db($this->input->post('tanggal'));
			$data['author'] = $this->input->post('author');
			$data['berita'] = $this->input->post('berita');
			
			$config['upload_path'] = './extras/berita/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size'] = '10000';
			$config['max_width'] = '3000';
			$config['max_height'] = '3000';
			$this->load->library('upload', $config);
			
			if (!$this->upload->do_upload('foto')){
			 	$this->load->helper('form');
				$error = array('error'=>$this->upload->display_errors());
				// $this->load->view('home/form_upload', $error);
				// print_r($this->upload->data());
			 }
			 else {
				$this->load->helper('form');
				 $upload_data = $this->upload->data();
				 $data['foto']=$upload_data['file_name'];
				 echo $data['foto'];
			
				// $upload_data['judul'] = $judul;
				 // $data = array('upload_data' => $upload_data);
				 // $this->load->view('uploadsample/view_upload_success', $data);
			 }
			
			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";
			$id=$this->input->post('id');
			$this->m_berita->edit($data,$id);
			redirect(site_url('berita/index')) ;
		}
		
}
