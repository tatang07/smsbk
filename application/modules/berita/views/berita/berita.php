<div class="grid_12">
<h1 class="grid_12">Berita</h1>
<div class="grid_12">
	<p class="grid_12">
		Selamat datang di Halaman Berita.<br>
		Halaman ini akan digunakan untuk memposting berita-berita.
		
	</p>
</div>
</div>
<div class="grid_12">			
	<h1 class="grid_12 margin-top no-margin-top-phone">Posting Berita</h1>
	 <div class="box with-table">
		 <div class="tabletools">
			<div class="right">
				
			  	<a href="<?php echo base_url('berita/add_berita/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			

            
        </div>

        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Judul Berita</th>
								<th >Tanggal</th>
								<th >Author</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
						
							<?php $no=1; ?>
							
							<?php foreach($berita as $d):?>
							
								<tr>
									<td class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['judul_berita']?>
									</td class='center'>
									<td class='center'>
									<?php  echo tanggal_view($d['tanggal']);?>
									</td>
									<td class='center'>
									<?php  echo $d['nama'];?>
									</td>
									<td class='center'>
									<a href="<?php echo site_url('berita/edit/'.$d['id_berita'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									
									<a href="<?php echo site_url('berita/delete/'.$d['id_berita'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
								
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>  
	</div>  
</div>

