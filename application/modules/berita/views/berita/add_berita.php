<div class="grid_12">
<h1 class="grid_12">Berita</h1>
<div class="grid_12">
	<p class="grid_12">
		Selamat datang di Halaman Berita.<br>
		Halaman ini akan digunakan untuk memposting berita-berita.
		
	</p>
</div>
</div>
<div class="grid_12">			
	<h1 class="grid_12 margin-top no-margin-top-phone">Posting Berita</h1>

		<form action="<?php echo base_url('berita/berita/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Post</legend>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Judul Berita</strong>
						</label>
						<div>
							<input type="text" name="judul_berita" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal</strong>
						</label>
						<div>
							<input type="date" name="tanggal" />
							<input type="hidden" name="author" value="<?php echo get_id_personal()?>" />
						</div>
					</div>

					<div class="row">
						<label for="f1_normal_input">
							<strong>Berita</strong>
						</label>
						<div>
							<textarea rows="4" cols="50" name="berita"></textarea> 
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Foto</strong>
						</label>
						<div>
							<input type="file" name="foto" />
						</div>
					</div>
			
				</fieldset>
				<div class="actions">
					<div class="left">
						<input type="submit" value="Simpan" name="send" />
						 <a href="<?php echo base_url('berita/berita/index/'); ?>"> <input value="Batal" type="button"></a>
					</div>
					<div class="right">
					</div>
				</div><!-- End of .actions -->
		</form><!-- End of .box -->
</div>

