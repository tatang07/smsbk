<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class profil_sekolah extends Simple_Controller {
    	public $myconf = array(
			//Nama controller
			"name" 			=> "profil_sekolah/profil_sekolah",
			"component"		=> "profil_sekolah",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Profil Sekolah",
			"subtitle"		=> "",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "m_profil_sekolah",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"nss"=>"NSS",
								"nps"=>"NPS",
								"nama_strip"=>"Nama Strip",
								"sekolah"=>"Sekolah",
								"alamat"=>"Alamat",
								"kode_pos"=>"Kode Pos",
								"telepon"=>"Telepon",
								"fax"=>"Fax",
								"sk_pendirian"=>"SK Pendirian",
								"tgl_sk_pendirian"=>"Tanggal SK Pendirian",
								"no_sk_akreditasi"=>"Nomor SK Akreditasi",
								"tgl_akreditasi"=>"Tanggal Akreditasi",
								"email"=>"Email",
								"website"=>"Website",
								"luas_halaman"=>"Luas Halaman",
								"luas_tanah"=>"Luas Tanah",
								"luas_bangunan"=>"Luas Bangunan",
								"luas_olahraga"=>"Luas Olahraga",
								"logo"=>"Logo",
								"foto"=>"Foto",
								"id_jenis_akreditasi"=>"Jenis Akreditasi",
								"id_kecamatan"=>"Kecamatan",
								"id_status_sekolah"=>"Status Sekolah",
								"status_aktif"=>"Status Aktif"
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								"tgl_sk_pendirian"=>"date",
								"tgl_akreditasi"=>"date",
								"id_profil_sekolah"=>"hidden",
								"foto"=>"file",
								"id_jenis_akreditasi"=>array("r_jenis_akreditasi","id_jenis_akreditasi","jenis_akreditasi"),
								"id_status_sekolah"=>array("r_status_sekolah","id_status_sekolah","status_sekolah"),
								"id_kecamatan"=>array("r_kecamatan","id_kecamatan","kecamatan"),
								"status_aktif"=>array("list"=>array("1"=>"Aktif","0"=>"Tidak Aktif"))
								
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"sekolah","alamat"
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								"nss","nps"
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("sekolah","jenis_kelamin","tempat_lahir","tgl_lahir", "hp"),
								"field_sort"			=> array("nama","jenis_kelamin","tempat_lahir","tgl_lahir"),
								"field_filter"			=> array("nama","nip"),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(),
								"field_comparator"		=> array("id_profil_sekolah"=>"=","id_sekolah"=>"="),
								"field_operator"		=> array("id_profil_sekolah"=>"AND","id_sekolah"=>"AND"),
								//"field_align"			=> array(),
								"field_size"			=> array("action_col"=>"150"),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array('detail'=>'profil_sekolah/detail'),
								//"custom_add_link"		=> array('label'=>'Add','href'=>'school/add' ),
								//"custom_edit_link"		=> array(),
								//"custom_delete_link"	=> array(),
								//"custom_link"			=> array("Kurikulum"=>"kurikulum/datalist","Siswa"=>"Siswa/datalist"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("status_sertifikasi","nuptk","kode_profil_sekolah","nip","nama","jenis_kelamin","tempat_lahir","tgl_lahir","alamat","hp","telepon","tgl_diangkat","gaji","foto","status_aktif","id_golongan_darah","id_status_pernikahan","id_agama","id_status_pegawai","id_kecamatan","id_jenjang_pendidikan","id_pangkat_golongan"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								//"redirect_link"			=> ""
							),
			//Konfigurasi tampilan edit
			/*"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"view_file"				=> "simple/edit",
								"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array(),
								"custom_action"			=> "",
								"msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								"msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);
	

		public function pre_process($data){
			//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
			//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
			
			
			//$id_pihak = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			//$list_pihak_komunikasi = get_list("m_profil_sekolah","id_profil_sekolah","kode_profil_sekolah");
			$data['conf']['data_list']['subtitle'] = "Daftar profil_sekolah";
			$data['conf']['data_add']['subtitle'] = "Penambahan profil_sekolah";
			$data['conf']['data_edit']['subtitle'] = "Edit profil_sekolah";
			
			$data['conf']['data_delete']['redirect_link'] = "profil_sekolah/datalist";
			$data['conf']['data_add']['redirect_link'] = "profil_sekolah/datalist";
			$data['conf']['data_edit']['redirect_link'] = "profil_sekolah/datalist";
			
			return $data;
		}
		
		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
			// $id_profil_sekolah = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			// $_POST['filter']['id_sekolah'] = get_id_sekolah();
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															// 'id_profil_sekolah'=>$id_profil_sekolah,
															// 'id_sekolah'=>get_id_sekolah() 
														);
			
			//Mengganti link add yang ada di kanan atas form datalist
			$data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'profil_sekolah/add');			
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'profil_sekolah/edit/');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'profil_sekolah/delete/');
			
			return $data;
		}
		
		public function before_datatable($data){
			//Method ini hanya akan dieksekusi di awal method datatable
			//Mengambil id_pihak yang diposting dari form datalist
			$filter = $this->input->post("filter");
			//echo "<br><br><br><br><br><br><br><br>";
			//$this->mydebug($filter);
			//$id_profil_sekolah = $filter['id_sekolah'];
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'profil_sekolah/edit/');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'profil_sekolah/delete/');
			
			return $data;
		}
		
				
		public function before_render_add($data){
			if(!$this->input->post()){
				//$id_pihak = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
				//mengisi nilai untuk id_pihak_komunikasi di form input
				//$_POST['item']['id_pihak_komunikasi'] = $id_pihak; //name='item[id_pihak_kom..]'
			
			}
				
			return $data;
		}
		
		public function before_add($data){
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
				$data['item']['id_sekolah'] = get_id_sekolah();
				
				$config['upload_path'] = './extras/profil_sekolah/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '100';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$config['max_height']  = '768';

				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload("item[foto]"))
					{
						set_error_message($this->upload->display_errors());

					//	$this->load->view('upload_form', $error);
					}
					else
					{
						$data = array('upload_data' => $this->upload->data());
						set_error_message("upload sukses");
					}

			}
			return $data;
		}
				
		public function before_edit($data){
			if(!$this->input->post()){
				$data['id'] = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 0;
			}else{
				$config['upload_path'] = './extras/profil_sekolah/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '1000';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$config['max_height']  = '768';

				$this->load->library('upload', $config);
				
				set_error_message(print_r($_FILES));
				
				if ( ! $this->upload->do_upload("item"))
					{
						
						//set_error_message("gagal");
						//set_error_message($this->upload->display_errors());

					//	$this->load->view('upload_form', $error);
					}
					else
					{
						$data = array('upload_data' => $this->upload->data());
						//set_error_message(print_r($_FILES['item']));
						//set_error_message("upload sukses");
					}
			}
			return $data;
		}
		
		public function before_delete($data){
			$data['id'] = $this->uri->rsegment(3);
			return $data;
		}
		
		public function detail(){
			$this->load->model('m_profil_sekolah');
			$id = $this->uri->rsegment(3);
			$data['detail'] = $this->m_profil_sekolah->getdetail($id);
			//$this->mydebug($data);
			render('detail', $data);
		}

}
