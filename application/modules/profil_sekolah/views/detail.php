
<h1 class="grid_12">Guru</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Detail Guru</h2>
       </div>
       
		<div class="content">
			<div class="dataTables_wrapper"  role="grid">    
				<table class="styled" >
					<tbody>
					<?php foreach($detail as $details):?>
						<tr>
							<td>Status Sertifikasi</td>
							<td><?php echo $details['status_sertifikasi']; ?></td>
						</tr>
						<tr>
							<td>NUPTK</td>
							<td><?php echo $details['nuptk']; ?></td>
						</tr>
						<tr>
							<td>Kode Guru</td>
							<td><?php echo $details['kode_guru']; ?></td>
						</tr>
						<tr>
							<td>NIP</td>
							<td><?php echo $details['nip']; ?></td>
						</tr>
						<tr>
							<td>Nama</td>
							<td><?php echo $details['nama']; ?></td>
						</tr>
						<tr>
							<td>Jenis Kelamin</td>
							<td><?php if($details['jenis_kelamin']=='l')echo 'Laki-laki';
										else echo 'perempuan'; ?></td>
						</tr>
						<tr>
							<td>Tempat Lahir</td>
							<td><?php echo $details['tempat_lahir']; ?></td>
						</tr>
						<tr>
							<td>Tanggal Lahir</td>
							<td><?php echo $details['tgl_lahir']; ?></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td><?php echo $details['alamat']; ?></td>
						</tr>
						<tr>
							<td>Nomor HP</td>
							<td><?php echo $details['hp']; ?></td>
						</tr>
						<tr>
							<td>Nomor Telepon</td>
							<td><?php echo $details['telepon']; ?></td>
						</tr>
						<tr>
							<td>Tanggal Diangkat</td>
							<td><?php echo $details['tgl_diangkat']; ?></td>
						</tr>
						<tr>
							<td>Gaji</td>
							<td><?php echo $details['gaji']; ?></td>
						</tr>
						<tr>
							<td>Foto</td>
							<td><?php echo $details['foto']; ?></td>
						</tr>
						<tr>
							<td>Status Aktif</td>
							<td><?php if($details['status_sertifikasi']==1) echo 'Aktif'; 
										else echo 'Tidak Aktif'; ?></td>
						</tr>
						<tr>
							<td>Golongan Darah</td>
							<td><?php echo $details['golongan_darah']; ?></td>
						</tr>
						<tr>
							<td>Status Pernikahan</td>
							<td><?php echo $details['status_pernikahan']; ?></td>
						</tr>
						<tr>
							<td>Agama</td>
							<td><?php echo $details['agama']; ?></td>
						</tr>
						<tr>
							<td>Status Pegawai</td>
							<td><?php echo $details['status']; ?></td>
						</tr>
						<tr>
							<td>Kecamatan</td>
							<td><?php echo $details['kecamatan']; ?></td>
						</tr>
						<tr>
							<td>Jenjang Pendidikan</td>
							<td><?php echo $details['jenjang_pendidikan']; ?></td>
						</tr>
						<tr>
							<td>Pangkat Golongan</td>
							<td><?php echo $details['pangkat']." - ".$details['golongan']; ?></td>
						</tr>
						
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
        </div>        
    </div>
 </div>
