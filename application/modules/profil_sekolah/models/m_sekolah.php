<?php
class m_guru extends MY_Model {
    public $table = "m_sekolah";
    public $id = "id_sekolah";
	
	//                  join                    ON 
	// public $join_to = array(
			// "r_golongan_darah gd"=>"gd.id_golongan_darah=m_guru.id_golongan_darah",
			// "r_status_pernikahan sp"=>"sp.id_status_pernikahan=m_guru.id_status_pernikahan",
			// "r_agama a"=>"a.id_agama=m_guru.id_agama",
			// "r_status_pegawai rsp"=>"rsp.id_status_pegawai=m_guru.id_status_pegawai",
			// "r_kecamatan k"=>"k.id_kecamatan=m_guru.id_kecamatan",
			// "r_jenjang_pendidikan jp"=>"jp.id_jenjang_pendidikan=m_guru.id_jenjang_pendidikan",
			// "r_pangkat_golongan pg"=>"pg.id_pangkat_golongan=m_guru.id_pangkat_golongan"
			// "m_sekolah s"=>"s.id_sekolah=m_guru.id_sekolah"
		// );
	// public $join_fields = array("golongan_darah","status_pernikahan","agama","status","kecamatan","jenjang_pendidikan","pangkat","golongan");
	
	function getdetail($id=0){
		$this->db->select('*');
		$this->db->from('m_sekolah s'); 
		$this->db->join('r_jenis_akreditasi ja', 'ja.id_jenis_akreditasi=s.id_jenis_akreditasi', 'left');
		$this->db->join('r_status_sekolah ss', 'ss.id_status_sekolah=s.id_status_sekolah', 'left');
		$this->db->join('r_kecamatan k', 'k.id_kecamatan=s.id_kecamatan', 'left');
		$this->db->where('s.id_sekolah',$id);
		$this->db->order_by('s.sekolah','asc');         
		$query = $this->db->get();
		
		if($query->num_rows() != 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	
	}
}