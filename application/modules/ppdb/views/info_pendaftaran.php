<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>
<div class="grid_12">
	<?php if(count($info_pendaftaran) >= 1){ ?>
	    <div class="box with-table">
	        <div class="header">
	            <h2><?php echo $info_pendaftaran['information_label']; ?></h2>
	        </div>
	        
			<div class="content" style="padding-left:20px;padding-right:20px;padding-top:10px;">
				<div class="right" style="float:right;">
				  	<a href="<?php echo site_url('information/edit/ppdb_info_pendaftaran')?>"><i class="icon-edit"></i> Edit</a> 
	            </div>
				<h3>
					<?php echo $info_pendaftaran['information_data']; ?>
				</h3>
			</div>
		</div>		
	<?php }else{?>
		<div class="box with-table">
			<div class="content" style="padding-left:20px;padding-right:20px;padding-top:10px;">
				<div class="right" style="float:right;">
				  	<a href="<?php echo site_url('information/tambah/ppdb_info_pendaftaran')?>"><i class="icon-plus"></i> Tambah</a> 
	            </div>
				<h3>
					Data Belum Ada !
				</h3>
			</div>
		</div>	
	<?php }?>
 </div>