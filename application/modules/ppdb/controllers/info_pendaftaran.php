<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class info_pendaftaran extends Simple_Controller {
	public function index(){
		$name = "ppdb_info_pendaftaran";
		$data['info_pendaftaran'] = get_information_sekolah($name);
		render("info_pendaftaran",$data);
	}
}
