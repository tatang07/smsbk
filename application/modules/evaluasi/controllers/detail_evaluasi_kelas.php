<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class detail_evaluasi_kelas extends MY_Controller {

		public function datalist(){
			$this->load->model('t_nilai');
			$this->load->model('t_nilai_detail');
			$this->load->model('t_rombel_detail');
			$this->load->model('t_guru_matpel_rombel');
			$this->load->model('t_nilai');
			$this->load->model('t_nilai_kategori');
			
			$id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
			$id_gmr = isset($_POST['id_gmr'.$id_rombel])?$_POST['id_gmr'.$id_rombel]:0;
			$id_komponen_nilai = isset($_POST['id_komponen_nilai'.$id_rombel])?$_POST['id_komponen_nilai'.$id_rombel]:0;
			$id_nilai_kategori = isset($_POST['id_nilai_kategori'.$id_komponen_nilai])?$_POST['id_nilai_kategori'.$id_komponen_nilai]:0;
			$id_term = get_id_term();		
			
			//$nkat = $this->t_nilai_kategori->get($id_nilai_kategori);
			//$gmr = $this->t_guru_matpel_rombel->get($id_gmr);
						
			$data["nilai_detail"] = $this->t_nilai_detail->get_nilai($id_gmr,$id_nilai_kategori, $id_term);
			$data["siswa"] = $this->t_rombel_detail->get_siswa_rombel($id_rombel);
			$data["nilai"] = $this->t_nilai->get_by_fields(array("id_nilai_kategori"=>$id_nilai_kategori,
																 "id_term"=>$id_term,
																 "id_guru_matpel_rombel"=>$id_gmr
																)
														);
			$data["id_rombel"] = $id_rombel;
			$data["id_gmr"] = $id_gmr;
			$data["id_nilai_kategori"] = $id_nilai_kategori;
			$data["id_komponen_nilai"] = $id_komponen_nilai;
			$data["component"] = "evaluasi_pembelajaran";
			$data['gid'] = $gid = get_usergroup();
			
			render('detail_evaluasi_kelas/datalist', $data);
		}
		
		public function tambah($id_gmr, $id_nilai_kategori){
			$this->load->model('t_guru_matpel_rombel');
			$this->load->model('t_rombel_detail');
			
			$gmr = $this->t_guru_matpel_rombel->get_gmr($id_gmr);
			$data["id_gmr"] = $id_gmr;
			$data["id_nilai_kategori"] = $id_nilai_kategori;
			$data["id_term"] = get_id_term();
			if($gmr){
			$data["nama"] = $gmr->nama;
			$data["rombel"] = $gmr->rombel;
			$id_rombel = $gmr->id_rombel;
			$data["pelajaran"] = $gmr->pelajaran;
			}else{
				set_error_message('pilih terlebih dahulu kelas, mata pelajaran, komponen, dan penilaian');
				redirect('evaluasi/detail_evaluasi_kelas/datalist');
			}
			
			$row = $this->t_guru_matpel_rombel->get_penilaian($id_nilai_kategori);
			$data["nilai_kategori"] = $id_nilai_kategori;
			$data["penilaian"] = $row->nama_nilai_kategori;
			$data["tunggal"] = $row->bernilai_tunggal;
			$data["komponen"] = $row->komponen_nilai;
			$data["siswa"] = $this->t_rombel_detail->get_siswa_rombel($id_rombel);
			// $data["siswa"] = $this->t_guru_matpel_rombel->get_siswa($id_gmr);
			
			$row = $this->t_guru_matpel_rombel->get_nilai($id_nilai_kategori, $id_gmr, get_id_term());
			
			$data["component"] = "evaluasi_pembelajaran";
			
			if($data["tunggal"]==1&&$row){
				set_error_message('penilaian sudah ada nilainya, silahkan lakukan edit');
				redirect('evaluasi/detail_evaluasi_kelas/datalist');
			}else{
				render('detail_evaluasi_kelas/tambah', $data);
			}
		}
		
		public function edit($id_gmr, $id_nilai_kategori){
			$this->load->model('t_guru_matpel_rombel');
			$this->load->model('t_nilai');
			$this->load->model('t_nilai_detail');
			$this->load->model('t_rombel_detail');
			
			$gmr = $this->t_guru_matpel_rombel->get_gmr($id_gmr);
			
			$id_term = get_id_term();	
			
			$data["id_gmr"] = $id_gmr;
			$data["id_nilai_kategori"] = $id_nilai_kategori;
			
			if($gmr){
			$data["nama"] = $gmr->nama;
			$data["rombel"] = $gmr->rombel;
			$id_rombel = $gmr->id_rombel;
			$data["pelajaran"] = $gmr->pelajaran;
			}else{
				set_error_message('pilih terlebih dahulu kelas, mata pelajaran, komponen, dan penilaian');
				redirect('evaluasi/detail_evaluasi_kelas/datalist');
			}
			
			$data["siswa"] = $this->t_rombel_detail->get_siswa_rombel($id_rombel);
			$row = $this->t_guru_matpel_rombel->get_penilaian($id_nilai_kategori);
			$data["penilaian"] = $row->nama_nilai_kategori;
			$data["tunggal"] = $row->bernilai_tunggal;
			$data["komponen"] = $row->komponen_nilai;
			// $data["siswa"] = $this->t_guru_matpel_rombel->get_siswa($id_gmr);
			$data["nilai_detail"] = $this->t_nilai_detail->get_nilai($id_gmr, $id_nilai_kategori, $id_term);
			
			$id_term = get_id_term();
			
			$data["nilai"] = $this->t_nilai->get_by_fields(array("id_nilai_kategori"=>$id_nilai_kategori,
																 "id_guru_matpel_rombel"=>$id_gmr,
																 "id_term"=>$id_term
																)
														);
			// echo "<pre>";
			// print_r($data["nilai_detail"]);
			// echo "</pre>";
			$data["component"] = "evaluasi_pembelajaran";
			
			render('detail_evaluasi_kelas/edit', $data);
		}
		
		public function submit(){
			$this->load->model('t_nilai_detail');
			$this->load->model('t_nilai');
			
			$nilai=$this->input->post('nilai_angka');

			// $id_standar_kompetensi = $this->input->post('id_standar_kompetensi');
			// $kompetensi_dasar = $this->input->post('kompetensi_dasar');
			// $id_kompetensi_dasar = $this->input->post('id_kompetensi_dasar');
			
			$action = $this->input->post('action');
			
			if($action == 'edit'){
				
				$id=$this->input->post('id');
				$kategori = $this->input->post('kategori');
				
				foreach($nilai as $id_s => $ns){
					foreach($ns as $id_n => $n){
						$data['id_siswa'] = $id_s;
						$data['id_nilai'] = $id_n;
						$data['nilai'] = $n;
						$data['id_nilai_detail'] = $id[$id_s][$id_n];
						if($data['id_nilai_detail']==0){
							$data1['id_siswa'] = $id_s;
							$data1['id_nilai'] = $id_n;
							$data1['nilai'] = $n;
							$this->t_nilai_detail->save($data1);
						}else{
							$this->t_nilai_detail->update($data);
						}
					}
				}
				
				set_success_message('Data Berhasil Diupdate');
				redirect("evaluasi/detail_evaluasi_kelas/datalist");
			}elseif($action == 'tambah'){
				
				$nama_nilai=$this->input->post('nilai');
				$gmr=$this->input->post('gmr');
				$nilai_kategori=$this->input->post('nilai_kategori');
				$term=$this->input->post('term');
				
				$kategori = $this->input->post('kategori');
				
				$data['id_nilai_kategori']=$nilai_kategori;
				$data['id_guru_matpel_rombel']=$gmr;
				$data['id_term']=$term;
				if($kategori==1){
					$nama_nilai="";
				}
				$data['nama_nilai']=$nama_nilai;
				$data['tanggal_penilaian']=date("Y-m-d");
				
				$cek = $this->t_nilai->get_by_fields(array("id_nilai_kategori"=>$nilai_kategori,
																 "id_term"=>$term,
																 "id_guru_matpel_rombel"=>$gmr,
																 "nama_nilai"=>$nama_nilai,
																)
														);
				if(!$cek){
					$this->t_nilai->save($data);
					$cek = $this->t_nilai->get_by_fields(array("id_nilai_kategori"=>$nilai_kategori,
																 "id_term"=>$term,
																 "id_guru_matpel_rombel"=>$gmr,
																 "nama_nilai"=>$nama_nilai,
																)
														);
					foreach($nilai as $id_s => $n){
						if($n==""){
							$n=0;
						}
						$data1['id_siswa'] = $id_s;
						$data1['id_nilai'] = $cek[0]['id_nilai'];
						$data1['nilai'] = $n;
						$this->t_nilai_detail->save($data1);
					}
					set_success_message('Data Berhasil ditambah');
					redirect("evaluasi/detail_evaluasi_kelas/datalist");
				}else{									
					set_error_message('nama nilai sudah ada, silahkan ganti nama');
					// $data['nilai']=$nilai;
					
					$this->tambah($gmr,$nilai_kategori);
					// render("evaluasi/detail_evaluasi_kelas/tambah/".$gmr."/".$nilai_kategori, $data);
				}
			}
		}

}
