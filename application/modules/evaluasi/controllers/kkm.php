<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Kkm extends Simple_Controller {

	public $myconf = array(
		//Nama controller
		"name" 			=> "evaluasi/kkm",
		"component"		=> "evaluasi_pembelajaran",
		//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
		//dan subtitile general ini akan ter-replace
		"title"			=> "Kriteria Ketuntasan Belajar",
		"subtitle"		=> "",
		//"view_file_index"=> "simple/index",
		//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
		//tidak ditentukan, maka akan digunakan model ini
		//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
		//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
		"model_name"	=> "m_kkm",
		//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
		//Format: nama_field=>"Labelnya"
		"data_label"	=> array(		
			"kode_pelajaran"=>"Kode MatPel",
			"pelajaran"=>"Pelajaran",
			"id_pelajaran"=>"Pelajaran",
			"nilai_kkm"=>"KKM",
			"file_dokumen_kkm"=>"Dokumen KKM"
		),
		//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
		//Tipe default jika tidak didefinisikan: String, 
		//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
		//Format: nama_field => tipe_data_nya
		"data_type"		=> array(
			"file_dokumen_kkm"=>"file",
			"id_pelajaran" =>array("m_pelajaran","id_pelajaran","pelajaran"),
		),
		//Field-field yang harus diisi
		"data_mandatory"=> array(),
		//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
		"data_unique"	=> array(),
		//Konfigurasi tampilan datalist
		"data_list"		=> array(
			//"title"			=> "simple",
			//"subtitle"		=> "simple",
			//"model_name"			=> "",
			//"table"					=> array(),
			//"view_file"				=> "guru/datalist",
			//"view_file_datatable"	=> "simple/datatable",
			//"view_file_datatable_action"	=> "simple/datatable_action",
			"field_list"			=> array("kode_pelajaran","pelajaran","nilai_kkm"),
			"field_sort"			=> array("kode_pelajaran","pelajaran","nilai_kkm"),
			"field_filter"			=> array(),
			"field_filter_hidden"	=> array(),
			"field_filter_dropdown"	=> array(),
			"field_comparator"		=> array("t_kkm.id_sekolah"=>"=","id_tingkat_kelas"=>"=","id_kurikulum"=>"=","id_tahun_ajaran"=>"="),
			"field_operator"		=> array("t_kkm.id_sekolah"=>"AND","id_tingkat_kelas"=>"AND","id_kurikulum"=>"AND","id_tahun_ajaran"=>"AND"),
								//"field_align"			=> array(),
			"field_size"			=> array("action_col"=>"240"),
			//"field_separated"		=> array(),
			//"field_sum"				=> array(),
			//"field_summary"			=> array(),
			//"enable_action"			=> TRUE,
			"custom_action_link"	=> array("Download Dokumen KKM" => 'evaluasi/kkm/download'),
			"custom_add_link"		=> array('label'=>'Edit','href'=>'evaluasi/kkm/tambah'),
			//"custom_edit_link"		=> array(),
			//"custom_delete_link"	=> array(),
			//"custom_link"			=> array("Kurikulum"=>"kurikulum/datalist","Siswa"=>"Siswa/datalist"),
		),
		//Konfigurasi tampilan add				
		"data_add"		=> array(
			//"title"			=> "simple",
			//"subtitle"		=> "simple",
			//"enable"				=> TRUE,
			//"model_name"			=> "",
			//"table"					=> array(),
			//"view_file"				=> "simple/add",
			//"view_file_form_input"	=> "simple/form_input",
			// "field_list"			=> array("id_pelajaran","nilai_kkm","file_dokumen_kkm"),
			//"custom_action"			=> "",
			//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
			//"msg_on_success"		=> "Data berhasil disimpan.",
			//"redirect_link"			=> ""
		),
		//Konfigurasi tampilan edit
		"data_edit"		=> array(
			//"title"			=> "simple",
			//"subtitle"		=> "simple",
			"enable"				=> false,
			// "model_name"			=> "",
			// "table"					=> array(),
			// "view_file"				=> "simple/edit",
			// "view_file_form_input"	=> "simple/form_input",
			// "field_list"			=> array(),
			// "custom_action"			=> "",
			// "msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
			// "msg_on_success"		=> "Data berhasil diperbaharui.",
			// "redirect_link"			=> ""
		),
		//Konfigurasi tampilan delete
		"data_delete"	=> array(
			"enable"				=> false
			// "model_name"			=> "",
			// "table"					=> array(),
			// "msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
			// "msg_on_success"		=> "Data berhasil dihapus.",
			// "redirect_link"			=> ""
		),
		/*//Konfigurasi tampilan export
		"data_export"	=> array(
			"enable_pdf"			=> TRUE,
			"enable_xls"			=> TRUE,
			"view_pdf"				=> "simple/laporan_datalist",
			"view_xls"				=> "simple/laporan_datalist",
			"field_size"			=> array(),
			"orientation"			=> "p",
			"special_number"		=> "",
			"enable_header"			=> TRUE,
			"enable_footer"			=> TRUE,
			)*/
	);

	function index()
	{
		redirect('evaluasi/kkm/datalist');
	}
	

	public function pre_process($data)
	{
		//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
		//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai


		//$id_pihak = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
		//$list_pihak_komunikasi = get_list("m_guru","id_guru","kode_guru");
		// $data['conf']['data_list']['subtitle'] = "Daftar Tenaga Pendidik";
		// $data['conf']['data_add']['subtitle'] = "Penambahan Tenaga Pendidik";
		// $data['conf']['data_edit']['subtitle'] = "Edit Tenaga Pendidik";

		// $data['conf']['data_delete']['redirect_link'] = "ptk/guru/datalist";
		// $data['conf']['data_add']['redirect_link'] = "ptk/guru/datalist";
		// $data['conf']['data_edit']['redirect_link'] = "ptk/guru/datalist";

		return $data;
	}

	public function before_datalist($data)
	{
		$this->load->model('evaluasi/t_nilai_kategori');
		$this->load->model('pps/m_penentuan_kelas');

		//Method ini hanya akan dieksekusi di awal method datalist
		//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
		// $id_guru = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
		$_POST['filter']['t_kkm.id_sekolah'] = get_id_sekolah();
		$_POST['filter']['t_kkm.id_tahun_ajaran'] = get_id_tahun_ajaran();

		$gid = get_usergroup();
		if($gid == 9){
			$data['conf']['data_list']['enable_action']=false;
			$data['conf']['data_add']['enable']=false;
			$data['conf']['data_edit']['enable']=false;
			$data['conf']['data_delete']['enable']=false;
		}
			
		$d['kurikulum'] = array();
		$kurikulum = $this->t_nilai_kategori->get_kurikulum();
		if($kurikulum){
			foreach ($kurikulum as $kur) {
				$d['kurikulum'][$kur['id_kurikulum']] = $kur['nama_kurikulum'];
			}
		}
		$d['tingkat_kelas'] = $this->m_penentuan_kelas->get_tingkat_kelas(array(), true);

		$data['conf']['data_list']["field_filter_dropdown"]	= array(
					"id_kurikulum"=>array("label"=>"Kurikulum","list"=>$d['kurikulum']),
					"id_tingkat_kelas"=>array("label"=>"Tingkat Kelas","list"=>$d['tingkat_kelas'])
					);
		//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
		//mengupdate datatable baik ketika search ataupun pindah page
		// $data['conf']['data_list']['field_filter_hidden'] = array(
		// 												// 'id_guru'=>$id_guru,
		// 												// 'id_sekolah'=>get_id_sekolah() 
		// 											);

		//Mengganti link add yang ada di kanan atas form datalist
		// $data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'evaluasi/kkm/add');

		//Memanipulasi parameter page
		$page = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 1;
		$data['page'] = $page;
		//Mengkustomisasi link tombol edit dan delete
		$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'evaluasi/kkm/edit');
		$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'evaluasi/kkm/delete');

		return $data;
	}

	public function before_datatable($data){
				//Method ini hanya akan dieksekusi di awal method datatable
				//Mengambil id_pihak yang diposting dari form datalist
		$_POST['filter']['t_kkm.id_sekolah'] = get_id_sekolah();
		$filter = $this->input->post("filter");
				//echo "<br><br><br><br><br><br><br><br>";
				//$this->mydebug($filter);
				//$id_guru = $filter['id_sekolah'];
				//Mengkustomisasi link tombol edit dan delete
		$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'evaluasi/kkm/edit');
		$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'evaluasi/kkm/delete');

		return $data;
	}


	public function before_render_add($data){
		if(!$this->input->post()){
					//$id_pihak = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
					//mengisi nilai untuk id_pihak_komunikasi di form input
					//$_POST['item']['id_pihak_komunikasi'] = $id_pihak; //name='item[id_pihak_kom..]'

		}

		return $data;
	}

	public function before_add($data){
		if($this->input->post()){
					//menambahkan satu field id_sekolah sebelum data disimpan ke db
			$data['item']['id_sekolah'] = get_id_sekolah();
			$data['item']['id_tahun_ajaran'] = get_id_tahun_ajaran();

			if($_FILES["item"]["error"]["file_dokumen_kkm"]==0){
				$fname = date("YmdHisu");
				$ext = pathinfo($_FILES["item"]["tmp_name"]["file_dokumen_kkm"], PATHINFO_EXTENSION);
				$src = 'extras/kkm/'. $fname.".".$ext;
				move_uploaded_file($_FILES["item"]["tmp_name"]["file_dokumen_kkm"],$src);
				$data['item']['file_dokumen_kkm'] = $fname.".".$ext;
			}
		}
		return $data;
	}

	public function before_edit($data){
		if(!$this->input->post()){
			$data['id'] = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 0;
		}else{
					// $data['item']['id_sekolah'] = get_id_sekolah();

			if($_FILES["item"]["error"]["file_dokumen_kkm"]==0){
				$fname = date("YmdHisu");
				$src = 'extras/kkm/'. $fname.".jpg";
				move_uploaded_file($_FILES["item"]["tmp_name"]["file_dokumen_kkm"],$src);
				$data['item']['file_dokumen_kkm'] = $fname.".jpg";
						// $this->mydebug($data);

				$config['image_library'] = 'gd2';
				$config['source_image'] = $src;
				$config['maintain_ratio'] = TRUE;
				$config['width'] = 200;
				$config['height'] = 200;
				$this->load->library('image_lib', $config);
				$this->image_lib->resize();
			}
		}
		return $data;
	}

	public function before_delete($data){
		$data['id'] = $this->uri->rsegment(3);
		return $data;
	}

	function tambah($id_kurikulum = false, $id_tingkat_kelas = false)
	{
		$this->load->model('evaluasi/t_nilai_kategori');
		$this->load->model('evaluasi/m_kkm');
		$this->load->model('pps/m_penentuan_kelas');

		$data['kurikulum'] = array('- pilih kurikulum -');
		$kurikulum = $this->t_nilai_kategori->get_kurikulum();
		if($kurikulum){
			foreach ($kurikulum as $kur) {
				$data['kurikulum'][$kur['id_kurikulum']] = $kur['nama_kurikulum'];
			}
		}

		if($id_kurikulum){
			$data['tingkat_kelas'] = $this->m_penentuan_kelas->get_tingkat_kelas(array(), true);
		}

		if($id_tingkat_kelas){
			// $data['pelajaran'] = array('- pilih pelajaran -');
			$data['pelajaran'] = $this->m_kkm->get_pelajaran($id_kurikulum, $id_tingkat_kelas);
			
			$arr = array();
			
			foreach($data['pelajaran'] as $p){
				$row = $this->m_kkm->get_kkm_by_pelajaran($p['id_pelajaran']);
				
				if($row){
					$arr[$p['id_pelajaran']]['pelajaran'] = $p['pelajaran'];
					$arr[$p['id_pelajaran']]['nilai'] = $row['nilai_kkm'];
					$arr[$p['id_pelajaran']]['file'] = $row['file_dokumen_kkm'];
				}else{
					$arr[$p['id_pelajaran']]['pelajaran'] = $p['pelajaran'];
					$arr[$p['id_pelajaran']]['nilai'] = '';
					$arr[$p['id_pelajaran']]['file'] = '';
				}
			}
			
			$data['nilai'] = $arr;
			
				// echo '<pre>';
				// print_r($arr);
				// echo '</pre>';
			// if($pelajaran){
				// foreach ($pelajaran as $pel) {
					// $data['pelajaran'][$pel['id_pelajaran']] = $pel['pelajaran'];
				// }
			// }
		}

		if($post = $this->input->post())
		{
			unset($post['id_kurikulum']);
			unset($post['id_tingkat_kelas']);

			foreach($post['nilai_kkm'] as $p=>$r){
				if($r){
					if($_FILES["file_dokumen_kkm".$p]["error"]==0){
						$fname = date("YmdHisu");
						$ext = pathinfo($_FILES["file_dokumen_kkm".$p]["name"], PATHINFO_EXTENSION);
						$src = 'extras/kkm/'. $fname.".".$ext;
						move_uploaded_file($_FILES["file_dokumen_kkm".$p]["tmp_name"],$src);
						$isi['file_dokumen_kkm'] = $fname.".".$ext;
					}
					$isi['id_tahun_ajaran'] = get_id_tahun_ajaran();
					$isi['id_sekolah'] = get_id_sekolah();
					$isi['id_pelajaran'] = $p;
					$isi['nilai_kkm'] = $r;

					$row = $this->m_kkm->get_kkm_by_pelajaran($p);
				
					if($row){
						$this->m_kkm->update($isi, $row['id_kkm']);
					}else{
						$this->m_kkm->insert($isi);
					}
						set_success_message('data berhasil ditambahkan');
					
					// echo '<pre>';
					// print_r($isi);
					// echo '</pre>';
				}

			}

			redirect('evaluasi/kkm/datalist/'.$id_kurikulum.'/'.$id_tingkat_kelas);
		}

		
		$data["id_kurikulum"] = $id_kurikulum;
		$data["id_tingkat_kelas"] = $id_tingkat_kelas;
		$data["component"] = "evaluasi_pembelajaran";
		render('kkm/tambah', $data);
	}

	function edit($id = false)
	{
		if(! $id) show_404();

		$this->load->model('evaluasi/m_kkm');

		if(! $data['kkm'] = $this->m_kkm->get_kkm($id)) show_404();

		if($this->input->post())
		{
			$post['nilai_kkm'] = $this->input->post('nilai_kkm');

			if($_FILES["file_dokumen_kkm"]["error"]==0){
				// hapus dulu file lama
				unlink('extras/kkm/'. $data['kkm']['file_dokumen_kkm']);

				$fname = date("YmdHisu");
				$ext = pathinfo($_FILES["file_dokumen_kkm"]["name"], PATHINFO_EXTENSION);
				$src = 'extras/kkm/'. $fname.".".$ext;
				move_uploaded_file($_FILES["file_dokumen_kkm"]["tmp_name"],$src);
				$post['file_dokumen_kkm'] = $fname.".".$ext;
			}

			$post['id_tahun_ajaran'] = get_id_tahun_ajaran();
			$post['id_sekolah'] = get_id_sekolah();

			if($this->m_kkm->update($post, $id))
				set_success_message('data berhasil ditambahkan');
			else
				set_error_message('data gagal ditambahkan');

			redirect('evaluasi/kkm/datalist/'.$id_kurikulum.'/'.$id_tingkat_kelas);
		}

		render('kkm/edit', $data);
	}

	function download($id = false)
	{
		if(! $id) show_404();

		$this->load->model('m_kkm');
		$this->load->helper('download');

		$data = $this->m_kkm->get_kkm($id);
		$ext = pathinfo($data['file_dokumen_kkm'], PATHINFO_EXTENSION);
		$name = 'KKM_'.url_title($data['pelajaran']).'_'.$data['tahun_awal'].'-'.$data['tahun_akhir'].'.'.$ext;

		if(file_exists('extras/kkm/'.$data['file_dokumen_kkm'])){
			$content = file_get_contents(base_url().'extras/kkm/'.$data['file_dokumen_kkm']);
			force_download($name, $content);
		} else {
			set_error_message('File tidak ditemukan di server.');
			redirect('evaluasi/kkm');
		}

	}

}
