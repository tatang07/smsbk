<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class komponen_evaluasi extends MY_Controller {

	function index()
	{
		redirect('evaluasi/komponen_evaluasi/datalist');
	}

	public function datalist($id_kurikulum = false, $id_komponen_nilai = false)
	{
		$this->load->model('t_nilai_kategori');

		$data['kurikulum'] = array('- pilih kurikulum -');
		$kurikulum = $this->t_nilai_kategori->get_kurikulum();
		if($kurikulum){
			foreach ($kurikulum as $kur) {
				$data['kurikulum'][$kur['id_kurikulum']] = $kur['nama_kurikulum'];
			}
		}

		$data['komponen_nilai'] = array('- pilih komponen nilai -');
		if($id_kurikulum){
			$komponen_nilai = $this->t_nilai_kategori->get_komponen_nilai($id_kurikulum);
			if($komponen_nilai){
				foreach ($komponen_nilai as $kur) {
					$data['komponen_nilai'][$kur['id_komponen_nilai']] = $kur['komponen_nilai'];
				}
			}
		}

		if($id_kurikulum && $id_komponen_nilai){
			$data["nilai"] = $this->t_nilai_kategori
									->get_by_fields(array(
										"id_komponen_nilai"=>$id_komponen_nilai,
										"id_tahun_ajaran"=>get_id_tahun_ajaran(),
										"id_sekolah"=>get_id_sekolah()
										)
									);
		}

		$data["id_kurikulum"] = $id_kurikulum;
		$data["id_komponen_nilai"] = $id_komponen_nilai;
		$data['gid'] = $gid = get_usergroup();
		$data["component"] = "evaluasi_pembelajaran";
		render('komponen_evaluasi/datalist', $data);
	}

	function tambah($id_kurikulum = false, $id_komponen_nilai = false)
	{
		$this->load->model('t_nilai_kategori');

		$data['kurikulum'] = array('- pilih kurikulum -');
		$kurikulum = $this->t_nilai_kategori->get_kurikulum();
		if($kurikulum){
			foreach ($kurikulum as $kur) {
				$data['kurikulum'][$kur['id_kurikulum']] = $kur['nama_kurikulum'];
			}
		}

		$data['komponen_nilai'] = array('- pilih komponen nilai -');
		if($id_kurikulum){
			$komponen_nilai = $this->t_nilai_kategori->get_komponen_nilai($id_kurikulum);
			if($komponen_nilai){
				foreach ($komponen_nilai as $kur) {
					$data['komponen_nilai'][$kur['id_komponen_nilai']] = $kur['komponen_nilai'];
				}
			}
		}

		if($post = $this->input->post()){
			$post['id_sekolah'] = get_id_sekolah();
			$post['id_tahun_ajaran'] = get_id_tahun_ajaran();

			unset($post['id_kurikulum']);

			if(!$this->t_nilai_kategori->save($post))
				set_error_message('data gagal disimpan');
			else
				set_success_message('data berhasil disimpan');

			redirect('evaluasi/komponen_evaluasi/datalist/'.$id_kurikulum.'/'.$id_komponen_nilai);
		}

		$data["id_kurikulum"] = $id_kurikulum;
		$data["id_komponen_nilai"] = $id_komponen_nilai;

		$data["component"] = "evaluasi_pembelajaran";
		render('komponen_evaluasi/tambah', $data);
	}

	function edit($id_nilai_kategori = false)
	{
		if(! $id_nilai_kategori) show_404();

		$this->load->model('t_nilai_kategori');

		$data['kurikulum'] = $this->t_nilai_kategori->get($id_nilai_kategori);

		if($post = $this->input->post()){
			if($this->t_nilai_kategori->update_data($post, $id_nilai_kategori))
				set_success_message('data berhasil disimpan.');
			else
				set_error_message('data gagal disimpan.');

			redirect('evaluasi/komponen_evaluasi/datalist/'.$data['kurikulum']['id_kurikulum'].'/'.$data['kurikulum']['id_komponen_nilai']);
		}

		$data["component"] = "evaluasi_pembelajaran";
		render('komponen_evaluasi/edit', $data);
	}

	function delete($id_nilai_kategori = false)
	{
		if(! $id_nilai_kategori) show_404();

		$this->load->model('t_nilai_kategori');
		$data['kurikulum'] = $this->t_nilai_kategori->get($id_nilai_kategori);
		
		if($this->t_nilai_kategori->delete($id_nilai_kategori))
			set_success_message('data berhasil dihapus.');
		else
			set_error_message('data gagal dihapus.');

		redirect('evaluasi/komponen_evaluasi/datalist/'.$data['kurikulum']['id_kurikulum'].'/'.$data['kurikulum']['id_komponen_nilai']);
	}

}
