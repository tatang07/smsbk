<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ujian_sekolah extends MY_Controller {

		public function datalist($jenis=0){
			$this->load->model('t_nilai');
			$this->load->model('t_nilai_detail');
			$this->load->model('t_rombel_detail');
			$this->load->model('t_guru_matpel_rombel');
			$this->load->model('t_nilai');
			$this->load->model('t_nilai_kategori');
			
			
			//------------------
			$this->load->model('t_ujian_sekolah');
			
			
			//------------------
			
			// print_r ($_POST);
			
			$id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
			$id_gmr = isset($_POST['id_gmr'.$id_rombel])?$_POST['id_gmr'.$id_rombel]:0; // Dapetin Id Pelajaran dari smsbk helper
			
			// print_r($id_gmr);
			// $id_komponen_nilai = isset($_POST['id_komponen_nilai'.$id_rombel])?$_POST['id_komponen_nilai'.$id_rombel]:0;
			// $id_nilai_kategori = isset($_POST['id_nilai_kategori'.$id_komponen_nilai])?$_POST['id_nilai_kategori'.$id_komponen_nilai]:0;
			$id_term = get_id_term();		
			
			//$nkat = $this->t_nilai_kategori->get($id_nilai_kategori);
			//$gmr = $this->t_guru_matpel_rombel->get($id_gmr);
			
			$tahun_ajaran=get_id_tahun_ajaran();
			$data["siswa"] = $this->t_ujian_sekolah->get_siswa($id_gmr,$jenis,$id_rombel);
			$data["nilai"] = $this->t_ujian_sekolah->get_nilai($id_gmr,$jenis,$id_rombel,$tahun_ajaran);
			
			// print_r($data["nilai_detail"]);
			$data["siswa"] = $this->t_rombel_detail->get_siswa_rombel($id_rombel);

			$data["id_rombel"] = $id_rombel;
			$data["id_gmr"] = $id_gmr;
			// $data["id_nilai_kategori"] = $id_nilai_kategori;
			// $data["id_komponen_nilai"] = $id_komponen_nilai;
			$data["component"] = "evaluasi_pembelajaran";
			
			render('ujian_sekolah/datalist', $data);
		}
		
		public function tambah($id_rombel, $id_gmr){
			$this->load->model('t_guru_matpel_rombel');
			$this->load->model('t_ujian_sekolah');
			$this->load->model('t_rombel_detail');
			$gmr = $this->t_ujian_sekolah->cek_rombel($id_rombel);
			$pelajaran = $this->t_ujian_sekolah->cek_pelajaran($id_gmr);
			
			if($gmr){
			//$data["nama"] = $gmr->nama;
			$data["rombel"] = $gmr->rombel;
			$data["pelajaran"] = $pelajaran->pelajaran;
			}else{
				set_error_message('pilih terlebih dahulu kelas dan mata pelajaran');
				redirect('evaluasi/ujian_sekolah/datalist');
			}
			$data['id_pelajaran']=$id_gmr;
			//$row = $this->t_guru_matpel_rombel->get_penilaian($id_nilai_kategori);
			$data["siswa"] = $this->t_guru_matpel_rombel->get_siswa2($id_gmr);
			$data["siswa"] = $this->t_rombel_detail->get_siswa_rombel($id_rombel);
			render('ujian_sekolah/add', $data);
		}
		
		public function edit($id_rombel, $id_gmr, $jenis){
			$this->load->model('t_guru_matpel_rombel');
			$this->load->model('t_nilai');
			$this->load->model('t_nilai_detail');
			$this->load->model('t_ujian_sekolah');
			$this->load->model('t_rombel_detail');
			
			$gmr = $this->t_ujian_sekolah->cek_rombel($id_rombel);
			$pelajaran = $this->t_ujian_sekolah->cek_pelajaran($id_gmr);
				// print_r($gmr);
				// print_r($pelajaran);
			if($gmr){
			//$data["nama"] = $gmr->nama;
			$data["rombel"] = $gmr->rombel;
			$data["pelajaran"] = $pelajaran->pelajaran;
			}else{
			
				set_error_message('pilih terlebih dahulu kelas dan mata pelajaran');
				redirect('evaluasi/ujian_sekolah/datalist');
			}
			

			$tahun_ajaran=get_id_tahun_ajaran();
			$data["siswa"] = $this->t_ujian_sekolah->get_siswa($id_rombel,$jenis,$id_gmr);
			$data["nilai"] = $this->t_ujian_sekolah->get_nilai($id_gmr,$jenis,$id_rombel,$tahun_ajaran);
			
			$data["siswa"] = $this->t_rombel_detail->get_siswa_rombel($id_rombel);
			render('ujian_sekolah/edit', $data);
		}
		
		public function submit(){
			$this->load->model('t_nilai_detail');
			$this->load->model('t_ujian_sekolah');
			$this->load->model('t_nilai');
			$this->load->model('t_nilai_akhir');
			$nilai=$this->input->post('nilai_angka');
			
			// print_r($nilai);
			// $id_standar_kompetensi = $this->input->post('id_standar_kompetensi');
			// $kompetensi_dasar = $this->input->post('kompetensi_dasar');
			// $id_kompetensi_dasar = $this->input->post('id_kompetensi_dasar');
			
			$action = $this->input->post('action');
			// echo $this->input->post('jenis');
			$jenis=$this->input->post('jenis');
		
			if($action == 'edit'){
				
				$id=$this->input->post('id');
				
				// $kategori = $this->input->post('kategori');
				$i=0;
				foreach($nilai as $n){
					$i=$i+1;
				}
				// echo $i;
				for($j=0;$j<$i;$j++){
					$id2=$id[$j];
					
					$data['nilai']=$nilai[$j];
					$this->t_nilai_detail->save_us($data,$id2);
				}
				
				// print_r($nilai);
				// print_r($id);
				// $jenis;
				set_success_message('Data Berhasil Diupdate');
				redirect("evaluasi/ujian_sekolah/datalist/$jenis");
			}elseif($action == 'tambah'){
				
				// $nama_nilai=$this->input->post('nilai');
				// $gmr=$this->input->post('gmr');
				// $nilai_kategori=$this->input->post('nilai_kategori');
				// $term=$this->input->post('term');
				$id=$this->input->post('id');
				$id_siswa=$this->input->post('id_siswa');
				$data['id_pelajaran']=$this->input->post('id_pelajaran');
				$data['id_tahun_ajaran']=get_id_tahun_ajaran();
				$data['tanggal_penilaian']=date('Y-m-d');
				$data['jenis']=$jenis;
				// echo $jenis;
				
				
				$this->t_nilai_akhir->save($data);
				$id_na=$this->t_nilai_akhir->new_id();
				$data["id_nilai_akhir"] = $this->t_ujian_sekolah->select_nilai_akhir();		//cek id_nilai_akhir
				$id_nilai_akhir=$data["id_nilai_akhir"];
				// $hasil=count ($id_nilai_akhir);
				// print_r($id_nilai_akhir);
				// exit();
				// foreach($id_nilai_akhir as $ina){
					// $hasil=$ina['id_nilai_akhir'];
				// }
				
				$i=0;
				foreach($nilai as $n){
					$i=$i+1;
				}
			
				for($j=0;$j<$i;$j++){
					$data2['id_siswa']=$id_siswa[$j];
					$data2['id_nilai_akhir']=$id_na;
					if($nilai[$j]==NULL){
						$nilai[$j]=0;
					}
					$data2['nilai']=$nilai[$j];
					$this->t_nilai_detail->save_add($data2);
				}
				// echo $jenis;
				set_success_message('Data Berhasil Diupdate');
				redirect("evaluasi/ujian_sekolah/datalist/$jenis");
				// $kategori = $this->input->post('kategori');
				
				// $data['id_nilai_kategori']=$nilai_kategori;
				// $data['id_guru_matpel_rombel']=$gmr;
				// $data['id_term']=$term;
				// if($kategori==1){
					// $nama_nilai="";
				// }
				// $data['nama_nilai']=$nama_nilai;
				// $data['tanggal_penilaian']=date("Y-m-d");
				
				// $cek = $this->t_nilai->get_by_fields(array("id_nilai_kategori"=>$nilai_kategori,
																 // "id_term"=>$term,
																 // "id_guru_matpel_rombel"=>$gmr,
																 // "nama_nilai"=>$nama_nilai,
																// )
														// );
				// if(!$cek){
					// $this->t_nilai->save($data);
					// $cek = $this->t_nilai->get_by_fields(array("id_nilai_kategori"=>$nilai_kategori,
																 // "id_term"=>$term,
																 // "id_guru_matpel_rombel"=>$gmr,
																 // "nama_nilai"=>$nama_nilai,
																// )
														// );
					// foreach($nilai as $id_s => $n){
						// if($n==""){
							// $n=0;
						// }
						// $data1['id_siswa'] = $id_s;
						// $data1['id_nilai'] = $cek[0]['id_nilai'];
						// $data1['nilai'] = $n;
						// $this->t_nilai_detail->save($data1);
					// }
					// set_success_message('Data Berhasil ditambah');
					// redirect("evaluasi/detail_evaluasi_kelas/datalist");
				}else{									
					set_error_message('nama nilai sudah ada, silahkan ganti nama');
					// $data['nilai']=$nilai;
					
					$this->tambah($gmr,$nilai_kategori);
					// render("evaluasi/detail_evaluasi_kelas/tambah/".$gmr."/".$nilai_kategori, $data);
				}
			
		}
}
