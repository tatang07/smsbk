<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class nilai_deskriptif extends CI_Controller {

	public function index(){
		$this->load->model('evaluasi/t_nilai_konversi_deskripsi');
		$data['kurikulum'] = $this->t_nilai_konversi_deskripsi->get_all_kurikulum();
		$data['tingkat'] = $this->t_nilai_konversi_deskripsi->get_all_tingkat_kelas();
		$data['komponen_nilai'] = $this->t_nilai_konversi_deskripsi->get_komponen();
		$data['gid'] = $gid = get_usergroup();
		$data['component']="evaluasi_pembelajaran";
		$data['nilai_deskriptif'] = $this->t_nilai_konversi_deskripsi->get_datalist(0,0,0);
		render('nilai_deskriptif/nilai_deskriptif',$data);
	}
	
	public function delete(){
		$this->load->model('evaluasi/t_nilai_konversi_deskripsi');
		$id = $this->uri->rsegment(3);
		$this->t_nilai_konversi_deskripsi->del($id);
		set_success_message("data berhasil di hapus");
		redirect(base_url('evaluasi/nilai_deskriptif'));
	}
	
	public function add(){
		$this->load->model('evaluasi/t_nilai_konversi_deskripsi');
		$data['id_pelajaran'] = $_GET['id'];
		$id = $this->t_nilai_konversi_deskripsi->get_kurikulum($data['id_pelajaran']);
		$data['nilai'] = $this->t_nilai_konversi_deskripsi->get_nilai_huruf($id['id_kurikulum']);
		$data['component']="evaluasi_pembelajaran";
		render('nilai_deskriptif/add', $data);
	}
	
	public function proses_add(){
		$this->load->model('evaluasi/t_nilai_konversi_deskripsi');
		$data=$this->input->post();
		unset($data['send']);
		for($i=0; $i<sizeof($data['id']);$i++){
			$item['id_nilai_konversi'] = $data['id'][$i];
			$item['deskripsi'] = $data['deskripsi'][$i];
			$item['id_pelajaran'] = $data['id_pelajaran'];
			if(!$data['id_nilai_konversi_deskripsi'][$i]){
				$this->t_nilai_konversi_deskripsi->add($item);
			}else{
				$this->t_nilai_konversi_deskripsi->edit($item, $data['id_nilai_konversi_deskripsi'][$i]);
			}
		}
		set_success_message("data berhasil di simpan");
		redirect(base_url('evaluasi/nilai_deskriptif/index/'));
	}
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */