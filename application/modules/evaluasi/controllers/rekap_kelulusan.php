<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class rekap_kelulusan extends CI_Controller {

	public function index(){	
		$this->load->model('evaluasi/v_siswa');
		$ta = $this->v_siswa->get_tahun_ajaran();
		$data['data_lulusan'] = $this->v_siswa->get_data_lulusan($ta);
		$data['data_tidak_lulus'] = $this->v_siswa->get_data_tidak_lulus($ta);
		$i=0;
		while(isset($data['data_lulusan'][$i])){
			$data['data_lulusan'][$i]['total']=$data['data_lulusan'][$i]['jml_lulus']+$data['data_tidak_lulus'][$i]['jml_tidak_lulus'];
			if($data['data_lulusan'][$i]['jml_lulus']!=0){
				$data['data_lulusan'][$i]['total']=round((($data['data_lulusan'][$i]['jml_lulus']/$data['data_lulusan'][$i]['total'])*100), 2);
			}else{
				$data['data_lulusan'][$i]['total']=round($data['data_lulusan'][$i]['total']*0.00, 2);
			}
			$i++;
		}
		
		$data['rata'] = $this->v_siswa->get_rata_rata();
		$data['array_ta'] = $this->v_siswa->get_tahun_ajaran();
		
		$data["component"] = "evaluasi_pembelajaran";
		
		// print_r($data);
		// exit;
		//print_r($data);
		render('rekap_kelulusan/rekap_kelulusan',$data);
	}
	
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */