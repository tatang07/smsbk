<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class nilai_konversi extends CI_Controller {

	public function index(){	
		$this->load->model('evaluasi/t_nilai_konversi');
		$data['kurikulum'] = $this->t_nilai_konversi->get_all_kurikulum();
		$data['komponen_nilai'] = $this->t_nilai_konversi->get_komponen();
		
		$data['cek'] = 0;
		$data['id_komponen'] = 0;
		
		$data['component']="evaluasi_pembelajaran";
		$data['nilai_konversi'] = $this->t_nilai_konversi->get_nilai_konversi_by(0);
		$data['gid'] = $gid = get_usergroup();
		
		render('nilai_konversi/nilai_konversi',$data);
	}
	
	public function search(){
		$this->load->model('evaluasi/t_nilai_konversi');
		
		$id = $this->input->post('id_kurikulum');
		$id_komponen = $this->input->post('id_komponen_nilai');
		$data['kurikulum'] = $this->t_nilai_konversi->get_all_kurikulum();
		$data['komponen_nilai'] = $this->t_nilai_konversi->get_komponen();
		
		$data['cek'] = $id;
		$data['id_komponen'] = $id_komponen;
		$data['gid'] = $gid = get_usergroup();	
		$data['component']="evaluasi_pembelajaran";
		$data['nilai_konversi'] = $this->t_nilai_konversi->get_nilai_konversi_by($id_komponen);
		render('nilai_konversi/nilai_konversi',$data);
	}
	
	public function edit(){
		$this->load->model('evaluasi/t_nilai_konversi');
		$id = $this->uri->rsegment(3);
		
		$data['kurikulum'] = $this->t_nilai_konversi->get_all_kurikulum();
		$data['komponen_nilai'] = $this->t_nilai_konversi->get_komponen();
		
		$data['component']="evaluasi_pembelajaran";
		$data['detail'] = $this->t_nilai_konversi->get_nilai_by($id);
		render('nilai_konversi/edit', $data);
	}
	
	public function proses_edit(){
		$this->load->model('evaluasi/t_nilai_konversi');
		$data=$this->input->post();
		$id = $data['id_nilai_konversi'];
		unset($data['id_nilai_konversi']);
		unset($data['send']);
		$this->t_nilai_konversi->edit($data, $id);
		set_success_message("data berhasil di perbarui");
		redirect(base_url('evaluasi/nilai_konversi'));
	}
	
	public function delete(){
		$this->load->model('evaluasi/t_nilai_konversi');
		$id = $this->uri->rsegment(3);
		$this->t_nilai_konversi->delete($id);
		set_success_message("data berhasil di hapus");
		redirect(base_url('evaluasi/nilai_konversi'));
	}
	
	public function add(){
		$this->load->model('evaluasi/t_nilai_konversi');
		$data['kurikulum'] = $this->t_nilai_konversi->get_all_kurikulum();
		$data['komponen_nilai'] = $this->t_nilai_konversi->get_komponen();
		
		$data['component']="evaluasi_pembelajaran";
		render('nilai_konversi/add', $data);
	}
	
	public function proses_add(){
		$this->load->model('evaluasi/t_nilai_konversi');
		$data=$this->input->post();
		unset($data['send']);
		$data['id_sekolah'] = get_id_sekolah();
		$data['id_tahun_ajaran'] = get_id_tahun_ajaran();
		$this->t_nilai_konversi->add($data);
		set_success_message("data berhasil di simpan");
		redirect(base_url('evaluasi/nilai_konversi'));
	}
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */