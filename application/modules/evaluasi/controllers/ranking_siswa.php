<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class ranking_siswa extends MY_Controller {

		public function datalist(){
			$this->load->model('t_nilai_detail');
			$this->load->model('t_rombel_detail');
			$this->load->model('t_guru_matpel_rombel');
			$this->load->model('t_nilai');
			$this->load->model('t_nilai_kategori');
			
			$id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
			$id_gmr = isset($_POST['id_gmr'.$id_rombel])?$_POST['id_gmr'.$id_rombel]:0;
						
			$data["tahun_ajaran"] = $this->t_nilai->get_all_tahun_ajaran();
			$data["term"] = $this->t_nilai->get_all_term();
			$data["tingkat"] = $this->t_nilai->get_all_tingkat();
			$data["gmr"] = $this->t_nilai->get_all_gmr_groupby_pelajaran();
			
			$data["siswa"] = $this->t_rombel_detail->get_siswa_rombel_by($id_rombel);
			
			$id_tahun_ajaran=0;
			$id_term=0;
			$id_tingkat=0;
			
			$data["nilai"] = $this->t_nilai->get_nilai($id_tahun_ajaran, $id_term, $id_tingkat, $id_gmr);
			
			$data["id_tahun_ajaran"] = $id_tahun_ajaran;
			$data["id_term"] = $id_term;
			$data["id_tingkat"] = $id_tingkat;
			$data["id_rombel"] = $id_rombel;
			$data["id_gmr"] = $id_gmr;
			$data["component"] = "evaluasi_pembelajaran";
			$data['gid'] = $gid = get_usergroup();
			$data['hasil'] =null;
			
			render('ranking_siswa/datalist', $data);
		}
		
		public function search(){
			$this->load->model('t_nilai_detail');
			$this->load->model('t_rombel_detail');
			$this->load->model('t_guru_matpel_rombel');
			$this->load->model('t_nilai');
			$this->load->model('t_nilai_kategori');
			
			$id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
			if($id_rombel==0){
				$id_gmr = $_POST['id_gmr'];
			}else{
				$id_gmr = isset($_POST['id_gmr'.$id_rombel])?$_POST['id_gmr'.$id_rombel]:0;
			}
			$data["tahun_ajaran"] = $this->t_nilai->get_all_tahun_ajaran();
			$data["term"] = $this->t_nilai->get_all_term();
			$data["tingkat"] = $this->t_nilai->get_all_tingkat();
			$data["gmr"] = $this->t_nilai->get_all_gmr_groupby_pelajaran();
			
			$data["siswa"] = $this->t_rombel_detail->get_siswa_rombel_by($id_rombel);
			
			
			$id_tahun_ajaran=$this->input->post('id_tahun_ajaran');;
			$id_term=$this->input->post('id_term');;
			$id_tingkat=$this->input->post('id_tingkat');;
			
			$data["nilai"] = $this->t_nilai->get_nilai($id_tahun_ajaran, $id_term, $id_tingkat, $id_gmr);
			
			$data["id_tahun_ajaran"] = $id_tahun_ajaran;
			$data["id_term"] = $id_term;
			$data["id_tingkat"] = $id_tingkat;
			$data["id_rombel"] = $id_rombel;
			$data["id_gmr"] = $id_gmr;
			
			$data["component"] = "evaluasi_pembelajaran";
			$data['gid'] = $gid = get_usergroup();
			
			// echo '<pre>';
			// print_r($data['nilai']);
			// echo '</pre>';
			
			$temp = array();
			$i=0;
			foreach($data["siswa"] as $s){
				$jum=0;
				if($data["nilai"]){
					foreach($data["nilai"] as $n){
						if($s['id_siswa']==$n['id_siswa']){
							$jum=$jum+$n['nilai_akhir'];
						}
					}
				}
				$temp[$i]['nis'] = $s['nis'];
				$temp[$i]['nama'] = $s['nama'];
				$temp[$i]['rombel'] = $s['rombel'];
				$temp[$i]['nilai'] = $jum;
				$i++;
			}
			
			//sorting menggunakan bubble sort
			$i=$i-1;
			$j=0; $status=0; $hold=array(); $k=0;
			while($j < $i && $status==0){
				$status=1;
				for($k=0; $k<$i-$j; $k++){
					if($temp[$k]['nilai'] < $temp[$k+1]['nilai']){
						$status=0;
						$hold[0]['nis']=$temp[$k]['nis'];
						$hold[0]['nama']=$temp[$k]['nama'];
						$hold[0]['rombel']=$temp[$k]['rombel'];
						$hold[0]['nilai']=$temp[$k]['nilai'];
						
						$temp[$k]['nis']=$temp[$k+1]['nis'];
						$temp[$k+1]['nis']=$hold[0]['nis'];
						$temp[$k]['nama']=$temp[$k+1]['nama'];
						$temp[$k+1]['nama']=$hold[0]['nama'];
						$temp[$k]['rombel']=$temp[$k+1]['rombel'];
						$temp[$k+1]['rombel']=$hold[0]['rombel'];
						$temp[$k]['nilai']=$temp[$k+1]['nilai'];
						$temp[$k+1]['nilai']=$hold[0]['nilai'];
					}		
				}
				$j++;
			}
			
			//penentuan ranking
			
			$j=0; $rank=0; $last=0;
			for($j=0; $j<$i+1; $j++){
				if($last != $temp[$j]['nilai']){
					$rank++;
				}
				$temp[$j]['ranking'] = $rank;
				$last=$temp[$j]['nilai'];
			}
			
			// echo '<pre>';
			// print_r($temp);
			// echo '</pre>';
			$data['hasil'] =$temp;
			render('ranking_siswa/datalist', $data);
		}
}
