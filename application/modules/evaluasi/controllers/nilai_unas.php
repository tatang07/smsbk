<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class nilai_unas extends CI_Controller {

	public function index(){	
		$this->load->model('evaluasi/v_siswa');
		$data['tahun_ajaran'] = $this->v_siswa->get_tahun_ajaran();
		
		$data["component"] = "evaluasi_pembelajaran";
		
		render('nilai_unas/nilai_unas',$data);
	}

	public function detail(){	
		$this->load->model('evaluasi/v_siswa');
		$id = $this->uri->rsegment(3);
		$data['detail'] = $this->v_siswa->get_detail_nilai($id);
		$data['identitas'] = $this->v_siswa->get_detail_siswa($id);
		//print_r($data);
		$data["component"] = "evaluasi_pembelajaran";
		
		render('nilai_unas/detail',$data);
	}
	
	public function print_pdf($id_tahun_ajaran=0){
		$this->load->model('evaluasi/v_siswa');
		$data['tahun_ajaran'] = $this->v_siswa->get_tahun_ajaran();
		$data['statuspil'] = $id_tahun_ajaran;
		
		print_pdf($this->load->view('nilai_unas/print_pdf', $data, true), "A4");
	}
	
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */