<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class hasil_un extends MY_Controller {

		public function datalist(){
			$this->load->model('t_rombel_detail');			
			$this->load->model('t_ujian_nasional');			
			$this->load->model('v_siswa');			
			
			
			$id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
			$tahun_ajaran = isset($_POST['id_tahun_ajaran'])?$_POST['id_tahun_ajaran']:0;
			
			$data["id_rombel"] = $id_rombel;
			$data["id_tahun_ajaran"] = $tahun_ajaran;
			$data["siswa"] = $this->t_rombel_detail->get_siswa_rombel($id_rombel);
			$data['tahun_ajaran'] = $this->v_siswa->get_tahun_ajaran();
			$data['mapel_nilai'] = $this->t_ujian_nasional->get_by_fields(array("jenis"=>2));
			$data['count_mapel'] = count($this->t_ujian_nasional->get_by_fields(array("jenis"=>2)));
			
			$data["component"] = "evaluasi_pembelajaran";
			
			// print_r($data['mapel_nilai']);
			render('hasil_un/datalist', $data);
		}

}
