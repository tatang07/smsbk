<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class evaluasi_kelas extends MY_Controller {

		public function datalist(){
			$this->load->model('t_nilai_kategori');
			$this->load->model('t_rombel_detail');
			$this->load->model('t_nilai_kategori');
			
			$id_rombel = isset($_POST['id_rombel'])?$_POST['id_rombel']:0;
			$id_gmr = isset($_POST['id_gmr'.$id_rombel])?$_POST['id_gmr'.$id_rombel]:0;
			$id_komponen_nilai = isset($_POST['id_komponen_nilai'.$id_rombel])?$_POST['id_komponen_nilai'.$id_rombel]:0;			
			$id_term = get_id_term();		

						
			$data["nilai_detail"] = $this->t_nilai_kategori->get_nilai_kategori($id_gmr,$id_komponen_nilai, $id_term);
			$data["siswa"] = $this->t_rombel_detail->get_siswa_rombel($id_rombel);
			$data["nilai"] = $this->t_nilai_kategori->get_by_fields(array("id_komponen_nilai"=>$id_komponen_nilai,
																 "id_tahun_ajaran"=>get_id_tahun_ajaran(),
																 "id_sekolah"=>get_id_sekolah()
																)
														);
			$data["id_rombel"] = $id_rombel;
			$data["id_gmr"] = $id_gmr;
			$data["id_komponen_nilai"] = $id_komponen_nilai;
			$data['gid'] = $gid = get_usergroup();
			$data["component"] = "evaluasi_pembelajaran";
			
			
			render('evaluasi_kelas/datalist', $data);
		}

}
