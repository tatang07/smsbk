<?php 
	$statuspil = $id_rombel;
	if(isset($_POST['id_rombel'])){
	$a = $_POST['id_rombel'];
	$statuspil = $a; } ?>

<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
	<div class="header">
			<h2>Buku Rapor</h2>
		</div>
		<div class="tabletools">
			<div class="dataTables_filter">
				<form action="" name= "myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=70px align='left'><span class="text">Kelas</span></th>
							<td width=200px align='left'>
								<select name="id_rombel" data-placeholder="-- Pilih Kelas --" id="id_kelas" onchange="submitform();">
									<?php if($rombel){ ?>
										<option value=0>-- Pilih Kelas --</option>
									<?php foreach($rombel as $k): ?>
										<?php if($k['id_rombel'] == $statuspil ){ ?>
											<option selected value='<?php echo $k['id_rombel']; ?>'><?php echo $k['rombel']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $k['id_rombel']; ?>'><?php echo $k['rombel']; ?></option>
										<?php } ?>
									<?php endforeach; } ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
				</form>
				<form action="<?php echo base_url('evaluasi/buku_rapor/search'); ?>" method="post" enctype="multipart/form-data">
							<th width=100px align='left'><span class="text">Siswa</span></th>
							<td width=200px align='left'>
								<input type="hidden" name="id_rombel" value="<?php echo $statuspil ?>">
								<select name="id_siswa" data-placeholder="-- Pilih Siswa --">
									<?php if($siswa){ ?>
										<option value=0>-- Pilih Siswa --</option>
									<?php foreach($siswa as $k): ?>
									<?php if($k['id_rombel'] == $statuspil){ ?>
										<?php if($k['id_siswa'] == $id_siswa ){ ?>
											<option selected value='<?php echo $k['id_siswa']; ?>'><?php echo $k['nama']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $k['id_siswa']; ?>'><?php echo $k['nama']; ?></option>
										<?php } } ?>
									<?php endforeach; } ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
							<input type="submit" name=send class="button grey tooltip" value="cari" />
							<!-- <a href="search" class="button grey tooltip"><i class='icon-search'></i></a> -->
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
		</div>
		
		<?php if($stat == 'search'){ ?>
		
	<div class="box with-table">
    	<div class="content" style="padding:20px;">
	    	<div style="float:left">
		    	<table>
		    		<tr>
		    			<td>Nama Sekolah</td>
		    			<td>:</td>
		    			<td> <?php echo $sekolah['sekolah']; ?></td>
		    		</tr>
		    		<tr>
		    			<td>Alamat Sekolah</td>
		    			<td>:</td>
		    			<td> <?php echo $sekolah['alamat']; ?></td>
		    		</tr>
		    		<tr>
		    			<td>&nbsp;</td>
		    		</tr>
		    		<tr>
		    			<td>Nama</td>
		    			<td>:</td>
		    			<td> <?php echo $siswa_rapor['nama']; ?></td>
		    		</tr>
		    		<tr>
		    			<td>Nomor Induk/NISN</td>
		    			<td>:</td>
		    			<td> <?php echo $siswa_rapor['nis'].'/'.$siswa_rapor['nisn']; ?></td>
		    		</tr>
		    	</table>
	    	</div>
	    	<div style="float:right;">
	    		<table>
		    		<tr>
		    			<td>Kelas</td>
		    			<td>:</td>
		    			<td> <?php echo $siswa_rapor['rombel']; ?></td> 
		    		</tr>
		    		<tr>
		    			<td>Semester</td>
		    			<td>:</td>
		    			<td> <?php echo get_nama_term(); ?></td>
		    		</tr>
	    			<tr>
		    			<td>Tahun Pelajaran</td>
		    			<td>:</td>
		    			<td> <?php echo get_nama_tahun_ajaran(); ?></td>
	    			</tr>
	    		</table>
	    	</div>
    	</div>
       <div class="header">
            <h2>CAPAIAN KOMPETENSI</h2>
       </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper" role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan='2' colspan='2'>MATA PELAJARAN</th>
								<th colspan='2'>Pengetahuan</th>
								<th colspan='2'>Keterampilan</th>
								<th colspan='2'>Sikap Spiritual</th>
							</tr>
							<tr>
								<th>Angka</th>
								<th>Perdikat</th>
								<th>Angka</th>
								<th>Predikat</th>
								<th>Dalam Mapel</th>
								<th>Antar Mapel</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($kelompok_pelajaran as $k): ?>
							<?php if($k['id_kurikulum'] == $siswa_rapor['id_kurikulum']){ ?>
								<tr>
									<td colspan='8' width='' class='left'><b><?php echo $k['kelompok_pelajaran'] ?></b></td>
								</tr>
								<?php $no=1; if($gmr){ foreach($gmr as $g): ?>
								<?php if($g['rombel'] == $siswa_rapor['rombel'] && $g['id_kelompok_pelajaran'] == $k['id_kelompok_pelajaran']){ ?>
								<tr>
									<td width='' class='center'><?php echo $no; ?></td>
									<td width='' class='left'><?php echo $g['pelajaran'].'<br>Nama Guru : -,'.$g['nama'].',-'; ?></td>
									<?php 
									// --------- hitungan untuk bagian pengetahuan --------- //
									$bobot = 0; $jumlah = 0;
									
									if($nilai){
									foreach($nilai as $n):
										if($n['id_komponen_nilai'] == 1 && $n['id_siswa'] == $siswa_rapor['id_siswa'] && $n['id_guru_matpel_rombel'] == $g['id_guru_matpel_rombel']){
											
											$angka = ($n['jumlah'] / $n['banyak']) * $n['bobot']/100;
											$jumlah = $jumlah + $angka;
										}
									endforeach;
									}
									
									$hasil = $jumlah;
									
									$predikat = '';
									//print_r($konversi);
									if(!empty($konversi)):
									foreach($konversi as $ko){
										if($hasil > $ko['nilai_angka_min'] && $hasil <= $ko['nilai_angka_max'] && $ko['id_komponen_nilai'] == 1){
											$predikat = $ko['nilai_huruf'];
										}
									}
									endif;
									?>
									<td width='' class='center'><?php echo $hasil; ?></td>
									<td width='' class='center'><?php echo $predikat; ?></td>
									
									<?php 
									// --------- hitungan untuk bagian keterampilan --------- //
									$bobot = 0; $jumlah = 0;
									
									if(!empty($nilai)):
									foreach($nilai as $n):
										if($n['id_komponen_nilai'] == 2 && $n['id_siswa'] == $siswa_rapor['id_siswa'] && $n['id_guru_matpel_rombel'] == $g['id_guru_matpel_rombel']){
											$angka = ($n['jumlah'] / $n['banyak']) * $n['bobot']/100;
											$jumlah = $jumlah + $angka;
										}
									endforeach;
									endif;
									
									$hasil = $jumlah;
									
									
									$predikat = '';
									if($konversi){
									foreach($konversi as $ko){
										if($hasil > $ko['nilai_angka_min'] && $hasil <= $ko['nilai_angka_max'] && $ko['id_komponen_nilai'] == 2){
											$predikat = $ko['nilai_huruf'];
										}
									}
									}
									?>
									<td width='' class='center'><?php echo $hasil; ?></td>
									<td width='' class='center'><?php echo $predikat; ?></td>
									
									<?php 
									// --------- hitungan untuk bagian sikap --------- //
									$bobot = 0; $jumlah = 0;
									
									if($nilai){
									foreach($nilai as $n):
										if($n['id_komponen_nilai'] == 3 && $n['id_siswa'] == $siswa_rapor['id_siswa'] && $n['id_guru_matpel_rombel'] == $g['id_guru_matpel_rombel']){
											$angka = ($n['jumlah'] / $n['banyak']) * $n['bobot']/100;
											$jumlah = $jumlah + $angka;
										}
									endforeach;
									}
									
									$hasil = $jumlah;
									
									$predikat = '';
									if($konversi){
									foreach($konversi as $ko){
										if($hasil > $ko['nilai_angka_min'] && $hasil <= $ko['nilai_angka_max'] && $ko['id_komponen_nilai'] == 3){
											$predikat = $ko['nilai_huruf'];
										}
									}
									}
									?>
									<td width='' class='center'><?php echo $predikat; ?></td>
									<td width='' class='center'><?php echo ''; ?></td>
								<?php $no++; } endforeach; } ?>
								</tr>
							<?php } endforeach; ?>
							
						</tbody>
					</table>
					
				</div>
            </div>
        </div>        
    </div>
	<?php }else{ ?>
		<div class="box with-table">
			<div class="tabletools">
				<div class="dataTables_filter">
					Pilih Kelas dan Siswa yang Ingin Dicari
				</div>
			</div>
		</div>
	<?php } ?>
 </div>

 <script>
	
	function submitform()
	{
	  document.myform.submit();
	}
</script>