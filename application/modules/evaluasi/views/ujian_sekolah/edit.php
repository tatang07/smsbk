	<h1 class="grid_12">Evaluasi Pembelajaran</h1>
			
			<form action="<?php echo base_url('evaluasi/ujian_sekolah/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<?php if($this->uri->segment(6)==1){?>
					<legend>Edit Nilai Ujian Sekolah</legend>
					<?php }else{?>
					<legend>Edit Nilai Ujian Nasional</legend>
					<?php } ?>
					<div class="row">
						<table>
						<input type="hidden" name="action" value="edit">
						<input type="hidden" name="kategori" value="<?php //echo $tunggal?>">
							<tr>
								<td width=150px >Kelas</td>
								<td width=50px >:</td>
								<td width=300px ><?php echo $rombel; ?></td>
							</tr>
							<tr>
								<td width=150px >Mata Pelajaran</td>
								<td width=50px >:</td>
								<td width=300px ><?php echo $pelajaran; ?></td>
							</tr>
	
						</table>
					</div>

				</fieldset><!-- End of fieldset -->
				
				<fieldset>
					<legend>Siswa</legend>
				</fieldset><!-- End of fieldset -->
			
					</br>
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">
							<table class="styled" >
								<thead>
									<tr>
										<th>No.</th>
										<th>NIS</th>
										<th>Nama</th>
										<th>Nilai</th>
									
									</tr>
								</thead>
								<tbody>
									<?php $var=0;?>
									<?php $no=1; if($siswa){ foreach($siswa as $s): ?>
										<tr>
											<td width=50px align="center"><?php echo $no;?></td>
											<td width=100px align="center"><?php echo $s['nis'];?></td>
											<td width=400px><?php echo $s['nama'];?></td>
											<?php foreach($nilai as $n):?>
											<?php if($s['id_siswa']==$n['id_siswa']){?>
											<td>
											<input type="text" name="nilai_angka[<?php echo $var ?>]" value="<?php echo $n['nilai'];?>" />
											<input type="hidden" name="id[<?php echo $var;?>]" value="<?php echo $n['id_nilai_akhir_detail'];?>" />
											<input type="hidden" name="jenis" value="<?php echo $this->uri->segment(6); ?>" >
											</td>
											<?php $var++; }?>
											<?php endforeach; ?>
											
																				
										</tr>
									<?php $no++; endforeach; } ?>
								</tbody>
							</table>
							</div>
						</div>
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('evaluasi/ujian_sekolah/datalist/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
