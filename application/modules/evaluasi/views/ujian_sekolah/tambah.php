	<h1 class="grid_12">Evaluasi Pembelajaran</h1>
			
			<form action="<?php echo base_url('evaluasi/detail_evaluasi_kelas/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Detail Evaluasi Kelas</legend>
					<div class="row">
						<table>
						<input type="hidden" name="action" value="tambah">
							<tr>
								<td width=150px height=30px>Kelas</td>
								<td width=50px height=30px>:</td>
								<td width=300px height=30px><?php echo $rombel; ?></td>
								<td width=150px height=30px>Komponen</td>
								<td width=50px height=30px>:</td>
								<td width=300px height=30px><?php echo $komponen; ?></td>
							</tr>
							<tr>
								<td width=150px height=30px>Mata Pelajaran</td>
								<td width=50px height=30px>:</td>
								<td width=300px height=30px><?php echo $pelajaran; ?></td>
								<td width=150px height=30px>Penilaian</td>
								<td width=50px height=30px>:</td>
								<td width=300px height=30px><?php echo $penilaian; ?></td>
							</tr>
							<tr>
								<td width=150px height=30px>Guru</td>
								<td width=50px height=30px>:</td>
								<td width=300px height=30px><?php echo $nama; ?></td>
								<td width=150px height=30px>Nilai</td>
								<td width=50px height=30px>:</td>
								<td width=300px height=30px><input type="text" name="nilai" /></td>
							</tr>
						</table>
					</div>

				</fieldset><!-- End of fieldset -->
				
				<fieldset>
					<legend>Siswa</legend>
				</fieldset><!-- End of fieldset -->
					
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">
							<table class="styled" >
								<thead>
									<tr>
										<th>No.</th>
										<th>NIS</th>
										<th>Nama</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<?php $no=1; if($siswa){ foreach($siswa as $s): ?>
										<tr>
											<td width=50px class="center"><?php echo $no;?></td>
											<td width=50px class="center"><?php echo $s->nis;?></td>
											<td width=400px><?php echo $s->nama;?></td>
											<td width=50px class="center"><input type="text" size="4" style="text-align: right;" name="nilai_angka" /></td>
										</tr>
									<?php $no++; endforeach; } ?>
								</tbody>
							</table>
							</div>
						</div>
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('evaluasi/detail_evaluasi_kelas/datalist/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
