
<?php $rombel = array("0"=>"-Pilih-")+get_rombel_akhir() ?>
<?php $pelajaran = get_id_pelajaran() ?>
<?php //$komponen = get_rombel_komponen_nilai() ?>
<?php //$kategori = get_nilai_kategori() ?>
<?php //$allkomponen =  array("0"=>"-Pilih-")+get_list("m_komponen_nilai","id_komponen_nilai","komponen_nilai"); ?>

<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
		<?php //print_r($nilai);?>
	    <div class="box with-table">
       <div class="header">
			<?php if($this->uri->segment(4)==1){?>
				   <h2>Ujian Sekolah</h2>
			<?php }else{ ?>
					<h2>Ujian Nasional</h2>
			<?php } ?>
        
       </div>
       <div class="tabletools">
			<div class="right">
					<?php $a=0; foreach($nilai as $n){?>
						<?php $a=count($n['nilai']);?>
					<?php } ?>
				
				
				<?php if($a==0){?>		
								<a href="<?php  echo base_url('evaluasi/ujian_sekolah/tambah/'.$id_rombel.'/'.$id_gmr.'/'.$this->uri->segment(4)); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php }else{?>
								<a href="<?php echo base_url('evaluasi/ujian_sekolah/edit/'.$id_rombel.'/'.$id_gmr.'/'.$this->uri->segment(4)); ?>"><i class="icon-pencil"></i>Edit</a> 
				<?php }?>

			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" >
					<table>
						<tr>
							<th width=100px align='left'><span class="text">Kelas</span></th>
							<td width=150px align='left'>
								<?php echo simple_form_dropdown_clear("id_rombel",$rombel,$id_rombel," id='idrombel' onChange=rombelChanged(this.value)",null) ?>
							</td>
						</tr>
						<tr>
							<th width=80px align='left'><span class="text">Mata Pelajaran</span></th>
							<td width=100px align='left'>
								<?php foreach($rombel as $id=>$det):?>
									<?php $mapel =  array("0"=>"-Pilih-") ?>
									<?php $mapel += isset($pelajaran[$id])?$pelajaran[$id]:array() ?>
									<div id='mapel<?php echo $id?>' <?php echo $id_rombel!=$id?"style='display:none'":""?> class='listmapel'>					
										<?php echo simple_form_dropdown_clear("id_gmr".$id,$mapel,$id_gmr,"",null) ?>
									</div>	
								<?php endforeach ?>
							</td>
						</tr>
						<tr>
							<td width=1px>&nbsp;</td>
							<td align='left'>
								<a href="javascript:document.getElementById('formPencarian').submit();" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
	</div>
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=2>No.</th>
								<th rowspan=2>NIS</th>
								<th rowspan=2>Nama</th>
							
								<?php if($a>0){?>
								<th rowspan=2>Nilai</th>
								<?php } ?>
							</tr>
						</thead>
						<tbody>
							<?php //print_r($nilai) ?>
						  <?php $no = 1 ?>
						  <?php foreach($siswa as $sis):?>
							
							<tr>
								<td width='10px' class='center'><?php echo $no++ ?></td>
								<td width='70px' class='center'><?php echo $sis['nis']?></td>
								<td width=''><?php echo $sis['nama']?></td>
								<?php foreach($nilai as $ni):?>
									<?php if($ni['id_siswa']==$sis['id_siswa'] && $a>0){?>
								<td width='50px' class='center'> <?php echo $ni['nilai']?></td>
									<?php } ?>
								<?php endforeach;?>
							</tr>							
						  <?php endforeach?>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info"></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>

	<script type="text/javascript">		
		function rombelChanged(id){
			$('.listmapel').hide();
			// $('.listkomp').hide();
			if(id>0){
				$('#mapel'+id).show();				
				// $('#komp'+id).show();				
			}
		}
	</script>