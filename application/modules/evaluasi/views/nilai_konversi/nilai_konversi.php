<?php 
	$statuspil = $cek;
	if(isset($_POST['id_kurikulum'])){
	$a = $_POST['id_kurikulum'];
	$statuspil = $a; } ?>
	
	
<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
   	<div class="box">
			
		<div class="header">
			<h2>Nilai Konversi</h2>
		</div>
		<div class="tabletools">
			<div class="right">
			<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
				<a href="<?php echo base_url('evaluasi/nilai_konversi/add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php } ?>
			  <br/><br/>
			</div>
			<div class="dataTables_filter">
				<form action="" name= "myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=70px align='left'><span class="text">Kurikulum &nbsp;</span></th>
							<td width=200px align='left'>
								<select name="id_kurikulum" data-placeholder="-- Pilih Kurikulum --" id="id_kurikulum" onchange="submitform();">
									<?php if($kurikulum){ ?>
										<option value=0>-- Pilih Kurikulum --</option>
									<?php foreach($kurikulum as $k): ?>
										<?php if($k['id_kurikulum'] == $statuspil ){ ?>
											<option selected value='<?php echo $k['id_kurikulum']; ?>'><?php echo $k['nama_kurikulum']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $k['id_kurikulum']; ?>'><?php echo $k['nama_kurikulum']; ?></option>
										<?php } ?>
									<?php endforeach; } ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
				</form>
				<form action="<?php echo base_url('evaluasi/nilai_konversi/search'); ?>" method="post" enctype="multipart/form-data">
							<th width=100px align='left'><span class="text">Komponen Nilai &nbsp;</span></th>
							<td width=200px align='left'>
								<input type="hidden" name="id_kurikulum" value="<?php echo $statuspil; ?>">
								<select name="id_komponen_nilai" data-placeholder="-- Pilih Komponen Nilai --">
									<?php if($komponen_nilai){ ?>
										<option value=0>-- Pilih Komponen Nilai --</option>
									<?php foreach($komponen_nilai as $k): ?>
									<?php if($k['id_kurikulum'] == $statuspil){ ?>
										<?php if($k['id_komponen_nilai'] == $id_komponen ){ ?>
											<option selected value='<?php echo $k['id_komponen_nilai']; ?>'><?php echo $k['komponen_nilai']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $k['id_komponen_nilai']; ?>'><?php echo $k['komponen_nilai']; ?></option>
										<?php } } ?>
									<?php endforeach; } ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
							<input type="submit" name=send class="button grey tooltip" value="cari" />
							<!-- <a href="search" class="button grey tooltip"><i class='icon-search'></i></a> -->
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
			<div class="dataTables_wrapper"  role="grid">
				<table class="styled" >
					<thead>
						<tr colspan='2'>
						</tr>
						<tr>
							<td width='20px' class='center'>No.</td>
							<td width='' class='center'>Nilai Huruf</td>
							<td width='' class='center'>Range Nilai Angka</td>
							<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
							<td width='150px' class='center'>Aksi</td>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php 
							if($nilai_konversi){
							$no=1;
							foreach($nilai_konversi as $temp){
						?>
						<tr>
							<td class='center'>
								<?php
									echo $no;
									$no++;
								?>
							</td>
							<td class='center'><?php echo $temp['nilai_huruf'];?></td>
							<td class='center'><?php echo number_format($temp['nilai_angka_min'],2)." < x <= ".number_format($temp['nilai_angka_max'],2);?></td>
							<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
								<td class='center'>
									<a href="<?php echo site_url('evaluasi/nilai_konversi/edit/'.$temp['id_nilai_konversi']);?>"class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a href="<?php echo site_url('evaluasi/nilai_konversi/delete/'.$temp['id_nilai_konversi']);?>"class="button small grey tooltip" data-gravity="s" onClick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Delete</a>
								</td>
							<?php } ?>
						</tr>
						<?php
							}
							}else{
								echo "<tr><td colspan='3'>Belum ada data!</td></tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div><!-- End of .content -->
		
	</div><!-- End of .box -->		
 </div>

 <script>
	
	function submitform()
	{
	  document.myform.submit();
	}
</script>