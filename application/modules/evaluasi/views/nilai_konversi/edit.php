<?php 
	$statuspil = $detail['id_kurikulum'];
	if(isset($_POST['id_kurikulum'])){
	$a = $_POST['id_kurikulum'];
	$statuspil = $a; } ?>
	
<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Edit Nilai Detail</h2>
       </div>
	</div>
</div>

<div class="grid_12">
    <div class="box with-table">
		<form action="" name= "myform" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>
					<div class="row">
						<label for="f2_select2">
							<strong>Kurikulum</strong>
						</label>
						<div>
							<select name="id_kurikulum" data-placeholder="Pilih Kurikulum" id="id_kurikulum" onchange="submitform();">
							<?php foreach($kurikulum as $t): ?>
							<?php if($t['id_kurikulum']==$statuspil){ ?>
									<option selected value="<?php echo $t['id_kurikulum']; ?>"><?php echo $t['nama_kurikulum']; ?></option>
							<?php }else{ ?>
									<option value="<?php echo $t['id_kurikulum']; ?>"><?php echo $t['nama_kurikulum']; ?></option>
							<?php } endforeach; ?>
							</select>
						</div>
					</div>
		</form>
	</div>
</div>

<div class="grid_12">
    <div class="box with-table">
		<form action="<?php echo base_url('evaluasi/nilai_konversi/proses_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>
					<div class="row">
						<label for="f2_select2">
							<strong>Komponen Nilai</strong>
						</label>
						<div>
							<select name="id_komponen_nilai" data-placeholder="Pilih Komponen">
							<?php foreach($komponen_nilai as $t): ?>
								<?php if($t['id_kurikulum'] == $statuspil){ ?>
								<?php if($t['id_komponen_nilai'] == $detail['id_komponen_nilai']){ ?>
									<option selected value="<?php echo $t['id_komponen_nilai']; ?>"><?php echo $t['komponen_nilai']; ?></option>
								<?php }else{ ?>
									<option value="<?php echo $t['id_komponen_nilai']; ?>"><?php echo $t['komponen_nilai']; ?></option>
								<?php } } ?>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
					<div class="row">
						<label for="f2_normal_input">
							<strong>Nilai Huruf</strong>
						</label>
						<div>
							<input type="text" name="nilai_huruf" value="<?php echo $detail['nilai_huruf'];?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nilai Angka Min</strong>
						</label>
						<div>
							<input type="text" name="nilai_angka_min" value="<?php echo $detail['nilai_angka_min'];?>"/>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nilai Angka Max</strong>
						</label>
						<div>
							<input type="text" name="nilai_angka_max" value="<?php echo $detail['nilai_angka_max'];?>"/>
							<input type="hidden" value="<?php echo $detail['id_nilai_konversi'];?>" name="id_nilai_konversi"/>
						</div>
					</div>
			</fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('evaluasi/nilai_konversi'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>

  <script>
	
	function submitform()
	{
	  document.myform.submit();
	}
</script>