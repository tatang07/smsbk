<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Edit Nilai Deskripsi</h2>
       </div>
		
		<form action="<?php echo base_url('evaluasi/nilai_deskriptif/proses_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>
					<div>
						<input type="hidden" name="id_pelajaran" value="<?php echo $id_pelajaran; ?>" />
					</div>
					<?php if($nilai){ $no=0; ?>
					<?php foreach ($nilai as $temp){ ?>
						<div class="row">
							<label for="f2_normal_input">
								<strong><?php echo $temp['nilai_huruf']; ?></strong>
							</label>
							<div>
								<input type="hidden" name="id[<?php echo $no; ?>]" value="<?php echo $temp['id_nilai_konversi_deskripsi']; ?>" />
								<input type="text" name="deskripsi[<?php echo $no; ?>]" value="<?php echo $temp['deskripsi'];?>" />
							</div>
						</div>
					<?php  $no++; } ?>
					<?php }else{ echo "Data nilai konversi tidak tersedia!"; } ?>
			<fieldset>
			<div class="actions">
				<div class="left">
					<?php if($nilai){ ?>
						<input type="submit" value="Simpan" name=send />
					<?php } ?>
					<a href="<?php echo base_url('evaluasi/nilai_deskriptif'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>
