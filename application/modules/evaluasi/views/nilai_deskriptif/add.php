<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Tambah Nilai Konversi Deskripsi</h2>
       </div>
		<?php
			$this->load->model('evauasi/t_nilai_konversi_deskripsi');
		?>
		<form action="<?php echo base_url('evaluasi/nilai_deskriptif/proses_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>
					<div>
						<input type="hidden" name="id_pelajaran" value="<?php echo $id_pelajaran; ?>" />
					</div>
					<?php if($nilai){ $no=0; ?>
					<?php foreach ($nilai as $temp){ ?>
						<div class="row">
							<label for="f2_normal_input">
								<strong><?php echo $temp['nilai_huruf']; ?></strong>
							</label>
							<div>
								<input type="hidden" name="id[<?php echo $no; ?>]" value="<?php echo $temp['id_nilai_konversi']; ?>" />
								<?php
									$deskripsi = $this->t_nilai_konversi_deskripsi->get_deskripsi($id_pelajaran, $temp['id_nilai_konversi']);
									if($deskripsi['deskripsi']){
								?>
										<input type="text" name="deskripsi[<?php echo $no; ?>]" value="<?php echo $deskripsi['deskripsi']; ?>" />
										<input type="hidden" name="id_nilai_konversi_deskripsi[<?php echo $no; ?>]" value="<?php echo $deskripsi['id_nilai_konversi_deskripsi']; ?>" />
								<?php
									}else{
								?>
									<input type="text" name="deskripsi[<?php echo $no; ?>]" value="" />
								<?php
									}
								?>
							</div>
						</div>
					<?php  $no++; } ?>
					<?php }else{ echo "Data nilai konversi tidak tersedia!"; } ?>
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('evaluasi/nilai_deskriptif'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>
 