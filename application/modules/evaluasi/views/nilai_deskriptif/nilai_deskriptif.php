<?php  
	if(isset($_POST['id_kurikulum'])){
		$id_tingkat_kelas = $_POST['id_tingkat_kelas'];
		$id_kurikulum = $_POST['id_kurikulum'];
		$id_pelajaran = $_POST['id_pelajaran'];
		$id_komponen_nilai = $_POST['id_komponen_nilai'];
	}else{
		$id_tingkat_kelas = 0;
		$id_kurikulum = 0;
		$id_pelajaran = 0;
	}
	
	$this->load->model('evauasi/t_nilai_konversi_deskripsi');
	$pelajaran=null;
	$status=0;
	$nilai_huruf = $this->t_nilai_konversi_deskripsi->get_nilai_huruf($id_kurikulum);
	// print_r($nilai_huruf);
	// exit;
	$pelajaran = $this->t_nilai_konversi_deskripsi->get_pelajaran($id_kurikulum,$id_tingkat_kelas);
	$nilai_deskriptif = $this->t_nilai_konversi_deskripsi->get_datalist($id_tingkat_kelas, $id_pelajaran, $id_kurikulum);
	
	if($nilai_deskriptif){ foreach($nilai_deskriptif as $n){
		if($n['id_guru'] == get_id_personal()){
			$status=1;
		}
	}
	}
?>

<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
   	<div class="box">
			
		<div class="header">
			<h2>Nilai Deskriptif</h2>
		</div>
		<div class="tabletools">
			<div class="right">
			<?php if($gid == 2 || $gid == 3 || $gid == 4 || ($gid == 9 && $status==1)){ ?>
				<?php if($id_pelajaran){ ?>
					<a href="<?php echo base_url('evaluasi/nilai_deskriptif/add/?id='.$id_pelajaran); ?>"><i class="icon-plus"></i>Edit</a> 
				<?php }else{ ?>
					<a href="<?php echo base_url('evaluasi/nilai_deskriptif/add/?id=0'); ?>"><i class="icon-plus"></i>Edit</a> 
				<?php } ?>
			<?php } ?>
			  <br/><br/>
			</div>
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=70px align='left'><span class="text">Kurikulum &nbsp;</span></th>
							<td width=150px align='left'>
								<select name="id_kurikulum" onchange="submitform();">
									<?php 
										if($id_kurikulum==0){
											echo "<option selected value=0>-Pilih Kurikulum-</option>";
										}
									?>
									<?php if($kurikulum){ ?>
									<?php foreach($kurikulum as $k): ?>
										<?php if($k['id_kurikulum'] == $id_kurikulum ){ ?>
											<option selected value='<?php echo $k['id_kurikulum']; ?>'><?php echo $k['nama_kurikulum']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $k['id_kurikulum']; ?>'><?php echo $k['nama_kurikulum']; ?></option>
										<?php } ?>
									<?php endforeach; }  ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<th width=100px align='left'><span class="text">Komponen Nilai &nbsp;</span></th>
							<td width=200px align='left'>
								<select name="id_komponen_nilai">
									<?php if($komponen_nilai){ ?>
										<option value=0>-Pilih Komponen Nilai-</option>
									<?php foreach($komponen_nilai as $k): ?>
									<?php if($k['id_kurikulum'] == $id_kurikulum){ ?>
										<?php if($k['id_komponen_nilai'] == $id_komponen_nilai ){ ?>
											<option selected value='<?php echo $k['id_komponen_nilai']; ?>'><?php echo $k['komponen_nilai']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $k['id_komponen_nilai']; ?>'><?php echo $k['komponen_nilai']; ?></option>
										<?php } } ?>
									<?php endforeach; } ?>
								</select>
							</td>
						</tr>
						<tr>
							<th width=60px align='left'><span class="text">Tingkat &nbsp;</span></th>
							<td width=150px align='left'>
								<select name="id_tingkat_kelas" onchange="submitform();">
									<?php 
										if($id_tingkat_kelas==0){
											echo "<option selected value=0>-Pilih Tingkat Kelas-</option>";
										}
									?>
									<?php if($tingkat){ ?>
									<?php foreach($tingkat as $k): ?>
										<?php if($k['id_tingkat_kelas'] == $id_tingkat_kelas ){ ?>
											<option selected value='<?php echo $k['id_tingkat_kelas']; ?>'><?php echo $k['tingkat_kelas']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $k['id_tingkat_kelas']; ?>'><?php echo $k['tingkat_kelas']; ?></option>
										<?php } ?>
									<?php endforeach; } ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<th width=70px align='left'><span class="text">Pelajaran &nbsp;</span></th>
							<td width=200px align='left'>
								<select name="id_pelajaran" onchange="submitform();">
									<?php 
										if($id_pelajaran==0){
											echo "<option selected value=0>-Pilih Pelajaran-</option>";
										}
									?>
									<?php if($pelajaran){ $status=0; ?>
									<?php foreach($pelajaran as $k): ?>
										<?php if($k['id_pelajaran'] == $id_pelajaran ){ $status=1; ?>
											<option selected value='<?php echo $k['id_pelajaran']; ?>'><?php echo $k['pelajaran']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $k['id_pelajaran']; ?>'><?php echo $k['pelajaran']; ?></option>
										<?php } ?>
									<?php endforeach; } ?>
									<?php 
										if($status==0 && $id_pelajaran!=0){
											echo "<option selected value=0>-Pilih Pelajaran-</option>";
										}
									?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
			<div class="dataTables_wrapper"  role="grid">
				<table class="styled" >
					<thead>
						<tr colspan='2'>
						</tr>
						<tr>
							<td width='20px' class='center'>No.</td>
							<td width='' class='center'>Nilai Huruf</td>
							<td width='' class='center'>Range Nilai Angka</td>
							<td width='' class='center'>Deskripsi</td>
						</tr>
					</thead>
					<tbody>
						<?php 
							if($nilai_huruf && $id_pelajaran!=0){
							$no=1;
							foreach($nilai_huruf as $temp){
						?>
						<tr>
							<td class='center'>
								<?php
									echo $no;
									$no++;
								?>
							</td>
							<td class='center'><?php echo $temp['nilai_huruf'];?></td>
							<td class='center'><?php echo number_format($temp['nilai_angka_min'],2)." < x <= ".number_format($temp['nilai_angka_max'],2);?></td>
							<td class='left'>
								<?php
									$deskripsi = $this->t_nilai_konversi_deskripsi->get_deskripsi($id_pelajaran, $temp['id_nilai_konversi']);
									if($deskripsi['deskripsi']){
										echo $deskripsi['deskripsi'];
									}else{
										echo "";
									}
								?>
							</td>
						</tr>
						<?php
							}
							}else{
								echo "<tr><td colspan='3'>Belum ada data!</td></tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div><!-- End of .content -->
		
	</div><!-- End of .box -->		
 </div>
 
 <script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>
