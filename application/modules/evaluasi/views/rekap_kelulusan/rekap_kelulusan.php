<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekap Lulusan</h2>
       </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=2>Tahun</th>
								<th colspan=2>Lulus</th>															
								<th rowspan=4>Rata-Rata NUAN</th>
								
							</tr>
							<tr>
								<th>Jumlah</th>
								<th>%</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach($data_lulusan as $temp){ ?>
							<tr>
								<td width='' class='center'><?php echo $temp['tahun_akhir'];?></td>
								<td width='' class='center'><?php echo $temp['jml_lulus'];?></td>
								<td width='' class='center'><?php echo $temp['total'];?> %</td>
								<td width='' class='center'>
									<?php
										$i=0;
										$status=0;
										while(isset($array_ta[$i])){
											if($temp['tahun_akhir']==$array_ta[$i]['tahun_akhir']){
												$j=0;
												while(isset($rata[$j])){
													if($array_ta[$i]['id_tahun_ajaran']==$rata[$j]['id_tahun_ajaran']){
														echo $rata[$j]['nilai'];
														$status=1;
													}
													$j++;
												}
											}
											$i++;
										}
										if($status==0){
											echo "0";
										}
									?>
								</td>
							</tr>
							<?php }?>
						</tbody>
					</table>
					
				</div>
            </div>
        </div>        
    </div>
 </div>
