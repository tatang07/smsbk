<h1 class="grid_12">Kriteria Ketuntasan Belajar</h1>

<form action="" class="grid_12 validate" novalidate="novalidate" method="post" id="valid" enctype="multipart/form-data">
	<fieldset>
		<legend>Penambahan Data Kriteria Ketuntasan Belajar</legend>

		<div class="row">
			<label><strong>Kurikulum</strong></label>
			<div>
				<input type="text" value="<?php echo $kkm['nama_kurikulum']; ?>" disabled>
			</div>
		</div>

		<div class="row">
			<label class="" for="id_tingkat_kelas">
				<strong>Tingkat Kelas</strong>
			</label>
			<div>
				<input type="text" value="<?php echo $kkm['tingkat_kelas']; ?>" disabled>
			</div>
		</div>

		<div class="row">
			<label class="" for="id_pelajaran">
				<strong>Pelajaran</strong>
			</label>
			<div>
				<input type="text" value="<?php echo $kkm['pelajaran']; ?>" disabled>
			</div>
		</div>															
		<div class="row">
			<label class="" for="nilai_kkm">
				<strong>KKM</strong>
			</label>
			<div>
				<input type="text" name="nilai_kkm" label="KKM" id="nilai_kkm" class="novalidate"  value="<?php echo $kkm['nilai_kkm']; ?>" />
			</div>
		</div>															
		<div class="row">
			<label class="" for="file_dokumen_kkm">
				<strong>Dokumen KKM</strong>
			</label>
			<div>
			<em style="display:block;margin-bottom:-15px">kosongkan bila tidak akan mengganti dokumen</em>
				<input type="file" name="file_dokumen_kkm" value="" label="Dokumen KKM" id="file_dokumen_kkm" class="novalidate" />
			</div>
		</div>

	</fieldset>
	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" />
			<a href="http://localhost/smsbkv2/evaluasi/kkm/datalist//1"> <input type="button" value="Batal" /></a>
		</div>
	</div>
</form>

<script type="text/javascript">		
	$(function(){
		$('#id_kurikulum').chosen().change(function(){
			window.location = "<?php echo site_url('evaluasi/kkm/tambah'); ?>/" + $(this).val();
		})
		$('#id_tingkat_kelas').chosen().change(function(){
			window.location = "<?php echo site_url('evaluasi/kkm/tambah/'.$this->uri->segment(4)); ?>/" + $(this).val();
		})
	})
</script>