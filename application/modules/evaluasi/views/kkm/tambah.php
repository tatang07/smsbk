﻿<h1 class="grid_12">Kriteria Ketuntasan Belajar</h1>

<form action="" class="grid_12 validate" novalidate="novalidate" method="post" id="valid" enctype="multipart/form-data">
	<fieldset>
		<legend>Penambahan Data Kriteria Ketuntasan Belajar</legend>

		<div class="row">
			<label><strong>Kurikulum</strong></label>
			<div>
				<?php echo simple_form_dropdown_clear("id_kurikulum", $kurikulum, $id_kurikulum, "", null) ?>
			</div>
		</div>

		<?php if ($id_kurikulum): ?>
			
		<div class="row">
			<label class="" for="id_tingkat_kelas">
				<strong>Tingkat Kelas</strong>
			</label>
			<div>
				<?php echo simple_form_dropdown_clear("id_tingkat_kelas", $tingkat_kelas, $id_tingkat_kelas, "", null) ?>
			</div>
		</div>

		<?php if ($id_tingkat_kelas): ?>
		<div class="box with-table">
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Mata Pelajaran</th>
								<th>KKM</th>
								<th>Dokumen KKM</th>
							</tr>
							
						</thead>
						<tbody>
						<?php $no=1; foreach($nilai as $p => $val): ?>
						<tr>
							<td width='25px' class="center"><?php echo $no++; ?></td>
							<td width='400px' class=""><?php echo $val['pelajaran']; ?></td>
							<td width='50px' class="center"><input type="text" size="4" style="text-align: right;" name="nilai_kkm[<?php echo $p; ?>]" value="<?php echo $val['nilai']; ?>" label="KKM" id="nilai_kkm<?php echo $p; ?>" class="novalidate"   /></td>
							<td width='200px' class=""><input type="file" name="file_dokumen_kkm<?php echo $p; ?>" value="<?php echo $val['file']; ?>" label="Dokumen KKM" id="file_dokumen_kkm" class="novalidate" /></td>
						</tr>
						<?php endforeach; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		</div>
		
		<?php endif ?>
		<?php endif ?>

	</fieldset>
	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" />
			<a href="<?php echo base_url('evaluasi/kkm/datalist/'); ?>"> <input type="button" value="Batal" /></a>
		</div>
	</div>
</form>

<script type="text/javascript">		
	$(function(){
		$('#id_kurikulum').chosen().change(function(){
			window.location = "<?php echo site_url('evaluasi/kkm/tambah'); ?>/" + $(this).val();
		})
		$('#id_tingkat_kelas').chosen().change(function(){
			window.location = "<?php echo site_url('evaluasi/kkm/tambah/'.$this->uri->segment(4)); ?>/" + $(this).val();
		})
	})
</script>