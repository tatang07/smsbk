<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
   	<div class="box">
			
		<div class="header">
			<h2>Detail Nilai Ujian Nasional</h2>
		</div>
			<div class="dataTables_wrapper"  role="grid">
				<table class="styled">
					<thead>
						<tr></tr>
					</thead>
					<tbody>
						<tr>
							<td colspan='2'></td>
						</tr>
						<tr>
							<td width='70px' class='right'><strong>NIS</strong></td>
							<td><?php echo $identitas['nis'];?></td>
						</tr>
						<tr>
							<td><strong>Nama</strong></td>
							<td><?php echo $identitas['nama'];?></td>
						</tr>
						<tr>
							<td><strong>Kelas</strong></td>
							<td><?php echo $identitas['rombel'];?></td>
						</tr>
						<tr>
							<td colspan='2'></td>
						</tr>
					</tbody>
				</table>
				<table class="styled" >
					<thead>
						<tr>
							<th>No.</th>
							<th>Kode Mata Pelajaran</th>
							<th>Mata Pelajaran</th>
							<th>Nilai</th>
						</tr>
					</thead>
					<tbody>
						<?php $no=1;?>
						<?php foreach($detail as $temp){?>
						<tr>
							<td width='30px' class='center'><?php echo $no;?></td>
							<td width='150px' class='center'><?php echo $temp['kode_pelajaran'];?></td>
							<td width='' class='right'><?php echo $temp['pelajaran'];?></td>
							<td width='100px' class='center'><?php echo $temp['nilai'];?></td>
						</tr>
						<?php $no++;}?>
					</tbody>
				</table>
			</div>
			<div class="actions">
				<div class="left">
					<a href="<?php echo base_url('evaluasi/nilai_unas'); ?>"> <input value="Kembali" type="button"></a>
				</div>
			</div>
		</div><!-- End of .content -->
		
	</div><!-- End of .box -->		
 </div>
