<?php  
	if(isset($_POST['id_tahun_ajaran'])){
		$a = $_POST['id_tahun_ajaran'];
		$statuspil = $a;
	}else{
		$statuspil = 1;
	}
?>

<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Sepuluh Peraih Nilai Ujian Nasional Terbaik</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url();?>evaluasi/nilai_unas/print_pdf/<?php echo $statuspil; ?>"><i class="icon-print"></i>Cetak</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=80px align='left'><span class="text"><span>Tahun Ajaran</span></th>
							<td width=200px align='left'>
								<select name="id_tahun_ajaran" id="id_tahun_ajaran" onchange="submitform();">
									<?php foreach($tahun_ajaran as $ta): ?>
										<?php if($ta['id_tahun_ajaran'] == $statuspil){ ?>
											<option selected value='<?php echo $ta['id_tahun_ajaran']; ?>' data-status-pilihan="<?php echo $ta['id_tahun_ajaran']; ?>"><?php echo $ta['tahun_awal'].'/'.$ta['tahun_akhir']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $ta['id_tahun_ajaran']; ?>' data-status-pilihan="<?php echo $ta['id_tahun_ajaran']; ?>"><?php echo $ta['tahun_awal'].'/'.$ta['tahun_akhir']; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
            
        </div>
		<?php
			$this->load->model('evaluasi/v_siswa');
			$id_tahun_ajaran = $statuspil;
			$nilai = $this->v_siswa->get_data_nilai_unas($id_tahun_ajaran);
		?>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama Siswa</th>
								<th>Kelas</th>
								<th>Jumlah Nilai</th>
								<th>Rata-rata</th>
								<th colspan='2'>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1;?> 
							<?php if(isset($nilai[0]['nis'])){?> 
							<?php foreach ($nilai as $temp) {?>
							<tr>
								<td width='' class='center'><?php echo $no;?></td>
								<td width='' class='center'><?php echo $temp['nis'];?></td>
								<td width='' class='center'><?php echo $temp['nama'];?></td>
								<td width='' class='center'><?php echo $temp['rombel'];?></td>
								<td width='' class='center'><?php echo $temp['jml'];?></td>
								<td width='' class='center'><?php echo $temp['rata'];?></td>
								<td width='' class='center'>
									<a href='<?php echo base_url('evaluasi/nilai_unas/detail/'.$temp['id_siswa']);?>' class="button small grey tooltip" data-gravity="s"><i class="icon-file"></i> Detail</a>
								</td>
							</tr>
							<?php $no=$no+1;?>
							<?php } ?>
							<?php } ?>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $no-1;?></div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
							<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
							<span>
								<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
							</span>
							<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
 
 <script>
	function submitform(){
		document.myform.submit();
	}
</script>
