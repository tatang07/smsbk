<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Sepuluh Peraih Nilai Ujian Nasional Terbaik</h2>
       </div>
     
		<?php
			$this->load->model('evaluasi/v_siswa');
			$id_tahun_ajaran = $statuspil;
			$nilai = $this->v_siswa->get_data_nilai_unas($id_tahun_ajaran);
		?>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" width="100%">
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama Siswa</th>
								<th>Kelas</th>
								<th>Jumlah Nilai</th>
								<th>Rata-rata</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1;?> 
							<?php if(isset($nilai[0]['nis'])){?> 
							<?php foreach ($nilai as $temp) {?>
							<tr>
								<td width='' class='center'><?php echo $no;?></td>
								<td width='' class='center'><?php echo $temp['nis'];?></td>
								<td width='' class='center'><?php echo $temp['nama'];?></td>
								<td width='' class='center'><?php echo $temp['rombel'];?></td>
								<td width='' class='center'><?php echo $temp['jml'];?></td>
								<td width='' class='center'><?php echo $temp['rata'];?></td>
							</tr>
							<?php $no=$no+1;?>
							<?php } ?>
							<?php } ?>
						</tbody>
					</table>
					
				</div>
            </div>
        </div>        
    </div>
 </div>