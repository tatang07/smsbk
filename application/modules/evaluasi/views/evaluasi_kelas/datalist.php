<?php if($gid == 9){
		$rombel = array("0"=>"-Pilih-")+get_rombel_guru_matpel_rombel_by_guru(get_id_personal());
	}else{
		$rombel = array("0"=>"-Pilih-")+get_rombel();
	}
		?>
<?php $pelajaran = get_pelajaran_guru_matpel_rombel() ?>
<?php $komponen = get_rombel_komponen_nilai() ?>
<?php $allkomponen =  array("0"=>"-Pilih-")+get_list("m_komponen_nilai","id_komponen_nilai","komponen_nilai"); ?>
<?php $jml_bobot = 0 ?>

<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
	
	    <div class="box with-table">
       <div class="header">
            <h2>Evaluasi Kelas</h2>
       </div>
       <div class="tabletools">
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" >
					<table>
						<tr>
							<th width=100px align='left'><span class="text">Kelas</span></th>
							<td width=300px align='left'>
								<?php echo simple_form_dropdown_clear("id_rombel",$rombel,$id_rombel," id='idrombel' onChange=rombelChanged(this.value)",null) ?>
							</td>
						</tr>
						<tr>
							<th width=80px align='left'><span class="text">Mata Pelajaran</span></th>
							<td width=100px align='left'>
								<?php foreach($rombel as $id=>$det):?>
									<?php $mapel =  array("0"=>"-Pilih-") ?>
									<?php $mapel += isset($pelajaran[$id])?$pelajaran[$id]:array() ?>
									<div id='mapel<?php echo $id?>' <?php echo $id_rombel!=$id?"style='display:none'":""?> class='listmapel'>					
										<?php echo simple_form_dropdown_clear("id_gmr".$id,$mapel,$id_gmr,"",null) ?>
									</div>	
								<?php endforeach ?>
							</td>
						</tr>
						<tr>
							<th width=80px align='left'><span class="text">Komponen</span></th>
							<td width=100px align='left'>
								<?php foreach($rombel as $id=>$det):?>
									<?php $komp =  array("0"=>"-Pilih-") ?>
									<?php $komp += isset($komponen[$id])?$komponen[$id]:array() ?>
									<div id='komp<?php echo $id?>' <?php echo $id_rombel!=$id?"style='display:none'":""?> class='listkomp'>					
										<?php echo simple_form_dropdown_clear("id_komponen_nilai".$id,$komp,$id_komponen_nilai," id='idkomponen' ",null) ?>
									</div>	
								<?php endforeach ?>
							</td>
						</tr>
						
						<tr>
							<td width=1px>&nbsp;</td>
							<td align='left'>
								<a href="javascript:document.getElementById('formPencarian').submit();" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
	</div>
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=3>No.</th>
								<th rowspan=3>NIS</th>
								<th rowspan=3>Nama</th>
								<?php if(count($nilai)>0):?>
									<th colspan=<?php echo count($nilai)>1?count($nilai)+1:1 ?>>Nilai</th>
								<?php endif ?>
							</tr>
							<?php if(count($nilai)>0):?>
								<tr>
									<?php foreach($nilai as $n):?>
										<th><?php echo $n['nama_nilai_kategori']?></th>
									<?php endforeach ?>
									<?php if(count($nilai)>1):?>
										<th rowspan=2>Nilai Akhir</th>
									<?php endif ?>
								</tr>
								<tr>
									<?php foreach($nilai as $n):?>
										<th><?php echo $n['bobot']?>%</th>
										<?php $jml_bobot += $n['bobot'] ?>
									<?php endforeach ?>
									<?php $jml_bobot = $jml_bobot==0?1:$jml_bobot ?>
								</tr>
							<?php endif ?>
						</thead>
						<tbody>
						  <?php $no = 1 ?>
						  <?php foreach($siswa as $sis):?>
							<tr>
								<td width='10px' class='center'><?php echo $no++ ?></td>
								<td width='70px' class='center'><?php echo $sis['nis']?></td>
								<td width=''><?php echo $sis['nama']?></td>
								<?php if(count($nilai)>0):?>
									<?php $sum = 0 ?>
									<?php foreach($nilai as $n):?>
										<?php $nil = isset($nilai_detail[$sis['id_siswa']][$n['id_nilai_kategori']]['nilai'])?$nilai_detail[$sis['id_siswa']][$n['id_nilai_kategori']]['nilai']:0 ?>
										<?php $sum += $nil * $n['bobot'] ?>
										<td width='50px' class='center'><?php echo round($nil,2)?></td>
									<?php endforeach ?>
									<?php if(count($nilai)>1):?>
										<td width='50px' class='center'><?php echo round($sum/$jml_bobot,2) ?></td>
									<?php endif ?>	
								<?php endif ?>
							</tr>							
						  <?php endforeach?>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info"></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>

	<script type="text/javascript">		
		function rombelChanged(id){
			$('.listmapel').hide();
			$('.listkomp').hide();
			if(id>0){
				$('#mapel'+id).show();				
				$('#komp'+id).show();				
			}
		}
	</script>