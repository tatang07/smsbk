<?php 
	$statuspil = $id_tahun_ajaran;
	$statuspilterm = $id_term;
	$statuspiltingkat = $id_tingkat;
	if(isset($_POST['id_tahun_ajaran'])){
	$a = $_POST['id_tahun_ajaran'];
	$statuspil = $a; }
	if(isset($_POST['id_term'])){
	$a = $_POST['id_term'];
	$statuspilterm = $a; }
	if(isset($_POST['id_tingkat'])){
	$a = $_POST['id_tingkat'];
	$statuspiltingkat = $a; }	?>

<?php $id_tingkat=0; if($gid == 9){
		$rombel = array("0"=>"-Semua Kelas-")+get_rombel_guru_matpel_rombel_by_guru_tingkat(get_id_personal(), $statuspiltingkat);
	}else{
		$rombel = array("0"=>"-Semua Kelas-")+get_rombel_by_tingkat($statuspiltingkat);
	}
		?>
<?php $pelajaran = get_pelajaran_guru_matpel_rombel() ?>

<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
	
	    <div class="box with-table">
       <div class="header">
            <h2>Ranking Siswa</h2>
       </div>
       <div class="tabletools">
			<div class="dataTables_filter">
				<form action="" name= "myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text">Tahun Ajaran</span></th>
							<td width=150px align='left'>
								<select name="id_tahun_ajaran" id="id_tahun_ajaran" onchange="submitform();">
									<option value="0">-Pilih Tahun Ajaran-</option>
								<?php foreach($tahun_ajaran as $q): ?>
									
									<?php if($q->id_tahun_ajaran == $statuspil){ ?>
										<option selected value='<?php echo $q->id_tahun_ajaran; ?>'><?php echo $q->tahun_awal.'/'.$q->tahun_akhir; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $q->id_tahun_ajaran; ?>'><?php echo $q->tahun_awal.'/'.$q->tahun_akhir; ?></option>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
						</tr>
						<tr>
							<th width=100px align='left'><span class="text">Semester</span></th>
							<td width=150px align='left'>
								<select name="id_term" >
									<option value="0">-Pilih Semester-</option>
								<?php foreach($term as $q): ?>
									
									<?php if($q->id_tahun_ajaran == $statuspil){ ?>
									<?php if($q->id_term == $statuspilterm){ ?>
										<option selected value='<?php echo $q->id_term; ?>'><?php echo $q->term; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $q->id_term; ?>'><?php echo $q->term; ?></option>
									<?php } ?>
								<?php } endforeach; ?>
								</select>
							</td>
						</tr>
						<tr>
							<th width=100px align='left'><span class="text">Tingkat</span></th>
							<td width=150px align='left'>
								<select name="id_tingkat" id="id_tingkat" onchange="submitform();">
									<option value="0">-Pilih Tingkat-</option>
								<?php foreach($tingkat as $q): ?>
									
									<?php if($q->id_tingkat_kelas == $statuspiltingkat){ ?>
										<option selected value='<?php echo $q->id_tingkat_kelas; ?>'><?php echo $q->tingkat_kelas; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $q->id_tingkat_kelas; ?>'><?php echo $q->tingkat_kelas; ?></option>
									<?php } ?>
								<?php endforeach; ?>
								</select>
							</td>
						</tr>
				</form>
                <form action="<?php echo base_url('evaluasi/ranking_siswa/search'); ?>" method="post" enctype="multipart/form-data">
					<input type="hidden" name="id_tahun_ajaran" value="<?php echo $statuspil; ?>">
					<input type="hidden" name="id_term" value="<?php echo $statuspilterm; ?>">
					<input type="hidden" name="id_tingkat" value="<?php echo $statuspiltingkat; ?>">
						<tr>
							<th width=100px align='left'><span class="text">Kelas</span></th>
							<td width=150px align='left'>
								<?php echo simple_form_dropdown_clear("id_rombel",$rombel,$id_rombel," id='idrombel' onChange=rombelChanged(this.value)",null) ?>
							</td>
						</tr>
						<tr>
							<th width=80px align='left'><span class="text">Mata Pelajaran</span></th>
							<td width=100px align='left'>
								<?php foreach($rombel as $id=>$det):?>
									<?php $mapel =  array("0"=>"-Semua Mata Pelajaran-") ?>
									<?php $mapel += isset($pelajaran[$id])?$pelajaran[$id]:array() ?>
									<div id='mapel<?php echo $id?>' <?php echo $id_rombel!=$id?"style='display:none'":""?> class='listmapel'>					
										<?php echo simple_form_dropdown_clear("id_gmr".$id,$mapel,$id_gmr,"",null) ?>
									</div>
								<?php endforeach ?>
								<div id="allmapel" style="display: none">
									<select name="id_gmr"">
										<option value="0">-Semua Mata Pelajaran-</option>
									<?php foreach($gmr as $q): ?>
										
										<?php if($q->id_tahun_ajaran == $statuspil && $q->id_tingkat_kelas == $statuspiltingkat){ ?>
											<option selected value='<?php echo $q->id_pelajaran; ?>'><?php echo $q->kode_pelajaran.'-'.$q->pelajaran; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q->id_pelajaran; ?>'><?php echo $q->kode_pelajaran.'-'.$q->pelajaran; ?></option>
										<?php } ?>
									<?php endforeach; ?>
									</select>
								</div>
							</td>
						</tr>
						<tr>
							<td width=1px>&nbsp;</td>
							<td align='right'>
								<input type="submit" name=send class="button grey tooltip" value="cari" />
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
	</div>
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Kelas</th>
								<th>Nilai</th>
								<th>Ranking</th>
							</tr>
						</thead>
						<tbody>
							<?php $no = 1 ?>
						  <?php if($hasil){ foreach($hasil as $sis):?>
							<tr>
								<td width='10px' class='center'><?php echo $no++ ?></td>
								<td width='70px' class='center'><?php echo $sis['nis']?></td>
								<td width='400px'><?php echo $sis['nama']?></td>
								<td width='100px' class='center'><?php echo $sis['rombel']?></td>
								<td width='70px' class='center'><?php echo $sis['nilai']?></td>
								<td width='70px' class='center'><?php echo $sis['ranking']?></td>
							</tr>							
						  <?php endforeach; } ?>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info"></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>

	<script type="text/javascript">		
		function submitform()
		{
		  document.myform.submit();
		}
		
		function rombelChanged(id){
			$('.listmapel').hide();
			$('.listkomp').hide();
			if(id>0){
				$('#mapel'+id).show();				
				$('#allmapel').hide();				
			}else{
				$('#allmapel').show();				
			}
		}
	</script>