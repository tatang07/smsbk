
<?php $rombel = array("0"=>"-Pilih Kelas-")+get_rombel_akhir() ?>
<?php
	// if(isset($_POST['id_tahun_ajaran'])){
		// $a = $_POST['id_tahun_ajaran'];
		// $statuspil = $a;
	// }else{
		// $statuspil = 1;
	// }
?>


<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
	
	<div class="box with-table">
       <div class="header">
            <h2>Rekap Hasil Ujian Nasional</h2>
       </div>
       <div class="tabletools">
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" >
					<table>
						<tr>
							<th width=100px align='left'><span class="text">Kelas</span></th>
							<td width=150px align='left'>
								<?php echo simple_form_dropdown_clear("id_rombel",$rombel," id='idrombel' onChange=rombelChanged(this.value)",null) ?>
							</td>
						</tr>
						
						<tr>
							<th width=80px align='left'><span class="text"><span>Tahun Ajaran</span></th>
							<td width=200px align='left'>
								<select name="id_tahun_ajaran" id="id_tahun_ajaran" onchange="submitform();">
									<option value='0'>-Pilih Tahun Ajaran-</option>
									<?php foreach($tahun_ajaran as $ta): ?>
										<?php if($ta['id_tahun_ajaran'] == $id_tahun_ajaran){ ?>
											<option selected value='<?php echo $ta['id_tahun_ajaran']; ?>' data-status-pilihan="<?php echo $ta['id_tahun_ajaran']; ?>"><?php echo $ta['tahun_awal'].'/'.$ta['tahun_akhir']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $ta['id_tahun_ajaran']; ?>' data-status-pilihan="<?php echo $ta['id_tahun_ajaran']; ?>"><?php echo $ta['tahun_awal'].'/'.$ta['tahun_akhir']; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>						
						
						<tr>
							<td width=1px>&nbsp;</td>
							<td align='left'>
								<a href="javascript:document.getElementById('formPencarian').submit();" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>            
        </div>
	</div>
	<?php
			$this->load->model('evaluasi/t_ujian_nasional');
			// $id_tahun_ajaran = $statuspil;
			$hasil = $this->t_ujian_nasional->get_data_hasil_un($id_tahun_ajaran, $id_rombel);
			// print_r($hasil);
		?>
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper" role="grid">
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan='2'>No.</th>
								<th rowspan='2'>NIS</th>
								<th rowspan='2'>Nama Siswa</th>
								<th colspan='<?php echo $count_mapel; ?>'>Nilai</th>
								<th rowspan='2'>Jumlah Nilai</th>
								<th rowspan='2'>Rata-rata</th>								
							</tr>
							<tr>
								<?php foreach ($mapel_nilai as $n) {?>
									<th><?php echo $n['pelajaran']; ?></th>
								<?php } ?>
							</tr>
						</thead>
					  
						<?php if($id_tahun_ajaran && $id_rombel != 0){ ?> 
						
						<tbody>
								
						  <?php $no=1;?> 
							<?php if(isset($hasil[0]['nis'])){?> 
							<?php foreach ($hasil as $ha) {?>
							<tr>
								<td width='' class='center'><?php echo $no;?></td>
								<td width='' class='center'><?php echo $ha['nis'];?></td>
								<td width='' class='center'><?php echo $ha['nama'];?></td>
								<?php $jumlah=0; $banyak=0; foreach ($mapel_nilai as $n) {?>
								<?php if($n['id_pelajaran'] == $ha['id_pelajaran']) {?>
								<td width='' class='center'><?php echo $ha['nilai'];?></td>
								<?php $jumlah=$jumlah+$ha['nilai']; $banyak++; } ?>
								<?php } ?>
								<td width='' class='center'><?php echo $jumlah;?></td>
								<td width='' class='center'><?php echo $jumlah/$banyak;?></td>								
								
							</tr>
							<?php $no=$no+1;?>
							<?php } ?>
							<?php } ?>
						</tbody>
							<?php }else{ ?>
							<tbody>
								<tr><td colspan=<?php echo $count_mapel+5; ?>>Pilih Kelas dan Tahun Ajaran</td></tr>
							</tbody>
							<?php } ?>
					</table>
					<div class="footer">
						<div class="dataTables_info"></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
<!--
	<script type="text/javascript">		
		function rombelChanged(id){
			$('.listmapel').hide();
			$('.listkomp').hide();
			if(id>0){
				$('#mapel'+id).show();				
				$('#komp'+id).show();				
			}
		}
	</script>