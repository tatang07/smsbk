<h1 class="grid_12">
<h1 class="grid_12">Komponen Penilaian</h1>
<div class="grid_12">
	<form action="" class="grid_12 validate no-box" novalidate="novalidate" method="post" id="valid" enctype="multipart/form-data">
		<fieldset>
			<legend>Penambahan Data Komponen Penilaian</legend>

			<div class="row">
				<label><strong>Kurikulum</strong></label>
				<div>
					<input type="text" value="<?php echo $kurikulum['nama_kurikulum'] ?>" disabled>
				</div>
			</div>

			<div class="row">
				<label><strong>Komponen Nilai</strong></label>
				<div>
					<input type="text" value="<?php echo $kurikulum['komponen_nilai'] ?>" disabled>
				</div>
			</div>
			
			<div class="row">
				<label><strong>Nama Nilai Kategori</strong></label>
				<div>
					<input type="text" name="nama_nilai_kategori" id="nama_nilai_kategori" value="<?php echo $kurikulum['nama_nilai_kategori'] ?>" class="novalidate">
				</div>
			</div>

			<div class="row">
				<label><strong>Bobot (%)</strong></label>
				<div>
					<input type="text" name="bobot" value="<?php echo $kurikulum['bobot'] ?>" id="bobot" value="" class="novalidate">
				</div>
			</div>

			<div class="row">
				<label><strong>Bernilai Tunggal</strong></label>
				<div>
					<select name="bernilai_tunggal" id="bernilai_tunggal">
						<option value="0">Tidak</option>
						<option value="1" <?php echo ($kurikulum['bernilai_tunggal'])? 'selected="selected"': ''; ?>>Ya</option>
					</select>
				</div>
			</div>
		</fieldset>

		<div class="actions">
			<div class="left">
				<input type="submit" value="Simpan">
				<a href="<?php echo base_url('evaluasi/komponen_evaluasi'); ?>" class="button grey"> Batal</a>
			</div>
		</div>
	</form>
</div>