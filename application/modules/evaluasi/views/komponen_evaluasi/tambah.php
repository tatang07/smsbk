<h1 class="grid_12">
<h1 class="grid_12">Komponen Penilaian</h1>
<div class="grid_12">
	<form action="" class="grid_12 validate no-box" novalidate="novalidate" method="post" id="valid" enctype="multipart/form-data">
		<fieldset>
			<legend>Penambahan Data Komponen Penilaian</legend>

			<div class="row">
				<label><strong>Kurikulum</strong></label>
				<div>
					<?php echo simple_form_dropdown_clear("id_kurikulum", $kurikulum, $id_kurikulum, "", null) ?>
				</div>
			</div>

			<?php if ($id_kurikulum): ?>
				
			<div class="row">
				<label><strong>Komponen Nilai</strong></label>
				<div>
					<?php echo simple_form_dropdown_clear("id_komponen_nilai", $komponen_nilai, $id_komponen_nilai, "", null) ?>
				</div>
			</div>

			
			<?php if ($id_komponen_nilai): ?>
			<div class="row">
				<label><strong>Nama Nilai Kategori</strong></label>
				<div>
					<input type="text" name="nama_nilai_kategori" value="" id="nama_nilai_kategori" class="novalidate">
				</div>
			</div>

			<div class="row">
				<label><strong>Bobot (%)</strong></label>
				<div>
					<input type="text" name="bobot" value="" id="bobot" class="novalidate">
				</div>
			</div>

			<div class="row">
				<label><strong>Bernilai Tunggal</strong></label>
				<div>
					<select name="bernilai_tunggal" id="bernilai_tunggal">
						<option value="0">Tidak</option>
						<option value="1">Ya</option>
					</select>
				</div>
			</div>
		</fieldset>

		<div class="actions">
			<div class="left">
				<input type="submit" value="Simpan">
				<a href="<?php echo base_url('evaluasi/komponen_evaluasi'); ?>" class="button grey"> Batal</a>
			</div>
		</div>
		<?php endif; ?>
		<?php endif; ?>
	</form>
</div>

<script type="text/javascript">		
	$(function(){
		$('#id_kurikulum').chosen().change(function(){
			window.location = "<?php echo site_url('evaluasi/komponen_evaluasi/tambah'); ?>/" + $(this).val();
		})
		$('#id_komponen_nilai').chosen().change(function(){
			window.location = "<?php echo site_url('evaluasi/komponen_evaluasi/tambah/'.$this->uri->segment(4)); ?>/" + $(this).val();
		})
	})
</script>