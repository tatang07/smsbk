<?php $jml_bobot = 0 ?>

<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
	
	<div class="box with-table">
		<div class="header">
			<h2>Komponen Penilaian</h2>
		</div>
		<div class="tabletools">
			<div class="right">
			<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
						<a href="<?php echo site_url('evaluasi/komponen_evaluasi/tambah/'.$id_kurikulum.'/'.$id_komponen_nilai); ?>"><i class="icon-plus"></i>Tambah</a>
				<?php } ?>
			</div>
			<div class="dataTables_filter">
				<form id="formPencarian" name="formPencarian" method="post" >
					<table>
						<tr>
							<th align='left'><span class="text">Kurikulum</span></th>
							<td width=10px align='left'>
								<div id='mapel<?php echo $id?>' class='listmapel'>
									<?php echo simple_form_dropdown_clear("id_kurikulum", $kurikulum, $id_kurikulum, "", null) ?>
								</div>
							</td>
							<td width="30px"></td>
							<?php if ($id_kurikulum): ?>
								<th align='left'><span class="text">Komponen Nilai</span></th>
								<td width=100px align='left'>
									<div id='mapel<?php echo $id?>' class='listmapel'>
										<?php echo simple_form_dropdown_clear("id_komponen_nilai", $komponen_nilai, $id_komponen_nilai, "", null) ?>
									</div>
								</td>
							<?php endif ?>
						</tr>
					</table>
				</form>
			</div>

		</div>
	</div>
	<div class="box with-table">
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Penilaian</th>
								<th>Bobot (%)</th>
								<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
									<th>Aksi</th>
								<?php } ?>
							</tr>
						</thead>
						<?php if (isset($nilai)): ?>
							<tbody>
								<?php $no=1 ?>
								<?php foreach($nilai as $det):?>
									<tr>
										<td width='10px' class='center'><?php echo $no++ ?></td>
										<td width=''><?php echo $det['nama_nilai_kategori']?></td>
										<td width='100px' class='center'><?php echo $det['bobot']?></td>
										<?php if($gid == 2 || $gid == 3 || $gid == 4){ ?>
										<td width='150px' class='center'>
											<a href="<?php echo site_url('evaluasi/komponen_evaluasi/edit/'.$det['id_nilai_kategori']); ?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
											<a href="<?php echo site_url('evaluasi/komponen_evaluasi/delete/'.$det['id_nilai_kategori']); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
										</td>
										<?php } ?>
									</tr>
								<?php endforeach ?>
							</tbody>
						<?php endif ?>
					</table>
					<div class="footer">
						<div class="dataTables_info"></div>
						
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">		
	$(function(){
		$('#id_kurikulum').chosen().change(function(){
			window.location = "<?php echo site_url('evaluasi/komponen_evaluasi/datalist'); ?>/" + $(this).val();
		})
		$('#id_komponen_nilai').chosen().change(function(){
			window.location = "<?php echo site_url('evaluasi/komponen_evaluasi/datalist/'.$this->uri->segment(4)); ?>/" + $(this).val();
		})
	})
</script>