	<h1 class="grid_12">Evaluasi Pembelajaran</h1>
			
			<form action="<?php echo base_url('evaluasi/detail_evaluasi_kelas/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Detail Evaluasi Kelas</legend>
					<div class="row">
						<br>
						<table>
						<input type="hidden" name="action" value="tambah">
						<input type="hidden" name="kategori" value="<?php echo $tunggal?>">
						<input type="hidden" name="nilai_kategori" value="<?php echo $nilai_kategori; ?>">
						<input type="hidden" name="gmr" value="<?php echo $id_gmr; ?>">
						<input type="hidden" name="term" value="<?php echo $id_term; ?>">
							<tr>
								<td width=100px >Kelas</td>
								<td width=10px >:</td>
								<td width=300px ><?php echo $rombel; ?></td>
								<td width=100px >Komponen</td>
								<td width=10px >:</td>
								<td width=300px ><?php echo $komponen; ?></td>
							</tr>
							<tr>
								<td width=100px height=30px>Mata Pelajaran</td>
								<td width=10px height=30px>:</td>
								<td width=300px height=30px><?php echo $pelajaran; ?> - <?php echo $nama; ?></td>
								<td width=100px >Penilaian</td>
								<td width=10px >:</td>
								<td width=300px ><?php echo $penilaian; ?></td>
							</tr>
							<tr>
								<td width=150px height=30px>Label Nilai (Mis: Tugas-1)</td>
								<td width=10px height=30px>:</td>
								<td width=300px height=30px><input class="required" type="text" name="nilai" /></td>
							</tr>
						</table>
						<br>
					</div>

				</fieldset><!-- End of fieldset -->
				
				<fieldset>
					<legend>Siswa</legend>
				</fieldset><!-- End of fieldset -->
					
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">
							<table class="styled" >
								<thead>
									<tr>
										<th>No.</th>
										<th>NIS</th>
										<th>Nama</th>
										<th>Nilai</th>
									</tr>
								</thead>
								<tbody>
									<?php $no=1; if($siswa){ foreach($siswa as $s): ?>
										<tr>
											<td width=10px align="center"><?php echo $no;?></td>
											<td width=100px align="center"><?php echo $s['nis'];?></td>
											<td><?php echo $s['nama'];?></td>
											
											<?php $nilai_siswa= isset($nilai[$s['id_siswa']])?$nilai[$s['id_siswa']]:0 ?>
											<?php $nilai_siswa= isset($_POST['nilai_angka'][$s['id_siswa']])?$_POST['nilai_angka'][$s['id_siswa']]:$nilai_siswa ?>
											
											<td width=50px align="center"><input size="4" style="text-align: right;" type="text" name="nilai_angka[<?php echo $s['id_siswa']; ?>]" value="<?php echo $nilai_siswa; ?>" /></td>
										</tr>
									<?php $no++; endforeach; } ?>
								</tbody>
							</table>
							</div>
						</div>
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('evaluasi/detail_evaluasi_kelas/datalist/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
