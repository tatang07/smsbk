<?php if($gid == 9){
		$rombel = array("0"=>"-Pilih-")+get_rombel_guru_matpel_rombel_by_guru(get_id_personal());
	}else{
		$rombel = array("0"=>"-Pilih-")+get_rombel();
	}
		?>
<?php $pelajaran = get_pelajaran_guru_matpel_rombel() ?>
<?php $komponen = get_rombel_komponen_nilai() ?>
<?php $kategori = get_nilai_kategori() ?>
<?php $allkomponen =  array("0"=>"-Pilih-")+get_list("m_komponen_nilai","id_komponen_nilai","komponen_nilai"); ?>

<h1 class="grid_12">Evaluasi Pembelajaran</h1>
<div class="grid_12">
	
	    <div class="box with-table">
       <div class="header">
            <h2>Detail Evaluasi Kelas</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('evaluasi/detail_evaluasi_kelas/tambah/'.$id_gmr.'/'.$id_nilai_kategori); ?>"><i class="icon-plus"></i>Tambah</a> 
			  	<a href="<?php echo base_url('evaluasi/detail_evaluasi_kelas/edit/'.$id_gmr.'/'.$id_nilai_kategori); ?>"><i class="icon-pencil"></i>Edit</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" >
					<table>
						<tr>
							<th width=100px align='left'><span class="text">Kelas</span></th>
							<td width=300px align='left'>
								<?php echo simple_form_dropdown_clear("id_rombel",$rombel,$id_rombel," id='idrombel' onChange=rombelChanged(this.value)",null) ?>
							</td>
						</tr>
						<tr>
							<th width=80px align='left'><span class="text">Mata Pelajaran</span></th>
							<td width=100px align='left'>
								<?php foreach($rombel as $id=>$det):?>
									<?php $mapel =  array("0"=>"-Pilih-") ?>
									<?php $mapel += isset($pelajaran[$id])?$pelajaran[$id]:array() ?>
									<div id='mapel<?php echo $id?>' <?php echo $id_rombel!=$id?"style='display:none'":""?> class='listmapel'>					
										<?php echo simple_form_dropdown_clear("id_gmr".$id,$mapel,$id_gmr,"",null) ?>
									</div>	
								<?php endforeach ?>
							</td>
						</tr>
						<tr>
							<th width=80px align='left'><span class="text">Komponen</span></th>
							<td width=100px align='left'>
								<?php foreach($rombel as $id=>$det):?>
									<?php $komp =  array("0"=>"-Pilih-") ?>
									<?php $komp += isset($komponen[$id])?$komponen[$id]:array() ?>
									<div id='komp<?php echo $id?>' <?php echo $id_rombel!=$id?"style='display:none'":""?> class='listkomp'>					
										<?php echo simple_form_dropdown_clear("id_komponen_nilai".$id,$komp,$id_komponen_nilai," id='idkomponen' onChange=komponenChanged(this.value)",null) ?>
									</div>	
								<?php endforeach ?>
							</td>
						</tr>
						<tr>
							<th width=50px align='left'><span class="text">Penilaian</span></th>
							<td width=100px align='left'>
								<?php
									$param['parent'] = $allkomponen;	
									$param['child'] = $kategori;		
									$param['parent_name'] = "komponen";														
									$param['current_parent'] = $id_komponen_nilai;								
									$param['current_child'] = $id_nilai_kategori;	
									$param['name'] = "id_nilai_kategori";	
								?>
								<?php $this->load->view("html_template/linked_box_child",$param) ?>
							</td>
						</tr>
						<tr>
							<td width=1px>&nbsp;</td>
							<td align='left'>
								<a href="javascript:document.getElementById('formPencarian').submit();" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
	</div>
    <div class="box with-table">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=2>No.</th>
								<th rowspan=2>NIS</th>
								<th rowspan=2>Nama</th>
								<?php if(count($nilai)>0):?>
									<th colspan=<?php echo count($nilai)>1?count($nilai)+1:1 ?>>Nilai</th>
								<?php endif ?>
							</tr>
							<?php if(count($nilai)>0):?>
								<tr>
									<?php foreach($nilai as $n):?>
										<th><?php echo $n['nama_nilai']?></th>
									<?php endforeach ?>
									<?php if(count($nilai)>1):?>
										<th rowspan=2>Rataan</th>
									<?php endif ?>
								</tr>
							<?php endif ?>
						</thead>
						<tbody>
						  <?php $no = 1 ?>
						  <?php foreach($siswa as $sis):?>
							<tr>
								<td width='10px' class='center'><?php echo $no++ ?></td>
								<td width='70px' class='center'><?php echo $sis['nis']?></td>
								<td width=''><?php echo $sis['nama']?></td>
								<?php if(count($nilai)>0):?>
									<?php $sum = 0 ?>
									<?php foreach($nilai as $n):?>
										<?php $nil = isset($nilai_detail[$sis['id_siswa']][$n['id_nilai']]['nilai'])?$nilai_detail[$sis['id_siswa']][$n['id_nilai']]['nilai']:0 ?>
										<?php $sum += $nil ?>
										<td width='50px' class='center'><?php echo $nil?></td>
									<?php endforeach ?>
									<?php if(count($nilai)>1):?>
										<td width='50px' class='center'><?php echo round($sum/count($nilai),2) ?></td>
									<?php endif ?>	
								<?php endif ?>
							</tr>							
						  <?php endforeach?>
						</tbody>
					</table>
					<div class="footer">
						<div class="dataTables_info"></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>

	<script type="text/javascript">		
		function rombelChanged(id){
			$('.listmapel').hide();
			$('.listkomp').hide();
			if(id>0){
				$('#mapel'+id).show();				
				$('#komp'+id).show();				
			}
		}
	</script>