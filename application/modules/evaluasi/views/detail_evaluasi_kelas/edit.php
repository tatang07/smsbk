	<h1 class="grid_12">Evaluasi Pembelajaran</h1>
			
			<form action="<?php echo base_url('evaluasi/detail_evaluasi_kelas/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Edit Detail Evaluasi Kelas</legend>
					<div class="row">
						<table>
						<input type="hidden" name="action" value="edit">
						<input type="hidden" name="kategori" value="<?php echo $tunggal?>">
							<tr>
								<td width=150px >Kelas</td>
								<td width=50px >:</td>
								<td width=300px ><?php echo $rombel; ?></td>
								<td width=150px >Komponen</td>
								<td width=50px >:</td>
								<td width=300px ><?php echo $komponen; ?></td>
							</tr>
							<tr>
								<td width=150px >Mata Pelajaran</td>
								<td width=50px >:</td>
								<td width=300px ><?php echo $pelajaran; ?></td>
								<td width=150px >Penilaian</td>
								<td width=50px >:</td>
								<td width=300px ><?php echo $penilaian; ?></td>
							</tr>
							<tr>
								<td width=150px >Guru</td>
								<td width=50px >:</td>
								<td width=300px ><?php echo $nama; ?></td>
							</tr>
						</table>
					</div>

				</fieldset><!-- End of fieldset -->
				
				<fieldset>
					<legend>Siswa</legend>
				</fieldset><!-- End of fieldset -->
					
						<div id="datatable" url="">
							<div class="dataTables_wrapper"  role="grid">
							<table class="styled" >
								<thead>
									<tr>
										<th>No.</th>
										<th>NIS</th>
										<th>Nama</th>
										<?php if($nilai){ foreach($nilai as $n): ?>
											<th><?php echo $n['nama_nilai'];?></th>
										<?php endforeach; } ?>
									</tr>
								</thead>
								<tbody>
									<?php $no=1; if($siswa){ foreach($siswa as $s): ?>
										<tr>
											<td width=10px align="center"><?php echo $no;?></td>
											<td width=100px align="center"><?php echo $s['nis'];?></td>
											<td><?php echo $s['nama'];?></td>
												<?php foreach($nilai as $n): ?>
													<?php $nilai_siswa= isset($nilai_detail[$s['id_siswa']][$n['id_nilai']])?$nilai_detail[$s['id_siswa']][$n['id_nilai']]['nilai']:0 ?>
													<?php $id= isset($nilai_detail[$s['id_siswa']][$n['id_nilai']])?$nilai_detail[$s['id_siswa']][$n['id_nilai']]['id_nilai_detail']:0 ?>
														<td align="center" width=50px><input type="text" size="4" style="text-align: right;" name="nilai_angka[<?php echo $s['id_siswa']; ?>][<?php echo $n['id_nilai']; ?>]" value="<?php echo $nilai_siswa;?>" /></td>
														<input type="hidden" name="id[<?php echo $s['id_siswa']; ?>][<?php echo $n['id_nilai']; ?>]" value="<?php echo $id; ?>">
												<?php endforeach; ?>
											
										</tr>
									<?php $no++; endforeach; } ?>
								</tbody>
							</table>
							</div>
						</div>
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('evaluasi/detail_evaluasi_kelas/datalist/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
