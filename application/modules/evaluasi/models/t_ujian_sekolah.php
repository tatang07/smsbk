<?php
class t_ujian_sekolah extends MY_Model {
    public $table = "t_ujian_sekolah";
    public $id = "t_ujian_sekolah";
	
	function get_siswa($id_gmr,$jenis,$id_rombel=0){
		$data['data'] = $this->db->query("SELECT s.id_siswa,nama,nis FROM t_rombel_detail as rd join v_siswa as s on rd.id_siswa=s.id_siswa WHERE rd.id_rombel=$id_rombel
							
		")->result_array();		
		// $res = array();
		
		// foreach($data as $det){
			// $res[$det['id_siswa']][$det['id_nilai_akhir']] = $det;
		// }
		
		return $data['data'];

	}
	
	function select_nilai_akhir(){
		$data['data'] = $this->db->query("SELECT * FROM t_nilai_akhir 
		")->result_array();		
		//order by id_nilai_akhir desc limit 1
		return $data['data'];
	}
	
	public function cek_rombel($id){
		$this->db->select('*');
		$this->db->from('t_rombel_detail rd');
		$this->db->join('m_rombel r', 'r.id_rombel=rd.id_rombel', 'left');
		$this->db->where('rd.id_rombel',$id);

		$query = $this->db->get();
		
		return $query->row();
	}
	
	public function cek_pelajaran($id){
		$this->db->select('*');
		$this->db->from('m_pelajaran');
		$this->db->where('id_pelajaran',$id);

		$query = $this->db->get();
		
		return $query->row();
	}
	
	function get_nilai($id_gmr,$jenis,$id_rombel,$tahun_ajaran){
		$data['data'] = $this->db->query("SELECT na.id_nilai_akhir,nad.id_nilai_akhir_detail, s.id_siswa,nilai FROM t_nilai_akhir_detail as nad join t_nilai_akhir as na on nad.id_nilai_akhir=na.id_nilai_akhir join v_siswa as s on nad.id_siswa=s.id_siswa WHERE na.id_pelajaran=$id_gmr AND na.id_tahun_ajaran=$tahun_ajaran AND na.jenis=$jenis AND s.id_rombel=$id_rombel")->result_array();		
		// $res = array();
		
		// foreach($data as $det){
			// $res[$det['id_siswa']][$det['id_nilai_akhir']] = $det;
		// }
		
		return $data['data'];

	}
}