<?php
class v_siswa extends MY_Model {
	
	public function get_data_nilai_unas($id_tahun_ajaran=0){
		$id=get_id_sekolah();
		$this->db->select('s.id_siswa, s.nis, s.nama, s.rombel, sum(nad.nilai) as jml, avg(nad.nilai) as rata');
		$this->db->from('v_siswa s');
		$this->db->join('t_nilai_akhir_detail nad', 'nad.id_siswa = s.id_siswa');
		$this->db->join('t_nilai_akhir na', 'na.id_nilai_akhir = nad.id_nilai_akhir');
		$this->db->where('s.id_sekolah',$id);
		$this->db->where('na.jenis',2);
		$this->db->where('s.id_tahun_ajaran',$id_tahun_ajaran);
		$this->db->group_by('s.nis, s.nama, s.rombel');
		$this->db->order_by('rata', 'desc');
		$query=$this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_data_hasil_un($id_tahun_ajaran=0){
		$id=get_id_sekolah();
		$this->db->select('s.id_siswa, s.nis, s.nama, s.rombel, sum(nad.nilai) as jml, avg(nad.nilai) as rata');
		$this->db->from('v_siswa s');
		$this->db->join('t_nilai_akhir_detail nad', 'nad.id_siswa = s.id_siswa');
		$this->db->join('t_nilai_akhir na', 'na.id_nilai_akhir = nad.id_nilai_akhir');
		$this->db->where('s.id_sekolah',$id);
		$this->db->where('s.id_tahun_ajaran',$id_tahun_ajaran);
		$this->db->group_by('s.nis, s.nama, s.rombel');
		$this->db->order_by('rata', 'desc');
		$query=$this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_tahun_ajaran(){
		$this->db->select('*'); 
		$this->db->from('m_tahun_ajaran');
		$query=$this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_detail_nilai($id=0){
		$this->db->select('p.pelajaran, p.kode_pelajaran, nad.nilai');
		$this->db->from('t_nilai_akhir_detail nad');
		$this->db->join('t_nilai_akhir na', 'na.id_nilai_akhir = nad.id_nilai_akhir');
		$this->db->join('m_pelajaran p', 'p.id_pelajaran = na.id_pelajaran');
		$this->db->where('nad.id_siswa',$id);
		$this->db->order_by('p.pelajaran', 'asc');
		$query=$this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_detail_siswa($id=0){
		$this->db->select('*');
		$this->db->from('v_siswa');
		$this->db->where('id_siswa',$id);
		$query=$this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	public function get_data_lulusan($ta=array()){
		$i=0;
		$id_sekolah = get_id_sekolah();
		foreach($ta as $temp){
			$this->db->select('count(s.id_siswa) as jml_lulus');
			$this->db->from('v_siswa s');
			$this->db->where('s.status_aktif',10);
			$this->db->where('s.id_sekolah',$id_sekolah);
			$this->db->where('s.id_tahun_ajaran',$temp['id_tahun_ajaran']);
			$query=$this->db->get();
			$data[$i]= $query->row_array();
			$data[$i]['tahun_akhir']= $temp['tahun_akhir'];
			$i++;
		}
		
		if($query->num_rows() != 0){
			return $data;
		}else{
			return false;
		}
	}
	
	public function get_data_tidak_lulus($ta=array()){
		$i=0;
		$id_sekolah = get_id_sekolah();
		$max = get_max_tingkat_kelas();
		foreach($ta as $temp){
			$this->db->select('count(id_siswa) as jml_tidak_lulus');
			$this->db->from('v_siswa');
			$this->db->where('status_aktif',1);
			$this->db->where('id_tingkat_kelas',$max);
			$this->db->where('id_sekolah',$id_sekolah);
			$this->db->where('id_tahun_ajaran',$temp['id_tahun_ajaran']);
			$query=$this->db->get();
			$data[$i]= $query->row_array();
			$i++;
		}
		
		if($query->num_rows() != 0){
			return $data;
		}else{
			return false;
		}
	}
	
	public function get_rata_rata(){
		$id_sekolah = get_id_sekolah();
		$this->db->select('na.id_tahun_ajaran, avg(nad.nilai) as nilai');
		$this->db->from('t_nilai_akhir_detail nad');
		$this->db->join('v_siswa s', 'nad.id_siswa = s.id_siswa');
		$this->db->join('t_nilai_akhir na', 'nad.id_nilai_akhir = na.id_nilai_akhir');
		$this->db->where('s.status_aktif',1);
		$this->db->or_where('s.status_aktif',10);
		$this->db->where('s.id_sekolah',$id_sekolah);
		$this->db->where('na.jenis',2);
		$this->db->group_by('na.id_tahun_ajaran');
		$query=$this->db->get();
		
		// echo $this->db->last_query();
		// exit;
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
}