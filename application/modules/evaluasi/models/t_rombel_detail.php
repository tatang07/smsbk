<?php
class t_rombel_detail extends MY_Model {
    public $table = "t_rombel_detail";
    public $id = "id_rombel_detail";
	
	function get_siswa_rombel($id_rombel=0){
		$data = $this->db->query("SELECT * 
							FROM t_rombel_detail rd,
							m_siswa s
							WHERE rd.id_siswa=s.id_siswa AND id_rombel=$id_rombel
						")->result_array();		
		return $data;

	}
	
	function get_siswa_rombel_by($id_rombel=0){
		$data = $this->db->query("SELECT * 
							FROM t_rombel_detail rd,
							m_siswa s, m_rombel r
							WHERE rd.id_siswa=s.id_siswa AND rd.id_rombel=r.id_rombel AND rd.id_rombel=$id_rombel
						")->result_array();		
		return $data;

	}
}