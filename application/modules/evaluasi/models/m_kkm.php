<?php
class m_kkm extends MY_Model {
    public $table = "t_kkm";
    public $id = "id_kkm";
	
	// join ON 
	public $join_to = array(
			"m_pelajaran p" => "p.id_pelajaran = t_kkm.id_pelajaran",
			"m_kelompok_pelajaran kp" => "kp.id_kelompok_pelajaran = p.id_kelompok_pelajaran"
		);

	 public $join_fields = array("pelajaran, kode_pelajaran");

	function get_kkm($id)
	{
		$this->db->from($this->table)
				->join("m_pelajaran p", "p.id_pelajaran = t_kkm.id_pelajaran")
				->join("m_tahun_ajaran ta", "ta.id_tahun_ajaran = t_kkm.id_tahun_ajaran")
				->join("m_kelompok_pelajaran kp", "kp.id_kelompok_pelajaran = p.id_kelompok_pelajaran")
				->join("m_kurikulum k", "k.id_kurikulum = kp.id_kurikulum")
				->join("m_tingkat_kelas tk", "tk.id_tingkat_kelas = p.id_tingkat_kelas")
				->where('id_kkm', $id);

		$res = $this->db->get();
		if($res->num_rows() > 0)
			return $res->row_array();

		return false;
	}

	function get_kkm_by_pelajaran($id_pelajaran)
	{
		$this->db->from($this->table)
				// ->join("m_pelajaran p", "p.id_pelajaran = t_kkm.id_pelajaran")
				// ->join("m_tahun_ajaran ta", "ta.id_tahun_ajaran = t_kkm.id_tahun_ajaran")
				// ->join("m_kelompok_pelajaran kp", "kp.id_kelompok_pelajaran = p.id_kelompok_pelajaran")
				// ->join("m_kurikulum k", "k.id_kurikulum = kp.id_kurikulum")
				// ->join("m_tingkat_kelas tk", "tk.id_tingkat_kelas = p.id_tingkat_kelas")
				->where('t_kkm.id_pelajaran', $id_pelajaran)
				->where('t_kkm.id_sekolah', get_id_sekolah())
				->where('t_kkm.id_tahun_ajaran', get_id_tahun_ajaran());

		$res = $this->db->get();
		if($res->num_rows() > 0)
			return $res->row_array();

		return false;
	}
	
	function get_pelajaran($id_kurikulum, $id_tingkat_kelas)
	{
		$kelompok = $this->db->from('m_kelompok_pelajaran')
				 			 ->where('id_kurikulum', $id_kurikulum)
							 ->get()->result_array();
		$id_kelompoks = array();
		if($kelompok)
			foreach ($kelompok as $kel) {
				$id_kelompoks[] = $kel['id_kelompok_pelajaran'];
			}

		$pelajaran = $this->db->from('m_pelajaran')
								->where_in('id_kelompok_pelajaran', $id_kelompoks)
								->where('id_tingkat_kelas', $id_tingkat_kelas)
								->get()->result_array();

		if($pelajaran)
			return $pelajaran;

		return false;
	}

	function insert($data)
	{
		$this->db->insert($this->table, $data);
		return $this->db->insert_id();
	}
	
	function update($data, $id)
	{
		$this->db->where('id_kkm', $id)
				->update($this->table, $data);

		return $this->db->affected_rows();
	}
}
