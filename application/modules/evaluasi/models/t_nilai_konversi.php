<?php
class t_nilai_konversi extends MY_Model {
	public $table = "t_nilai_konversi";
    public $id = "id_nilai_konversi";
	
	public function get_nilai_konversi_by($id=0){
		$id_sekolah=get_id_sekolah();
		$id_tahun_ajaran=get_id_tahun_ajaran();
		$this->db->select('*');
		$this->db->from('t_nilai_konversi n');
		$this->db->join('m_komponen_nilai k', 'n.id_komponen_nilai=k.id_komponen_nilai');
		$this->db->where('n.id_komponen_nilai',$id);
		$this->db->where('n.id_sekolah',$id_sekolah);
		$this->db->where('n.id_tahun_ajaran',$id_tahun_ajaran);
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
		
	}
	
	public function get_all_kurikulum(){
		$this->db->select('*');
		$this->db->from('m_kurikulum');
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_komponen(){
		$this->db->select('*');
		$this->db->from('m_komponen_nilai');
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_nilai_by($id=0){
		$this->db->select('*');
		$this->db->from('t_nilai_konversi n');
		$this->db->join('m_komponen_nilai k', 'n.id_komponen_nilai=k.id_komponen_nilai');
		$this->db->where('n.id_nilai_konversi',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	function edit($data, $id){
		$this->db->where('id_nilai_konversi', $id);
		$this->db->update('t_nilai_konversi', $data);
	}
	
	function add($data){
		$this->db->insert('t_nilai_konversi', $data);
	}
	
}
