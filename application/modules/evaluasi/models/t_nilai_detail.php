<?php
class t_nilai_detail extends MY_Model {
    public $table = "t_nilai_detail";
    public $id = "id_nilai_detail";
	
	function get_nilai($id_gmr,$id_nilai_kategori, $id_term){
		$data = $this->db->query("SELECT * 
							FROM t_nilai_detail nd,
							t_nilai n
							WHERE nd.id_nilai=n.id_nilai
							AND n.id_guru_matpel_rombel=$id_gmr
							AND n.id_nilai_kategori=$id_nilai_kategori
							AND n.id_term = $id_term
						")->result_array();		
		$res = array();
		
		foreach($data as $det){
			$res[$det['id_siswa']][$det['id_nilai']] = $det;
		}
		
		return $res;

	}
	
	function save_us($data,$id){
		$this->db->where('id_nilai_akhir_detail',$id);
		$this->db->update('t_nilai_akhir_detail',$data);
	}
	function insert_pelajaran($data){
		$this->db->insert('t_nilai_akhir',$data);
	}
	function save_add($data){
			// $this->db->where('id_nilai_akhir_detail',$id);
			$this->db->insert('t_nilai_akhir_detail',$data);
	}
}