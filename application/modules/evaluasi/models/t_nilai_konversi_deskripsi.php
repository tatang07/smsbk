<?php
class t_nilai_konversi_deskripsi extends MY_Model {
	public $table = "t_nilai_konversi_deskripsi";
    public $id = "id_nilai_konversi_deskripsi";
	
	public function get_datalist($tingkat=0, $id_pelajaran=0, $id_kurikulum=0){
		$id_tahun_ajaran=get_id_tahun_ajaran();
		$this->db->select('*');
		$this->db->from('t_nilai_konversi_deskripsi nkd');
		$this->db->join('m_pelajaran p', 'p.id_pelajaran = nkd.id_pelajaran');
		$this->db->join('t_guru_matpel_rombel gm', 'gm.id_pelajaran = nkd.id_pelajaran');
		$this->db->join('m_kelompok_pelajaran kp', 'kp.id_kelompok_pelajaran = p.id_kelompok_pelajaran');
		$this->db->join('t_nilai_konversi nk', 'nk.id_nilai_konversi = nkd.id_nilai_konversi');
		$this->db->where('p.id_tingkat_kelas',$tingkat);
		$this->db->where('nkd.id_pelajaran',$id_pelajaran);
		$this->db->where('kp.id_kurikulum',$id_kurikulum);
		$this->db->where('nk.id_sekolah',get_id_sekolah());
		
		$query = $this->db->get();
		
		//echo $this->db->last_query();
		//exit;
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
		
	}
	
	public function get_komponen(){
		$this->db->select('*');
		$this->db->from('m_komponen_nilai');
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_all_kurikulum(){
		$this->db->select('*');
		$this->db->from('m_kurikulum');
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_all_tingkat_kelas(){
		$id_jenjang_sekolah = get_jenjang_sekolah();
		$this->db->select('*');
		$this->db->from('m_tingkat_kelas');
		$this->db->where('id_jenjang_sekolah', $id_jenjang_sekolah);
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_pelajaran($id_kurikulum=0, $id_tingkat_kelas=0){
		$this->db->select('*');
		$this->db->from('m_pelajaran p');
		$this->db->join('m_kelompok_pelajaran kp', 'kp.id_kelompok_pelajaran = p.id_kelompok_pelajaran');
		$this->db->where('kp.id_kurikulum', $id_kurikulum);
		$this->db->where('p.id_tingkat_kelas', $id_tingkat_kelas);
		
		
		$query = $this->db->get();
		
		//echo $this->db->last_query();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_detail($id=0){
		$this->db->select('*');
		$this->db->from('t_nilai_konversi_deskripsi nkd');
		$this->db->join('t_nilai_konversi nk', 'nkd.id_nilai_konversi = nk.id_nilai_konversi');
		$this->db->where('nkd.id_pelajaran', $id);
		$this->db->where('nk.id_sekolah', get_id_sekolah());
		
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_kurikulum($id=0){
		$this->db->select('kp.id_kurikulum');
		$this->db->from('m_pelajaran p');
		$this->db->join('m_kelompok_pelajaran kp', 'p.id_kelompok_pelajaran = kp.id_kelompok_pelajaran');
		$this->db->where('p.id_pelajaran', $id);
		
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	public function get_nilai_huruf($id=0){
		$this->db->select('*');
		$this->db->from('t_nilai_konversi n');
		$this->db->join('m_komponen_nilai k', 'n.id_komponen_nilai=k.id_komponen_nilai');
		$this->db->where('k.id_kurikulum', $id);
		$this->db->where('n.id_sekolah', get_id_sekolah());
		
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_deskripsi($id_pelajaran=0, $id_nilai_konversi=0){
		$this->db->select('*');
		$this->db->from('t_nilai_konversi_deskripsi');
		$this->db->where('id_pelajaran', $id_pelajaran);
		$this->db->where('id_nilai_konversi', $id_nilai_konversi);
		
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	function edit($data, $id){
		$this->db->where('id_nilai_konversi_deskripsi', $id);
		$this->db->update('t_nilai_konversi_deskripsi', $data);
	}
	
	function add($data){
		$this->db->insert('t_nilai_konversi_deskripsi', $data);
	}
	
	function del($id_pelajaran){
		$this->db->where('id_pelajaran', $id_pelajaran);
		$this->db->delete('t_nilai_konversi_deskripsi');
	}
	
}
