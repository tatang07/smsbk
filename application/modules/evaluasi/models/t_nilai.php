<?php
class t_nilai extends MY_Model {
    public $table = "t_nilai";
    public $id = "id_nilai";
	
	public function get_all_term(){
		$this->db->from('m_term'); 
		$query = $this->db->get();
				
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_all_tahun_ajaran(){
		$this->db->from('m_tahun_ajaran'); 
		$query = $this->db->get();
				
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_all_tingkat(){
		$this->db->join('r_jenjang_sekolah j', 'j.id_jenjang_sekolah=k.id_jenjang_sekolah');
		$this->db->where('j.id_jenjang_sekolah', get_jenjang_sekolah());
		$query = $this->db->get('m_tingkat_kelas k');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_all_gmr_groupby_pelajaran(){
		$this->db->join('m_pelajaran p', 'p.id_pelajaran=gm.id_pelajaran');
		$this->db->join('m_rombel r', 'r.id_rombel=gm.id_rombel');
		$this->db->where('r.id_sekolah', get_id_sekolah());
		$this->db->group_by('gm.id_pelajaran');
		$query = $this->db->get('t_guru_matpel_rombel gm');
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_nilai($id_tahun_ajaran, $id_term, $id_tingkat, $id_gmr){
		$query = $this->db
						->select('nd.id_siswa, nk.id_nilai_kategori, nk.id_komponen_nilai, t.id_guru_matpel_rombel, sum(nd.nilai) as jumlah, count(nd.id_nilai) as banyak_nilai, (sum(nd.nilai)/count(nd.id_nilai))*(nk.bobot/100) as nilai_akhir, nk.bobot')
						->from('t_nilai_kategori nk')
						->join('t_nilai t', 't.id_nilai_kategori=nk.id_nilai_kategori')
						->join('t_nilai_detail nd', 'nd.id_nilai=t.id_nilai')
						->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=t.id_guru_matpel_rombel')
						->join('m_rombel r', 'r.id_rombel=gm.id_rombel')
						->group_by('nk.id_nilai_kategori')
						->group_by('nd.id_siswa')
						->order_by('nd.id_siswa')
						->where('nk.id_tahun_ajaran', $id_tahun_ajaran)
						->where('nk.id_sekolah', get_id_sekolah())
						->where('nk.id_komponen_nilai', 1)
						->where('t.id_term', $id_term)
						->where('r.id_tingkat_kelas', $id_tingkat)
						->where('gm.id_guru_matpel_rombel', $id_gmr)
						->get();
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
}