<?php
class t_nilai_kategori extends MY_Model {

	public $table = "t_nilai_kategori";
	public $id = "id_nilai_kategori";
	
	public function get_nilai_kategori($id_gmr=0, $id_komponen_nilai=0, $id_term=0){
		$q = "SELECT nd.id_siswa, nk.id_nilai_kategori, nk.bobot, avg(nilai) as nilai
		FROM t_nilai_kategori nk,
		t_nilai n,
		t_nilai_detail nd
		WHERE nk.id_nilai_kategori = n.id_nilai_kategori
		AND nd.id_nilai = n.id_nilai
		AND n.id_term=$id_term
		AND n.id_guru_matpel_rombel=$id_gmr
		AND nk.id_komponen_nilai=$id_komponen_nilai
		GROUP BY nd.id_siswa, nk.id_nilai_kategori";
		$data = $this->db->query($q)->result_array();	
		$res = array();
		
		foreach($data as $det){
			$res[$det['id_siswa']][$det['id_nilai_kategori']] = $det;
		}
		
		return $res;
	}

	function get_komponen_nilai($id_kurikulum)
	{
		$res = $this->db->from('m_komponen_nilai')
		->where('id_kurikulum', $id_kurikulum)
		->get();

		if($res->num_rows() > 0)
			return $res->result_array();

		return false;
	}

	public function get_kurikulum(){
		$query = $this->db->get('m_kurikulum');

		if($query->num_rows() > 0)
			return $query->result_array();

		return false;
	}

	function get($id)
	{
		$this->db->from($this->table.' t')
				->join('m_komponen_nilai k', 't.id_komponen_nilai = k.id_komponen_nilai')
				->join('m_kurikulum m', 'k.id_kurikulum = m.id_kurikulum')
				->where('id_nilai_kategori', $id)
				->where('id_tahun_ajaran', get_id_tahun_ajaran())
				->where('id_sekolah', get_id_sekolah());

		$res = $this->db->get();
		if($res->num_rows() > 0)
			return $res->row_array();

		return false;
	}

	function update_data($data, $id)
	{
		$this->db->where('id_nilai_kategori', $id)->update($this->table, $data);
		return $this->db->affected_rows();
	}

	function delete($id)
	{
		$this->db->where('id_nilai_kategori', $id)->delete($this->table);
		return $this->db->affected_rows();
	}
}