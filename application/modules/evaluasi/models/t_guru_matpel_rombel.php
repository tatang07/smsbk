<?php
class t_guru_matpel_rombel extends MY_Model {
    public $table = "t_guru_matpel_rombel";
    public $id = "id_guru_matpel_rombel";
	
	public function get_gmr($id){
		$this->db->where('g.id_guru_matpel_rombel',$id);
		$this->db->join('m_guru p', 'p.id_guru=g.id_guru', 'left');
		$this->db->join('m_rombel r', 'r.id_rombel=g.id_rombel', 'left');
		$this->db->join('m_pelajaran j', 'j.id_pelajaran=g.id_pelajaran', 'left');
		$query = $this->db->get('t_guru_matpel_rombel g');
		
		return $query->row();
	}
	

	
	public function get_penilaian($id){
		$this->db->where('n.id_nilai_kategori',$id);
		$this->db->join('m_komponen_nilai k', 'k.id_komponen_nilai=n.id_komponen_nilai', 'left');
		$query = $this->db->get('t_nilai_kategori n');
		
		return $query->row();
	}
	
	public function get_nilai($id, $id_gmr, $id_term){
		$this->db->from('t_nilai_detail nd'); 
		
		$this->db->join('t_nilai n', 'n.id_nilai=nd.id_nilai', 'left');
		
		$this->db->where('n.id_nilai_kategori',$id);      
		$this->db->where('n.id_guru_matpel_rombel',$id_gmr);      
		$this->db->where('n.id_term',$id_term);      
		$query = $this->db->get();
				
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_siswa($id_gmr){

		$this->db->from('m_siswa s'); 
		
		$this->db->join('t_siswa_absen sa', 'sa.id_siswa=s.id_siswa', 'left');
		$this->db->join('t_agenda_kelas ak', 'sa.id_agenda_kelas=ak.id_agenda_kelas', 'left');
		$this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=ak.id_guru_matpel_rombel', 'left');
		
		$this->db->where('gm.id_guru_matpel_rombel',$id_gmr);
		$this->db->group_by('s.nis');         
		$query = $this->db->get();
				
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
		function get_siswa2($id_gmr){
		$data['data'] = $this->db->query("SELECT nama,nilai,nis,s.id_siswa,n.id_nilai_akhir,nd.id_nilai_akhir_detail
							FROM t_nilai_akhir_detail as nd join t_nilai_akhir as n on nd.id_nilai_akhir=n.id_nilai_akhir join m_siswa as s on nd.id_siswa=s.id_siswa WHERE n.id_pelajaran=$id_gmr
							
							
							
						")->result();		
		// $res = array();
		
		// foreach($data as $det){
			// $res[$det['id_siswa']][$det['id_nilai_akhir']] = $det;
		// }
		
		return $data['data'];

	}
	
	
}