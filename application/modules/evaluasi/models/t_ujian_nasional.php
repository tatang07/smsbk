<?php
class t_ujian_nasional extends MY_Model {
    public $table = "v_pelajaran_nilai_akhir";
    public $id = "id_pelajaran";
	
	function get_siswa($id_gmr,$jenis,$id_rombel=0){
		$data['data'] = $this->db->query("SELECT s.id_siswa,nama,nis FROM t_rombel_detail as rd join v_siswa as s on rd.id_siswa=s.id_siswa WHERE rd.id_rombel=$id_rombel
							
		")->result_array();		
		// $res = array();
		
		// foreach($data as $det){
			// $res[$det['id_siswa']][$det['id_nilai_akhir']] = $det;
		// }
		
		return $data['data'];

	}
	
	function select_nilai_akhir($id){
		$data['data'] = $this->db->query("SELECT * FROM t_nilai_akhir where jenis = $id
		")->result_array();		

		return $data['data'];

	}
	
	public function cek_rombel($id){
		$this->db->select('*');
		$this->db->from('t_rombel_detail rd');
		$this->db->join('m_rombel r', 'r.id_rombel=rd.id_rombel', 'left');
		$this->db->where('rd.id_rombel',$id);

		$query = $this->db->get();
		
		return $query->row();
	}
	
	public function cek_pelajaran($id){
		$this->db->select('*');
		$this->db->from('m_pelajaran');
		$this->db->where('id_pelajaran',$id);

		$query = $this->db->get();
		
		return $query->row();
	}
	
	function get_nilai($id_gmr,$jenis,$id_rombel,$tahun_ajaran){
		$data['data'] = $this->db->query("SELECT na.id_nilai_akhir,nad.id_nilai_akhir_detail, s.id_siswa,nilai FROM t_nilai_akhir_detail as nad join t_nilai_akhir as na on nad.id_nilai_akhir=na.id_nilai_akhir join v_siswa as s on nad.id_siswa=s.id_siswa WHERE na.id_pelajaran=$id_gmr AND na.id_tahun_ajaran=$tahun_ajaran AND na.jenis=$jenis AND s.id_rombel=$id_rombel")->result_array();		
		// $res = array();
		
		// foreach($data as $det){
			// $res[$det['id_siswa']][$det['id_nilai_akhir']] = $det;
		// }
		
		return $data['data'];

	}
	
	public function get_data_hasil_un($id_tahun_ajaran, $id_rombel){
		$id=get_id_sekolah();
		$this->db->select('s.id_siswa, s.nis, s.nama, s.rombel, na.id_pelajaran, nad.nilai');
		$this->db->from('v_siswa s');
		$this->db->join('t_nilai_akhir_detail nad', 'nad.id_siswa = s.id_siswa');
		$this->db->join('t_nilai_akhir na', 'na.id_nilai_akhir = nad.id_nilai_akhir');
		$this->db->where('s.id_sekolah',$id);
		$this->db->where('s.id_tahun_ajaran',$id_tahun_ajaran);
		$this->db->where('s.id_rombel',$id_rombel);
		$this->db->where('na.jenis',2);
		// $this->db->group_by('s.nis, s.nama, s.rombel');
		// $this->db->order_by('rata', 'desc');
		$query=$this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
}