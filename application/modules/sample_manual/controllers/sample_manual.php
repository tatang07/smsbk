<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sample_manual extends CI_Controller {

	public function index()
	{
		render('welcome_message');
	}
	
	public function form_sample(){
		render('form_sample');
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */