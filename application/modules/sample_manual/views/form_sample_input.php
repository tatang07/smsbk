		<?php $list_agama = array("1"=>"Islam", "2"=>"Kristen") ?>
		<?php $list_sp = array("Kawin"=>"Kawin", "Tidak Kawin"=>"Tidak Kawin") ?>
		<?php $list_pekerjaan = array(1=>"Petani",2=>"Dosen", 0=>"Lainnya") ?>
		
        <div class='row'>
            <?php echo simple_form_input2(array('disabled'=>'disabled','name'=>'nomor_induk', 'id'=>'nomor_induk', 'label'=>'Nomor Induk', 'class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'gelar', 'id'=>'gelar', 'label'=>'Gelar','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'nama_lengkap', 'id'=>'nama_lengkap', 'label'=>'Nama Lengkap', 'class'=>'required','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'nama_panggilan', 'id'=>'nama_panggilan', 'label'=>'Nama Panggilan','class_div'=>'_25')) ?>
        </div>    
        <div class='row'>
            <?php echo simple_form_input2(array('name'=>'nik', 'id'=>'nik', 'label'=>'NIK', 'class'=>'required','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'tgl_akhir_ktp','class'=>'required', 'type'=>'date', 'id'=>'tgl_akhir_ktp', 'label'=>'Tanggal Akhir KTP', 'class'=>'required','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'no_akta_lahir', 'id'=>'no_akta_lahir', 'label'=>'Nomor Akta Lahir','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'tahun_akta_lahir', 'id'=>'tahun_akta_lahir', 'label'=>'Tahun Akta Lahir','class_div'=>'_25')) ?>
        </div>    
        <div class='row'>
            <?php echo simple_form_dropdown2('jenis_kelamin',array("L"=>"Laki-laki", "P"=>"Perempuan"),null,"",array('label'=>'Jenis Kelamin','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'kewarganegaraan', 'id'=>'kewarganegaraan', 'label'=>'Kewarganegaraan', 'class'=>'required','class_div'=>'_50')) ?>
            <?php echo simple_form_dropdown2('golongan_darah',array("A"=>"A","B"=>"B"),null,"",array('label'=>'Golongan Darah','class_div'=>'_25')) ?>
        </div>
        <div class='row'>
            <?php echo simple_form_input2(array('name'=>'tempat_lahir', 'id'=>'tempat_lahir', 'label'=>'Tempat Lahir', 'class'=>'required','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'tgl_lahir', 'id'=>'tgl_lahir', 'label'=>'Tanggal Lahir', 'class'=>'required','class_div'=>'_25', 'type'=>'date')) ?>
            <?php echo simple_form_input2(array('name'=>'anak_ke','class_div'=>'_25', 'id'=>'anak_ke', 'label'=>'Anak Ke', 'class'=>'required')) ?>
            <?php echo simple_form_input2(array('name'=>'jml_saudara_kandung','class_div'=>'_25', 'id'=>'jml_saudara_kandung', 'label'=>'Jumlah Saudara Kandung', 'class'=>'required')) ?>
        </div>
        <div class='row'>
            <?php echo simple_form_dropdown2('id_agama',$list_agama,null,"",array('label'=>'Agama','class_div'=>'_50')) ?>
            <?php echo simple_form_dropdown2('id_status_perkawinan',$list_sp,null,"",array('label'=>'Status Perkawinan','class_div'=>'_50')) ?>
        </div>   
        <div class='row'>
            <?php echo simple_form_input2(array('class_div'=>'_50','name'=>'alamat_tinggal', 'id'=>'alamat_tinggal', 'class'=>'novalidate')) ?>
            <?php echo simple_form_input2(array('class_div'=>'_25','name'=>'kota', 'id'=>'kota', 'class'=>'novalidate')) ?>
            <?php echo simple_form_input2(array('class_div'=>'_25','name'=>'kode_pos', 'id'=>'kode_pos','class'=>'number')) ?>
        </div>    
        <div class='row'>
            <?php echo simple_form_input2(array('class_div'=>'_25','name'=>'hp', 'id'=>'hp', 'label'=>'HP', 'class'=>'required')) ?>
            <?php echo simple_form_input2(array('class_div'=>'_25','name'=>'telp', 'id'=>'telp', 'label'=>'Telepon', 'class'=>'novalidate')) ?>
            <?php echo simple_form_input2(array('class_div'=>'_50','name'=>'email', 'id'=>'email', 'label'=>'Email', 'class'=>'novalidate')) ?>
        </div> 
        <div class='row'>
            <?php echo simple_form_dropdown2('pekerjaan',$list_pekerjaan,null,"onChange=javascript:showElement('pekerjaan_lainnya',this.value)",array('label'=>'Pekerjaan','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('class_div'=>'_25 hilangkan', 'style'=>'display:none','name'=>'pekerjaan_lainnya', 'id'=>'pekerjaan_lainnya', 'label'=>'&nbsp;', 'class'=>'novalidate')) ?>
            <?php echo simple_form_input2(array('class_div'=>'_25','name'=>'jabatan_kerja', 'id'=>'jabatan_kerja', 'label'=>'Jabatan di Tempat Kerja Lain', 'class'=>'novalidate')) ?>
            <?php echo simple_form_input2(array('class_div'=>'_25','name'=>'bidang_usaha', 'id'=>'bidang_usaha', 'label'=>'Bidang Usaha Tempat Kerja', 'class'=>'novalidate')) ?>
    
        </div>
        <div class='row'>
			<?php $lbl_kerja = isset($_POST['pekerjaan'])&& $_POST['pekerjaan']=='Pelajar/Mahasiswa'?'Sekolah':'Tempat Kerja'?>
            <?php echo simple_form_input2(array('class_div'=>'_25','name'=>'nama_tempat_kerja', 'id'=>'nama_tempat_kerja', 'label'=>"Nama <span id=tpt_kerja>$lbl_kerja</span>", 'class'=>'novalidate')) ?>
            <?php echo simple_form_input2(array('class_div'=>'_25','name'=>'telp_tempat_kerja', 'id'=>'telp_tempat_kerja', 'label'=>"Telepon <span id=telp_kerja>$lbl_kerja</span>", 'class'=>'novalidate')) ?>
            <?php echo simple_form_input2(array('class_div'=>'_50','name'=>'alamat_kerja', 'id'=>'alamat_kerja', 'class'=>'novalidate', 'label'=>"Alamat <span id=alamat_kerja>$lbl_kerja</span>")) ?>
        </div>   
        <div class='row'>
            <?php echo simple_form_input2(array('class_div'=>'_25', 'pattern'=>"^\d{1,10}$", 'name'=>'pendapatan', 'id'=>'pendapatan', 'label'=>'Pendapatan (Rp)', 'class'=>'novalidate')) ?>
            <?php echo simple_form_dropdown2('sumber_pendapatan',array("1"=>"Gaji", 2=>"Lainnya"),null,"",array('label'=>'Sumber Pendapatan','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('class_div'=>'_50','name'=>'npwp', 'id'=>'npwb', 'label'=>'NPWP', 'class'=>'novalidate')) ?>
        </div>
        <div class='row'>
            <?php echo simple_form_input2(array('class_div'=>'_25','name'=>'nama_ayah', 'id'=>'nama_ayah', 'label'=>'Nama Ayah', 'class'=>'required')) ?>
            <?php echo simple_form_dropdown2('pekerjaan_ayah',$list_pekerjaan,null,"",array('label'=>'Pekerjaan Ayah','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('class_div'=>'_25','name'=>'nama_ibu', 'id'=>'nama_ibu', 'label'=>'Nama Ibu', 'class'=>'required')) ?>
            <?php echo simple_form_dropdown2('pekerjaan_ibu',$list_pekerjaan,null,"",array('label'=>'Pekerjaan Ibu','class_div'=>'_25')) ?>
        </div>   
        <?php echo simple_form_input(array('name'=>'alamat_orang_tua', 'id'=>'alamat_orang_tua', 'class'=>'required','class'=>'novalidate')) ?>           
        <?php echo simple_form_input(array('name'=>'password', 'id'=>'password', "type"=>"password", 'class'=>'required')) ?>           
        <?php echo simple_form_textarea(array('name'=>'textarea', 'id'=>'textarea',  'class'=>'required')) ?>           
        <?php echo simple_form_wysiwyg(array('name'=>'wysiwyg', 'id'=>'wysiwyg',  'class'=>'required')) ?> 
		<?php echo simple_form_dropdown('pekerjaan_ayah',$list_pekerjaan,null,"",array('label'=>'Pekerjaan Ayah')) ?>
		<?php echo simple_form_tags_select('pekerjaan_ayah[]',$list_pekerjaan,null,"",array('label'=>'Pekerjaan Ayah')) ?>
		<?php echo simple_form_dropdown_dual('pekerjaan_ayah[]',$list_pekerjaan,null,"",array('label'=>'Pekerjaan Ayah')) ?>
  
        <div class="hilangkan">
        <?php echo form_input(array('name'=>'foto','type'=>'file', 'id'=>'foto')) ?>
        </div>
        <?php echo form_hidden('id_personal',"23") ?>