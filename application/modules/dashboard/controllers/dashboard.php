<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class dashboard extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_dashboard');
			$this->load->helper('download');
			$this->load->library('form_validation');
		}
		
		public function index(){
			$data['siswa'] = $this->m_dashboard->get_all_siswa();
			$data['rombel'] = $this->m_dashboard->get_all_rombel();
			$data['ptk'] = $this->m_dashboard->get_all_ptk();
			$data['prestasi'] = $this->m_dashboard->get_all_prestasi();
			$data['kalender_akademik'] = $this->m_dashboard->get_all_kalender_akademik();
			$tahun_ajaran = $this->m_dashboard->get_all_tahun_ajaran();
			$tahun = get_id_tahun_ajaran();
			foreach($tahun_ajaran as $t){
				if($t->id_tahun_ajaran == $tahun){
					$data['tahun_ajaran'] = $t->tahun_awal.' / '.$t->tahun_akhir;
				}
			}
			
			$data['component'] = 'dashboard';
			// echo '<pre>';
			// print_r($data);
			// echo '</pre>';
			render("dashboard/dashboard", $data);
		}
		
		
}
