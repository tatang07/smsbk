<div class="grid_12">
<h1 class="grid_12">Dashboard</h1>
<div class="grid_12">
	<p class="grid_12">
		Selamat datang di SMSBK.<br><br>
		SMSBK adalah Sistem Manajemen Sekolah Berbasis Keunggulan dengan menggunakan ICT secara penuh.
		<br><br>
		SMSBK ini penting diterapkan sebagai upaya untuk mewujudkan keunggulan dalam pengelolaan sekolah. Hal ini dimaksudkan untuk pengokohan performa sekolah yang dapat menampung kompleksitas manajemen dalam suatu perangkat kerja computerized yang simple tetapi komprehensif sehingga dapat meningkatkan kapasitas dan kapabilitas manajemen sekolah.
		<br/>&nbsp;
	</p>
</div>
</div>
<div class="grid_12">


		
		
			
			<h1 class="grid_12 margin-top no-margin-top-phone">Statistik</h1>
			
			<div class="grid_12 center-elements">
				<div class="full-stats">
					<div class="stat hlist" data-list='[{"val":<?php echo $siswa['jumlah']; ?>,"format":"0,0;-0,0","title":"Total Siswa","color":"green"},
														{"val":<?php echo $rombel['jumlah']; ?>,"format":"0,0;-0,0","title":"Total Rombongan Belajar","color":"green"},
														{"val":<?php echo $ptk['jumlah']; ?>,"format":"0,0;-0,0","title":"Total Pendidik dan Tenaga Kependidikan","color":"green"}]', 
														data-flexiwidth=true></div>
				</div>
			</div>
			
			<div class="grid_12">
			<div class="box with-table">
				<div class="header">
					<h2>Kalender Akademik <?php echo $tahun_ajaran; ?></h2>
				</div>
				
				<div class="content">
					<div id="datatable" url="">
						<div class="dataTables_wrapper"  role="grid">    
							<table class="styled" >
								<thead>
									<tr>
										<th>No.</th>
										<th>Tanggal</th>
										<th>Kegiatan Akademik</th>
									</tr>
								</thead>
								<tbody>
								<?php $no = 1; if($kalender_akademik){ foreach($kalender_akademik as $k): ?>
									<tr>
									<td width='10px' class='center'><?php echo $no; ?></td>
									<td width='80px' class='center'><?php echo tanggal_view($k->tanggal_mulai).' - '.tanggal_view($k->tanggal_selesai); ?></td>
									<td width='300px' class='left'><?php echo $k->kegiatan; ?></td>
									</tr>
								<?php $no++; endforeach; } ?>
								</tbody>
							</table>
							
						</div>
					</div>
				</div>        
			</div>
			</div>
			
			<div class="grid_12">
				<div class="box">
					<div class="header">
						<h2>Prestasi Terkini Tahun <?php echo $tahun_ajaran; ?></h2>
					</div>
				</div>
			</div>
	
			<div class="grid_6">
				<div class="box">
				
					<div class="header">
						<h2>Intra Kurikuler</h2>
					</div>
					
					<div class="accordion toggle">
						<?php if($prestasi){ foreach($prestasi as $p): ?>
						<?php if($p->id_ekstra_kurikuler == 0){ ?>
							<h3><a href="#"><?php echo $p->perlombaan; ?></a></h3>
							<div>
								<p><a href="#"><?php echo $p->prestasi; ?></a></p>
							</div>		
						
						<?php } endforeach; } ?>
						
												
					</div><!-- End of .content -->
					
				</div><!-- End of .box -->
			</div><!-- End of .grid_4 -->
			<div class="grid_6">
				<div class="box">
				
					<div class="header">
						<h2>Ekstra Kurikuler</h2>
					</div>
					
					<div class="accordion toggle">										
						<?php if($prestasi){ foreach($prestasi as $p): ?>
						<?php if($p->id_ekstra_kurikuler != 0){ ?>
							<h3><a href="#"><?php echo $p->perlombaan; ?></a></h3>
							<div>
								<p><a href="#"><?php echo $p->prestasi; ?></a></p>
							</div>		
						
						<?php } endforeach; } ?>
					</div><!-- End of .content -->
					
				</div><!-- End of .box -->
			</div><!-- End of .grid_4 -->
	
</div>

