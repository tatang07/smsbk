<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class visi extends Simple_Controller {
    	public function index(){
			redirect('sekolah/visi/datalist');
		}
		
		public $myconf = array(
			//Nama controller
			"name" 			=> "sekolah/visi",
			"component"		=> "pengelolaan_sekolah",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Pengelolaan Sekolah",
			"subtitle"		=> "Visi",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "m_visi",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"visi"=>"Visi",
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								"visi"=>"wysiwyg"
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"visi"
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								"id_sekolah_visi",
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("visi"),
								"field_sort"			=> array("visi"),
								"field_filter"			=> array("visi"),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(),
								//"field_comparator"		=> array("id_pihak_komunikasi"=>"=","id_sekolah"=>"="),
								//"field_operator"		=> array("id_pihak_komunikasi"=>"AND","id_sekolah"=>"AND"),
								//"field_align"			=> array(),
								"field_size"			=> array(),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								//"custom_action_link"	=> array(),
								"custom_add_link"		=> array('label'=>'Tambah','href'=>'sekolah/visi/add' ),
								"custom_edit_link"		=> array('label'=>'Edit', 'href'=>'sekolah/visi/edit'),
								"custom_delete_link"	=> array('label'=>'Delete', 'href'=>'sekolah/visi/delete'),
								//"custom_link"			=> array("Kurikulum"=>"kurikulum/datalist","Siswa"=>"Siswa/datalist"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("visi"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								//"redirect_link"			=> "sekolah/visi/datalist"
							),
			//Konfigurasi tampilan edit
			"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"enable"				=> TRUE,
								//"model_name"			=> "m_visi",
								"table"					=> array(),
								"view_file"				=> "simple/edit",
								"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array('visi'),
								"custom_action"			=> "",
								"msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								"msg_on_success"		=> "Data berhasil diperbaharui.",
								//"redirect_link"			=> "sekolah/visi/datalist"
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								//"model_name"			=> "m_visi",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								//"redirect_link"			=> "sekolah/visi/datalist"
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "sample/laporan_datalist",
								"view_xls"				=> "sample/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)
		);
	
}
