<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class sekolah extends Simple_Controller {
    	public function index(){
			redirect('sekolah/datalist');
	    }

    	public $myconf = array(
			//Nama controller
			"name" 			=> "sekolah",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Daftar Sekolah",
			"subtitle"		=> "",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "m_sekolah",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"nss"=>"Nomor Statistik Sekolah",
								"nps"=>"Nomor Pokok Sekolah",
								"nama_strip"=>"Panggilan Sekolah",
								"sekolah"=>"Nama Sekolah",
								"alamat"=>"Alamat",	
								"kode_pos"=>"Kode Pos",	
								"telepon"=>"Telepon",					
								"fax"=>"Faximile",					
								"sk_pendirian"=>"SK Pendirian",	
								"tgl_sk_pendirian"=>"Tanggal SK Pendirian",
								"no_sk_akreditasi"=>"SK Akreditasi",	
								"tgl_akreditasi"=>"Tanggal SK Akreditasi",	
								"email"=>"E-Mail",					
								"website"=>"Alamat Website",
								"luas_halaman"=>"Luas Halaman",					
								"luas_tanah"=>"Luas Tanah",					
								"luas_bangunan"=>"Luas Bangunan",					
								"luas_olahraga"=>"Luas Fasilitas Olahraga",					
								"id_kecamatan"=>"Kecamatan",					
								"kecamatan"=>"Kecamatan",
								"id_jenjang_sekolah"=>"Jenjang Sekolah",					
								"jenjang_sekolah"=>"Jenjang Sekolah",					
								"id_jenis_akreditasi"=>"Akreditasi",						
								"jenis_akreditasi"=>"Akreditasi",					
								"id_status_sekolah"=>"Status",					
								"status_sekolah"=>"Status",					
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								"id_kecamatan"=>array("r_kecamatan","id_kecamatan","kecamatan"),
								"id_jenjang_sekolah"=>array("r_jenjang_sekolah","id_jenjang_sekolah","jenjang_sekolah"),
								"id_jenis_akreditasi"=>array("r_jenis_akreditasi","id_jenis_akreditasi","jenis_akreditasi"),
								"id_status_sekolah"=>array("r_status_sekolah","id_status_sekolah","status_sekolah"),
								"tgl_sk_pendirian"=>"date",
								"tgl_akreditasi"=>"date",
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"sekolah"
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								"sekolah"
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("sekolah","alamat","telepon"),
								"field_sort"			=> array("sekolah","alamat","telepon"),
								"field_filter"			=> array("sekolah","alamat","telepon"),
								"field_filter_dropdown"	=> array(
															),
								//"field_comparator"		=> array(),
								//"field_operator"		=> array(),
								//"field_align"			=> array(),
								"field_size"			=> array("action_col"=>"15%"),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array(
																),
								//"custom_add_link"		=> array('label'=>'Add','href'=>'school/add' ),
								//"custom_edit_link"		=> array(),
								//"custom_delete_link"	=> array(),
								"custom_link"			=> array(
														),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> 	array(
																"nss",
																"nps",
																"nama_strip",
																"sekolah",
																"alamat",	
																"kode_pos",	
																"telepon",					
																"fax",					
																"sk_pendirian",	
																"tgl_sk_pendirian",
																"no_sk_akreditasi",	
																"tgl_akreditasi",	
																"email",					
																"website",
																"luas_halaman",					
																"luas_tanah",					
																"luas_bangunan",					
																"luas_olahraga",					
																"id_kecamatan",					
																"id_jenjang_sekolah",					
																"id_jenis_akreditasi",						
																"id_status_sekolah",					
															),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								//"redirect_link"			=> ""
							),
			//Konfigurasi tampilan edit
			/*"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"view_file"				=> "simple/edit",
								"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array(),
								"custom_action"			=> "",
								"msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								"msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan delete
			*/
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini? Data yang berhubungan dengan Sekolah, sperti User, Data, dll akan ikut terhapus",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> FALSE,
								"enable_xls"			=> FALSE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)
		);	
		
		public function after_add($data){
			if($this->input->post()){
				$fakta['username'] = $data['item']['nps']; 
				$fakta['password'] = md5($fakta['username']);
				$fakta['default_password'] = md5($fakta['username']);
				$fakta['email'] = $data['item']['email']; 
				$fakta['date_created'] = date('Y-m-d G:i:s'); 
				$fakta['is_active'] = 1; 
				$fakta['id_group'] = 3; 
				$id = $this->m_sekolah->get_new_id();
				$fakta['id_sekolah'] = $id;
				$fakta['id_personal'] = $this->m_sekolah->new_id();
				$this->m_sekolah->tambah_user($fakta);
			}
			return $data;
		}

		public function after_delete($data){
			$id=$this->uri->rsegment(3);
			$query = "delete from sys_user where id_sekolah='$id'";
			$exec = mysql_query($query);
			return $data;
		}
}
