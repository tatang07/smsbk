<?php
class m_sekolah extends MY_Model {
    public $table = "m_sekolah";
    public $id = "id_sekolah";
    public $order_fields = array("sekolah DESC","nss");
	// join ON 
	public $join_to = array(
			"r_jenis_akreditasi rja"=>"rja.id_jenis_akreditasi=m_sekolah.id_jenis_akreditasi",
			"r_status_sekolah rs"=>"rs.id_status_sekolah=m_sekolah.id_status_sekolah"
		);
	public $join_fields = array("jenis_akreditasi","status_sekolah");

	public function get_new_id(){
		$query = "select id_sekolah from m_sekolah order by id_sekolah desc limit 1";
		$result=mysql_query($query);
		$data = mysql_fetch_array($result);
		$new_id = $data['id_sekolah'];
		return $new_id;
	}
	public function tambah_user($data=array()){
		$this->db->insert('sys_user', $data);
	}
}