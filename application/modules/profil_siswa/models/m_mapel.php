<?php
class m_mapel extends MY_Model {
    function get_tingkat($id){
    	$this->db->select('*');
		$this->db->from('m_tingkat_kelas');
		$this->db->where('id_jenjang_sekolah', $id);
		return $this->db->get();
    }

    function select_data_by($id, $id_sis){
		$this->db->select('*, gmr.id_guru as id_guur');
		$this->db->from('t_guru_matpel_rombel gmr');
		$this->db->join('m_rombel r', 'r.id_rombel = gmr.id_rombel');
		$this->db->join('t_rombel_detail rd', 'rd.id_rombel = gmr.id_rombel');
		$this->db->where('r.id_tingkat_kelas',$id);
		$this->db->where('rd.id_siswa',$id_sis);
		return $this->db->get();		
	}

	function select_pelajaran($id){
		$this->db->select('*');
		$this->db->from('m_pelajaran');
		$this->db->where('id_pelajaran',$id);
		return $this->db->get();
	}

	function select_guru($id){
		$this->db->select('*');
		$this->db->from('m_guru');
		$this->db->where('id_guru',$id);
		return $this->db->get();
	}
}