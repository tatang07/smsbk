<?php
class m_jadwal extends MY_Model {
	public function get_jadwal($id_sis){
		$this->db->select('*');
		$this->db->from('m_jadwal_pelajaran_detail jp'); 
		$this->db->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel = jp.id_guru_matpel_rombel');
		$this->db->join('t_rombel_detail rd', 'rd.id_rombel = gm.id_rombel');
		$this->db->join('m_pelajaran m', 'gm.id_pelajaran = m.id_pelajaran');
		$this->db->join('m_ruang_belajar rb', 'rb.id_ruang_belajar = jp.id_ruang_belajar');
		$this->db->join('m_guru g', 'gm.id_guru=g.id_guru');
		$this->db->where('rd.id_siswa',$id_sis);
		return $this->db->get();
		//$this->db->where('jp.hari',$hari);
	}
}