<?php
class m_ekskul extends MY_Model {
    function get_ekskul($id){
    	$this->db->select('*');
		$this->db->from('t_ekstra_kurikuler_peserta ekp');
		$this->db->join('m_ekstra_kurikuler ek', 'ek.id_ekstra_kurikuler = ekp.id_ekstra_kurikuler');
		//$this->db->join('m_siswa s', 's.id_siswa = ekp.id_siswa');
		$this->db->where('ekp.id_siswa', $id);
		$this->db->where('ek.jenis', '1');
		return $this->db->get();
    }
}