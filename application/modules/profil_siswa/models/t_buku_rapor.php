<?php
class t_buku_rapor extends MY_Model {
	public $table = "t_buku_rapor";
    public $id = "id_nilai_konversi";
	
	public function get_sekolah_by(){
		$this->db->select('*');
		$this->db->from('m_sekolah');
		$this->db->where('id_sekolah',get_id_sekolah());
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	public function get_siswa_by($id_sis){
		$this->db->select('*');
		$this->db->from('v_siswa');
		$this->db->where('id_siswa',$id_sis);
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	public function get_all_rombel(){
		$query = $this->db
						->from('m_rombel gm')
						->join('m_tingkat_kelas p', 'gm.id_tingkat_kelas=p.id_tingkat_kelas')
						->where('p.id_jenjang_sekolah', get_jenjang_sekolah())
						->where('gm.id_sekolah', get_id_sekolah())
						->get();
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	
	public function get_rombel(){
		$query = $this->db
						->from('t_rombel_detail rd')
						->join('m_rombel r', 'rd.id_rombel=r.id_rombel')
						->join('m_siswa s', 'rd.id_siswa=s.id_siswa')												
						->where('rd.id_siswa', get_id_personal())
						->get();
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_all_konversi(){
		$query = $this->db
						->from('t_nilai_konversi')
						->where('id_tahun_ajaran', get_id_tahun_ajaran())
						->where('id_sekolah', get_id_sekolah())
						->get();
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_all_siswa(){
		$query = $this->db->get('v_siswa');
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_all_kelompok_pelajaran(){
		$query = $this->db->get('m_kelompok_pelajaran');
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_all_gmr(){
		$query = $this->db
						->from('t_guru_matpel_rombel gm')
						->join('m_rombel r', 'gm.id_rombel=r.id_rombel')
						->join('m_guru g', 'gm.id_guru=g.id_guru')
						->join('m_pelajaran p', 'gm.id_pelajaran=p.id_pelajaran')
						->get();
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_nilai(){
		$query = $this->db
						->select('nd.id_siswa, nk.id_nilai_kategori, nk.id_komponen_nilai, t.id_guru_matpel_rombel, sum(nd.nilai) as jumlah, count(nd.id_nilai) as banyak, nk.bobot')
						->from('t_nilai_kategori nk')
						->join('t_nilai t', 't.id_nilai_kategori=nk.id_nilai_kategori')
						->join('t_nilai_detail nd', 'nd.id_nilai=t.id_nilai')
						->join('t_guru_matpel_rombel gm', 'gm.id_guru_matpel_rombel=t.id_guru_matpel_rombel')
						->group_by('nk.id_nilai_kategori')
						->group_by('nd.id_siswa')
						->order_by('nd.id_siswa')
						->where('nk.id_tahun_ajaran', get_id_tahun_ajaran())
						->where('nk.id_sekolah', get_id_sekolah())
						->where('t.id_term', get_id_term())
						->get();
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
}
