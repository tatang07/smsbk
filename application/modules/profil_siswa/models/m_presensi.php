<?php
class m_presensi extends MY_Model {
    function get_presensi($id){
    	$this->db->select('*');
		$this->db->from('t_siswa_absen sa');
		$this->db->join('t_agenda_kelas ak', 'ak.id_agenda_kelas = sa.id_agenda_kelas');
		$this->db->join('r_jenis_absen ja', 'ja.id_jenis_absen = sa.id_jenis_absen');
		$this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_guru_matpel_rombel = ak.id_guru_matpel_rombel');
		$this->db->join('m_pelajaran p', 'p.id_pelajaran = gmr.id_pelajaran');



		//$this->db->join('m_siswa s', 's.id_siswa = ekp.id_siswa');
		$this->db->where('sa.id_siswa', $id);
		//$this->db->where('ek.jenis', '1');
		return $this->db->get();
    }
}