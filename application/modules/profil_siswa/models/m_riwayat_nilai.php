<?php
class m_riwayat_nilai extends MY_Model {
	function get_kategori($id){
		$this->db->select('*');
		$this->db->from('t_nilai_kategori');
		$this->db->where('id_sekolah', $id);
		return $this->db->get();
	}

	function get_nilaii($id, $id_kelas){
		$this->db->select('*');
		$this->db->from('t_nilai n');
		$this->db->join('t_nilai_detail nd', 'n.id_nilai = nd.id_nilai');
		$this->db->join('t_guru_matpel_rombel gmr', 'n.id_guru_matpel_rombel = gmr.id_guru_matpel_rombel');
		$this->db->join('m_rombel mr', 'mr.id_rombel = gmr.id_rombel');
		$this->db->join('m_pelajaran p', 'p.id_pelajaran = gmr.id_pelajaran');
		$this->db->join('t_nilai_kategori nk', 'nk.id_nilai_kategori = n.id_nilai_kategori');
		$this->db->where('nd.id_siswa', $id);
		$this->db->where('mr.id_tingkat_kelas', $id_kelas);
		$this->db->order_by('p.pelajaran');
		return $this->db->get();
	}

	function get_pelajaran($id){
		$this->db->select('*');
		$this->db->from('t_nilai_detail nd');
		$this->db->join('t_nilai n', 'n.id_nilai = nd.id_nilai');
		$this->db->join('t_nilai_kategori nk', 'nk.id_nilai_kategori = n.id_nilai_kategori');
		$this->db->join('t_guru_matpel_rombel gmr', 'n.id_guru_matpel_rombel = gmr.id_guru_matpel_rombel');
		$this->db->join('m_pelajaran p', 'p.id_pelajaran = gmr.id_pelajaran');
		$this->db->where('nd.id_siswa', $id);
		return $this->db->get();
	}

	function get_nilai($id, $id_nilai){
		$this->db->select('*');
		$this->db->from('t_nilai_detail nd');
		$this->db->join('t_nilai n', 'n.id_nilai = nd.id_nilai');
		$this->db->join('t_nilai_kategori nk', 'nk.id_nilai_kategori = n.id_nilai_kategori');
		$this->db->join('t_guru_matpel_rombel gmr', 'n.id_guru_matpel_rombel = gmr.id_guru_matpel_rombel');
		$this->db->join('m_pelajaran p', 'p.id_pelajaran = gmr.id_pelajaran');
		$this->db->where('nd.id_siswa', $id);
		$this->db->where('nk.id_nilai_kategori', $id_nilai);
		return $this->db->get();
	}
}