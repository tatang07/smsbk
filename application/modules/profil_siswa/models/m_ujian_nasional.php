<?php
class m_ujian_nasional extends MY_Model {
    function get_nilai($id){
    	$this->db->select('*');
		$this->db->from('t_nilai_akhir na');
		$this->db->join('t_nilai_akhir_detail nad', 'na.id_nilai_akhir = nad.id_nilai_akhir');
		$this->db->join('m_pelajaran p', 'na.id_pelajaran = p.id_pelajaran');
		$this->db->where('nad.id_siswa', $id);
		$this->db->where('na.jenis', '1');
		return $this->db->get();
    }
}