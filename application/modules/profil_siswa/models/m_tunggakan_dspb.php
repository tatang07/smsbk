<?php
class m_tunggakan_dspb extends MY_Model {
    function get_tahun_ajaran(){
    	$this->db->select('*');
		$this->db->from('m_tahun_ajaran');
		return $this->db->get();
    }

    function get_tunggakan($id_siswa, $id_tahun_ajaran, $bulan, $id_sekolah){
		$this->db->select('*, sum(kps.jumlah_dibayar) as jumlah_dibayar');
		$this->db->from('t_keuangan_pembayaran_siswa kps');
		$this->db->join('v_siswa s','s.id_siswa = kps.id_siswa');
		$this->db->join('t_keuangan_besaran kb', 's.id_tingkat_kelas = kb.id_tingkat_kelas');
		$this->db->where('s.id_siswa',$id_siswa);
		$this->db->where('s.id_tahun_ajaran',$id_tahun_ajaran);
		$this->db->where('kps.bulan',$bulan);
		$this->db->where('kps.id_jenis_pembayaran','1');
		$this->db->where('kb.id_jenis_pembayaran','1');
		$this->db->where('kb.id_sekolah',$id_sekolah);
		$this->db->group_by('kps.bulan');
		
		return $this->db->get();
	}
}