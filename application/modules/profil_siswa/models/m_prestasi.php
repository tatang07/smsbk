<?php
class m_prestasi extends MY_Model {
    function get_prestasi($id){
    	$this->db->select('*');
		$this->db->from('t_prestasi_siswa ps');
		$this->db->join('r_tingkat_wilayah tw', 'ps.id_tingkat_wilayah = tw.id_tingkat_wilayah');
		$this->db->where('ps.id_siswa', $id);
		return $this->db->get();
    }
}