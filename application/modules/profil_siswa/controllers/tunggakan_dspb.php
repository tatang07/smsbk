<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class tunggakan_dspb extends Simple_Controller {    	
	public function index(){
		$this->load->model('m_tunggakan_dspb');
		$data['dspb'] = array();
		$data['tahun_ajaran'] = $this->m_tunggakan_dspb->get_tahun_ajaran()->result_array();
		render('tunggakan_dspb/datalist', $data);
	}

	public function search(){
		$this->load->model('m_tunggakan_dspb');
		$id_sis = get_id_personal();
		$id_sekolah = get_id_sekolah();

		$id_tahun_ajaran = $this->input->post('id_tahun_ajaran');
		$bulan = $this->input->post('bulan');
		
		$data['dspb'] = $this->m_tunggakan_dspb->get_tunggakan($id_sis, $id_tahun_ajaran, $bulan, $id_sekolah)->result_array();
		
		//print_r($data['dspb']);

		render('tunggakan_dspb/datalist', $data);
	}
}
