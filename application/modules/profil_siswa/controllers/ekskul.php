<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class ekskul extends Simple_Controller {    	
	public function index(){
		$this->load->model('m_ekskul');
		$id_sis = get_id_personal();
		$data['eks'] = $this->m_ekskul->get_ekskul($id_sis)->result_array();
		
		//print_r($data['eks']);
		render('ekskul/datalist', $data);
	}
}
