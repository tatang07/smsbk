<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class ujian_sekolah extends Simple_Controller {    	
	public function index(){
		$this->load->model('m_ujian_sekolah');
		$id_sis = get_id_personal();
		$data['nilai'] = $this->m_ujian_sekolah->get_nilai($id_sis)->result_array();
		render('ujian_sekolah/datalist', $data);
	}
}
