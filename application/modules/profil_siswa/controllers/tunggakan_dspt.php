<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class tunggakan_dspt extends Simple_Controller {    	
	public function index(){
		$this->load->model('m_tunggakan_dspt');
		$data['dspt'] = array();
		$data['tahun_ajaran'] = $this->m_tunggakan_dspt->get_tahun_ajaran()->result_array();
		render('tunggakan_dspt/datalist', $data);
	}

	public function search(){
		$this->load->model('m_tunggakan_dspt');
		$id_sis = get_id_personal();
		$id_sekolah = get_id_sekolah();

		$id_tahun_ajaran = $this->input->post('id_tahun_ajaran');
		
		$data['dspt'] = $this->m_tunggakan_dspt->get_tunggakan($id_sis, $id_tahun_ajaran, $id_sekolah)->result_array();
		
		//print_r($data['dspb']);

		render('tunggakan_dspt/datalist', $data);
	}
}
