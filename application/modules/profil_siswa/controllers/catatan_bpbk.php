<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class catatan_bpbk extends Simple_Controller {    	
	public function index(){
		$this->load->model('m_catatan_bpbk');
		$id_sis = get_id_personal();
		$data['bpbk'] = $this->m_catatan_bpbk->get_catatan($id_sis)->result_array();
		render('catatan_bpbk/datalist', $data);
	}
}
