<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class riwayat_nilai extends Simple_Controller {    	
	public function index(){
		$this->load->model('m_mapel');
		$id_jenjang = get_jenjang_sekolah();

		$data['tingkat'] = $this->m_mapel->get_tingkat($id_jenjang)->result_array();
		render('riwayat_nilai/datalist', $data);
	}
}
