<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class jadwal extends Simple_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('m_jadwal');
	}
	
	public function index(){
		$id_sekolah = get_id_sekolah();
		$id_sis = get_id_personal();
		$data['jadwal'] = $this->m_jadwal->get_jadwal($id_sis)->result_array();

		//print_r($data['jadwal']);

		render("jadwal/datalist", $data);
	}		
}