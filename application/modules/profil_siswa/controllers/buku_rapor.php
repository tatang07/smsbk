<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class buku_rapor extends CI_Controller {

	public function index(){
		
		$id_sekolah = get_id_sekolah();
		$id_sis = get_id_personal();
	
		$this->load->model('t_buku_rapor');
		
		// echo $id_sis;
		$this->load->model('profil_siswa/t_buku_rapor');
		$data['sekolah'] = $this->t_buku_rapor->get_sekolah_by();
		$data['rombel'] = $this->t_buku_rapor->get_all_rombel();
		$data['konversi'] = $this->t_buku_rapor->get_all_konversi();
		$data['siswa'] = $this->t_buku_rapor->get_all_siswa();
		$data['kelompok_pelajaran'] = $this->t_buku_rapor->get_all_kelompok_pelajaran();
		$data['gmr'] = $this->t_buku_rapor->get_all_gmr();
		$data['nilai'] = $this->t_buku_rapor->get_nilai();
		$data['siswa_rapor'] = $this->t_buku_rapor->get_siswa_by($id_sis);
		$data['kelas'] = $this->t_buku_rapor->get_rombel();
		
		 // echo  "<pre>";
		 // print_r($data['siswa_rapor']);
		 // echo"</pre>";
		
		// $data['id_rombel'] = 0;
		// $data['id_sis'] = 0;
		
		// $data['stat'] = 'home';
		
		// $data['component']="evaluasi_pembelajaran";
		render('buku_rapor/buku_rapor',$data);
	}
	
	// public function search(){
		
		//$id_rombel = $this->input->post('id_rombel');
		// $id_siswa = $this->input->post('id_siswa');
		// $id_rombel = $this->input->post('id_rombel');
		// $id_siswa = $this->input->post('id_siswa');

		// $id_sekolah = get_id_sekolah();
		// $id_sis = get_id_personal();
		// $this->load->model('evaluasi/t_buku_rapor');
		
		// $this->load->model('t_buku_rapor');

		// $data['id_rombel'] = $id_rombel;
		// $data['id_siswa'] = $id_siswa;
		
		// $data['id_rombel'] = $id_rombel;
		// $data['id_siswa'] = $id_siswa;
		// $data['sekolah'] = $this->t_buku_rapor->get_sekolah_by();
		// $data['rombel'] = $this->t_buku_rapor->get_all_rombel();
		// $data['konversi'] = $this->t_buku_rapor->get_all_konversi();
		// $data['siswa'] = $this->t_buku_rapor->get_all_siswa();
		// $data['kelompok_pelajaran'] = $this->t_buku_rapor->get_all_kelompok_pelajaran();
		// $data['gmr'] = $this->t_buku_rapor->get_all_gmr();
		// $data['nilai'] = $this->t_buku_rapor->get_nilai();
		// $data['siswa_rapor'] = $this->t_buku_rapor->get_siswa_by($id_siswa);
		
		// $data['sekolah'] = $this->t_buku_rapor->get_sekolah_by();
		// $data['rombel'] = $this->t_buku_rapor->get_all_rombel();
		// $data['konversi'] = $this->t_buku_rapor->get_all_konversi();
		// $data['siswa'] = $this->t_buku_rapor->get_all_siswa();
		// $data['kelompok_pelajaran'] = $this->t_buku_rapor->get_all_kelompok_pelajaran();
		// $data['gmr'] = $this->t_buku_rapor->get_all_gmr();
		// $data['nilai'] = $this->t_buku_rapor->get_nilai();
		// $data['siswa_rapor'] = $this->t_buku_rapor->get_siswa_by($id_sis);
		
		
		// echo "<pre>";
		// print_r($data['nilai']);
		// echo "</pre>";
		// $data['stat'] = 'search';
		
		

		// $data['component']="evaluasi_pembelajaran";		
		//print_r($data['konversi']);
		// render('buku_rapor/buku_rapor',$data);
	// }
		// $data['component']="evaluasi_pembelajaran";		
		// print_r($data['konversi']);
		// render('buku_rapor/buku_rapor',$data);
	// }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */