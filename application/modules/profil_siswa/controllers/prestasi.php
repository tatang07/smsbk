<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class prestasi extends Simple_Controller {    	
	public function index(){
		$this->load->model('m_prestasi');
		$id_sis = get_id_personal();

		$data['prestasi'] = $this->m_prestasi->get_prestasi($id_sis)->result_array();
		
		render('prestasi/datalist', $data);
	}
}
