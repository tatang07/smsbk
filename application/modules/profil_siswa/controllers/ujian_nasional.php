<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class ujian_nasional extends Simple_Controller {    	
	public function index(){
		$this->load->model('m_ujian_nasional');
		$id_sis = get_id_personal();
		$data['nilai'] = $this->m_ujian_nasional->get_nilai($id_sis)->result_array();
		render('ujian_nasional/datalist', $data);
	}
}
