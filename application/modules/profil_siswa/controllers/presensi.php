<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class presensi extends Simple_Controller {    	
	public function index(){
		$this->load->model('m_presensi');
		$id_sis = get_id_personal();

		$data['presensi'] = $this->m_presensi->get_presensi($id_sis)->result_array();
		
		$result = array();
		foreach($data['presensi'] as $absen){
			if(! isset($result[$absen['pelajaran']][$absen['jenis_absen']])){
				$result[$absen['pelajaran']][$absen['jenis_absen']] = 1;
			}else{
				$result[$absen['pelajaran']][$absen['jenis_absen']]++ ;
			}
		}

		$data['result'] = $result;

		render('presensi/datalist', $data);
	}
}
