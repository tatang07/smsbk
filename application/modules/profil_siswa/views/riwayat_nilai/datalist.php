
<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar Riwayat Nilai</h2>
       </div>
       
		<div class="content">
			<div class="dataTables_wrapper"  role="grid">    
				<div class="content" style='margin:5px;'>				
				<div class="box tabbedBox">
					<div class="header">
						<h2>&nbsp;</h2>
						<ul>
							<?php $c = 1; ?>
							<?php foreach($tingkat as $t){ ?>
								<li><a href="#<?php echo 't1-c'.$c ?>"><?php echo $t['tingkat_kelas']; ?></a></li>								
							<?php $c++; } ?>
						</ul>
					</div>
					
					<div class="content tabbed">
						<?php $c = 1; ?>
						<?php foreach($tingkat as $t){ ?>
							<div id="<?php echo 't1-c'.$c ?>">
								<?php
									$id = $t['id_tingkat_kelas'];
									$id_sis = get_id_personal();
									//echo $id;
									$this->load->model('m_riwayat_nilai');
									$nilai=$this->m_riwayat_nilai->get_nilaii($id_sis, $id)->result_array();
								?>
								<table class="styled" >
									<thead>
										<tr>
											<th>No.</th>
											<th>Mata Pelajaran</th>
											<th>Nama Nilai</th>
											<th>Kategori Nilai</th>
											<th>Nilai</th>

										</tr>
									</thead>
									<tbody>
										<?php $i=1; ?>
										<?php foreach($nilai as $n){ ?>
											<tr>
												<td class="center" width="100px"><?php echo $i; $i++; ?></td>
												<td><?php echo $n['pelajaran']; ?></td>
												<td><?php echo $n['nama_nilai']; ?></td>
												<td><?php echo $n['nama_nilai_kategori']; ?></td>
												<td><?php echo $n['nilai']; ?></td>
											</tr>
										<?php } ?>	
									</tbody>
								</table>
								<div class="footer">
								</div>	
							</div>								
						<?php $c++; } ?>
					</div>
				</div>
			</div>
        </div>        
    </div>
 </div>