<?php $this->load->model('m_mapel');?>
<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar Mata Pelajaran</h2>
       </div>
       
		<div class="content">
			<div class="dataTables_wrapper"  role="grid">    
				<div class="content" style='margin:5px;'>				
				<div class="box tabbedBox">
					<div class="header">
						<h2>&nbsp;</h2>
						<ul>
							<?php $c = 1; ?>
							<?php foreach($tingkat as $t){ ?>
								<li><a href="#<?php echo 't1-c'.$c ?>"><?php echo $t['tingkat_kelas']; ?></a></li>								
							<?php $c++; } ?>
						</ul>
					</div>
					
					<div class="content tabbed">
						<?php $c = 1; ?>
						<?php foreach($tingkat as $t){ ?>
							<div id="<?php echo 't1-c'.$c ?>">
								<?php
									$id = $t['id_tingkat_kelas'];
									$id_sis = get_id_personal();
									//echo $id;									
									$data['siswa']=$this->m_mapel->select_data_by($id, $id_sis)->result_array();
									$i=0;
									$a=null;
									foreach ($data['siswa'] as $siswa) {
										$a[$i]['pel'] = $this->m_mapel->select_pelajaran($siswa['id_pelajaran'])->result_array();
										$a[$i]['guru'] = $this->m_mapel->select_guru($siswa['id_guur'])->result_array();
										$i++;
									}	
								?>
								<table class="styled">
									<thead>
										<tr>
											<th>No.</th>
											<th>Mata Pelajaran</th>
											<th>Guru</th>
										</tr>
									</thead>
									<tbody>
										<?php $i=0;$no=1; ?>
										<?php if($a){foreach($a as $b) { ?>
										<tr>
											<td width='' class='center'><?php echo $no;$no++; ?></td>
											<td width='' class='left'><?php echo $b['pel'][0]['pelajaran']?></td>
											<td width='' class='left'><?php echo $b['guru'][0]['nama']?></td>
										</tr>
										<?php $i++;}}?>
									</tbody>
								</table>	
							</div>								
						<?php $c++; } ?>
					</div>
				</div>
			</div>
        </div>        
    </div>
 </div>