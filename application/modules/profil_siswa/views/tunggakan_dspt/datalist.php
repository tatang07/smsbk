<h1 class="grid_12">Pembiayaan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Tunggakan DSPT</h2>
       </div>
	
       <div class="tabletools">
			<div class="right">
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('profil_siswa/tunggakan_dspt/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<td width="200px" align="left">
									<select name="id_tahun_ajaran">									
										<option value="0">Tahun Ajaran</option>
										<?php foreach($tahun_ajaran as $ta):?>
											<option value="<?php echo $ta['id_tahun_ajaran'] ?>"><?php echo $ta['tahun_awal'] .' - '. $ta['tahun_akhir']?></option>
										<?php endforeach;?>
									</select>
								</td>
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
	
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Nis</th>
								<th >Nama</th>
								<th >Jumlah Tagihan</th>
								<th >Dibayar</th>
								<th >Tunggakan</th>
							</tr>
						</thead>
				
						<tbody>
							<?php $no=1; if($dspt) foreach($dspt as $d):?>							
								<tr>
									<td width='10px' class='center'><?php echo $no; $no++;?></td>
									<td class='center'><?php  echo $d['nis']?></td class='center'>
									<td class='center'><?php  echo $d['nama'];?></td>									
									<td class='center'><?php  echo $d['besaran'];?></td>
									<td class='center'><?php  echo $d['jumlah_dibayar'];?></td>
									<td class='center'><?php  echo $d['tunggakan'] = $d['besaran'] - $d['jumlah_dibayar'];?></td>
									
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
					</div>
				</div>
            </div>
        </div>        
    </div>
</script>
</div>