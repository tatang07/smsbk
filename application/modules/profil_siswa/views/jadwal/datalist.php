
<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Jadwal Siswa</h2>
       </div>
       
		<div class="content">
			<div class="dataTables_wrapper"  role="grid">    
				<div class="content" style='margin:5px;'>				
				
				<div class="box with-table tabbedBox" id="field1">		
					<div class="header">
						<h2>Jadwal Kelas</h2>
						<?php $hari = array('Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'); ?>
						<ul>
							<?php for($i=0;$i<6;$i++){ ?>
								<li><a href="#<?php echo $hari[$i]; ?>"><?php echo $hari[$i]; ?></a></li>
							<?php } ?>
						</ul>
					</div><!-- End of .header -->
					
					<div class="content tabbed">
						<?php for($i=0;$i<6;$i++){ ?>
							<div id="<?php echo $hari[$i]; ?>">								
								<div id="datatable" url="">
									<div class="dataTables_wrapper"  role="grid">    
										<table class="styled" >
											<thead>
												<tr>
													<th>Waktu</th>
													<th>Ruang</th>
													<th>Mata Pelajaran</th>
													<th>Guru</th>
												</tr>
											</thead>
											<tbody>
												<?php if($jadwal){ ?>
												<?php foreach($jadwal as $d): ?>
													<?php $ha=ucwords($d['hari']); if($ha == $hari[$i]){ ?>
													<tr>
														<td width="150px" class="center"><?php echo $d['mulai']." - ".$d['selesai']; ?></td>
														<td width="100px" class="center"><?php echo $d['ruang_belajar']; ?></td>
														<td width="300px" class=""><?php echo $d['pelajaran']; ?></td>
														<td width="200px" class=""><?php echo $d['nama']; ?></td>
													</tr>
												<?php } endforeach; ?>
											</tbody>
										</table>
												<?php }else{ ?>
														</tbody>
													</table>
													<div class="footer">Data tidak ditemukan!</div>
												<?php } ?>
									</div>
								</div>
							</div>
						<?php } ?>
					</div>        
				</div>
        </div>        
    </div>
 </div>