<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Ekstrakulikuler Siswa</h2>
		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Ekstrakulikuler</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1; ?>
							<?php foreach($eks as $e){ ?>
								<tr>
									<td class="center" width="100px"><?php echo $i; $i++; ?></td>
									<td><?php echo $e['nama_ekstra_kurikuler'] ?></td>
								</tr>
							<?php } ?>						
						</tbody>
					</table>
					
					<div class="footer">
						
					</div>
				</div>
			</div>
		</div>        
	</div>
</div>