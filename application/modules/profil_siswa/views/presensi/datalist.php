<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Prestasi Siswa</h2>
		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Mata Pelajaran</th>
								<th>Hadir</th>
								<th>Tanpa Keterangan</th>
								<th>Sakit</th>
								<th>Ijin</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1; ?>
							<?php foreach($result as $pel => $p){ ?>
								<tr>
									<td class="center" width="100px"><?php echo $i; $i++; ?></td>
									<td><?php echo $pel; ?></td>
									<td class="center" width="100px"><?php echo isset($p['Hadir']) ? $p['Hadir'] : 0; ?></td>
									<td class="center" width="100px"><?php echo isset($p['Tanpa Keterangan']) ? $p['Tanpa Keterangan'] : 0; ?></td>
									<td class="center" width="100px"><?php echo isset($p['Sakit']) ? $p['Sakit'] : 0; ?></td>
									<td class="center" width="100px"><?php echo isset($p['Izin']) ? $p['Izin'] : 0; ?></td>
								</tr>
							<?php } ?>						
						</tbody>
					</table>
					
					<div class="footer">
						
					</div>
				</div>
			</div>
		</div>        
	</div>
</div>