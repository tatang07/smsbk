<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Prestasi Siswa</h2>
		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Prestasi</th>
								<th>Kategori</th>
								<th>Perlombaan</th>
								<th>Tingkat</th>
								<th>Tanggal</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1; ?>
							<?php foreach($prestasi as $p){ ?>
								<tr>
									<td class="center" width="100px"><?php echo $i; $i++; ?></td>
									<td><?php echo $p['prestasi'] ?></td>
									<td><?php echo $p['kategori'] ?></td>
									<td><?php echo $p['perlombaan'] ?></td>
									<td><?php echo $p['tingkat_wilayah'] ?></td>
									<td><?php echo tanggal_indonesia($p['tanggal']) ?></td>
								</tr>
							<?php } ?>						
						</tbody>
					</table>
					
					<div class="footer">
						
					</div>
				</div>
			</div>
		</div>        
	</div>
</div>