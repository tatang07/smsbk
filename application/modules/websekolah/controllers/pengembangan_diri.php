<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class pengembangan_diri extends Simple_Controller {
    	
		
		public function detail($nama_sekolah="",$id3){
			
			$this->load->model('m_websekolah');
			$sekolah=get_sekolah_by_name($nama_sekolah);	
			$id=$sekolah['id_sekolah'];	
			$id2=$sekolah['id_tahun_ajaran_aktif'];	
			
			$data['sekolah']=$this->m_websekolah->select_data_sekolah($id)->result_array();
			$data['web_sekolah']=$this->m_websekolah->get_web_sekolah($id)->result_array();
			// echo $id;
			$data['pengembangan_diri']=$this->m_websekolah->get_pengembangan_diri_by_id($id3)->result_array();
			//print_r($data['pengembangan_diri']);
			render("websekolah/pengembangan_diri",$data,"web");
		}
		
		
}
