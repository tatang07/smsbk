<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class web_sekolah extends Simple_Controller {
    	
		public function index($nama_sekolah=""){
			
			$this->load->model('m_websekolah');
			$sekolah=get_sekolah_by_name($nama_sekolah);	
			
			$id=$sekolah['id_sekolah'];	
			$id2=$sekolah['id_tahun_ajaran_aktif'];	
			
			$data['sekolah']=$this->m_websekolah->select_data_sekolah($id)->result_array();
			// echo get_id_tahun_ajaran();
			
			$data['siswa']=$this->m_websekolah->get_all_siswa($id,$id2);
			$data['rombel']=$this->m_websekolah->get_all_rombel($id,$id2);
			$data['ptk']=$this->m_websekolah->get_all_ptk($id);
			$data['web_sekolah']=$this->m_websekolah->get_web_sekolah($id)->result_array();
			$data['berita']=$this->m_websekolah->get_berita($id)->result_array();
			$data['prestasi']=$this->m_websekolah->get_prestasi($id)->result_array();
			$data['ekskul']=$this->m_websekolah->get_ekskul($id)->result_array();
			$data['pengembangan_diri']=$this->m_websekolah->get_pengembangan_diri($id)->result_array();
			// $data['component'] = "pendidik_dan_tenaga_kependidikan";
			
			render("websekolah/home",$data,"web");
		}
		

		
		
}
