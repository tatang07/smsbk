	<?php// print_r($sekolah)?>
	<?php //echo "hahaha"?>
	<?php foreach($sekolah as $s):?>
	<?php foreach($web_sekolah as $ws):?>
	
	<header id="header">
		<div class="header_top">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								
								<li><a href=""><i class="fa "></i> <img width="50px" src='<?php echo base_url('extras/websekolah/logo.png'); ?>' alt="" /></a></li>
								
								<li><a href=""><i class="fa fa-phone"></i><?php echo $s['telepon']?></a></li>
								<li><a href=""><i class="fa fa-envelope"></i><?php echo $s['email']?></a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href=""><i class="fa fa-jabar"><img width="20px" src='<?php echo base_url('extras/websekolah/jabar.png'); ?>' /></i></a></li>
								<li><a href=""><i class="fa fa-upi"><img width="20px" src='<?php echo base_url('extras/websekolah/upi.png'); ?>' /></i></a></li>
								
							
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="header-bottom">
			<div class="container">
				<div class="row">
					<div class="col-md-1 col-xs-12 text-center">
						<img style="padding-top:20px" width="70px" src='<?php echo base_url('extras/sekolah/'.$s['logo']) ?>'/>
					</div>
					<div class="col-xs-11 col-xs-12-center">
						<h2><?php echo $s['sekolah']?></h2>
						<h5><?php echo $s['alamat']?> | <?php echo $s['kode_pos']?></h5>
					</div>
				</div>
		
				<div class="row">
					
				</div>
			</div>
		</div>
	</header>
	

	<section id="slider"><!--slider-->
		<div class="container ">
			
			<!-- Static navbar -->
			  <nav class="navbar navbar-smsbk">
				<div class="container-fluid">
				  <div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					  <span class="sr-only">Toggle navigation</span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"></a>
				  </div>
				  <div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
					  <li class="active"><a href="#">Beranda</a></li>
					  <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu Sekolah<span class="caret"></span></a>
						<ul class="dropdown-menu">
						  <li><a href="#">Visi dan Misi</a></li>
						  <li><a href="#">Struktur Organisasi Sekolah</a></li>
						  <li><a href="#">Sarana dan Prasarana</a></li>
						
						</ul>
					  </li>
					 <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kesiswaan<span class="caret"></span></a>
						<ul class="dropdown-menu">
						  <li><a href="#">Ekstrakurikuler</a></li>
						  <li><a href="#">Pengembangan Diri</a></li>
						
						</ul>
					  </li>
					  <li><a href="#">Kontak</a></li>
					</ul>
					
				  </div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			  </nav>

		</div>
	  
		
	</section><!--/slider-->

<div class="features-grids text-center">
	<div class="container">
	    <div class="row">
			<div class="col-md-12  ">
			<?php //print_r($berita)?>
				<?php foreach($pengembangan_diri as $b):?>
					
					<strong><h3 style="text-align:left;margin-top:-35px;" ><?php echo $b['nama_ekstra_kurikuler']?> <br/></h3></strong>
					<h6 style="text-align:left;"><i class="fa fa-pencil"></i> Pelatih Oleh : <?php echo $b['pelatih']?> </h6> 
					<img width="200px" style="float:left;margin-bottom:20px" src="<?php echo base_url("extras/ekstra_kurikuler/$b[foto]")?>">
					
					<p style=" margin-left:220px;text-align:justify"><?php echo $b['tentang']?></p>
					<br/>
					<br/>
				<?php endforeach?>
			</div>
		</div>
	</div>
</div>


	<div class="container">
		<div class="row text-center">
			
	   </div>
		
   </div>
	<footer id="footer">
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-md-6 ">
						<h2 class="slide_text">Tentang <?php echo $s['sekolah'] ?> </h2>
						<p class="slide_text"> <?php echo $ws['tentang_sekolah']?><br/>
					</div>
					<div class="col-sm-1">
						
					</div>
					<div class="col-md-5 ">
						<h4 class="slide_text">Bekerja sama dengan:</h4>
						<img width="50px" src='<?php echo base_url('extras/websekolah/jabar.png'); ?>'/>
						<img style="padding-left:20px"width="70px" src='<?php echo base_url('extras/websekolah/upi.png'); ?>'"/>
						<h4 class="slide_text">Kontak Kami :</h4>
						<div class="slide_text">No telp : +62-022-204404<br/>
						Alamat : UPI. Jl Dr. Setiabudi No. 229 Bandung 40154
						</div>
					</div>	
		
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p style="color:#fff" class="pull-left">Copyright 2015. http://smsbk.org/ </p>
				
				</div>
			</div>
		</div>
		
	</footer>
<?php endforeach?>
<?php endforeach?>
<?php 
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
}
?>
    <!--<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.magnify.js"></script>-->
