	<?php //print_r($sekolah)?>
	<?php foreach($sekolah as $s):?>

	<?php foreach($web_sekolah as $ws):?>
	
	<header id="header">
		<div class="header_top">
			<div class="container">
				<div class="row">
					<div class="col-sm-6">
						<div class="contactinfo">
							<ul class="nav nav-pills">
								
								<li><a href=""><i class="fa "></i> <img width="50px" src='<?php echo base_url('extras/websekolah/logo.png'); ?>' alt="" /></a></li>
								
								<li><a href=""><i class="fa fa-phone"></i><?php echo $s['telepon']?></a></li>
								<li><a href=""><i class="fa fa-envelope"></i><?php echo $s['email']?></a></li>
							</ul>
						</div>
					</div>
					<div class="col-sm-6">
						<div class="social-icons pull-right">
							<ul class="nav navbar-nav">
								<li><a href=""><i class="fa fa-jabar"><img width="20px" src='<?php echo base_url('extras/websekolah/jabar.png'); ?>' /></i></a></li>
								<li><a href=""><i class="fa fa-upi"><img width="20px" src='<?php echo base_url('extras/websekolah/upi.png'); ?>' /></i></a></li>
								
							
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	
		<div class="header-bottom">
			<div class="container">
				<div class="row">
					<div class="col-md-1 col-xs-12 text-center">
						<img style="padding-top:20px" width="70px" src='<?php echo base_url('extras/sekolah/'.$s['logo']) ?>'/>
					</div>
					<div class="col-xs-11 col-xs-12-center">
						<h2><?php echo $s['sekolah']?></h2>
						<h5><?php echo $s['alamat']?> | <?php echo $s['kode_pos']?></h5>
					</div>
				</div>
		
				<div class="row">
					
				</div>
			</div>
		</div>
	</header>
	

	<section id="slider"><!--slider-->
		<div class="container ">
			
			<!-- Static navbar -->
			  <nav class="navbar navbar-smsbk">
				<div class="container-fluid">
				  <div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					  <span class="sr-only">Toggle navigation</span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					  <span class="icon-bar"></span>
					</button>
					<a class="navbar-brand" href="#"></a>
				  </div>
				  <div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav">
					  <li class="active"><a href="#">Beranda</a></li>
					  <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Menu Sekolah<span class="caret"></span></a>
						<ul class="dropdown-menu">
						  <li><a href="#">Visi dan Misi</a></li>
						  <li><a href="#">Struktur Organisasi Sekolah</a></li>
						  <li><a href="#">Sarana dan Prasarana</a></li>
						
						</ul>
					  </li>
					 <li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Kesiswaan<span class="caret"></span></a>
						<ul class="dropdown-menu">
						  <li><a href="#">Ekstrakurikuler</a></li>
						  <li><a href="#">Pengembangan Diri</a></li>
						
						</ul>
					  </li>
					  <li><a href="#">Kontak</a></li>
					</ul>
					
				  </div><!--/.nav-collapse -->
				</div><!--/.container-fluid -->
			  </nav>

		</div>
	  
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner" style="background:url(<?php echo base_url("extras/banner_sekolah/$ws[banner]") ?>); height:400px">
							<div class="item active" >
								<div class="col-sm-5">
									
									<h2 style="color:#fff; padding-top:100px">Selamat Datang Di <?php echo $s['sekolah']?></h2>
									<p style="color:#fff;">Sistem Manajemen Sekolah Berbasis Keunggulan Berbasis ICT yang dapat melakukan pengolahan data secara baik dan cepat</p><br>
									<a href="http://smsbk.org/sistem/login"type="button" class="btn btn-primary">LOGIN</a>
								</div>
								<div class="col-sm-3">
									
							
								</div>
							</div>
							<div class="item">
								<div class="col-sm-5 ">
									
									<h2 style="color:#fff; padding-top:100px">Selamat Datang Di <?php echo $s['sekolah']?></h2>
									<p style="color:#fff;">Sistem Manajemen Sekolah Berbasis Keunggulan Berbasis ICT yang dapat melakukan pengolahan data secara baik dan cepat</p><br>
									<a href="http://smsbk.org/sistem/login"type="button" class="btn btn-primary">LOGIN</a>
								</div>
								<div class="col-sm-3">
									
								</div>
							</div>
							
							<div class="item">
								<div class="col-sm-5 ">
								
									<h2 style="color:#fff; padding-top:100px">Selamat Datang Di <?php echo $s['sekolah']?></h2>
									<p style="color:#fff;">Sistem Manajemen Sekolah Berbasis Keunggulan Berbasis ICT yang dapat melakukan pengolahan data secara baik dan cepat</p><br>
									<a href="http://smsbk.org/sistem/login"type="button" class="btn btn-primary">LOGIN</a>
								</div>
								<div class="col-sm-3">
									
								</div>
							</div>
							
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->

<div class="features-grids text-center">
	<div class="container">
	    <div class="row">
			<div class="col-md-3 ">
				<img width="100%" src='<?php echo base_url('extras/sekolah/'.$s['foto']) ?>'/>
			</div>
			<div style="text-align:left" class="col-md-9 ">
				<div class="table-responsive">
				<table class="table">
				
					<tr>
						<td width="120px">
						Nama Sekolah 
						</td>
						<td width="20px">
							:
						</td>
						<td>
						<?php echo $s['sekolah']?>
						</td>
					</tr>
					<tr>
						<td>
						NPSN
						</td>
						<td>
							:
						</td>
						<td>
						<?php echo $s['nps']?>
						</td>
					</tr>
					<tr>
						<td>
						Alamat 
						</td>
						<td>
							:
						</td>
						<td>
						<?php echo $s['alamat']?>
					</tr>
					<tr>
						<td>
						No Telp 
						</td>
						<td>
							:
						</td>
						<td>
						<?php echo $s['telepon']?>
						</td>
					</tr>
					<tr>
						<td>
						Email 
						</td>
						<td>
							:
						</td>
						<td>
						<?php echo $s['email']?>
						</td>
					</tr>
					<tr>
						<td>
						Website 
						</td>
						<td>
							:
						</td>
						<td>
						<?php echo $s['website']?>
						</td>
					</tr>
				</table>
				</div>
			</div>
	   </div>

	   <div class="row">
		<div class="col-md-9">
			<h2 class="bg_judul ">Berita</h2>
		</div>
	   </div>
	   <div class="row">
		<div style="text-align:left" class="col-md-9">
				<?php $byk_brt=0; ?>
				<?php foreach($berita as $br):?>
				<?php if($byk_brt < 6){ $byk_brt++;?>
				<div class="col-md-6 spliter_bot">
					<table>
						<tr>
							<td colspan="2" class="padding_judul"><?php echo $br['judul_berita']; ?></td>
						</tr>
						<tr class="padding_isi">
							<td valign="top" class="td-padd"><img style="width:100%" src="<?php echo base_url("extras/berita/$br[foto]"); ?>" /></td>
							<td class="padding_text">
							<strong class="tgl">Tanggal Postingan : <?php echo $br['tanggal']?> </strong><br/>
							<?php custom_echo($br['berita'] , 200); ?><a class="pull-right green-a" href="<?php echo site_url("$s[nama_strip]/berita/detail/$br[id_berita]")?>">Selengkapnya >></a>
							</td>
						</tr>
							
					</table>
				</div>
				<?php }?>
				<?php endforeach; ?>
				
		</div>
		
		<div style="text-align:left" class="col-md-3 features-grid">
				<div class="left-sidebar">
						<h2>Komponen</h2>
						<div class="panel-group category-products" id="accordian"><!--category-productsr-->
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"> <a href="#">Info Pendaftaran Siswa</a></h4>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#kurikulum">
											<span class="badge pull-right"><i class="fa fa-caret-down"></i></span>
											Kurikulum
										</a>
									</h4>
								</div>
								<div id="kurikulum" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#">Program </a></li>
											<li><a href="#">Ekstrakurikuler </a></li>
											<li><a href="#">Pengembangan Diri </a></li>
											<li><a href="#">Ketersediaan Silabus</a></li>
											<li><a href="#">Ketersediaan RPP </a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">
										<a data-toggle="collapse" data-parent="#accordian" href="#mens">
											<span class="badge pull-right"><i class="fa fa-caret-down"></i></span>
											PELAYANAN SISWA
										</a>
									</h4>
								</div>
								<div id="mens" class="panel-collapse collapse">
									<div class="panel-body">
										<ul>
											<li><a href="#">Proporsi Gender</a></li>
											<li><a href="#">Proporsi Agama</a></li>
											<li><a href="#">Keadaan Peserta Didik</a></li>
											<li><a href="#">Prestasi</a></li>		
										</ul>
									</div>
								</div>
							</div>		
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title"><a href="#">Info Pendaftaran Siswa</a></h4>
								</div>
							</div>
						</div><!--/category-products-->
					</div>
					
						<div class="menu_left spliter_bot" >
							
							Jumlah Siswa : <input style="width:100%;text-align:center;" type="text" disabled value="<?php echo $siswa['jumlah']; ?>"/><br/>
							Total Rombongan Belajar : <input style="width:100%;text-align:center;" width="100%"type="text" disabled value="<?php echo $rombel['jumlah']; ?>"/><br/>
							Total PTK : <input style="width:100%;text-align:center;" width="100%"type="text" disabled value="<?php echo $ptk['jumlah']; ?>"/><br/>
						</div>		
			</div>	
	   </div>
		
		<div class="row">
			<div class="col-md-12">
				<h2 class="bg_judul">Prestasi</h2>
			<?php $byk_pres=0;?>
			<?php foreach ($prestasi as $p):?>
			<?php if($byk_pres<3){ $byk_pres++;?>
			<div class="col-md-4">
				<table>
						<tr>
							<td colspan="2" class="padding_judul"><?php echo $p['prestasi']?></td>
						</tr>
						<tr class="padding_isi">
							<td valign="top" class="td-padd"><img style="width:100%" src='<?php echo base_url("extras/prestasi/$p[foto]"); ?>' /></td>
							<td class="padding_text">
							<strong class="tgl">Tanggal : <?php echo $p['tanggal']?></strong><br/>
							<?php custom_echo($p['keterangan'] , 200); ?> <a class="pull-right green-a" href="<?php echo site_url("$s[nama_strip]/prestasi/detail/$p[id_prestasi_siswa]")?>">Selengkapnya >></a>
							</td>
						</tr>
							
					</table>
			</div>
			<?php }?>
			<?php endforeach;?>
			</div>
		</div>
		
		<div class="row">
		<div style="text-align:left" class="col-md-6">
			<h2 class="bg_judul">Pengembangan Diri</h2>
				<?php $byk_pengembangan_diri=0?>
				<?php foreach($pengembangan_diri as $pd):?>
				<?php if($byk_pengembangan_diri < 2){ $byk_pengembangan_diri++;?>
				<div class="col-md-12 spliter_bot">
					<table>
						<tr>
							<td colspan="2" class="padding_judul"><?php echo $pd['nama_ekstra_kurikuler']?></td>
						</tr>
						<tr class="padding_isi">
							<td valign="top" class="td-padd"><img style="width:100%" src='<?php echo base_url("extras/pengembangan_diri/$pd[lokasi_file_lambang]"); ?>' /></td>
							<td class="padding_text">
							<strong class="tgl">Pelatih :<?php echo $pd['pelatih']?> </strong><br/>
							<?php custom_echo($pd['tentang'] , 200); ?><a class="pull-right green-a" href="<?php echo site_url("$s[nama_strip]/pengembangan_diri/detail/$pd[id_ekstra_kurikuler]")?>">Selengkapnya >></a>
							</td>
						</tr>
							
					</table>
				</div>
				<?php } ?>
				<?php endforeach?>
				
		</div>
		
		<div style="text-align:left" class="col-md-6">
			<h2 class="bg_judul">Ekstrakurikuler</h2>
			<?php $byk_ekskul=0; ?>
				<?php foreach($ekskul as $e):?>
				<?php if($byk_ekskul < 2){ $byk_ekskul++;?>
				<div class="col-md-12 spliter_bot">
					<table>
						<tr>
							<td colspan="2" class="padding_judul"><?php echo $e['nama_ekstra_kurikuler']?></td>
						</tr>
						<tr class="padding_isi">
							<td valign="top" class="td-padd"><img style="width:100%" src='<?php echo base_url("extras/ekstra_kurikuler/$e[lokasi_file_lambang]"); ?>' /></td>
							<td class="padding_text">
							<strong class="tgl">Pelatih : <?php echo $e['pelatih']?> </strong><br/>
							<?php custom_echo($e['tentang'] , 200); ?><a class="pull-right green-a" href="<?php echo site_url("$s[nama_strip]/ekskul/detail/$pd[id_ekstra_kurikuler]")?>">>Selengkapnya >></a>
							</td>
						</tr>
							
					</table>
				</div>
				<?php }?>
				<?php endforeach;?>
				
		</div>
		</div>
		
		</div>
   </div>


	<div class="container">
		<div class="row text-center">
			
	   </div>
		
   </div>
	<footer id="footer">
		<div class="footer-top">
			<div class="container">
				<div class="row">
					<div class="col-md-6 ">
						<h2 class="slide_text">Tentang <?php echo $s['sekolah'] ?> </h2>
						<p class="slide_text"> <?php echo $ws['tentang_sekolah']?><br/>
					</div>
					<div class="col-sm-1">
						
					</div>
					<div class="col-md-5 ">
						<h4 class="slide_text">Bekerja sama dengan:</h4>
						<img width="50px" src='<?php echo base_url('extras/websekolah/jabar.png'); ?>'/>
						<img style="padding-left:20px"width="70px" src='<?php echo base_url('extras/websekolah/upi.png'); ?>'"/>
						<h4 class="slide_text">Kontak Kami :</h4>
						<div class="slide_text">No telp : +62-022-204404<br/>
						Alamat : UPI. Jl Dr. Setiabudi No. 229 Bandung 40154
						</div>
					</div>	
		
				</div>
			</div>
		</div>
		
		<div class="footer-bottom">
			<div class="container">
				<div class="row">
					<p style="color:#fff" class="pull-left">Copyright 2015. http://smsbk.org/ </p>
				
				</div>
			</div>
		</div>
		
	</footer>
<?php endforeach?>
<?php endforeach?>
<?php 
function custom_echo($x, $length)
{
  if(strlen($x)<=$length)
  {
    echo $x;
  }
  else
  {
    $y=substr($x,0,$length) . '...';
    echo $y;
  }
}
?>
    <!--<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.magnify.js"></script>-->
