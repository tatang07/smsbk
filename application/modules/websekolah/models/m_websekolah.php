<?php
class m_websekolah extends MY_Model {

	function select_data_sekolah($id){
		$this->db->select('*');
		$this->db->from('m_sekolah');
		$this->db->where('id_sekolah',$id);
		return $this->db->get();
	}
	
	function get_web_sekolah($id){
		$this->db->select('*');
		$this->db->from('m_web_sekolah');
		$this->db->where('id_sekolah',$id);
		return $this->db->get();
	}
	
	function get_berita($id){
		$this->db->select('b.foto,g.id_guru,b.author,g.id_sekolah,b.berita,b.id_berita,b.tanggal,b.judul_berita');
		$this->db->from('m_berita b');
		$this->db->join('m_guru g', 'g.id_guru = b.author');
		$this->db->where('g.id_sekolah',$id);
		return $this->db->get();
	}
	
	function get_berita_by_id($id){
		$this->db->select('b.foto,g.id_guru,b.author,g.nama,g.id_sekolah,b.berita,b.tanggal,b.judul_berita');
		$this->db->from('m_berita b');
		$this->db->join('m_guru g', 'g.id_guru = b.author');
		$this->db->where('b.id_berita',$id);
		return $this->db->get();
	}
	
	function get_prestasi($id){
		$this->db->select('ps.foto,ps.tanggal,ps.prestasi,ps.id_prestasi_siswa,ps.perlombaan,ps.keterangan');
		$this->db->from('t_prestasi_siswa ps');
		$this->db->join('m_siswa s', 's.id_siswa = ps.id_siswa');
		$this->db->where('s.id_sekolah',$id);
		return $this->db->get();
	}
	
	function get_prestasi_by_id($id){
		$this->db->select('ps.foto,ps.tanggal,ps.prestasi,ps.perlombaan,ps.keterangan');
		$this->db->from('t_prestasi_siswa ps');
		$this->db->join('m_siswa s', 's.id_siswa = ps.id_siswa');
		$this->db->where('ps.id_prestasi_siswa',$id);
		return $this->db->get();
	}
	
	function get_ekskul($id){
		$this->db->select('*');
		$this->db->from('m_ekstra_kurikuler ek');
		$this->db->where('id_sekolah',$id);
		$this->db->where('jenis',1);
		return $this->db->get();
	}
	
	function get_ekskul_by_id($id){
		$this->db->select('*');
		$this->db->from('m_ekstra_kurikuler ek');
		$this->db->where('id_ekstra_kurikuler',$id);
		$this->db->where('jenis',1);
		return $this->db->get();
	}
	
	
	function get_pengembangan_diri($id){
		$this->db->select('*');
		$this->db->from('m_ekstra_kurikuler ek');
		$this->db->where('id_sekolah',$id);
		$this->db->where('jenis',2);
		return $this->db->get();
	}
	
	function get_pengembangan_diri_by_id($id){
		$this->db->select('*');
		$this->db->from('m_ekstra_kurikuler ek');
		$this->db->where('id_ekstra_kurikuler',$id);
		$this->db->where('jenis',2);
		return $this->db->get();
	}
	
	public function get_all_siswa($id,$id2){
			
			
			$query = $this->db->query("SELECT count(id_siswa) as jumlah FROM v_siswa WHERE id_sekolah='$id' and id_tahun_ajaran='$id2'");
			return $query->row_array();
	}
		
		public function get_all_rombel($id,$id2){
		
			$query = $this->db->query("SELECT count(id_rombel) as jumlah FROM m_rombel WHERE id_sekolah='$id' and id_tahun_ajaran='$id2'");
			return $query->row_array();
	}
		
		public function get_all_ptk($id){
		
			$query = $this->db->query("SELECT count(id_guru) as jumlah FROM m_guru WHERE id_sekolah='$id'");
			return $query->row_array();
		}
}
