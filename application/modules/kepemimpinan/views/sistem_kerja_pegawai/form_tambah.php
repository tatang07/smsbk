<h1 class="grid_12">Kepemimpinan</h1>
  	<form action="<?php echo base_url('kepemimpinan/sistem_kerja_pegawai/simpan'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
		<fieldset>
			<legend>Tambah Sistem Kerja Pegawai</legend>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Periode Mulai</strong>
				</label>
				<div>
					<input type="date" name="tgl_mulai" value="" required/>
				</div>
			</div>

			<div class="row">
				<label for="f1_normal_input">
					<strong>Periode Akhir</strong>
				</label>
				<div>
					<input type="date" name="tgl_akhir" value="" required/>
				</div>
			</div>

			<div class="row">
				<label for="f1_textarea">
					<strong>File</strong>
				</label>
				<div>
					<input type="file" name="userfile" value="" required/>
				</div>
			</div>

			<input type="hidden" name="id_kepala_sekolah" value="<?php echo $id_kepala_sekolah ?>">
		</fieldset>
		
		<div class="actions">
			<div class="left">
				<input type="submit" value="Simpan" name=send />
				 <a href="<?php echo base_url('kepemimpinan/sistem_kerja_pegawai/index/'); ?>"> <input value="Batal" type="button"></a>
			</div>
			<div class="right">
			</div>
		</div><!-- End of .actions -->
	</form><!-- End of .box -->
</div><!-- End of .grid_4 -->
