<?php  
	$statuspil = 0;
	if(isset($_POST['id_kepala_sekolah'])){
	$a = $_POST['id_kepala_sekolah'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Sistem Kerja Pegawai</h2>
       </div>
	
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('kepemimpinan/sistem_kerja_pegawai/load_add/'.$statuspil); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="110px" align="left"><span class="text">Kepala Sekolah</span></th>
								<td width="200px" align="left">
								<select name="id_kepala_sekolah" id="id_kepala_sekolah" onchange="submitform();">
									<option value="0">-Nama Kepala Sekolah-</option>
									<?php foreach($kepsek as $q): ?>
										<?php if($q['id_kepala_sekolah'] == $statuspil){ ?>
											<option selected value='<?php echo $q['id_kepala_sekolah']; ?>' data-status-pilihan="<?php echo $q['id_kepala_sekolah']; ?>"><?php echo $q['nama']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q['id_kepala_sekolah']; ?>' data-status-pilihan="<?php echo $q['id_kepala_sekolah']; ?>"><?php echo $q['nama']; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
								</td>
								
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Periode Penilaian Penilai</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
							<?php 
								$this->load->model('kepemimpinan/m_sistem_kerja_pegawai');
								$data['isi'] = $this->m_sistem_kerja_pegawai->get_data($statuspil)->result_array();
								$isi=$data['isi'];
							?>
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
								<?php //print_r($isi)?>
								<tr>
									<td class='center'><?php echo $no; $no++;?></td>
									<td class='center'><?php  echo tanggal_indonesia($d['tgl_mulai'])?> - <?php  echo tanggal_indonesia($d['tgl_akhir'])?> </td>
									<td class='center'>
										<a href="<?php echo base_url('kepemimpinan/sistem_kerja_pegawai/download/'.$d['id_kepsek_skp'])?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-print"></i> Download</a>
										<a href="<?php echo site_url('kepemimpinan/sistem_kerja_pegawai/delete/'.$d['id_kepsek_skp'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>