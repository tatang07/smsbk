<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Kebijakan Sekolah</h2>
		</div>
		<div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('kepemimpinan/kebijakan_sekolah/add/?id='.$id_tahun_ajaran); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('kepemimpinan/kebijakan_sekolah/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=80px align='left'><span class="text">Tahun Ajaran &nbsp:</span></th>
							<td width=100px align='left'>
								<select name="id_tahun_ajaran">
								<?php if($stat == 'home'){ ?>
									<?php foreach($tahun_ajaran as $r): ?>
										<?php if($r->id_tahun_ajaran == $first_tahun_ajaran ){ ?>
											<option selected value='<?php echo $r->id_tahun_ajaran; ?>'><?php echo $r->tahun_awal." / ".$r->tahun_akhir; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $r->id_tahun_ajaran; ?>'><?php echo $r->tahun_awal." / ".$r->tahun_akhir; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								<?php }else{ ?>
									<?php foreach($tahun_ajaran as $r): ?>
										<?php if($r->id_tahun_ajaran == $id_tahun_ajaran ){ ?>
											<option selected value='<?php echo $r->id_tahun_ajaran; ?>'><?php echo $r->tahun_awal." / ".$r->tahun_akhir; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $r->id_tahun_ajaran; ?>'><?php echo $r->tahun_awal." / ".$r->tahun_akhir; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								<?php }?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
							<input type="submit" name=send class="button grey tooltip" value="cari" />
							<!-- <a href="search" class="button grey tooltip"><i class='icon-search'></i></a> -->
							</td>
						</tr>
					</table>
				</form>
			</div>

		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>No Surat</th>
								<th>Perihal</th>
								<th>Keterangan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
						
							<?php $no = $page + 1; ?>
							<?php if($list){ ?>
								<?php foreach($list as $l): ?>
									
										<tr>
											<td width="20px" class="center"><?php echo $no; ?></td>
											<td width="150px" class="center"><?php echo $l->no_surat; ?></td>
											<td width="" class="center"><?php echo $l->perihal; ?></td>
											<td width="" class="right"><?php echo $l->keterangan; ?></td>
											<td width="150px" class="center">
												<a href="<?php echo base_url('kepemimpinan/kebijakan_sekolah/download/'.$l->id_sekolah_kebijakan); ?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-print"></i> Download</a>
												<a href="<?php echo base_url('kepemimpinan/kebijakan_sekolah/delete/'.$l->id_sekolah_kebijakan); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
										<?php $no++; ?>
								<?php endforeach; ?>
							
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $jumlah; ?></div>
						<div class="dataTables_paginate paging_full_numbers">
							<p><?php if($links){echo $links; } ?></p>
						</div>
					</div>
							<?php }else{ ?>
								</tbody>
								</table>
								<div class="footer">Data tidak ditemukan!
								</div>
							<?php } ?>
				</div>
			</div>
		</div>        
	</div>
</div>