	<h1 class="grid_12">Kepemimpinan</h1>
			
			<form action="<?php echo base_url('kepemimpinan/kebijakan_sekolah/submit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Kebijakan Sekolah</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nomor Surat</strong>
						</label>
						<div>
							<input type="text" name="nomor_surat" value="" id="nomor_surat"/>
							<input type="hidden" name="action" value="add" />
							<input type="hidden" name="id_tahun_ajaran" value="<?php echo $_GET['id'];?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Perihal</strong>
						</label>
						<div>
							<input type="text" name="perihal" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>File</strong>
						</label>
						<div>
							<input type="file" name="alamatfile" value="" />
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kepemimpinan/kebijakan_sekolah/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->

 <script>
 	$(function(){
		$(document).on('blur', '#nomor_surat', function(){
			var nomor_surat = $(this).val();
			$.post("<?php echo site_url('kepemimpinan/kebijakan_sekolah/cek_nomor_surat'); ?>/", {nmr:nomor_surat}, function(data){
				//alert(data);
				if(data > 0){
					alert("Nomer surat sudah digunakan !!");
					$('#nomor_surat').val("").focus();
				}
			});
		});
	});
 </script>