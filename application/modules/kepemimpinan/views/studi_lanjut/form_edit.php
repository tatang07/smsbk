	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			
			<form action="<?php echo base_url('kepemimpinan/studi_lanjut/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Studi Lanjut</legend><?php //echo($tingkat_kelas);?>
					<?php //print_r($data_edit); ?>
					<?php foreach($data_edit as $d):?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama</strong>
						</label>
						<div>
					
							
							<input disabled type="text" name="nama" value="<?php echo $d['nama']?>" />
							<input disabled type="hidden" name="id_guru" value="<?php echo $d['id_kepsek']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tahun Masuk</strong>
						</label>
						<div>
							<input type="text" name="tahun_masuk" value="<?php echo $d['tahun_masuk']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tahun Lulus</strong>
						</label>
						<div>
							<input type="text" name="tahun_lulus" value="<?php echo $d['tahun_keluar']?>" />
							<input type="hidden" name="id" value="<?php echo $d['id_kepsek_studi_lanjut']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama Kampus</strong>
						</label>
						<div>
							<input type="text" name="nama_kampus" value="<?php echo $d['nama_kampus']?>" />
							
						</div>
					</div>

					<div class="row">
						<label for="f1_normal_input">
							<strong>Status</strong>
						</label>
						<div><?php $statuspil=$d['status']?>
							<select name="status" id="status" >
									<option value="0">-Status-</option>
									<?php if($statuspil==1){?>
									<option selected value="1">Lulus</option>
									<?php }else {?>
									<option value="1">Lulus</option>
									<?php } ?>
									<?php if($statuspil==2){?>
									<option selected value="2">Belum Lulus</option>
									<?php }else {?>
									<option value="2">Belum Lulus</option>
									<?php } ?>
									<?php if($statuspil==3){?>
									<option selected value="3">Gagal</option>
									<?php }else {?>
									<option value="3">Gagal</option>
									<?php } ?>
									</select> 
						</div>
					</div>
				<?php endforeach?>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kepemimpinan/studi_lanjut/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		