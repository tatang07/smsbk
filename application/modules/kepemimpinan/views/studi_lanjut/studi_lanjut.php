<?php  
	$statuspil = 0;
	if(isset($_POST['status'])){
	$a = $_POST['status'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar Studi Lanjut</h2>
			<?php //echo $statuspil ?>
       </div>
	
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('kepemimpinan/studi_lanjut/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('kepemimpinan/studi_lanjut/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="40px" align="left"><span class="text">Nama</span></th>
								<td align="left"><input type="text" name="nama" id="nama"></td>
								<td width="10px"></td>
								<th width="110px" align="left"><span class="text">Status</span></th>
								<td width="200px" align="left">
								<select name="status" id="status" onchange="submitform();">
									<option value="0">-Status-</option>
									<?php if($statuspil==1){?>
									<option selected value="1">Lulus</option>
									<?php }else {?>
									<option value="1">Lulus</option>
									<?php } ?>
									<?php if($statuspil==2){?>
									<option selected value="2">Belum Lulus</option>
									<?php }else {?>
									<option value="2">Belum Lulus</option>
									<?php } ?>
									<?php if($statuspil==3){?>
									<option selected value="3">Gagal</option>
									<?php }else {?>
									<option value="3">Gagal</option>
									<?php } ?>

								</select>
								</td>
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >NIP</th>
								<th >Nama</th>
								<th >Tahun Masuk</th>
								<th >Tahun Lulus</th>
								<th >Kampus</th>
								<th >Jenjang</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
						
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
							
								<tr>
									<td class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['nip']?>
									</td class='center'>
									<td class='center'>
									<?php  echo $d['nama'];?>
									</td>
									<td class='center'>
									<?php  echo $d['tahun_masuk'];?>
									</td>
									<td class='center'>
									<?php  echo $d['tahun_keluar'];?>
									</td>	
									<td class='center'>
									<?php  echo $d['nama_kampus'];?>
									</td>
									<td class='center'>
									<?php  echo $d['jenjang_pendidikan'];?>
									</td>
									
									<td class='center'>
									<a href="<?php echo site_url('kepemimpinan/studi_lanjut/load_edit/'.$d['id_kepsek_studi_lanjut'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<a href="<?php echo site_url('kepemimpinan/studi_lanjut/delete/'.$d['id_kepsek_studi_lanjut'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>