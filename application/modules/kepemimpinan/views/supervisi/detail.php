<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
	<div class="box with-table">
		<div class="header">
			<h2>Detail Supervisi</h2>
		</div>

		<table class="styled">
			<tbody>
				<tr>
					<th style="width:100px !important">Kode Supervisi</th>
					<td><?php echo $supervisi['id_supervisi']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Tanggal Terbit</th>
					<td><?php echo date("d F Y", strtotime($supervisi['tanggal_terbit'])); ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Waktu</th>
					<td><?php echo date("d F Y", strtotime($supervisi['waktu'])); ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Pelajaran</th>
					<td><?php echo $supervisi['pelajaran']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Materi</th>
					<td><?php echo $supervisi['materi']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Guru</th>
					<td><?php echo $supervisi['nama']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Deskripsi</th>
					<td><?php echo $supervisi['hasil']; ?></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="box with-table">
		<table class="styled">
			<thead>
				<tr>
					<th style="width:20px !important">No</th>
					<th style="width:300px !important">Butir Supervisi</th>
					<th style="width:50px !important">Hasil</th>
					<th style="width:50px !important">Bobot</th>
					<th>Keterangan</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; foreach ($detail as $value): ?>
				<tr>
					<td><?php echo $i; ?></td>
					<td><?php echo $value['butir_supervisi']; ?></td>
					<td style="text-align:center"><?php echo $value['nilai']; ?></td>
					<td style="text-align:center"><?php echo $value['bobot']; ?></td>
					<td><?php echo $value['keterangan']; ?></td>
				</tr>
				<?php $i++; endforeach; ?>
			</tbody>
		</table>

		<div class="actions">
			<a href="<?php echo base_url('kepemimpinan/supervisi'); ?>" class="button"> Kembali</a>
		</div>
	</div>
</div>
