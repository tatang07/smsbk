<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Tampah Pertemuan</h2>
       </div>
		
		<form action="<?php echo base_url('kepemimpinan/pertemuan/edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>		
				<?php foreach ($pertemuan as $p): ?>			
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama Kegiatan</strong>
						</label>
						<div>
							<input type="text" name="nama_kegiatan" value="<?php echo $p['nama_kegiatan'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal</strong>
						</label>
						<div>
							<input type="date" name="tanggal" value="<?php echo $p['tanggal'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Hasil Pertemuan</strong>
						</label>
						<div>
							<input type="text" name="hasil_pertemuan" value="<?php echo $p['hasil_pertemuan'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tempat</strong>
						</label>
						<div>
							<input type="text" name="tempat" value="<?php echo $p['tempat'] ?>" />
						</div>
					</div>
				<?php endforeach ?>
				<input type="hidden" name="id_sekolah_pertemuan" value="<?php echo $id_sekolah_pertemuan; ?>">		
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('kepemimpinan/pertemuan'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>
