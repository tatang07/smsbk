	<h1 class="grid_12">Kepemimpinan</h1>
			
			<form action="<?php echo base_url('kepemimpinan/program_kerja_tahunan/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Program Kerja Tahun Ajaran <?php foreach($tabi as $t):?> <?php echo $t->tahun_awal.'-'.$t->tahun_akhir; ?> <?php endforeach;?></legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Program Kerja</strong>
						</label>
						<div>
							<input type="text" name="program_kerja" value="" />
							<input type="hidden" name="id_tahun_ajaran" value="<?php echo $id_tahun_ajaran?>" />
						</div>
					</div>
					<?php //print_r($ta)?>
			
					<div class="row">
						<label for="f1_normal_input">
							<strong>Periode Awal</strong>
						</label>
						<div>
							<input type="date" name="periode_awal" value="" />
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Periode Akhir</strong>
						</label>
						<div>
							<input type="date" name="periode_akhir" value="" />
						
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="" />
						</div>
					</div>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kepemimpinan/program_kerja_tahunan/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		
<script>
$(function(){
	$(document).on('change', '#kelompok_pelajaran', function(){
		var id_kelompok_pelajaran = $(this).val();
		$.post("<?php echo site_url('kurikulum/silabus/get_pelajaran'); ?>/" + id_kelompok_pelajaran, function(data){
			$("#pelajaran").html(data);
			$("#pelajaran").trigger("chosen:updated");
		});
	});
});
</script>