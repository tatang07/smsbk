	<h1 class="grid_12">Kepemimpinan</h1>
			<?php //echo ($id)?>
			<form action="<?php echo base_url('kepemimpinan/program_kerja_tahunan/submit_edit_sub'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Program Kerja Tahun Ajaran</legend>
				<?php foreach($data_edit as $t){?>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Program Kerja</strong>
						</label>
						<div>
							<input type="text" name="sub_program_kerja" value="<?php echo $t['sub_program_kerja'];?>" />
							<input type="hidden" name="id" value="<?php echo $id?>" />
							<input type="hidden" name="id2" value="<?php echo $id2?>" />
						</div>
					</div>
			
					<div class="row">
						<label for="f1_normal_input">
							<strong>Periode Awal</strong>
						</label>
						<div>
							<input type="date" name="periode_awal" value="<?php echo tanggal_view($t['tanggal_mulai']);?>" />
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Periode Akhir</strong>
						</label>
						<div>
							<input type="date" name="periode_akhir" value="<?php echo tanggal_view($t['tanggal_akhir']);?>" />
						
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="<?php echo $t['keterangan'];?>" />
						</div>
					</div>
					
				<?php } ?>
				</fieldset>
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('kepemimpinan/program_kerja_tahunan/load_detail/'.$t['id_sekolah_program_kerja']); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		