	<h1 class="grid_12">Kepemimpinan</h1>
			
			<form action="<?php echo base_url('Kepemimpinan/peningkatan_kompetensi/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Peningkatan Kompetensi</legend>
					<?php $jenjang_sekolah = get_jenjang_sekolah(); ?>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama Program</strong>
						</label>
						<div>
							<input type="text" name="nama_program" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Mulai</strong>
						</label>
						<div>
							<input type="date" name="tanggal_mulai" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Akhir</strong>
						</label>
						<div>
							<input type="date" name="tanggal_selesai" value="" />
							
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Penanggung Jawab</strong>
						</label>
						<div>
							<select name="id_kepsek" id="id_kepsek" >
							<option value="">-Nama Kepala Sekolah-</option>
							<?php foreach($kepsek as $d):?>
							
							  <option value="<?php echo $d['id_kepala_sekolah']?>"><?php echo $d['nama']?></option>
							
							 <?php endforeach ?>
							</select> 
						</div>
					</div>
					
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('Kepemimpinan/peningkatan_kompetensi/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		