<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Detail Program : <?php echo $nama_program->nama_program; ?></h2>
       </div>

       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('kepemimpinan/peningkatan_kompetensi/load_add_detail/'.$id_kepsek_peningkatan_kompetensi); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<?php foreach($isi as $d):?>
				<form action="<?php echo base_url('kepemimpinan/peningkatan_kompetensi/search_detail/'.$d['id_kepsek_peningkatan_kompetensi']); ?>" method="post" enctype="multipart/form-data">
				<?php endforeach ?>
					<table>
						<tbody>
							<tr>
								<th width="40px" align="left"><span class="text">Nama</span></th>
								<td align="left"><input type="text" name="nama" id="nama"></td>
								<td width="10px"></td>
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Nip</th>
								<th >Nama</th>							
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
						
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
							
								<tr>
									<td class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['nip']?>
									</td class='center'>
									<td class='center'>
									<?php  echo $d['nama'];?>
									</td>
								
									<td class='center'>
						
									<a href="<?php echo site_url('kepemimpinan/peningkatan_kompetensi/delete_detail/'.$d['id_kepsek_peningkatan_kompetensi'].'/'.$d['id_kepsek_peningkatan_kompetensi_detail'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>