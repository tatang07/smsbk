<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Edit Data Kepala Sekolah</h2>
       </div>
		
		<form action="<?php echo base_url('kepemimpinan/kepala_sekolah/proses_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>
					<div class="row">
						<label for="f1_normal_input">
							<strong>NIP</strong>
						</label>
						<div>
							<input type="text" name="nip" value="<?php echo $detail['nip'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama</strong>
						</label>
						<div>
							<input id="nama" type="text" name="nama" value="<?php echo $detail['nama'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenjang Pendidikan</strong>
						</label>
						<div>
							<select name="id_jenjang_pendidikan">
								<?php foreach($jenjang as $jp): ?>
									<?php if($jp['id_jenjang_pendidikan'] == $detail['id_jenjang_pendidikan']){ ?>
										<option selected value='<?php echo $jp['id_jenjang_pendidikan']; ?>'><?php echo $jp['jenjang_pendidikan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_jenjang_pendidikan']; ?>'><?php echo $jp['jenjang_pendidikan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Pangkat / Golongan</strong>
						</label>
						<div>
							<select name="id_pangkat_golongan">
								<?php foreach($pangkat as $pg): ?>
									<?php if($pg['id_pangkat_golongan'] == $detail['id_pangkat_golongan']){ ?>
										<option selected value='<?php echo $pg['id_pangkat_golongan']; ?>'><?php echo $pg['pangkat']." / ".$pg['golongan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $pg['id_pangkat_golongan']; ?>'><?php echo $pg['pangkat']."/".$pg['golongan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nomor SK</strong>
						</label>
						<div>
							<input type="text" name="no_sk" value="<?php echo $detail['no_sk']; ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal SK</strong>
						</label>
						<div>
							<input type="date" name="tgl_sk" value="<?php echo $detail['tgl_sk']; ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>TMT SK</strong>
						</label>
						<div>
							<input type="date" name="tmt_sk" value="<?php echo $detail['tmt_sk']; ?>" />
						</div>
					</div>
				<input type="hidden" name="id_kepala_sekolah" value="<?php echo $detail['id_kepala_sekolah']; ?>">		
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('kepemimpinan/kepala_sekolah'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>
