<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Tambah Data Kepala Sekolah</h2>
       </div>
		
		<form action="<?php echo base_url('kepemimpinan/kepala_sekolah/proses_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>
					<div class="row">
						<label for="f1_normal_input">
							<strong>NIP</strong>
						</label>
						<div>
							<input type="text" name="nip" value="" id="nip" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama</strong>
						</label>
						<div>
							<input type="text" name="nama" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenjang Pendidikan</strong>
						</label>
						<div>
							<select name="id_jenjang_pendidikan">
								<?php foreach($jenjang as $jp): ?>
									<option value='<?php echo $jp['id_jenjang_pendidikan']; ?>'><?php echo $jp['jenjang_pendidikan']; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Pangkat / Golongan</strong>
						</label>
						<div>
							<select name="id_pangkat_golongan">
								<?php foreach($pangkat as $pg): ?>
									<option value='<?php echo $pg['id_pangkat_golongan']; ?>'><?php echo $pg['pangkat']."/".$pg['golongan']; ?></option>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nomor SK</strong>
						</label>
						<div>
							<input type="text" name="no_sk" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal SK</strong>
						</label>
						<div>
							<input type="date" name="tgl_sk" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>TMT SK</strong>
						</label>
						<div>
							<input type="date" name="tmt_sk" value="" />
						</div>
					</div>
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('kepemimpinan/kepala_sekolah'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>

 <script>
 	$(function(){
		$(document).on('blur', '#nip', function(){
			var nip = $(this).val();
			$.post("<?php echo site_url('kepemimpinan/kepala_sekolah/cek_nip'); ?>/" + nip, function(data){
				if(data > 0){
					alert("NIP sudah digunakan !!");
					$('#nip').val("").focus();
				}
			});
		});
	});
 </script>
