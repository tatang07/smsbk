<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
   	<div class="box">
			
		<div class="header">
			<h2>Identitas Kepala Sekolah</h2>
		</div>
			
			<div class="content">
				<table class="styled" >
					<thead>
						<tr>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width='200px' class='right'>Nama</td>
							<td ><?php echo $kepsek[0]['nama']?></td>
						</tr>
						<tr>
							<td width='' class='right'>NIP</td>
							<td ><?php echo $kepsek[0]['nip']?></td>
						</tr>
						<tr>
							<td width='' class='right'>Pangkat/Golongan</td>
							<td ><?php echo $kepsek[0]['pangkat']."/".$kepsek[0]['golongan']?></td>

						</tr>
						<tr>
							<td width='' class='right'>Pendidikan</td>
							<td ><?php echo $kepsek[0]['jenjang_pendidikan']?></td>

						</tr>
						<tr>
							<td width='' class='right'>Nomor SK Pengangkatan</td>
							<td ><?php echo $kepsek[0]['no_sk']?></td>
						</tr>
						<tr>
							<td width='' class='right'>Tanggal Pengangkatan</td>
							<td ><?php echo tanggal_indonesia($kepsek[0]['tgl_sk'])?></td>
						</tr>
						<tr>
							<td width='' class='right'>TMT</td>
							<td ><?php echo tanggal_indonesia($kepsek[0]['tmt_sk'])?></td>
						</tr>
						<tr>
							<td colspan='2'></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="content">
			<h3>Biodata Sekolah</h3>
			<div>
				<table class="styled" >
					<thead>
						<tr>
							<td colspan='2'></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width='200px' class='right'>Nama Sekolah</td>
							<td ><?php echo ": ".$kepsek[0]['sekolah']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Nomor Statistik Sekolah</td>
							<td ><?php echo ": ".$kepsek[0]['nss']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Nomor Identitas Sekolah</td>
							<td ><?php echo ": ".$kepsek[0]['nps']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Status Sekolah</td>
							<td ><?php echo ": ".$kepsek[0]['status_sekolah']; ?></td>

						</tr>
						<tr>
							<td width='' class='right'>Tanggal Berdiri</td>
							<td ><?php echo ": ".$kepsek[0]['tgl_sk_pendirian']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>SK Pendirian</td>
							<td ><?php echo ": ".$kepsek[0]['sk_pendirian']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Alamat</td>
							<td ><?php echo ": ".$kepsek[0]['alamat']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Kode Pos</td>
							<td ><?php echo ": ".$kepsek[0]['kode_pos']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Nomor Telepon</td>
							<td ><?php echo ": ".$kepsek[0]['telepon']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Fax</td>
							<td ><?php echo ": ".$kepsek[0]['fax']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Email</td>
							<td ><?php echo ": ".$kepsek[0]['email']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Website</td>
							<td ><?php echo ": ".$kepsek[0]['website']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Luas Halaman</td>
							<td ><?php echo ": ".$kepsek[0]['luas_halaman']; ?> m<sup>2</sup></td>
						</tr>
						<tr>
							<td width='' class='right'>Luas Tanah</td>
							<td ><?php echo ": ".$kepsek[0]['luas_tanah']; ?> m<sup>2</sup></td>
						</tr>
						<tr>
							<td width='' class='right'>Luas Bangunan</td>
							<td ><?php echo ": ".$kepsek[0]['luas_bangunan']; ?> m<sup>2</sup></td>
						</tr>
						<tr>
							<td width='' class='right'>Luas Lapangan Olahraga</td>
							<td ><?php echo ": ".$kepsek[0]['luas_olahraga']; ?> m<sup>2</sup></td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<h3>Kepala Sekolah</h3>
			<div>
				<table class="styled" >
					<thead>
						<tr colspan='2'>
						</tr>
						<tr>
							<td width='20px' class='center'>No.</td>
							<td width='100px' class='center'>NIP</td>
							<td width='' class='center'>Nama</td>
							<td width='130px' class='center'>Tgl. Pegangkatan</td>
						</tr>
					</thead>
					<tbody>
						<?php 
							$no=1;
							foreach($kepsek as $temp){
						?>
						<tr>
							<td class='center'>
								<?php
									echo $no;
									$no++;
								?>
							</td>
							<td class='center'><?php echo $temp['nip'];?></td>
							<td class='right'><?php echo $temp['nama'];?></td>
							<td class='center'><?php echo $temp['tgl_sk'];?></td>
							
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
			</div>
		</div><!-- End of .content -->
		
	</div><!-- End of .box -->		
 </div>
