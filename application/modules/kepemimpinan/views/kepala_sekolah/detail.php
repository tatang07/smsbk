<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
   	<div class="box">
			
		<div class="header">
			<h2>Detail Kepala Sekolah</h2>
		</div>
			<div class="dataTables_wrapper"  role="grid">
				<table class="styled" >
					<thead>
						<tr>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width='200px' class='right'>Nama</td>
							<td ><?php echo $kepsek[0]['nama']?></td>
						</tr>
						<tr>
							<td width='' class='right'>NIP</td>
							<td ><?php echo $kepsek[0]['nip']?></td>
						</tr>
						<tr>
							<td width='' class='right'>Pangkat/Golongan</td>
							<td ><?php echo $kepsek[0]['pangkat']."/".$kepsek[0]['golongan']?></td>

						</tr>
						<tr>
							<td width='' class='right'>Pendidikan</td>
							<td ><?php echo $kepsek[0]['jenjang_pendidikan']?></td>

						</tr>
						<tr>
							<td width='' class='right'>Nomor SK Pengangkatan</td>
							<td ><?php echo $kepsek[0]['no_sk']?></td>
						</tr>
						<tr>
							<td width='' class='right'>Tanggal Pengangkatan</td>
							<td ><?php echo $kepsek[0]['tgl_sk']?></td>
						</tr>
						<tr>
							<td width='' class='right'>TMT</td>
							<td ><?php echo $kepsek[0]['tmt_sk']?></td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="actions">
				<div class="left">
					<a href="<?php echo base_url('kepemimpinan/kepala_sekolah'); ?>"> <input value="Kembali" type="button"></a>
				</div>
			</div>
		</div><!-- End of .content -->
		
	</div><!-- End of .box -->		
 </div>
