<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
   	<div class="box">
			
		<div class="header">
			<h2>Identitas Kepala Sekolah</h2>
		</div>
		<?php //print_r($kepsek);?>
		<div class="tabletools">
			<div class="right">
				<a href="<?php echo base_url('kepemimpinan/kepala_sekolah/print_pdf/'); ?>"><i class="icon-print"></i>Cetak Ke File</a> 
			  <br/><br/>
			</div>
		</div>
			
			<div class="dataTables_wrapper"  role="grid">
				<table class="styled" >
					<thead>
						<tr>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width='200px' class='right'>Nama</td>
							<td ><?php echo $kepsek[0]['nama']?></td>
						</tr>
						<tr>
							<td width='' class='right'>NIP</td>
							<td ><?php echo $kepsek[0]['nip']?></td>
						</tr>
						<tr>
							<td width='' class='right'>Pangkat/Golongan</td>
							<td ><?php echo $kepsek[0]['pangkat']."/".$kepsek[0]['golongan']?></td>

						</tr>
						<tr>
							<td width='' class='right'>Pendidikan</td>
							<td ><?php echo $kepsek[0]['jenjang_pendidikan']?></td>

						</tr>
						<tr>
							<td width='' class='right'>Nomor SK Pengangkatan</td>
							<td ><?php echo $kepsek[0]['no_sk']?></td>
						</tr>
						<tr>
							<td width='' class='right'>Tanggal Pengangkatan</td>
							<td >
								<?php 
									if($kepsek[0]['tgl_sk']){
										echo tanggal_indonesia($kepsek[0]['tgl_sk']);								
									}else{
										echo "";
									}
								?>
							</td>
						</tr>
						<tr>
							<td width='' class='right'>TMT</td>
							<td >
								<?php
									if($kepsek[0]['tmt_sk']){										
										echo tanggal_indonesia($kepsek[0]['tmt_sk']);
									}else{
										echo "";
									}
								?>
							</td>
						</tr>
						<tr>
							<td colspan='2'></tr>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="accordion toggle">
			<h3><a href="#">Biodata Sekolah</a></h3>
			<div>
				<table class="styled" >
					<thead>
						<tr>
							<td colspan='2'></td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width='200px' class='right'>Nama Sekolah</td>
							<td ><?php echo $kepsek[0]['sekolah']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Nomor Statistik Sekolah: </td>
							<td ><?php echo $kepsek[0]['nss']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Nomor Identitas Sekolah</td>
							<td ><?php echo $kepsek[0]['nps']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Status Sekolah: </td>
							<td ><?php echo $kepsek[0]['status_sekolah']; ?></td>

						</tr>
						<tr>
							<td width='' class='right'>Tanggal Berdiri: </td>
							<td ><?php echo $kepsek[0]['tgl_sk_pendirian']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>SK Pendirian: </td>
							<td ><?php echo $kepsek[0]['sk_pendirian']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Alamat: </td>
							<td ><?php echo $kepsek[0]['alamat']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Kode Pos: </td>
							<td ><?php echo $kepsek[0]['kode_pos']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Nomor Telepon: </td>
							<td ><?php echo $kepsek[0]['telepon']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Fax: </td>
							<td ><?php echo $kepsek[0]['fax']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Email: </td>
							<td ><?php echo $kepsek[0]['email']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Website: </td>
							<td ><?php echo $kepsek[0]['website']; ?></td>
						</tr>
						<tr>
							<td width='' class='right'>Luas Halaman: </td>
							<td ><?php echo $kepsek[0]['luas_halaman']; ?> m<sup>2</sup></td>
						</tr>
						<tr>
							<td width='' class='right'>Luas Tanah: </td>
							<td ><?php echo $kepsek[0]['luas_tanah']; ?> m<sup>2</sup></td>
						</tr>
						<tr>
							<td width='' class='right'>Luas Bangunan: </td>
							<td ><?php echo $kepsek[0]['luas_bangunan']; ?> m<sup>2</sup></td>
						</tr>
						<tr>
							<td width='' class='right'>Luas Lapangan Olahraga: </td>
							<td ><?php echo $kepsek[0]['luas_olahraga']; ?> m<sup>2</sup></td>
						</tr>
					</tbody>
				</table>
			</div>
			
			<h3><a href="#">Kepala Sekolah</a></h3>
			<div>
				<div class="tabletools">
					<div class="right">
						<a href="<?php echo site_url('kepemimpinan/kepala_sekolah/add');?>"><i class="icon-plus"></i>Tambah</a> 
					  <br/><br/>
					</div>
				</div>
				<table class="styled" >
					<thead>
						<tr colspan='2'>
						</tr>
						<tr>
							<td width='20px' class='center'>No.</td>
							<td width='100px' class='center'>NIP</td>
							<td width='' class='center'>Nama</td>
							<td width='130px' class='center'>Tgl. Pegangkatan</td>
							<td width='200px' class='center'>Aksi</td>
						</tr>
					</thead>
					<tbody>
						<?php 
							$no=1;
							if(!empty($kepsek)):
							foreach($kepsek as $temp){
						?>
						<tr>
							<td class='center'>
								<?php
									echo $no;
									$no++;
								?>
							</td>
							<td class='center'><?php echo $temp['nip'];?></td>
							<td class='right'><?php echo $temp['nama'];?></td>
							<td class='center'><?php echo $temp['tgl_sk'];?></td>
							<td class='center'>
								<a href="<?php echo site_url('kepemimpinan/kepala_sekolah/edit/'.$temp['id_kepala_sekolah']);?>"class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a href="<?php echo site_url('kepemimpinan/kepala_sekolah/delete/'.$temp['id_kepala_sekolah']);?>"class="button small grey tooltip" data-gravity="s" onClick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Delete</a>
								<a href="<?php echo site_url('kepemimpinan/kepala_sekolah/detail/'.$temp['id_kepala_sekolah']);?>"class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Detail</a>
							</td>
						</tr>
						<?php
							}
							endif;
						?>
					</tbody>
				</table>
			</div>
		</div><!-- End of .content -->
		
	</div><!-- End of .box -->		
 </div>
