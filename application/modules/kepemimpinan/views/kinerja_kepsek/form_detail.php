	<h1 class="grid_12">Kepemimpinan</h1>
		<div class="grid_12">	   
			   <div class="box with-table">
				<div class="header">
					<h2>Detail Penilaian Kinerja Kepala Sekolah</h2>
				</div>
				<fieldset>
			
			<div class="content">
				<div class="dataTables_wrapper"  role="grid">    
					<div class="content" style='margin:5px;'>
						<table style="border-spacing: 0px; margin: 5px 0 20px 0; width: 100%; border: 1px solid #D2D2D2; background: #EDF2FD; padding: 5px;">
							<?php foreach($data_detail as $dd):?>
							<tbody>
								<tr>
									<td style='width:100px;'>Periode Mulai</td>
									<td style='width:10px;'>:&nbsp;</td>
								
									<td>
									<?php echo $dd['periode_awal']?>
									</td>
								</tr>
								<tr>
									<td style='width:100px;'>Periode Akhir</td>
									<td style='width:10px;'>:&nbsp;</td>
								
									<td>
									<?php echo $dd['periode_akhir']?>
									</td>
								</tr>
								<tr>
									<td style='width:100px;'>Yang Dinilai</td>
									<td style='width:10px;'>:&nbsp;</td>
								
									<td>
									<?php echo $dd['nama']?>
									</td>
								</tr>
								<tr>
									<td style='width:100px;'>Pejabat Penilai</td>
									<td style='width:10px;'>:&nbsp;</td>
								
									<td>
									<?php echo $dd['penilai']?>
									</td>
								</tr>
							</tbody>		
							<?php endforeach;?>
						</table>
					</div>
				</div>
			</div>
		
				<fieldset>
				
				
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kompetensi Inti</th>
								<th>Angka</th>
								<th>Keterangan</th>
							</tr>
						</thead>
						
						<tbody>
							<?php $no=1; foreach($jk_kepsek as $j): ?>
								<tr>
								<td width=50px class="center"><?php echo $no; ?></td>
								<td colspan=3><b><?php echo $j['jenis_kompetensi_kepsek']; ?></b></td>
								</tr>
								<?php foreach($bk_kepsek as $b): ?>
								<?php if($b['id_jenis_kompetensi_kepsek']==$j['id_jenis_kompetensi_kepsek']){ ?>
									<tr>
										<td></td>
										<td width=500px><?php echo $b['butir_kompetensi_kepsek']; ?></td>
										<?php $val=0;?>
										
										<?php foreach($data_butir as $db): ?>
											<?php if($val==0) {?> 
												<?php if($db['id_butir_kompetensi_kepsek']==$b['id_butir_kompetensi_kepsek']){ ?>
												<td width=100px class="center"><?php echo $db['nilai_angka'];?></td>
												<td width=250px class="center"><?php echo $db['keterangan'];?></td>
												<?php $val=1;?>
												<?php }?>
			 
												<?php }?>
											
										<?php endforeach; ?>
								
									</tr>
								<?php }endforeach; ?>
							<?php $no++; endforeach; ?>
						</tbody>
					</table>
				</fieldset>
				
				<div class="content">
				<div class="dataTables_wrapper"  role="grid">    
					<div class="content" style='margin:5px;'>
						<table style="border-spacing: 0px; margin: 5px 0 20px 0; width: 100%; border: 1px solid #D2D2D2; background: #EDF2FD; padding: 5px;">
							<?php foreach($data_detail as $dd):?>
							<tbody>
								<tr>
									<td style='width:100px;'>Kesimpulan</td>
									<td style='width:10px;'>:&nbsp;</td>
								
									<td>
									<?php echo $dd['kesimpulan']?>
									</td>
								</tr>
							</tbody>		
							<?php endforeach;?>
						</table>
					</div>
				</div>
			</div>
				
	</div>
	</div>
			</div><!-- End of .grid_4 -->
		
<!--
<script>
$(function(){
	$('#kelas').chosen().change(function(){
		var id_tingkat_kelas = $(this).val();
		console.log(id_tingkat_kelas);
		document.location = "<?php echo site_url('pps/bp_bk/get_nama_siswa'); ?>/" + id_tingkat_kelas;
	});
	
	// $(document).on('change', '#kabupaten', function(){
		// var id_kabupaten = $(this).val();
		// console.log(id_kabupaten);
		// $.post("<?php echo site_url('pps/prestasi_siswa/getKecamatan'); ?>/" + id_kabupaten, function(data){
			// $("#kecamatan").html(data);
			// $("#kecamatan").trigger("chosen:updated");
		// });
	// });
});
</script> -->