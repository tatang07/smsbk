<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class peningkatan_kompetensi extends CI_Controller {
	public function index()
	{	
		$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
		$id_sekolah = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		
		$data['isi'] = $this->m_peningkatan_kompetensi->get_data($id_sekolah);
		$data['component']="kepemimpinan";
		render('peningkatan_kompetensi/peningkatan_kompetensi',$data);
	}
	public function load_detail($id_kepsek_peningkatan_kompetensi)
	{	
		$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
		$id_sekolah = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		
		$data['id_kepsek_peningkatan_kompetensi'] = $id_kepsek_peningkatan_kompetensi;
		$data['nama_program'] = $this->m_peningkatan_kompetensi->get_nama_program($id_kepsek_peningkatan_kompetensi);
		$data['isi'] = $this->m_peningkatan_kompetensi->get_data_detail($id_kepsek_peningkatan_kompetensi);
		$data['component']="kepemimpinan";
		render('peningkatan_kompetensi/peningkatan_kompetensi_detail',$data);
	}
	public function load_add()
	{	
		$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
		$id_sekolah= get_id_sekolah();
		$data['kepsek'] = $this->m_peningkatan_kompetensi->select_kepsek($id_sekolah);
		$data['component']="kepemimpinan";
		render('peningkatan_kompetensi/form_tambah',$data);
	}
	public function load_add_detail($id_kepsek_peningkatan_kompetensi)
	{	
		$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
		$id_sekolah= get_id_sekolah();
		$data['id_kepsek_peningkatan_kompetensi']=$id_kepsek_peningkatan_kompetensi;
		$data['kepsek'] = $this->m_peningkatan_kompetensi->select_kepsek($id_sekolah);
		$data['component']="kepemimpinan";
		render('peningkatan_kompetensi/form_tambah_detail',$data);
	}
	public function load_edit($id_kepsek_peningkatan_kompetensi)
	{	
		$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_peningkatan_kompetensi->get_data_edit($id_kepsek_peningkatan_kompetensi);
	
		$data['component']="kepemimpinan";
		render('peningkatan_kompetensi/form_edit',$data);
	}

	public function delete($id_kepsek_peningkatan_kompetensi){
			$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
			$this->m_peningkatan_kompetensi->delete($id_kepsek_peningkatan_kompetensi);

			set_success_message('Data Berhasil Dihapus!');
			redirect(site_url('kepemimpinan/peningkatan_kompetensi/index/')) ;
	}
	public function delete_detail($id, $id_kepsek_peningkatan_kompetensi_detail){
			$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
			$this->m_peningkatan_kompetensi->delete_detail($id_kepsek_peningkatan_kompetensi_detail);

			set_success_message('Data Berhasil Dihapus!');
			redirect(site_url('kepemimpinan/peningkatan_kompetensi/load_detail/'. $id)) ;
	}
	public function submit_add(){
			$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
			$this->load->library('form_validation');

			$this->form_validation->set_rules('nama_program', 'Nama Program', 'required');
			$this->form_validation->set_rules('tanggal_mulai', 'Tanggal Mulai', 'required');
			$this->form_validation->set_rules('tanggal_selesai', 'Tanggal Selesai', 'required');
			$this->form_validation->set_rules('id_kepsek', 'Kepala Sekolah', 'required');

			if($this->form_validation->run() == TRUE){
				$data['id_kepsek'] = $this->input->post('id_kepsek');
				$data['nama_program'] = $this->input->post('nama_program');
				$data['tanggal_mulai'] = tanggal_db($this->input->post('tanggal_mulai'));
				$data['tanggal_selesai'] = tanggal_db($this->input->post('tanggal_selesai'));		
			
				$this->m_peningkatan_kompetensi->add($data);

				set_success_message('Data Berhasil Ditambah!');
				redirect(site_url('kepemimpinan/peningkatan_kompetensi/index/')) ;
			}else{
				set_error_message('Terjadi Kesalahan Pengisian!');
				redirect(site_url('kepemimpinan/peningkatan_kompetensi/index/')) ;
			}	
	}
	public function submit_add_detail(){
			$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
			$this->load->library('form_validation');

			$this->form_validation->set_rules('id_kepala_sekolah', 'Kepala Sekolah', 'required');
			$id = $this->input->post('id_kepsek_peningkatan_kompetensi');

			if($this->form_validation->run() == TRUE){
				$data['id_kepsek'] = $this->input->post('id_kepala_sekolah');
				$data['id_kepsek_peningkatan_kompetensi'] = $this->input->post('id_kepsek_peningkatan_kompetensi');
			
				$this->m_peningkatan_kompetensi->add_detail($data);
				set_success_message('Data Berhasil Ditambah!');
				redirect(site_url('kepemimpinan/peningkatan_kompetensi/load_detail/'. $id )) ;
			}else{
				set_error_message('Terjadi Kesalahan Pengisian!');
				redirect(site_url('kepemimpinan/peningkatan_kompetensi/load_detail/'. $id)) ;
			}
	}
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_peningkatan_kompetensi->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_peningkatan_kompetensi->select_kelas($jenjang_sekolah);
			render('peningkatan_kompetensi/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
			$id=$this->input->post('id');
			$data['id_kepsek'] = $this->input->post('id_kepsek');
			$data['nama_program'] = $this->input->post('nama_program');
			$data['tanggal_mulai'] = tanggal_db($this->input->post('tanggal_mulai'));
			$data['tanggal_selesai'] = tanggal_db($this->input->post('tanggal_selesai'));
			$this->m_peningkatan_kompetensi->edit($data,$id);

			set_success_message('Data Berhasil Diupdate!');
			redirect(site_url('kepemimpinan/peningkatan_kompetensi/index/')) ;
	}
	public function search(){
		$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
		$nama = $this->input->post('nama');
		$id_sekolah = get_id_sekolah();
		$data['isi'] = $this->m_peningkatan_kompetensi->get_data_search($nama,$id_sekolah);
	
		$data['component']="kepemimpinan";
		render('peningkatan_kompetensi/peningkatan_kompetensi',$data);
	}
	public function search_detail($id_kepsek_peningkatan_kompetensi){
		$this->load->model('kepemimpinan/m_peningkatan_kompetensi');
		$nama = $this->input->post('nama');
		$id_sekolah = get_id_sekolah();
		$data['id_kepsek_peningkatan_kompetensi'] = $id_kepsek_peningkatan_kompetensi;
		$data['isi'] = $this->m_peningkatan_kompetensi->get_data_search_detail($nama,$id_kepsek_peningkatan_kompetensi);
	
		$data['component']="kepemimpinan";
		render('peningkatan_kompetensi/peningkatan_kompetensi_detail',$data);
	}
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */