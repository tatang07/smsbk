<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class evaluasi_diri extends CI_Controller {

	public function index()
	{	
		$this->load->model('kepemimpinan/m_evaluasi_diri');
		$id_sekolah = get_id_sekolah();
		
		$data['kepsek'] = $this->m_evaluasi_diri->get_kepsek($id_sekolah)->result_array();
		$data['component']="kepemimpinan";
		render('evaluasi_diri/evaluasi_diri',$data);
	}

	public function load_add($id)
	{	
		$this->load->model('kepemimpinan/m_evaluasi_diri');

		if($id == 0){
			set_error_message('Pilih Kepala Sekolah Terlebih Dahulu!');
			redirect(site_url('kepemimpinan/evaluasi_diri'));
		}else{
			$data['id_kepala_sekolah'] = $id;
			$data['component']="kepemimpinan";
			render('evaluasi_diri/form_tambah', $data);					
		}
	}

	public function simpan(){
		$this->load->model('kepemimpinan/m_evaluasi_diri');

		$id_kepala_sekolah = $this->input->post('id_kepala_sekolah');
		$tgl_mulai = tanggal_db($this->input->post('tgl_mulai'));
		$tgl_akhir = tanggal_db($this->input->post('tgl_akhir'));

		$config['upload_path'] = './extras/evaluasi/';
		$config['allowed_types'] = 'pdf|doc|docx|xls|txt';
		$config['max_size']	= '10000';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile')){
			$error = array('error' => $this->upload->display_errors());
		}else{
			$lokasi_file = $this->upload->data();
		}

		$this->m_evaluasi_diri->insert($id_kepala_sekolah, $tgl_mulai, $tgl_akhir, base_url("extras/evaluasi/".$lokasi_file['file_name']));
		redirect(site_url('kepemimpinan/evaluasi_diri'));
	}

	public function download(){
		$this->load->model('kepemimpinan/m_evaluasi_diri');
		$this->load->helper('download');
		$id = $this->uri->segment(4);
		if ($id == NULL){
			redirect('kepemimpinan/evaluasi_diri');
		}
		$row = $this->m_evaluasi_diri->get_data_by($id)->row();
		$file = $row->lokasi_file;
		$data = file_get_contents($file);
		$filename = basename($file);
		
		force_download($filename, $data);
	}

	public function delete($id_kepsek_evaluasi){
		$this->load->model('kepemimpinan/m_evaluasi_diri');
		$this->m_evaluasi_diri->delete($id_kepsek_evaluasi);
		redirect(site_url('kepemimpinan/evaluasi_diri'));
	}
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */