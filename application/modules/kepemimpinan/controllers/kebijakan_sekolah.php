<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class kebijakan_sekolah extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('t_sekolah_kebijakan');
			$this->load->helper('download');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('kepemimpinan/kebijakan_sekolah/home');
		}
		
		public function home(){
		
			$data['tahun_ajaran'] = $this->t_sekolah_kebijakan->get_all_tahun_ajaran();
			$first_tahun_ajaran = $this->t_sekolah_kebijakan->get_first_tahun_ajaran();
			if($first_tahun_ajaran){
				$id_tahun_ajaran = $first_tahun_ajaran->id_tahun_ajaran;
			}else{
				$id_tahun_ajaran = 0;
			}
			
			$id_sekolah = get_id_sekolah();
			$id_tahun_ajaran = get_id_tahun_ajaran();
			$data['id_tahun_ajaran'] = $id_tahun_ajaran;
			
			$config = array();
			$config["base_url"] = base_url()."kepemimpinan/kebijakan_sekolah/home";
			$config["total_rows"] = $this->t_sekolah_kebijakan->count_datalist($id_sekolah, $id_tahun_ajaran);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			
			$data['list'] = $this->t_sekolah_kebijakan->get_datalist($id_sekolah, $id_tahun_ajaran, $config["per_page"], $page);
			$data['first_tahun_ajaran'] = $id_tahun_ajaran;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['stat'] = 'home';
			$data['jumlah'] = $this->t_sekolah_kebijakan->count_datalist($id_sekolah, $id_tahun_ajaran);

			$data['component']="kepemimpinan";
			render("kebijakan_sekolah/kebijakan_sekolah", $data);
		}
		
		public function search($id_tahun_ajaran=0, $status=0){
			if($id_tahun_ajaran==0){
				$id_tahun_ajaran = $this->input->post('id_tahun_ajaran');
			}
			$data['id_tahun_ajaran'] = $id_tahun_ajaran;
			$data['tahun_ajaran'] = $this->t_sekolah_kebijakan->get_all_tahun_ajaran();
			
			
			$id_sekolah = get_id_sekolah();
			
			$config = array();
			$config["base_url"] = base_url()."kepemimpinan/kebijakan_sekolah/home";
			$config["total_rows"] = $this->t_sekolah_kebijakan->count_datalist($id_sekolah, $id_tahun_ajaran);
			$config["per_page"] = 20;
			$config["uri_segment"] = 4;
	 
			$this->pagination->initialize($config);
	 
			if($status==0){
				$page = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
			}else{
				$page = 0;
			}
			$data['list'] = $this->t_sekolah_kebijakan->get_datalist($id_sekolah, $id_tahun_ajaran, $config["per_page"], $page);
			$data['first_tahun_ajaran'] = $id_tahun_ajaran;
			$data["links"] = $this->pagination->create_links();
			$data['page'] = $page;
			$data['stat'] = 'search';
			$data['jumlah'] = $this->t_sekolah_kebijakan->count_datalist($id_sekolah, $id_tahun_ajaran);

			$data['component']="kepemimpinan";
			render("kebijakan_sekolah/kebijakan_sekolah", $data);
		}
		
		public function add(){
			$data['component']="kepemimpinan";
			render("kebijakan_sekolah/form_tambah", $data);
		}
		
		public function submit(){
			$this->form_validation->set_rules('nomor_surat', 'Nomor Surat', 'required');
			$this->form_validation->set_rules('perihal', 'Perihal', 'required');
			$this->form_validation->set_rules('alamatfile', 'File', 'file_req');
			
			$filediisi = true;
			if($_FILES['alamatfile']['error'] > 0){
				$filediisi = false;
			}
			
			if($this->form_validation->run() == TRUE && $filediisi){
				$nomor_surat = $this->input->post('nomor_surat');
				$perihal = $this->input->post('perihal');
				$keterangan = $this->input->post('keterangan');
				$id_tahun_ajaran = $this->input->post('id_tahun_ajaran');
				$id_sekolah = get_id_sekolah();
				
				$config['upload_path'] = './extras/kebijakan_sekolah/';
				$config['allowed_types'] = 'pdf|doc|docx|xls|txt';
				$config['max_size']	= '10000';

				$this->load->library('upload', $config);
				$lokasi_file=null;
				if ( ! $this->upload->do_upload('alamatfile')){
					$error = array('error' => $this->upload->display_errors());
				}else{
					$lokasi_file = $this->upload->data();
				}
				
				
				$action = $this->input->post('action');
				if($action == 'add'){
					$this->t_sekolah_kebijakan->insert($nomor_surat, $perihal, base_url("extras/kebijakan_sekolah/".$lokasi_file['file_name']), $id_sekolah, $id_tahun_ajaran, $keterangan);
					set_success_message('Data Berhasil Ditambah!');
					$this->search($id_tahun_ajaran, 9);
				}
			}
			else{
				set_error_message(validation_errors());
				if(!$filediisi){
					set_error_message("file wajib diisi");
				}
				redirect(getenv('HTTP_REFERER'));
			}
		}
		
		public function delete(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kepemimpinan/kebijakan_sekolah/home');
			}
			$temp = $this->t_sekolah_kebijakan->get_sekolah_kebijakan_by($id);
			$id_tahun_ajaran = $temp->id_tahun_ajaran;
			$this->t_sekolah_kebijakan->delete($id);
			set_success_message('Data Berhasil Dihapus!');
			$this->search($id_tahun_ajaran, 9);
		}
		
		public function download(){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('kepemimpinan/kebijakan_sekolah/home');
			}
			$row = $this->t_sekolah_kebijakan->get_sekolah_kebijakan_by($id);
			$file = $row->alamat_file;
			$data = file_get_contents($file);
			$filename = basename($file);;
			
			force_download($filename, $data);
			redirect('kepemimpinan/kebijakan_sekolah/home');
		}
		public function cek_nomor_surat(){
			$nomor_surat = $this->input->post("nmr");
			$nomor = $this->t_sekolah_kebijakan->cek_nomor_surat($nomor_surat);
			
			echo $nomor->num_rows;
		}	
}
