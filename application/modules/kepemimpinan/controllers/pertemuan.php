<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class pertemuan extends CI_Controller {
	public function index()
	{	
		$this->load->model('kepemimpinan/m_pertemuan');

		$id = get_id_tahun_ajaran();
		$data['tahun_ajaran'] = $this->m_pertemuan->getTahunAjaran($id)->result_array();

		$data['component']="kepemimpinan";
		render('pertemuan/pertemuan', $data);
	}

	public function load_add($id){
		$data['tahun_ajaran'] = $id;

		$data['component']="kepemimpinan";
		render('pertemuan/add_pertemuan', $data);
	}

	public function add(){
		$this->load->model('kepemimpinan/m_pertemuan');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nama_kegiatan', 'Nama Kegiatan', 'required');
		$this->form_validation->set_rules('tanggal', 'Tanggal', 'required');
		$this->form_validation->set_rules('hasil_pertemuan', 'Hasil Pertemuan', 'required');
		$this->form_validation->set_rules('tempat', 'Tempat', 'required');

		if($this->form_validation->run() == TRUE){
			$data['id_sekolah'] = get_id_sekolah();
			$data['id_tahun_ajaran'] = $this->input->post('id_tahun_ajaran');
			$data['nama_kegiatan'] = $this->input->post('nama_kegiatan');
			$data['tanggal'] = tanggal_db($this->input->post('tanggal'));
			$data['hasil_pertemuan'] = $this->input->post('hasil_pertemuan');
			$data['tempat'] = $this->input->post('tempat');
			$this->m_pertemuan->add($data);

			set_success_message('Data Berhasil Ditambah!');
			redirect(site_url('kepemimpinan/pertemuan'));
		}else{
			set_error_message('Terjadi Kesalahan Pengisian!');
			redirect(site_url('kepemimpinan/pertemuan')) ;
		}	
	}

	public function load_edit($id){
		$this->load->model('kepemimpinan/m_pertemuan');
		$data['id_sekolah_pertemuan'] = $id;
		$data['pertemuan'] = $this->m_pertemuan->getDataPertemuan_ById($id)->result_array();

		$data['component']="kepemimpinan";
		render('pertemuan/edit_pertemuan', $data);
	}

	public function edit(){
		$this->load->model('kepemimpinan/m_pertemuan');
		$data['nama_kegiatan'] = $this->input->post('nama_kegiatan');
		$data['tanggal'] = tanggal_db($this->input->post('tanggal'));
		$data['hasil_pertemuan'] = $this->input->post('hasil_pertemuan');
		$data['tempat'] = $this->input->post('tempat');
		$id_sekolah_pertemuan = $this->input->post('id_sekolah_pertemuan');
		$this->m_pertemuan->edit($data, $id_sekolah_pertemuan);

		set_success_message('Data Berhasil Diupdate!');
		redirect(site_url('kepemimpinan/pertemuan'));
	}

	public function delete($id_sekolah_pertemuan){
		$this->load->model('kepemimpinan/m_pertemuan');
		$this->m_pertemuan->delete($id_sekolah_pertemuan);

		set_success_message('Data Berhasil Dihapus!');
		redirect(site_url('kepemimpinan/pertemuan'));
	}
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */