<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class penilaian_tugas_pokok extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->helper('download');
	}

	public function index()
	{	
		$this->load->model('kepemimpinan/m_penilaian_tugas_pokok');
		$id_sekolah = get_id_sekolah();
		
		$data['kepsek'] = $this->m_penilaian_tugas_pokok->get_kepsek($id_sekolah)->result_array();

		$data['component']="kepemimpinan";
		render('penilaian_tugas_pokok/ptp',$data);
	}

	public function load_add($id)
	{	
		$this->load->model('kepemimpinan/m_penilaian_tugas_pokok');

		if($id == 0){
			set_error_message('Pilih Kepala Sekolah Terlebih Dahulu!');
			redirect(site_url('kepemimpinan/penilaian_tugas_pokok'));
		}else{
			$data['id_kepala_sekolah'] = $id;
			$data['component']="kepemimpinan";
			render('penilaian_tugas_pokok/form_tambah', $data);			
		}
	}

	public function simpan(){
		$this->load->model('kepemimpinan/m_penilaian_tugas_pokok');

		$id_kepala_sekolah = $this->input->post('id_kepala_sekolah');
		$tgl_mulai = tanggal_db($this->input->post('tgl_mulai'));
		$tgl_akhir = tanggal_db($this->input->post('tgl_akhir'));

		$config['upload_path'] = './extras/ptp/';
		$config['allowed_types'] = 'pdf|doc|docx|xls|txt';
		$config['max_size']	= '10000';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile')){
			$error = array('error' => $this->upload->display_errors());
		}else{
			$lokasi_file = $this->upload->data();
		}

		$this->m_penilaian_tugas_pokok->insert($id_kepala_sekolah, $tgl_mulai, $tgl_akhir, base_url("extras/ptp/".$lokasi_file['file_name']));
		redirect(site_url('kepemimpinan/penilaian_tugas_pokok'));
	}

	public function download(){
		$this->load->model('kepemimpinan/m_penilaian_tugas_pokok');

		$id = $this->uri->segment(4);
		if ($id == NULL){
			redirect('kepemimpinan/penilaian_tugas_pokok');
		}
		$row = $this->m_penilaian_tugas_pokok->get_data_by($id)->row();
		$file = $row->lokasi_file;
		$data = file_get_contents($file);
		$filename = basename($file);
		
		force_download($filename, $data);
	}

	public function delete($id_kepsek_penilaian_tupok){
		$this->load->model('kepemimpinan/m_penilaian_tugas_pokok');
		$this->m_penilaian_tugas_pokok->delete($id_kepsek_penilaian_tupok);
		redirect(site_url('kepemimpinan/penilaian_tugas_pokok'));
	}
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */