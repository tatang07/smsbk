<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class studi_lanjut extends CI_Controller {

	public function index()
	{	
		$this->load->model('kepemimpinan/m_studi_lanjut');
		$id_sekolah = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		
		$data['isi'] = $this->m_studi_lanjut->get_data($id_sekolah);
		$data['component']="kepemimpinan";
		render('studi_lanjut/studi_lanjut',$data);
	}
	public function load_add()
	{	
		$this->load->model('kepemimpinan/m_studi_lanjut');
		$id_sekolah= get_id_sekolah();
		// $jenjang_sekolah = get_jenjang_sekolah();
		$data['kepsek'] = $this->m_studi_lanjut->select_kepsek($id_sekolah);
		$data['component']="kepemimpinan";
		render('studi_lanjut/form_tambah',$data);
	}

	public function submit_add()
	{
		$this->load->model('kepemimpinan/m_studi_lanjut');
		$data['id_kepsek'] = $this->input->post('id_kepsek');
		$data['tahun_masuk'] = $this->input->post('tahun_masuk');
		$data['tahun_keluar'] = $this->input->post('tahun_lulus');
		$data['nama_kampus'] = $this->input->post('nama_kampus');
		$data['status'] = $this->input->post('status');

		$this->m_studi_lanjut->add($data);
		redirect(site_url('kepemimpinan/studi_lanjut/index/')) ;
	}

	public function load_edit($id_kepsek_studi_lanjut)
	{	
		$this->load->model('kepemimpinan/m_studi_lanjut');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_studi_lanjut->get_data_edit($id_kepsek_studi_lanjut);
		$data['component']="kepemimpinan";
		render('studi_lanjut/form_edit',$data);
	}

	public function delete($id_guru_studi_lanjut){
			$this->load->model('kepemimpinan/m_studi_lanjut');
			$this->m_studi_lanjut->delete($id_guru_studi_lanjut);
			redirect(site_url('kepemimpinan/studi_lanjut/index/')) ;
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('kepemimpinan/m_studi_lanjut');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_studi_lanjut->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_studi_lanjut->select_kelas($jenjang_sekolah);
			render('studi_lanjut/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('kepemimpinan/m_studi_lanjut');
			$id=$this->input->post('id');
			$data['tahun_masuk'] = $this->input->post('tahun_masuk');
			$data['tahun_keluar'] = $this->input->post('tahun_lulus');
			$data['nama_kampus'] = $this->input->post('nama_kampus');
			$data['status'] = $this->input->post('status');


			$this->m_studi_lanjut->edit($data,$id);
			redirect(site_url('kepemimpinan/studi_lanjut/index/')) ;
	}
	public function search(){
		$this->load->model('kepemimpinan/m_studi_lanjut');
		$nama = $this->input->post('nama');
		$status = $this->input->post('status');
		
		$id_sekolah = get_id_sekolah();
		if($status==0){
		$data['isi'] = $this->m_studi_lanjut->get_data_search_default($nama,$id_sekolah);
		}else{
		$data['isi'] = $this->m_studi_lanjut->get_data_search($status,$nama,$id_sekolah);
		}
		$data['component']="kepemimpinan";
		render('studi_lanjut/studi_lanjut',$data);
	}
	
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */