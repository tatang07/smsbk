<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class asuransi extends Simple_Controller {
    	
		public function index(){

			$this->load->model('m_asuransi');
			$data['data_asuransi']=$this->m_asuransi->select_data_asuransi()->result_array();
			$data['component'] = "kepemimpinan";
			render("asuransi/asuransi",$data);
		}
		public function load_detail($id_asuransi)
		{	
			$this->load->model('m_asuransi');
			$data['data_asuransi']=$this->m_asuransi->select_data_asuransi()->result();
			$data['isi'] = $this->m_asuransi->get_data_detail($id_asuransi)->result_array();
			$data['statuspil'] = $id_asuransi;
			$data['component'] = "kepemimpinan";
			render('asuransi/asuransi_detail',$data);
		}
		
		public function delete($id_asuransi_detail){
			$this->load->model('kepemimpinan/m_asuransi');
			$this->m_asuransi->delete($id_asuransi_detail);
			redirect(site_url('kepemimpinan/asuransi/')) ;
		}
}
