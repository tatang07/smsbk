<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class tunjangan_tunjangan extends Simple_Controller {
    	
		public function index(){

			$this->load->model('m_tunjangan');
			$data['data_tunjangan']=$this->m_tunjangan->select_data_tunjangan()->result_array();
			$data['component'] = "kepemimpinan";
			render("tunjangan/tunjangan",$data);
		}
		
}
