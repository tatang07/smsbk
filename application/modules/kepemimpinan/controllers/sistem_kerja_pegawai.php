<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class sistem_kerja_pegawai extends CI_Controller {

	public function index()
	{	
		$this->load->model('kepemimpinan/m_sistem_kerja_pegawai');
		$id_sekolah = get_id_sekolah();
		
		$data['kepsek'] = $this->m_sistem_kerja_pegawai->get_kepsek($id_sekolah)->result_array();
		$data['component']="kepemimpinan";
		render('sistem_kerja_pegawai/skp',$data);
	}

	public function load_add($id)
	{	
		$this->load->model('kepemimpinan/m_sistem_kerja_pegawai');

		if($id == 0){
			set_error_message('Pilih Kepala Sekolah Terlebih Dahulu!');
			redirect(site_url('kepemimpinan/sistem_kerja_pegawai'));
		}else{
			$data['id_kepala_sekolah'] = $id;
			$data['component']="kepemimpinan";
			render('sistem_kerja_pegawai/form_tambah', $data);					
		}
	}

	public function simpan(){
		$this->load->model('kepemimpinan/m_sistem_kerja_pegawai');

		$id_kepala_sekolah = $this->input->post('id_kepala_sekolah');
		$tgl_mulai = tanggal_db($this->input->post('tgl_mulai'));
		$tgl_akhir = tanggal_db($this->input->post('tgl_akhir'));

		$config['upload_path'] = './extras/skp/';
		$config['allowed_types'] = 'pdf|doc|docx|xls|txt';
		$config['max_size']	= '10000';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile')){
			$error = array('error' => $this->upload->display_errors());
		}else{
			$lokasi_file = $this->upload->data();
		}

		$this->m_sistem_kerja_pegawai->insert($id_kepala_sekolah, $tgl_mulai, $tgl_akhir, base_url("extras/skp/".$lokasi_file['file_name']));
		redirect(site_url('kepemimpinan/sistem_kerja_pegawai'));
	}

	public function download(){
		$this->load->model('kepemimpinan/m_sistem_kerja_pegawai');
		$this->load->helper('download');
		$id = $this->uri->segment(4);
		if ($id == NULL){
			redirect('kepemimpinan/sistem_kerja_pegawai');
		}
		$row = $this->m_sistem_kerja_pegawai->get_data_by($id)->row();
		$file = $row->lokasi_file;
		$data = file_get_contents($file);
		$filename = basename($file);
		
		force_download($filename, $data);
	}

	public function delete($id_kepsek_skp){
		$this->load->model('kepemimpinan/m_sistem_kerja_pegawai');
		$this->m_sistem_kerja_pegawai->delete($id_kepsek_skp);
		redirect(site_url('kepemimpinan/sistem_kerja_pegawai'));
	}
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */