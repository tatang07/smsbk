<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class kepala_sekolah extends CI_Controller {

	public function index(){	
		$this->load->model('kepemimpinan/m_kepala_sekolah');
		$data['kepsek'] = $this->m_kepala_sekolah->get_kepsek();

		$data['component']="kepemimpinan";
		render('kepala_sekolah/kepala_sekolah',$data);
	}

	public function print_pdf(){
		$this->load->model('kepemimpinan/m_kepala_sekolah');
		$data['kepsek'] = $this->m_kepala_sekolah->get_kepsek();
		print_pdf($this->load->view('kepala_sekolah/kepala_sekolah_print', $data, true), "A4");

		$data['component']="kepemimpinan";
		render('kepala_sekolah/kepala_sekolah',$data);
	}
	
	public function edit(){
		$this->load->model('kepemimpinan/m_kepala_sekolah');
		$id = $this->uri->rsegment(3);
		$data['detail'] = $this->m_kepala_sekolah->get_detail_by_id($id);
		$data['jenjang'] = $this->m_kepala_sekolah->get_all_jenjang();
		$data['pangkat'] = $this->m_kepala_sekolah->get_all_pangkat();

		$data['component']="kepemimpinan";
		render('kepala_sekolah/edit', $data);
	}
	
	public function proses_edit(){
		$this->load->model('kepemimpinan/m_kepala_sekolah');
		$data=$this->input->post();
		$id = $data['id_kepala_sekolah'];
		$data['tgl_sk'] = tanggal_db($data['tgl_sk']);
		$data['tmt_sk'] = tanggal_db($data['tmt_sk']);
		unset($data['id_kepala_sekolah']);
		unset($data['send']);
		//print_r($data);
		$this->m_kepala_sekolah->edit($data, $id);
		set_success_message("data berhasil di perbarui");
		redirect("kepemimpinan/kepala_sekolah");
	}
	
	public function delete(){
		$this->load->model('kepemimpinan/m_kepala_sekolah');
		$id = $this->uri->rsegment(3);
		$this->m_kepala_sekolah->delete($id);
		set_success_message("data berhasil di hapus");
		redirect("kepemimpinan/kepala_sekolah");
	}
	
	public function detail(){	
		$this->load->model('kepemimpinan/m_kepala_sekolah');
		$id = $this->uri->rsegment(3);
		$data['kepsek'] = $this->m_kepala_sekolah->get_kepsek_by_id($id);
		foreach($data['kepsek'] as $kepala){
			$kepala['tgl_sk'] = tanggal_indonesia($kepala['tgl_sk']);
			$kepala['tmt_sk'] = tanggal_indonesia($kepala['tmt_sk']);
		}
		//print_r($data);
		
		$data['component']="kepemimpinan";
		render('kepala_sekolah/detail',$data);
	}
	
	public function add(){
		$this->load->model('kepemimpinan/m_kepala_sekolah');
		$data['jenjang'] = $this->m_kepala_sekolah->get_all_jenjang();
		$data['pangkat'] = $this->m_kepala_sekolah->get_all_pangkat();

		$data['component']="kepemimpinan";
		render('kepala_sekolah/add', $data);
	}
	
	public function proses_add(){
		$this->load->model('kepemimpinan/m_kepala_sekolah');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('nip', 'NIP', 'required');
		$this->form_validation->set_rules('nama', 'Nama', 'required');
		$this->form_validation->set_rules('no_sk', 'No SK', 'required');
		$this->form_validation->set_rules('tgl_sk', 'Tanggal SK', 'required');
		$this->form_validation->set_rules('tmt_sk', 'TMT Sk', 'required');

		if($this->form_validation->run() == TRUE){
			$data=$this->input->post();
			$data['id_sekolah'] = get_id_sekolah();
			$data['tgl_sk'] = tanggal_db($data['tgl_sk']);
			$data['tmt_sk'] = tanggal_db($data['tmt_sk']);
			unset($data['send']);
			//print_r($data);
			$this->m_kepala_sekolah->add($data);
			set_success_message("data berhasil di simpan");
			redirect("kepemimpinan/kepala_sekolah");
		}else{
			set_error_message('Terjadi Kesalahan Pengisian!');
			redirect("kepemimpinan/kepala_sekolah");
		}
	}
	
	public function cek_nip($nip){
		$this->load->model('kepemimpinan/m_kepala_sekolah');
		$nip = $this->m_kepala_sekolah->cek_nip($nip);
		
		echo $nip->num_rows;
	}	
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */