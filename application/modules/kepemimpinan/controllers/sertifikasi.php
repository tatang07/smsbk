<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class sertifikasi extends CI_Controller {

	public function index()
	{	
		$this->load->model('kepemimpinan/m_sertifikasi');
		$id_sekolah = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		
		$data['isi'] = $this->m_sertifikasi->get_data($id_sekolah);
		$data['component']="kepemimpinan";
		render('sertifikasi/sertifikasi',$data);
	}
	public function load_add()
	{	
		$this->load->model('kepemimpinan/m_sertifikasi');
		$id_sekolah= get_id_sekolah();
		$data['kepsek'] = $this->m_sertifikasi->select_kepsek($id_sekolah);
		$data['component']="kepemimpinan";
		render('sertifikasi/form_tambah',$data);
	}

	public function load_edit($id_guru_sertifikasi)
	{	
		$this->load->model('kepemimpinan/m_sertifikasi');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_sertifikasi->get_data_edit($id_guru_sertifikasi);
	
		$data['component']="kepemimpinan";
		render('sertifikasi/form_edit',$data);
	}

	public function delete($id_guru_sertifikasi){
			$this->load->model('kepemimpinan/m_sertifikasi');
			$this->m_sertifikasi->delete($id_guru_sertifikasi);

			set_success_message('Data Berhasil Dihapus!');
			redirect(site_url('kepemimpinan/sertifikasi/index/')) ;
	}

	public function submit_add(){
			$this->load->model('kepemimpinan/m_sertifikasi');
			$data['id_kepsek'] = $this->input->post('id_kepsek');
			$data['tanggal_sertifikasi'] = tanggal_db($this->input->post('tanggal_sertifikasi'));
			$data['tanggal_kadaluarsa'] = tanggal_db($this->input->post('tanggal_kadaluarsa'));
			$data['sertifikasi'] = $this->input->post('sertifikasi');
		
		
			$this->m_sertifikasi->add($data);
			set_success_message('Data Berhasil Ditambah!');
			redirect(site_url('kepemimpinan/sertifikasi/index/')) ;
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('kepemimpinan/m_sertifikasi');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_sertifikasi->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_sertifikasi->select_kelas($jenjang_sekolah);
			render('sertifikasi/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('kepemimpinan/m_sertifikasi');
			$id=$this->input->post('id');
			$data['id_kepsek'] = $this->input->post('id_kepsek');
			
			$data['tanggal_sertifikasi'] = tanggal_db($this->input->post('tanggal_sertifikasi'));
			$data['tanggal_kadaluarsa'] = tanggal_db($this->input->post('tanggal_kadaluarsa'));
			$data['sertifikasi'] = $this->input->post('sertifikasi');
			$this->m_sertifikasi->edit($data,$id);

			set_success_message('Data Berhasil Diupdate!');
			redirect(site_url('kepemimpinan/sertifikasi/index/')) ;
	}
	public function search(){
		$this->load->model('kepemimpinan/m_sertifikasi');
		$nama = $this->input->post('nama');
		// $status = $this->input->post('status');
		
		$id_sekolah = get_id_sekolah();

		$data['isi'] = $this->m_sertifikasi->get_data_search($nama,$id_sekolah);
	
		$data['component']="kepemimpinan";
		render('sertifikasi/sertifikasi',$data);
	}
	
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */