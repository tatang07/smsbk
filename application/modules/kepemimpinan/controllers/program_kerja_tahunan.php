<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class program_kerja_tahunan extends CI_Controller {

	public function index()
	{	
		$this->load->model('kepemimpinan/m_program_kerja_tahunan');
		$data['ta'] = $this->m_program_kerja_tahunan->get_tahun_ajaran();
		$data['component']="kepemimpinan";
		render('program_kerja_tahunan/program_kerja_tahunan',$data);
	}
	public function load_add($id_tahun_ajaran)
	{	
		$this->load->model('kepemimpinan/m_program_kerja_tahunan');

		if($id_tahun_ajaran != 0){
			$data['tabi'] = $this->m_program_kerja_tahunan->get_tahun_ajaran_by_id($id_tahun_ajaran);
			$data['id_tahun_ajaran']=$id_tahun_ajaran;
			$data['component']="kepemimpinan";
			render('program_kerja_tahunan/form_tambah',$data);
		}else{
			set_error_message('Pilih Tahun Ajaran Terlebih Dahulu!');
			redirect(site_url('kepemimpinan/program_kerja_tahunan')) ;
		}
	}
	public function load_add_sub($id_sekolah_program_kerja)
	{	
		$this->load->model('kepemimpinan/m_program_kerja_tahunan');
		$data['id_sekolah_program_kerja']=$id_sekolah_program_kerja;
		$data['component']="kepemimpinan";
		render('program_kerja_tahunan/form_tambah_sub',$data);
	}
	public function load_detail($id_sekolah_program_kerja)
	{	
		$this->load->model('kepemimpinan/m_program_kerja_tahunan');
		$data['program_kerja'] = $this->m_program_kerja_tahunan->get_data_by_id($id_sekolah_program_kerja);
		$data['sub'] = $this->m_program_kerja_tahunan->get_data_sub($id_sekolah_program_kerja);
		$data['component']="kepemimpinan";
		render('program_kerja_tahunan/sub_program_kerja_tahunan',$data);
	}
	public function load_edit($id_tahun_ajaran,$id_sekolah_program_kerja)
	{	
		$this->load->model('kepemimpinan/m_program_kerja_tahunan');
		$data['tabi'] = $this->m_program_kerja_tahunan->get_tahun_ajaran_by_id($id_tahun_ajaran);
		$data['data_edit'] = $this->m_program_kerja_tahunan->get_data_by_id($id_sekolah_program_kerja);
		$data['id']=$id_sekolah_program_kerja;
		$data['component']="kepemimpinan";
		render('program_kerja_tahunan/form_edit',$data);
	}
	public function load_edit_sub($id_sekolah_sub_program_kerja,$id_sekolah_program_kerja)
	{	
		$this->load->model('kepemimpinan/m_program_kerja_tahunan');
		$data['data_edit'] = $this->m_program_kerja_tahunan->get_data_sub_by_id($id_sekolah_sub_program_kerja);
		$data['id']=$id_sekolah_sub_program_kerja;
		$data['id2']=$id_sekolah_program_kerja;
		$data['component']="kepemimpinan";
		render('program_kerja_tahunan/form_edit_sub',$data);
	}
	public function delete($id_sekolah_program_kerja){
		$this->load->model('kepemimpinan/m_program_kerja_tahunan');
		$this->m_program_kerja_tahunan->delete($id_sekolah_program_kerja);

		set_success_message('Data Berhasil Dihapus!');
		redirect(site_url('kepemimpinan/program_kerja_tahunan/index/')) ;
	}
	public function delete_sub($id_sekolah_sub_program_kerja,$id_sekolah_program_kerja){
		$id2=$id_sekolah_program_kerja;
		$this->load->model('kepemimpinan/m_program_kerja_tahunan');
		$this->m_program_kerja_tahunan->delete_sub($id_sekolah_sub_program_kerja);
		// echo $id2;

		set_success_message('Data Berhasil Dihapus!');
		redirect(site_url('kepemimpinan/program_kerja_tahunan/load_detail/'.$id2)) ;
	}
	public function submit_add(){
		$this->load->model('kepemimpinan/m_program_kerja_tahunan');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('program_kerja', 'Program Kerja', 'required');
		$this->form_validation->set_rules('periode_awal', 'Periode Awal', 'required');
		$this->form_validation->set_rules('periode_akhir', 'Periode Akhir', 'required');

		if($this->form_validation->run() == TRUE){
			$data['program_kerja'] = $this->input->post('program_kerja');
			$data['tanggal_mulai'] = tanggal_db($this->input->post('periode_awal'));
			$data['tanggal_akhir'] = tanggal_db($this->input->post('periode_akhir'));
			$data['keterangan'] = $this->input->post('keterangan');
			$data['id_tahun_ajaran'] = $this->input->post('id_tahun_ajaran');
			$data['id_sekolah'] = get_id_sekolah();
			$this->m_program_kerja_tahunan->add($data);

			set_success_message('Data Berhasil Ditambah!');
			redirect(site_url('kepemimpinan/program_kerja_tahunan/index/')) ;
		}else{
			set_error_message('Terjadi Kesalahan Pengisian!');
			redirect(site_url('kepemimpinan/program_kerja_tahunan/index/')) ;
		}	
	}
	public function submit_add_sub(){
		$this->load->model('kepemimpinan/m_program_kerja_tahunan');
		$this->load->library('form_validation');

		$this->form_validation->set_rules('sub_program_kerja', 'Program Kerja', 'required');
		$this->form_validation->set_rules('periode_awal', 'Periode Awal', 'required');
		$this->form_validation->set_rules('periode_akhir', 'Periode Akhir', 'required');

		$data['sub_program_kerja'] = $this->input->post('sub_program_kerja');
		$data['tanggal_mulai'] = tanggal_db($this->input->post('periode_awal'));
		$data['tanggal_akhir'] = tanggal_db($this->input->post('periode_akhir'));
		$data['keterangan'] = $this->input->post('keterangan');
		$data['id_sekolah_program_kerja'] = $this->input->post('id_sekolah_program_kerja');
		
		if($this->form_validation->run() == TRUE){
			$this->m_program_kerja_tahunan->add_sub($data);

			set_success_message('Data Berhasil Ditambah!');
			redirect(site_url('kepemimpinan/program_kerja_tahunan/load_detail/'.$data['id_sekolah_program_kerja'])) ;
		}else{
			set_error_message('Terjadi Kesalahan Pengisian!');
			redirect(site_url('kepemimpinan/program_kerja_tahunan/load_detail/'.$data['id_sekolah_program_kerja'])) ;
		}	
	}
	public function submit_edit(){
		$this->load->model('kepemimpinan/m_program_kerja_tahunan');
		$id=$this->input->post('id');
		$data['program_kerja'] = $this->input->post('program_kerja');
		$data['tanggal_mulai'] = tanggal_db($this->input->post('periode_awal'));
		$data['tanggal_akhir'] = tanggal_db($this->input->post('periode_akhir'));
		$data['keterangan'] = $this->input->post('keterangan');
		// echo $id;

		$this->m_program_kerja_tahunan->edit($data,$id);

		set_success_message('Data Berhasil Diupdate!');
		redirect(site_url('kepemimpinan/program_kerja_tahunan/index/')) ;
	}
	public function submit_edit_sub(){
		$this->load->model('kepemimpinan/m_program_kerja_tahunan');
		$id=$this->input->post('id');
		$id2=$this->input->post('id2');
		$data['sub_program_kerja'] = $this->input->post('sub_program_kerja');
		$data['tanggal_mulai'] = tanggal_db($this->input->post('periode_awal'));
		$data['tanggal_akhir'] = tanggal_db($this->input->post('periode_akhir'));
		$data['keterangan'] = $this->input->post('keterangan');
		// echo $id;

		$this->m_program_kerja_tahunan->edit_sub($data,$id);

		set_success_message('Data Berhasil Diupdate!');
		redirect(site_url('kepemimpinan/program_kerja_tahunan/load_detail/'.$id2)) ;
	}

		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */