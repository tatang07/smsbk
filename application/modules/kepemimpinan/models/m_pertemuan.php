<?php
	class m_pertemuan extends MY_Model {
		function getDataPertemuan($id_sekolah,$id_tahun_ajaran){
			$this->db->select('*'); 
			$this->db->from('t_sekolah_pertemuan');
			$this->db->where('id_sekolah', $id_sekolah);
			$this->db->where('id_tahun_ajaran', $id_tahun_ajaran);
			return $this->db->get();
		}

		function getDataPertemuan_ById($id){
			$this->db->select('*'); 
			$this->db->from('t_sekolah_pertemuan');
			$this->db->where('id_sekolah_pertemuan', $id);
			return $this->db->get();
		}

		function getTahunAjaran(){
			$this->db->select('*'); 
			$this->db->from('m_tahun_ajaran');
			return $this->db->get();
		}

		function add($data){
			$this->db->insert('t_sekolah_pertemuan',$data);
		}

		function edit($data, $id){
			$this->db->where('id_sekolah_pertemuan', $id);
			$this->db->update('t_sekolah_pertemuan', $data);
		}

		function delete($id){
			$this->db->where('id_sekolah_pertemuan', $id);
			$this->db->delete('t_sekolah_pertemuan');
		}
	}
?>