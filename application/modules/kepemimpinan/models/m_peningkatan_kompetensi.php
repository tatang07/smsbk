<?php
class m_peningkatan_kompetensi extends MY_Model {
	
	public function get_data_search($nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM t_kepsek_peningkatan_kompetensi as kpk join m_kepala_sekolah as k on k.id_kepala_sekolah = kpk.id_kepsek where kpk.nama_program  like '%$nama%' and k.id_sekolah=$id_sekolah");
		return $query->result_array();
	}
	public function get_data_search_detail($nama,$id_kepsek_peningkatan_kompetensi){
		$query = $this->db->query("SELECT * FROM t_kepsek_peningkatan_kompetensi_detail as kpkd join m_kepala_sekolah as k on k.id_kepala_sekolah = kpkd.id_kepsek where k.nama  like '%$nama%' and kpkd.id_kepsek_peningkatan_kompetensi=$id_kepsek_peningkatan_kompetensi");
		return $query->result_array();
	}
	function get_data($id_sekolah){
			$this->db->select('*');
			$this->db->from('t_kepsek_peningkatan_kompetensi kpk');
			$this->db->join('m_kepala_sekolah k','k.id_kepala_sekolah = kpk.id_kepsek');
			
			$this->db->where('k.id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}
	function get_data_detail($id_kepsek_peningkatan_kompetensi){
			$this->db->select('*');
			$this->db->from('t_kepsek_peningkatan_kompetensi_detail kpkd');
			$this->db->join('m_kepala_sekolah k','k.id_kepala_sekolah = kpkd.id_kepsek');
			
			$this->db->where('kpkd.id_kepsek_peningkatan_kompetensi',$id_kepsek_peningkatan_kompetensi);
			
			return $this->db->get()->result_array();
	}

	function get_nama_program($id){
		$this->db->select('nama_program');
		$this->db->from('t_kepsek_peningkatan_kompetensi');
		$this->db->where('id_kepsek_peningkatan_kompetensi',$id);
		return $this->db->get()->row();
	}

	function get_data_edit($id_kepsek_peningkatan_kompetensi){
			$this->db->select('*');
			$this->db->from('t_kepsek_peningkatan_kompetensi kpk');
			$this->db->join('m_kepala_sekolah k','k.id_kepala_sekolah = kpk.id_kepsek');
			$this->db->where('kpk.id_kepsek_peningkatan_kompetensi',$id_kepsek_peningkatan_kompetensi);
			
			return $this->db->get()->result_array();
	}
		
	function select_kepsek($id_sekolah){
			$this->db->select('*');
			$this->db->from('m_kepala_sekolah');
			$this->db->where('id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}

	function add($data){
			$this->db->insert('t_kepsek_peningkatan_kompetensi',$data);
	}
	
	function add_detail($data){
			$this->db->insert('t_kepsek_peningkatan_kompetensi_detail',$data);
	}
	function edit($data,$id=0){
			$this->db->where('id_kepsek_peningkatan_kompetensi',$id);
			$this->db->update('t_kepsek_peningkatan_kompetensi',$data);
	}

	function delete($id_kepsek_peningkatan_kompetensi){
			$this->db->where('id_kepsek_peningkatan_kompetensi', $id_kepsek_peningkatan_kompetensi);
			$this->db->delete('t_kepsek_peningkatan_kompetensi');
	}
	function delete_detail($id_kepsek_peningkatan_kompetensi_detail){
			$this->db->where('id_kepsek_peningkatan_kompetensi_detail', $id_kepsek_peningkatan_kompetensi_detail);
			$this->db->delete('t_kepsek_peningkatan_kompetensi_detail');
	}
}