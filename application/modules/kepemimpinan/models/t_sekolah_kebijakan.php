<?php
class t_sekolah_kebijakan extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_datalist($id_sekolah, $id_tahun_ajaran, $limit, $start){

			$this->db->limit($limit, $start);
			$this->db->select('*');
			$this->db->from('t_sekolah_kebijakan'); 
			
			$this->db->where('id_sekolah',$id_sekolah);
			$this->db->where('id_tahun_ajaran',$id_tahun_ajaran);
			$query = $this->db->get();
					
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		public function count_datalist($id_sekolah, $id_tahun_ajaran){

			$this->db->select('*');
			$this->db->from('t_sekolah_kebijakan'); 
			
			$this->db->where('id_sekolah',$id_sekolah);
			$this->db->where('id_tahun_ajaran',$id_tahun_ajaran);
			$query = $this->db->get();
					
					
			return $query->num_rows();
			
		}
		
		public function get_all_tahun_ajaran(){
			$query = $this->db
						->from('m_tahun_ajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}
		
		
		public function get_tahun_ajaran_by($id){
			$query = $this->db->query("SELECT * FROM m_tahun_ajaran WHERE id_tahun_ajaran='$id'");
			return $query->row();
		}
				
		public function get_sekolah_kebijakan_by($id){
			$query = $this->db->query("SELECT * FROM t_sekolah_kebijakan WHERE id_sekolah_kebijakan='$id'");
			return $query->row();
		}
		
		public function get_first_tahun_ajaran(){
			$query = $this->db->query("SELECT * FROM m_tahun_ajaran ORDER BY id_tahun_ajaran DESC LIMIT 1;");
			return $query->row();
		}
		
		public function delete($id){
			$this->db->delete('t_sekolah_kebijakan', array('id_sekolah_kebijakan'=>$id));
		}
		
		public function insert($no_surat, $perihal, $alamat_file, $id_sekolah, $id_tahun_ajaran, $keterangan){
			$this->db->query("INSERT INTO t_sekolah_kebijakan(no_surat, perihal, alamat_file, id_sekolah, id_tahun_ajaran, keterangan) VALUES ('$no_surat', '$perihal', '$alamat_file', '$id_sekolah', '$id_tahun_ajaran', '$keterangan')");
		}

		function cek_nomor_surat($nomor_surat){
			$this->db->select('*');
			$this->db->from('t_sekolah_kebijakan');
			$this->db->where('no_surat', $nomor_surat);
			return $this->db->get();
		}
}
