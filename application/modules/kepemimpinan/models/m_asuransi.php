<?php
class m_asuransi extends MY_Model {
    function select_data_asuransi(){
		$this->db->select('*');
		$this->db->from('m_asuransi_kepsek');
		return $this->db->get();
	}

	function get_data_detail($id_asuransi=0){
		$this->db->select('nip,nama,id_asuransi_detail');
		$this->db->from('t_asuransi_kepsek_detail ad');
		$this->db->join('m_kepala_sekolah k', 'k.id_kepala_sekolah = ad.id_kepsek');
		$this->db->where('ad.id_asuransi',$id_asuransi);
		return $this->db->get();
	}
	
	function delete($id_asuransi_detail){
			$this->db->where('id_asuransi_detail', $id_asuransi_detail);
			$this->db->delete('t_asuransi_kepsek_detail');
	}
}