<?php
class m_kinerja_kepsek extends MY_Model {
	
	public function get_kepsek($id_sekolah){
			$query = $this->db
						->from('m_kepala_sekolah')
						->order_by('id_kepala_sekolah')
						->where('id_sekolah',$id_sekolah)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
	}
	
	
	public function get_data_search($id_tingkat_kelas, $nama){
		$query = $this->db->query("SELECT * FROM t_siswa_catatan_bk as sc join t_rombel_detail as rd on rd.id_siswa=sc.id_siswa join m_rombel as r on r.id_rombel =rd.id_rombel join m_siswa as s on s.id_siswa = rd.id_siswa where s.nama like '%$nama%' and r.id_tingkat_kelas = '$id_tingkat_kelas' ");
		return $query->result_array();
	}
	
	function get_data($id_kepsek){
			$this->db->select('*');
			$this->db->from(' t_penilaian_kinerja_kepsek pkg');
			// $this->db->join('t_rombel_detail rd','rd.id_siswa = sc.id_siswa');
			// $this->db->join('m_rombel r', 'r.id_rombel = rd.id_rombel');
			// $this->db->join('m_siswa s', 's.id_siswa = rd.id_siswa');
			
			$this->db->where('pkg.id_kepsek',$id_kepsek);
			
			return $this->db->get()->result_array();
	}
	function get_siswa($id_sekolah,$id_tingkat_kelas){
			$this->db->select('*');
			$this->db->from('t_rombel_detail rd');
			$this->db->join('m_siswa s','s.id_siswa = rd.id_siswa');		
			$this->db->join('m_rombel r','r.id_rombel = rd.id_rombel');
			$this->db->where('id_sekolah',$id_sekolah);
			$this->db->where('r.id_tingkat_kelas',$id_tingkat_kelas);
			return $this->db->get()->result_array();
	}

	function select_data_detail($id_penilaian_kinerja_kepsek){
			$this->db->select('*');
			$this->db->from('t_penilaian_kinerja_kepsek_detail pkgd');
			$this->db->join('t_penilaian_kinerja_kepsek pkg','pkgd.id_penilaian_kinerja_kepsek=pkg.id_penilaian_kinerja_kepsek');
			$this->db->join('m_kepala_sekolah g','pkg.id_kepsek = g.id_kepala_sekolah');
			
			$this->db->where('pkg.id_penilaian_kinerja_kepsek',$id_penilaian_kinerja_kepsek);
			$this->db->group_by('pkgd.id_penilaian_kinerja_kepsek');
			return $this->db->get()->result_array();
	}
	
	function select_data_butir($id_penilaian_kinerja_kepsek){
			$this->db->select('*');
			$this->db->from('t_penilaian_kinerja_kepsek_detail pkgd');
			$this->db->where('id_penilaian_kinerja_kepsek',$id_penilaian_kinerja_kepsek);
			return $this->db->get()->result_array();
	}
	
	function select_kelas($id_jenjang_sekolah){
			$this->db->select('*');
			$this->db->from('m_tingkat_kelas');
			$this->db->where('id_jenjang_sekolah',$id_jenjang_sekolah);
			
			return $this->db->get()->result_array();
	}
	
	function select_kepsek(){
			$this->db->select('*');
			$this->db->from('m_kepala_sekolah');
			$this->db->where('id_sekolah',get_id_sekolah());
			$this->db->where('status_aktif',1);
			
			return $this->db->get()->result_array();
	}
	
	function select_jenis_kompetensi_kepsek(){
			$this->db->select('*');
			$this->db->from('r_jenis_kompetensi_kepsek');
			
			return $this->db->get()->result_array();
	}
	
	function select_butir_kompetensi_kepsek(){
			$this->db->select('*');
			$this->db->from('r_butir_kompetensi_kepsek');
			
			return $this->db->get()->result_array();
	}

	function get_pk_kepsek($id, $periode_awal, $periode_akhir, $penilai, $kesimpulan){
			$this->db->select('*');
			$this->db->from('t_penilaian_kinerja_kepsek');
			$this->db->where('id_kepsek',$id);
			$this->db->where('periode_awal',$periode_awal);
			$this->db->where('periode_akhir',$periode_akhir);
			$this->db->where('penilai',$penilai);
			$this->db->where('kesimpulan',$kesimpulan);
			$this->db->group_by('id_penilaian_kinerja_kepsek');
			
			return $this->db->get()->result_array();
	}
	// function get_data_sub($id){
			// $this->db->select('*');
			// $this->db->from('t_sekolah_sub_program_kerja');
			// $this->db->where('id_sekolah_program_kerja',$id);
			
			// return $this->db->get()->result_array();
	// }

	function add($data){
			$this->db->insert('t_penilaian_kinerja_kepsek',$data);
	}


	function add_detail($data){
			$this->db->insert('t_penilaian_kinerja_kepsek_detail',$data);
	}
	// function add_sub($data){
			// $this->db->insert('t_sekolah_sub_program_kerja',$data);
	// }

	function edit($data,$id=0){
			$this->db->where('id_siswa_catatan_bk',$id);
			$this->db->update('t_siswa_catatan_bk',$data);
	}

	function delete($id_penilaian_kinerja_kepsek){
			$this->db->where('id_penilaian_kinerja_kepsek', $id_penilaian_kinerja_kepsek);
			$this->db->delete('t_penilaian_kinerja_kepsek');
	}
	

}