 <?php
class m_evaluasi_diri extends MY_Model {
	function get_kepsek($id_sekolah){
		$this->db->select('*');
		$this->db->from('m_kepala_sekolah');
		$this->db->where('id_sekolah',$id_sekolah);
		return $this->db->get();
	}

	function get_data($id_kepsek){
		$this->db->select('*');
		$this->db->from('t_kepsek_evaluasi');
		$this->db->where('id_kepsek',$id_kepsek);	
		return $this->db->get();
	}

	function get_data_by($id){
		$this->db->select('*');
		$this->db->from('t_kepsek_evaluasi');
		$this->db->where('id_kepsek_evaluasi',$id);	
		return $this->db->get();
	}

 	function insert($id_kepala_sekolah, $tgl_mulai, $tgl_akhir, $lokasi_file){
		$this->db->query("INSERT INTO t_kepsek_evaluasi(id_kepsek, tgl_mulai, tgl_akhir, lokasi_file) VALUES ('$id_kepala_sekolah', '$tgl_mulai', '$tgl_akhir', '$lokasi_file');");
	}

	function delete($id){
		$this->db->where('id_kepsek_evaluasi', $id);
		$this->db->delete('t_kepsek_evaluasi');
	}
}