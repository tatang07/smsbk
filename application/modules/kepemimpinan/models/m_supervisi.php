<?php

class M_supervisi extends MY_Model {

    public $table = "v_supervisi";
    public $id = "id_supervisi";

    function get_supervisi_detail($id_supervisi)
    {
    	$res = $this->db->from('t_supervisi_detail d')
    			->join('r_butir_supervisi b', 'b.id_butir_supervisi = d.id_butir_supervisi')
    			->where('id_supervisi', $id_supervisi)
    			->get();

    	if($res->num_rows() > 0)
    		return $res->result_array();

    	return false;
    }

    function get_butir_supervisi()
    {
    	return $this->db->get('r_butir_supervisi')->result_array();
    }

    function get_guru_gmr($id)
    {
    	$res = $this->db->select('gmr.id_guru, nama')
    					->from('t_guru_matpel_rombel gmr')
                        ->join('m_guru g', 'g.id_guru = gmr.id_guru')
    					->where('g.id_sekolah', $id )
    					->group_by('gmr.id_guru')
    					->get();

    	if($res->num_rows() > 0)
    		return $res->result_array();

    	return false;
    }

    function get_matpel_rombel($id_guru)
    {
    	$res = $this->db->from('t_guru_matpel_rombel gmr')
    					->join('m_rombel r', 'r.id_rombel = gmr.id_rombel')
    					->join('m_pelajaran p', 'p.id_pelajaran = gmr.id_pelajaran')
    					->where('gmr.id_guru', $id_guru)
    					->where('id_tahun_ajaran', get_id_tahun_ajaran())
    					->get();

    	if($res->num_rows() > 0)
    		return $res->result_array();

    	return false;
    }

    function insert_supervisi($data)
    {
    	$this->db->insert('t_supervisi', $data);
    	return $this->db->insert_id();
    }

    function insert_supervisi_detail($data)
    {
    	return $this->db->insert_batch('t_supervisi_detail', $data);
    }

    function delete_supervisi($id_supervisi){
    	if($this->db->where('id_supervisi', $id_supervisi)->delete('t_supervisi') &&
    		$this->db->where('id_supervisi', $id_supervisi)->delete('t_supervisi_detail'))
    		return true;

    	return false;
    }
}