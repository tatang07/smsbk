<?php
class m_sertifikasi extends MY_Model {
	
	public function get_data_search($nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM t_kepsek_sertifikasi as ks join m_kepala_sekolah as k on k.id_kepala_sekolah = ks.id_kepsek where k.nama  like '%$nama%' and k.id_sekolah=$id_sekolah");
		return $query->result_array();
	}
	public function get_data_search_default($nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM t_kepsek_sertifikasi as ks join m_kepala_sekolah as k on k.id_kepala_sekolah = ks.id_kepsek where k.nama  like '%$nama%' and k.id_sekolah=$id_sekolah");
		return $query->result_array();
	}

	function get_data($id_sekolah){
			$this->db->select('*');
			$this->db->from('t_kepsek_sertifikasi ks');
			$this->db->join('m_kepala_sekolah k','k.id_kepala_sekolah = ks.id_kepsek');
			
			$this->db->where('k.id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}
	function get_siswa($id_sekolah,$id_tingkat_kelas){
			$this->db->select('*');
			$this->db->from('t_rombel_detail rd');
			$this->db->join('m_siswa s','s.id_siswa = rd.id_siswa');		
			$this->db->join('m_rombel r','r.id_rombel = rd.id_rombel');
			$this->db->where('s.id_sekolah',$id_sekolah);
			$this->db->where('r.id_tingkat_kelas',$id_tingkat_kelas);
			return $this->db->get()->result_array();
	}

	function get_data_edit($id_kepsek_sertifikasi){
			$this->db->select('*');
			$this->db->from('t_kepsek_sertifikasi ks');
			$this->db->join('m_kepala_sekolah k','k.id_kepala_sekolah = ks.id_kepsek');
			$this->db->where('ks.id_kepsek_sertifikasi',$id_kepsek_sertifikasi);
			
			return $this->db->get()->result_array();
	}
	
	
	function select_kepsek($id_sekolah){
			$this->db->select('*');
			$this->db->from('m_kepala_sekolah');
			$this->db->where('id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}

	function add($data){
			$this->db->insert('t_kepsek_sertifikasi',$data);
	}

	function edit($data,$id=0){
			$this->db->where('id_kepsek_sertifikasi',$id);
			$this->db->update('t_kepsek_sertifikasi',$data);
	}

	function delete($id_guru_sertifikasi){
			$this->db->where('id_kepsek_sertifikasi', $id_guru_sertifikasi);
			$this->db->delete('t_kepsek_sertifikasi');
	}

}