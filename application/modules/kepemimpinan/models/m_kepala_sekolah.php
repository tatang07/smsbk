<?php
class m_kepala_sekolah extends MY_Model {
	public $table = "m_kepala_sekolah";
    public $id = "id_kepala_sekolah";
	
	public function get_kepsek(){
		$id=get_id_sekolah();
		$this->db->select('*');
		$this->db->from('m_kepala_sekolah kepsek');
		$this->db->join('r_jenjang_pendidikan jp', 'jp.id_jenjang_pendidikan = kepsek.id_jenjang_pendidikan');
		$this->db->join('r_pangkat_golongan pg', 'pg.id_pangkat_golongan = kepsek.id_pangkat_golongan');
		$this->db->join('m_sekolah s', 's.id_sekolah = kepsek.id_sekolah');
		$this->db->join('r_status_sekolah ss', 'ss.id_status_sekolah = s.id_status_sekolah');
		$this->db->where('kepsek.id_sekolah',$id);
		$this->db->order_by('kepsek.tgl_sk','desc');
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_kepsek_by_id($id=0){
		$this->db->select('*');
		$this->db->from('m_kepala_sekolah kepsek');
		$this->db->join('r_jenjang_pendidikan jp', 'jp.id_jenjang_pendidikan = kepsek.id_jenjang_pendidikan');
		$this->db->join('r_pangkat_golongan pg', 'pg.id_pangkat_golongan = kepsek.id_pangkat_golongan');
		$this->db->where('kepsek.id_kepala_sekolah',$id);
		$this->db->order_by('kepsek.tgl_sk','desc');
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_detail_by_id($id=0){
		$this->db->select('*');
		$this->db->from('m_kepala_sekolah');
		$this->db->where('id_kepala_sekolah',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	public function get_all_jenjang(){
		$this->db->select('*');
		$this->db->from('r_jenjang_pendidikan');
		$this->db->order_by('jenjang_pendidikan','asc');
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_all_pangkat(){
		$this->db->select('*');
		$this->db->from('r_pangkat_golongan');
		$this->db->order_by('golongan','asc');
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function edit($data, $id){
		$this->db->where('id_kepala_sekolah', $id);
		$this->db->update('m_kepala_sekolah', $data);
	}
	
	function add($data){
		$this->db->insert('m_kepala_sekolah', $data);
	}
	
	function cek_nip($nip){
		$this->db->select('*');
		$this->db->from('m_kepala_sekolah');
		$this->db->where('nip', $nip);
		return $this->db->get();
	}
}
