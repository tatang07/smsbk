<?php
class m_program_kerja_tahunan extends MY_Model {
	
	public function get_tahun_ajaran(){
			$query = $this->db
						->from('m_tahun_ajaran')
						->order_by('id_tahun_ajaran')
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
	}
	
	public function get_tahun_ajaran_by_id($id){
			$query = $this->db
						->from('m_tahun_ajaran')
						->where('id_tahun_ajaran',$id)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
	}
	function get_data($id_tahun_ajaran,$id_sekolah){
			$this->db->select('*');
			$this->db->from('t_sekolah_program_kerja');
			$this->db->where('id_tahun_ajaran',$id_tahun_ajaran);
			$this->db->where('id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}
	function get_data_by_id($id){
			$this->db->select('*');
			$this->db->from('t_sekolah_program_kerja');
			$this->db->where('id_sekolah_program_kerja',$id);
			
			return $this->db->get()->result_array();
	}
	function get_data_sub_by_id($id){
			$this->db->select('*');
			$this->db->from('t_sekolah_sub_program_kerja');
			$this->db->where('id_sekolah_sub_program_kerja',$id);
			
			return $this->db->get()->result_array();
	}
	function get_data_sub($id){
			$this->db->select('*');
			$this->db->from('t_sekolah_sub_program_kerja');
			$this->db->where('id_sekolah_program_kerja',$id);
			
			return $this->db->get()->result_array();
	}
	function add($data){
			$this->db->insert('t_sekolah_program_kerja',$data);
	}
	function add_sub($data){
			$this->db->insert('t_sekolah_sub_program_kerja',$data);
	}
	function edit($data,$id=0){
			$this->db->where('id_sekolah_program_kerja',$id);
			$this->db->update('t_sekolah_program_kerja',$data);
	}
	function edit_sub($data,$id=0){
			$this->db->where('id_sekolah_sub_program_kerja',$id);
			$this->db->update('t_sekolah_sub_program_kerja',$data);
	}
	function delete($id_sekolah_program_kerja){
			$this->db->where('id_sekolah_program_kerja', $id_sekolah_program_kerja);
			$this->db->delete('t_sekolah_program_kerja');
	}
	function delete_sub($id_sekolah_sub_program_kerja){
			$this->db->where('id_sekolah_sub_program_kerja', $id_sekolah_sub_program_kerja);
			$this->db->delete('t_sekolah_sub_program_kerja');
	}
	// function hitung_beban(){
			// $this->db->select('*');
			// $this->db->from('m_alokasi_waktu_per_minggu awpm');
			// $this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_pelajaran = awpm.id_pelajaran');
			// return $this->db->get()->result_array();
	// }

}