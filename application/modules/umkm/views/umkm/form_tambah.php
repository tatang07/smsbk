	<h1 class="grid_12">Pengelolaan Sekolah</h1>
			
			<form action="<?php echo base_url('umkm/umkm/submit_post'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah <?php echo $judul; ?></legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong><?php echo $judul; ?></strong>
						</label>
						<div>
							<textarea rows=5 name="<?php echo $judul; ?>" id="f1_textarea_grow"></textarea>
							<input type="hidden" name="id" value="<?php echo '0'; ?>" />
							<input type="hidden" name="id_sekolah" value="<?php echo get_id_sekolah(); ?>" />
							<input type="hidden" name="action" value="add" />
							<input type="hidden" name="judul" value="<?php echo $judul; ?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_textarea">
							<strong>Nama UMKM</strong>
						</label>
						<div>
							<input type="text" name="nama" value="" />
						</div>
					</div>

					<div class="row">
						<label for="f1_textarea">
							<strong>Tanggal Berdiri</strong>
						</label>
						<div>
							<input type="text" name="tanggalberdiri" value="" />
						</div>
					</div>

					<div class="row">
						<label for="f1_textarea">
							<strong>Deskripsi</strong>
						</label>
						<div>
							<textarea rows=5 name="deskripsi" id="deskripsi"></textarea>
						</div>
					</div>

					<div class="row">
						<label for="f1_textarea">
							<strong>Nomor Telepon</strong>
						</label>
						<div>
							<input type="text" name="phone" value="" />
						</div>
					</div>

					<div class="row">
						<label for="f1_textarea">
							<strong>Alamat</strong>
						</label>
						<div>
							<textarea rows=5 name="alamat" id="alamat"></textarea>
						</div>
					</div>

					<div class="row">
						<label for="f1_textarea">
							<strong>Layanan UMKM</strong>
						</label>
						<div class="col-md-4">
                            <div class="mt-checkbox-list" data-error-container="#form_2_services_error">
                                <label class="mt-checkbox">
                                    <input type="checkbox" value="online" name="service_online" checked="checked"> Online
                                    <span></span>
                                </label>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <label class="mt-checkbox">
                                    <input type="checkbox" value="offline" name="service_offline"> Offline
                                    <span></span>
                                </label>
                                
                            </div>
                            
                            <div id="form_2_services_error"> </div>
                        </div>
					</div>

					<div class="row">
						<label for="f1_textarea">
							<strong>Produk Unggulan</strong>
						</label>
						<div>
							<input type="text" name="produk" data-role="tagsinput" class="form-control"> 
                            <span class="help-block"> *beri koma (,) untuk input </span>
						</div>
					</div>

					<div class="row">
						<label for="f1_textarea">
							<strong>Catatan</strong>
						</label>
						<div>
							<textarea rows=5 name="catatan" id="catatan"></textarea>
						</div>
					</div>

					<div class="row">
						<label for="f1_textarea">
							<strong>Logo UMKM</strong>
						</label>
						<div>
							<input type="file" name="userfile" value="" />
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('umkm/umkm/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		