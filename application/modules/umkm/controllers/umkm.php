<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class umkm extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_umkm');
			$this->load->library('form_validation');
		}
		
		public function index(){
			redirect('umkm/umkm/home');
		}
		
		public function home(){
			
			
			$data['umkm'] = $this->m_umkm->get_all_umkm();
			
			$data['component']="umkm_siswa";
			render("umkm/umkm", $data);
		}
	
		public function tambah(){
			$data['judul'] = 'UMKM';
			
			$data['component']="UMKM Siswa";
			render("umkm/form_tambah", $data);
		}
		
		public function edit_visi(){
			$id = $this->uri->segment(4);
			if ($id == NULL) {
				redirect('pengelolaan_sekolah/visi_misi/home');
			}
			
			$row = $this->m_umkm->get_visi_by($id);
			$data['id'] = $id;
			$data['visi'] = $row->visi;
			$data['urutan'] = $row->urutan;
			$data['judul'] = "Visi";
			
			$data['component']="pengelolaan_sekolah";
			render("visi_misi/form_edit", $data);
		}
		
		public function edit_misi(){
			$id = $this->uri->segment(4);
			if ($id == NULL) {
				redirect('pengelolaan_sekolah/visi_misi/home');
			}
			
			$row = $this->m_umkm->get_misi_by($id);
			$data['id'] = $id;
			$data['misi'] = $row->misi;
			$data['urutan'] = $row->urutan;
			$data['judul'] = "Misi";
			
			$data['component']="pengelolaan_sekolah";
			render("visi_misi/form_edit", $data);
		}
		
		public function delete_visi($id){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('pengelolaan_sekolah/visi_misi/home');
			}
			
			$this->m_umkm->delete_visi($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('pengelolaan_sekolah/visi_misi/home');
		}
		
		public function delete_misi($id){
			$id = $this->uri->segment(4);
			if ($id == NULL){
				redirect('pengelolaan_sekolah/visi_misi/home');
			}
			
			$this->m_umkm->delete_misi($id);
			set_success_message('Data Berhasil Dihapus!');
			redirect('pengelolaan_sekolah/visi_misi/home');
		}
		
		public function submit_post(){
			$judul = $this->input->post('judul');
			if($judul == "Visi"){
				$this->form_validation->set_rules('Visi', 'Visi', 'required');
			}else{
				$this->form_validation->set_rules('Misi', 'Misi', 'required');
			}
			$this->form_validation->set_rules('urutan', 'Urutan', 'required');
			// $this->form_validation->set_rules('jumlah_jam', 'Jumlah Jam', 'required');
			
			if($this->form_validation->run() == TRUE){
			
				if($judul == "Visi"){
					$visi = $this->input->post('Visi');
				}else{
					$misi = $this->input->post('Misi');
				}
				
				$urutan = $this->input->post('urutan');
				$id = $this->input->post('id');
				$id_sekolah = $this->input->post('id_sekolah');
				
				$action = $this->input->post('action');
				if($action == 'update'){
					if($judul == "Visi"){
						$this->m_umkm->update_visi($id, $visi, $urutan);
					}else{
						$this->m_umkm->update_visi($id, $misi, $urutan);
					}
					set_success_message('Data Berhasil Diupdate!');
					redirect("pengelolaan_sekolah/visi_misi/home");
				}else{
					if($judul == "Visi"){
						$this->m_umkm->tambah_visi($id_sekolah, $visi, $urutan);
					}else{
						$this->m_umkm->tambah_misi($id_sekolah, $misi, $urutan);
					}
					
					set_success_message('Data Berhasil Ditambah!');
					redirect("pengelolaan_sekolah/visi_misi/home");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian Data!');
				redirect("pengelolaan_sekolah/visi_misi/home/");
			}
		}
		
}
