<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class mutasi_siswa extends Simple_Controller {

    	public $myconf = array(
			//Nama controller
			"name" 			=> "pps/mutasi_siswa",
			"component"		=> "pps",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Pelayanan dan Pembinaan Siswa",
			"subtitle"		=> "Mutasi Siswa",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_mutasi_siswa", "id"=>"id_mutasi_siswa"),
			"model_name"	=> "t_mutasi",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"id_siswa"=>"Nama",
								"id_jenis_mutasi_siswa"=>"Jenis Mutasi",
								"nis"=>"Nis",					
								"jenis_kelamin"=>"Jenis Kelamin",					
								"nama"=>"Nama",					
								"tanggal_mutasi"=>"Tanggal Mutasi",					
								"jenis_mutasi"=>"Jenis Mutasi",		
								"keterangan"=>"Keterangan"					
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								"id_jenis_mutasi_siswa"=>array("r_jenis_mutasi_siswa","id_jenis_mutasi_siswa","jenis_mutasi"),
								"id_siswa"=>array("m_siswa","id_siswa","nama", array("id_sekolah"=>1), array("nama")),
								"tanggal_mutasi"=> "date"
							),
			//Field-field yang harus diisi
			// "data_mandatory"=> array(
			// 					"mutasi_siswa"
			// 				),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			// "data_unique"	=> array(
			// 					"mutasi_siswa"
			// 				),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("nis","nama", "jenis_kelamin","tanggal_mutasi","jenis_mutasi","keterangan"),
								"field_sort"			=> array("nis","nama", "jenis_kelamin","tanggal_mutasi","jenis_mutasi","keterangan"),
								"field_filter"			=> array("nis","nama", "jenis_kelamin","tanggal_mutasi","jenis_mutasi","keterangan"),
								/*
								"field_filter_dropdown"	=> array(
																"id_jenis_akreditasi"=>array("label"=>"Akreditasi","table"=>"r_jenis_akreditasi", "table_id"=>"id_jenis_akreditasi", "table_label"=>"jenis_akreditasi"),
																"id_status_mutasi_siswa"=>array("label"=>"Status","list"=>array(1=>"Negeri",2=>"Swasta")),
															),
															*/
								"field_comparator"		=> array("ms.id_sekolah"=>"="),
								"field_operator"		=> array("ms.id_sekolah"=>"AND"),
								"field_align"			=> array("jenis_kelamin"=>"center", "jenis_mutasi"=>"center", "tanggal_mutasi"=>"center"),
								"field_size"			=> array("action_col"=>"15%"),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array(
																),
								//"custom_add_link"		=> array('label'=>'Add','href'=>'school/add' ),
								//"custom_edit_link"		=> array(),
								//"custom_delete_link"	=> array(),
								"custom_link"			=> array(
														),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("id_siswa","tanggal_mutasi","keterangan","id_jenis_mutasi_siswa"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								//"redirect_link"			=> ""
							),
			//Konfigurasi tampilan edit
			/*"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"view_file"				=> "simple/edit",
								"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array(),
								"custom_action"			=> "",
								"msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								"msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);	
		
		public function before_render_add($data){
			$data['conf']['data_type']['id_siswa']['list'] = get_list_filter("m_siswa","id_siswa",
								array("nama"),
								array('id_sekolah'=>get_id_sekolah()));
			return $data;
		}

		public function before_render_edit($data){
			$data['conf']['data_type']['id_siswa']['list'] = get_list_filter("m_siswa","id_siswa",
								array("nama"),
								array('id_sekolah'=>get_id_sekolah()));
			return $data;
		}

		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
			$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			// $_POST['filter']['id_program'] = $id_program;
			$_POST['filter']['ms.id_sekolah'] = get_id_sekolah();
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															//'id_program'=>$id_program
															'id_sekolah'=>get_id_sekolah() 
														);
			
			//Mengganti link add yang ada di kanan atas form datalist
			$data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'pps/mutasi_siswa/add/');			
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
		
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'pps/mutasi_siswa/edit');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'pps/mutasi_siswa/delete');
			
			return $data;
		}

		public function after_add($data){
			if($this->input->post()){
				$ids = $data['item']['id_siswa']; 
				$fakta['status_aktif'] = 0;
				
				$this->t_mutasi->update_status_siswa($fakta, $ids);
			}
			return $data;
		}
		
}
