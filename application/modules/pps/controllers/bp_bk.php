<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class bp_bk extends CI_Controller {

	public function index()
	{	
		$this->load->model('pps/m_bp_bk');
		$id_sekolah = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		$data['ta'] = $this->m_bp_bk->get_kelas($jenjang_sekolah);
		$data['isi'] = $this->m_bp_bk->get_data($id_sekolah);
		$data['component']="pps";
		render('bp_bk/bp_bk',$data);
	}
	public function load_add()
	{	
		$this->load->model('pps/m_bp_bk');
		$id_sekolah= get_id_sekolah();
		// $data['tabi'] = $this->m_bp_bk->get_siswa($id_sekolah);
		$data['tingkat_kelas']=0;
		$jenjang_sekolah = get_jenjang_sekolah();
		$data['kelas'] = $this->m_bp_bk->select_kelas($jenjang_sekolah);
		$data['component']="pps";
		render('bp_bk/form_tambah',$data);
	}
	// public function load_add_sub($id_sekolah_program_kerja)
	// {	
		// $this->load->model('pps/m_bp_bk');
		// $data['id_sekolah_program_kerja']=$id_sekolah_program_kerja;
		// render('bp_bk/form_tambah_sub',$data);
	// }
	// public function load_detail($id_sekolah_program_kerja)
	// {	
		// $this->load->model('pps/m_bp_bk');
		// $data['program_kerja'] = $this->m_bp_bk->get_data_by_id($id_sekolah_program_kerja);
		// $data['sub'] = $this->m_bp_bk->get_data_sub($id_sekolah_program_kerja);
		// render('bp_bk/sub_bp_bk',$data);
	// }
	public function load_edit($id_siswa_catatan_bk)
	{	
		$this->load->model('pps/m_bp_bk');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		$data['kelas'] = $this->m_bp_bk->select_kelas($jenjang_sekolah);
		
		$data['data_edit'] = $this->m_bp_bk->get_data_edit($id_siswa_catatan_bk);
		foreach($data['data_edit'] as $a){
			$id_tingkat_kelas=$a['id_tingkat_kelas'];
		}
		$data['tabi'] = $this->m_bp_bk->get_siswa($id_sekolah,$id_tingkat_kelas);
		$data['component']="pps";
		render('bp_bk/form_edit',$data);
	}
	// public function load_edit_sub($id_sekolah_sub_program_kerja,$id_sekolah_program_kerja)
	// {	
		// $this->load->model('pps/m_bp_bk');
		// $data['data_edit'] = $this->m_bp_bk->get_data_sub_by_id($id_sekolah_sub_program_kerja);
		// $data['id']=$id_sekolah_sub_program_kerja;
		// $data['id2']=$id_sekolah_program_kerja;
		// render('bp_bk/form_edit_sub',$data);
	// }
	public function delete($id_siswa_catatan_bk){
			$this->load->model('pps/m_bp_bk');
			$this->m_bp_bk->delete($id_siswa_catatan_bk);
			redirect(site_url('pps/bp_bk/index/')) ;
	}
	// public function delete_sub($id_sekolah_sub_program_kerja,$id_sekolah_program_kerja){
			// $id2=$id_sekolah_program_kerja;
			// $this->load->model('pps/m_bp_bk');
			// $this->m_bp_bk->delete_sub($id_sekolah_sub_program_kerja);

			// redirect(site_url('pps/bp_bk/load_detail/'.$id2)) ;
	// }
	public function submit_add(){
			$this->load->model('pps/m_bp_bk');
			$data['id_siswa'] = $this->input->post('id_siswa');
			$data['tanggal_catatan'] = tanggal_db($this->input->post('tanggal'));
			$data['keterangan'] = $this->input->post('keterangan');
			$this->m_bp_bk->add($data);
			redirect(site_url('pps/bp_bk/index/')) ;
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('pps/m_bp_bk');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_bp_bk->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_bp_bk->select_kelas($jenjang_sekolah);
			render('bp_bk/form_tambah',$data);
	}

	// public function submit_add_sub(){
			// $this->load->model('pps/m_bp_bk');
			// $data['sub_program_kerja'] = $this->input->post('sub_program_kerja');
			// $data['tanggal_mulai'] = tanggal_db($this->input->post('periode_awal'));
			// $data['tanggal_akhir'] = tanggal_db($this->input->post('periode_akhir'));
			// $data['keterangan'] = $this->input->post('keterangan');
			// $data['id_sekolah_program_kerja'] = $this->input->post('id_sekolah_program_kerja');
			// $this->m_bp_bk->add_sub($data);

			// redirect(site_url('pps/bp_bk/load_detail/'.$data['id_sekolah_program_kerja'])) ;
	// }
	public function submit_edit(){
			$this->load->model('pps/m_bp_bk');
			$id=$this->input->post('id');
			$data['id_siswa'] = $this->input->post('id_siswa');
			$data['tanggal_catatan'] = tanggal_db($this->input->post('tanggal'));
			$data['keterangan'] = $this->input->post('keterangan');


			$this->m_bp_bk->edit($data,$id);
			redirect(site_url('pps/bp_bk/index/')) ;
	}
	public function search(){
		$this->load->model('pps/m_bp_bk');
		$nama = $this->input->post('nama');
		$id_tingkat_kelas = $this->input->post('id_tingkat_kelas');
		$id_sekolah = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		$data['ta'] = $this->m_bp_bk->get_kelas($jenjang_sekolah);
		$data['isi'] = $this->m_bp_bk->get_data_search($id_tingkat_kelas,$nama);
		$data['component']="pps";
		render('bp_bk/bp_bk',$data);
		// $data['kurikulum'] = $this->m_dokumen_kurikulum->get_all_kurikulum();
		// $data['alldata'] = $this->m_dokumen_kurikulum->get_data_search($nama, $dokumen);
		// $data['jumlah'] = $this->m_dokumen_kurikulum->get_data_search_count($nama, $dokumen);
	
		 // $data['nama'] = $nama;
		 // $data['dokumen'] = $dokumen;
		
		// render("dokumen_kurikulum/datalist", $data);
	}
	
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */