<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class gender extends Simple_Controller {	
	
	public function index(){			
		$this->load->model('pps/m_gender');
		$data['gender'] = $this->m_gender->select_gender()->result_array();
		$data['lp'] = $this->m_gender->lp()->result_array();
		$data['tingkat'] = $this->m_gender->select_tingkat()->result_array();	
		// $this->mydebug($data['gender']);
		
		$gender=array();
		foreach($data['tingkat'] as $tingkat){
			foreach($data['lp'] as $lp){
				if($lp['jenis_kelamin'] == 'l' || $lp['jenis_kelamin'] == 'L'){
					$lp['jenis_kelamin'] = 'l';
					$gender[$tingkat['tingkat_kelas']][$lp['jenis_kelamin']] = 0;
				}elseif($lp['jenis_kelamin'] == 'p' || $lp['jenis_kelamin'] == 'P') {
					$lp['jenis_kelamin'] = 'p';
					$gender[$tingkat['tingkat_kelas']][$lp['jenis_kelamin']] = 0;
				}
				
			}
		}
		
		foreach($data['gender'] as $d){
			if($d['jenis_kelamin'] == 'l' || $d['jenis_kelamin'] == 'L'){
					$d['jenis_kelamin'] = 'l';
					$gender[$d['tingkat_kelas']] [$d['jenis_kelamin']]=$d['total'];
				}elseif($d['jenis_kelamin'] == 'p' || $d['jenis_kelamin'] == 'P') {
					$d['jenis_kelamin'] = 'p';
					$gender[$d['tingkat_kelas']] [$d['jenis_kelamin']]=$d['total'];
				}
			
		}
		
		$data['hasil']=$gender;	
		$data['component']="pps";
		
		render('gender',$data);
	}
	
}