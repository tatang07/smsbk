<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT tk.id_tingkat_kelas, tk.tingkat_kelas, s.id_rekap_siswa, count(id_rekap_siswa) as rekap_siswa FROM `t_rombel_detail` as rd join m_siswa as s on rd.id_siswa=s.id_siswa join m_rombel as r on rd.id_rombel=r.id_rombel join m_tingkat_kelas as tk on r.id_tingkat_kelas=tk.id_tingkat_kelas group by tk.id_tingkat_kelas ,id_rekap_siswa

class rekap_siswa extends Simple_Controller {

	public function index()
	{	
		$this->load->model('pps/m_rekap_siswa');
		
		$data['tingkat_kelas'] = $this->m_rekap_siswa->get_all_tingkat(get_jenjang_sekolah());
		$data['rombel'] = $this->m_rekap_siswa->get_all_rombel();
		$data['jumlah'] = $this->m_rekap_siswa->get_siswa();
		
		// $this->mydebug($data);
		$data['component']="pps";
		render('rekap_siswa/rekap_siswa',$data);
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */