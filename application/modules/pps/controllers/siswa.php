<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class siswa extends Simple_Controller {
    	public $myconf = array(
			//Nama controller
			"name" 			=> "pps/siswa",
			"component"		=> "pps",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Pelayanan dan Pembinaan Siswa",
			"subtitle"		=> "Identitas Siswa",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "t_siswa",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"nis"=>"NIS",
								"nisn"=>"NISN",
								"nama"=>"Nama",					
								"telepon"=>"No Telepon",
								"tempat_lahir"=>"Tempat Lahir",
								"tanggal_lahir"=>"Tanggal Lahir",
								"jenis_kelamin"=>"Jenis Kelamin",
								"anak_ke"=>"Anak Ke",
								"jumlah_sdr"=>"Jumlah Saudara",
								"alamat"=>"Alamat",
								"e_mail"=>"E-mail",
								"kode_pos"=>"Kode Pos",
								"foto"=>"Foto",
								"status_aktif"=>"Status Aktif",
								"id_agama"=>"Agama",
								"id_status_anak"=>"Status Anak",
								"id_golongan_darah"=>"Golongan Darah",
								"id_kecamatan"=>"Kecamatan",
								"id_asal_sekolah"=>"Asal Sekolah",
								"rombel"=>"Kelas"
								
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								//"tanggal"=>"date",
								//"id_pihak_komunikasi"=>"hidden",
								"id_siswa"=>"hidden",
								"jenis_kelamin"=>array("list"=>array("l"=>"Laki-laki","p"=>"Permpuan")),
								"tanggal_lahir"=>"date",
								"alamat"=>"text",
								"foto"=>"file",
								"id_sekolah"=>"hidden",
								"id_golongan_darah"=>array("r_golongan_darah","id_golongan_darah","golongan_darah"),
								"id_agama"=>array("r_agama","id_agama","agama"),
								"id_status_anak"=>array("r_status_anak","id_status_anak","status_anak"),
								"id_kecamatan"=>array("r_kecamatan","id_kecamatan","kecamatan"),
								"id_asal_sekolah"=>array("r_asal_sekolah","id_asal_sekolah","nama_sekolah"),
								"status_aktif"=>array("list"=>array("1"=>"Aktif","0"=>"Tidak Aktif")),
								
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"nis","nisn","nama","telepon","nama","nis"
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			// "data_unique"	=> array(
			// 					"telepon"
			// 				),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"model_name"			=> "v_siswa",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("nis","nisn","nama","telepon","rombel"),
								"field_sort"			=> array("nis","nisn","nama"),
								"field_filter"			=> array("nis","nama","nisn"),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(),
								"field_comparator"		=> array("id_siswa"=>"=","id_rombel"=>"=","id_sekolah"=>"=","id_tahun_ajaran"=>"=","status_aktif"=>"="),
								"field_operator"		=> array("id_siswa"=>"AND","id_rombel"=>"AND","id_sekolah"=>"AND","id_tahun_ajaran"=>"AND","status_aktif"=>"AND"),
								//"field_align"			=> array(),
								"field_filter_dropdown" => array("id_rombel"=>array("label"=>"Kelas",array())),
								"field_size"			=> array("action_col"=>"150"),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array('detail'=>'pps/siswa/detail_siswa'),					
								//"custom_add_link"		=> array('label'=>'Add','href'=>'school/add' ),
								// "custom_edit_link"		=> array('edit'=>'pps/siswa/edit'),
								//"custom_delete_link"	=> array(),
								"custom_link"			=> array("Import" => "import/siswa"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								"view_file_form_input"	=> "form_input",
								"field_list"			=> array("id_siswa", "nisn","nis","nama","tempat_lahir","tanggal_lahir","jenis_kelamin","anak_ke","jumlah_sdr","telepon","e_mail","alamat","kode_pos","foto","status_aktif","id_golongan_darah","id_agama","id_status_anak","id_kecamatan","id_asal_sekolah"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								//"redirect_link"			=> ""
							),
			//Konfigurasi tampilan edit
			"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								// "enable"				=> TRUE,
								// "model_name"			=> "",
								// "table"					=> array(),
								// "view_file"				=> "simple/edit",
								"view_file_form_input"	=> "form_input",
								// "field_list"			=> array(),
								// "custom_action"			=> "",
								// "msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								// "msg_on_success"		=> "Data berhasil diperbaharui.",
								// "redirect_link"			=> ""
							),
			//Konfigurasi tampilan delete
			/*"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);
	

		public function pre_process($data){
			//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
			//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
			
			
			//$id_pihak = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			//$list_pihak_komunikasi = get_list("m_siswa","id_siswa","nama");
			$data['conf']['data_list']['subtitle'] ="Identitas Siswa";
			$data['conf']['data_add']['subtitle'] = "Penambahan Data Identitas Siswa";
			$data['conf']['data_edit']['subtitle'] = "Perubahan Identitas Siswa";
			
			$data['conf']['data_delete']['redirect_link'] = "pps/siswa/datalist/";
			$data['conf']['data_add']['redirect_link'] = "pps/siswa/datalist/";
			$data['conf']['data_edit']['redirect_link'] = "pps/siswa/datalist/";
			
			return $data;
		}
		
		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
			//$id_siswa = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			//$_POST['filter']['id_siswa'] = $id_siswa;
			$_POST['filter']['id_sekolah'] = get_id_sekolah();
			$_POST['filter']['id_tahun_ajaran'] = get_id_tahun_ajaran();
			$_POST['filter']['status_aktif'] = 1;
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															//'id_siswa'=>$id_siswa, 
															//'id_sekolah'=>get_id_sekolah() 
														);
			
			$data['conf']['data_list']["field_filter_dropdown"]	= array("id_rombel"=>array("label"=>"Kelas","list"=>get_rombel()));
			//Mengganti link add yang ada di kanan atas form datalist
			
			// $data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'siswa/add/');			
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'pps/siswa/edit/' );
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'pps/siswa/delete/' );
			
			return $data;
		}
		
		public function before_datatable($data){
			//Method ini hanya akan dieksekusi di awal method datatable
			//Mengambil id_pihak yang diposting dari form datalist
			$filter = $this->input->post("filter");
			
			//$id_siswa = $filter['id_sekolah'];
			//Mengkustomisasi link tombol edit dan delete
			$_POST['filter']['id_sekolah'] = get_id_sekolah();
			$_POST['filter']['id_tahun_ajaran'] = get_id_tahun_ajaran();
			$_POST['filter']['status_aktif'] = 1;
			
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'pps/siswa/edit/' );
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'pps/siswa/delete/' );
			$data['conf']['data_list']["field_filter_dropdown"]	= array("id_rombel"=>array("label"=>"Kelas","list"=>get_rombel()));
			return $data;
		}
		
				
		public function before_render_add($data){
			if(!$this->input->post()){
				//$id_siswa = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
				//mengisi nilai untuk id_pihak_komunikasi di form input
				//$_POST['item']['id_siswa'] = $id_siswa; //name='item[id_pihak_kom..]'
			}
			return $data;
		}
		
		public function before_add($data){
			// echo "<pre>";
			// print_r($data);
			// exit;
			
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
				$data['item']['id_sekolah'] = get_id_sekolah();
				$data['simpanan']['id_rombel'] = $data['item']['id_rombel'];
				unset($data['item']['id_rombel']);
	
				if($_FILES["item"]["error"]["foto"]==0){
					$fname = date("YmdHisu");
					$src = 'extras/siswa/'. $fname.".jpg";
					move_uploaded_file($_FILES["item"]["tmp_name"]["foto"],$src);
					$_POST['item']['foto'] = $fname.".jpg";
					$data['item']['foto'] = $fname.".jpg";
					$config['image_library'] = 'gd2';
					$config['source_image'] = $src;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 200;
					$config['height'] = 200;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
				}

			}
			return $data;
		}
		
		public function before_edit($data){
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
	
				if($_FILES["item"]["error"]["foto"]==0){
					$fname = date("YmdHisu");
					$src = 'extras/siswa/'. $fname.".jpg";
					move_uploaded_file($_FILES["item"]["tmp_name"]["foto"],$src);
					$_POST['item']['foto'] = $fname.".jpg";
					$data['item']['foto'] = $fname.".jpg";
					$config['image_library'] = 'gd2';
					$config['source_image'] = $src;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 200;
					$config['height'] = 200;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
				}

			}
			return $data;
		}
		
		public function before_delete($data){
			$data['id'] = $this->uri->rsegment(3);
			return $data;
		}
		
		public function detail_siswa(){
			$this->load->model('t_siswa');
			$id = $this->uri->rsegment(3);
			$gid = get_usergroup();
			if($gid==10){
				$id = get_id_personal();
			}

			$data['detail'] = $this->t_siswa->getdetail($id);
			$data['ortu'] = $this->t_siswa->getortu($id);
			$data['hobi'] = $this->t_siswa->gethobi($id);
			$data['pEkstra'] = $this->t_siswa->getPrestasiEkstra($id);
			$data['pIntra'] = $this->t_siswa->getPrestasiIntra($id);
			//$this->mydebug($data);
			render('detail_siswa', $data);
		}
		
		public function tambah_hobi($id){
			$this->load->model('t_siswa_hobi');
			$data['id']=$id;
			$data['hobi']=$this->t_siswa_hobi->get_all_hobi();
			render('tambah_hobi', $data);
		}
		
		public function submit_hobi(){
			$this->load->library('form_validation');
			$this->load->model('t_siswa_hobi');
			
			$this->form_validation->set_rules('hobi', 'Hobi', 'required');
			
			if($this->form_validation->run() == TRUE){
				$id_hobi = $this->input->post('hobi');
				$id_siswa = $this->input->post('id');
				
				
				$action = $this->input->post('action');
				if($action == 'add'){
					
					$data['id_siswa']=$id_siswa;
					$data['id_hobi']=$id_hobi;
					$this->t_siswa_hobi->save($data);
					
					// print_r($data1);
					set_success_message('Data Berhasil Ditambah!');
					redirect("pps/siswa/detail_siswa/".$id_siswa."#t1-c3");
				}
			}
			else{
				set_error_message('Terjadi Kesalahan Pengisian!');
				redirect("pps/siswa/detail_siswa/".$id_siswa."#t1-c3");
			}
		}
		
		public function hapus_hobi(){
			$this->load->model('t_siswa_hobi');
			$id_hobi = $this->uri->rsegment(3);
			//print_r($id_hobi);
			$sh = $this->t_siswa_hobi->get($id_hobi);
			$this->t_siswa_hobi->delete($id_hobi);
			set_success_message("data berhasil di hapus");
			//$this->mydebug($data);
			redirect("pps/siswa/detail_siswa/".$sh["id_siswa"]."#t1-c3");
		}
		
		public function hapus_pEkstra(){
			$this->load->model('t_prestasi_siswa');
			$id = $this->uri->rsegment(3);
			//print_r($id_hobi);
			$ps = $this->t_prestasi_siswa->get($id_siswa);
			$this->t_prestasi_siswa->delete($id);
			set_success_message("data berhasil di hapus");
			//$this->mydebug($data);
			redirect("pps/siswa/detail_siswa/".$ps["id_siswa"]."#t1-c1");
		}
		
		public function hapus_pIntra(){
			$this->load->model('t_prestasi_siswa');
			$id = $this->uri->rsegment(3);
			//print_r($id_hobi);
			$ps = $this->t_prestasi_siswa->get($id_siswa);
			$this->t_prestasi_siswa->delete($id);
			set_success_message("data berhasil di hapus");
			//$this->mydebug($data);
			redirect("pps/siswa/detail_siswa/".$ps["id_siswa"]."#t1-c2");
		}
		
		public function cek_ortu(){
			$this->load->model('t_siswa');
			$id_siswa = $this->uri->rsegment(3);
			$data['ortu'] = $this->t_siswa->getortu($id_siswa);
			$data['jenjang_pendidikan'] = $this->t_siswa->getJenjangPendidikan()->result_array();
			$data['pekerjaan'] = $this->t_siswa->getPekerjaan()->result_array();
			$data['penghasilan'] = $this->t_siswa->getPenghasilan()->result_array();
			$data['kecamatan'] = $this->t_siswa->getKecamatan()->result_array();
			$data['id_siswa'] = $id_siswa;
			if(isset($data['ortu']['id_siswa'])){
				$data['id_edit'] = 1;
				render('detail_siswa/edit_orangtua', $data);
			}else{
				$data['id_edit'] = 0;
				render('detail_siswa/edit_orangtua', $data);
			}
		}
		
		public function edit_ortu(){
			$this->load->model('m_ortu');
			$input=$this->input->post();
			$data['nama_ayah'] = $input['nama_ayah'];
			$data['nama_ibu'] = $input['nama_ibu'];
			$data['nama_wali'] = $input['nama_wali'];
			$data['alamat_ayah'] = $input['alamat_ayah'];
			$data['alamat_ibu'] = $input['alamat_ibu'];
			$data['alamat_wali'] = $input['alamat_wali'];
			$data['id_jenjang_pendidikan_ayah'] = $input['jp_ayah'];
			$data['id_jenjang_pendidikan_ibu'] = $input['jp_ibu'];
			$data['id_jenjang_pendidikan_wali'] = $input['jp_wali'];
			$data['id_pekerjaan_ayah'] = $input['p_ayah'];
			$data['id_pekerjaan_ibu'] = $input['p_ibu'];
			$data['id_pekerjaan_wali'] = $input['p_wali'];
			$data['id_range_penghasilan_ayah'] = $input['rp_ayah'];
			$data['id_range_penghasilan_ibu'] = $input['rp_ibu'];
			$data['id_range_penghasilan_wali'] = $input['rp_wali'];
			$data['id_kecamatan_ayah'] = $input['k_ayah'];
			$data['id_kecamatan_ibu'] = $input['k_ibu'];
			$data['id_kecamatan_wali'] = $input['k_wali'];
			$data['id_siswa'] = $input['id_siswa'];
			$data['kecamatan_id'] = 0;
			if($input['edit']==1){
				$this->m_ortu->edit($data, $input['id']);
			}else{
				$this->m_ortu->add($data);
			}
			set_success_message("data berhasil di perbarui");
			redirect("pps/siswa/detail_siswa/".$data["id_siswa"]);
		}

		public function after_add($data){
			if($this->input->post()){
				$fakta['username'] = $data['item']['nis']; 
				$fakta['password'] = md5($fakta['username']);
				//$fakta['default_password'] = md5($fakta['username']);
				$fakta['email'] = $data['item']['e_mail']; 
				$fakta['date_created'] = date('Y-m-d G:i:s'); 
				$fakta['is_active'] = 1;
				$fakta['id_group'] = 10;
				$id = get_id_sekolah();
				$fakta['id_sekolah'] = $id;
				$fakta['id_personal'] = $this->t_siswa->new_id();
				//masukin rombel
				$rombel['id_rombel']=$data['simpanan']['id_rombel'];
				$rombel['id_siswa'] = $fakta['id_personal'];
				$this->db->insert('t_rombel_detail', $rombel);
				//masukin user
				$this->db->insert('sys_user', $fakta);
			}
			return $data;
		}

		public function edit_foto($id=0){
			$data['id_siswa'] = $id;
			render('detail_siswa/edit_foto', $data);
		}

		public function submit_foto(){
			if($this->input->post()){
				$id=$this->input->post('id_siswa');
	
				if($_FILES["foto"]["error"]==0){
					$fname = date("YmdHisu");
					$src = 'extras/siswa/'. $fname.".jpg";
					move_uploaded_file($_FILES["foto"]["tmp_name"],$src);
					$data['foto'] = $fname.".jpg";
					$config['image_library'] = 'gd2';
					$config['source_image'] = $src;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 200;
					$config['height'] = 200;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();

					$this->db->where('id_siswa', $id);
					$this->db->update('m_siswa', $data);
					set_success_message('Data berhasil diubah!');
				}else{
					set_error_message('Data gagal diubah!');
				}

				redirect(site_url('pps/siswa/detail_siswa/').'/'.$id) ;
			}
		}
}
