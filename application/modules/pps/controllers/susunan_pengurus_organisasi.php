<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class susunan_pengurus_organisasi extends CI_Controller {

	public function index(){	
		$this->load->model('pps/m_siswa_organisasi_pengurus');
		$data['organisasi'] = $this->m_siswa_organisasi_pengurus->get_all_organisasi();
		if($data['organisasi'][0]){
			$id = $data['organisasi'][0]['id_siswa_organisasi'];
			$data['id'] = $id;
		}else{
			$id=0;
			$data['id'] = 0;
		}
		$data['component']="pps";
		$data['pengurus'] = $this->m_siswa_organisasi_pengurus->get_organisasi_pengurus_by($id);
		render('susunan_pengurus_organisasi/susunan_pengurus_organisasi',$data);
	}
	
	public function search($id=0){
		if($id==0)
			$id=$this->input->post('id_siswa_organisasi');
		$this->load->model('pps/m_siswa_organisasi_pengurus');
		$data['organisasi'] = $this->m_siswa_organisasi_pengurus->get_all_organisasi();
		if($data['organisasi'][0]&&$id==""){
			$id = $data['organisasi'][0]['id_siswa_organisasi'];
		}
		$data['id'] = $id;
		$data['component']="pps";
		$data['pengurus'] = $this->m_siswa_organisasi_pengurus->get_organisasi_pengurus_by($id);
		render('susunan_pengurus_organisasi/susunan_pengurus_organisasi',$data);
	}
	
	public function edit(){
		$this->load->model('pps/m_siswa_organisasi_pengurus');
		$data['id'] = $this->uri->rsegment(3);
		$id = $this->uri->rsegment(4);
		$data['component']="pps";
		$data['detail'] = $this->m_siswa_organisasi_pengurus->get_struktur_by($id);
		render('susunan_pengurus_organisasi/edit', $data);
	}
	
	public function proses_edit(){
		$this->load->model('pps/m_siswa_organisasi_pengurus');
		$data=$this->input->post();
		$id = $data['id_siswa_organisasi_struktur'];
		$id_redirect = $data['id_redirect'];
		unset($data['id_redirect']);
		unset($data['nis']);
		unset($data['id_siswa_organisasi_struktur']);
		unset($data['send']);
		$this->m_siswa_organisasi_pengurus->edit($data, $id);
		set_success_message("data berhasil di perbarui");
		$this->search($id_redirect);
	}
	
	public function delete(){
		$this->load->model('pps/m_siswa_organisasi_pengurus');
		$id_redirect = $this->uri->rsegment(3);
		$id = $this->uri->rsegment(4);
		$this->m_siswa_organisasi_pengurus->delete($id);
		set_success_message("data berhasil di hapus");
		$this->search($id_redirect);
	}
	
	public function add(){
		$this->load->model('pps/m_siswa_organisasi_pengurus');
		$data['id'] = $_GET['id'];
		$data['component']="pps";
		$data['jabatan'] = $this->m_siswa_organisasi_pengurus->get_all_jabatan($data['id']);
		render('susunan_pengurus_organisasi/add', $data);
	}
	
	public function proses_add(){
		$this->load->model('pps/m_siswa_organisasi_pengurus');
		$data=$this->input->post();
		$re = $data['id_redirect'];
		unset($data['nis']);
		unset($data['send']);
		unset($data['id_redirect']);
		$this->m_siswa_organisasi_pengurus->add($data);
		set_success_message("data berhasil di simpan");
		$this->search($re);
	}
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */