<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class struktur_organisasi_siswa extends CI_Controller {

	public function index(){	
		$this->load->model('pps/m_siswa_organisasi_struktur');
		$data['organisasi'] = $this->m_siswa_organisasi_struktur->get_all_organisasi();
		if($data['organisasi'][0]){
			$id = $data['organisasi'][0]['id_siswa_organisasi'];
			$data['id'] = $id;
		}else{
			$id=0;
			$data['id'] = 0;
		}
		$data['component']="pps";
		$data['struktur'] = $this->m_siswa_organisasi_struktur->get_organisasi_struktur_by($id);
		render('struktur_organisasi_siswa/struktur_organisasi_siswa',$data);
	}
	
	public function search($id=0){
		$id=$this->input->post('id_siswa_organisasi');
		$this->load->model('pps/m_siswa_organisasi_struktur');
		$data['organisasi'] = $this->m_siswa_organisasi_struktur->get_all_organisasi();
		if($data['organisasi'][0]&&$id==""){
			$id = $data['organisasi'][0]['id_siswa_organisasi'];
		}
		$data['id'] = $id;
		$data['component']="pps";
		$data['struktur'] = $this->m_siswa_organisasi_struktur->get_organisasi_struktur_by($id);
		render('struktur_organisasi_siswa/struktur_organisasi_siswa',$data);
	}
	
	public function edit(){
		$this->load->model('pps/m_siswa_organisasi_struktur');
		$id = $this->uri->rsegment(3);
		$data['component']="pps";
		$data['detail'] = $this->m_siswa_organisasi_struktur->get_struktur_by($id);
		render('struktur_organisasi_siswa/edit', $data);
	}
	
	public function proses_edit(){
		$this->load->model('pps/m_siswa_organisasi_struktur');
		$data=$this->input->post();
		$id = $data['id_siswa_organisasi_struktur'];
		unset($data['id_siswa_organisasi_struktur']);
		unset($data['send']);
		$this->m_siswa_organisasi_struktur->edit($data, $id);
		set_success_message("data berhasil di perbarui");
		redirect("pps/struktur_organisasi_siswa");
	}
	
	public function delete(){
		$this->load->model('pps/m_siswa_organisasi_struktur');
		$id = $this->uri->rsegment(3);
		$data=$this->m_siswa_organisasi_struktur->get($id);
		$this->m_siswa_organisasi_struktur->delete($id);
		// print_r($data);
		// exit;
		set_success_message("data berhasil di hapus");
		$this->search($data['id_siswa_organisasi']);
	}
	
	public function add(){
		$data['id'] = $_GET['id'];
		$data['component']="pps";
		render('struktur_organisasi_siswa/add', $data);
	}
	
	public function proses_add(){
		$this->load->model('pps/m_siswa_organisasi_struktur');
		$data=$this->input->post();
		unset($data['send']);
		//print_r($data);
		$this->m_siswa_organisasi_struktur->add($data);
		set_success_message("data berhasil di simpan");
		$this->search($data['id_siswa_organisasi']);
	}
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */