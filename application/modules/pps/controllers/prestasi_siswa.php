<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class prestasi_siswa extends CI_Controller {

	public function index()
	{	
		$this->load->model('pps/m_prestasi_siswa');
		//$id_sekolah = get_id_sekolah();
		
		//$data['prestasi'] = $this->m_prestasi_siswa->getPrestasi($id_sekolah)->result_array();
		$data['tahunajaran'] = $this->m_prestasi_siswa->getTahunAjaran()->result_array();
		$data['component']="pps";
		render('prestasi_siswa/datalist',$data);
	}

	public function load_add()
	{	
		$this->load->model('pps/m_prestasi_siswa');
		$data['component']="pps";
		render('prestasi_siswa/add',$data);
	}

	public function load_edit($id)
	{	
		$this->load->model('pps/m_prestasi_siswa');
		$data['prestasi'] = $this->m_prestasi_siswa->getPrestasiBy($id);
		$data['component']="pps";
		render('prestasi_siswa/edit',$data);
	}


	public function cekData($nim){
		$this->load->model('pps/m_prestasi_siswa');
		$data = $this->m_prestasi_siswa->getData($nim);
		echo json_encode ($data);
	}

	public function insertData(){
		$this->load->model('pps/m_prestasi_siswa');
		$data['id_ekstra_kurikuler'] = 0;
		$data['id_siswa'] = $this->input->post('id_siswa');
		$data['prestasi'] = $this->input->post('prestasi');
		$data['tanggal'] = tanggal_db($this->input->post('tanggal'));
		$data['kategori'] = $this->input->post('kategori');
		$data['perlombaan'] = $this->input->post('perlombaan');
		$data['keterangan'] = $this->input->post('keterangan');
		$data['id_tingkat_wilayah'] = $this->input->post('id_tingkat_wilayah');
		
		 $config['upload_path'] = './extras/prestasi/';
			 $config['allowed_types'] = 'gif|jpg|png';
			 $config['max_size'] = '10000';
			 $config['max_width'] = '3000';
			 $config['max_height'] = '3000';
			$this->load->library('upload', $config);
 
			 if (!$this->upload->do_upload('foto')){
			 	$this->load->helper('form');
				$error = array('error'=>$this->upload->display_errors());
			 }
			 else {
				$this->load->helper('form');
				$upload_data = $this->upload->data();
				$data['foto']=$upload_data['file_name'];
				set_success_message('Data Berhasil Ditambah!');
			 }
			 
		
		$this->m_prestasi_siswa->insertData($data);
		
		redirect('pps/prestasi_siswa');
	}

	public function insertEdit(){
		$this->load->model('pps/m_prestasi_siswa');
		$data['id_ekstra_kurikuler'] = 0;
		$data['id_siswa'] = $this->input->post('id_siswa');
		$data['prestasi'] = $this->input->post('prestasi');
		$data['tanggal'] = tanggal_db($this->input->post('tanggal'));
		$data['kategori'] = $this->input->post('kategori');
		$data['perlombaan'] = $this->input->post('perlombaan');
		$data['keterangan'] = $this->input->post('keterangan');
		$data['id_tingkat_wilayah'] = $this->input->post('id_tingkat_wilayah');
		$id = $this->input->post('id_prestasi_siswa');
		
		$this->m_prestasi_siswa->edit($data, $id);
		set_success_message("data berhasil di perbarui");
		redirect("pps/prestasi_siswa");
	}

	function print_pdf(){
        $this->load->model('pps/m_prestasi_siswa');
        $id_sekolah = get_id_sekolah();
		$data['prestasi'] = $this->m_prestasi_siswa->getPrestasi($id_sekolah)->result_array();

        print_pdf($this->load->view('prestasi_siswa/print', $data, true), "A4");
        redirect(getenv('HTTP_REFERER'));
    }

	public function delete($id){
		$this->load->model('pps/m_prestasi_siswa');
		$this->m_prestasi_siswa->delete($id);
		set_success_message('Data Berhasil Dihapus!');
		redirect(site_url('pps/prestasi_siswa'));
	}
	/*
	public function search(){
		$this->load->model('pps/m_prestasi_siswa');
		$key = $this->input->post('key');
		$id_sekolah = get_id_sekolah();
		$data['isi'] = $this->m_prestasi_siswa->get_data_search($key,$id_sekolah);
		$data['status'] = 1;
		$data['component']="pps";
		//print_r($data);
		render('prestasi_siswa/datalist',$data);
	}*/
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */