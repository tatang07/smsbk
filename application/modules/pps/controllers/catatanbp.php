<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class catatanbp extends Simple_Controller {
    	public $myconf = array(
			//Nama controller
			"name" 			=> "pps/catatanbp",
			"component"		=> "pps",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Pelayanan dan Pembinaan Siswa",
			"subtitle"		=> "Catatan BP/BK",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "m_catatanbp",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"nis"=>"NIS",
								"nama"=>"Nama",
								"rombel"=>"Kelas"
								
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								//"tanggal"=>"date",
								//"id_pihak_komunikasi"=>"hidden",
								"id_siswa"=>"hidden",
								// "jenis_kelamin"=>array("list"=>array("l"=>"Laki-laki","p"=>"Permpuan")),
								// "tanggal_lahir"=>"date",
								// "alamat"=>"text",
								// "foto"=>"file",
								// "id_sekolah"=>"hidden",
								// "id_golongan_darah"=>array("r_golongan_darah","id_golongan_darah","golongan_darah"),
								// "id_agama"=>array("r_agama","id_agama","agama"),
								// "id_status_anak"=>array("r_status_anak","id_status_anak","status_anak"),
								// "id_kecamatan"=>array("r_kecamatan","id_kecamatan","kecamatan"),
								// "id_asal_sekolah"=>array("r_asal_sekolah","id_asal_sekolah","nama_sekolah"),
								// "status_aktif"=>array("list"=>array("1"=>"Aktif","0"=>"Tidak Aktif")),
								
							),
			//Field-field yang harus diisi
			// "data_mandatory"=> array(
			// 					"nis","nisn","nama","telepon","nama","nis"
			// 				),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			// "data_unique"	=> array(
			// 					"telepon"
			// 				),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"model_name"			=> "v_siswa",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("nis","nama","rombel"),
								"field_sort"			=> array("nis","nama","rombel"),
								"field_filter"			=> array("nis","nama","rombel"),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(),
								"field_comparator"		=> array("id_siswa"=>"=","id_rombel"=>"=","id_sekolah"=>"=","id_tahun_ajaran"=>"=","status_aktif"=>"="),
								"field_operator"		=> array("id_siswa"=>"AND","id_rombel"=>"AND","id_sekolah"=>"AND","id_tahun_ajaran"=>"AND","status_aktif"=>"AND"),
								//"field_align"			=> array(),
								"field_filter_dropdown" => array("id_rombel"=>array("label"=>"Kelas",array())),
								"field_size"			=> array("action_col"=>"150"),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array('detail'=>'pps/catatanbp/load_detail'),					
								//"custom_add_link"		=> array('label'=>'Add','href'=>'school/add' ),
								// "custom_edit_link"		=> array('edit'=>'pps/siswa/edit'),
								//"custom_delete_link"	=> array(),
								//"custom_link"			=> array("Import" => "import/siswa"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"enable"				=> FALSE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								// "view_file_form_input"	=> "form_input",
								// "field_list"			=> array("id_siswa", "nisn","nis","nama","tempat_lahir","tanggal_lahir","jenis_kelamin","anak_ke","jumlah_sdr","telepon","e_mail","alamat","kode_pos","foto","status_aktif","id_golongan_darah","id_agama","id_status_anak","id_kecamatan","id_asal_sekolah"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								//"redirect_link"			=> ""
							),
			//Konfigurasi tampilan edit
			"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"enable"				=> FALSE,
								// "model_name"			=> "",
								// "table"					=> array(),
								// "view_file"				=> "simple/edit",
								"view_file_form_input"	=> "form_input",
								// "field_list"			=> array(),
								// "custom_action"			=> "",
								// "msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								// "msg_on_success"		=> "Data berhasil diperbaharui.",
								// "redirect_link"			=> ""
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> FALSE,
								// "model_name"			=> "",
								// "table"					=> array(),
								// "msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								// "msg_on_success"		=> "Data berhasil dihapus.",
								// "redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			// "data_export"	=> array(
			// 					"enable_pdf"			=> TRUE,
			// 					"enable_xls"			=> TRUE,
			// 					"view_pdf"				=> "simple/laporan_datalist",
			// 					"view_xls"				=> "simple/laporan_datalist",
			// 					"field_size"			=> array(),
			// 					"orientation"			=> "p",
			// 					"special_number"		=> "",
			// 					"enable_header"			=> TRUE,
			// 					"enable_footer"			=> TRUE,
			// 				)
		);
	

				
		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
			//$id_siswa = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			//$_POST['filter']['id_siswa'] = $id_siswa;
			$_POST['filter']['id_sekolah'] = get_id_sekolah();
			$_POST['filter']['id_tahun_ajaran'] = get_id_tahun_ajaran();
			$_POST['filter']['status_aktif'] = 1;
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															//'id_siswa'=>$id_siswa, 
															//'id_sekolah'=>get_id_sekolah() 
														);
			
			$data['conf']['data_list']["field_filter_dropdown"]	= array("id_rombel"=>array("label"=>"Kelas","list"=>get_rombel()));
			//Mengganti link add yang ada di kanan atas form datalist
			
			// $data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'siswa/add/');			
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
			// $data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'pps/siswa/edit/' );
			// $data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'pps/siswa/delete/' );
			
			return $data;
		}
		
		public function before_datatable($data){
			//Method ini hanya akan dieksekusi di awal method datatable
			//Mengambil id_pihak yang diposting dari form datalist
			$filter = $this->input->post("filter");
			
			//$id_siswa = $filter['id_sekolah'];
			//Mengkustomisasi link tombol edit dan delete
			$_POST['filter']['id_sekolah'] = get_id_sekolah();
			$_POST['filter']['id_tahun_ajaran'] = get_id_tahun_ajaran();
			$_POST['filter']['status_aktif'] = 1;
			
			// $data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'pps/siswa/edit/' );
			// $data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'pps/siswa/delete/' );
			$data['conf']['data_list']["field_filter_dropdown"]	= array("id_rombel"=>array("label"=>"Kelas","list"=>get_rombel()));
			return $data;
		}
		
		public function load_detail($id){
			$this->load->model('m_catatanbp');
			$data['data_siswa']=$this->m_catatanbp->get_siswa();
			$data['statuspil'] = $id;
			$data['component'] = "pps";
			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";
			// exit;
			render("catatanbp/catatan_detail",$data);
		}

		public function load_add_detail($ids){
			$this->load->model('m_catatanbp');
		
			$data['ids'] = $ids;
			render("catatanbp/form_tambah_detail",$data);
		
		}

		public function submit_add(){
			$this->load->model('m_catatanbp');
			$data['id_siswa'] = $this->input->post('id_siswa');
			$data['tanggal_catatan'] = tanggal_db($this->input->post('tanggal'));
			$data['keterangan'] = $this->input->post('keterangan');
			$data['id_user'] = get_userid();
			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";
			// exit;
			$this->m_catatanbp->add($data);
			redirect(site_url('pps/catatanbp/load_detail/').'/'.$data['id_siswa']);
		}

		public function delete_catatan_detail($id,$ids){
			$this->load->model('m_catatanbp');
			$this->m_catatanbp->delete($id);

			redirect(site_url('pps/catatanbp/load_detail/').'/'.$ids) ;
		}

		public function kontrak_guru_bp(){
			$this->load->model('m_catatanbp');
			$data['daftar_guru'] = $this->m_catatanbp->get_guru();
			$data['daftar_rombel'] = $this->m_catatanbp->get_rombel_none();
			// echo "<pre>";
			// print_r($data);
			// echo "</pre>";
			// exit;

			render("catatanbp/form_kontrak",$data);
		}

		public function submit_kontrak(){
			$this->load->model('m_catatanbp');
			$data['id_guru'] = $this->input->post('id_guru');
			$rombel = $this->input->post('kontrak');
			foreach ($rombel as $r) {
				$data['id_rombel'] = $r;

				$this->m_catatanbp->insert_kontrak($data);
			}

			redirect(site_url('pps/catatanbp/datalist/'));
		}
}
