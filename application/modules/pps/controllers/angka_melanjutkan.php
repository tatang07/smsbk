<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class angka_melanjutkan extends CI_Controller {

	public function index()
	{	
		$this->load->model('pps/m_angka_melanjutkan');
		$id_sekolah=get_id_sekolah();
		$data['hasil'] = $this->m_angka_melanjutkan->select_data()->result_array();
		
		// print_r($data['hasil']);
		$data['tahun_ajaran'] = $this->m_angka_melanjutkan->select_tahun_ajaran()->result_array();
		$data['component']="pps";
		render('angka_melanjutkan/angka_melanjutkan',$data);
	}
	public function detail($id_perguruan_tinggi)
	{	
		$this->load->model('pps/m_angka_melanjutkan');
		$id_sekolah=get_id_sekolah();
		$data['hasil'] = $this->m_angka_melanjutkan->select_detail($id_sekolah,$id_perguruan_tinggi)->result_array();
		$data['component']="pps";
		render('angka_melanjutkan/detail',$data);
	}
	public function cekData($nim){
		$this->load->model('pps/m_angka_melanjutkan');
		$data = $this->m_angka_melanjutkan->getData($nim);
		
		echo json_encode ($data);
	}
	public function load_tambah_angka_melanjutkan($id_rombel=0){
		$this->load->model('pps/m_angka_melanjutkan');
		$data['jalur_masuk'] = $this->m_angka_melanjutkan->getJalurMasuk()->result_array();
		$data['rombel'] = $this->m_angka_melanjutkan->getRombel()->result_array();
		$data['perguruan_tinggi'] = $this->m_angka_melanjutkan->getPerguruanTinggi()->result_array();
		
		$siswa = $this->m_angka_melanjutkan->getSiswa($id_rombel)->result_array();
		$siswaPT = $this->m_angka_melanjutkan->getSiswaPT($id_rombel)->result_array();
		
		$siswa_kelas_asal = array();
		$siswa_kelas = array();
		
		foreach($siswa as $s){
			$siswa_kelas_asal[$s['id_siswa']] = $s['nama'];
		}
		
		foreach($siswaPT as $s){
			$siswa_kelas[$s['id_siswa']] = $s['nama'];
		}
		
		$data['siswa_kelas_asal'] = $siswa_kelas_asal;
		$data['siswa_kelas'] = $siswa_kelas;
		$data['id_rombel'] = $id_rombel;
		$data['id_tahun_ajaran'] = get_id_tahun_ajaran();
		// print_r($siswaPT);
		$data['component']="pps";
		render('angka_melanjutkan/tambah_angka_melanjutkan',$data);
	}
	
	public function c_angka_melanjutkan(){
			$data['component']="pps";
			$this->load->model('pps/m_angka_melanjutkan');
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('id_perguruan_tinggi', 'id_perguruan_tinggi', 'required');
			$this->form_validation->set_rules('id_jalur_masuk_pt', 'id_jalur_masuk_pt', 'required');
			$this->form_validation->set_rules('siswa', 'siswa', 'required');
			
			if($this->form_validation->run() == TRUE){
				$id_siswa = $this->input->post('siswa');
				$data['id_perguruan_tinggi'] = $this->input->post('id_perguruan_tinggi');
				$data['id_jalur_masuk_pt'] = $this->input->post('id_jalur_masuk_pt');
				$data['id_tahun_ajaran'] = $this->input->post('id_tahun_ajaran');
				// $data['id_rombel'] = $this->input->post('id_rombel');
				
				foreach($id_siswa as $s){
					$data['id_siswa'] = $s;
					unset($data['component']);
					$this->m_angka_melanjutkan->tambah_angka_melanjutkan($data);
				}
				// print_r($data);
				// print_r($id_siswa);
				set_success_message('Data Berhasil Ditambah');
				redirect(site_url('pps/angka_melanjutkan/index/'));
			}
			else{
				set_error_message('Jurusan, Siswa, atau Perguruan Tinggi belum dipilih, silahkan coba lagi');
				redirect("pps/angka_melanjutkan/index/");
			}
	}
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */