<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class organisasi_siswa extends CI_Controller {

	public function index(){	
		$this->load->model('pps/m_siswa_organisasi');
		$data['organisasi'] = $this->m_siswa_organisasi->get_organisasi();
		$data['component']="pps";
		render('organisasi_siswa/organisasi_siswa',$data);
	}
	
	public function search(){
		$nama=$this->input->post('nama_organisasi');
		$this->load->model('pps/m_siswa_organisasi');
		$data['component']="pps";
		$data['organisasi'] = $this->m_siswa_organisasi->get_organisasi_like($nama);
		render('organisasi_siswa/organisasi_siswa',$data);
	}

	public function print_pdf(){
		$this->load->model('pps/m_siswa_organisasi');
		$data['organisasi'] = $this->m_siswa_organisasi->get_organisasi();
		print_pdf($this->load->view('organisasi_siswa/organisasi_siswa_print', $data, true), "A4");
		render('organisasi_siswa/organisasi_siswa',$data);
	}
	
	public function edit(){
		$this->load->model('pps/m_siswa_organisasi');
		$id = $this->uri->rsegment(3);
		$data['component']="pps";
		$data['detail'] = $this->m_siswa_organisasi->get_organisasi_by($id);
		render('organisasi_siswa/edit', $data);
	}
	
	public function proses_edit(){
		$this->load->model('pps/m_siswa_organisasi');
		$data=$this->input->post();
		$id = $data['id_siswa_organisasi'];
		unset($data['id_siswa_organisasi']);
		unset($data['send']);
		//print_r($data);
		$this->m_siswa_organisasi->edit($data, $id);
		set_success_message("data berhasil di perbarui");
		redirect("pps/organisasi_siswa");
	}
	
	public function delete(){
		$this->load->model('pps/m_siswa_organisasi');
		$id = $this->uri->rsegment(3);
		$this->m_siswa_organisasi->delete($id);
		set_success_message("data berhasil di hapus");
		redirect("pps/organisasi_siswa");
	}
	
	public function add(){
		$data['component']="pps";
		render('organisasi_siswa/add', $data);
	}
	
	public function proses_add(){
		$this->load->model('pps/m_siswa_organisasi');
		$data=$this->input->post();
		$data['id_sekolah'] = get_id_sekolah();
		unset($data['send']);
		//print_r($data);
		$this->m_siswa_organisasi->add($data);
		set_success_message("data berhasil di simpan");
		redirect("pps/organisasi_siswa");
	}
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */