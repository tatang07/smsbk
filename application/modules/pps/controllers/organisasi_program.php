<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class organisasi_program extends CI_Controller {

	public function index(){	
		$this->load->model('pps/m_siswa_organisasi_program');
		$data['organisasi'] = $this->m_siswa_organisasi_program->get_all_organisasi();
		if($data['organisasi'][0]){
			$id = $data['organisasi'][0]['id_siswa_organisasi'];
			$data['id'] = $id;
		}else{
			$id=0;
			$data['id'] = 0;
		}
		$data['component']="pps";
		$data['program'] = $this->m_siswa_organisasi_program->get_organisasi_program_by($id);
		render('organisasi_program/organisasi_program',$data);
	}
	
	public function search($id=0){
		$id=$this->input->post('id_siswa_organisasi');
		$this->load->model('pps/m_siswa_organisasi_program');
		$data['organisasi'] = $this->m_siswa_organisasi_program->get_all_organisasi();
		if($data['organisasi'][0]&&$id==""){
			$id = $data['organisasi'][0]['id_siswa_organisasi'];
		}
		$data['id'] = $id;
		$data['component']="pps";
		$data['program'] = $this->m_siswa_organisasi_program->get_organisasi_program_by($id);
		render('organisasi_program/organisasi_program',$data);
	}
	
	public function edit(){
		$this->load->model('pps/m_siswa_organisasi_program');
		$id = $this->uri->rsegment(3);
		$data['component']="pps";
		$data['detail'] = $this->m_siswa_organisasi_program->get_program_by($id);
		render('organisasi_program/edit', $data);
	}
	
	public function proses_edit(){
		$this->load->model('pps/m_siswa_organisasi_program');
		$data=$this->input->post();
		$data['tanggal']=tanggal_db($data['tanggal']);
		$id = $data['id_siswa_organisasi_program'];
		unset($data['id_siswa_organisasi_program']);
		unset($data['send']);
		$this->m_siswa_organisasi_program->edit($data, $id);
		set_success_message("data berhasil di perbarui");
		redirect("pps/organisasi_program");
	}
	
	public function delete(){
		$this->load->model('pps/m_siswa_organisasi_program');
		$id = $this->uri->rsegment(3);
		$data=$this->m_siswa_organisasi_program->get($id);
		$this->m_siswa_organisasi_program->delete($id);
		// print_r($data);
		// exit;
		set_success_message("data berhasil di hapus");
		$this->search($data['id_siswa_organisasi']);
	}
	
	public function add(){
		$data['id'] = $_GET['id'];
		$data['component']="pps";
		render('organisasi_program/add', $data);
	}
	
	public function proses_add(){
		$this->load->model('pps/m_siswa_organisasi_program');
		$data=$this->input->post();
		$data['tanggal']=tanggal_db($data['tanggal']);
		unset($data['send']);
		//print_r($data);
		$this->m_siswa_organisasi_program->add($data);
		set_success_message("data berhasil di simpan");
		$this->search($data['id_siswa_organisasi']);
	}
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */