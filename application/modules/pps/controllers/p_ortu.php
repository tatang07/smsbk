<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class p_ortu extends Simple_Controller {	
	
	public function index(){			
		$this->load->model('pps/m_p_ortu');
		
		$data['ortu'] = $this->m_p_ortu->select_ortu()->result_array();	
		$data['pekerjaan'] = $this->m_p_ortu->select_pekerjaan()->result_array();	
		$data['penghasilan'] = $this->m_p_ortu->select_penghasilan()->result_array();	
		// $data['lp'] = $this->m_p_ortu->get_siswa();	
		// $id = $this->m_p_ortu->select_tahun_ajaran()->result_array();
		
		// $data['tahun_ajaran'] = $id[0]['tahun_awal'].'/'.$id[0]['tahun_akhir'];
		// $this->mydebug($id);
		$data['component']="pps";
		render('p_ortu',$data);
	}
	
}