<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class keadaan_peserta_didik extends Simple_Controller {	
	
	public function index(){			
		$this->load->model('pps/m_keadaan_peserta_didik');
		
		$data['tingkat'] = $this->m_keadaan_peserta_didik->select_tingkat()->result_array();	
		$data['jumlah_rombel'] = $this->m_keadaan_peserta_didik->select_rombel()->result_array();	
		$data['lp'] = $this->m_keadaan_peserta_didik->get_siswa();	
		$id = $this->m_keadaan_peserta_didik->select_tahun_ajaran()->result_array();
		$data['tahun_ajaran'] = $id[0]['tahun_awal'].'/'.$id[0]['tahun_akhir'];
		// $this->mydebug($id);
		$data['component']="pps";
		render('keadaan_peserta_didik',$data);
	}
	
}