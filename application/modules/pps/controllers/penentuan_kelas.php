	<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class Penentuan_kelas extends Simple_Controller {

	public $myconf = array(
		//Nama controller
		"name" 			=> "pps/penentuan_kelas",
		"component"		=> "pps",
		//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
		//dan subtitile general ini akan ter-replace
		"title"			=> "Daftar Kelas",
		"subtitle"		=> "",
		//"view_file_index"=> "simple/index",
		//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
		//tidak ditentukan, maka akan digunakan model ini
		//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
		//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
		"model_name"	=> "m_penentuan_kelas",
		//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
		//Format: nama_field=>"Labelnya"
		"data_label"	=> array(		
							"id_tingkat_kelas"=>"Tingkat",
							"tingkat_kelas"=>"Tingkat",
							"rombel"=>"Kelas",
							"id_kurikulum"=>"Kurikulum",
							"nama_kurikulum"=>"Kurikulum",
							"nama"=>"Wali Kelas",
							"id_guru"=>"Wali Kelas"
						),
		//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
		//Tipe default jika tidak didefinisikan: String, 
		//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
		//Format: nama_field => tipe_data_nya
		// "data_type"		=> array(
		 					// "id_sekolah"=>"hidden",
						// ),
		//Field-field yang harus diisi
		// "data_mandatory"=> array(
		// 					"kode_sarana_prasarana","nama_sarana_prasarana","penanggung_jawab","id_kondisi","daya_tampung"
		// 				),
		//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
		// "data_unique"	=> array(
		// 					"id_sarana_prasarana","kode_sarana_prasarana"
		// 				),
		//Konfigurasi tampilan datalist
		"data_list"		=> array(
							//"title"			=> "simple",
							//"subtitle"		=> "simple",
							//"model_name"			=> "",
							//"table"					=> array(),
							//"view_file"				=> "simple/datalist",
							//"view_file_datatable"	=> "simple/datatable",
							//"view_file_datatable_action"	=> "simple/datatable_action",
							"field_list"			=> array("tingkat_kelas","rombel","nama_kurikulum","nama"),
							"field_sort"			=> array("tingkat_kelas","rombel","nama_kurikulum","nama"),
							"field_filter"			=> array("tingkat_kelas","rombel"),
							"field_filter_hidden"	=> array(),
							"field_filter_dropdown"	=> array(),
							"field_comparator"		=> array("id_tingkat_kelas"=>"=","id_rombel"=>"=","m_rombel.id_sekolah"=>"=","id_tahun_ajaran"=>"="),
							"field_operator"		=> array("id_tingkat_kelas"=>"AND","id_rombel"=>"AND","m_rombel.id_sekolah"=>"AND", "id_tahun_ajaran"=>"AND"),
							//"field_align"			=> array(),
							"field_size"			=> array('action_col' => "20%"),
							//"field_separated"		=> array(),
							//"field_sum"				=> array(),
							//"field_summary"			=> array(),
							//"enable_action"			=> TRUE,
							"custom_action_link"	=> array("Siswa" => "pps/penentuan_kelas/siswa"),
							//"custom_add_link"		=> array('label'=>'Add','href'=>'school/add' ),
							//"custom_edit_link"		=> array(),
							//"custom_delete_link"	=> array(),
							//"custom_link"			=> array("Kurikulum"=>"kurikulum/datalist","Siswa"=>"Siswa/datalist"),
						),
		//Konfigurasi tampilan add				
		"data_add"		=> array(
							//"title"			=> "simple",
							//"subtitle"		=> "simple",
							//"enable"				=> TRUE,
							//"model_name"			=> "",
							//"table"					=> array(),
							//"view_file"				=> "simple/add",
							//"view_file_form_input"	=> "simple/form_input",
							"field_list"			=> array("id_tingkat_kelas","rombel","id_kurikulum","id_guru"),
							//"custom_action"			=> "",
							//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
							//"msg_on_success"		=> "Data berhasil disimpan.",
							//"redirect_link"			=> ""
						),
		//Konfigurasi tampilan edit
		"data_edit"		=> array(
							//"title"			=> "simple",
							//"subtitle"		=> "simple",
							// "enable"				=> TRUE,
							// "model_name"			=> "",
							// "table"					=> array(),
							// "view_file"				=> "simple/edit",
							// "view_file_form_input"	=> "simple/form_input",
							// "field_list"			=> array(),
							"field_list"			=> array("id_tingkat_kelas","rombel","id_kurikulum","id_guru"),
							// "custom_action"			=> "",
							// "msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
							// "msg_on_success"		=> "Data berhasil diperbaharui.",
							// "redirect_link"			=> ""
						),
		//Konfigurasi tampilan delete
		/*"data_delete"	=> array(
							"enable"				=> TRUE,
							"model_name"			=> "",
							"table"					=> array(),
							"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
							"msg_on_success"		=> "Data berhasil dihapus.",
							"redirect_link"			=> ""
						),
		//Konfigurasi tampilan export
		"data_export"	=> array(
							"enable_pdf"			=> TRUE,
							"enable_xls"			=> TRUE,
							"view_pdf"				=> "simple/laporan_datalist",
							"view_xls"				=> "simple/laporan_datalist",
							"field_size"			=> array(),
							"orientation"			=> "p",
							"special_number"		=> "",
							"enable_header"			=> TRUE,
							"enable_footer"			=> TRUE,
						)*/
	);

	function index()
	{
		redirect('pps/penentuan_kelas/datalist');
	}

	public function pre_process($data){
		//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
		//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
		
		
		// $id_sapras = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
		// $list_sarana_prasarana = get_list("r_jenis_sarana_prasarana","id_jenis_sarana_prasarana","jenis_sarana_prasarana");
		// $data['conf']['data_list']['subtitle'] = $list_sarana_prasarana[$id_sapras];
		// $data['conf']['data_add']['subtitle'] = $list_sarana_prasarana[$id_sapras];
		// $data['conf']['data_edit']['subtitle'] = $list_sarana_prasarana[$id_sapras];
		
		// $data['conf']['data_delete']['redirect_link'] = "sarana_prasarana/datalist/$id_sapras";
		// $data['conf']['data_add']['redirect_link'] = "sarana_prasarana/datalist/$id_sapras";
		// $data['conf']['data_edit']['redirect_link'] = "sarana_prasarana/datalist/$id_sapras";
		
		return $data;
	}
	
	public function before_datalist($data){
		//Method ini hanya akan dieksekusi di awal method datalist
		//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
		// $id_sapras = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;

		// $_POST['filter']['id_jenis_sarana_prasarana'] = $id_sapras;
		$_POST['filter']['m_rombel.id_sekolah'] = get_id_sekolah();
		$_POST['filter']['id_tahun_ajaran'] = get_id_tahun_ajaran();
		// //Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
		// //mengupdate datatable baik ketika search ataupun pindah page
		// $data['conf']['data_list']['field_filter_hidden'] = array(
		// 												'id_tahun_ajaran'=>2, 
		// 												'id_sekolah'=>get_id_sekolah() 
		// 											);
		
		// //Mengganti link add yang ada di kanan atas form datalist
		// $data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'sarana_prasarana/add/'.$id_sapras );			
		
		// //Memanipulasi parameter page
		// $page = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 1;
		// $data['page'] = $page;
		// //Mengkustomisasi link tombol edit dan delete
		// $data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'sarana_prasarana/edit/'.$id_sapras );
		// $data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'sarana_prasarana/delete/'.$id_sapras );
		
		return $data;
	}
	
	public function before_datatable($data){
		//Method ini hanya akan dieksekusi di awal method datatable
		//Mengambil id_pihak yang diposting dari form datalist
		// $filter = $this->input->post("filter");
		$_POST['filter']['m_rombel.id_sekolah'] = get_id_sekolah();
		$_POST['filter']['id_tahun_ajaran'] = get_id_tahun_ajaran();
		// $id_sapras = $filter['id_jenis_sarana_prasarana'];
		// //Mengkustomisasi link tombol edit dan delete
		// $data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'sarana_prasarana/edit/'.$id_sapras );
		// $data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'sarana_prasarana/delete/'.$id_sapras );
		
		return $data;
	}
	
			
	public function before_render_add($data){
		$data['conf']['data_type'] = array(
			"id_tingkat_kelas" => array('list' => $this->m_penentuan_kelas->get_tingkat_kelas(false, true)),
			"id_kurikulum" => array('m_kurikulum', 'id_kurikulum', 'nama_kurikulum'),
			"id_guru" => array('list' => $this->m_penentuan_kelas->get_guru(false, true))
			);
		// if(!$this->input->post()){
		// 	$id_sapras = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
		// 	//mengisi nilai untuk id_pihak_komunikasi di form input
		// 	$_POST['item']['id_jenis_sarana_prasarana'] = $id_sapras; //name='item[id_pihak_kom..]'
		// }
		return $data;
	}
	
	public function before_add($data){
		// if($this->input->post()){
		// 	//menambahkan satu field id_sekolah sebelum data disimpan ke db
		// }
		if($this->input->post()){
			$data['item']['id_tahun_ajaran'] = get_id_tahun_ajaran();
			$data['item']['id_sekolah'] = get_id_sekolah();

			//dummy dulu
			$data['item']['id_program'] = 1;
		}

		return $data;
	}
			
	public function before_edit($data){
		$data['conf']['data_type'] = array(
			"id_tingkat_kelas" => array('list' => $this->m_penentuan_kelas->get_tingkat_kelas(false, true)),
			"id_kurikulum" => array('m_kurikulum', 'id_kurikulum', 'nama_kurikulum'),
			"id_guru" => array('list' => $this->m_penentuan_kelas->get_guru(false, true))
			);
		// if(!$this->input->post()){
		// 	$data['id'] = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 0;
		// }
		return $data;
	}
	
	public function before_delete($data){
		// $data['id'] = $this->uri->rsegment(4);
		return $data;
	}

	function siswa($id_rombel = false, $id_rombel_asal = false)
	{
		if(! $id_rombel) show_404();

		$data['id_rombel_asal'] = $id_rombel_asal;

		$this->load->model('m_penentuan_kelas');
		$data['detail'] = $this->m_penentuan_kelas->get($id_rombel);
		$rombel = $this->m_penentuan_kelas->get_rombel_lawas();

		// siapkan data untuk dropdown kelas asal
		$data['kelas_asal']['noclass'] = 'Tak berkelas';
		if($rombel)
			foreach ($rombel as $value) {
				$data['kelas_asal'][$value['id_rombel']] = $value['rombel'];
			}

		$data['siswa_kelas_asal'] = array();
		$data['option_siswa_asal'] = '';

		// ambil siswa dari dropdown kelas asal 
		// bila pilihan kelas asal bukan siswa tidak berkelas dan
		// id tahun ajaran bukan id tahun ajaran pertama
		if($id_rombel_asal && $id_rombel_asal !== 'noclass' && get_id_tahun_ajaran() !== 1){
			if($siswa_kelas_asal = $this->m_penentuan_kelas->get_siswa_rombel($id_rombel_asal, get_id_tahun_ajaran() - 1))
				foreach ($siswa_kelas_asal as $siswa) {
					$data['siswa_kelas_asal'][$siswa['id_siswa']] = $siswa['nama'];
				}

		// siswa yang belum dapet kelas
		} else {
			if($siswa_kelas_asal = $this->m_penentuan_kelas->get_siswa_non_rombel())
				foreach ($siswa_kelas_asal as $siswa) {
					$data['siswa_kelas_asal'][$siswa['id_siswa']] = $siswa['nama'];
				}
		}

		// ambil siswa dari kelas yang sedang ditampilkan
		$data['siswa_kelas'] = array();
		if($siswa_kelas = $this->m_penentuan_kelas->get_siswa_rombel($id_rombel, get_id_tahun_ajaran()))
			foreach ($siswa_kelas as $siswa) {
				$data['siswa_kelas'][$siswa['id_siswa']] = $siswa['nama'];
			}

		// merge siswa dari kelas asal dan siswa dari kelas yang aktif
		// nantinya siswa dari kelas aktif akan diberi atribut selected
		$data['siswa_kelas_asal'] = $data['siswa_kelas_asal'] + $data['siswa_kelas'];

		// jika form disubmit
		if($post = $this->input->post('siswa')){
			$this->m_penentuan_kelas->update_rombel_siswa($id_rombel, $id_rombel_asal, $post);
			redirect(getenv('HTTP_REFERER'));
		}
		$data['component']="pps";
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// exit;
		render('penentuan_kelas/form_kelas_siswa', $data);
	}

}
