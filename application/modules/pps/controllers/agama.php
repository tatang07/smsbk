<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT tk.id_tingkat_kelas, tk.tingkat_kelas, s.id_agama, count(id_agama) as agama FROM `t_rombel_detail` as rd join m_siswa as s on rd.id_siswa=s.id_siswa join m_rombel as r on rd.id_rombel=r.id_rombel join m_tingkat_kelas as tk on r.id_tingkat_kelas=tk.id_tingkat_kelas group by tk.id_tingkat_kelas ,id_agama

class agama extends CI_Controller {

	public function index()
	{	
		$this->load->model('pps/m_agama');
		$id= get_id_sekolah();
		$tahun_ajaran= get_id_tahun_ajaran();
		// echo $tahun_ajaran;
		$data['data'] = $this->m_agama->select_data($id,$tahun_ajaran)->result_array();
		$data['agama'] = $this->m_agama->select_agama()->result_array();
		$jenjang_sekolah= get_jenjang_sekolah();
		$data['tingkat'] = $this->m_agama->select_tingkat($jenjang_sekolah)->result_array();

		$agama=array();
		foreach($data['tingkat'] as $tingkat){
			foreach($data['agama'] as $ag){
				$agama[$tingkat['tingkat_kelas']][$ag['id_agama']] = 0;
			}
		}
		
		foreach($data['data'] as $d){
			$agama[$d['tingkat_kelas']][$d['id_agama']]=$d['total'];
		}
		$data['hasil']=$agama;
		
		$agama1=array();
		foreach($data['agama'] as $a){
			foreach($data['tingkat'] as $t){
				$agama1[$a['agama']][$t['tingkat_kelas']] = 0;
			}
		}
		
		foreach($data['data'] as $d){
			$agama1[$d['agama']][$d['tingkat_kelas']]=$d['total'];
		}
		$data['hasil1']=$agama1;
		
		$data['component']="pps";
		// echo "<pre>";
		// print_r($data['hasil1']);
		// echo "</pre>";
		render('agama',$data);
	}
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */