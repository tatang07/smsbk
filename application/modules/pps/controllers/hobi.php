<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class hobi extends CI_Controller {

	public function index(){	
		$this->load->model('pps/m_hobi');
		$data['hobi'] = $this->m_hobi->select_hobi()->result();
		$data['count'] = $this->m_hobi->count_hobi()->num_rows();
		$data['component']="pps";
		render('hobi',$data);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */