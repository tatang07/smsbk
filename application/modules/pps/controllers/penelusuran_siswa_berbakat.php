<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class penelusuran_siswa_berbakat extends Simple_Controller {
    	 public function index(){
			redirect('pps/penelusuran_siswa_berbakat/datalist');
	    }

    	public $myconf = array(
			//Nama controller
			"name" 			=> "pps/penelusuran_siswa_berbakat",
			"component"		=> "pps",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Pelayanan dan Pembinaan Siswa",
			"subtitle"		=> "Penelusuran dan Pembinaan Siswa Berbakat",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "m_siswa_program_pembinaan",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"program_pembinaan"=>"Program Pembinaan",
								"deskripsi"=>"Deskripsi",
								"penanggung_jawab"=>"Penanggung Jawab",
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(								
								"id_sekolah"=>"hidden",
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"program_pembinaan"
							), 
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								// "view_file"				=> "penelusuran_siswa_berbakat/datalist",
								// "view_file_datatable"	=> "penelusuran_siswa_berbakat/datatable",
								// "view_file_datatable_action"	=> "penelusuran_siswa_berbakat/datatable_action",
								"field_list"			=> array("program_pembinaan", "deskripsi", "penanggung_jawab"),
								"field_sort"			=> array("program_pembinaan", "deskripsi", "penanggung_jawab"),
								"field_filter"			=> array("program_pembinaan"),
								"field_filter_hidden"	=> array(),
								// "field_filter_dropdown"	=> array(
								// 								"id_siswa_program_pembinaan"=>array("label"=>"Program Pembinaan","table"=>"m_siswa_program_pembinaan", "table_id"=>"id_siswa_program_pembinaan", "table_label"=>"program_pembinaan"),
								// 							),
								"field_comparator"		=> array("id_sekolah"=>"="),
								"field_operator"		=> array("id_sekolah"=>"AND"),
								//"field_align"			=> array(),
								"field_size"			=> array("action_col"=>"20%"),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array('Detail'=>'pps/penelusuran_siswa_berbakat/detail'),
								// "custom_add_link"		=> array('label'=>'Add','href'=>'pps/penelusuran_siswa_berbakat/add' ),
								// "custom_edit_link"		=> array('label'=>'Edit','href'=>'pps/penelusuran_siswa_berbakat/edit'),
						
								//"custom_delete_link"	=> array(),
								//"custom_link"			=> array("program"=>"program/datalist","Siswa"=>"Siswa/datalist"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								// "view_file"				=> "pps/program/detail",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("program_pembinaan", "id_sekolah", "deskripsi", "penanggung_jawab"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								// "redirect_link"			=> "pps/program"
							),
			//Konfigurasi tampilan edit
			/*"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								// "enable"				=> TRUE,
								// "model_name"			=> "",
								// "table"					=> array(),
								"view_file"				=> "ekskul_jenis/edit",
								"view_file_form_input"	=> "ekskul_jenis/form_input",
								"field_list"			=> array("nama_ekstra_kurikuler", "pelatih", "proker", "tujuan","Foto"),
								// "custom_action"			=> "",
								// "msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								// "msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> "pps"
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);
		public function detail($id){
			//print_r($id);
			$this->load->model('pps/m_siswa_program_pembinaan');
			$data['program'] = $this->m_siswa_program_pembinaan->select_isi_by_id($id)->result();
			$data['siswa'] = $this->m_siswa_program_pembinaan->select_detail($id)->result();
			$data['id'] = $id;
			$data['component'] = "pps";
			render('penelusuran_siswa_berbakat/detail',$data);				
			// print_r($data['siswa']);
		}

		public function tambah($id){
			$data['id'] = $id;
			$data['component'] = "pps";
			render('penelusuran_siswa_berbakat/tambah',$data);				
			// print_r($data['siswa']);
		}

		public function tambah_siswa(){
			$data = $this->input->post();
			unset($data['send']);
			unset($data['nim']);
			$this->db->insert('t_siswa_program_pembinaan_detail', $data);
			set_success_message('Data berhasil ditambah');
			redirect(site_url('pps/penelusuran_siswa_berbakat/detail/')."/".$data['id_siswa_program_pembinaan']);
		}

		public function hapus_siswa($id=0, $id_red=0){
			$this->db->where('id_siswa_program_pembinaan_detail', $id);
			$this->db->delete('t_siswa_program_pembinaan_detail');
			set_success_message('Data berhasil dihapus');
			redirect(site_url('pps/penelusuran_siswa_berbakat/detail/')."/".$id_red);
		}
		
		public function load_add(){	
			$this->load->model('pps/m_siswa_program_pembinaan');
			// $id_sekolah= get_id_sekolah();			
			// $data['tingkat_kelas']=0;
			// $jenjang_sekolah = get_jenjang_sekolah();
			// $data['siswa'] = $this->m_siswa_program_pembinaan->select_kelas($jenjang_sekolah);
			render('penelusuran_siswa_berbakat/form_tambah',$data);
		}

		public function pre_process($data){
			//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
			//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
						
			$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			// $list_program = get_list("m_program","id_program","program");
			$data['conf']['data_list']['subtitle'] = "Penelusuran dan Pembinaan Siswa Berbakat";
			
			$data['conf']['data_add']['subtitle'] = "Tambah";
			$data['conf']['data_edit']['subtitle'] = "Edit";
			
			$data['conf']['data_delete']['redirect_link'] = "pps/penelusuran_siswa_berbakat/datalist/";
			$data['conf']['data_add']['redirect_link']    = "pps/penelusuran_siswa_berbakat/datalist/";
			$data['conf']['data_edit']['redirect_link']   = "pps/penelusuran_siswa_berbakat/datalist/";
			
			return $data;
		}
		
		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
			// $id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			// $_POST['filter']['id_program'] = $id_program;
			$_POST['filter']['id_sekolah'] = get_id_sekolah();

			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															//'id_program'=>$id_program
															'id_sekolah'=>get_id_sekolah() 
														);
			
			//$data['conf']['data_list']['field_filter_dropdown']['id_siswa_program_pembinaan']['filter'] = array('id_sekolah'=>get_id_sekolah());
			// $data['conf']['data_filter']['id_siswa_program_pembinaan']['list'] = get_list_filter("m_siswa_program_pembinaan","id_siswa_program_pembinaan",
			// 					array("program_pembinaan"),
			// 					array('id_sekolah'=>get_id_sekolah()));
			//Mengganti link add yang ada di kanan atas form datalist
			$data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'pps/penelusuran_siswa_berbakat/add/');		
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
		
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'pps/penelusuran_siswa_berbakat/edit');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'pps/penelusuran_siswa_berbakat/delete');
			
			return $data;
		}
		
		public function before_datatable($data){
			//Method ini hanya akan dieksekusi di awal method datatable
			//Mengambil id_pihak yang diposting dari form datalist
			$filter = $this->input->post("filter");

			// $id_program = $filter['id_program'];
			$_POST['filter']['id_sekolah'] = get_id_sekolah();
			//Mengkustomisasi link tombol edit dan delete
			
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'pps/penelusuran_siswa_berbakat/edit/');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'pps/penelusuran_siswa_berbakat/delete/');
			
			return $data;
		}
		
				
		public function before_render_add($data){
			if(!$this->input->post()){
				$id_program = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
				//mengisi nilai untuk id_ di form input
				$_POST['item']['id_sekolah'] = get_id_sekolah();
			}
			return $data;
		}
		
		public function before_add($data){
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
				//$data['item']['id_sekolah'] = get_id_sekolah();
			}
			return $data;
		}
				
		public function before_edit($data){
			if(!$this->input->post()){
				$data['id'] = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 0;
			}
			return $data;
		}
		
		public function before_delete($data){
			$data['id'] = $this->uri->rsegment(3);
			return $data;
		}
}
