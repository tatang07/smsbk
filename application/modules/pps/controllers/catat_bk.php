<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class catat_bk extends Simple_Controller {

    	public $myconf = array(
			//Nama controller
			"name" 			=> "pps/catat_bk",
			"component"		=> "pps",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Pelayanan dan Pembinaan Siswa",
			"subtitle"		=> "catatan BP/BK",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_catat_bk", "id"=>"id_catat_bk"),
			"model_name"	=> "t_catatbk",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"id_siswa"=>"Nama",								
								"nis"=>"Nis",					
								"nama"=>"Nama",					
								"tanggal_catatan"=>"Tanggal Dicatat",													
								"keterangan"=>"Keterangan",
								"id_rombel"=>"Kelas"
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(								
								"id_siswa"=>array("m_siswa","id_siswa","nis"),
								"id_siswa"=>array("m_siswa","id_siswa","nama"),							
								"tanggal_catatan"=> "date",
								"id_rombel"=>array("m_rombel","id_rombel","rombel")
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"catat_bk"
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								"catat_bk"
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("nis","nama","tanggal_catatan","keterangan"),
								"field_sort"			=> array("nis","nama","tanggal_catatan","keterangan"),
								"field_filter"			=> array("nis","nama","tanggal_catatan","keterangan"),
								"field_filter_dropdown"	=> array(
																// "id_jenis_akreditasi"=>array("label"=>"Akreditasi","table"=>"r_jenis_akreditasi", "table_id"=>"id_jenis_akreditasi", "table_label"=>"jenis_akreditasi"),
																"id_rombel"=>array("label"=>"Kelas","table"=>"m_rombel", "table_id"=>"id_rombel", "table_label"=>"rombel"),
															),															
								"field_comparator"		=> array("id_sekolah"=>"="),
								"field_operator"		=> array("id_sekolah"=>"AND"),
								//"field_align"			=> array(),
								"field_size"			=> array("action_col"=>"30%"),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array(
																),
								//"custom_add_link"		=> array('label'=>'Add','href'=>'school/add' ),
								//"custom_edit_link"		=> array(),
								//"custom_delete_link"	=> array(),
								"custom_link"			=> array(
														),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("id_rombel","id_siswa","tanggal_catatan","keterangan"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								//"redirect_link"			=> ""
							),
			//Konfigurasi tampilan edit
			/*"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"view_file"				=> "simple/edit",
								"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array(),
								"custom_action"			=> "",
								"msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								"msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);

		public function before_datalist($data){
			$_POST['filter']['id_sekolah'] = get_id_sekolah();
			return $data;
		}
		
}
