<?php
class m_siswa_organisasi extends MY_Model {
	public $table = "m_siswa_organisasi";
    public $id = "id_siswa_organisasi";
	
	public function get_organisasi(){
		$id=get_id_sekolah();
		$this->db->select('*');
		$this->db->from('m_siswa_organisasi');
		$this->db->where('id_sekolah',$id);
		$this->db->order_by('nama_organisasi','asc');
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_organisasi_like($nama=""){
		$id=get_id_sekolah();
		$this->db->select('*');
		$this->db->from('m_siswa_organisasi');
		$this->db->where('id_sekolah',$id);
		$this->db->like('nama_organisasi',$nama);
		$this->db->order_by('nama_organisasi','asc');
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_organisasi_by($id=0){
		$this->db->select('*');
		$this->db->from('m_siswa_organisasi');
		$this->db->where('id_siswa_organisasi',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	function edit($data, $id){
		$this->db->where('id_siswa_organisasi', $id);
		$this->db->update('m_siswa_organisasi', $data);
	}
	
	function add($data){
		$this->db->insert('m_siswa_organisasi', $data);
	}
	
}
