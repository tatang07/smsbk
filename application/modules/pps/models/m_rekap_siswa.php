<?php
class m_rekap_siswa extends MY_Model {

	public function get_jumlah($id_term, $id_sekolah){
			$this->db->select('r.id_rombel, r.rombel, s.id_siswa, s.jenis_kelamin');
			$this->db->from('m_rombel r'); 
			
			$this->db->join('t_guru_matpel_rombel gm', 'gm.id_rombel=r.id_rombel', 'left');
			$this->db->join('t_agenda_kelas ak', 'ak.id_guru_matpel_rombel=gm.id_guru_matpel_rombel', 'left');
			$this->db->join('t_siswa_absen sa', 'sa.id_agenda_kelas=ak.id_agenda_kelas', 'left');
			$this->db->join('m_siswa s', 's.id_siswa=sa.id_siswa', 'left');
			
			$this->db->where('r.id_tahun_ajaran',get_id_tahun_ajaran());
			// $this->db->where('gm.id_rombel',$id_rombel);
			// $this->db->where('j.id_sekolah',$id_sekolah);
			$this->db->where('s.status_aktif',1);
			$this->db->where('s.id_sekolah',$id_sekolah);
			$this->db->group_by('s.id_siswa');
			$this->db->group_by('r.id_rombel');
			$query = $this->db->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
		}

	public function get_siswa(){
		$query = $this->db
					->select('id_siswa, jenis_kelamin, id_rombel')
					->from('v_siswa')
					->get();
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_all_tingkat($jenjang){
		$query = $this->db
					->from('m_tingkat_kelas')
					->where('id_jenjang_sekolah', $jenjang)
					->get();
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
	
	public function get_all_rombel(){
		$query = $this->db
					->from('m_rombel')
					->where('id_tahun_ajaran',get_id_tahun_ajaran())
					->where('id_sekolah',get_id_sekolah())
					->order_by('rombel')
					->get();
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
}