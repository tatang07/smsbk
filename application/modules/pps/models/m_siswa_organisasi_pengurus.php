<?php
class m_siswa_organisasi_pengurus extends MY_Model {
	public $table = "m_siswa_organisasi_pengurus";
    public $id = "id_siswa_organisasi_pengurus";
	
	public function get_organisasi_pengurus_by($id=0){
		$id_sekolah=get_id_sekolah();
		$this->db->select('*');
		$this->db->from('m_siswa_organisasi_pengurus sop');
		$this->db->join('m_siswa_organisasi_struktur sos','sos.id_siswa_organisasi_struktur = sop.id_siswa_organisasi_struktur');
		$this->db->join('m_siswa s','s.id_siswa = sop.id_siswa');
		$this->db->join('m_siswa_organisasi so','sos.id_siswa_organisasi = so.id_siswa_organisasi');
		$this->db->where('so.id_sekolah',$id_sekolah);
		$this->db->where('so.id_siswa_organisasi',$id);
		
		$query = $this->db->get();
		// echo $this->db->last_query();
		// exit;
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
		
	}
	
	public function get_struktur_by($id=0){
		$this->db->select('*');
		$this->db->from('m_siswa_organisasi_pengurus sop');
		$this->db->join('m_siswa_organisasi_struktur sos','sos.id_siswa_organisasi_struktur = sop.id_siswa_organisasi_struktur');
		$this->db->join('m_siswa s','s.id_siswa = sop.id_siswa');
		$this->db->join('m_siswa_organisasi so','sos.id_siswa_organisasi = so.id_siswa_organisasi');
		$this->db->where('id_siswa_organisasi_pengurus',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	public function get_all_organisasi(){
		$this->db->select('*');
		$this->db->from('m_siswa_organisasi');
		$id=get_id_sekolah();
		$this->db->where('id_sekolah',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	public function get_all_jabatan($id=0){
		$this->db->select('*');
		$this->db->from('m_siswa_organisasi_struktur');
		$this->db->where('id_siswa_organisasi',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function edit($data, $id){
		$this->db->where('id_siswa_organisasi_struktur', $id);
		$this->db->update('m_siswa_organisasi_pengurus', $data);
	}
	
	function add($data){
		$this->db->insert('m_siswa_organisasi_pengurus', $data);
	}
	
}
