<?php
class m_agama extends MY_Model {

	function insert_isi($data){
			$this->db->insert($this->table, $data);
	}
	
	function select_data($id,$tahun_ajaran){
	
			$this->db->select('tk.id_tingkat_kelas,tk.tingkat_kelas,s.id_agama,a.agama,count(s.id_agama) total '); //,count(gmr.id_pelajaran) s 
			$this->db->from('t_rombel_detail rd');
			$this->db->join('m_siswa s', 's.id_siswa = rd.id_siswa');
			$this->db->join('r_agama a', 's.id_agama = a.id_agama');
			// $this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_guru = m_guru.id_guru');
			$this->db->join('m_rombel r', 'r.id_rombel = rd.id_rombel');
			$this->db->join('m_tingkat_kelas tk', 'tk.id_tingkat_kelas = r.id_tingkat_kelas');
			$this->db->where('r.id_tahun_ajaran',$tahun_ajaran);
			$this->db->where('s.id_sekolah',$id);
			$this->db->group_by("tk.id_tingkat_kelas ,id_agama");
			return $this->db->get();
		}
	function select_agama(){
	
			$this->db->select('*'); //,count(gmr.id_pelajaran) s 
			$this->db->from('r_agama');
			return $this->db->get();
	}
	function select_tingkat($jenjang_sekolah){
	
			$this->db->select('*'); //,count(gmr.id_pelajaran) s 
			$this->db->from('m_tingkat_kelas');
			$this->db->where('id_jenjang_sekolah',$jenjang_sekolah);
			return $this->db->get();
	}
	// function get_jumlah_jam($id_pel,$pel){
			// $this->db->select('*');
			// $this->db->from('m_alokasi_waktu_per_minggu awpm');
		
			// $this->db->where_in('awpm.id_tingkat_kelas', $pel );
			// $this->db->where('awpm.id_pelajaran',$id_pel);
			// return $this->db->get()->result_array();
	// }
	
	// function hitung_beban(){
			// $this->db->select('*');
			// $this->db->from('m_alokasi_waktu_per_minggu awpm');
			// $this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_pelajaran = awpm.id_pelajaran');
			// return $this->db->get()->result_array();
	// }

}