<?php
class m_catatanbp extends MY_Model {
    public $table = "m_siswa";
    public $id = "id_siswa";
	
	function get_catatan_by($idsiswa=0){
		$this->db->select('*');
		$this->db->from('t_siswa_catatan_bk sc');
		$this->db->join('t_rombel_detail rd','rd.id_siswa = sc.id_siswa');
		$this->db->join('m_rombel r', 'r.id_rombel = rd.id_rombel');
		$this->db->join('m_siswa s', 's.id_siswa = rd.id_siswa');
		// $this->db->where('r.id_tingkat_kelas',$id_tingkat_kelas);
		$this->db->where('r.id_tahun_ajaran',get_id_tahun_ajaran());
		$this->db->where('s.id_siswa',$idsiswa);
		$this->db->where('s.status_aktif',1);
		
		return $this->db->get()->result_array();
	
	}

	function get_siswa(){
		$this->db->select('s.id_siswa, s.nama');
		$this->db->from('m_siswa s');
		$this->db->join('t_rombel_detail rd', 'rd.id_siswa = s.id_siswa');
		$this->db->join('m_rombel r', 'r.id_rombel = rd.id_rombel');
		
		$this->db->where('r.id_tahun_ajaran',get_id_tahun_ajaran());
		$this->db->where('s.id_sekolah',get_id_sekolah());
		$this->db->where('status_aktif',1);
		
		return $this->db->get()->result_array();
	
	}

	function delete($id_siswa_catatan_bk){
		$this->db->where('id_siswa_catatan_bk', $id_siswa_catatan_bk);
		$this->db->delete('t_siswa_catatan_bk');
	}

	function add($data){
			$this->db->insert('t_siswa_catatan_bk',$data);
	}

	function insert_kontrak($data){
			$this->db->insert('t_rombel_guru_bk',$data);
	}

	function get_guru(){
		$this->db->select('id_guru, nama');
		$this->db->from('m_guru');
		$this->db->where('id_sekolah',get_id_sekolah());
		$this->db->where('status_aktif',1);
		return $this->db->get()->result_array();
	}

	function get_existing(){
		$this->db->select('rbk.id_rombel, rombel');
		$this->db->from('t_rombel_guru_bk rbk');
		$this->db->join('m_rombel r', 'r.id_rombel = rbk.id_rombel');
		$this->db->where('id_tahun_ajaran',get_id_tahun_ajaran());
		$this->db->where('id_sekolah',get_id_sekolah());
		return $this->db->get()->result_array();
	}	

	function get_rombel_none(){
		$now = $this->get_existing();
		$exist = array();
		if($now){
			foreach ($now as $value) {
				$exist[] = $value['id_rombel'];
			}
		}
		$this->db->select('id_rombel, rombel');
		$this->db->from('m_rombel');
		if($now) $this->db->where_not_in('id_rombel', $exist);
		$this->db->where('id_tahun_ajaran',get_id_tahun_ajaran());
		$this->db->where('id_sekolah',get_id_sekolah());
		return $this->db->get()->result_array();
	}
}