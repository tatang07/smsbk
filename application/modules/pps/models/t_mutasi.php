<?php
class t_mutasi extends MY_Model {
    public $table = "t_siswa_mutasi";
    public $id = "id_siswa_mutasi";
	
	public $join_to = array(
		"r_jenis_mutasi_siswa rjms"=>"rjms.id_jenis_mutasi_siswa=t_siswa_mutasi.id_jenis_mutasi_siswa",
		"m_siswa ms"=>"ms.id_siswa=t_siswa_mutasi.id_siswa"
	);
	public $join_fields = array("jenis_mutasi","nis","nama","jenis_kelamin");

	function update_status_siswa($data,$id=0){
			$this->db->where('id_siswa',$id);
			$this->db->update('m_siswa',$data);
	}	
}