<?php

class M_penentuan_kelas extends MY_Model {

    public $table = "m_rombel";
    public $id = "id_rombel";
	
	// join ON 
	public $join_to = array(
			"m_guru g" => "m_rombel.id_guru = g.id_guru",
			"m_kurikulum k" => "m_rombel.id_kurikulum = k.id_kurikulum",
			"m_tingkat_kelas tk" => "m_rombel.id_tingkat_kelas = tk.id_tingkat_kelas"
		 );

	public $join_fields = array("tingkat_kelas", "nama_kurikulum", "rombel", "nama");

	function get($id){
		$this->db->from($this->table);

		foreach ($this->join_to as $field => $on) {
			$this->db->join($field, $on);
		}

		return $this->db->where($this->id, $id)
						->get()->row_array();
	}

	function get_rombel_lawas()
	{
		$data = $this->db
					->from('t_rombel_detail rd')
					->join($this->table.' r', 'r.id_rombel = rd.id_rombel')
					// ->where('id_tahun_ajaran', get_id_tahun_ajaran()-1)
					->where('id_sekolah', get_id_sekolah()) 
					->where('id_tahun_ajaran', get_id_tahun_ajaran() - 1) //ceritanya current tahun ajaran = 2
					->group_by('rd.id_rombel')
					->get();

		if($data->num_rows() > 0){
			return $data->result_array();
		}

		return false;
	}

	function get_siswa_rombel($id_rombel = false, $id_tahun_ajaran = false)
	{
		$this->db->from('t_rombel_detail rd')
				->join('m_siswa s', 's.id_siswa = rd.id_siswa')
				->join('m_rombel r', 'r.id_rombel = rd.id_rombel')
				->where('s.id_sekolah', get_id_sekolah());

		if($id_rombel)
			$this->db->where('r.id_rombel', $id_rombel);

		if($id_tahun_ajaran)
			$this->db->where('r.id_tahun_ajaran', $id_tahun_ajaran);

		$data = $this->db->get();

		if($data->num_rows() > 0){
			return $data->result_array();
		}else{
			return false;
			
		}


	}

	// get siswa yang belum punya rombel terdahulu
	function get_siswa_non_rombel()
	{
		$all_siswa = $this->get_siswa_rombel();
		$siswa = array();
		if($all_siswa){
			foreach ($all_siswa as $value) {
				$siswa[] = $value['id_siswa'];
			}
		}

		$this->db->from('m_siswa');
		$this->db->where('id_sekolah', get_id_sekolah());
		if($all_siswa) $this->db->where_not_in('id_siswa', $siswa);
		$result = $this->db->get();

		if($result->num_rows() > 0)
			return $result->result_array();

		return false;
	}

	// dropdown function
	function get_guru($where = array(), $for_dropdown = false)
	{
		$this->db->from('m_guru');
		if(!empty($where))
			$this->db->where($where);

		$this->db->where('id_sekolah', get_id_sekolah());
		$this->db->where('status_aktif', 1);
		$result = $this->db->get();

		if($result->num_rows() > 0){
			if($for_dropdown){
				$data = array(0 => '-- pilih guru --');
				foreach ($result->result_array() as $value) {
					$data[$value['id_guru']] = $value['nama'];
				}
				return $data;
			} else
				return $result->result_array();
		}

		return false;
	}

	function get_tingkat_kelas($where = array(), $for_dropdown = false)
	{
		$this->db->from('m_tingkat_kelas');
		if(!empty($where))
			$this->db->where($where);

		$this->db->where('id_jenjang_sekolah', get_jenjang_sekolah());
		$result = $this->db->get();

		if($result->num_rows() > 0){
			if($for_dropdown){
				$data = array(0 => '-- pilih tingkat kelas --');
				foreach ($result->result_array() as $value) {
					$data[$value['id_tingkat_kelas']] = $value['tingkat_kelas'];
				}
				return $data;
			} else
				return $result->result_array();
		}

		return false;
	}

	function update_rombel_siswa($id_rombel, $id_rombel_asal, $siswa){
		// delete from kelas asal first
		$this->db->where('id_rombel', $id_rombel_asal)
				->where_in('id_siswa', $siswa)
				->delete('t_rombel_detail');

		// delete all from current kelas 
		$this->db->where('id_rombel', $id_rombel)
				->delete('t_rombel_detail');

		// create new then
		$data = array();
		foreach ($siswa as $id_siswa) {
			$data[] = array(
				'id_siswa' => $id_siswa,
				'id_rombel' => $id_rombel
				);
		}
		$this->db->insert_batch('t_rombel_detail', $data);
	}
}