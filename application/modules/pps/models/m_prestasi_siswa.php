<?php
class m_prestasi_siswa extends MY_Model {
	//public function get_data_search($key,$id_sekolah){
	//	$query = $this->db->query("SELECT * FROM t_prestasi_siswa as ps join r_tingkat_wilayah as tw on ps.id_tingkat_wilayah = tw.id_tingkat_wilayah join m_siswa as s on ps.id_siswa = s.id_siswa where ps.prestasi  like '%$key%' or s.nis like '%$key%' or s.nama like '%$key%' and s.id_sekolah=$id_sekolah");
	//	return $query->result_array();
	//}

	function getPrestasi($id,$ta){
		$this->db->select('*');
		$this->db->from('t_prestasi_siswa ps');
		$this->db->join('r_tingkat_wilayah tw','ps.id_tingkat_wilayah = tw.id_tingkat_wilayah');
		//$this->db->join('m_ekstra_kurikuler ek','ps.id_ekstra_kurikuler = ek.id_ekstra_kurikuler');
		$this->db->join('m_siswa s','ps.id_siswa = s.id_siswa');
		$this->db->where('s.id_sekolah',$id);
		$this->db->where('ps.id_tahun_ajaran',$ta);
		return $this->db->get();
	}

	function getAllPrestasi($id){
		$this->db->select('*');
		$this->db->from('t_prestasi_siswa ps');
		$this->db->join('r_tingkat_wilayah tw','ps.id_tingkat_wilayah = tw.id_tingkat_wilayah');
		//$this->db->join('m_ekstra_kurikuler ek','ps.id_ekstra_kurikuler = ek.id_ekstra_kurikuler');
		$this->db->join('m_siswa s','ps.id_siswa = s.id_siswa');
		$this->db->where('s.id_sekolah',$id);
		return $this->db->get();
	}

	function getPrestasiBy($id){
		$this->db->select('*');
		$this->db->from('t_prestasi_siswa ps');
		$this->db->join('r_tingkat_wilayah tw','ps.id_tingkat_wilayah = tw.id_tingkat_wilayah');
		//$this->db->join('m_ekstra_kurikuler ek','ps.id_ekstra_kurikuler = ek.id_ekstra_kurikuler');
		$this->db->join('m_siswa s','ps.id_siswa = s.id_siswa');
		$this->db->where('ps.id_prestasi_siswa',$id);
		$query = $this->db->get();
		return $query->row_array();
	}

	function getData($nim){
		$this->db->select('id_siswa,nama');
		$this->db->from('m_siswa');
		$this->db->where('nis', $nim);
		$this->db->where('id_sekolah', get_id_sekolah());
		$hasil=$this->db->get()->row();
		return $hasil;
	}

	function getWilayah(){
		$this->db->select('*');
		$this->db->from('r_tingkat_wilayah');
		$hasil=$this->db->get();
		return $hasil;
	}

	function insertData($data){
		$this->db->insert("t_prestasi_siswa", $data);
	}

	function edit($data, $id){
		$this->db->where('id_prestasi_siswa', $id);
		$this->db->update('t_prestasi_siswa', $data);
	}

	function delete($id){
		$this->db->where('id_prestasi_siswa', $id);
		$this->db->delete('t_prestasi_siswa');
	}

	function getTahunAjaran(){
		$this->db->select('*');
		$this->db->from('m_tahun_ajaran');
		return $this->db->get();
	}
}
