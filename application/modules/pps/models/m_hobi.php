<?php
class m_hobi extends MY_Model {
    public $table = "t_siswa_hobi";
    public $id = "id_siswa_hobi";
	
	function select_hobi(){
		$this->db->select('*, count(t_siswa_hobi.id_siswa_hobi) jumlah');
		$this->db->from('t_siswa_hobi');
		$this->db->join('m_siswa', 't_siswa_hobi.id_siswa = m_siswa.id_siswa');
		$this->db->join('r_hobi', 't_siswa_hobi.id_hobi = r_hobi.id_hobi');		
		$this->db->group_by("r_hobi.id_hobi");
		$this->db->where('id_sekolah ='.get_id_sekolah());		
		return $this->db->get();
	}
	
	function count_hobi(){
		$this->db->from('t_siswa_hobi');
		return $this->db->get();
	}

}