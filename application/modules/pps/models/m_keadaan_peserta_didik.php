<?php
class m_keadaan_peserta_didik extends MY_Model {
  
	function select_keadaan_peserta_didik(){
		$this->db->select('tk.id_tingkat_kelas,tk.tingkat_kelas,s.jenis_kelamin,r.rombel,count(s.jenis_kelamin) total');
		$this->db->from('t_rombel_detail rd');
		$this->db->join('m_siswa s', 'rd.id_siswa = s.id_siswa');
		$this->db->join('m_rombel r', 'r.id_rombel = rd.id_rombel');
		$this->db->join('m_tingkat_kelas tk', 'r.id_tingkat_kelas = tk.id_tingkat_kelas');
		$this->db->group_by("tk.id_tingkat_kelas,s.jenis_kelamin");
		$this->db->where('s.status_aktif',1);
		$this->db->where('s.id_sekolah =', get_id_sekolah());
		$this->db->where('r.id_tahun_ajaran =', get_id_tahun_ajaran());		
		return $this->db->get();
	}
	
	function select_tingkat(){
		$this->db->select('*');
		$this->db->from('m_tingkat_kelas');
		$this->db->where('id_jenjang_sekolah =', get_jenjang_sekolah());
		return $this->db->get();
	}
	
	function select_tahun_ajaran(){
		$this->db->select('*');
		$this->db->from('m_tahun_ajaran');
		$this->db->where('id_tahun_ajaran =', get_id_tahun_ajaran());
		return $this->db->get();
	}
	
	function select_rombel(){
		$this->db->select('count(id_rombel) as jumlah, id_tingkat_kelas');
		$this->db->where('id_sekolah =', get_id_sekolah());
		$this->db->where('id_tahun_ajaran =', get_id_tahun_ajaran());
		$this->db->where('rombel <>', 'non-rombel');
		$this->db->from('m_rombel');
		$this->db->group_by('id_tingkat_kelas');
		return $this->db->get();
	}
	
	public function get_siswa(){
		$query = $this->db
					->select('id_siswa, jenis_kelamin, id_tingkat_kelas')
					->from('v_siswa')
					->where('id_tahun_ajaran', get_id_tahun_ajaran())
					->where('id_sekolah', get_id_sekolah())
					->get();
		if($query->num_rows() > 0){
			foreach($query->result() as $data){
				$result[] = $data;
			}	return $result;
		}
	}
}