<?php
class m_angka_melanjutkan extends MY_Model {

	function select_tahun_ajaran(){
		$this->db->select('*');
		$this->db->from('m_tahun_ajaran');
		return $this->db->get();
	}
	
	function select_data(){
		$this->db->select('*,count(spt.id_siswa) as jumlah');
		$this->db->from('t_siswa_perguruan_tinggi spt');
		$this->db->join('r_perguruan_tinggi pt', 'spt.id_perguruan_tinggi=pt.id_perguruan_tinggi');		
		$this->db->join('v_siswa s','s.id_siswa=spt.id_siswa');		
		$this->db->where('s.id_sekolah',get_id_sekolah());
		$this->db->group_by('perguruan_tinggi');
		
		return $this->db->get();
	}
	function select_detail($id_sekolah=0,$id_perguruan_tinggi){
		$id_sekolah=get_id_sekolah();
		$this->db->select('*');
		$this->db->from('t_siswa_perguruan_tinggi spt');		
		$this->db->join('v_siswa s','s.id_siswa=spt.id_siswa');		
		$this->db->join('r_jalur_masuk_pt jm','jm.id_jalur_masuk_pt=spt.id_jalur_masuk_pt');		
		$this->db->where('s.id_sekolah',$id_sekolah);
		$this->db->where('spt.id_perguruan_tinggi',$id_perguruan_tinggi);
		
		// $this->db->group_by('perguruan_tinggi');
		
		return $this->db->get();
	}
	
	function getData($nim){
		$this->db->select('id_siswa,nama');
		$this->db->from('m_siswa');
		$this->db->where('nis', $nim);
		$hasil=$this->db->get()->row();
		return $hasil;
	}
	function getJalurMasuk(){
		$this->db->select('*');
		$this->db->from('r_jalur_masuk_pt');
		$hasil=$this->db->get();
		return $hasil;
	}
	function getRombel(){
		$query = $this->db->query("SELECT * FROM m_tingkat_kelas where id_jenjang_sekolah = ".get_jenjang_sekolah()." ORDER BY id_tingkat_kelas desc LIMIT 1;");
		$row = $query->row();
		
		$id_tingkat_kelas = $row->id_tingkat_kelas;
		
		$this->db->select('*');
		$this->db->from('m_rombel');
		$this->db->where('id_tingkat_kelas', $id_tingkat_kelas);
		$this->db->where('id_sekolah', get_id_sekolah());
		$hasil=$this->db->get();
		return $hasil;
	}
	
	function getSiswa($id_rombel){
		// $query = $this->db->query("SELECT * FROM m_tingkat_kelas where id_jenjang_sekolah = ".get_jenjang_sekolah()." ORDER BY id_tingkat_kelas asc LIMIT 1;");
		// $row = $query->row();
		
		// $id_tingkat_kelas = $row->id_tingkat_kelas;
		
		$this->db->select('*');
		$this->db->from('v_siswa');
		$this->db->where('id_rombel', $id_rombel);
		$this->db->where('id_tahun_ajaran', get_id_tahun_ajaran());
		$hasil=$this->db->get();
		return $hasil;
	}
	
	function getSiswaPT($id_rombel){
		// $query = $this->db->query("SELECT * FROM m_tingkat_kelas where id_jenjang_sekolah = ".get_jenjang_sekolah()." ORDER BY id_tingkat_kelas asc LIMIT 1;");
		// $row = $query->row();
		
		// $id_tingkat_kelas = $row->id_tingkat_kelas;
		
		$id_sekolah=get_id_sekolah();
		$this->db->select('*');
		$this->db->from('t_siswa_perguruan_tinggi spt');		
		$this->db->join('v_siswa s','s.id_siswa=spt.id_siswa');		
		$this->db->where('s.id_rombel', $id_rombel);
		
		// $this->db->group_by('perguruan_tinggi');
		
		return $this->db->get();
	}
	
	function getPerguruanTinggi(){
		$this->db->select('*');
		$this->db->from('r_perguruan_tinggi');
		$hasil=$this->db->get();
		return $hasil;
	}
	
	function tambah_angka_melanjutkan($data){
		$this->db->insert('t_siswa_perguruan_tinggi',$data);
	}
}
