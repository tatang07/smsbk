<?php
class m_gender extends MY_Model {
  
	function select_gender(){
		$this->db->select('tk.id_tingkat_kelas,tk.tingkat_kelas,s.jenis_kelamin,r.rombel,count(s.jenis_kelamin) total');
		$this->db->from('t_rombel_detail rd');
		$this->db->join('m_siswa s', 'rd.id_siswa = s.id_siswa');
		$this->db->join('m_rombel r', 'r.id_rombel = rd.id_rombel');
		$this->db->join('m_tingkat_kelas tk', 'r.id_tingkat_kelas = tk.id_tingkat_kelas');
		$this->db->group_by("tk.id_tingkat_kelas,s.jenis_kelamin");
		$this->db->where('s.id_sekolah ='.get_id_sekolah());
		$this->db->where('r.id_tahun_ajaran ='.get_id_tahun_ajaran());		
		return $this->db->get();
	}
	
	function select_tingkat(){
		$this->db->select('*');
		$this->db->from('m_tingkat_kelas');
		$this->db->where('id_jenjang_sekolah ='.get_jenjang_sekolah());
		return $this->db->get();
	}
	
	function lp(){
		$this->db->select('jenis_kelamin');
		$this->db->from('m_siswa');
		$this->db->group_by('jenis_kelamin');
		return $this->db->get();
	}
	
}