<?php
class m_siswa_organisasi_program extends MY_Model {
	public $table = "m_siswa_organisasi_program";
    public $id = "id_siswa_organisasi_program";
	
	public function get_organisasi_program_by($id=0){
		$this->db->select('*');
		$this->db->from('m_siswa_organisasi_program');
		$this->db->where('id_siswa_organisasi',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
		
	}
	
	public function get_program_by($id=0){
		$this->db->select('*');
		$this->db->from('m_siswa_organisasi_program');
		$this->db->where('id_siswa_organisasi_program',$id);
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	public function get_all_organisasi(){
		$this->db->select('*');
		$this->db->from('m_siswa_organisasi');
		$this->db->where('id_sekolah',get_id_sekolah());
		
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function edit($data, $id){
		$this->db->where('id_siswa_organisasi_program', $id);
		$this->db->update('m_siswa_organisasi_program', $data);
	}
	
	function add($data){
		$this->db->insert('m_siswa_organisasi_program', $data);
	}
	
}
