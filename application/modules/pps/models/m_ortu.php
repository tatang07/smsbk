<?php
class m_ortu extends MY_Model {
    public $table = "m_ortu";
    public $id = "id_ortu";
	
	function add($data){
		$this->db->insert('m_ortu', $data);
	}

	function edit($data, $id){
		$this->db->where('id_ortu', $id);
		$this->db->update('m_ortu', $data);
	}
	
}