<?php
class m_siswa_program_pembinaan extends MY_Model {
    public $table = "m_siswa_program_pembinaan";
    public $id = "id_siswa_program_pembinaan";
	
	
		function select_isi_by_id($id){
			$this->db->select('*');
			$this->db->from('m_siswa_program_pembinaan');
			$this->db->where('id_sekolah', get_id_sekolah());
			$this->db->where('id_siswa_program_pembinaan', $id);
			return $this->db->get();
		}
		
		function select_detail($id){
			$this->db->select('s.nis,s.nama, sp.id_siswa_program_pembinaan_detail, sp.id_siswa_program_pembinaan');
			$this->db->from('t_siswa_program_pembinaan_detail sp');
			$this->db->join('m_siswa s', 'sp.id_siswa = s.id_siswa');
			$this->db->where('sp.id_siswa_program_pembinaan', $id);
			$this->db->group_by("s.id_siswa");
			return $this->db->get();
		}
}
	