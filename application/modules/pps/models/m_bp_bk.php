<?php
class m_bp_bk extends MY_Model {
	
	public function get_kelas($jenjang_sekolah){
			$query = $this->db
						->from('m_tingkat_kelas')
						->order_by('id_tingkat_kelas')
						->where('id_jenjang_sekolah',$jenjang_sekolah)
						->get();
			if($query->num_rows() > 0){
				foreach($query->result() as $data){
					$result[] = $data;
				}	return $result;
			}
	}
	
	// public function get_tahun_ajaran_by_id($id){
			// $query = $this->db
						// ->from('m_tahun_ajaran')
						// ->where('id_tahun_ajaran',$id)
						// ->get();
			// if($query->num_rows() > 0){
				// foreach($query->result() as $data){
					// $result[] = $data;
				// }	return $result;
			// }
	// }
	
	public function get_data_search($id_tingkat_kelas, $nama){
		$id_sekolah = get_id_sekolah();
		$query = $this->db->query("SELECT * FROM t_siswa_catatan_bk as sc join t_rombel_detail as rd on rd.id_siswa=sc.id_siswa join m_rombel as r on r.id_rombel =rd.id_rombel join m_siswa as s on s.id_siswa = rd.id_siswa where s.nama like '%$nama%' and r.id_tingkat_kelas = '$id_tingkat_kelas' and s.id_sekolah='$id_sekolah'");
		return $query->result_array();
	}
	
	function get_data($id_sekolah){
			$this->db->select('*');
			$this->db->from('t_siswa_catatan_bk sc');
			$this->db->join('t_rombel_detail rd','rd.id_siswa = sc.id_siswa');
			$this->db->join('m_rombel r', 'r.id_rombel = rd.id_rombel');
			$this->db->join('m_siswa s', 's.id_siswa = rd.id_siswa');
			// $this->db->where('r.id_tingkat_kelas',$id_tingkat_kelas);
			$this->db->where('s.id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}
	function get_siswa($id_sekolah,$id_tingkat_kelas){
			$this->db->select('*');
			$this->db->from('t_rombel_detail rd');
			$this->db->join('m_siswa s','s.id_siswa = rd.id_siswa');		
			$this->db->join('m_rombel r','r.id_rombel = rd.id_rombel');
			$this->db->where('s.id_sekolah',$id_sekolah);
			$this->db->where('r.id_tingkat_kelas',$id_tingkat_kelas);
			return $this->db->get()->result_array();
	}

	function get_data_edit($id_siswa_catatan_bk){
			$this->db->select('*');
			$this->db->from('t_siswa_catatan_bk sc');
			$this->db->join('t_rombel_detail rd','rd.id_siswa = sc.id_siswa');
			$this->db->join('m_rombel r', 'r.id_rombel = rd.id_rombel');
			$this->db->join('m_siswa s', 's.id_siswa = rd.id_siswa');
			$this->db->where('sc.id_siswa_catatan_bk',$id_siswa_catatan_bk);
			
			return $this->db->get()->result_array();
	}
	
	
	function select_kelas($id_jenjang_sekolah){
			$this->db->select('*');
			$this->db->from('m_tingkat_kelas');
			$this->db->where('id_jenjang_sekolah',$id_jenjang_sekolah);
			
			return $this->db->get()->result_array();
	}
	// function get_data_sub_by_id($id){
			// $this->db->select('*');
			// $this->db->from('t_sekolah_sub_program_kerja');
			// $this->db->where('id_sekolah_sub_program_kerja',$id);
			
			// return $this->db->get()->result_array();
	// }
	// function get_data_sub($id){
			// $this->db->select('*');
			// $this->db->from('t_sekolah_sub_program_kerja');
			// $this->db->where('id_sekolah_program_kerja',$id);
			
			// return $this->db->get()->result_array();
	// }
	function add($data){
			$this->db->insert('t_siswa_catatan_bk',$data);
	}
	// function add_sub($data){
			// $this->db->insert('t_sekolah_sub_program_kerja',$data);
	// }
	function edit($data,$id=0){
			$this->db->where('id_siswa_catatan_bk',$id);
			$this->db->update('t_siswa_catatan_bk',$data);
	}
	// function edit_sub($data,$id=0){
			// $this->db->where('id_sekolah_sub_program_kerja',$id);
			// $this->db->update('t_sekolah_sub_program_kerja',$data);
	// }
	// function delete($id_sekolah_program_kerja){
			// $this->db->where('id_sekolah_program_kerja', $id_sekolah_program_kerja);
			// $this->db->delete('t_sekolah_program_kerja');
	// }
	function delete($id_siswa_catatan_bk){
			$this->db->where('id_siswa_catatan_bk', $id_siswa_catatan_bk);
			$this->db->delete('t_siswa_catatan_bk');
	}
	
	
	
	
	
	
	
	// function hitung_beban(){
			// $this->db->select('*');
			// $this->db->from('m_alokasi_waktu_per_minggu awpm');
			// $this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_pelajaran = awpm.id_pelajaran');
			// return $this->db->get()->result_array();
	// }

}