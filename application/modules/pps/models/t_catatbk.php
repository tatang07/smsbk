<?php
class t_catatbk extends MY_Model {
    public $table = "t_siswa_catatan_bk";
    public $id = "id_siswa_catatan_bk";
	
	public function get_nama_sekolah(){
		return get_session_sekolah("sekolah");
	}	
	
	public $join_to = array(		
		"m_siswa ms"=>"ms.id_siswa=t_siswa_catatan_bk.id_siswa"		
	);
	public $join_fields = array("nis","nama");
}