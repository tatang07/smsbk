<?php
class t_siswa extends MY_Model {
    public $table = "m_siswa";
    public $id = "id_siswa";

 //    public $join_to = array(
	// 	"t_rombel_detail rd"=>"rd.id_siswa = m_siswa.id_siswa",
	// 	"m_rombel r"=>"r.id_rombel = rd.id_rombel",
	// );
	
	function getdetail($id=0){
		$this->db->select('*, sa.foto as poto_siswa, sa.telepon as telepon_siswa, sa.alamat as alamat_siswa'); 
		$this->db->from('m_siswa sa'); 
		$this->db->join('r_golongan_darah gd', 'gd.id_golongan_darah=sa.id_golongan_darah', 'left');
		$this->db->join('r_agama a', 'a.id_agama=sa.id_agama', 'left');
		$this->db->join('r_status_anak rsa', 'rsa.id_status_anak=sa.id_status_anak', 'left');
		$this->db->join('r_kecamatan k', 'k.id_kecamatan=sa.id_kecamatan', 'left');
		$this->db->join('r_asal_sekolah ras', 'ras.id_asal_sekolah=sa.id_asal_sekolah', 'left');
		$this->db->join('m_sekolah ms', 'ms.id_sekolah=sa.id_sekolah', 'left');
		$this->db->where('sa.id_siswa',$id);
		$this->db->order_by('sa.nis','asc');         
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			return false;
		}
	
	}
	
	function gethobi($id=0){
		$this->db->select('id_siswa_hobi, hobi');
		$this->db->from('t_siswa_hobi sh'); 
		$this->db->join('r_hobi h', 'sh.id_hobi=h.id_hobi', 'left');
		$this->db->where('sh.id_siswa',$id);
		$this->db->order_by('h.hobi','asc');
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
		
	}
	
	function getortu($id=0){
		$this->db->select('*, rja.jenjang_pendidikan as jenjang_pendidikan_ayah, rji.jenjang_pendidikan as jenjang_pendidikan_ibu, rjw.jenjang_pendidikan as jenjang_pendidikan_wali, rpa.pekerjaan as pekerjaan_ayah, rpi.pekerjaan as pekerjaan_ibu, rpw.pekerjaan as pekerjaan_wali, rpea.range_penghasilan as range_penghasilan_ayah, rpei.range_penghasilan as range_penghasilan_ibu, rpew.range_penghasilan as range_penghasilan_wali');
		$this->db->from('m_ortu mo');
		$this->db->join('m_siswa ms', 'ms.id_siswa=mo.id_siswa', 'left');
		$this->db->join('r_jenjang_pendidikan rja', 'rja.id_jenjang_pendidikan=mo.id_jenjang_pendidikan_ayah', 'left');
		$this->db->join('r_jenjang_pendidikan rji', 'rji.id_jenjang_pendidikan=mo.id_jenjang_pendidikan_ibu', 'left');
		$this->db->join('r_jenjang_pendidikan rjw', 'rjw.id_jenjang_pendidikan=mo.id_jenjang_pendidikan_wali', 'left');
		$this->db->join('r_pekerjaan rpa', 'rpa.id_pekerjaan=mo.id_pekerjaan_ayah', 'left');
		$this->db->join('r_pekerjaan rpi', 'rpi.id_pekerjaan=mo.id_pekerjaan_ibu', 'left');
		$this->db->join('r_pekerjaan rpw', 'rpw.id_pekerjaan=mo.id_pekerjaan_wali', 'left');
		$this->db->join('r_range_penghasilan rpea', 'rpea.id_range_penghasilan=mo.id_range_penghasilan_ayah', 'left');
		$this->db->join('r_range_penghasilan rpei', 'rpei.id_range_penghasilan=mo.id_range_penghasilan_ibu', 'left');
		$this->db->join('r_range_penghasilan rpew', 'rpew.id_range_penghasilan=mo.id_range_penghasilan_wali', 'left');
		$this->db->join('r_kecamatan rka', 'rka.id_kecamatan=mo.id_kecamatan_ayah', 'left');
		$this->db->join('r_kecamatan rki', 'rki.id_kecamatan=mo.id_kecamatan_ibu', 'left');
		$this->db->join('r_kecamatan rkw', 'rkw.id_kecamatan=mo.id_kecamatan_wali', 'left');
		$this->db->where('mo.id_siswa',$id);
		$this->db->order_by('mo.nama_ayah','asc');
		$query = $this->db->get();
	
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			return false;
		}
	}
	
	function getPrestasiEkstra($id=0){
		$this->db->select('*');
		$this->db->from('t_prestasi_siswa p');
		$this->db->join('r_tingkat_wilayah tw', 'p.id_tingkat_wilayah=tw.id_tingkat_wilayah', 'left');
		$this->db->join('m_ekstra_kurikuler ek', 'p.id_ekstra_kurikuler=ek.id_ekstra_kurikuler', 'left');
		$this->db->where('p.id_siswa',$id);
		$this->db->order_by('tw.tingkat_wilayah','asc');
		$query = $this->db->get();
	
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getPrestasiIntra($id=0){
		$this->db->select('*');
		$this->db->from('t_prestasi_siswa p');
		$this->db->join('r_tingkat_wilayah tw', 'p.id_tingkat_wilayah=tw.id_tingkat_wilayah', 'left');
		$this->db->join('m_ekstra_kurikuler ek', 'p.id_ekstra_kurikuler=ek.id_ekstra_kurikuler', 'left');
		$this->db->where('p.id_siswa',$id);
		$this->db->where('p.id_ekstra_kurikuler',99);
		$this->db->order_by('tw.tingkat_wilayah','asc');
		$query = $this->db->get();
	
		if($query->num_rows() != 0){
			return $query->result_array();
		}else{
			return false;
		}
	}
	
	function getJenjangPendidikan(){
		$this->db->select('*'); 
		$this->db->from('r_jenjang_pendidikan');
		return $this->db->get();
	}
	
	function getPekerjaan(){
		$this->db->select('*'); 
		$this->db->from('r_pekerjaan');
		return $this->db->get();
	}
	
	function getPenghasilan(){
		$this->db->select('*'); 
		$this->db->from('r_range_penghasilan');
		return $this->db->get();
	}
	
	function getKecamatan(){
		$this->db->select('*'); 
		$this->db->from('r_kecamatan');
		return $this->db->get();
	}
	
}