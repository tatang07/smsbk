<?php

class m_import_siswa extends MY_Model {

	function get_id_agama($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('agama', $capt)->get('r_agama')->row_array();
		if($res)
			return $res['id_agama'];
		else
			return 0;
	}

	function get_id_kecamatan($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('kecamatan', $capt)->get('r_kecamatan')->row_array();
		if($res)
			return $res['id_kecamatan'];
		else
			return 0;
	}

	function get_id_jenis_tinggal($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('jenis_tinggal', $capt)->get('r_jenis_tinggal')->row_array();
		if($res)
			return $res['id_jenis_tinggal'];
		else
			return 0;
	}

	function get_id_alat_transportasi($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('alat_transportasi', $capt)->get('r_alat_transportasi')->row_array();
		if($res)
			return $res['id_alat_transportasi'];
		else
			return 0;
	}

	function get_id_golongan_darah($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('golongan_darah', $capt)->get('r_golongan_darah')->row_array();
		if($res)
			return $res['id_golongan_darah'];
		else
			return 0;
	}

	function get_id_asal_sekolah($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('nama_sekolah', $capt)->get('r_asal_sekolah')->row_array();
		if($res)
			return $res['id_asal_sekolah'];
		else {
			$this->db->insert('r_asal_sekolah', array('nama_sekolah' => $capt));
			return $this->db->insert_id();
		}
	}

	function get_id_status_anak($capt)
	{
		if(empty($capt)) return 0;

		if(strtolower($capt) == 'kandung') 
			$capt = 'Anak Kandung';
		else if(strtolower($capt) == 'tiri') 
			$capt = 'Anak Tiri';

		$res = $this->db->where('status_anak', $capt)->get('r_status_anak')->row_array();
		if($res)
			return $res['id_status_anak'];
		else
			return 0;
	}

	function insert_siswa($data)
	{
		$where = array(
				'nisn' => $data['nisn'],
				'nama' => $data['nama'],
				'tanggal_lahir' => $data['tanggal_lahir'],
			);

		$res = $this->db->where($where)->get('m_siswa')->row_array();
		if($res)
			return $res['id_siswa'];
		else {
			$this->db->insert('m_siswa', $data);
			return $this->db->insert_id();
		}
	}

	function get_id_jenjang_pendidikan($obj, $capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('jenjang_pendidikan', $capt)->get('r_jenjang_pendidikan')->row_array();
		if($res)
			return $res['id_jenjang_pendidikan'];
		else {
			$this->db->insert('r_jenjang_pendidikan', array('jenjang_pendidikan' => $capt));
			return $this->db->insert_id();
		}
	}

	function get_id_pekerjaan($obj, $capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('pekerjaan', $capt)->get('r_pekerjaan')->row_array();
		if($res)
			return $res['id_pekerjaan'];
		else {
			$this->db->insert('r_pekerjaan', array('pekerjaan' => $capt));
			return $this->db->insert_id();
		}
	}

	function get_id_range_penghasilan($obj, $capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('range_penghasilan', $capt)->get('r_range_penghasilan')->row_array();
		if($res)
			return $res['id_range_penghasilan'];
		else {
			$this->db->insert('r_range_penghasilan', array('range_penghasilan' => $capt));
			return $this->db->insert_id();
		}
	}

	function insert_ortu($data)
	{
		$where = array(
				'nama_ayah' => $data['nama_ayah'],
				'nama_ibu' => $data['nama_ibu'],
			);

		$res = $this->db->where($where)->get('m_ortu')->row_array();
		if($res)
			return $res['id_ortu'];
		else {
			$this->db->insert('m_ortu', $data);
			return $this->db->insert_id();
		}
	}

	function insert_rombel($data)
	{
		$rombel = $this->db
						->where('rombel', $data['id_rombel'])
						->where('id_tahun_ajaran', get_id_tahun_ajaran())
						->where('id_sekolah', get_id_sekolah())
						->get('m_rombel')->row_array();

		if($rombel){
			$data['id_rombel'] = $rombel['id_rombel'];
			$this->db->insert('t_rombel_detail', $data);
			return $this->db->insert_id();
		} else 
			return false;
	}

}