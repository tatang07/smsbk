<?php
class m_p_ortu extends MY_Model {
  
	function select_ortu(){
		$this->db->select('s.id_siswa, o.*, count(o.id_pekerjaan_ayah) as jumlah, count(o.id_range_penghasilan_ayah) as jumlah_hasil');
		$this->db->from('v_siswa s');
		$this->db->join('m_ortu o','s.id_siswa=o.id_siswa');
		$id=get_id_sekolah();
		$this->db->where('s.id_sekolah', $id);
		// $this->db->where('o.id_range_penghasilan_ayah !=', 0);
		$this->db->group_by('o.id_pekerjaan_ayah');
		return $this->db->get();
	}
	
	function select_pekerjaan(){
		$this->db->select('*');
		$this->db->from('r_pekerjaan');
		return $this->db->get();
	}
	
	function select_penghasilan(){
		$this->db->select('*');
		$this->db->from('r_range_penghasilan');
		return $this->db->get();
	}
	// function select_tahun_ajaran(){
		// $this->db->select('*');
		// $this->db->from('m_tahun_ajaran');
		// $this->db->where('id_tahun_ajaran ='.get_id_tahun_ajaran());
		// return $this->db->get();
	// }
	
	// function select_rombel(){
		// $this->db->select('count(id_rombel) as jumlah, id_tingkat_kelas');
		// $this->db->from('m_rombel');
		// $this->db->group_by('id_tingkat_kelas');
		// return $this->db->get();
	// }
	
	// public function get_ortu(){
		// $query = $this->db
					// ->select('s.id_siswa, o.*')
					// ->from('v_siswa s')
					// ->join('m_ortu o','s.id_siswa=o.id_siswa')
					// ->get();
		// if($query->num_rows() > 0){
			// foreach($query->result() as $data){
				// $result[] = $data;
			// }	return $result;
		// }
	// }
}