<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Edit Foto Siswa</h2>
       </div>
		
		<form action="<?php echo base_url('pps/siswa/submit_foto'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>
					<div class="row">
						<label for="f3_normal_input">
							<strong>Foto</strong>
						</label>
						<div>
							<input type="file" name="foto" />
							<input type="hidden" name="id_siswa" value="<?php echo $id_siswa;?>" />
						</div>
					</div>
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('pps/siswa/detail_siswa').'/'.$id_siswa; ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>
