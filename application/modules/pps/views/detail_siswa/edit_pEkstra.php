
<h1 class="grid_12">Tenaga Pendidik</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>EDIT JABATAN</h2>
       </div>
		
		<form action="<?php echo base_url('ptk/guru/c_update_jabatan'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<?php foreach($det_jabatan as $dj) {?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>JABATAN :</strong>
						</label>
						<div>
							<input type="hidden" name="id_guru" value="<?php echo $id_guru ?>" />
							<input type="hidden" name="id_guru_jabatan" value="<?php echo $dj['id_guru_jabatan'] ?>" />
							<!--<input type="text" name="id_jenis_jabatan" value="<?php //echo $dj['id_jenis_jabatan'] ?>" /> -->
							<select name="id_jenis_jabatan">
							<?php foreach($macam_jabatan as $mj):?>
							  <?php if($dj['id_jenis_jabatan']==$mj['id_jenis_jabatan']){?>
								<option selected value="<?php echo $mj['id_jenis_jabatan']?>"><?php echo $mj['jenis_jabatan'] ?></option>
							  <?php }else {?>
								<option value="<?php echo $mj['id_jenis_jabatan']?>"><?php echo $mj['jenis_jabatan'] ?></option>
							  <?php } ?>
							<?php endforeach ?>
							</select> 
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>STATUS :</strong>
						</label>
						<div>
							<?php if($dj['status']==1){?>
							
							<select name="status">
							  <option selected value="1">Aktif</option>
							  <option value="2">Tidak Aktif</option>
							</select> 
							<?php }else{ ?>
							<select name="status">
							  <option value="1">Aktif</option>
							  <option selected value="2">Tidak Aktif</option>
							</select> 
							
							<?php }?>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>NO SK :</strong>
						</label>
						<div>
							<input type="text" name="no_sk" value="<?php echo $dj['no_sk'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>TANGGAL SK :</strong>
						</label>
						<div>
							<input type="date" name="tgl_sk" value="<?php echo $dj['tgl_sk'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>TMT :</strong>
						</label>
						<div>
							<input type="date" name="tmt_sk" value="<?php echo $dj['tmt_sk'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>KETERANGAN :</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="<?php echo $dj['keterangan'] ?>" />
						</div>
					</div>
					<?php } ?>
				<fieldset>
				<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							<a href="<?php echo base_url('kurikulum/rpp/home/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						
				</div>
		</form>
    </div>
 </div>
