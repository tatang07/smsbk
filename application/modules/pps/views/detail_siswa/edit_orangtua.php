
<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Edit Orang Tua</h2>
       </div>
		
		<form action="<?php echo base_url('pps/siswa/edit_ortu'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<div class="row">
						<div class="header">
							<strong><h2>Ayah</h2></strong>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama Ayah</strong>
						</label>
						<div>
							<input type="text" name="nama_ayah" value="<?php echo $ortu['nama_ayah'] ?>" />
							<input type="hidden" name="edit" value="<?php echo $id_edit ?>" />
							<input type="hidden" name="id_siswa" value="<?php echo $id_siswa ?>" />
							<input type="hidden" name="id" value="<?php echo $ortu['id_ortu'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Alamat Ayah</strong>
						</label>
						<div>
							<input type="text" name="alamat_ayah" value="<?php echo $ortu['alamat_ayah'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenjang Pendidikan Ayah</strong>
						</label>
						<div>
							<select name="jp_ayah">
								<?php foreach($jenjang_pendidikan as $jp): ?>
									<?php if($jp['id_jenjang_pendidikan'] == $ortu['id_jenjang_pendidikan_ayah']){ ?>
										<option selected value='<?php echo $jp['id_jenjang_pendidikan']; ?>'><?php echo $jp['jenjang_pendidikan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_jenjang_pendidikan']; ?>'><?php echo $jp['jenjang_pendidikan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Pekerjaan Ayah</strong>
						</label>
						<div>
							<select name="p_ayah">
								<?php foreach($pekerjaan as $jp): ?>
									<?php if($jp['id_pekerjaan'] == $ortu['id_pekerjaan_ayah']){ ?>
										<option selected value='<?php echo $jp['id_pekerjaan']; ?>'><?php echo $jp['pekerjaan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_pekerjaan']; ?>'><?php echo $jp['pekerjaan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Range Penghasilan Ayah</strong>
						</label>
						<div>
							<select name="rp_ayah">
								<?php foreach($penghasilan as $jp): ?>
									<?php if($jp['id_range_penghasilan'] == $ortu['id_range_penghasilan_ayah']){ ?>
										<option selected value='<?php echo $jp['id_range_penghasilan']; ?>'><?php echo $jp['range_penghasilan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_range_penghasilan']; ?>'><?php echo $jp['range_penghasilan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kecamatan Ayah</strong>
						</label>
						<div>
							<select name="k_ayah">
								<?php foreach($kecamatan as $jp): ?>
									<?php if($jp['id_kecamatan'] == $ortu['id_kecamatan_ayah']){ ?>
										<option selected value='<?php echo $jp['id_kecamatan']; ?>'><?php echo $jp['kecamatan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_kecamatan']; ?>'><?php echo $jp['kecamatan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="header">
							<strong><h2>Ibu</h2></strong>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama Ibu</strong>
						</label>
						<div>
							<input type="text" name="nama_ibu" value="<?php echo $ortu['nama_ibu'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Alamat Ibu</strong>
						</label>
						<div>
							<input type="text" name="alamat_ibu" value="<?php echo $ortu['alamat_ibu'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenjang Pendidikan Ibu</strong>
						</label>
						<div>
							<select name="jp_ibu">
								<?php foreach($jenjang_pendidikan as $jp): ?>
									<?php if($jp['id_jenjang_pendidikan'] == $ortu['id_jenjang_pendidikan_ibu']){ ?>
										<option selected value='<?php echo $jp['id_jenjang_pendidikan']; ?>'><?php echo $jp['jenjang_pendidikan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_jenjang_pendidikan']; ?>'><?php echo $jp['jenjang_pendidikan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Pekerjaan Ibu</strong>
						</label>
						<div>
							<select name="p_ibu">
								<?php foreach($pekerjaan as $jp): ?>
									<?php if($jp['id_pekerjaan'] == $ortu['id_pekerjaan_ibu']){ ?>
										<option selected value='<?php echo $jp['id_pekerjaan']; ?>'><?php echo $jp['pekerjaan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_pekerjaan']; ?>'><?php echo $jp['pekerjaan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Range Penghasilan Ibu</strong>
						</label>
						<div>
							<select name="rp_ibu">
								<?php foreach($penghasilan as $jp): ?>
									<?php if($jp['id_range_penghasilan'] == $ortu['id_range_penghasilan_ibu']){ ?>
										<option selected value='<?php echo $jp['id_range_penghasilan']; ?>'><?php echo $jp['range_penghasilan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_range_penghasilan']; ?>'><?php echo $jp['range_penghasilan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kecamatan Ibu</strong>
						</label>
						<div>
							<select name="k_ibu">
								<?php foreach($kecamatan as $jp): ?>
									<?php if($jp['id_kecamatan'] == $ortu['id_kecamatan_ibu']){ ?>
										<option selected value='<?php echo $jp['id_kecamatan']; ?>'><?php echo $jp['kecamatan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_kecamatan']; ?>'><?php echo $jp['kecamatan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="header">
							<strong><h2>Wali</h2></strong>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama Wali</strong>
						</label>
						<div>
							<input type="text" name="nama_wali" value="<?php echo $ortu['nama_wali'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Alamat Wali</strong>
						</label>
						<div>
							<input type="text" name="alamat_wali" value="<?php echo $ortu['alamat_wali'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenjang Pendidikan Wali</strong>
						</label>
						<div>
							<select name="jp_wali">
								<?php foreach($jenjang_pendidikan as $jp): ?>
									<?php if($jp['id_jenjang_pendidikan'] == $ortu['id_jenjang_pendidikan_wali']){ ?>
										<option selected value='<?php echo $jp['id_jenjang_pendidikan']; ?>'><?php echo $jp['jenjang_pendidikan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_jenjang_pendidikan']; ?>'><?php echo $jp['jenjang_pendidikan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Pekerjaan Wali</strong>
						</label>
						<div>
							<select name="p_wali">
								<?php foreach($pekerjaan as $jp): ?>
									<?php if($jp['id_pekerjaan'] == $ortu['id_pekerjaan_wali']){ ?>
										<option selected value='<?php echo $jp['id_pekerjaan']; ?>'><?php echo $jp['pekerjaan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_pekerjaan']; ?>'><?php echo $jp['pekerjaan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Range Penghasilan Wali</strong>
						</label>
						<div>
							<select name="rp_wali">
								<?php foreach($penghasilan as $jp): ?>
									<?php if($jp['id_range_penghasilan'] == $ortu['id_range_penghasilan_wali']){ ?>
										<option selected value='<?php echo $jp['id_range_penghasilan']; ?>'><?php echo $jp['range_penghasilan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_range_penghasilan']; ?>'><?php echo $jp['range_penghasilan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kecamatan Wali</strong>
						</label>
						<div>
							<select name="k_wali">
								<?php foreach($kecamatan as $jp): ?>
									<?php if($jp['id_kecamatan'] == $ortu['id_kecamatan_wali']){ ?>
										<option selected value='<?php echo $jp['id_kecamatan']; ?>'><?php echo $jp['kecamatan']; ?></option>
									<?php }else{ ?>
										<option value='<?php echo $jp['id_kecamatan']; ?>'><?php echo $jp['kecamatan']; ?></option>
									<?php } ?>
								<?php endforeach; ?>
							</select>
						</div>
					</div>
				<fieldset>
				<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							<a href="<?php echo base_url('pps/siswa/detail_siswa/'.$ortu['id_siswa']); ?>"> <input value="Batal" type="button"></a>
						</div>
				</div>
		</form>
    </div>
 </div>
