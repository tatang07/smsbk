
<h1 class="grid_12">Tenaga Pendidik</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>TAMBAH JABATAN</h2>
       </div>
       
		<?php //print_r($macam_jabatan)?>
		
		<form action="<?php echo base_url('ptk/guru/c_tambah_jabatan'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>					
					<div class="row">
						<label for="f1_normal_input">
							<strong>JABATAN :</strong>
						</label>
						<div>
							
							<select name="id_jenis_jabatan">
							<?php foreach($macam_jabatan as $mj): ?>
							  <option value="<?php echo $mj['id_jenis_jabatan']?>"> <?php echo $mj['jenis_jabatan']?></option>
							<?php endforeach ?>
							</select> 
							<input type="hidden" name="id_guru" value="<?php echo $id_guru ?>" />	
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>STATUS :</strong>
						</label>
						<div>
							 <select name="status">
							  <option value="1">Aktif</option>
							  <option value="2">Tidak Aktif</option>
							</select> 
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>NO SK :</strong>
						</label>
						<div>
							<input type="text" name="no_sk" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>TANGGAL SK :</strong>
						</label>
						<div>
							<input type="date" name="tgl_sk" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>TMT :</strong>
						</label>
						<div>
							<input type="date" name="tmt_sk" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>KETERANGAN :</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="" />
						</div>
					</div>
				<fieldset>
				<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							<a href="<?php echo base_url('ptk/guru/detail/'.$id_guru); ?>"> <input value="Batal" type="button"></a>
						</div>
						
				</div>
		</form>
    </div>
 </div>
