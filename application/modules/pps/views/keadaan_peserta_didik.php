<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>

<div class="grid_12">
  <div class="box with-table">
       <div class="header">
            <h2>Keadaan Peserta Didik Tahun Ajaran <?php echo $tahun_ajaran; ?></h2>
       </div>
    </div>
	
  <div class="box with-table">
	<div class="content">
		<div id="datatable" url="">
			<div class="dataTables_wrapper"  role="grid">    
				<table class="styled" >
					<thead>
						<tr>
							<th rowspan="2">No.</th>
							<th rowspan="2">Tingkat Kelas</th>
							<th rowspan="2">Jumlah Ruangan belajar</th>
							<th colspan="2">Jumlah Siswa</th>
							<th rowspan="2">Total</th>
						</tr>
						<tr>
							<th>L</th>
							<th>P</th>
						</tr>
					</thead>												
					<tbody>
						<?php $no=1;?>																		
						<?php foreach($tingkat as $t):?>																		
							<tr>
								<td width=50px class="center"><?php echo $no; ?></td>
								<td width=100px class="center"><?php echo $t['tingkat_kelas']; ?></td>
								<?php foreach($jumlah_rombel as $j):?>																	
									<?php if($j['id_tingkat_kelas']==$t['id_tingkat_kelas']){ ?>																	
										<td width=100px class="center"><?php echo $j['jumlah']; ?></td>
								<?php } endforeach;?>
								<?php $juml=0; $jump=0; if($lp){ foreach($lp as $j): ?>
									<?php  if($j->id_tingkat_kelas == $t['id_tingkat_kelas']){ ?>
										<?php if($j->jenis_kelamin == 'l' || $j->jenis_kelamin == 'L'){ ?>
										<?php $juml++; }elseif($j->jenis_kelamin == 'p' || $j->jenis_kelamin == 'P'){ ?>
										<?php $jump++;} ?>
									<?php } ?>
								<?php endforeach; } ?>
								<td width="100px" class="center"><?php echo $juml; ?></td>
								<td width="100px" class="center"><?php echo $jump; ?></td>
								<td width="100px" class="center"><b><?php echo $juml+$jump; ?></b></td>
							</tr>
						<?php $no++; endforeach;?>
					</tbody>
				</table>
			</div>
		</div>
	</div>
  </div>	
</div>	
