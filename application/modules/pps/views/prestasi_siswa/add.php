<h1 class="grid_12">Prestasi Siswa</h1>			
	<form action="<?php echo site_url("pps/prestasi_siswa/insertData"); ?>" method="post" enctype="multipart/form-data" class="grid_12">
		<fieldset>
			<legend>Tambah</legend>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>NIS</strong>
				</label>
				<div>
					<input type="text" name="nim" value="" id="nim" required/>
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Nama</strong>
				</label>
				<div>
					<input disabled type="text" value="" id="nama" />
					<input type="hidden" value="" id="id_siswa" name="id_siswa"/>
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Prestasi</strong>
				</label>
				<div>
					<input name="prestasi" type="text" value="" id="prestasi" required/>
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Kategori</strong>
				</label>
				<div>
					<input type="text" name="kategori" value="" id="kategori" required/>
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Perlombaan</strong>
				</label>
				<div>
					<input type="text" name="perlombaan" value="" id="perlombaan" required/>
				</div>
			</div>

			<div class="row">
				<label for="f1_normal_input">
					<strong>Tingkat Wilayah</strong>
				</label>
				<?php 
					$this->load->model('pps/m_prestasi_siswa');
					$data["provinsi"] = $this->m_prestasi_siswa->getWilayah()->result();
					$provinsi=$data["provinsi"];							
					?>
				<div>	
					 <select name="id_tingkat_wilayah" id="provinsi">
						<?php foreach($provinsi as $p) {?>
							<option value="<?php echo $p->id_tingkat_wilayah ?>"><?php echo $p->tingkat_wilayah ?> </option>
						<?php }?>
					 </select>
				</div>
			</div>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Tanggal</strong>
				</label>
				<div>
					<input type="date" name="tanggal" value="" required/>
				</div>
			</div>
			<div class="row">
				<label for="f1_normal_input">
					<strong>Foto</strong>
				</label>
				<div>
					<input type="file" name="foto" value="" required />
				</div>
			</div>
			<div class="row">
				<label for="f1_normal_input">
					<strong>Keterangan</strong>
				</label>
				<div>
					<input type="text" name="keterangan" value="" id="keterangan" />
				</div>
			</div>			
			
		</fieldset><!-- End of fieldset -->
		
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					 <a href="<?php echo base_url('pps/prestasi_siswa/datalist/'); ?>"> <input value="Batal" type="button"></a>
				</div>
				<div class="right">
				</div>
			</div><!-- End of .actions -->
		</form><!-- End of .box -->
	</div><!-- End of .grid_4 -->

<script>
	$(document).ready(function(){
		$("#nim").on("keyup",function(){
			$.get("<?php echo site_url("pps/prestasi_siswa/cekData")?>/" + $(this).val(),function(data,status){
				var a = JSON.parse(data);
				if(a){
					console.log(a);
					$("#nama").val(a.nama);
					$("#id_siswa").val(a.id_siswa);
				}
			});
		});
	});
</script>