<?php  
	$statuspil = 0;
	if(isset($_POST['id_tahun_ajaran'])){
	$a = $_POST['id_tahun_ajaran'];
	$statuspil = $a; } 
?>

<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Siswa Berprestasi</h2>
       </div>

       <div class="tabletools">
			<div class="right">
				<a href="<?php echo base_url('pps/prestasi_siswa/print_pdf/'); ?>"><i class="icon-print"></i>PDF</a> 
			  	<a href="<?php echo base_url('pps/prestasi_siswa/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<!--
				<form action="<?php //echo base_url('pps/prestasi_siswa/search/'.$statuspil); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<td align="left"><input type="text" name="key" id="key"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							
				</form>
				-->
				<form action="" name="myform" method="post" enctype="multipart/form-data">
								<td width="10px"></td>
								<td width="10px"></td>
								<td width="10px"></td>
								<td width="10px"></td>
								<td>
									<select name="id_tahun_ajaran" id="id_tahun_ajaran" onchange="submitform();">
										<option value="0">-Tahun Ajaran-</option>
										<?php foreach($tahunajaran as $ta): ?>
											<?php if($ta['id_tahun_ajaran'] == $statuspil){ ?>
												<option selected value='<?php echo $ta['id_tahun_ajaran']; ?>' data-status-pilihan="<?php echo $ta['id_tahun_ajaran']; ?>"><?php echo $ta['tahun_awal'].'-'.$ta['tahun_akhir']; ?></option>
											<?php }else{ ?>
												<option value='<?php echo $ta['id_tahun_ajaran']; ?>' data-status-pilihan="<?php echo $ta['id_tahun_ajaran']; ?>"><?php echo $ta['tahun_awal'].'-'.$ta['tahun_akhir']; ?></option>
											<?php } ?>
										<?php endforeach; ?>
									</select>
								</td>								
							</tr>
						</tbody>
					</table>
				</form>
			</div>            
        </div>
		
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama</th>
								<th>Prestasi</th>
								<th>Kategori</th>
								<th>Perlombaan</th>
								<th>Tingkat</th>
								<th>Tanggal</th>
								<th>Aksi</th>
								
							</tr>
						</thead>
						<tbody>
							<?php 
								$this->load->model('pps/m_prestasi_siswa');
								$id_sekolah = get_id_sekolah();
								if($statuspil == 0)
									$data['prestasi'] = $this->m_prestasi_siswa->getAllPrestasi($id_sekolah)->result_array();
								else
									$data['prestasi'] = $this->m_prestasi_siswa->getPrestasi($id_sekolah, $statuspil)->result_array();

								//$data['isi'] = $this->m_penilaian_tugas_pokok->get_data($statuspil)->result_array();
								$isi=$data['prestasi'];
							?>
							<?php $no=1;?>
							<?php foreach($isi as $p):?>
								<tr>
									<td width='' class='center'><?php echo $no;?></td>
									<td width='' class='right'><?php echo $p['nis'];?></td>								
									<td width='' class='right'><?php echo $p['nama'];?></td>								
									<td width='' class='right'><?php echo $p['prestasi'];?></td>								
									<td width='' class='right'><?php echo $p['kategori'];?></td>								
									<td width='' class='right'><?php echo $p['perlombaan'];?></td>								
									<td width='' class='right'><?php echo $p['tingkat_wilayah'];?></td>								
									<td width='' class='right'><?php echo tanggal_indonesia($p['tanggal']);?></td>								
									<td class='center'>
										<a href="<?php echo site_url('pps/prestasi_siswa/load_edit/'.$p['id_prestasi_siswa'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>	
										<a href="<?php echo site_url('pps/prestasi_siswa/delete/'.$p['id_prestasi_siswa'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
									</td>							
								</tr>
							<?php $no++;?>
							<?php endforeach ?>
						</tbody>
					</table>
						
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>