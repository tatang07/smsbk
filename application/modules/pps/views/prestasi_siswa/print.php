<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar Prestasi Siswa</h2>
       </div>
			
		<div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled">
						<thead>
							<tr>
								<th>No.</th>
                                <th>NIS</th>
                                <th>Nama</th>
                                <th>Kategori</th>
                                <th>Perlombaan</th>
                                <th>Tingkat</th>
                                <th>Tanggal</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							<?php foreach($prestasi as $p){ ?>
								<tr>
									<td><?php echo $i?></td>
		                            <td><?php echo $p['nis'] ?> </td>
		                            <td><?php echo $p['nama'] ?> </td>
		                            <td><?php echo $p['kategori'] ?> </td>
		                            <td><?php echo $p['perlombaan'] ?> </td>
		                            <td><?php echo $p['tingkat'] ?> </td>
		                            <td><?php echo tanggal_indonesia($p['tingkat']) ?> </td>
								</tr>
							<?php $i++;} ?>
						</tbody>
					</table>
				</div>		
			</div>		
		</div>
 	</div>
</div>
