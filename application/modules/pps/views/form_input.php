<?php //array("id_kecamatan","foto","status_aktif","status_sertifikasi"),?>
        <?php $agama = get_list("r_agama","id_agama","agama") ?>
        <?php $goldar = get_list("r_golongan_darah","id_golongan_darah","golongan_darah") ?>
        <?php $status_anak = get_list("r_status_anak","id_status_anak","status_anak") ?>
        <?php $kec = get_list("r_kecamatan","id_kecamatan","kecamatan") ?>
        <?php $asal = get_list("r_asal_sekolah","id_asal_sekolah","nama_sekolah") ?>
        <?php $aktif = array("1"=>"Aktif","0"=>"Tidak Aktif") ?>
        <?php $id_rombel = get_rombel() ?>
        <div class='row'>
            <?php echo simple_form_input2(array('name'=>'item[nama]', 'id'=>'nama', 'label'=>'Nama', 'class'=>'required','class_div'=>'_50')) ?>
            <?php echo simple_form_input2(array('name'=>'item[nisn]', 'id'=>'nisn', 'label'=>'NISN', 'class'=>'required','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'item[nis]', 'id'=>'nis', 'label'=>'Nomor Induk Siswa', 'class'=>'required','class_div'=>'_25')) ?>
        </div>    
        <div class='row'>
            <?php echo simple_form_dropdown2('item[jenis_kelamin]',array("L"=>"Laki-Laki","P"=>"Perempuan"),null,"",array('label'=>'Jenis Kelamin','class_div'=>'_50')) ?>
            <?php echo simple_form_dropdown2('item[id_golongan_darah]',$goldar,null,"",array('label'=>'Golongan Darah','class_div'=>'_25')) ?>
            <?php echo simple_form_dropdown2('item[id_agama]',$agama,null,"",array('label'=>'Agama','class_div'=>'_25')) ?>
        </div>
        <div class='row'>
            <?php echo simple_form_input2(array('name'=>'item[tempat_lahir]', 'id'=>'tempat_lahir', 'label'=>'Tempat Lahir', 'class'=>'required','class_div'=>'_50')) ?>
            <?php echo simple_form_input2(array('name'=>'item[tanggal_lahir]', 'id'=>'tanggal_lahir', 'label'=>'Tanggal Lahir', 'class'=>'required','class_div'=>'_50', 'type'=>'date')) ?>
        </div> 
        <div class='row'>
            <?php echo simple_form_input2(array('name'=>'item[alamat]', 'id'=>'alamat', 'label'=>'Alamat',  'class'=>'required', 'class_div'=>'_100')) ?>
        </div>
        <div class='row'>
            <?php echo simple_form_dropdown2('item[id_kecamatan]',$kec,null,"",array('label'=>'kecamatan','class_div'=>'_100')) ?>
        </div>
        <div class='row'>
            <?php echo simple_form_input2(array('name'=>'item[kode_pos]', 'id'=>'kode_pos', 'label'=>'Kode Pos',  'class'=>'novalidate','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'item[telepon]', 'id'=>'telepon', 'label'=>'No Telepon',  'class'=>'novalidate','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'item[e_mail]', 'id'=>'email', 'label'=>'Email',  'class'=>'novalidate','class_div'=>'_50')) ?>
        </div>
        <div class='row'>
            <?php echo simple_form_dropdown2('item[id_status_anak]',$status_anak,null,"",array('label'=>'Status Anak','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'item[anak_ke]', 'id'=>'anak_ke', 'label'=>'Anak Ke', 'class'=>'novalidate','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'item[jumlah_sdr]', 'id'=>'jumlah_sdr', 'label'=>'Jumlah Saudara',  'class'=>'novalidate', 'class_div'=>'_25')) ?>
        </div>
        <div class='row'>
            <?php echo simple_form_dropdown2('item[status_aktif]',$aktif,null,"",array('label'=>'Status Aktif','class_div'=>'_25')) ?>
            <?php if(!isset($_POST['item']['id_siswa'])){ ?>
            <?php   echo simple_form_dropdown2('item[id_rombel]',$id_rombel,null,"",array('label'=>'Rombel','class_div'=>'_25')) ?>
            <?php } ?>
        </div>
        
        <?php echo simple_form_input(array('name'=>'item[foto]','type'=>'file', 'id'=>'foto', 'label'=>'Foto')) ?>
		
		<?php echo form_hidden('item[id_siswa]',isset($_POST['item']['id_siswa'])?$_POST['item']['id_siswa']:"") ?>
        