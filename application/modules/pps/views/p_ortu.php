<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekapitulasi Pekerjaan dan Penghasilan Orang Tua</h2>
       </div>
	   <div class="box">
	   <div class="content" style="height: 250px;">
			<table class=chart data-type=pie data-donut=0>
				<thead>
					<tr>
						<th></th>
						<th>Grafik Rekap Pekerjaan Orang Tua</th>
					</tr>
				</thead>
				<tbody>
					<?php foreach($pekerjaan as $p):?>
					<tr>
						<th><?php echo $p['pekerjaan']; ?></th>
						<?php $status=0; foreach($ortu as $o):?>
						<?php if($o['id_pekerjaan_ayah']==$p['id_pekerjaan']){ ?>
							<td><?php echo $o['jumlah'];?></td>
						<?php  $status=1; } endforeach; ?>
						<?php  if($status==0){ ?>
							<td>0</td>
						<?php  } ?>
					</tr>
					<?php endforeach; ?>
				</tbody>	
			</table>
		</div><!-- End of .content -->
		</div><!-- End of .box -->
					
        <div class="content" style="border-top:1px solid #bbb">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jenis Pekerjaan</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($pekerjaan as $p):?>
							<tr>
								<td width="50px" class="center"><?php echo $no;?></td>
								<td width="400px" class=""><?php echo $p['pekerjaan']; ?></td>
								<?php $status=0; foreach($ortu as $o):?>
								<?php if($o['id_pekerjaan_ayah']==$p['id_pekerjaan']){ ?>
									<td width="100px" class="center"><?php echo $o['jumlah'];?></td>
								<?php  $status=1; } endforeach; ?>
								<?php  if($status==0){ ?>
									<td width="100px" class="center">0</td>
								<?php  } ?>
							</tr>
							<?php $no++; endforeach; ?>
						</tbody>
					</table>
					
				</div>
            </div>
        </div>        
        </div> 

	<div class="box with-table">
		<div class="header">
            <h2>Rekapitulasi Penghasilan Orang Tua</h2>
       </div>
	   	   	<div class="box">
					<div class="content" style="height: 250px;">
						<table class=chart data-type=bars>
				<thead>					
				</thead>
				<tbody>
					<?php $no=1; foreach($penghasilan as $p):?>
					<tr>
						<th><?php echo $p['range_penghasilan']; ?></th>
						<?php $status=0; foreach($ortu as $o):?>
						<?php if($o['id_range_penghasilan_ayah']==$p['id_range_penghasilan']){ ?>
							<td><?php echo $o['jumlah_hasil'];?></td>
						<?php  $status=1; } endforeach; ?>
						<?php  if($status==0){ ?>
							<td>0</td>
						<?php  } ?>
					</tr>
					<?php $no++; endforeach; ?>
				</tbody>	
			</table>
		</div><!-- End of .content -->
		</div><!-- End of .box -->
		
		        <div class="content" style="border-top:1px solid #bbb">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<?php foreach($penghasilan as $p):?>
							<th><?php echo $p['range_penghasilan']; ?></th>
							<?php endforeach; ?>
						</thead>
						<tbody>
							<tr>
							<?php foreach($penghasilan as $p):?>
								<?php $status=0; foreach($ortu as $o):?>
								<?php if($o['id_range_penghasilan_ayah']==$p['id_range_penghasilan']){ ?>
									<td width="" class="center"><?php echo $o['jumlah_hasil'];?></td>
								<?php  $status=1; } endforeach; ?>
								<?php  if($status==0){ ?>
									<td width="" class="center">0</td>
								<?php  } ?>
							<?php endforeach; ?>
							</tr>
							
						</tbody>
					</table>
					
				</div>
            </div>
        </div> 	
    </div>
 </div>
