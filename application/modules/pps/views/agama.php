<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<pre>
<?php 
	// print_r($data);
?>

</pre>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Proporsi Agama Siswa</h2>
       </div>
	   	<div class="box">
					<div class="content" style="height: 250px;">
						<table class=chart data-type=bars>
							<thead>
								<tr>
								<th></th>
								<?php foreach($tingkat as $a){?>
									
									<th><?php echo $a['tingkat_kelas']; ?></th>
								<?php }?>
								</tr>
							</thead>
					
							<tbody>
								<?php $no=1;?>
								<?php foreach($hasil1 as $kelas=>$h) {?>		<!-- NGEPRINT KEY NYA-->
								<?php $total=0;?>
								<tr>
									<th><?php echo $kelas ?></th>
									<?php foreach ($h as $h2) {?> 			<!-- NGEPRINT BANYAK AGAMA-->
									<td width='50px' class='center'><?php echo $h2; ?> </td>
									<?php } ?>
								</tr>
								<?php } ?>
								
							</tbody>	
						</table>
					</div><!-- End of .content -->
					
				</div><!-- End of .box -->		
        <div class="content" style="border-top:1px solid #bbb">
		
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan=2>No.</th>
								<th rowspan=2>Tingkat</th>
								<th colspan=7>Jumlah Siswa Menurut Agama</th>
							</tr>
							<tr>
								<?php foreach($agama as $a){?>
									<th><?php echo $a['agama']; ?></th>
								<?php }?>
								<th> Total </th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1;?>
							<?php foreach($hasil as $kelas=>$h) {?>		<!-- NGEPRINT KEY NYA-->
							<?php $total=0;?>
							<tr>
								<td width='30px' class='center'><?php echo $no;?></td>
								<td width='50px'><?php echo $kelas ?></td>
								
								<?php foreach ($h as $h2) {?> 			<!-- NGEPRINT BANYAK AGAMA-->
								<td width='50px' class='center'><?php echo $h2; ?> </td>
								<?php $total=$h2+$total?>
								<?php } ?>
								<td width='50px' class='center'><?php echo $total ?></td>
							</tr>
							<?php $no=$no+1;?>
							<?php } ?>
						</tbody>
					</table>
					
				</div>
            </div>
        </div>        
    </div>
 </div>
