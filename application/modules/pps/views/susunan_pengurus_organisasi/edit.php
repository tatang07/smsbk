<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Edit Pengurus Organisasi Siswa</h2>
       </div>
		
		<form action="<?php echo base_url('pps/susunan_pengurus_organisasi/proses_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>
					<div class="row">
						<label for="f2_normal_input">
							<strong>NIS</strong>
						</label>
						<div>
							<input id='nis' type="text" name="nis" value="<?php echo $detail['nis'];?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>NAMA</strong>
						</label>
						<div>
							<input disabled type="text" value="<?php echo $detail['nama'];?>" id="nama" />
							<input type="hidden" value="" id="id_siswa" name="id_siswa"/>
							<input type="hidden" value="<?php echo $detail['id_siswa_organisasi_struktur'];?>" name="id_siswa_organisasi_struktur"/>
							<input type="hidden" value="<?php echo $id;?>" name="id_redirect"/>
						</div>
					</div>
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('pps/susunan_pengurus_organisasi'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>
 
 <script>
$(document).ready(function(){
  $("#nis").on("keyup",function(){
    $.get("<?php echo site_url("pps/prestasi_siswa/cekData")?>/" + $(this).val(),function(data,status){
      var a = JSON.parse(data);
	  if(a){
		console.log(a);
		$("#nama").val(a.nama);
		$("#id_siswa").val(a.id_siswa);
	  }
	  
    });
  });
});
</script>
