<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Hobi, Minat dan Bakat Siswa</h2>
       </div>
	   <div class="box">
	   <div class="content" style="height: 250px;">
						<table class=chart data-type=pie data-donut=0>
							<thead>
								<?php foreach ($hobi as $d) {?>
								<tr>
									<th><?php echo $d->hobi; ?></th>
							
								</tr>
								<?php } ?>
							</thead>
							
							<tbody>
							<?php foreach ($hobi as $d) {?>
								<tr>
									<th><?php echo $d->hobi; ?></th>
									<td><?php echo $d->jumlah; ?></td>
								</tr>	
							<?php } ?>			
							</tbody>	
						</table>
					</div><!-- End of .content -->
				</div><!-- End of .box -->
	   
		<div class="box">
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
				
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Hobi, Minat dan Bakat Siswa</th>
								<th>Jumlah</th>
							</tr>
						</thead>						
						<tbody>
							<?php $no=1;?>
							
							<?php foreach ($hobi as $d) {?>
							<tr>
								<td width='50px' class='center'><?php echo $no ?></td>
								<td width='400px' class=''><?php echo $d->hobi; ?><br/></td>
								<td width='100px' class='center'><?php echo '<b>'.$d->jumlah.'</b>'; ?></td>
								<?php $no=$no+1 ?>
							</tr>
							<?php }?>							
							<td colspan=2><b>Jumlah</b></td>							
							<td align="center"><b><?php echo $no-1; ?></b></td>	
						</tbody>
					</table>
					
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
