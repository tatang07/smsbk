<h1 class="grid_12">Penentuan Kelas Siswa</h1>
<form action="" class="grid_12 validate" novalidate="novalidate" method="post" id="valid" enctype="multipart/form-data">
	<fieldset>
		<?php $list_pekerjaan = array(1=>"Petani",2=>"Dosen", 0=>"Lainnya") ?>

		<div class="row">
			<label class="" for="tingkat" style="width: 79px;"><strong>Tingkat</strong></label>
			<div style="margin-left: 94px;">
				<label style="margin:20px 0; display: block;"><?php echo $detail['tingkat_kelas']; ?></label>
			</div>
		</div>

		<div class="row">
			<label class="" for="kelas" style="width: 79px;"><strong>Kelas</strong></label>
			<div style="margin-left: 94px;">
				<label style="margin:20px 0; display: block;"><?php echo $detail['rombel']; ?></label>
			</div>
		</div>

		<?php echo simple_form_dropdown('id_rombel', $kelas_asal, $id_rombel_asal, "", array('label'=>'Kelas Asal')); ?>

		<?php echo simple_form_dropdown_dual('siswa[]', $siswa_kelas_asal, $siswa_kelas, "", array('label'=>'Siswa')); ?>

	</fieldset>
	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" />
			<a href="<?php echo site_url('pps/penentuan_kelas')?>"> <input type="button" value="Kembali" /></a>
		</div>
		<div class="right">
			<a href="<?php echo site_url('#')?>"> <input type="button" value="Cetak Form" /></a>
		</div>
	</div>
</form>

<script>
	$(function(){
		$('#id_rombel').chosen().change(function(){
			window.location = "<?php echo site_url('pps/penentuan_kelas/siswa/'.$this->uri->segment(4)).'/'; ?>" + $(this).val();
		});
	})
</script>