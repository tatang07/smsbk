<h1 class="grid_12">Siswa</h1>
<style type="text/css">
	.item{
		padding-left:20px;
	}
</style>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Detail Siswa</h2>
       </div>
	   <pre>
       <?php //print_r($detail)?>
	   </pre>
		<div class="content">
			<div class="dataTables_wrapper"  role="grid">    
				<table class="styled" >
					<tbody>
					<?php foreach($detail as $details):?>
						<tr>
							<td class='item'>NISN</td>
							<td><?php echo $details['nisn']; ?></td>
						</tr>
						<tr>
							<td>NIS</td>
							<td><?php echo $details['nis']; ?></td>
						</tr>
						<tr>
							<td>Nama</td>
							<td><?php echo $details['nama']; ?></td>
						</tr>
						<tr>
							<td>Tempat Lahir</td>
							<td><?php echo $details['tempat_lahir']; ?></td>
						</tr>
						<tr>
							<td>Tanggal Lahir</td>
							<td><?php echo $details['tanggal_lahir']; ?></td>
						</tr>
						<tr>
							<td>Jenis Kelamin</td>
							<td><?php if($details['jenis_kelamin']=='l')echo 'Laki-laki';
										else echo 'perempuan'; ?></td>
						</tr>
						<tr>
							<td>Anak Ke</td>
							<td><?php echo $details['anak_ke']; ?></td>
						</tr>
						<tr>
							<td>Jumlah Saudara</td>
							<td><?php echo $details['jumlah_sdr']; ?></td>
						</tr>
						<tr>
							<td>No Telepon</td>
							<td><?php echo $details['telepon_siswa']; ?></td>
						</tr>
						<tr>
							<td>Alamat</td>
							<td><?php echo $details['alamat']; ?></td>
						</tr>
						<tr>
							<td>Kode POS</td>
							<td><?php echo $details['kode_pos']; ?></td>
						</tr>
						<tr>
							<td>Foto</td>
							<td><?php echo $details['foto']; ?></td>
						</tr>
						<tr>
							<td>Status Aktif</td>
							<td><?php if($details['status_aktif']=='1')echo 'Aktif';
										else echo 'Tidak Aktif'; ?></td>
						</tr>
					
						<tr>
							<td>Golongan Darah</td>
							<td><?php echo $details['golongan_darah']; ?></td>
						</tr>
						<tr>
							<td>Agama</td>
							<td><?php echo $details['agama']; ?></td>
						</tr>
						<tr>
							<td>Status Anak</td>
							<td><?php echo $details['status_anak']; ?></td>
						</tr>
						<tr>
							<td>Kecamatan</td>
							<td><?php echo $details['kecamatan']; ?></td>
						</tr>
						<tr>
							<td>Asal Sekolah</td>
							<td><?php echo $details['nama_sekolah']; ?></td>
						</tr>
						
					<?php endforeach; ?>
					</tbody>
				</table>
			</div>
        </div>        
    </div>
 </div>
