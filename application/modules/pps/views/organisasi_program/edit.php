<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Edit Struktur Organisasi Siswa</h2>
       </div>
		
		<form action="<?php echo base_url('pps/organisasi_program/proses_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>
					<div class="row">
						<label for="f2_normal_input">
							<strong>Program Kegiatan</strong>
						</label>
						<div>
							<input type="text" name="program" value="<?php echo $detail['program'];?>" />
							<input type="hidden" name="id_siswa_organisasi_program" value="<?php echo $detail['id_siswa_organisasi_program'];?>" />
						</div>
					</div>
					<div class="row">
						<label for="f2_normal_input">
							<strong>Tanggal</strong>
						</label>
						<div>
							<input type="date" name="tanggal" value="<?php echo tanggal_view($detail['tanggal']);?>" />
						</div>
					</div>
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('pps/organisasi_program'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>
