								<?php $myconf= $conf['data_list']?>
								<?php $wdt = isset($myconf['field_size']['action_col'])?$myconf['field_size']['action_col']:"15%"?>
								<td  width='<?php echo $wdt?>' class='center'>
									<?php if($conf['data_edit']['enable'] && !empty($myconf['custom_edit_link']) && is_allow($myconf['custom_edit_link']['href']) ):?>
										<a original-title="" href="<?php echo site_url($myconf['custom_edit_link']['href'].'/'.$detail[$myconf['id_model']])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> <?php echo $myconf['custom_edit_link']['label']?></a>
									<?php elseif($conf['data_edit']['enable'] && is_allow($conf['name'].'/edit') && !empty($conf['data_edit']['field_list'])):?>
										<a original-title="Edit" href="<?php echo site_url($conf['name'].'/edit/'.$detail[$myconf['id_model']])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<?php endif ?>
									
									<?php if($conf['data_delete']['enable'] && !empty($myconf['custom_delete_link']) && is_allow($myconf['custom_delete_link']['href']) ):?>																				
										<a original-title="" href="<?php echo site_url($myconf['custom_delete_link']['href'].'/'.$detail[$myconf['id_model']])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('<?php echo $conf['data_delete']['msg_confirmation']?>')"><i class="icon-remove" ></i> <?php echo $myconf['custom_delete_link']['label'] ?></a>
									<?php elseif($conf['data_delete']['enable'] && is_allow($conf['name'].'.delete')):?>
										<a original-title="Delete" href="<?php echo site_url($conf['name'].'/delete/'.$detail[$myconf['id_model']])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('<?php echo $conf['data_delete']['msg_confirmation']?>')"><i class="icon-remove" ></i> Delete</a>
									<?php endif ?>
									
									<?php foreach($myconf['custom_action_link'] as $label=>$url): ?>
										<?php if(is_allow($url)):?>
											<a original-title="<?php echo $label ?>" href="<?php echo site_url($url."/".$detail[$myconf['id_model']])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-share-alt" ></i> <?php echo $label ?></a>
										<?php endif ?>
									<?php endforeach ?>
								</td>
								