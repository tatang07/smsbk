<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
        <div class="header">
            <h2>Penelusuran dan Pembinaan Siswa Berbakat</h2>
        </div>
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pps/penelusuran_siswa_berbakat/tambah/')."/$id"?>"><i class="icon-plus"></i>Tambah</a>
			    <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							<th width=120px align='left'><span class="text">Program Pembinaan:</span></th>
							<td ><?php foreach ($program as $p) {?>
								<?php echo $p->program_pembinaan; ?>
								<?php }?></h2></td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<td width=1px></td>
						</tr>
					</table>
                </form>
            </div>         
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIS</th>
								<th>Nama Siswa</th>								
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1;?>
								<?php foreach ($siswa as $s):?>
								<tr>
									<td width="25px" class='center'><?php echo $no; $no++; ?></td>	
									<td width="200px" class='center'><?php echo $s->nis; ?></td>	
									<td class='left'><?php echo $s->nama; ?></td>									
									<td width="170px" class='center'> 
									<a href="<?php echo base_url('pps/penelusuran_siswa_berbakat/hapus_siswa/')."/$s->id_siswa_program_pembinaan_detail/$s->id_siswa_program_pembinaan"?>" class="button small grey tooltip" data-gravity="s" onClick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
								<?php endforeach;?>						
						</tbody>
					</table>
					<div class="footer">
		
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>