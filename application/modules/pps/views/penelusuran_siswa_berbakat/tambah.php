	<h1 class="grid_12">Penelusuran dan Pembinaan Siswa Berbakat</h1>
			
			<form action="<?php echo site_url("pps/penelusuran_siswa_berbakat/tambah_siswa"); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>NIS</strong>
						</label>
						<div>
							<input type="text" name="nim" value="" id="nim" />
							<input type="hidden" name="id_siswa_program_pembinaan" value="<?php echo $id; ?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>NAMA</strong>
						</label>
						<div>
							<input disabled type="text" value="" id="nama" />
							<input type="hidden" value="" id="id_siswa" name="id_siswa"/>
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pps/penelusuran_siswa_berbakat/detail/').'/'.$id; ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
<script>
$(document).ready(function(){
  $("#nim").on("keyup",function(){
    $.get("<?php echo site_url("pps/prestasi_siswa/cekData")?>/" + $(this).val(),function(data,status){
      var a = JSON.parse(data);
	  if(a){
		console.log(a);
		$("#nama").val(a.nama);
		$("#id_siswa").val(a.id_siswa);
	  }
	  
    });
  });
});
</script>
