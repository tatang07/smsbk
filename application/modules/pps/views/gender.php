<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>

<div class="grid_12">
  <div class="box with-table">
       <div class="header">
            <h2>Proporsi Gender</h2>
       </div>
	<div class="box">
	   <div class="content" style="height: 250px;">
				<table class=chart data-type=bars>
					<thead>
						<tr>
							<th></th>
								<?php foreach($tingkat as $a){?>								
									<th><?php echo $a['tingkat_kelas']; ?></th>
								<?php }?>							
						</tr>
					</thead>
						<tbody>
							<tr>
								<th>Laki-laki</th>								
								<?php foreach ($hasil as $kelas => $t) {?>
									<?php $count=0; ?>
									<?php foreach ($t as $h2) {?> 
										<?php if($count==0){ ?>
										<td width='' class='center'><?php echo $h2; ?> </td>
										<?php $count=1 ?>
										<?php } ?>
									<?php } ?>													
								<?php }?>	
							</tr>
							<tr>
								<th>Perempuan</th>
								<?php foreach ($hasil as $kelas => $t) {?>
									<?php $count=0; ?>
									<?php foreach ($t as $h2) {?> 
										<?php if($count==1){ ?>
										<td width='' class='center'><?php echo $h2; ?> </td>
										
										<?php } ?>
										<?php $count=$count+1; ?>
									<?php } ?>													
								<?php }?>	

							</tr>
						</tbody>
				</table>
		</div><!-- End of .content -->
	</div><!-- End of .box -->
	  
	<div class="box">   
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan="2">No.</th>
								<th rowspan="2">Tingkat</th>
								<th colspan="3">Jumlah Siswa Menurut Jenis Kelamin</th>
							</tr>
							<tr>
								<th>L</th>
								<th>P</th>
								<th>Total</th>
							</tr>
						</thead>												
						<tbody>
							<?php $no=1;?>																		
							<?php foreach ($hasil as $kelas => $t) {?>
							<?php $total=0;?>
								<tr>
									<td width='' class='center'><?php echo $no ?></td>
									<td class='center'><?php echo $kelas ?></td>									
											<?php foreach ($t as $h2) {?> 
												<td width='' class='center'><?php echo $h2; ?> </td>
													<?php $total=$h2+$total?>
											<?php } ?>
									<td width='' class='center'><?php echo $total ?></td>
									<?php $no=$no+1 ?>
							<?php }?>							
								</tr>									
						</tbody>
					</table>
				</div>
			</div>
		</div>
    </div>               
  </div>	
