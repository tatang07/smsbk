<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<?php echo form_open_multipart('pps/siswa/process_import', 'class="grid_12 validate no-box"'); ?>

<fieldset>
	<legend>Import Data Siswa</legend>
	<div class="row">
		<?php echo form_label('CSV file', 'userfile'); ?>		
		<div class="customfile"><?php echo form_upload('userfile'); ?></div>
	</div>
</fieldset>

<div class="actions">
	<div class="left">
		<?php echo form_submit('submit_upload', 'Upload', 'class="btn"'); ?>
	</div>
	<div class="right">
		<a href="#"><span class="icon icon-download"></span> Download Format Excel</a>
	</div>
</div>

<?php echo form_close(); ?>