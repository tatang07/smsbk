<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<style type="text/css">
	.item{
		padding-left:20px;
	}
</style>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Identitas Siswa</h2>
       </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<div class="content" style='margin:5px;'>
					<table style="border-spacing: 0px; margin: 5px 0 20px 0; width: 100%; border: 1px solid #D2D2D2; background: #EDF2FD; padding: 5px;">
						<tbody>
							<tr>
								<td class="item" ><h2>Siswa</h2></td>
								<td></td>
								<td></td>
								<td style="padding-left:10px; padding-right:8px; padding-top:8px; text-align:right" rowspan='13'>
									<?php if(empty($detail['poto_siswa'])){?>
									<div class="photos" style="display:inline-block; text-align:center;" >
										<img style="text-align:center; display:block;" class=" foto" width="200" height="200" src= '<?php echo base_url()?>extras/logo_default.jpg' />
										<a href="<?php echo site_url('pps/siswa/edit_foto/'.$detail['id_siswa']);?>"><i class="icon-edit"></i> Edit Foto</a>
									</div>
									<?php }else{?>
									<div class="photos" style="display:inline-block; text-align:center;" >
										<img style="text-align:center; display:block;" class=" foto" width="200" height="200" src= '<?php echo base_url()?>extras/siswa/<?php  echo $detail['poto_siswa']?>' />
										<a href="<?php echo site_url('pps/siswa/edit_foto/'.$detail['id_siswa']);?>"><i class="icon-edit"></i> Edit Foto</a>
									</div>
									<?php }?>
								</td>
								
							</tr>
							<tr>
								<td class="item" style='width:150px;'>Nama</td>
								<td>:&nbsp;</td>
								<td><?php echo $detail['nama']; ?></td>
							</tr>
							<tr>
								<td class="item" >NIS</td>
								<td>:&nbsp;</td>
								<td><?php echo $detail['nis']; ?></td>
							</tr>
							<tr>
								<td class="item" >NISN</td>
								<td>:&nbsp;</td>
								<td><?php echo $detail['nisn']; ?></td>
							</tr>
							<tr>
								<td class="item" >Tempat / Tgl. Lahir</td>
								<td>:&nbsp;</td>
								<td><?php echo $detail['tempat_lahir']; ?>, <?php echo $detail['tanggal_lahir']; ?></td>
							</tr>
							<tr>
								<td class="item" >Jenis Kelamin</td>
								<td>:&nbsp;</td>
								<td><?php if($detail['tempat_lahir']=='l'){
										echo "Laki-laki";
									}else{
										echo "Perempuan";
									} ?>
								</td>
							</tr>
							<tr>
								<td class="item" >Golongan Darah</td>
								<td>:&nbsp;</td>
								<td><?php echo $detail['golongan_darah']; ?></td>
							</tr>
							<tr>
								<td class="item" >Agama</td>
								<td>:&nbsp;</td>
								<td><?php echo $detail['agama']; ?></td>
							</tr>
							<tr>
								<td class="item" >Status Anak</td>
								<td>:&nbsp;</td>
								<td><?php echo $detail['status_anak']; ?></td>
							</tr>
							<tr>
								<td class="item">Anak ke</td>
								<td>:&nbsp;</td>
								<td><?php echo $detail['anak_ke']; ?> Dari <?php echo $detail['jumlah_sdr']; ?> saudara</td>
							</tr>
							<tr>
								<td class="item">Alamat</td>
								<td>:&nbsp;</td>
								<td><?php echo $detail['alamat_siswa']; ?></td>
							</tr>
							<tr>
								<td class="item">Nomor Telepon</td>
								<td>:&nbsp;</td>
								<td><?php echo $detail['telepon_siswa']; ?></td>
							</tr>
							<tr>
								<td class="item"><h2>Ayah</h2></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="item">Nama Ayah</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['nama_ayah']; ?></td>
							</tr>
							<tr>
								<td class="item">Pendidikan Ayah</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['jenjang_pendidikan_ayah']; ?></td>
							</tr>
							<tr>
								<td class="item">Pekerjaan Ayah</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['pekerjaan_ayah']?></td>
							</tr>
							<tr>
								<td class="item">Penghasilan Ayah</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['range_penghasilan_ayah']?></td>
							</tr>
							<tr>
								<td class="item">Alamat Ayah</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['alamat_ayah']; ?></td>
							</tr>
							<tr>
								<td class="item"><h2>Ibu</h2></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="item">Nama Ibu</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['nama_ibu']; ?></td>
							</tr>
							<tr>
								<td class="item">Pendidikan Ibu</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['jenjang_pendidikan_ibu']; ?></td>
							</tr>
							<tr>
								<td class="item">Pekerjaan Ibu</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['pekerjaan_ibu']?></td>
							</tr>
							<tr>
								<td class="item">Penghasilan Ibu</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['range_penghasilan_ibu']?></td>
							</tr>
							<tr>
								<td class="item">Alamat Ibu</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['alamat_ibu']; ?></td>
							</tr>
							<?php if($ortu['nama_wali']!=""){ ?>
							<tr>
								<td class="item"><h2>Wali</h2></td>
								<td></td>
								<td></td>
							</tr>
							<tr>
								<td class="item">Nama Wali</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['nama_wali']; ?></td>
							</tr>
							<tr>
								<td class="item">Pendidikan Wali</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['jenjang_pendidikan_wali']; ?></td>
							</tr>
							<tr>
								<td class="item">Pekerjaan Wali</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['pekerjaan_wali']?></td>
							</tr>
							<tr>
								<td class="item">Penghasilan Wali</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['range_penghasilan_wali']?></td>
							</tr>
							<tr>
								<td class="item">Alamat Wali</td>
								<td>:&nbsp;</td>
								<td><?php echo $ortu['alamat_wali']; ?></td>
							</tr>
							<?php } ?>
							<tr>
								<td colspan='6' style="text-align:right;">
									<a href="<?php echo site_url('pps/siswa/cek_ortu/'.$detail['id_siswa']);?>"><button class="block">LENGKAPI ORTU/WALI</button></a>
								</td>
							</tr>
						</tbody>
					</table>
					
					<div class="box tabbedBox">
						<div class="header">
							<h2>Prestasi dan Hobi</h2>
							<ul>
								<li><a href="#t1-c1">Prestasi Ekstrakurikuler</a></li>
								<li><a href="#t1-c2">Prestasi Intrakulikuler</a></li>
								<li><a href="#t1-c3">Hobby</a></li>
							</ul>
						</div><!-- End of .header -->
						
						<div class="content tabbed">
							<div id="t1-c1">
								<table class="styled">
									<thead>
										<tr>
											<th>No.</th>
											<th>Prestasi Ektrakurikuler</th>
											<th>Ekskul</th>
											<th>Tingkat</th>
											<th>Tahun</th>
										</tr>
									</thead>
									<tbody>
										<?php											
											$no=1;
											if($pEkstra != null){
												foreach ($pEkstra as $a){
													if($a['id_ekstra_kurikuler']!=99){
													echo "<tr><td width='' class='center'>".$no."</td>";
													$no++;
													echo "<td width='' class='center'>".$a['prestasi']."</td>";
													echo "<td width='' class='center'>".$a['nama_ekstra_kurikuler']."</td>";
													echo "<td width='' class='center'>".$a['tingkat_wilayah']."</td>";
													echo "<td width='' class='center'>".$a['tanggal']."</td>";
										?>
											<?php echo "</tr>"; }} ?>
										<?php } ?>
									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>	
								</div>
							</div>
							
							<div id="t1-c2">
								<table class="styled">
									<thead>
										<tr>
											<th>No.</th>
											<th>Prestasi Intrakurikuler</th>
											<th>Tingkat</th>
											<th>Tahun</th>
										</tr>
									</thead>
									<tbody>
										<?php											
											$no=1;
											if($pIntra != null){
												foreach ($pIntra as $a){
													echo "<tr><td width='' class='center'>".$no."</td>";
													$no++;
													echo "<td width='' class='center'>".$a['prestasi']."</td>";
													echo "<td width='' class='center'>".$a['tingkat_wilayah']."</td>";
													echo "<td width='' class='center'>".$a['tanggal']."</td>";
										?>
												<?php echo "</tr>"; } ?>
										<?php } ?>
									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>	
								</div>
								
							</div>
							
							<div id="t1-c3">
								<div class="tabletools">
									<div class="right">
									<a href="<?php echo site_url('pps/siswa/tambah_hobi/'.$detail['id_siswa']);?>"><i class="icon-plus"></i>Tambah Hobi</a> 
									</div>
								</div>
								<table class="styled">
									<thead>
										<tr>
											<th>No.</th>
											<th>Hobby</th>
											<th colspan='2'>Aksi</th>
										</tr>
									</thead>
									<tbody>										
										<?php											
											if($hobi != null){
											$no=1;
												foreach ($hobi as $a){
													echo "<tr><td width='10px' class='center'>".$no."</td>";
													$no++;
													echo "<td width='400px' class='left'>".$a['hobi']."</td>";
										?>
												<td width=100px class="center">
												<a href="<?php echo site_url('pps/siswa/hapus_hobi/'.$a['id_siswa_hobi']);?>"class="button small grey tooltip" data-gravity="s" onClick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Delete</a>
												</td>
											<?php echo "</tr>"; } ?>
										<?php } ?>										
									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>	
								</div>
							</div>							
						</div><!-- End of .content -->
					</div><!-- End of .box -->
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>