	<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
			
			<form action="<?php echo base_url('pps/siswa/submit_hobi'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Hobi</legend>
					
					<div class="row">
						<label for="f2_select2">
							<strong>Hobi</strong>
						</label>
						<div>
							<input type="hidden" name="action" value="add" />
							<input type="hidden" name="id" value="<?php echo $id; ?>" />
							<select name="hobi" data-placeholder="Piilih Hobi">
							<?php foreach($hobi as $t): ?>
									<option value="<?php echo $t['id_hobi']; ?>"><?php echo $t['hobi']; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pps/siswa/detail_siswa/'.$id); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		