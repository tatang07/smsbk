<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Rekapitulasi Jumlah Siswa</h2>
       </div>
	</div>
	<?php foreach($tingkat_kelas as $t): ?>
    <div class="box with-table">
        <div class="content">
	       <div class="header">
	            <h2>Tingkat <?php echo $t->tingkat_kelas; ?></h2>
	       </div>
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th rowspan='2'>No.</th>
								<th rowspan='2'>Kelas</th>
								<th colspan='3'>Jumlah</th>
							</tr>
							<tr>
								<th>L</th>
								<th>P</th>
								<th>Total</th>
							</tr>
						</thead>
						<tbody>
							<?php if ($rombel){ $no=1; ?>
							<?php foreach($rombel as $r): ?>
								<tr>
									<?php if($r->id_tingkat_kelas == $t->id_tingkat_kelas and strcmp($r->rombel, 'non-rombel')!=0){ ?>
										<td width="50px" class="center"><?php echo $no; $no++; ?></td>
										<td width="250px" class="center"><?php echo $r->rombel; ?></td>
										<?php $juml=0; $jump=0; if($jumlah){ foreach($jumlah as $j): ?>
											<?php  if($j->id_rombel == $r->id_rombel){ ?>
												<?php if($j->jenis_kelamin == 'l' || $j->jenis_kelamin == 'L'){ ?>
												<?php $juml++; }elseif($j->jenis_kelamin == 'p' || $j->jenis_kelamin == 'P'){ ?>
												<?php $jump++;} ?>
											<?php } ?>
										<?php endforeach; } ?>
										<td width="100px" class="center"><?php echo $juml; ?></td>
										<td width="100px" class="center"><?php echo $jump; ?></td>
										<td width="100px" class="center"><b><?php echo $juml+$jump; ?></b></td>
									<?php } ?>
								</tr>
							<?php endforeach;?>
							<?php } ?>
						</tbody>
					</table>
				</div> <!-- end of datatable wrapper -->
            </div>  <!-- end of datatable -->
        </div> <!--  end of content -->
    </div>
	<?php endforeach; ?>
 </div>
