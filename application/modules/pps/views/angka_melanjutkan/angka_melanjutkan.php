<?php  
	$statuspil = 0;
	if(isset($_POST['id_tahun_ajaran'])){
	$a = $_POST['id_tahun_ajaran'];
	$statuspil = $a; } ?>
	<pre>
		<?php //print_r ($hasil) ?>
	</pre>
<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Angka Melanjutkan</h2>
		</div>

		<div class="tabletools">
			<div class="right">
			  	<a href="<?php echo site_url('pps/angka_melanjutkan/load_tambah_angka_melanjutkan/')?>"> <i class="icon-plus"></i> Tambah</a> 
			  <br/><br/>
            </div>
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Tampilkan</span></th>
							<td width=200px align='left'>
								<select name="id_tahun_ajaran" id="id_tahun_ajaran" onchange="submitform();">
									<option value="0">-Pilih Tahun Ajaran-</option>
									<?php foreach($tahun_ajaran as $q): ?>
										
										<?php if($q['id_tahun_ajaran'] == $statuspil){ ?>
											<option selected value='<?php echo $q['id_tahun_ajaran']; ?>' data-status-pilihan="<?php echo $q['id_tahun_ajaran']; ?>"><?php echo $q['tahun_awal'].'/'.$q['tahun_akhir']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q['id_tahun_ajaran']; ?>' data-status-pilihan="<?php echo $q['id_tahun_ajaran']; ?>"><?php echo $q['tahun_awal'].'/'.$q['tahun_akhir']; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					
					<?php if($statuspil!=0){ ?>
					
					<table class="styled" >
						<thead>
							<tr>
								<th width="20px">No.</th>
								<th>Nama Universitas</th>
								<th>Jumlah</th>
								<th>Aksi</th>
							</tr>
						
						</thead>
				
						<tbody>
							<?php $no=1; ?>
							<?php foreach($hasil as $h):?>
							<?php if($statuspil==$h['id_tahun_ajaran']){ ?>
							<tr>
								<td width='' class='center'> <?php echo $no; $no++; ?> </td>
								<td width='' class='center'> <?php echo $h['perguruan_tinggi']?> </td>
								<td width='' class='center'> <?php echo $h['jumlah'] ?> </td>
								<td width='' class='center'>
									<a class="button small grey tooltip" data-gravity="s" href="<?php echo site_url('pps/angka_melanjutkan/detail/'.$h['id_perguruan_tinggi'])?>" ><i class="icon-list"></i> Detail</a>
								</td>
							</tr>
							<?php } endforeach; ?>
						</tbody>
						
							
					</table>
					
					<?php }else{ ?>
					<div class="footer">
						<div class="dataTables_info">Pilih Tahun Ajaran Yang Anda Cari</div>
					</div>
					<?php } ?>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>