	<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
			
			<form action="<?php echo base_url('pps/angka_melanjutkan/c_angka_melanjutkan'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Angka Melanjutkan</legend>
					
					<input type="hidden" name="id_tahun_ajaran" value="<?php echo $id_tahun_ajaran; ?>" />
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kelas</strong>
						</label>
						<div>
							<select name="id_rombel" id="id_rombel" data-placeholder="-- Pilih Kelas --">
								<option value="">-- Pilih Kelas --</option>
								<?php foreach($rombel as $ta):?>
									<?php if($ta['id_rombel'] == $id_rombel){ ?>
									<option selected value="<?php echo $ta['id_rombel']; ?>"><?php echo $ta['rombel'];?> </option>
									<?php }else{ ?>
										<option value="<?php echo $ta['id_rombel']; ?>"><?php echo $ta['rombel'];?> </option>
									<?php } ?>
								<?php endforeach; ?>
							</select> 
						</div>
					</div>
						
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jalur Masuk PT</strong>
						</label>
						<div>
							 <select name="id_jalur_masuk_pt" data-placeholder="-- Pilih Jalur Masuk PT --">
								<option value="">-- Pilih Jalur Masuk PT --</option>
								<?php foreach($jalur_masuk as $jm):?>
							  <option value="<?php echo $jm['id_jalur_masuk_pt']?>"><?php echo $jm['jalur_masuk_pt']?> </option>
								<?php endforeach; ?>
							</select> 
						</div>
					</div>
		
					<div class="row">
						<label for="f1_normal_input">
							<strong>Perguruan Tinggi</strong>
						</label>
						<div>
							<select name="id_perguruan_tinggi" data-placeholder="-- Pilih Perguruan Tinggi --">
								<option value="">-- Pilih Perguruan Tinggi --</option>
								<?php foreach($perguruan_tinggi as $pt):?>
							  <option value="<?php echo $pt['id_perguruan_tinggi']?>"><?php echo $pt['perguruan_tinggi']?> </option>
								<?php endforeach; ?>
							</select> 
						</div>
					</div>
					
					<?php echo simple_form_dropdown_dual('siswa[]', $siswa_kelas_asal, $siswa_kelas, "", array('label'=>'Siswa')); ?>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pps/angka_melanjutkan/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		


<script>
	$(function(){
		$('#id_rombel').chosen().change(function(){
			window.location = "<?php echo site_url('pps/angka_melanjutkan/load_tambah_angka_melanjutkan/').'/'; ?>" + $(this).val();
		});
	})
</script>