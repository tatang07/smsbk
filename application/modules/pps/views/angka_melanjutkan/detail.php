<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Angka Melanjutkan</h2>
		</div>

        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
			
					<table class="styled" >
						<thead>
							<tr>
								<th width="20px">No.</th>
								<th>NIS</th>
								<th>NAMA</th>
								<th>JALUR MASUK</th>
							</tr>
						
						</thead>
				
						<tbody>
							<?php $no=1; ?>
							<?php foreach($hasil as $h):?>
							<tr>
								<td width='' class='center'> <?php echo $no; $no++; ?> </td>
								<td width='' class='center'> <?php echo $h['nis']?> </td>
								<td width='' class='center'> <?php echo $h['nama'] ?> </td>
								<td width='' class='center'> <?php echo $h['jalur_masuk_pt'] ?> </td>
							</tr>
							<?php endforeach ?>
						</tbody>
						
							
					</table>
					
				
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1 ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
	<a href="<?php echo site_url('pps/angka_melanjutkan/');?>"><button class="block">Kembali</button></a>
</div>