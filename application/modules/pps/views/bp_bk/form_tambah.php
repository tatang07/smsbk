	<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
			
			<form action="<?php echo base_url('pps/bp_bk/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Daftar Catatan BP/BK</legend><?php //echo($tingkat_kelas);?>
					<?php $jenjang_sekolah = get_jenjang_sekolah(); ?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kelas</strong>
						</label>
						<div>
							
							<select name="kelas" id="kelas" >
							<option value="0">-Tingkat Kelas-</option>
							<?php foreach($kelas as $d):?>
								<?php if($tingkat_kelas==$d['id_tingkat_kelas']){?>
							  <option selected value="<?php echo $d['id_tingkat_kelas']?>"><?php echo $d['tingkat_kelas']?></option>
								<?php }else { ?>
								 <option value="<?php echo $d['id_tingkat_kelas']?>"><?php echo $d['tingkat_kelas']?></option>
								<?php } ?>
							 <?php endforeach ?>
							</select> 
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Siswa</strong>
						</label>
						<div>
							<select name="id_siswa" >
							
							<?php foreach($tabi as $d):?>
							  <option value="<?php echo $d['id_siswa']?>"><?php echo $d['nama']?></option>
							 <?php endforeach ?>
							</select> 
					
						</div>
					</div>
			
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal</strong>
						</label>
						<div>
							<input type="date" name="tanggal" value="" />
							
						</div>
					</div>

					<div class="row">
						<label for="f1_normal_input">
							<strong>Keterangan</strong>
						</label>
						<div>
							<input type="text" name="keterangan" value="" />
						</div>
					</div>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pps/bp_bk/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		

<script>
$(function(){
	$('#kelas').chosen().change(function(){
		var id_tingkat_kelas = $(this).val();
		console.log(id_tingkat_kelas);
		document.location = "<?php echo site_url('pps/bp_bk/get_nama_siswa'); ?>/" + id_tingkat_kelas;
	});
	
	// $(document).on('change', '#kabupaten', function(){
		// var id_kabupaten = $(this).val();
		// console.log(id_kabupaten);
		// $.post("<?php echo site_url('pps/prestasi_siswa/getKecamatan'); ?>/" + id_kabupaten, function(data){
			// $("#kecamatan").html(data);
			// $("#kecamatan").trigger("chosen:updated");
		// });
	// });
});
</script>