<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
   	<div class="box">
			
		<div class="header">
			<h2>Organisasi Siswa</h2>
		</div>
		<?php //print_r($kepsek);?>
		<div class="tabletools">
			<div class="right">
				<a href="<?php echo base_url('pps/organisasi_siswa/add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
				<a href="<?php echo base_url('pps/organisasi_siswa/print_pdf/'); ?>"><i class="icon-print"></i>Cetak Ke File</a> 
			  <br/><br/>
			</div>
			<div class="dataTables_filter">
				<form action="<?php echo base_url('pps/organisasi_siswa/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=110px align='left'><span class="text">Nama Organisasi &nbsp:</span></th>
							<td width=100px align='left'>
								<input type="text" name="nama_organisasi" value="" />
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
							<input type="submit" name=send class="button grey tooltip" value="cari" />
							<!-- <a href="search" class="button grey tooltip"><i class='icon-search'></i></a> -->
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
			<div class="dataTables_wrapper"  role="grid">
				<table class="styled" >
					<thead>
						<tr colspan='2'>
						</tr>
						<tr>
							<td width='20px' class='center'>No.</td>
							<td width='' class='center'>Organisasi</td>
							<td width='100px' class='center'>Inisial</td>
							<td width='' class='center'>Sekeretariat</td>
							<td width='120px' class='center'>Aksi</td>
						</tr>
					</thead>
					<tbody>
						<?php
							if($organisasi){
							$no=1;
							foreach($organisasi as $temp){
						?>
						<tr>
							<td class='center'>
								<?php
									echo $no;
									$no++;
								?>
							</td>
							<td class='right'><?php echo $temp['nama_organisasi'];?></td>
							<td class='center'><?php echo $temp['inisial'];?></td>
							<td class='right'><?php echo $temp['sekretariat'];?></td>
							<td class='center'>
								<a href="<?php echo site_url('pps/organisasi_siswa/edit/'.$temp['id_siswa_organisasi']);?>"class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a href="<?php echo site_url('pps/organisasi_siswa/delete/'.$temp['id_siswa_organisasi']);?>"class="button small grey tooltip" data-gravity="s" onClick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Delete</a>
							</td>
						</tr>
						<?php
							}
							}
						?>
					</tbody>
				</table>
			</div>
		</div><!-- End of .content -->
		
	</div><!-- End of .box -->		
 </div>
