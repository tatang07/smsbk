<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
   	<div class="box">
			
		<div class="header">
			<h2>Organisasi Siswa</h2>
		</div>
		<?php //print_r($kepsek);?>
			<div class="dataTables_wrapper"  role="grid">
				<table class="styled" >
					<thead>
						<tr colspan='2'>
						</tr>
						<tr>
							<td width='20px' class='center'>No.</td>
							<td width='' class='center'>Organisasi</td>
							<td width='100px' class='center'>Inisial</td>
							<td width='' class='center'>Sekeretariat</td>
						</tr>
					</thead>
					<tbody>
						<?php 
							$no=1;
							foreach($organisasi as $temp){
						?>
						<tr>
							<td class='center'>
								<?php
									echo $no;
									$no++;
								?>
							</td>
							<td class='right'><?php echo $temp['nama_organisasi'];?></td>
							<td class='center'><?php echo $temp['inisial'];?></td>
							<td class='right'><?php echo $temp['sekretariat'];?></td>
						</tr>
						<?php
							}
						?>
					</tbody>
				</table>
			</div>
		
	</div><!-- End of .box -->		
 </div>
