<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Edit Organisasi Siswa</h2>
       </div>
		
		<form action="<?php echo base_url('pps/organisasi_siswa/proses_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>
					<div class="row">
						<label for="f2_normal_input">
							<strong>Nama Organisasi</strong>
						</label>
						<div>
							<input type="text" name="nama_organisasi" value="<?php echo $detail['nama_organisasi'];?>" />
							<input type="hidden" name="id_siswa_organisasi" value="<?php echo $detail['id_siswa_organisasi'];?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Inisial</strong>
						</label>
						<div>
							<input type="text" name="inisial" value="<?php echo $detail['inisial'];?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Sekretariat</strong>
						</label>
						<div>
							<input type="text" name="sekretariat" value="<?php echo $detail['sekretariat'];?>" />
						</div>
					</div>
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('pps/organisasi_siswa'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>
