	<h1 class="grid_12">Kurikulum</h1>
			
			<form action="<?php echo base_url('pps/catatanbp/submit_kontrak'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Kontrak Guru BP/BK</legend>
					
					<div class="row">
						<label for="f1_select">
							<strong>Guru BP/BK</strong>
						</label>
						<div>
							<select name="id_guru" data-placeholder="-Pilih Guru-">
								<option value="0">-Pilih Guru-</option>
							<?php foreach($daftar_guru as $r): ?>
									<option value="<?php echo $r['id_guru']; ?>"><?php echo $r['nama']; ?></option>
							<?php endforeach; ?>
							</select>
						</div>
					</div>

					<div class="row">
						<label for="f1_select">
							<strong>Kelas</strong>
						</label>
						<div>
							<?php foreach($daftar_rombel as $r): ?>
									<input value="<?php echo $r['id_rombel']; ?>" type="checkbox" name = "kontrak[]"><?php echo $r['rombel']; ?>
									<?php echo "<br/>"; ?>
							<?php endforeach; ?>
						</div>
					</div>
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pps/catatanbp/datalist/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		