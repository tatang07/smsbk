<?php  
	$statuspil = $statuspil;
	if(isset($_POST['id_tunjangan'])){
	$a = $_POST['id_tunjangan'];
	$statuspil = $a; } ?>

<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Catatan BP/BK</h2>
       </div>
		<div class="tabletools">
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Nama</span></th>
							<td width=200px align='left'>
								<select name="id_tunjangan" id="id_tunjangan" onchange="submitform();">
									<option value="0">-Nama Siswa-</option>
									<?php foreach($data_siswa as $da): ?>
										
										<?php if($da['id_siswa'] == $statuspil){ ?>
											<option selected value='<?php echo $da['id_siswa']; ?>' data-status-pilihan="<?php echo $da['id_siswa']; ?>"><?php echo $da['nama']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $da['id_siswa']; ?>' data-status-pilihan="<?php echo $da['id_siswa']; ?>"><?php echo $da['nama']; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
		
		
			<div class="right">
				<?php 
				$gid=get_usergroup();
				$id_tunjangan=$this->uri->segment(4);
				
				if($gid!=9){?>
			  	<a href="<?php echo base_url('pps/catatanbp/load_add_detail/'.$statuspil); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php }elseif($gid==9){?>
				
				<?php }?>
			  <br><br>
            </div>
        </div>
			
		
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<!-- <th>NIS</th>
								<th>Nama</th> -->
								<th>Tanggal Dicatat</th>
								<th>Keterangan</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php
								$this->load->model('m_catatanbp');
								$data['data_catatan']=$this->m_catatanbp->get_catatan_by($statuspil);
							?>
							<?php $no=1; foreach($data['data_catatan'] as $dt):?>
							<tr>
								<td width='20' class='center'><?php echo $no;?></td>
								<!-- <td width='60' class='center'><?php echo $dt['nis'];?></td>								
								<td width='200' class='right'><?php echo $dt['nama'];?></td> -->								
								<td width='100' class='center'><?php echo $dt['tanggal_catatan'];?></td>
								<td width='' class='right'><?php echo $dt['keterangan'];?></td>
								<td width='80' class='center'>
								<a original-title="" href="<?php echo base_url("pps/catatanbp/delete_catatan_detail/$dt[id_siswa_catatan_bk]/$statuspil"); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"> <i class="icon-remove" ></i> Delete</a>
								
								
								
								</td>
							</tr>
							<?php $no++; endforeach ?>
						</tbody>
					</table>
						
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>