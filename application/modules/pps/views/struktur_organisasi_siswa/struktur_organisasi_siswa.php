<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
   	<div class="box">
			
		<div class="header">
			<h2>Struktur Organisasi Siswa</h2>
		</div>
		<?php //print_r($kepsek);?>
		<div class="tabletools">
			<div class="right">
				<?php if($id){ ?>
					<a href="<?php echo base_url('pps/struktur_organisasi_siswa/add/?id='.$id); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php }else{ $id=0; ?>
					<a href="<?php echo base_url('pps/organisasi_siswa'); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php } ?>
			  <br/><br/>
			</div>
			<div class="dataTables_filter">
				<form action="<?php echo base_url('pps/struktur_organisasi_siswa/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=110px align='left'><span class="text">Nama Organisasi &nbsp:</span></th>
							<td width=200px align='left'>
								<select name="id_siswa_organisasi">
									<?php if($organisasi){ ?>
									<?php foreach($organisasi as $o): ?>
										<?php if($o['id_siswa_organisasi'] == $id ){ ?>
											<option selected value='<?php echo $o['id_siswa_organisasi']; ?>'><?php echo $o['nama_organisasi']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $o['id_siswa_organisasi']; ?>'><?php echo $o['nama_organisasi']; ?></option>
										<?php } ?>
									<?php endforeach; } ?>
								</select>
							</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
							<input type="submit" name=send class="button grey tooltip" value="cari" />
							<!-- <a href="search" class="button grey tooltip"><i class='icon-search'></i></a> -->
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
			<div class="dataTables_wrapper"  role="grid">
				<table class="styled" >
					<thead>
						<tr colspan='2'>
						</tr>
						<tr>
							<td width='20px' class='center'>No.</td>
							<td width='' class='center'>Jabatan</td>
							<td width='120px' class='center'>Aksi</td>
						</tr>
					</thead>
					<tbody>
						<?php 
							if($struktur){
							$no=1;
							foreach($struktur as $temp){
						?>
						<tr>
							<td class='center'>
								<?php
									echo $no;
									$no++;
								?>
							</td>
							<td class='right'><?php echo $temp['jabatan'];?></td>
							<td class='center'>
								<a href="<?php echo site_url('pps/struktur_organisasi_siswa/edit/'.$temp['id_siswa_organisasi_struktur']);?>"class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
								<a href="<?php echo site_url('pps/struktur_organisasi_siswa/delete/'.$temp['id_siswa_organisasi_struktur']);?>"class="button small grey tooltip" data-gravity="s" onClick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Delete</a>
							</td>
						</tr>
						<?php
							}
							}else{
								echo "<tr><td colspan='3'>Belum ada struktur!</td></tr>";
							}
						?>
					</tbody>
				</table>
			</div>
		</div><!-- End of .content -->
		
	</div><!-- End of .box -->		
 </div>
