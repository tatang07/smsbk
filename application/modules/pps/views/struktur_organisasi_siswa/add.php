<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Tambah Struktur Organisasi Siswa</h2>
       </div>
		
		<form action="<?php echo base_url('pps/struktur_organisasi_siswa/proses_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
			<fieldset>
					<div class="row">
						<label for="f2_normal_input">
							<strong>Jabatan</strong>
						</label>
						<div>
							<input type="text" name="jabatan" value="" />
							<input type="hidden" name="id_siswa_organisasi" value="<?php echo $id;?>" />
						</div>
					</div>
			<fieldset>
			<div class="actions">
				<div class="left">
					<input type="submit" value="Simpan" name=send />
					<a href="<?php echo base_url('pps/struktur_organisasi_siswa'); ?>"> <input value="Batal" type="button"></a>
				</div>
			</div>
		</form>
    </div>
 </div>
