<?php
class m_home extends MY_Model {
    public function __construct(){
			parent::__construct();
		}
		
		public function get_data_sekolah(){
			$id = get_id_sekolah();
			$query = $this->db->query("SELECT * FROM m_sekolah WHERE id_sekolah='$id'");
			return $query->result_array();
		}
		
		function edit($data,$id=0){
			$this->db->where('id_sekolah',$id);
			$this->db->update('m_sekolah',$data);
		}
		
		public function get_all_siswa(){
			$id = get_id_sekolah();
			$id_tahun = get_id_tahun_ajaran();
			$query = $this->db->query("SELECT count(id_siswa) as jumlah FROM v_siswa WHERE id_sekolah='$id' and id_tahun_ajaran='$id_tahun'");
			return $query->row_array();
		}
		
		public function get_all_rombel(){
			$id = get_id_sekolah();
			$id_tahun = get_id_tahun_ajaran();
			$query = $this->db->query("SELECT count(id_rombel) as jumlah FROM m_rombel WHERE id_sekolah='$id' and id_tahun_ajaran='$id_tahun'");
			return $query->row_array();
		}
		
		public function get_all_ptk(){
			$id = get_id_sekolah();
			$query = $this->db->query("SELECT count(id_guru) as jumlah FROM m_guru WHERE id_sekolah='$id'");
			return $query->row_array();
		}
}