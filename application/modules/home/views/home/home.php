
<div class="grid_12">
	<div class="grid_1">
		<?php foreach($sekolah as $sekolah):?>
		<img width="60px" height="60px" src='<?php echo base_url('extras/sekolah/'.$sekolah['logo'])?>' />
	</div><p class="grid_10">
		<font size="4px"><b>Sistem Manajemen Sekolah Berbasis Keunggulan<br/><?php echo $sekolah['sekolah'];?></b></font>
		<br/>&nbsp;
	</p>
	<div class="grid_1">
		<img width="60px" src='<?php echo base_url('extras/sekolah/jabar.png')?>' />
	</div>

	<div class="grid_12" style="padding-bottom:20px">
	<img width="100%" src='<?php echo base_url('extras/sekolah/line.png')?>' />
	</div>
	<div class="grid4_5 ">
		<div class="radius-border">
		<img width="250px" src='<?php echo base_url('extras/sekolah/'.$sekolah['foto'])?>' />
		</div>
	</div>
	<div class="grid_8">
	   <div class="box with-table">
       <div class="header">
            <h2>PROFIL <?php echo $sekolah['sekolah'];?> </h2>
			<div class="tabletools">
				<div class="right">
				<?php if(get_usergroup()<=3){ ?>
				<a href="<?php echo site_url('home/home/load_edit/'.get_id_sekolah())?>"><i class="icon-plus"></i>Edit</a> 
				<?php } ?>
				</div>
			</div>
			
       </div>
		<div class="content">
			<div class="dataTables_wrapper"  role="grid">   
			<div style="padding-left:10px">
			<table>
				<tr>
					<td>
					<font size="2px"><b>Nama Sekolah</b></font>
					</td>
					<td width="10px">
					:
					</td>
					<td>
					<font size="2px"><b><?php echo $sekolah['sekolah']?></b></font>
					</td>
				</tr>
				<tr>
					<td>
					<font size="2px"><b>NPSN</b></font>
					</td>
					<td width="10px">
					:
					</td>
					<td>
					<font size="2px"><b><?php echo $sekolah['nps']?></b></font>
					</td>
				</tr>
				<tr>
					<td>
					<font size="2px"><b>Alamat</b></font>
					</td>
					<td>
					:
					</td>
					<td>
					<font size="2px"><b><?php echo $sekolah['alamat']?></b></font>
					</td>
				</tr>
				<tr>
					<td>
					<font size="2px"><b>No Telp</b></font>
					</td>
					<td>
					:
					</td>
					<td>
					<font size="2px"><b><?php echo $sekolah['telepon']?></b></font>
					</td>
				</tr>
				<tr>
					<td>
					<font size="2px"><b>Email</b></font>
					</td>
					<td>
					:
					</td>
					<td>
					<font size="2px"><b><?php echo $sekolah['email']?></b></font>
					</td>
				</tr>
				<tr>
					<td>
					<font size="2px"><b>Website</b></font>
					</td>
					<td width="10px">
					:
					</td>
					<td>
					<font size="2px"><b><?php echo $sekolah['website']?></b></font>
					</td>
				</tr>
				
			</table>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
			<br/>
				
			</div>
			</div>
		</div>
		</div>
	</div>
	<div class="grid_12">
				<h1 class="grid_12 margin-top no-margin-top-phone">Statistik</h1>
				<div class="grid_12 center-elements">
					<div class="full-stats">
						<div class="stat hlist" data-list='[{"val":<?php echo $siswa['jumlah']; ?>,"format":"0,0;-0,0","title":"Total Siswa","color":"green"},
						{"val":<?php echo $rombel['jumlah']; ?>,"format":"0,0;-0,0","title":"Total Rombongan Belajar","color":"green"},
						{"val":<?php echo $ptk['jumlah']; ?>,"format":"0,0;-0,0","title":"Total Pendidik dan Tenaga Kependidikan","color":"green"}]', 
						data-flexiwidth=true></div>
					</div>
				</div>
	</div>


<?php endforeach?>