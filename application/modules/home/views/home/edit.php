	<h1 class="grid_12">Home</h1>

			<form action="<?php echo base_url('home/home/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Sekolah</legend>
		
				<?php foreach($sekolah as $t){?>
				<?php //echo $t['program_kerja']; ?>

					<div class="row">
						<label for="f1_normal_input">
							<strong>NSS</strong>
						</label>
						<div>
							<input type="text" name="nss" value="<?php echo $t['nss']?>" />
							<input type="hidden" name="id" value="<?php echo $t['id_sekolah']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>NPSN</strong>
						</label>
						<div>
							<input type="text" name="nps" value="<?php echo $t['nps']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Sekolah</strong>
						</label>
						<div>
							<input type="text" name="sekolah" value="<?php echo $t['sekolah']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Alamat</strong>
						</label>
						<div>
							<input type="text" name="alamat" value="<?php echo $t['alamat']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kode Pos</strong>
						</label>
						<div>
							<input type="text" name="kode_pos" value="<?php echo $t['kode_pos']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Telepon</strong>
						</label>
						<div>
							<input type="text" name="telepon" value="<?php echo $t['telepon']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Fax</strong>
						</label>
						<div>
							<input type="text" name="fax" value="<?php echo $t['fax']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>SK Pendirikan</strong>
						</label>
						<div>
							<input type="text" name="sk_pendirian" value="<?php echo $t['sk_pendirian']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal SK Pendirikan</strong>
						</label>
						<div>
							<input type="date" name="tgl_sk_pendirian" value="<?php echo $t['tgl_sk_pendirian']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>No SK Akreditasi</strong>
						</label>
						<div>
							<input type="text" name="no_sk_akreditasi" value="<?php echo $t['no_sk_akreditasi']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Akreditasi</strong>
						</label>
						<div>
							<input type="date" name="tgl_akreditasi" value="<?php echo $t['tgl_akreditasi']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Email</strong>
						</label>
						<div>
							<input type="text" name="email" value="<?php echo $t['email']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Website</strong>
						</label>
						<div>
							<input type="text" name="website" value="<?php echo $t['website']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Luas Halaman</strong>
						</label>
						<div>
							<input type="text" name="luas_halaman" value="<?php echo $t['luas_halaman']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Luas Tanah</strong>
						</label>
						<div>
							<input type="text" name="luas_tanah" value="<?php echo $t['luas_tanah']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Luas Bangunan</strong>
						</label>
						<div>
							<input type="text" name="luas_bangunan" value="<?php echo $t['luas_bangunan']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Luas Lapangan Olahraga</strong>
						</label>
						<div>
							<input type="text" name="luas_olahraga" value="<?php echo $t['luas_olahraga']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Logo</strong>
						</label>
						<div>
							<input type="file" name="logo" value="<?php echo $t['logo']?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Foto</strong>
						</label>
						<div>
							<input type="file" name="foto" value="<?php echo $t['foto']?>" />
						</div>
					</div>
				<?php } ?>
				</fieldset>
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('home/home/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
