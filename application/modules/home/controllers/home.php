<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class home extends Simple_Controller {
    	 public function __construct(){
			parent::__construct();
			// untuk load model
			$this->load->model('m_home');
			// $this->load->helper('download');
			// $this->load->library('form_validation');
		}
		
		public function index(){
			$data['sekolah'] = $this->m_home->get_data_sekolah();
			$data['component'] = 'home';
			$data['siswa'] = $this->m_home->get_all_siswa();
			$data['rombel'] = $this->m_home->get_all_rombel();
			$data['ptk'] = $this->m_home->get_all_ptk();
			render("home/home", $data);
		}
		public function load_edit(){
			$data['sekolah'] = $this->m_home->get_data_sekolah();
			render("home/edit", $data);
		}
		public function submit_edit(){
			$id=$this->input->post('id');
			$data['nss'] = $this->input->post('nss');
			$data['nps'] = $this->input->post('nps');
		
			$data['sekolah'] = $this->input->post('sekolah');
			$data['alamat'] = $this->input->post('alamat');
			$data['kode_pos'] = $this->input->post('kodepos');
			$data['telepon'] = $this->input->post('telepon');
			$data['fax'] = $this->input->post('fax');
			$data['sk_pendirian'] = $this->input->post('sk_pendirian');
			$data['tgl_sk_pendirian'] = $this->input->post('tgl_sk_pendirian');
			$data['no_sk_akreditasi'] = $this->input->post('no_sk_akreditasi');
			$data['tgl_akreditasi'] = $this->input->post('tgl_akreditasi');
			$data['email'] = $this->input->post('email');
			$data['website'] = $this->input->post('website');
			$data['luas_halaman'] = $this->input->post('luas_halaman');
			$data['luas_tanah'] = $this->input->post('luas_tanah');
			$data['luas_bangunan'] = $this->input->post('luas_bangunan');
			$data['luas_olahraga'] = $this->input->post('luas_olahraga');
			// $data['logo'] = $this->input->post('logo');
			// $data['foto'] = $this->input->post('foto');
			
			 $config['upload_path'] = './extras/sekolah/';
			 $config['allowed_types'] = 'gif|jpg|png';
			 $config['max_size'] = '10000';
			 $config['max_width'] = '3000';
			 $config['max_height'] = '3000';
			$this->load->library('upload', $config);
 
			 if (!$this->upload->do_upload('logo')){
			 	$this->load->helper('form');
				$error = array('error'=>$this->upload->display_errors());
				// $this->load->view('home/form_upload', $error);
				// print_r($this->upload->data());
			 }
			  else {
				$this->load->helper('form');
				$upload_data = $this->upload->data();
				$data['logo']=$upload_data['file_name'];
				echo $data['logo'];
				
				// $data = array('upload_data' => $upload_data);
				// $this->load->view('uploadsample/view_upload_success', $data);
			 }
			 
			 if (!$this->upload->do_upload('foto')){
			 	$this->load->helper('form');
				$error = array('error'=>$this->upload->display_errors());
				// $this->load->view('home/form_upload', $error);
				// print_r($this->upload->data());
			 }
			 else {
				$this->load->helper('form');
				 $upload_data = $this->upload->data();
				 $data['foto']=$upload_data['file_name'];
				 echo $data['foto'];
			
				// $upload_data['judul'] = $judul;
				 // $data = array('upload_data' => $upload_data);
				 // $this->load->view('uploadsample/view_upload_success', $data);
			 }
			
			$this->m_home->edit($data,$id);
			
			redirect(site_url('home/home/index/')) ;
		}
}
