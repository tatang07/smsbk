<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class kg_gaji_guru extends Simple_Controller {
	
	public function index()
	{	
		
		$this->load->model('ptk/m_kg_gaji_guru');
		$id_sekolah = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		$gid=get_usergroup();
		if($gid!=9){
		$data['isi'] = $this->m_kg_gaji_guru->get_data($id_guru);
		}elseif($gid==9){
		$id_guru=get_id_personal();
		//echo $id_guru;
		$data['isi'] = $this->m_kg_gaji_guru->get_data_guru($id_guru);
		}
		//echo $gid;
		
		
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('kg_gaji_guru/kg_gaji_guru',$data);
	}
	
	

}
