<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class penugasan_dan_penentuan_beban_mengajar extends CI_Controller {

	public function index()
	{	
		$this->load->model('ptk/m_penugasan_dan_penentuan_beban_mengajar');
		$id = get_id_sekolah();
		$data['data'] = $this->m_penugasan_dan_penentuan_beban_mengajar->select_data($id)->result_array();
		echo '<pre>';
		print_r($data['data']);
		echo '</pre>';
		exit();
		$data['component'] = "pendidik_dan_tenaga_kependidikan";
		render('penugasan_dan_penentuan_beban_mengajar/penugasan_dan_penentuan_beban_mengajar',$data);
	}
	
	public function search(){
		$this->load->model('ptk/m_penugasan_dan_penentuan_beban_mengajar');
		$nama = $this->input->post('nama');
		
		$id = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		
		$data['data'] = $this->m_penugasan_dan_penentuan_beban_mengajar->get_data_search($id,$nama);
		if($nama==NULL){
			$data['data'] = $this->m_penugasan_dan_penentuan_beban_mengajar->select_data($id)->result_array();
		}
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('penugasan_dan_penentuan_beban_mengajar/penugasan_dan_penentuan_beban_mengajar',$data);

	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */