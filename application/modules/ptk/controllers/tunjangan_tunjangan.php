<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class tunjangan_tunjangan extends Simple_Controller {
    	
		public function index(){
			$this->load->model('m_tunjangan');
			$id=get_id_sekolah();
			$data['data_tunjangan']=$this->m_tunjangan->select_data_tunjangan($id)->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render("tunjangan_tunjangan/tunjangan_tunjangan",$data);
		}
		
		public function load_add(){
			render("tunjangan_tunjangan/form_tambah");
		
		}
		
		public function load_edit($id){
			$this->load->model('m_tunjangan');
			$data['data'] = $this->m_tunjangan->select_data_edit($id)->result_array();
			render("tunjangan_tunjangan/form_edit",$data);
		
		}
		
		public function load_add_detail($id_tunjangan){
			$this->load->model('m_tunjangan');
			$id=get_id_sekolah();
		
			$data['guru'] = $this->m_tunjangan->select_guru($id)->result_array();
			$data['data_tunjangan2']=$this->m_tunjangan->select_data_tunjangan2($id)->result_array();
			//print_r($data['data_tunjangan2']);
			render("tunjangan_tunjangan/form_tambah_detail",$data);
		
		}
		
		public function load_detail($id){
			$this->load->model('m_tunjangan');
			$id_sekolah=get_id_sekolah();
			$data['data_tunjangan2']=$this->m_tunjangan->select_data_tunjangan2($id_sekolah)->result();
			$data['statuspil'] = $id;
			$data['data_tunjangan']=$this->m_tunjangan->select_data_tunjangan_detail($id)->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render("tunjangan_tunjangan/tunjangan_detail",$data);	
		}
		
		public function submit_add(){
			$this->load->model('m_tunjangan');
		
			$data['id_sekolah'] = get_id_sekolah();
			$data['nama_tunjangan'] = $this->input->post('jenis_tunjangan');
			$data['besaran_uang'] = $this->input->post('besaran');
			$data['besaran_prosentase'] = $this->input->post('gaji_pokok');


			$this->m_tunjangan->add($data);
			redirect(site_url('ptk/tunjangan_tunjangan/index/')) ;
		}
		
		public function submit_edit(){
			$this->load->model('m_tunjangan');
		
			$id = $this->input->post('id_tunjangan');
			$data['nama_tunjangan'] = $this->input->post('jenis_tunjangan');
			$data['besaran_uang'] = $this->input->post('besaran');
			$data['besaran_prosentase'] = $this->input->post('gaji_pokok');


			$this->m_tunjangan->update_detail($id,$data);
			redirect(site_url('ptk/tunjangan_tunjangan/index/')) ;
		}
		
		public function submit_add_detail(){
			$this->load->model('m_tunjangan');
			// print_r($_POST);
			$data['id_guru'] = $this->input->post('id_guru');
			
			$data['id_tunjangan'] = $this->input->post('id_tunjangan');
			$data['jumlah'] = $this->input->post('jumlah');
			if($data['id_tunjangan']==0){
				set_error_message('Pilih Jenis Tunjangan');
				redirect(site_url('ptk/tunjangan_tunjangan/load_add_detail/'.$data['id_tunjangan'])) ;
				
			}
			
			
			
			if($data['jumlah']==NULL){
				$data['jumlah']=1;
				//echo $data['jumlah'];
			}
			
			$this->m_tunjangan->add_detail($data);
		
			redirect(site_url('ptk/tunjangan_tunjangan/load_detail/'.$data['id_tunjangan'])) ;
		}
		
		public function delete_tunjangan_detail($id,$id_tunjangan){
			$this->load->model('m_tunjangan');
			$this->m_tunjangan->delete_detail($id);
			redirect(site_url('ptk/tunjangan_tunjangan/load_detail/'.$id_tunjangan)) ;
		}
		
		public function delete($id_tunjangan){
			$this->load->model('m_tunjangan');
			$this->m_tunjangan->delete($id_tunjangan);
			$this->m_tunjangan->delete_detail2($id_tunjangan);
			redirect(site_url('ptk/tunjangan_tunjangan/index/')) ;
		}
}
