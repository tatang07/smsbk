<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class kualifikasi_pangkat extends CI_Controller {

	public function index()
	{	
		$this->load->model('ptk/m_kualifikasi_pangkat');
		$id = get_id_sekolah();
		$data['data'] = $this->m_kualifikasi_pangkat->select_golongan($id)->result();
		$data['component'] = "pendidik_dan_tenaga_kependidikan";
		render('kualifikasi_pangkat/kualifikasi_pangkat',$data);
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */