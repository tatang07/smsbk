<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class asuransi extends Simple_Controller {
    	
		public function index(){

			$this->load->model('m_asuransi');
			$id=get_id_Sekolah();
			$data['data_asuransi']=$this->m_asuransi->select_data_asuransi($id)->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render("asuransi/asuransi",$data);
		}
		public function load_detail($id_asuransi)
		{	
			$this->load->model('m_asuransi');
			$id_sekolah=get_id_sekolah();
			$data['data_asuransi']=$this->m_asuransi->select_data_asuransi($id_sekolah)->result();
			// echo $id_sekolah;
			
			$data['isi'] = $this->m_asuransi->get_data_detail($id_asuransi,$id_sekolah)->result_array();
			$data['statuspil'] = $id_asuransi;
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render('asuransi/asuransi_detail',$data);
		}
		
		public function load_add_detail($id_asuransi){
			$this->load->model('m_asuransi');
			$id=get_id_sekolah();
			$data['guru'] = $this->m_asuransi->select_guru($id)->result_array();
			$data['data_asuransi']=$this->m_asuransi->select_data_asuransi($id)->result();
			$data['id_asuransi']=$id_asuransi;
			render("asuransi/form_tambah_detail",$data);
		
		}
		
		public function submit_add_detail(){
			$this->load->model('m_asuransi');
			// print_r($_POST);
			$data['id_guru'] = $this->input->post('id_guru');
			$data['id_asuransi'] = $this->input->post('id_asuransi');
			
			$id=$data['id_asuransi'];
			// echo $id;
			// exit();
			$this->m_asuransi->add_detail($data);
		
			redirect("ptk/asuransi/load_detail/$id") ;
		}
		
		public function delete($id_asuransi_detail,$id){
			$this->load->model('ptk/m_asuransi');
			$this->m_asuransi->delete($id_asuransi_detail);
			redirect(site_url('ptk/asuransi/load_detail/'.$id)) ;
		}
		
		public function delete2($id){
			$this->load->model('ptk/m_asuransi');
			$this->m_asuransi->delete_utama($id);
			redirect(site_url('ptk/asuransi/index/')) ;
		}
		
		public function load_add(){
			render("asuransi/form_tambah");
		
		}
		
		public function submit_add(){
			$this->load->model('m_asuransi');
		
			$data['id_sekolah'] = get_id_sekolah();
			$data['asuransi'] = $this->input->post('jenis_asuransi');
	


			$this->m_asuransi->add($data);
			redirect(site_url('ptk/asuransi/index/')) ;
		}
}
