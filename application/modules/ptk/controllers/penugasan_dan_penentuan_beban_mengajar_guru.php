<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class penugasan_dan_penentuan_beban_mengajar_guru extends CI_Controller {

	public function index()
	{	
		$this->load->model('ptk/m_penugasan_dan_penentuan_beban_mengajar_guru');
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();
		$id_guru = get_id_personal();
		$data['data'] = $this->m_penugasan_dan_penentuan_beban_mengajar_guru->select_data($id_sekolah,$id_tahun_ajaran,$id_guru)->result_array();
		$data['component'] = "pendidik_dan_tenaga_kependidikan";
		render('penugasan_dan_penentuan_beban_mengajar_guru/penugasan_dan_penentuan_beban_mengajar_guru',$data);
	}
	
	public function search(){
		$this->load->model('ptk/m_penugasan_dan_penentuan_beban_mengajar_guru');
		$nama = $this->input->post('nama');
		
		$id = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		
		$data['data'] = $this->m_penugasan_dan_penentuan_beban_mengajar_guru->get_data_search($id,$nama);
		if($nama==NULL){
			$data['data'] = $this->m_penugasan_dan_penentuan_beban_mengajar_guru->select_data($id)->result_array();
		}
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('penugasan_dan_penentuan_beban_mengajar_guru/penugasan_dan_penentuan_beban_mengajar_guru',$data);

	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */