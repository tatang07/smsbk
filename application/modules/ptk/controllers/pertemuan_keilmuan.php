<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class pertemuan_keilmuan extends CI_Controller {

	public function index()
	{	
		$this->load->model('ptk/m_pertemuan_keilmuan');
		$id_sekolah = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		$gid=get_usergroup();
		if($gid!=9){
		$data['isi'] = $this->m_pertemuan_keilmuan->get_data($id_sekolah);
		}elseif($gid==9){
		$id_guru=get_id_personal();
		$data['isi'] = $this->m_pertemuan_keilmuan->get_data_guru($id_sekolah,$id_guru);	
		}
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('pertemuan_keilmuan/pertemuan_keilmuan',$data);
	}
	public function load_add()
	{	
		$this->load->model('ptk/m_pertemuan_keilmuan');
		$id_sekolah= get_id_sekolah();
		// $jenjang_sekolah = get_jenjang_sekolah();
		$data['guru'] = $this->m_pertemuan_keilmuan->select_guru($id_sekolah);
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('pertemuan_keilmuan/form_tambah',$data);
	}

	public function load_edit($id_guru_pertemuan_keilmuan)
	{	
		$this->load->model('ptk/m_pertemuan_keilmuan');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_pertemuan_keilmuan->get_data_edit($id_guru_pertemuan_keilmuan);
	
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('pertemuan_keilmuan/form_edit',$data);
	}

	public function delete($id_guru_pertemuan_keilmuan){
			$this->load->model('ptk/m_pertemuan_keilmuan');
			$this->m_pertemuan_keilmuan->delete($id_guru_pertemuan_keilmuan);
			redirect(site_url('ptk/pertemuan_keilmuan/index/')) ;
	}

	public function submit_add(){
			$this->load->model('ptk/m_pertemuan_keilmuan');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['kegiatan'] = $this->input->post('kegiatan');
			$data['tempat'] = $this->input->post('tempat');
			$data['tanggal_pelaksanaan'] = tanggal_db($this->input->post('tanggal_pelaksanaan'));
		
		
			$this->m_pertemuan_keilmuan->add($data);
			redirect(site_url('ptk/pertemuan_keilmuan/index/')) ;
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('ptk/m_pertemuan_keilmuan');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_pertemuan_keilmuan->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_pertemuan_keilmuan->select_kelas($jenjang_sekolah);
			render('pertemuan_keilmuan/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('ptk/m_pertemuan_keilmuan');
			$id=$this->input->post('id');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['kegiatan'] = $this->input->post('kegiatan');
			$data['tempat'] = $this->input->post('tempat');
			$data['tanggal_pelaksanaan'] = tanggal_db($this->input->post('tanggal_pelaksanaan'));
			// echo $this->input->post('id_guru');
			// echo $data['id_guru'];
		// print_r ($data);


			$this->m_pertemuan_keilmuan->edit($data,$id);
			redirect(site_url('ptk/pertemuan_keilmuan/index/')) ;
	}
	public function search(){
		$this->load->model('ptk/m_pertemuan_keilmuan');
		$nama = $this->input->post('nama');
		// $status = $this->input->post('status');
		
		$id_sekolah = get_id_sekolah();

		$data['isi'] = $this->m_pertemuan_keilmuan->get_data_search($nama,$id_sekolah);
	
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('pertemuan_keilmuan/pertemuan_keilmuan',$data);
	}
	
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */