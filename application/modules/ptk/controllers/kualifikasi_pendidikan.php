<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class kualifikasi_pendidikan extends CI_Controller {

	public function index()
	{	
		$this->load->model('ptk/m_kualifikasi_pendidikan');
		$id = get_id_sekolah();
		$data['data'] = $this->m_kualifikasi_pendidikan->select_golongan($id,1)->result();
		$data['component'] = "pendidik_dan_tenaga_kependidikan";
		$data['title']="Pendidik";
		render('kualifikasi_pendidikan/kualifikasi_pendidikan',$data);
	}
	
	public function pendidik(){
		$this->index();
	}
	
	public function tenaga_kependidikan(){
		$this->load->model('ptk/m_kualifikasi_pendidikan');
		$id = get_id_sekolah();
		$data['data'] = $this->m_kualifikasi_pendidikan->select_golongan($id,2)->result();
		$data['component'] = "pendidik_dan_tenaga_kependidikan";
		$data['title']="Tenaga Kependidikan";
		render('kualifikasi_pendidikan/kualifikasi_pendidikan',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */