<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class Supervisi extends Simple_Controller {

	public $myconf = array(
		//Nama controller
		"name" 			=> "ptk/supervisi",
		"component"		=> "pendidik_dan_tenaga_kependidikan",
		//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
		//dan subtitile general ini akan ter-replace
		"title"			=> "Supervisi/Penilaian Kinerja",
		"subtitle"		=> "",
		//"view_file_index"=> "simple/index",
		//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
		//tidak ditentukan, maka akan digunakan model ini
		//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
		//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
		"model_name"	=> "m_supervisi",
		//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
		//Format: nama_field=>"Labelnya"
		"data_label"	=> array(		
							"indeks"=>"Indeks Prestasi",
							"waktu"=>"Waktu",
							"nama"=>"Nama Guru",
							"pelajaran"=>"Mata Pelajaran",
							"materi"=>"Materi",
							"kelas"=>"Kelas",
							"semester"=>"Term"
						),
		//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
		//Tipe default jika tidak didefinisikan: String, 
		//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
		//Format: nama_field => tipe_data_nya
		// "data_type"		=> array(
		// 					"id_kondisi"=>array("r_kondisi","id_kondisi","kondisi"),
		// 					"id_jenis_sarana_prasarana"=>"hidden",
		// 					"id_sekolah"=>"hidden"
		// 				),
		//Field-field yang harus diisi
		// "data_mandatory"=> array(
		// 					"kode_sarana_prasarana","nama_sarana_prasarana","penanggung_jawab","id_kondisi","daya_tampung"
		// 				),
		//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
		// "data_unique"	=> array(
		// 					"id_sarana_prasarana","kode_sarana_prasarana"
		// 				),
		//Konfigurasi tampilan datalist
		"data_list"		=> array(
							//"title"			=> "simple",
							//"subtitle"		=> "simple",
							//"model_name"			=> "",
							//"table"					=> array(),
							//"view_file"				=> "simple/datalist",
							//"view_file_datatable"	=> "simple/datatable",
							//"view_file_datatable_action"	=> "simple/datatable_action",
							"field_list"			=> array("indeks", "waktu", "nama", "pelajaran", "materi", "kelas", "semester", ),
							"field_sort"			=> array("indeks", "waktu", "nama", "pelajaran", "materi", "kelas", "semester", ),
							"field_filter"			=> array("nama", "semester"),
							"field_filter_hidden"	=> array(),
							"field_filter_dropdown"	=> array(),
							"field_comparator"		=> array("id_sekolah"=>"="),
							"field_operator"		=> array("id_sekolah"=>"AND"),
							//"field_align"			=> array(),
							"field_size"			=> array('action_col' => "20%"),
							//"field_separated"		=> array(),
							//"field_sum"				=> array(),
							//"field_summary"			=> array(),
							//"enable_action"			=> TRUE,
							"custom_action_link"	=> array("Detail" => "ptk/supervisi/detail"),
							"custom_add_link"		=> array('label'=>'Tambah','href'=>'ptk/supervisi/tambah' ),
							// "custom_edit_link"		=> array(),
							"custom_delete_link"	=> array('label'=>'Delete','href'=>'ptk/supervisi/hapus'),
							//"custom_link"			=> array("Kurikulum"=>"kurikulum/datalist","Siswa"=>"Siswa/datalist"),
						),
		//Konfigurasi tampilan add				
		"data_add"		=> array(
							//"title"			=> "simple",
							//"subtitle"		=> "simple",
							//"enable"				=> TRUE,
							//"model_name"			=> "",
							//"table"					=> array(),
							//"view_file"				=> "simple/add",
							//"view_file_form_input"	=> "simple/form_input",
							"field_list"			=> array("id_tingkat_kelas","rombel","id_kurikulum","id_guru"),
							//"custom_action"			=> "",
							//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
							//"msg_on_success"		=> "Data berhasil disimpan.",
							//"redirect_link"			=> ""
						),
		//Konfigurasi tampilan edit
		"data_edit"		=> array(
							//"title"			=> "simple",
							//"subtitle"		=> "simple",
							"enable"				=> FALSE,
							// "model_name"			=> "",
							// "table"					=> array(),
							// "view_file"				=> "simple/edit",
							// "view_file_form_input"	=> "simple/form_input",
							// "field_list"			=> array(),
							// "custom_action"			=> "",
							// "msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
							// "msg_on_success"		=> "Data berhasil diperbaharui.",
							// "redirect_link"			=> ""
						),
		//Konfigurasi tampilan delete
		/*"data_delete"	=> array(
							"enable"				=> TRUE,
							"model_name"			=> "",
							"table"					=> array(),
							"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
							"msg_on_success"		=> "Data berhasil dihapus.",
							"redirect_link"			=> ""
						),
		//Konfigurasi tampilan export
		"data_export"	=> array(
							"enable_pdf"			=> TRUE,
							"enable_xls"			=> TRUE,
							"view_pdf"				=> "simple/laporan_datalist",
							"view_xls"				=> "simple/laporan_datalist",
							"field_size"			=> array(),
							"orientation"			=> "p",
							"special_number"		=> "",
							"enable_header"			=> TRUE,
							"enable_footer"			=> TRUE,
						)*/
	);

	function index()
	{
		redirect('ptk/supervisi/datalist');
	}

	public function pre_process($data){
		//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
		//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
		
		
		// $id_sapras = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
		// $list_sarana_prasarana = get_list("r_jenis_sarana_prasarana","id_jenis_sarana_prasarana","jenis_sarana_prasarana");
		// $data['conf']['data_list']['subtitle'] = $list_sarana_prasarana[$id_sapras];
		// $data['conf']['data_add']['subtitle'] = $list_sarana_prasarana[$id_sapras];
		// $data['conf']['data_edit']['subtitle'] = $list_sarana_prasarana[$id_sapras];
		
		// $data['conf']['data_delete']['redirect_link'] = "sarana_prasarana/datalist/$id_sapras";
		// $data['conf']['data_add']['redirect_link'] = "sarana_prasarana/datalist/$id_sapras";
		// $data['conf']['data_edit']['redirect_link'] = "sarana_prasarana/datalist/$id_sapras";
		
		return $data;
	}
	
	public function before_datalist($data){
		//Method ini hanya akan dieksekusi di awal method datalist
		//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
		// $id_sapras = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;

		// $_POST['filter']['id_jenis_sarana_prasarana'] = $id_sapras;
		$_POST['filter']['id_sekolah'] = get_id_sekolah();
		// $_POST['filter']['id_tahun_ajaran'] = get_id_tahun_ajaran();
		// //Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
		// //mengupdate datatable baik ketika search ataupun pindah page
		// $data['conf']['data_list']['field_filter_hidden'] = array(
		// 												'id_tahun_ajaran'=>2, 
		// 												'id_sekolah'=>get_id_sekolah() 
		// 											);
		
		// //Mengganti link add yang ada di kanan atas form datalist
		// $data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'sarana_prasarana/add/'.$id_sapras );			
		
		// //Memanipulasi parameter page
		// $page = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 1;
		// $data['page'] = $page;
		// //Mengkustomisasi link tombol edit dan delete
		// $data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'sarana_prasarana/edit/'.$id_sapras );
		// $data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'sarana_prasarana/delete/'.$id_sapras );
		
		return $data;
	}
	
	public function before_datatable($data){
		//Method ini hanya akan dieksekusi di awal method datatable
		//Mengambil id_pihak yang diposting dari form datalist
		// $filter = $this->input->post("filter");
		$_POST['filter']['id_sekolah'] = get_id_sekolah();
		// $_POST['filter']['id_tahun_ajaran'] = get_id_tahun_ajaran();
		// $id_sapras = $filter['id_jenis_sarana_prasarana'];
		// //Mengkustomisasi link tombol edit dan delete
		// $data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'sarana_prasarana/edit/'.$id_sapras );
		// $data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'sarana_prasarana/delete/'.$id_sapras );
		
		return $data;
	}
	
			
	public function before_render_add($data){
		// $data['conf']['data_type'] = array(
		// 	"id_tingkat_kelas" => array('list' => $this->m_penentuan_kelas->get_tingkat_kelas(false, true)),
		// 	"id_kurikulum" => array('m_kurikulum', 'id_kurikulum', 'nama_kurikulum'),
		// 	"id_guru" => array('list' => $this->m_penentuan_kelas->get_guru(false, true))
		// 	);
		// if(!$this->input->post()){
		// 	$id_sapras = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
		// 	//mengisi nilai untuk id_pihak_komunikasi di form input
		// 	$_POST['item']['id_jenis_sarana_prasarana'] = $id_sapras; //name='item[id_pihak_kom..]'
		// }
		return $data;
	}
	
	public function before_add($data){
		// if($this->input->post()){
		// 	//menambahkan satu field id_sekolah sebelum data disimpan ke db
		// 	$data['item']['id_sekolah'] = get_id_sekolah();
		// }
		// if($this->input->post()){
		// 	$data['item']['id_tahun_ajaran'] = get_id_tahun_ajaran();

		// 	//dummy dulu
		// 	$data['item']['id_program'] = 1;
		// }

		return $data;
	}
			
	public function before_edit($data){
		// if(!$this->input->post()){
		// 	$data['id'] = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 0;
		// }
		return $data;
	}
	
	public function before_delete($data){
		// $data['id'] = $this->uri->rsegment(4);
		return $data;
	}

	function detail($id_supervisi)
	{
		$this->load->model('m_supervisi');
		$data['supervisi'] = $this->m_supervisi->get($id_supervisi);
		$data['detail'] = $this->m_supervisi->get_supervisi_detail($id_supervisi);

		render('supervisi/detail', $data);
	}

	function tambah($id_guru = false)
	{
		$this->load->model('m_supervisi');
		$id_s=get_id_sekolah();
		$data['guru_gmr'] = $this->m_supervisi->get_guru_gmr($id_s);
		$data['butir_supervisi'] = $this->m_supervisi->get_butir_supervisi();
		$data['id_guru'] = $id_guru;

		$data['matpel_rombel'] = array();
		if($id_guru){
			if($matpel_rombel = $this->m_supervisi->get_matpel_rombel($id_guru)){
				foreach ($matpel_rombel as $mr) {
					$data['matpel_rombel'][$mr['id_guru_matpel_rombel']] = $mr['rombel'].' : '.$mr['pelajaran'];
				}
			}
		}

		if($post = $this->input->post()){
			$supervisi = array(
					'id_guru' => $id_guru,
					'waktu' => tanggal_db($post['waktu']),
					'tanggal_terbit' => tanggal_db($post['tanggal_terbit']),
					'kode' => $post['kode_supervisi'],
					'materi' => $post['materi'],
					'hasil' => $post['kesimpulan'],
					'id_term' => get_id_term(),
				);

			foreach ($matpel_rombel as $mr) {
				if($mr['id_guru_matpel_rombel'] == $post['id_gmr']){
					$supervisi['id_pelajaran'] = $mr['id_pelajaran'];
					$supervisi['id_rombel'] = $mr['id_rombel'];
					break;
				}
			}
			
			// // add supervisi
			$id_supervisi = $this->m_supervisi->insert_supervisi($supervisi);

			// add detail supervisi
			$supervisi_detail = array();
			foreach ($data['butir_supervisi'] as $butir) {
				$bobot = 1;
				if($post['nilai'][$butir['id_butir_supervisi']] === "A")
					$bobot = 3;
				else if($post['nilai'][$butir['id_butir_supervisi']] === "B")
					$bobot = 2;

				$supervisi_detail[] = array(
						'id_supervisi' => $id_supervisi,
						'id_butir_supervisi' => $butir['id_butir_supervisi'],
						'nilai' => $post['nilai'][$butir['id_butir_supervisi']],
						'bobot' => $bobot,
						'keterangan' => $post['keterangan'][$butir['id_butir_supervisi']]
					);
			}
			if(! $this->m_supervisi->insert_supervisi_detail($supervisi_detail)){
				set_error_message("data gagal disimpan.");
				redirect('ptk/supervisi/tambah/'.$id_guru);
			} else {
				set_success_message("data berhasil disimpan.");
				redirect('ptk/supervisi/');
			}
		}

		render('supervisi/tambah', $data);
	}

	function hapus($id_supervisi = false)
	{
		$this->load->model('m_supervisi');
		
		if($this->m_supervisi->delete_supervisi($id_supervisi)){
			set_success_message("data berhasil dihapus.");
		} else {
			set_error_message("data gagal dihapus.");
		}

		redirect(getenv('HTTP_REFERER'));
	}

}
