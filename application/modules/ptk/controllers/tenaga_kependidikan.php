<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class tenaga_kependidikan extends Simple_Controller {
    	public $myconf = array(
			//Nama controller
			"name" 			=> "ptk/tenaga_kependidikan",
			"component"		=> "pendidik_dan_tenaga_kependidikan",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Pendidik dan Tenaga Kependidikan",
			"subtitle"		=> "",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "m_tenaga_kependidikan",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"status_sertifikasi"=>"Status Sertifikasi",
								"nuptk"=>"NUPTK",
								"kode_guru"=>"Kode Guru",
								"nip"=>"NIP",
								"nama"=>"Nama",
								"jenis_kelamin"=>"Jenis Kelamin",					
								"tempat_lahir"=>"Tempat Lahir",					
								"tgl_lahir"=>"Tanggal Lahir",					
								"alamat"=>"Alamat Rumah",
								"hp"=>"Nomor HP",
								"telepon"=>"Nomor Telepon",
								"tgl_diangkat"=>"Tanggal Diangkat",
								"gaji"=>"Gaji",
								"foto"=>"Foto",
								"status_aktif"=>"Status Aktif",
								"id_golongan_darah"=>"Golongan Darah",
								"id_status_pernikahan"=>"Status Pernikahan",
								"id_agama"=>"Agama",
								"id_jenis_kepegawaian"=>"Jenis Pegawai",
								"id_status_pegawai"=>"Status Pegawai",
								"id_kecamatan"=>"Kecamatan",
								"id_jenjang_pendidikan"=>"Jenjang Pendidikan",
								"id_pangkat_golongan"=>"Pangkat Golongan",
								"id_sekolah"=>"Sekolah"
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								"tgl_lahir"=>"date",
								"tgl_diangkat"=>"date",
								"id_guru"=>"hidden",
								"foto"=>"file",
								"id_golongan_darah"=>array("r_golongan_darah","id_golongan_darah","golongan_darah"),
								"id_status_pernikahan"=>array("r_status_pernikahan","id_status_pernikahan","status_pernikahan"),
								"id_agama"=>array("r_agama","id_agama","agama"),
								"id_status_pegawai"=>array("r_status_pegawai","id_status_pegawai","status"),
								"id_kecamatan"=>array("r_kecamatan","id_kecamatan","kecamatan"),
								"jenis_kelamin"=>array("list"=>array("l"=>"Laki-laki","p"=>"Permpuan")),
								"status_aktif"=>array("list"=>array("1"=>"Aktif","0"=>"Tidak Aktif")),
								"id_jenjang_pendidikan"=>array("r_jenjang_pendidikan","id_jenjang_pendidikan","jenjang_pendidikan"),
								"id_jenis_kepegawaian"=>array("r_jenis_kepegawaian","id_jenis_kepegawaian","jenis_kepegawaian"),
								"id_sekolah"=>"hidden"
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
							
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								"nuptk","nip"
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "guru/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("nama","jenis_kelamin","tempat_lahir","tgl_lahir", "jenis_kepegawaian","hp"),
								"field_sort"			=> array("nama","jenis_kelamin","tempat_lahir","tgl_lahir"),
								"field_filter"			=> array("nama","nip"),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(),
								"field_comparator"		=> array("id_guru"=>"=","id_sekolah"=>"="),
								"field_operator"		=> array("id_guru"=>"AND","id_sekolah"=>"AND"),
								//"field_align"			=> array(),
								"field_size"			=> array("action_col"=>"150"),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array('detail'=>'ptk/tenaga_kependidikan/detail'),
								//"custom_add_link"		=> array('label'=>'Add','href'=>'school/add' ),
								//"custom_edit_link"		=> array(),
								//"custom_delete_link"	=> array(),
								//"custom_link"			=> array("Kurikulum"=>"kurikulum/datalist","Siswa"=>"Siswa/datalist"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								//"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array("nama","nip","nuptk","kode_guru","jenis_kelamin","tempat_lahir","tgl_lahir","alamat","hp","telepon","tgl_diangkat","gaji","foto","status_aktif","id_golongan_darah","id_status_pernikahan","id_agama","id_status_pegawai","id_kecamatan","id_jenjang_pendidikan","id_jenis_kepegawaian"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								//"redirect_link"			=> ""
							),
			//Konfigurasi tampilan edit
			/*"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"view_file"				=> "simple/edit",
								"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array(),
								"custom_action"			=> "",
								"msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								"msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);
	
	
		public function pre_process($data){
			//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
			//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
			
			
			//$id_pihak = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			//$list_pihak_komunikasi = get_list("m_guru","id_guru","kode_guru");
			$data['conf']['data_list']['subtitle'] = "Daftar Tenaga Kependidikan";
			$data['conf']['data_add']['subtitle'] = "Penambahan Tenaga Kependidikan";
			$data['conf']['data_edit']['subtitle'] = "Edit Tenaga Kependidikan";
			
			$data['conf']['data_delete']['redirect_link'] = "ptk/tenaga_kependidikan/datalist";
			$data['conf']['data_add']['redirect_link'] = "ptk/tenaga_kependidikan/datalist";
			$data['conf']['data_edit']['redirect_link'] = "ptk/tenaga_kependidikan/datalist";
			
			return $data;
		}
		
		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
			// $id_guru = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			// $_POST['filter']['id_sekolah'] = get_id_sekolah();
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															// 'id_guru'=>$id_guru,
															// 'id_sekolah'=>get_id_sekolah() 
														);
			
			//Mengganti link add yang ada di kanan atas form datalist
			$data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'ptk/tenaga_kependidikan/add');			
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'ptk/tenaga_kependidikan/edit/');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'ptk/tenaga_kependidikan/delete/');
			
			return $data;
		}
		
		public function before_datatable($data){
			//Method ini hanya akan dieksekusi di awal method datatable
			//Mengambil id_pihak yang diposting dari form datalist
			$filter = $this->input->post("filter");
			//echo "<br><br><br><br><br><br><br><br>";
			//$this->mydebug($filter);
			$id_guru = $filter['id_sekolah'];
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'ptk/tenaga_kependidikan/edit/');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'ptk/tenaga_kependidikan/delete/');
			
			return $data;
		}
		
				
		public function before_render_add($data){
			if(!$this->input->post()){
				//$id_pihak = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
				//mengisi nilai untuk id_pihak_komunikasi di form input
				//$_POST['item']['id_pihak_komunikasi'] = $id_pihak; //name='item[id_pihak_kom..]'
			
			}
				
			return $data;
		}
		
		public function before_add($data){
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
				$data['item']['id_sekolah'] = get_id_sekolah();
				
				$config['upload_path'] = './extras/guru/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '100';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$config['max_height']  = '768';

				$this->load->library('upload', $config);
				
				if ( ! $this->upload->do_upload("item[foto]"))
					{
						set_error_message($this->upload->display_errors());

					//	$this->load->view('upload_form', $error);
					}
					else
					{
						$data = array('upload_data' => $this->upload->data());
						set_error_message("upload sukses");
					}

			}
			return $data;
		}
				
		public function before_edit($data){
			if(!$this->input->post()){
				$data['id'] = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 0;
			}else{
				$config['upload_path'] = './extras/guru/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['max_size']	= '1000';
				$config['max_width']  = '1024';
				$config['max_height']  = '768';
				$config['max_height']  = '768';

				$this->load->library('upload', $config);
				
				// set_error_message(print_r($_FILES));
				
				if ( ! $this->upload->do_upload("item"))
					{
						
						//set_error_message("gagal");
						//set_error_message($this->upload->display_errors());

					//	$this->load->view('upload_form', $error);
					}
					else
					{
						$data = array('upload_data' => $this->upload->data());
						//set_error_message(print_r($_FILES['item']));
						//set_error_message("upload sukses");
					}
			}
			return $data;
		}
		
		public function before_delete($data){
			$data['id'] = $this->uri->rsegment(3);
			return $data;
		}
		
		public function detail(){
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			$this->load->model('m_tenaga_kependidikan');
			$id = $this->uri->rsegment(3);
			$data['detail'] = $this->m_tenaga_kependidikan->getdetail($id);
			//$this->mydebug($data);
			render('tenaga_kependidikan/detail', $data);
		}

}
