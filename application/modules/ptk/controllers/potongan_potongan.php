<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class potongan_potongan extends Simple_Controller {
    	
		public function index(){
			$this->load->model('m_potongan');
			$data['data_potongan']=$this->m_potongan->select_data_potongan()->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render("potongan_potongan/potongan_potongan",$data);
		}
		
		public function load_add(){
			render("potongan_potongan/form_tambah");
		
		}
		
		public function load_edit($id){
			$this->load->model('m_potongan');
			$data['data'] = $this->m_potongan->select_data_edit($id)->result_array();
			render("potongan_potongan/form_edit",$data);
		
		}
		
		public function load_add_detail($id_potongan){
			$this->load->model('m_potongan');
			$id=get_id_sekolah();
		
			$data['guru'] = $this->m_potongan->select_guru($id)->result_array();
			$data['data_potongan2']=$this->m_potongan->select_data_potongan2($id)->result_array();
			//print_r($data['data_potongan2']);
			render("potongan_potongan/form_tambah_detail",$data);
		
		}
		
		public function load_detail($id){
			$this->load->model('m_potongan');
			$id_sekolah=get_id_sekolah();
			$data['data_potongan2']=$this->m_potongan->select_data_potongan2($id_sekolah)->result();
			$data['statuspil'] = $id;
			$data['data_potongan']=$this->m_potongan->select_data_potongan_detail($id)->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render("potongan_potongan/potongan_detail",$data);	
		}
		
		public function submit_add(){
			$this->load->model('m_potongan');
		
			$data['id_sekolah'] = get_id_sekolah();
			$data['nama_potongan'] = $this->input->post('jenis_potongan');
			$data['besaran_uang'] = $this->input->post('besaran');
			$data['besaran_prosentase'] = $this->input->post('gaji_pokok');


			$this->m_potongan->add($data);
			redirect(site_url('ptk/potongan_potongan/index/')) ;
		}
		
		public function submit_edit(){
			$this->load->model('m_potongan');
		
			$id = $this->input->post('id_potongan');
			$data['nama_potongan'] = $this->input->post('jenis_potongan');
			$data['besaran_uang'] = $this->input->post('besaran');
			$data['besaran_prosentase'] = $this->input->post('gaji_pokok');


			$this->m_potongan->update_detail($id,$data);
			redirect(site_url('ptk/potongan_potongan/index/')) ;
		}
		
		public function submit_add_detail(){
			$this->load->model('m_potongan');
			// print_r($_POST);
			$data['id_guru'] = $this->input->post('id_guru');
			
			$data['id_potongan'] = $this->input->post('id_potongan');
			$data['jumlah'] = $this->input->post('jumlah');
			
			if($data['id_potongan']==0){
				set_error_message('Pilih Jenis Potongan');
				redirect(site_url('ptk/potongan_potongan/load_add_detail/'.$data['id_potongan'])) ;
				
			}
			
			if($data['jumlah']==NULL){
				$data['jumlah']=1;
				//echo $data['jumlah'];
			}
			
			$this->m_potongan->add_detail($data);
		
			redirect(site_url('ptk/potongan_potongan/load_detail/'.$data['id_potongan'])) ;
		}
		
		public function delete_potongan_detail($id,$id_potongan){
			$this->load->model('m_potongan');
			$this->m_potongan->delete_detail($id);
			redirect(site_url('ptk/potongan_potongan/load_detail/'.$id_potongan)) ;
		}
		
		public function delete($id_potongan){
			$this->load->model('m_potongan');
			$this->m_potongan->delete($id_potongan);
			$this->m_potongan->delete_detail2($id_potongan);
			redirect(site_url('ptk/potongan_potongan/index/')) ;
		}
}
