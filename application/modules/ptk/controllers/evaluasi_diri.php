<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class evaluasi_diri extends CI_Controller {

	public function index()
	{	
		$this->load->model('ptk/m_evaluasi_diri');
		$id_sekolah = get_id_sekolah();
		$gid=get_usergroup();
		
		if($gid!=9){
		$data['guru'] = $this->m_evaluasi_diri->get_guru($id_sekolah)->result_array();
		}elseif($gid==9){
		$id_guru=get_id_personal();
		$data['guru'] = $this->m_evaluasi_diri->get_guru_guru($id_sekolah,$id_guru)->result_array();
		}
		$data['component'] = "pendidik_dan_tenaga_kependidikan";			
		render('evaluasi_diri/evaluasi_diri',$data);
	}

	public function load_add($id)
	{	
		$this->load->model('ptk/m_evaluasi_diri');

		if($id == 0){
			set_error_message('Pilih Guru Terlebih Dahulu!');
			redirect(site_url('ptk/evaluasi_diri'));
		}else{
			$data['id_guru'] = $id;
			render('evaluasi_diri/form_tambah', $data);					
		}
	}

	public function simpan(){
		$this->load->model('ptk/m_evaluasi_diri');

		$id_guru = $this->input->post('id_guru');
		$tgl_mulai = tanggal_db($this->input->post('tgl_mulai'));
		$tgl_akhir = tanggal_db($this->input->post('tgl_akhir'));

		$config['upload_path'] = './extras/evaluasi/';
		$config['allowed_types'] = 'pdf|doc|docx|xls|txt';
		$config['max_size']	= '10000';

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile')){
			$error = array('error' => $this->upload->display_errors());
		}else{
			$lokasi_file = $this->upload->data();
		}

		$this->m_evaluasi_diri->insert($id_guru, $tgl_mulai, $tgl_akhir, $lokasi_file['full_path']);
		redirect(site_url('ptk/evaluasi_diri'));
	}

	public function download(){
		$this->load->model('ptk/m_evaluasi_diri');
		$this->load->helper('download');
		$id = $this->uri->segment(4);
		if ($id == NULL){
			redirect('ptk/evaluasi_diri');
		}
		$row = $this->m_evaluasi_diri->get_data_by($id)->row();
		$file = $row->lokasi_file;
		$data = file_get_contents($file);
		$filename = basename($file);
		
		force_download($filename, $data);
	}

	public function delete($id_guru_evaluasi){
		$this->load->model('ptk/m_evaluasi_diri');
		$this->m_evaluasi_diri->delete($id_guru_evaluasi);
		redirect(site_url('ptk/evaluasi_diri'));
	}
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */