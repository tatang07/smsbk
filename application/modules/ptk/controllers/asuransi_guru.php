<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class asuransi_guru extends Simple_Controller {
    	
		public function index(){

			$this->load->model('m_asuransi_guru');
			$id=get_id_personal();
			$data['data_asuransi']=$this->m_asuransi_guru->select_data_asuransi($id)->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render("asuransi_guru/asuransi_guru",$data);
		}
		
}
