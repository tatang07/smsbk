<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class tunjangan_tunjangan_guru extends Simple_Controller {
    	
		public function index(){
			$this->load->model('m_tunjangan_guru');
			$id=get_id_personal();
			//echo $id;
			
			$data['data_tunjangan']=$this->m_tunjangan_guru->select_data_tunjangan($id)->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render("tunjangan_tunjangan_guru/tunjangan_tunjangan_guru",$data);
		}
		
		
}
