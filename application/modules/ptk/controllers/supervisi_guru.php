<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class Supervisi_guru extends Simple_Controller {

	public function index()
	{	
		$this->load->model('ptk/m_supervisi_guru');
		$id_sekolah = get_id_sekolah();
		$id_tahun_ajaran = get_id_tahun_ajaran();
		$id_guru = get_id_personal();
		$data['data'] = $this->m_supervisi_guru->select_data(7000);
		
		// print_r($data['data']);
		
		// exit();
		$data['component'] = "pendidik_dan_tenaga_kependidikan";
		render('supervisi_guru/supervisi_guru',$data);
	}

	function detail($id_supervisi)
	{
		$this->load->model('m_supervisi');
		$data['supervisi'] = $this->m_supervisi->get($id_supervisi);
		$data['detail'] = $this->m_supervisi->get_supervisi_detail($id_supervisi);

		render('supervisi_guru/detail', $data);
	}

	function tambah($id_guru = false)
	{
		$this->load->model('m_supervisi');
		$data['guru_gmr'] = $this->m_supervisi->get_guru_gmr();
		$data['butir_supervisi'] = $this->m_supervisi->get_butir_supervisi();
		$data['id_guru'] = $id_guru;

		$data['matpel_rombel'] = array();
		if($id_guru){
			if($matpel_rombel = $this->m_supervisi->get_matpel_rombel($id_guru)){
				foreach ($matpel_rombel as $mr) {
					$data['matpel_rombel'][$mr['id_guru_matpel_rombel']] = $mr['rombel'].' : '.$mr['pelajaran'];
				}
			}
		}

		if($post = $this->input->post()){
			$supervisi = array(
					'id_guru' => $id_guru,
					'waktu' => tanggal_db($post['waktu']),
					'tanggal_terbit' => tanggal_db($post['tanggal_terbit']),
					'kode' => $post['kode_supervisi'],
					'materi' => $post['materi'],
					'hasil' => $post['kesimpulan'],
					'id_term' => get_id_term(),
				);

			foreach ($matpel_rombel as $mr) {
				if($mr['id_guru_matpel_rombel'] == $post['id_gmr']){
					$supervisi['id_pelajaran'] = $mr['id_pelajaran'];
					$supervisi['id_rombel'] = $mr['id_rombel'];
					break;
				}
			}
			
			// // add supervisi
			$id_supervisi = $this->m_supervisi->insert_supervisi($supervisi);

			// add detail supervisi
			$supervisi_detail = array();
			foreach ($data['butir_supervisi'] as $butir) {
				$bobot = 1;
				if($post['nilai'][$butir['id_butir_supervisi']] === "A")
					$bobot = 3;
				else if($post['nilai'][$butir['id_butir_supervisi']] === "B")
					$bobot = 2;

				$supervisi_detail[] = array(
						'id_supervisi' => $id_supervisi,
						'id_butir_supervisi' => $butir['id_butir_supervisi'],
						'nilai' => $post['nilai'][$butir['id_butir_supervisi']],
						'bobot' => $bobot,
						'keterangan' => $post['keterangan'][$butir['id_butir_supervisi']]
					);
			}
			if(! $this->m_supervisi->insert_supervisi_detail($supervisi_detail)){
				set_error_message("data gagal disimpan.");
				redirect('ptk/supervisi/tambah/'.$id_guru);
			} else {
				set_success_message("data berhasil disimpan.");
				redirect('ptk/supervisi/');
			}
		}

		render('supervisi_guru/tambah', $data);
	}

	function hapus($id_supervisi = false)
	{
		$this->load->model('m_supervisi');
		
		if($this->m_supervisi->delete_supervisi($id_supervisi)){
			set_success_message("data berhasil dihapus.");
		} else {
			set_error_message("data gagal dihapus.");
		}

		redirect(getenv('HTTP_REFERER'));
	}

}
