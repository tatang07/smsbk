<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class kinerja_guru extends CI_Controller {

	public function index()
	{	
		$this->load->model('ptk/m_kinerja_guru');
		$id_sekolah = get_id_sekolah();
		$gid = get_usergroup();
		if($gid!=9){
		$data['guru'] = $this->m_kinerja_guru->get_guru($id_sekolah);
		}elseif($gid==9){
		$id_guru=get_id_personal();
		$data['guru'] = $this->m_kinerja_guru->get_guru_guru($id_sekolah,$id_guru);
		}
		// $data['isi'] = $this->m_kinerja_guru->get_data();
		$data['component'] = "pendidik_dan_tenaga_kependidikan";
		render('kinerja_guru/kinerja_guru',$data);
	}
	public function load_add()
	{	
		$this->load->model('ptk/m_kinerja_guru');
		$id_sekolah= get_id_sekolah();
		$data['tingkat_kelas']=0;
		$jenjang_sekolah = get_jenjang_sekolah();
		$data['guru'] = $this->m_kinerja_guru->select_guru();
		$data['jk_guru'] = $this->m_kinerja_guru->select_jenis_kompetensi_guru();
		$data['bk_guru'] = $this->m_kinerja_guru->select_butir_kompetensi_guru();
		$data['component'] = "pendidik_dan_tenaga_kependidikan";
		render('kinerja_guru/form_tambah',$data);
	}

	public function load_detail($id_penilaian_kinerja_guru,$id_guru)
	{	
		$this->load->model('ptk/m_kinerja_guru');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		$data['data_detail'] = $this->m_kinerja_guru->select_data_detail($id_penilaian_kinerja_guru);
		$data['data_butir'] = $this->m_kinerja_guru->select_data_butir($id_penilaian_kinerja_guru);
		
		$data['jk_guru'] = $this->m_kinerja_guru->select_jenis_kompetensi_guru();
		$data['bk_guru'] = $this->m_kinerja_guru->select_butir_kompetensi_guru();
		// $data['data_edit'] = $this->m_kinerja_guru->get_data_edit($id_siswa_catatan_bk);
		// foreach($data['data_edit'] as $a){
			// $id_tingkat_kelas=$a['id_tingkat_kelas'];
		// }
		// $data['tabi'] = $this->m_kinerja_guru->get_siswa($id_sekolah,$id_tingkat_kelas);
		// print_r($data['data_butir']);
		$data['component'] = "pendidik_dan_tenaga_kependidikan";
		render('kinerja_guru/form_detail',$data);
	}

	public function delete($id_penilaian_kinerja_guru){
			$this->load->model('ptk/m_kinerja_guru');
			$this->m_kinerja_guru->delete($id_penilaian_kinerja_guru);
			redirect(site_url('ptk/kinerja_guru/index/')) ;
	}

	public function submit_add(){
			$this->load->model('ptk/m_kinerja_guru');
			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('periode_mulai', 'periode_mulai', 'required');
			$this->form_validation->set_rules('periode_akhir', 'periode_akhir', 'required');
			$this->form_validation->set_rules('dinilai', 'periode_akhir', 'required');
			$this->form_validation->set_rules('penilai', 'periode_akhir', 'required');
			$this->form_validation->set_rules('kesimpulan', 'periode_akhir', 'required');
			
			if($this->form_validation->run() == TRUE){
			$data['id_guru'] = $this->input->post('dinilai');
			$data['periode_awal'] = tanggal_db($this->input->post('periode_mulai'));
			$data['periode_akhir'] = tanggal_db($this->input->post('periode_akhir'));
			$data['penilai'] = $this->input->post('penilai');
			$data['keterangan'] = null;
			$data['kesimpulan'] = $this->input->post('kesimpulan');


			$this->m_kinerja_guru->add($data);
			$row=$this->m_kinerja_guru->get_pk_guru($data['id_guru'], $data['periode_awal'], $data['periode_akhir'], $data['penilai'], $data['kesimpulan']);
			$id=$row[0]['id_penilaian_kinerja_guru'];
			$data1['id_penilaian_kinerja_guru'] = $id;
			$angka = $this->input->post('angka');
			$keterangan = $this->input->post('keterangan');
			
			foreach($angka as $id_butir => $n){
				if($n!=''){
					$data1['id_butir_kompetensi_guru'] = $id_butir;
					$data1['nilai_angka'] = $n;
				}else{
					$data1['id_butir_kompetensi_guru'] = $id_butir;
					$data1['nilai_angka'] = 0;
				}
				$data1['keterangan'] = $keterangan[$id_butir];
				
				$this->m_kinerja_guru->add_detail($data1);
			}
			
			set_success_message('Data Berhasil Ditambah!');
			redirect(site_url('ptk/kinerja_guru/index/'));
			}
			else{
				set_error_message('Data belum terisi semua atau terjadi kesalahan pengisian, silahkan tambah data lagi.');
				redirect(site_url('ptk/kinerja_guru/load_add/'));
			}
	}
	
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('ptk/m_kinerja_guru');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_kinerja_guru->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_kinerja_guru->select_kelas($jenjang_sekolah);
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render('bp_bk/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('ptk/m_kinerja_guru');
			$id=$this->input->post('id');
			$data['id_siswa'] = $this->input->post('id_siswa');
			$data['tanggal_catatan'] = tanggal_db($this->input->post('tanggal'));
			$data['keterangan'] = $this->input->post('keterangan');


			$this->m_kinerja_guru->edit($data,$id);
			redirect(site_url('ptk/bp_bk/index/')) ;
	}
	public function search(){
		$this->load->model('ptk/m_kinerja_guru');
		$nama = $this->input->post('nama');
		$id_tingkat_kelas = $this->input->post('id_tingkat_kelas');
		$id_sekolah = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		$data['ta'] = $this->m_kinerja_guru->get_kelas($jenjang_sekolah);
		$data['isi'] = $this->m_kinerja_guru->get_data_search($id_tingkat_kelas,$nama);
		$data['component'] = "pendidik_dan_tenaga_kependidikan";
		render('bp_bk/bp_bk',$data);

	}
	
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */