<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


// SELECT id_silabus,p.id_pelajaran,kode_pelajaran,pelajaran,id_tingkat_kelas,id_program FROM `m_silabus` as s join m_pelajaran as p on s.id_pelajaran=p.id_pelajaran join m_mata_pelajaran as mp on mp.id_pelajaran=p.id_pelajaran

class peningkatan_kompetensi extends CI_Controller {

	public function index()
	{	
		$this->load->model('ptk/m_peningkatan_kompetensi');
		$id_sekolah = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		$gid=get_usergroup();
		if($gid!=9){
		$data['isi'] = $this->m_peningkatan_kompetensi->get_data($id_sekolah);
		}elseif($gid==9){
		$id_guru=get_id_personal();
		$data['isi'] = $this->m_peningkatan_kompetensi->get_data_guru($id_sekolah,$id_guru);
		}
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('peningkatan_kompetensi/peningkatan_kompetensi',$data);
	}
	public function load_detail($id_guru_peningkatan_kompetensi)
	{	
		$this->load->model('ptk/m_peningkatan_kompetensi');
		$id_sekolah = get_id_sekolah();
		$jenjang_sekolah = get_jenjang_sekolah();
		$gid=get_usergroup();
		if($gid!=9){
		$data['isi'] = $this->m_peningkatan_kompetensi->get_data_detail($id_guru_peningkatan_kompetensi);
		}elseif($gid==9){
			$id_guru=get_id_personal();
			$data['isi'] = $this->m_peningkatan_kompetensi->get_data_detail_guru($id_guru_peningkatan_kompetensi,$id_guru);
		}
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('peningkatan_kompetensi/peningkatan_kompetensi_detail',$data);
	}
	public function load_add()
	{	
		$this->load->model('ptk/m_peningkatan_kompetensi');
		$id_sekolah= get_id_sekolah();
		// $jenjang_sekolah = get_jenjang_sekolah();
		$data['guru'] = $this->m_peningkatan_kompetensi->select_guru($id_sekolah);
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('peningkatan_kompetensi/form_tambah',$data);
	}
	public function load_add_detail($id_guru_peningkatan_kompetensi)
	{	
		$this->load->model('ptk/m_peningkatan_kompetensi');
		$id_sekolah= get_id_sekolah();
		// $jenjang_sekolah = get_jenjang_sekolah();
		$data['id_guru_peningkatan_kompetensi']=$id_guru_peningkatan_kompetensi;
		$data['guru'] = $this->m_peningkatan_kompetensi->select_guru($id_sekolah);
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('peningkatan_kompetensi/form_tambah_detail',$data);
	}
	public function load_edit($id_guru_peningkatan_kompetensi)
	{	
		$this->load->model('ptk/m_peningkatan_kompetensi');
		$id_sekolah= get_id_sekolah();
		$jenjang_sekolah= get_jenjang_sekolah();
		
		$data['data_edit'] = $this->m_peningkatan_kompetensi->get_data_edit($id_guru_peningkatan_kompetensi);
	
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('peningkatan_kompetensi/form_edit',$data);
	}

	public function delete($id_guru_peningkatan_kompetensi){
			$this->load->model('ptk/m_peningkatan_kompetensi');
			$this->m_peningkatan_kompetensi->delete($id_guru_peningkatan_kompetensi);
			redirect(site_url('ptk/peningkatan_kompetensi/index/')) ;
	}
	public function delete_detail($id_guru_peningkatan_kompetensi_detail){
			$this->load->model('ptk/m_peningkatan_kompetensi');
			$this->m_peningkatan_kompetensi->delete_detail($id_guru_peningkatan_kompetensi_detail);
			redirect(site_url('ptk/peningkatan_kompetensi/index/')) ;
	}
	public function submit_add(){
			$this->load->model('ptk/m_peningkatan_kompetensi');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['nama_program'] = $this->input->post('nama_program');
			$data['tanggal_mulai'] = tanggal_db($this->input->post('tanggal_mulai'));
			$data['tanggal_selesai'] = tanggal_db($this->input->post('tanggal_selesai'));
			$data['id_sekolah']=get_id_sekolah();
		
			$this->m_peningkatan_kompetensi->add($data);
			redirect(site_url('ptk/peningkatan_kompetensi/index/')) ;
	}
	public function submit_add_detail(){
			$this->load->model('ptk/m_peningkatan_kompetensi');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['id_guru_peningkatan_kompetensi'] = $this->input->post('id_guru_peningkatan_kompetensi');
		
		
			$this->m_peningkatan_kompetensi->add_detail($data);
			redirect(site_url('ptk/peningkatan_kompetensi/index/')) ;
	}
	public function get_nama_siswa($id_tingkat_kelas){
			$this->load->model('ptk/m_peningkatan_kompetensi');
			$id_sekolah=get_id_sekolah();
			$data['tabi'] = $this->m_peningkatan_kompetensi->get_siswa($id_sekolah,$id_tingkat_kelas);
			$jenjang_sekolah = get_jenjang_sekolah();
			$data['tingkat_kelas']=$id_tingkat_kelas;
			$data['kelas'] = $this->m_peningkatan_kompetensi->select_kelas($jenjang_sekolah);
			render('peningkatan_kompetensi/form_tambah',$data);
	}

	public function submit_edit(){
			$this->load->model('ptk/m_peningkatan_kompetensi');
			$id=$this->input->post('id');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['nama_program'] = $this->input->post('nama_program');
			$data['tanggal_mulai'] = tanggal_db($this->input->post('tanggal_mulai'));
			$data['tanggal_selesai'] = tanggal_db($this->input->post('tanggal_selesai'));
			// echo $this->input->post('id_guru');
			// echo $data['id_guru'];
		// print_r ($data);


			$this->m_peningkatan_kompetensi->edit($data,$id);
			redirect(site_url('ptk/peningkatan_kompetensi/index/')) ;
	}
	public function search(){
		$this->load->model('ptk/m_peningkatan_kompetensi');
		$nama = $this->input->post('nama');
		// $status = $this->input->post('status');
		
		$id_sekolah = get_id_sekolah();

		$data['isi'] = $this->m_peningkatan_kompetensi->get_data_search($nama,$id_sekolah);
	
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('peningkatan_kompetensi/peningkatan_kompetensi',$data);
	}
	public function search_detail($id_guru_peningkatan_kompetensi){
		$this->load->model('ptk/m_peningkatan_kompetensi');
		$nama = $this->input->post('nama');
		// $status = $this->input->post('status');
		
		$id_sekolah = get_id_sekolah();

		$data['isi'] = $this->m_peningkatan_kompetensi->get_data_search_detail($nama,$id_guru_peningkatan_kompetensi);
	
		$data['component']="pendidik_dan_tenaga_kependidikan";
		render('peningkatan_kompetensi/peningkatan_kompetensi_detail',$data);
	}
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */