<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class potongan_potongan_guru extends Simple_Controller {
    	
		public function index(){
			$this->load->model('m_potongan_guru');
			$id=get_id_personal();
			//echo $id;
			
			$data['data_potongan']=$this->m_potongan_guru->select_data_potongan($id)->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render("potongan_potongan_guru/potongan_potongan_guru",$data);
		}
		
		
}
