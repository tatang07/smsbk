<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class guru extends Simple_Controller {
    	public $myconf = array(
			//Nama controller
			"name" 			=> "ptk/guru",
			"component"		=> "pendidik_dan_tenaga_kependidikan",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Pendidik dan Tenaga Kependidikan",
			"subtitle"		=> "",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "m_guru",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"status_sertifikasi"=>"Status Sertifikasi",
								"nuptk"=>"NUPTK",
								"kode_guru"=>"Kode Guru",
								"nip"=>"NIP",
								"NIK"=>"NIK",
								"nama"=>"Nama",
								"jenis_kelamin"=>"Jenis Kelamin",					
								"tempat_lahir"=>"Tempat Lahir",					
								"tgl_lahir"=>"Tanggal Lahir",					
								"alamat"=>"Alamat Rumah",
								"hp"=>"Nomor HP",
								"e_mail"=>"Email",
								"telepon"=>"Nomor Telepon",
								"tgl_diangkat"=>"Tanggal Diangkat",
								"gaji"=>"Gaji",
								"foto"=>"Foto",
								"status_aktif"=>"Status Aktif",
								"id_golongan_darah"=>"Golongan Darah",
								"id_status_pernikahan"=>"Status Pernikahan",
								"id_agama"=>"Agama",
								"pangkat"=>"Pangkat",
								"id_status_pegawai"=>"Status Pegawai",
								"id_kecamatan"=>"Kecamatan",
								"id_jenjang_pendidikan"=>"Jenjang Pendidikan",
								"id_pangkat_golongan"=>"Pangkat Golongan",
								"id_sekolah"=>"Sekolah",
								"id_jenis_jabatan"=>"Jabatan"
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								"tgl_lahir"=>"date",
								"tgl_diangkat"=>"date",
								"id_guru"=>"hidden",
								"foto"=>"file",
								"id_golongan_darah"=>array("r_golongan_darah","id_golongan_darah","golongan_darah"),
								"id_status_pernikahan"=>array("r_status_pernikahan","id_status_pernikahan","status_pernikahan"),
								"id_agama"=>array("r_agama","id_agama","agama"),
								"id_status_pegawai"=>array("r_status_pegawai","id_status_pegawai","status"),
								"id_kecamatan"=>array("r_kecamatan","id_kecamatan","kecamatan"),
								"jenis_kelamin"=>array("list"=>array("L"=>"Laki-laki","P"=>"Perempuan")),
								"status_sertifikasi"=>array("list"=>array("b"=>"Belum Tersertifikasi","s"=>"Tersertifikasi")),
								"status_aktif"=>array("list"=>array("1"=>"Aktif","0"=>"Tidak Aktif")),
								"id_jenjang_pendidikan"=>array("r_jenjang_pendidikan","id_jenjang_pendidikan","jenjang_pendidikan"),
								"id_pangkat_golongan"=>array("r_pangkat_golongan","id_pangkat_golongan","pangkat"),
								"id_jenis_jabatan"=>array("r_jenis_jabatan","id_jenis_jabatan","jenis_jabatan"),
								"id_sekolah"=>"hidden"
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"status_sertifikasi","nuptk","kode_guru","nip","nama","jenis_kelamin","tempat_lahir","tgl_lahir","alamat","tgl_diangkat","gaji","status_aktif"
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
								"nip","NIK"
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "guru/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("NIK","nip","nama","pangkat","hp","e_mail"),
								"field_sort"			=> array("nama","jenis_kelamin","tempat_lahir","tgl_lahir"),
								"field_filter"			=> array("nama","nip"),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(),
								"field_comparator"		=> array("id_guru"=>"=","id_sekolah"=>"="),
								"field_operator"		=> array("id_guru"=>"AND","id_sekolah"=>"AND"),
								//"field_align"			=> array(),
								"field_size"			=> array("action_col"=>"150"),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								"custom_action_link"	=> array('detail'=>'ptk/guru/detail'),
								//"custom_add_link"		=> array('label'=>'Add','href'=>'school/add' ),
								//"custom_edit_link"		=> array(),
								//"custom_delete_link"	=> array(),
								"custom_link"			=> array("Import"=>"import/ptk"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								"view_file_form_input"	=> "guru/form_input",
								//"field_list"			=> array("nama","nip","nuptk","jenis_kelamin","id_agama","id_status_pernikahan","tempat_lahir","tgl_lahir","alamat","id_kecamatan","telepon","hp","e_mail","id_status_pegawai","tgl_diangkat","id_jenis_jabatan","id_pangkat_golongan","id_jenjang_pendidikan","id_golongan_darah","gaji","foto","status_aktif","status_sertifikasi","kode_guru"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								//"redirect_link"			=> ""
							),
			//Konfigurasi tampilan edit
			"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/edit",
								"view_file_form_input"	=> "guru/form_input",
								//"field_list"			=> array(),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil diperbaharui.",
								//"redirect_link"			=> ""
							),
			//Konfigurasi tampilan delete
			/*"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);
	
	
		public function pre_process($data){
			//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
			//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
			
			$status_pendidik = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
				
			
			//$id_pihak = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			//$list_pihak_komunikasi = get_list("m_guru","id_guru","kode_guru");
			$data['conf']['data_list']['subtitle'] = "Daftar Tenaga Pendidik";
			$data['conf']['data_add']['subtitle'] = "Penambahan Tenaga Pendidik";
			$data['conf']['data_edit']['subtitle'] = "Edit Tenaga Pendidik";
			
			if($status_pendidik==1){
				$data['conf']['data_add']['subtitle']="Penambahan Pendidik";
			}
			
			$data['conf']['data_delete']['redirect_link'] = "ptk/guru/datalist";
			$data['conf']['data_add']['redirect_link'] = "ptk/guru/datalist";
			$data['conf']['data_edit']['redirect_link'] = "ptk/guru/datalist";
			
			return $data;
		}
		
		public function after_add($data){
			if($this->input->post()){
				$fakta['username'] = $data['item']['nip']; 
				$fakta['password'] = md5($fakta['username']);
				//$fakta['default_password'] = md5($fakta['username']);
				$fakta['email'] = $data['item']['e_mail']; 
				$fakta['date_created'] = date('Y-m-d G:i:s'); 
				$fakta['is_active'] = 1; 
				$fakta['id_group'] = 9; 
				$fakta['id_personal'] = $this->m_guru->new_id(); 
				$id = get_id_sekolah();
				$fakta['id_sekolah'] = $id;
				$this->m_guru->tambah_user($fakta);
			}
			return $data;
		}
		
		public function before_datalist($data){
			
			//Method ini hanya akan dieksekusi di awal method datalist
			//Melakukan filter data agar sesuai dengan id_pihak_komunikasi dan id_sekolah
			// $id_guru = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
			$_POST['filter']['id_sekolah'] = get_id_sekolah();
			
			//membedakan pendidik dan tenaga kependidikan
			
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			$data['conf']['data_list']['field_filter_hidden'] = array(
															// 'id_guru'=>$id_guru,
															// 'id_sekolah'=>get_id_sekolah() 
														);
			
			//Mengganti link add yang ada di kanan atas form datalist
			$data['conf']['data_list']['custom_add_link'] = array('label'=>'Tambah','href'=>'ptk/guru/add');			
			
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'ptk/guru/edit/');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'ptk/guru/delete/');
			if(get_usergroup()==9){
				redirect('ptk/guru/detail');
			}
			return $data;
		}
		
		public function before_datatable($data){
			//Method ini hanya akan dieksekusi di awal method datatable
			//Mengambil id_pihak yang diposting dari form datalist
			$_POST['filter']['id_sekolah'] = get_id_sekolah();
			$filter = $this->input->post("filter");
			//echo "<br><br><br><br><br><br><br><br>";
			//$this->mydebug($filter);
			//$id_guru = $filter['id_sekolah'];
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'ptk/guru/edit/');
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'ptk/guru/delete/');
			
			return $data;
		}
		
				
		public function before_render_add($data){
			if(!$this->input->post()){
				//$id_pihak = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
				//mengisi nilai untuk id_pihak_komunikasi di form input
				//$_POST['item']['id_pihak_komunikasi'] = $id_pihak; //name='item[id_pihak_kom..]'
			
			}
				
			return $data;
		}
		
		public function before_add($data){
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
				$data['item']['id_sekolah'] = get_id_sekolah();
				$status_pendidik = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 1;
				$data['item']['status_pendidik']="$status_pendidik";
				
				// print_r($data['item']);
				// exit();
				// $data['item']['status_aktif'] = get_id_sekolah();
				
				if($_FILES["item"]["error"]["foto"]==0){
					$fname = date("YmdHisu");
					$src = 'extras/guru/'. $fname.".jpg";
					move_uploaded_file($_FILES["item"]["tmp_name"]["foto"],$src);
					$data['item']['foto'] = $fname.".jpg";
					
					$config['image_library'] = 'gd2';
					$config['source_image'] = $src;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 200;
					$config['height'] = 200;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
				}
			}
			return $data;
		}
				
		public function before_edit($data){
			if(!$this->input->post()){
				$data['id'] = $this->uri->total_rsegments()>2? $this->uri->rsegment(3): 0;
			}else{
				// $data['item']['id_sekolah'] = get_id_sekolah();
				
				if($_FILES["item"]["error"]["foto"]==0){
					$fname = date("YmdHisu");
					$src = 'extras/guru/'. $fname.".jpg";
					move_uploaded_file($_FILES["item"]["tmp_name"]["foto"],$src);
					$data['item']['foto'] = $fname.".jpg";
					// $this->mydebug($data);
					
					$config['image_library'] = 'gd2';
					$config['source_image'] = $src;
					$config['maintain_ratio'] = TRUE;
					$config['width'] = 200;
					$config['height'] = 200;
					$this->load->library('image_lib', $config);
					$this->image_lib->resize();
				}
			}
			return $data;
		}
		
		public function before_delete($data){
			$data['id'] = $this->uri->rsegment(3);
			return $data;
		}
		
		public function detail(){
			$this->load->model('m_guru');
			$id = $this->uri->rsegment(3);
			$gid = get_usergroup();
			if($gid==9){
				$id = get_id_personal();
			}
			
			// echo $id;
			// exit();
			
			$data['detail'] = $this->m_guru->getdetail($id);
			$data['det_jabatan'] = $this->m_guru->getJabatan($id)->result_array();
			$data['det_penataran'] = $this->m_guru->getPenataran($id)->result_array();
			$data['det_pendidikan'] = $this->m_guru->getPendidikan($id)->result_array();
			$data['det_pengalaman'] = $this->m_guru->getPengalaman($id)->result_array();
			$data['det_prestasi'] = $this->m_guru->getPrestasi($id)->result_array();
			//$this->mydebug($data);
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			
			render('guru/detail', $data);
		}
		
		// ------------ bagian jabatan

		public function load_tambah_jabatan($id_guru){
			$this->load->model('m_guru');			
			$id = $this->uri->rsegment(3);

			$data['id_guru']=$id_guru;
			$data['jabatan']=$this->m_guru->getJabatan($id)->result_array();
			$data['macam_jabatan']=$this->m_guru->getMacamJabatan()->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render('guru/tambah_jabatan',$data);
		}
		public function load_edit_jabatan($id_guru,$id_gj){
			$this->load->model('m_guru');
			$data['id_guru']=$id_guru;
			$data['id_gj']=$id_gj;
			$data['det_jabatan'] = $this->m_guru->getDetJabatan($id_guru,$id_gj)->result_array();
			$data['macam_jabatan']=$this->m_guru->getMacamJabatan()->result_array();
			render('guru/edit_jabatan',$data);
		}
		public function c_tambah_jabatan(){
			$this->load->model('m_guru');
			$data['id_jenis_jabatan'] = $this->input->post('id_jenis_jabatan');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['no_sk'] = $this->input->post('no_sk');
			$data['status'] = $this->input->post('status');
			$data['tgl_sk'] = tanggal_db($this->input->post('tgl_sk'));
			$data['tmt_sk'] = tanggal_db($this->input->post('tmt_sk'));
			$data['keterangan'] = $this->input->post('keterangan');
			$this->m_guru->tambah_jabatan($data);
			redirect(site_url('ptk/guru/detail/'.$data['id_guru']));
		}
		
		public function c_update_jabatan(){
			$this->load->model('m_guru');
			$data['id_jenis_jabatan'] = $this->input->post('id_jenis_jabatan');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['no_sk'] = $this->input->post('no_sk');
			$data['status'] = $this->input->post('status');
			$data['tgl_sk'] = tanggal_db($this->input->post('tgl_sk'));
			$data['tmt_sk'] = tanggal_db($this->input->post('tmt_sk'));
			$data['keterangan'] = $this->input->post('keterangan');
			$id_guru_jabatan = $this->input->post('id_guru_jabatan');
			// print_r($data);
			//echo $id_guru_jabatan;
			$this->m_guru->update_jabatan($data,$id_guru_jabatan);
			redirect(site_url('ptk/guru/detail/'.$data['id_guru']));
		}
		
		public function c_delete_jabatan($id_guru,$id_guru_jabatan){
			$this->load->model('m_guru');
			$this->m_guru->delete_jabatan($id_guru_jabatan);
			redirect(site_url('ptk/guru/detail/'.$id_guru));
		}
		
		// ------------ bagian penataran
		
		public function load_tambah_penataran($id_guru){
			$data['id_guru']=$id_guru;
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render('guru/tambah_penataran',$data);
		}
		
		public function load_edit_penataran($id_guru,$id_gp){
			$this->load->model('m_guru');
			$data['id_guru']=$id_guru;
			$data['id_gp']=$id_gp;
			$data['det_penataran'] = $this->m_guru->getDetPenataran($id_guru,$id_gp)->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render('guru/edit_penataran',$data);
		}

		public function c_tambah_penataran(){
			$this->load->model('m_guru');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['tahun'] = $this->input->post('tahun');
			$data['lokasi'] = $this->input->post('lokasi');
			$data['penataran'] = $this->input->post('penataran');
			$data['deskripsi'] = $this->input->post('deskripsi');
			$this->m_guru->tambah_penataran($data);
			redirect(site_url('ptk/guru/detail/'.$data['id_guru'].'#t1-c2'));
		}

		public function c_update_penataran(){
			$this->load->model('m_guru');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['tahun'] = $this->input->post('tahun');
			$data['lokasi'] = $this->input->post('lokasi');
			$data['penataran'] = $this->input->post('penataran');
			$data['deskripsi'] = $this->input->post('deskripsi');
			$id_guru_penataran = $this->input->post('id_guru_penataran');
			$this->m_guru->update_penataran($data,$id_guru_penataran);
			redirect(site_url('ptk/guru/detail/'.$data['id_guru'].'#t1-c2'));
		}
		
		public function c_delete_penataran($id_guru,$id_guru_penataran){
			$this->load->model('m_guru');
			$this->m_guru->delete_penataran($id_guru_penataran);
			redirect(site_url('ptk/guru/detail/'.$id_guru.'#t1-c2'));
		}

		// ------------ bagian pendidikan

		public function load_tambah_pendidikan($id_guru){
			$this->load->model('m_guru');
			$data['id_guru']=$id_guru;
			$data['jenjang_pendidikan']=$this->m_guru->getJenjangPendidikan()->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render('guru/tambah_pendidikan',$data);
		}

		public function load_edit_pendidikan($id_guru,$id_gp){
			$this->load->model('m_guru');
			$data['id_guru']=$id_guru;
			$data['id_gp']=$id_gp;
			$data['det_pendidikan'] = $this->m_guru->getDetPendidikan($id_guru,$id_gp)->result_array();
			$data['jenjang_pendidikan']=$this->m_guru->getJenjangPendidikan()->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render('guru/edit_pendidikan',$data);
		}
		
		public function c_tambah_pendidikan(){
			$this->load->model('m_guru');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['id_jenjang_pendidikan'] = $this->input->post('id_jenjang_pendidikan');
			$data['program_studi'] = $this->input->post('program_studi');
			$data['nama_instansi'] = $this->input->post('instansi');
			$data['tahun_mulai'] = $this->input->post('tahun_mulai');
			$data['tahun_selesai'] = $this->input->post('tahun_selesai');
			$this->m_guru->tambah_pendidikan($data);
			redirect(site_url('ptk/guru/detail/'.$data['id_guru'].'#t1-c3'));
		}
		
		public function c_update_pendidikan(){
			$this->load->model('m_guru');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['id_jenjang_pendidikan'] = $this->input->post('id_jenjang_pendidikan');
			$data['program_studi'] = $this->input->post('program_studi');
			$data['nama_instansi'] = $this->input->post('instansi');
			$data['tahun_mulai'] = $this->input->post('tahun_mulai');
			$data['tahun_selesai'] = $this->input->post('tahun_selesai');
			$id_guru_pendidikan = $this->input->post('id_guru_pendidikan');
			$this->m_guru->update_pendidikan($data, $id_guru_pendidikan);
			redirect(site_url('ptk/guru/detail/'.$data['id_guru'].'#t1-c3'));
		}

		public function c_delete_pendidikan($id_guru,$id_guru_pendidikan){
			$this->load->model('m_guru');
			$this->m_guru->delete_pendidikan($id_guru_pendidikan);
			redirect(site_url('ptk/guru/detail/'.$id_guru.'#t1-c3'));
		}

		// ------------ bagian pengalaman

		public function load_tambah_pengalaman($id_guru){
			$data['id_guru']=$id_guru;
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render('guru/tambah_pengalaman',$data);
		}

		public function c_tambah_pengalaman(){
			$this->load->model('m_guru');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['pengalaman'] = $this->input->post('pengalaman');
			$data['lokasi'] = $this->input->post('lokasi');
			$data['tgl_mulai'] = tanggal_db($this->input->post('tanggal_mulai'));
			$data['tgl_akhir'] = tanggal_db($this->input->post('tanggal_akhir'));
			$this->m_guru->tambah_pengalaman($data);
			redirect(site_url('ptk/guru/detail/'.$data['id_guru'].'#t1-c4'));
		}
		
		public function load_edit_pengalaman($id_guru,$id_gp){
			$this->load->model('m_guru');
			$data['id_guru']=$id_guru;
			$data['id_gp']=$id_gp;
			$data['det_pengalaman'] = $this->m_guru->getDetPengalaman($id_guru,$id_gp)->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render('guru/edit_pengalaman',$data);
		}
		
		public function c_update_pengalaman(){
			$this->load->model('m_guru');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['pengalaman'] = $this->input->post('pengalaman');
			$data['lokasi'] = $this->input->post('lokasi');
			$data['tgl_mulai'] = tanggal_db($this->input->post('tanggal_mulai'));
			$data['tgl_akhir'] = tanggal_db($this->input->post('tanggal_akhir'));
			$id_guru_pengalaman = $this->input->post('id_guru_pengalaman');
			$this->m_guru->update_pengalaman($data,$id_guru_pengalaman);
			redirect(site_url('ptk/guru/detail/'.$data['id_guru'].'#t1-c4'));
		}
		
		public function c_delete_pengalaman($id_guru,$id_guru_pengalaman){
			$this->load->model('m_guru');
			$this->m_guru->delete_pengalaman($id_guru_pengalaman);
			redirect(site_url('ptk/guru/detail/'.$id_guru.'#t1-c4'));
		}
		
		// ------------ bagian prestasi

		public function load_tambah_prestasi($id_guru){
			$this->load->model('m_guru');
			$data['id_guru']=$id_guru;
			$data['tingkat_wilayah']=$this->m_guru->getTingkatWilayah()->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render('guru/tambah_prestasi',$data);
		}
		
		public function load_edit_prestasi($id_guru,$id_gp){
			$this->load->model('m_guru');
			$data['id_guru']=$id_guru;
			$data['id_gp']=$id_gp;
			$data['det_prestasi'] = $this->m_guru->getDetPrestasi($id_guru,$id_gp)->result_array();
			$data['tingkat_wilayah']=$this->m_guru->getTingkatWilayah()->result_array();
			$data['component'] = "pendidik_dan_tenaga_kependidikan";
			render('guru/edit_prestasi',$data);
		}

		public function c_tambah_prestasi(){
			$this->load->model('m_guru');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['id_tingkat_wilayah'] = $this->input->post('id_tingkat_wilayah');
			$data['lokasi'] = $this->input->post('lokasi');
			$data['tahun'] = $this->input->post('tahun');
			$data['deskripsi'] = $this->input->post('deskripsi');
			$this->m_guru->tambah_prestasi($data);
			redirect(site_url('ptk/guru/detail/'.$data['id_guru'].'#t1-c5'));
		}

		public function c_update_prestasi(){
			$this->load->model('m_guru');
			$data['id_guru'] = $this->input->post('id_guru');
			$data['id_tingkat_wilayah'] = $this->input->post('id_tingkat_wilayah');
			$data['lokasi'] = $this->input->post('lokasi');
			$data['tahun'] = $this->input->post('tahun');
			$data['deskripsi'] = $this->input->post('deskripsi');
			$id_guru_prestasi = $this->input->post('id_guru_prestasi');
			$this->m_guru->update_prestasi($data,$id_guru_prestasi);
			redirect(site_url('ptk/guru/detail/'.$data['id_guru'].'#t1-c5'));
		}

		public function c_delete_prestasi($id_guru,$id_guru_prestasi){
			$this->load->model('m_guru');
			$this->m_guru->delete_prestasi($id_guru_prestasi);
			redirect(site_url('ptk/guru/detail/'.$id_guru.'#t1-c5'));
		}
	
		
		// public function c_tambah_prestasi(){
			// $this->load->model('m_guru');
			
			// $data['id_guru'] = $this->input->post('id_guru');
			// $data['id_tingkat_wilayah'] = $this->input->post('wilayah');
			// $data['lokasi'] = $this->input->post('lokasi');
			// $data['tahun'] = $this->input->post('tahun');
			// $data['deskripsi'] = $this->input->post('deskripsi');
			
			
			// $this->m_guru->tambah_prestasi($data);
			// redirect(site_url('ptk/guru/detail/'.$data['id_guru']));
		// }
}
