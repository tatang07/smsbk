	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			
			<form action="<?php echo base_url('ptk/sertifikasi/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Sertifikasi</legend><?php //echo($tingkat_kelas);?>
					<?php $jenjang_sekolah = get_jenjang_sekolah(); ?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama</strong>
						</label>
						<div>
							<select name="id_guru" id="id_guru" required>
							<option value="">-Nama Guru-</option>
							<?php foreach($guru as $d):?>
							
							  <option value="<?php echo $d['id_guru']?>"><?php echo $d['nama']?></option>
							
							 <?php endforeach ?>
							</select> 
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Sertifikasi</strong>
						</label>
						<div>
							<input type="text" name="sertifikasi" value="" required />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kualifikasi Guru</strong>
						</label>
						<div>
							<select name="id_kualifikasi_guru" id="id_kualifikasi_guru" required>
							<option value="">-Kualifikasi Guru-</option>
							<?php foreach($kualifikasi as $d):?>
							
							  <option value="<?php echo $d['id_kualifikasi_guru']?>"><?php echo $d['kualifikasi_guru']?></option>
							
							 <?php endforeach ?>
							</select> 
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Sertifikasi</strong>
						</label>
						<div>
							<input type="date" name="tanggal_sertifikasi" value="" required/>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Kadaluarsa</strong>
						</label>
						<div>
							<input type="date" name="tanggal_kadaluarsa" value="" required/>
							
						</div>
					</div>
					
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('ptk/sertifikasi/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		