	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			
			<form action="<?php echo base_url('ptk/sertifikasi/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Sertifikasi</legend>
					<?php foreach($data_edit as $d):?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama</strong>
						</label>
						<div>
					
							
							<input disabled type="text" name="nama" value="<?php echo $d['nama']?>" />
							<input type="hidden" name="id_guru" value="<?php echo $d['id_guru'];?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Sertifikasi</strong>
						</label>
						<div>
							<input type="text" name="sertifikasi" value="<?php echo $d['sertifikasi']?>" required/>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kualifikasi Guru</strong>
						</label>
						<div>
							<select name="id_kualifikasi_guru" id="id_kualifikasi_guru" required>
							<option value="">-Kualifikasi Guru-</option>
							<?php foreach($kualifikasi as $k):?>
							
							  <option value="<?php echo $k['id_kualifikasi_guru']?>" <?php if($k['id_kualifikasi_guru']==$d['id_kualifikasi_guru']){ echo "selected"; }?>><?php echo $k['kualifikasi_guru']?></option>
							
							 <?php endforeach ?>
							</select> 
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Sertifikasi</strong>
						</label>
						<div>
							<input type="date" name="tanggal_sertifikasi" value="<?php echo tanggal_view($d['tanggal_sertifikasi']);?>" />
							<input type="hidden" name="id" value="<?php echo $d['id_guru_sertifikasi']?>" required/>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Kadaluarsa</strong>
						</label>
						<div>
							<input type="date" name="tanggal_kadaluarsa" value="<?php echo tanggal_view($d['tanggal_kadaluarsa'])?>" required/>
							
						</div>
					</div>
				<?php endforeach?>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('ptk/sertifikasi/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		