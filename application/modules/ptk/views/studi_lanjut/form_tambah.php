	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			
			<form action="<?php echo base_url('ptk/studi_lanjut/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Studi Lanjut</legend><?php //echo($tingkat_kelas);?>
					<?php $jenjang_sekolah = get_jenjang_sekolah(); ?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama</strong>
						</label>
						<div>
							<select name="id_guru" id="id_guru" >
							<option value="0">-Nama Guru-</option>
							<?php foreach($guru as $d):?>
							
							  <option value="<?php echo $d['id_guru']?>"><?php echo $d['nama']?></option>
							
							 <?php endforeach ?>
							</select> 
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tahun Masuk</strong>
						</label>
						<div>
							<input type="text" name="tahun_masuk" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tahun Lulus</strong>
						</label>
						<div>
							<input type="text" name="tahun_lulus" value="" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama Kampus</strong>
						</label>
						<div>
							<input type="text" name="nama_kampus" value="" />
							
						</div>
					</div>
				
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenjang Pendidikan</strong>
						</label>
						<div>
							<select name="id_jenjang_pendidikan" >
							<option value="4">S1</option>
							<option value="5">S2</option>
							<option value="6">S3</option>
							</select> 
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Status</strong>
						</label>
						<div>
							<select name="status" id="status" >
							<option value="0">-Status-</option>
							<option value="1">Lulus</option>
							<option value="2">Belum Lulus</option>
							<option value="3">Gagal</option>
							</select> 
						</div>
					</div>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('ptk/studi_lanjut/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		