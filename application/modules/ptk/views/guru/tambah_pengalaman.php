
<h1 class="grid_12">Tenaga Pendidik</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Tambah Pengalaman</h2>
       </div>
		
		<form action="<?php echo base_url('ptk/guru/c_tambah_pengalaman'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Pengalaman</strong>
						</label>
						<div>
							<input type="hidden" name="id_guru" value="<?php echo $id_guru ?>" />
							<input type="text" name="pengalaman" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Lokasi</strong>
						</label>
						<div>
							<input type="text" name="lokasi" value="" />
						</div>
					</div>
		
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Mulai</strong>
						</label>
						<div>
							<input type="date" name="tanggal_mulai" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Akhir</strong>
						</label>
						<div>
							<input type="date" name="tanggal_akhir" value="" />
						</div>
					</div>

				<fieldset>
				<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							<a href="<?php echo base_url('ptk/guru/detail/'.$id_guru.'#t1-c4'); ?>"> <input value="Batal" type="button"></a>
						</div>
						
				</div>
		</form>
    </div>
 </div>
