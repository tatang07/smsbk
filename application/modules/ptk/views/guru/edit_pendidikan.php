
<h1 class="grid_12">Tenaga Pendidik</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Edit Pendidikan</h2>
       </div>
		
		<form action="<?php echo base_url('ptk/guru/c_update_pendidikan'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<?php foreach($det_pendidikan as $dp) {?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenjang Pendidikan</strong>
						</label>
						<div>
							<input type="hidden" name="id_guru" value="<?php echo $id_guru ?>" />
							<input type="hidden" name="id_guru_pendidikan" value="<?php echo $dp['id_guru_pendidikan'] ?>" />
							<!--<input type="text" name="id_jenis_jabatan" value="<?php //echo $dj['id_jenis_jabatan'] ?>" /> -->
							<select name="id_jenjang_pendidikan">
							<?php foreach($jenjang_pendidikan as $jp):?>
							  <?php if($dp['id_jenjang_pendidikan']==$jp['id_jenjang_pendidikan']){?>
								<option selected value="<?php echo $jp['id_jenjang_pendidikan']?>"><?php echo $jp['jenjang_pendidikan'] ?></option>
							  <?php }else {?>
								<option value="<?php echo $jp['id_jenjang_pendidikan']?>"><?php echo $jp['jenjang_pendidikan'] ?></option>
							  <?php } ?>
							<?php endforeach ?>
							</select> 
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Program Studi</strong>
						</label>
						<div>
							<input type="text" name="program_studi" value="<?php echo $dp['program_studi'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Instansi</strong>
						</label>
						<div>
							<input type="text" name="instansi" value="<?php echo $dp['nama_instansi'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tahun Mulai</strong>
						</label>
						<div>
							<input type="text" name="tahun_mulai" value="<?php echo $dp['tahun_mulai'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tahun Selesai</strong>
						</label>
						<div>
							<input type="text" name="tahun_selesai" value="<?php echo $dp['tahun_selesai'] ?>" />
						</div>
					</div>
					<?php } ?>
				<fieldset>
				<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							<a href="<?php echo base_url('ptk/guru/detail/'.$dp['id_guru']); ?>"> <input value="Batal" type="button"></a>
						</div>
				</div>
		</form>
    </div>
 </div>
