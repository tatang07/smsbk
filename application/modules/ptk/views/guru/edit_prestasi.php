
<h1 class="grid_12">Tenaga Pendidik</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Edit Prestasi</h2>
       </div>
		
		<form action="<?php echo base_url('ptk/guru/c_update_prestasi'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
				<?php foreach($det_prestasi as $dp) {?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Wilayah</strong>
						</label>
						<div>
							<input type="hidden" name="id_guru" value="<?php echo $id_guru ?>" />
							<input type="hidden" name="id_guru_prestasi" value="<?php echo $dp['id_guru_prestasi'] ?>" />
							<!--<input type="text" name="id_jenis_jabatan" value="<?php //echo $dp['id_jenis_jabatan'] ?>" /> -->
							<select name="id_tingkat_wilayah">
							<?php foreach($tingkat_wilayah as $tw):?>
							  <?php if($dp['id_tingkat_wilayah']==$tw['id_tingkat_wilayah']){?>
								<option selected value="<?php echo $tw['id_tingkat_wilayah']?>"><?php echo $tw['tingkat_wilayah'] ?></option>
							  <?php }else {?>
								<option value="<?php echo $tw['id_tingkat_wilayah']?>"><?php echo $tw['tingkat_wilayah'] ?></option>
							  <?php } ?>
							<?php endforeach ?>
							</select> 
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Lokasi</strong>
						</label>
						<div>
							<input type="text" name="lokasi" value="<?php echo $dp['lokasi'] ?>" />
						</div>
					</div>
		
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tahun</strong>
						</label>
						<div>
							<input type="text" name="tahun" value="<?php echo $dp['tahun'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Deskripsi</strong>
						</label>
						<div>
							<input type="text" name="deskripsi" value="<?php echo $dp['deskripsi'] ?>" />
						</div>
					</div>
					<?php } ?>
				<fieldset>
				<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							<a href="<?php echo base_url('ptk/guru/detail/'.$dp['id_guru']); ?>"> <input value="Batal" type="button"></a>
						</div>
						
				</div>
		</form>
    </div>
 </div>
