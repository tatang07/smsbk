
<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
			<?php foreach($detail as $details){?>
            <h2>Detail Tenaga Pendidik</h2>
			<div class="tabletools">
				<div class="right">
		
				<a href="<?php echo site_url('ptk/guru/edit/'.$details['id_guru'])?>"><i class="icon-plus"></i>Edit</a> 
			
				</div>
			</div>
       </div>

		<div class="content">
			<div class="dataTables_wrapper"  role="grid">    
				<div class="content" style='margin:5px;'>
					<table style="border-spacing: 0px; margin: 5px 0 20px 0; width: 96%; border: 1px solid #D2D2D2; background: #EDF2FD; margin-left: 20px;">
					<tbody>
					
						<?php $id_guru=$details['id_guru']; ?>
						<tr>
							<td style='width:200px; padding-left:20px; padding-top:10px'>Nama</td>
							<td style='width:10px; padding-left:20px; padding-top:10px'>:&nbsp;</td>
							<td style="padding-left:20px; padding-top:10px"><?php echo $details['nama']; ?></td>
							<td style="padding-right:8px; padding-left:20px; padding-top:8px" rowspan='13'>
								<?php if($details['foto']==NULL){?>
								<div class="photos">
								<li style="list-style:none">
									<img class=" foto" align="right" src= '<?php echo base_url('extras/guru/foto_default.jpg')?>' />
								</li>
								</div>
								<?php }else{?>
								<div class="photos">
								<li style="list-style:none">
									<img class="radius-border2 foto" align="right" src= '<?php echo base_url('extras/guru/'.$details['foto'])?>' />
								</li>
								</div>
								<?php }?>
								</td>
						</tr>
						<tr>
							<td style="padding-left:20px;">NIK</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php echo $details['NIK']; ?></td>							
						</tr>
						<tr>
							<td style="padding-left:20px;">NIP/NUPTK</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php echo $details['nip']; ?>/<?php echo $details['nuptk']; ?></td>							
						</tr>
						<tr>
							<td style="padding-left:20px;">Jenis Kelamin</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php if($details['jenis_kelamin']=='L' || $details['jenis_kelamin']=='l' )echo 'Laki-laki';
							else echo 'Perempuan'; ?></td>
						</tr>
						<tr>
							<td style="padding-left:20px;">Agama</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php echo $details['agama']; ?></td>
						</tr>
						<tr>
							<td style="padding-left:20px;">Status Pernikahan</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php echo $details['status_pernikahan']; ?></td>
						</tr>
						<tr>
							<td style="padding-left:20px;">Tempat Lahir dan Tanggal Lahir</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php echo $details['tempat_lahir']; ?>/<?php echo tanggal_indonesia($details['tgl_lahir']); ?></td>
						</tr>
						<tr>
							<td style="padding-left:20px;">Alamat</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php echo $details['alamat']; ?><?php //echo ", ".$details['kecamatan']; ?></td>
						</tr>
						<tr>
							<td style="padding-left:20px;">Nomor Telepon/Nomor HP</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php echo $details['hp']; ?>/<?php echo $details['telepon']; ?></td>
						</tr>
						<tr>
							<td style="padding-left:20px;">Status Kepegawaian</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php echo $details['status']; ?></td>
						</tr>
						<tr>
							<td style="padding-left:20px;">Tanggal Diangkat</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php echo tanggal_indonesia($details['tgl_diangkat']); ?></td>
						</tr>
						<tr>
							<td style="padding-left:20px;">Jabatan/Pangkat/Golongan</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php echo $details['jenis_jabatan']."-".$details['pangkat']."/".$details['golongan']; ?></td>
						</tr>
						<tr>
							<td style="padding-left:20px;">Pendidikan Terakhir</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php echo $details['jenjang_pendidikan']; ?></td>
						</tr>
						<tr>
							<td style="padding-left:20px; padding-bottom:15px">Golongan Darah</td>
							<td style="padding-left:20px;">:&nbsp;</td>
							<td style="padding-left:20px;"><?php echo $details['golongan_darah']; ?></td>
						</tr>
					
						<!--<tr>
							<td>Status Sertifikasi</td>
							<td>:&nbsp;</td>
							<td><?php //echo $details['status_sertifikasi']=="s"?"Tersertifikasi":"Belum Tersertifikasi"; ?></td>
						</tr>
				
						<tr>
							<td>Kode Guru</td>
							<td>:&nbsp;</td>
							<td><?php //echo $details['kode_guru']; ?></td>
						</tr>

						<tr>
							<td>Gaji</td>
							<td>:&nbsp;</td>
							<td><?php //echo viewUang($details['gaji']); ?></td>
						</tr>
						<tr>
							<td>Status Aktif</td>
							<td>:&nbsp;</td>
							<td><?php //if($details['status_aktif']==1) echo 'Aktif'; 
										//else echo 'Tidak Aktif'; ?></td>
						</tr>-->
		
					<?php }; ?>
					</tbody>
				</table>
				
				<div class="box tabbedBox">
						<div class="header">
							<h2>&nbsp;</h2>
							<ul>
								<li><a href="#t1-c1">Riwayat Jabatan</a></li>
								<li><a href="#t1-c2">Riwayat Penataran</a></li>
								<li><a href="#t1-c3">Riwayat Pendidikan</a></li>
								<li><a href="#t1-c4">Riwayat Pengalaman</a></li>
								<li><a href="#t1-c5">Riwayat Prestasi</a></li>
							</ul>
						</div><!-- End of .header -->
						
						<div class="content">
							<div id="t1-c1">
								<div class="tabletools">
									<div class="right">
									<a href="<?php echo site_url('ptk/guru/load_tambah_jabatan/'.$id_guru)?>"><i class="icon-plus"></i>Tambah Jabatan</a> 
									</div>
								</div>
								<div class="dataTables_wrapper"  role="grid"> 
								
								<table class="styled">
									<thead>
										<tr>
											<th>No.</th>
											<th>Jabatan</th>
											<th>Status</th>
											<th>No SK</th>
											<th>Tanggal SK</th>
											<th>TMT</th>
											<th>Keterangan</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php $no=1;?>
										<?php foreach($det_jabatan as $dj) {?>
										<tr>
											<td width='10px' class='center'><?php echo $no; $no++ ?></td>
											<td width='' class='left'><?php echo $dj['jenis_jabatan']?></td>
											<td width='' class='left'>
											<?php if($dj['status']==1) {?>
											Aktif
											<?php }else{?>
											Tidak Aktif
											<?php }?>
											</td>
											<td width='' class='left'><?php echo $dj['no_sk']; ?></td>
											<td width='50px' class='center'><?php echo tanggal_view($dj['tgl_sk']) ?></td>
											<td width='50px' class='center'><?php echo tanggal_view($dj['tmt_sk']) ?></td>
											<td width='' class='left'><?php echo $dj['keterangan']; ?></td>
											<td width='100px' class='center'>
												<a href="<?php echo site_url('ptk/guru/load_edit_jabatan/'.$id_guru.'/'.$dj['id_guru_jabatan'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
												<a href="<?php echo site_url('ptk/guru/c_delete_jabatan/'.$id_guru.'/'.$dj['id_guru_jabatan'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i  class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
										<?php }?>
									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>	
								</div>
								</div>

							</div>
							
							<div id="t1-c2">
								<div class="tabletools">
									<div class="right">
									<a href="<?php echo site_url('ptk/guru/load_tambah_penataran/'.$id_guru)?>"><i class="icon-plus"></i>Tambah Penataran</a> 
									</div>
								</div>
								<div class="dataTables_wrapper"  role="grid"> 
								<table class="styled">
									<thead>
										<tr>
											<th>No.</th>
											<th>Penataran</th>
											<th>Lokasi</th>
											<th>Tahun</th>
											<th>Deskripsi</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php $no=1;?>
									<?php foreach($det_penataran as $dp) {?>
										<tr>
											<td width='10px' class='center'><?php echo $no; $no++?></td>
											<td width='' class='left'><?php echo $dp['penataran']?></td>
											<td width='' class='left'><?php echo $dp['lokasi']?></td>
											<td width='50px' class='center'><?php echo $dp['tahun']?></td>
											<td width='' class='left'><?php echo $dp['deskripsi']?></td>
											<td width='100px' class='center'>
												<a href="<?php echo site_url('ptk/guru/load_edit_penataran/'.$id_guru.'/'.$dp['id_guru_penataran'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
												<a href="<?php echo site_url('ptk/guru/c_delete_penataran/'.$id_guru.'/'.$dp['id_guru_penataran'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
									<?php }?>
									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>	
								</div>
								</div>
						
							</div>
							
							<div id="t1-c3">
								<div class="tabletools">
									<div class="right">
									<a href="<?php echo site_url('ptk/guru/load_tambah_pendidikan/'.$id_guru)?>"><i class="icon-plus"></i>Tambah Pendidikan</a> 
									</div>
								</div>
								<div class="dataTables_wrapper"  role="grid"> 
								<table class="styled">
									<thead>
										<tr>
											<th>No.</th>
											<th>Jenjang Pendidikan</th>
											<th>Program Studi</th>
											<th>Instansi</th>
											<th>Tahun Mulai</th>
											<th>Tahun Selesai</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php $no=1;?>
									<?php foreach($det_pendidikan as $dp) {?>
										<tr>
											<td width='10px' class='center'><?php echo $no; $no++?></td>
											<td width='' class='left'><?php echo $dp['jenjang_pendidikan']?></td>
											<td width='' class='left'><?php echo $dp['program_studi']?></td>
											<td width='' class='left'><?php echo $dp['nama_instansi']?></td>
											<td width='50px' class='center'><?php echo $dp['tahun_mulai']?></td>
											<td width='50px' class='center'><?php echo $dp['tahun_selesai']?></td>
											<td width='100px' class='center'>
												<a href="<?php echo site_url('ptk/guru/load_edit_pendidikan/'.$id_guru.'/'.$dp['id_guru_pendidikan'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
												<a href="<?php echo site_url('ptk/guru/c_delete_pendidikan/'.$id_guru.'/'.$dp['id_guru_pendidikan'])?>" class="button small grey tooltip" data-gravity="s"onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
									<?php }?>
									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>	
								</div>
								</div>
								
							</div>
							<div id="t1-c4">
								<div class="tabletools">
									<div class="right">
									<a href="<?php echo site_url('ptk/guru/load_tambah_pengalaman/'.$id_guru)?>"><i class="icon-plus"></i>Tambah Pengalaman</a> 
									</div>
								</div>
								<div class="dataTables_wrapper"  role="grid"> 
								<table class="styled">
									<thead>
										<tr>
											<th>No.</th>
											<th>Pengalaman</th>
											<th>Lokasi</th>
											<th>Tanggal Mulai</th>
											<th>Tanggal Akhir</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php $no=1;?>
									<?php foreach($det_pengalaman as $dp) {?>
										<tr>
											<td width='10px' class='center'><?php echo $no; $no++?></td>
											<td width='' class='left'><?php echo $dp['pengalaman']?></td>
											<td width='' class='left'><?php echo $dp['lokasi']?></td>
										
											<td width='50px' class='center'><?php echo tanggal_view($dp['tgl_mulai'])?></td>
											<td width='50px' class='center'><?php echo tanggal_view($dp['tgl_akhir'])?></td>
											<td width='100px' class='center'>
												<a href="<?php echo site_url('ptk/guru/load_edit_pengalaman/'.$id_guru.'/'.$dp['id_guru_pengalaman'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
												<a href="<?php echo site_url('ptk/guru/c_delete_pengalaman/'.$id_guru.'/'.$dp['id_guru_pengalaman'])?>" class="button small grey tooltip" data-gravity="s"onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
									<?php }?>
									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>	
								</div>
								</div>
							
							</div>
							<div id="t1-c5">
							<div class="tabletools">
									<div class="right">
									<a href="<?php echo site_url('ptk/guru/load_tambah_prestasi/'.$id_guru)?>"><i class="icon-plus"></i>Tambah Prestasi</a> 
									</div>
								</div>
								<table class="styled">
									<thead>
										<tr>
											<th>No.</th>
											<th>Wilayah</th>
											<th>Lokasi</th>
											<th>Tahun</th>
											<th>Deskripsi</th>
											<th>Aksi</th>
										</tr>
									</thead>
									<tbody>
									<?php $no=1;?>
									<?php foreach($det_prestasi as $dp) {?>
										<tr>
											<td width='10px' class='center'><?php echo $no; $no++?></td>
											<td width='' class='left'><?php echo $dp['tingkat_wilayah']?></td>
											<td width='' class='left'><?php echo $dp['lokasi']?></td>
										
											<td width='50px' class='center'><?php echo $dp['tahun']?></td>
											<td width='' class='left'><?php echo $dp['deskripsi']?></td>
											<td width='100px' class='center'>
												<a href="<?php echo site_url('ptk/guru/load_edit_prestasi/'.$id_guru.'/'.$dp['id_guru_prestasi'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
												<a href="<?php echo site_url('ptk/guru/c_delete_prestasi/'.$id_guru.'/'.$dp['id_guru_prestasi'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
											</td>
										</tr>
									<?php }?>
									</tbody>
								</table>
								<div class="footer">
									<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>	
								</div>
								</div>
								
							</div>
						</div><!-- End of .content -->
					</div><!-- End of .box -->
				</div>
			</div>
        </div>        
    </div>
 </div>
<script>

	$(document).ready(function() {  
  
	  
	$('.photos img').css('opacity', 0.7);  

	$('.photos li').hover(  	
	   function(){  
		  $(this).find('img').stop().fadeTo('slow', 1);  
	   },  
	   function(){  
		  $(this).find('img').stop().fadeTo('slow', 0.7);  
	   });  
	  
	}); 
</script>