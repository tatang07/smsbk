
<h1 class="grid_12">Tenaga Pendidik</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Tambah Prestasi</h2>
       </div>
		
		<form action="<?php echo base_url('ptk/guru/c_tambah_prestasi'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Wilayah</strong>
						</label>
						<div>
							
							<select name="id_tingkat_wilayah">
							<?php foreach($tingkat_wilayah as $tw): ?>
							  <option value="<?php echo $tw['id_tingkat_wilayah']?>"> <?php echo $tw['tingkat_wilayah']?></option>
							<?php endforeach ?>
							</select> 
							<input type="hidden" name="id_guru" value="<?php echo $id_guru ?>" />	
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Lokasi</strong>
						</label>
						<div>
							<input type="text" name="lokasi" value="" />
						</div>
					</div>
		
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tahun</strong>
						</label>
						<div>
							<input type="text" name="tahun" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Deskripsi</strong>
						</label>
						<div>
							<input type="text" name="deskripsi" value="" />
						</div>
					</div>

				<fieldset>
				<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							<a href="<?php echo base_url('ptk/guru/detail/'.$id_guru.'#t1-c5'); ?>"> <input value="Batal" type="button"></a>
						</div>
						
				</div>
		</form>
    </div>
 </div>
