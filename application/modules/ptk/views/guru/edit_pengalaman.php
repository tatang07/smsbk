
<h1 class="grid_12">Tenaga Pendidik</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Edit Pengalaman</h2>
       </div>
       
	
		
		<form action="<?php echo base_url('ptk/guru/c_update_pengalaman'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<?php foreach($det_pengalaman as $p):?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Pengalaman</strong>
						</label>
						<div>
							<input type="hidden" name="id_guru" value="<?php echo $id_guru ?>" />
							<input type="hidden" name="id_guru_pengalaman" value="<?php echo $p['id_guru_pengalaman'] ?>" />
							<input type="text" name="pengalaman" value="<?php echo $p['pengalaman'] ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Lokasi</strong>
						</label>
						<div>
							<input type="text" name="lokasi" value="<?php echo $p['lokasi']?>" />
						</div>
					</div>
		
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Mulai</strong>
						</label>
						<div>
							<input type="date" name="tanggal_mulai" value="<?php echo tanggal_view($p['tgl_mulai']);?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Akhir</strong>
						</label>
						<div>
							<input type="date" name="tanggal_akhir" value="<?php echo tanggal_view($p['tgl_akhir']);?>" />
						</div>
					</div>
				<?php endforeach; ?>
				<fieldset>
				<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							<a href="<?php echo base_url('ptk/guru/detail/'.$p['id_guru']); ?>"> <input value="Batal" type="button"></a>
						</div>
						
				</div>
		</form>
    </div>
 </div>
