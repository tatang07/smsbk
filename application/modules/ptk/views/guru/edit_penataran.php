
<h1 class="grid_12">Tenaga Pendidik</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Edit Penataran</h2>
       </div>
		
		<form action="<?php echo base_url('ptk/guru/c_update_penataran'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<?php foreach($det_penataran as $dp) {?>
						<div class="row">
							<label for="f1_normal_input">
								<strong>Penataran</strong>
							</label>
							<div>
								<input type="text" name="penataran" value="<?php echo $dp['penataran'] ?>" />
							</div>
						</div>
						<div class="row">
							<label for="f1_normal_input">
								<strong>Lokasi</strong>
							</label>
							<div>
								<input type="text" name="lokasi" value="<?php echo $dp['lokasi'] ?>" />
							</div>
						</div>
						<div class="row">
							<label for="f1_normal_input">
								<strong>Tahun</strong>
							</label>
							<div>
								<input type="date" name="tahun" value="<?php echo $dp['tahun'] ?>" />
							</div>
						</div>
						<div class="row">
							<label for="f1_normal_input">
								<strong>Deskripsi</strong>
							</label>
							<div>
								<input type="text" name="deskripsi" value="<?php echo $dp['deskripsi'] ?>" />
							</div>
						</div>

						<input type="hidden" name="id_guru" value="<?php echo $dp['id_guru'] ?>" />
						<input type="hidden" name="id_guru_penataran" value="<?php echo $dp['id_guru_penataran'] ?>" />
					<?php } ?>
				<fieldset>
				<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							<a href="<?php echo base_url('ptk/guru/detail/'.$dp['id_guru']); ?>"> <input value="Batal" type="button"></a>
						</div>
						
				</div>
		</form>
    </div>
 </div>
