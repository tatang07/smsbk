
<?php //array("id_kecamatan","foto","status_aktif","status_sertifikasi"),?>
        <?php $agama = get_list("r_agama","id_agama","agama") ?>
        <?php $pernikahan = get_list("r_status_pernikahan","id_status_pernikahan","status_pernikahan") ?>
        <?php $goldar = get_list("r_golongan_darah","id_golongan_darah","golongan_darah") ?>
        <?php $speg = get_list("r_status_pegawai","id_status_pegawai","status") ?>
        <?php $jabatan = get_list("r_jenis_jabatan","id_jenis_jabatan","jenis_jabatan") ?>
        <?php $pangkat = get_list("r_pangkat_golongan","id_pangkat_golongan","pangkat") ?>
        <?php $jpend = get_list("r_jenjang_pendidikan","id_jenjang_pendidikan","jenjang_pendidikan") ?>
        <?php $jpend = get_list("r_jenjang_pendidikan","id_jenjang_pendidikan","jenjang_pendidikan") ?>
		<?php $kguru = get_list("r_kualifikasi_guru","id_kualifikasi_guru","kualifikasi_guru") ?>
		<div class='row'>
            <?php echo simple_form_input2(array('name'=>'item[nama]', 'id'=>'nama', 'label'=>'Nama Lengkap', 'class'=>'required','class_div'=>'_50')) ?>
			<?php echo simple_form_input2(array('name'=>'item[NIK]', 'id'=>'NIK', 'label'=>'NIK', 'class'=>'required','class_div'=>'_50')) ?>
        </div>    
		<div class='row'>
			<?php echo simple_form_input2(array('name'=>'item[nip]', 'id'=>'nip', 'label'=>'NIP', 'class'=>'required','class_div'=>'_50' ,'type'=>'digits')) ?>
            <?php echo simple_form_input2(array('name'=>'item[nuptk]', 'id'=>'nuptk', 'label'=>'NUPTK', 'class'=>'required','class_div'=>'_25' ,'type'=>'digits')) ?>
            <?php echo simple_form_input2(array('name'=>'item[kode_guru]', 'id'=>'kode_guru', 'label'=>'Kode Guru', 'class'=>'required','class_div'=>'_25')) ?>
		</div>
		<div class='row'>
            <?php echo simple_form_dropdown2('item[jenis_kelamin]',array("L"=>"Laki-Laki","P"=>"Perempuan"),null,"",array('label'=>'Jenis Kelamin','class_div'=>'_25')) ?>
            <?php echo simple_form_dropdown2('item[id_golongan_darah]',$goldar,null,"",array('label'=>'Golongan Darah','class_div'=>'_25')) ?>
            <?php echo simple_form_dropdown2('item[id_agama]',$agama,null,"",array('label'=>'Agama','class_div'=>'_25')) ?>
            <?php echo simple_form_dropdown2('item[id_status_pernikahan]',$pernikahan,null,"",array('label'=>'Status Pernikahan','class_div'=>'_25')) ?>
        </div>
		<div class='row'>
            <?php echo simple_form_input2(array('name'=>'item[tempat_lahir]', 'id'=>'tempat_lahir', 'label'=>'Tempat Lahir', 'class'=>'required','class_div'=>'_50')) ?>
            <?php echo simple_form_input2(array('name'=>'item[tgl_lahir]', 'id'=>'tgl_lahir', 'label'=>'Tanggal Lahir', 'class'=>'required','class_div'=>'_50', 'type'=>'date')) ?>
        </div> 
		<div class='row'>
            <?php echo simple_form_input2(array('name'=>'item[alamat]', 'id'=>'alamat', 'label'=>'Alamat Rumah',  'class'=>'required', 'class_div'=>'_100')) ?>
        </div>
		<div class='row'>
            <?php echo simple_form_input2(array('name'=>'item[telepon]', 'id'=>'telepon', 'label'=>'No. Telepon',  'class'=>'novalidate', 'class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'item[hp]', 'id'=>'hp', 'label'=>'No. HP',  'class'=>'novalidate','class_div'=>'_25')) ?>
            <?php echo simple_form_input2(array('name'=>'item[e_mail]', 'id'=>'email', 'label'=>'Email',  'class'=>'novalidate','class_div'=>'_50')) ?>
        </div>
		<div class='row'>
            <?php echo simple_form_dropdown2('item[id_status_pegawai]',$speg,null,"",array('label'=>'Status Pegawai','class_div'=>'_25')) ?>
			<?php echo simple_form_input2(array('name'=>'item[tgl_diangkat]', 'id'=>'tgl_diangkat', 'label'=>'Tanggal Diangkat', 'class'=>'required','class_div'=>'_25', 'type'=>'date')) ?>
            <?php echo simple_form_dropdown2('item[id_jenis_jabatan]',$jabatan,null,"",array('label'=>'Jabatan','class_div'=>'_25')) ?>
            <?php echo simple_form_dropdown2('item[id_pangkat_golongan]',$pangkat,null,"",array('label'=>'Pangkat-Golongan','class_div'=>'_25')) ?>            
        </div>
		<div class='row'>
            <?php echo simple_form_dropdown2('item[id_jenjang_pendidikan]',$jpend,null,"",array('label'=>'Pendidikan Terakhir','class_div'=>'_25')) ?>         
			<?php echo simple_form_input2(array('name'=>'item[gaji]', 'id'=>'gaji', 'label'=>'Gaji Pokok', 'class'=>'required','class_div'=>'_25', 'type'=>'number')) ?>
			<?php echo simple_form_dropdown2('item[status_aktif]',array("1"=>"Aktif","2"=>"Tidak Aktif"),null,"",array('label'=>'Status Aktif','class_div'=>'_25')) ?>
			<?php echo simple_form_dropdown2('item[status_sertifikasi]',array("Belum Tersertifikasi"=>"Belum Tersertifikasi","Tersertifikasi"=>"Tersertifikasi"),null,"",array('label'=>'Status Sertifikasi','class_div'=>'_25')) ?>
        </div>
		<div class='row'>
            <?php echo simple_form_dropdown2('item[id_kualifikasi_guru]',$kguru,null,"",array('label'=>'Kualifikasi Guru','class_div'=>'_25')) ?>    
        </div>
		
		<?php echo simple_form_input(array('name'=>'item[foto]','type'=>'file', 'id'=>'foto', 'label'=>'Foto')) ?>
		
		<?php echo form_hidden('item[id_guru]',isset($_POST['item']['id_guru'])?$_POST['item']['id_guru']:"") ?>
        