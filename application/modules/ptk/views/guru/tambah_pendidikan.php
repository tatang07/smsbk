
<h1 class="grid_12">Tenaga Pendidik</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Tambah Pendidikan</h2>
       </div>
		
		<form action="<?php echo base_url('ptk/guru/c_tambah_pendidikan'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenjang Pendidikan</strong>
						</label>
						<div>
							
							<select name="id_jenjang_pendidikan">
							<?php foreach($jenjang_pendidikan as $jp): ?>
							  <option value="<?php echo $jp['id_jenjang_pendidikan']?>"> <?php echo $jp['jenjang_pendidikan']?></option>
							<?php endforeach ?>
							</select> 
							<input type="hidden" name="id_guru" value="<?php echo $id_guru ?>" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Program Studi</strong>
						</label>
						<div>
							<input type="text" name="program_studi" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Instansi</strong>
						</label>
						<div>
							<input type="text" name="instansi" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tahun Mulai</strong>
						</label>
						<div>
							<input type="text" name="tahun_mulai" value="" />
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tahun Selesai</strong>
						</label>
						<div>
							<input type="text" name="tahun_selesai" value="" />
						</div>
					</div>
					
				<fieldset>
				<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							<a href="<?php echo base_url('ptk/guru/detail/'.$id_guru.'#t1-c3'); ?>"> <input value="Batal" type="button"></a>
						</div>
				</div>
		</form>
    </div>
 </div>
