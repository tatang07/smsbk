<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Penugasan dan Penentuan Beban Mengajar</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i><img src="http://localhost/smsbkv2//theme/img/icons/packs/iconsweets2/14x14/excel-document.png"></i>Excel</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
					<table>
						<tr>
							
							<th width=1px align='left'><span class="text">Kriteria:</span></th>
							<td width=100px align='left'><select>
									<option value=''>Nama</option>
									<option value=''>Beban Mengajar</option>
									<option value=''>Pendidikan</option>
									<option value=''>Jumlah Mata Pelajaran</option>
									</select>
								</td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>
							<th width=75px align='left'><span class="text">Kata Kunci:</span></th>
							<td width=100px align='left'><input type='text'></td>
							<td width=1px>&nbsp;</td>
							<td width=1px>&nbsp;</td>							
							<td width=1px>
								<a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
							</td>
						</tr>
					</table>
                </form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Guru</th>
								<th>Beban Mengajar</th>
								<th>Pendidikan</th>
								<th>Jumlah Mata Pelajaran</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<pre>
						<?php print_r($data)?>
						</pre>
						<tbody>
							<?php $bebanguru = array(); ?>
							<?php $no=1?>
							<?php foreach ($data as $d) {?>
							<tr>
								<?php $bebanguru[$d['id_guru']] = $d; 
								$bebanguru[$d['id_guru']]['pelajaran'][$d['id_pelajaran']][$d['id_tingkat_kelas']] = 
								isset($bebanguru[$d['id_guru']]['pelajaran'][$d['id_pelajaran']][$d['id_tingkat_kelas']])
								? $bebanguru[$d['id_guru']]['pelajaran'][$d['id_pelajaran']][$d['id_tingkat_kelas']] + 1
								: 1;
								?>
								
								<td width='30px' class='center'><?php echo $no ?></td>
								<td width='400px'><?php echo $d['nama']; ?></td>
								<?php $hasil=0; ?>
								<td width='100px' class='center'>
								<?php foreach($bebanguru as $id_guru => &$b){
									$totalbeban = 0;
									foreach($b['pelajaran'] as $id_pel => $pel){
										$jumlah_jam = $this->m_penugasan_dan_penentuan_beban_mengajar->get_jumlah_jam($id_pel, array_keys($pel));
										// print_r($jumlah_jam);
										foreach($jumlah_jam as $j){
											$totalbeban += $j['jumlah_jam'] * $pel[$j['id_tingkat_kelas']];
										}
										}//echo $totalbeban ;
										$hasil = $hasil + $totalbeban;	
									}?>
								<?php //echo $hasil;?>
								</td>
								<td width='100px' class='center'><?php echo $d['jenjang_pendidikan']; ?></td>
								<td width='100px' class='center'><?php echo $d['s']; ?></td>	
								<td width='' class='center'><a class="button small grey tooltip" data-gravity="s"><i class="icon-list"></i> Jadwal</a></td>
							</tr>
							<?php $no=$no+1 ?>
							<?php } ?>
						</tbody>
					</table>
					<pre>
					<?php print_r($jumlah_jam) ?>
					</pre>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: 20</div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
