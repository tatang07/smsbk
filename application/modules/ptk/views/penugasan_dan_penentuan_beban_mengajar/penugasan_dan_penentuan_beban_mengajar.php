hahahah<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Penugasan dan Penentuan Beban Mengajar</h2>
       </div>
       <div class="tabletools">
			
			
            <div class="dataTables_filter">
                <form action="<?php echo base_url('ptk/penugasan_dan_penentuan_beban_mengajar/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="40px" align="left"><span class="text">Nama</span></th>
								<td align="left"><input type="text" name="nama" id="nama"></td>
								<td width="10px"></td>
							
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama Guru</th>
								<th>Beban Mengajar</th>
								<th>Pendidikan</th>
								<th>Jumlah Mata Pelajaran</th>
							
							</tr>
						</thead>
					
						<tbody>
							<?php $bebanguru = array(); ?>
							<?php $no=1?>
							
							<?php foreach ($data as $d) {?>
							<tr>
							
								
								<td width='30px' class='center'><?php echo $no ?></td>
								<td width='400px'><?php echo $d['nama']; ?></td>
								<?php $hasil=0; ?>
								<td width='100px' class='center'>
								
								<?php echo $d['jml']; ?>
								</td>
								<td width='100px' class='center'><?php echo $d['jenjang_pendidikan']; ?></td>
								<td width='100px' class='center'><?php echo $d['s']; ?></td>	
								
							</tr>
							<?php $no=$no+1 ?>
							<?php } ?>
						</tbody>
					</table>
			
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1;?></div>
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
