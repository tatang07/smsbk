<?php //print_r($data_asuransi)?>
<h1 class="grid_12">Guru</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Asuransi</h2>
       </div>

		<div class="tabletools">
			<div class="right">
				<?php 
				$gid=get_usergroup();
				
				if($gid!=9){?>
			  	<a href="<?php echo base_url('ptk/asuransi/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php }elseif($gid==9){?>
				
				<?php }?>
			  <br><br>
            </div>
        </div>
		
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jenis Asuransi</th>
								
								
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($data_asuransi as $dt):?>
							<tr>
								<td width='' class='center'><?php echo $no; $no++;?></td>
								<td width='' class='right'><?php echo $dt['asuransi'];?></td>								
								
								
						
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
						
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
