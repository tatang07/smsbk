<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
	<form action="" class="grid_12 validate no-box" novalidate="novalidate" method="post" id="valid" enctype="multipart/form-data">
		<fieldset>
			<legend>Penambahan Data Supervisi</legend>

			<div class="row">
				<label><strong>Guru</strong></label>
				<div>
					<select name="id_guru" id="id_guru">
						<option value="">- pilih guru -</option>
						<?php foreach ($guru_gmr as $guru): ?>
							<option value="<?php echo $guru['id_guru']; ?>" <?php if(isset($id_guru) && $id_guru == $guru['id_guru']) echo 'selected="selected"'; ?>><?php echo $guru['nama']; ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>

			<?php if ($id_guru): ?>
				
			<div class="row">
				<label><strong>Kelas - Pelajaran</strong></label>
				<div>
					<select name="id_gmr" id="id_gmr">
						<option value="">- pilih kelas &amp; pelajaran -</option>
						<?php foreach ($matpel_rombel as $id_gmr => $label_gmr): ?>
							<option value="<?php echo $id_gmr; ?>"><?php echo $label_gmr; ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>

			<div class="row">
				<label><strong>Kode Dokumen</strong></label>
				<div>
					<input type="text" name="kode_supervisi" value="" id="kode_supervisi" class="novalidate">
				</div>
			</div>

			<div class="row">
				<label><strong>Tanggal</strong></label>
				<div>
					<input type="date" name="waktu" value="" id="waktu" class="novalidate">
				</div>
			</div>

			<div class="row">
				<label><strong>Tanggal Terbit</strong></label>
				<div>
					<input type="date" name="tanggal_terbit" value="" id="tanggal_terbit" class="novalidate">
				</div>
			</div>

			<div class="row">
				<label><strong>Materi</strong></label>
				<div>
					<input type="text" name="materi" value="" id="materi" class="novalidate">
				</div>
			</div>

			<div class="row">
				<label><strong>Edisi</strong></label>
				<div>
					<input type="text" name="edisi" value="" id="edisi" class="novalidate">
				</div>
			</div>	
		</fieldset>

		<br>

		<fieldset>
			<legend>Butir Supervisi</legend>

			<table class="styled">
				<thead>
					<tr>
						<th rowspan="2">No</th>
						<th rowspan="2">Butir Supervisi</th>
						<th colspan="3">Hasil</th>
						<th rowspan="2">Keterangan</th>
					</tr>
					<tr>
						<th>A</th>
						<th>B</th>
						<th>C</th>
					</tr>
				</thead>
				<tbody>
					<?php $i = 1; foreach ($butir_supervisi as $field): ?>
					<tr>
						<td><?php echo $i; ?></td>
						<td><?php echo $field['butir_supervisi']; ?></td>
						<td style="text-align:center"><input type="radio" name="nilai[<?php echo $field['id_butir_supervisi'] ?>]" value="A"></td>
						<td style="text-align:center"><input type="radio" name="nilai[<?php echo $field['id_butir_supervisi'] ?>]" value="B"></td>
						<td style="text-align:center"><input type="radio" name="nilai[<?php echo $field['id_butir_supervisi'] ?>]" value="C"></td>
						<td><input type="text" name="keterangan[<?php echo $field['id_butir_supervisi'] ?>]" style="width:96%"></td>
					</tr>
					<?php $i++; endforeach; ?>
				</tbody>
			</table>

			<div class="row">
				<label><strong>Kesimpulan dan Saran</strong></label>
				<div>
					<textarea name="kesimpulan" id="kesimpulan" rows="3"></textarea>
				</div>
			</div>
			<?php endif ?>
		</fieldset>

		<div class="actions">
			<div class="left">
				<input type="submit" value="Simpan">
				<a href="<?php echo base_url('ptk/supervisi'); ?>" class="button"> Batal</a>
			</div>
		</div>
	</form>
</div>

<script>
	$('#id_guru').chosen();
	$('#id_guru').chosen().change(function(){
		window.location = '<?php echo site_url("ptk/supervisi/tambah"); ?>/' + $(this).val();
	});
</script>
<style>
	.radiobutton {margin:0;}
</style>