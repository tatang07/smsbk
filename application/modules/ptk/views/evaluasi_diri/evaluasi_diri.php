<?php  
	$statuspil = 0;
	if(isset($_POST['id_guru'])){
	$a = $_POST['id_guru'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Pendidik Dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Sistem Kerja Pegawai</h2>
       </div>
	
       <div class="tabletools">
			<div class="right">
				<?php $gid=get_usergroup();?>
				
			  	<a href="<?php echo base_url('ptk/evaluasi_diri/load_add/'.$statuspil); ?>"><i class="icon-plus"></i>Tambah</a> 
			
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="110px" align="left"><span class="text">Guru</span></th>
								<td width="200px" align="left">
								<select name="id_guru" id="id_guru" onchange="submitform();">
									<option value="0">-Nama Guru-</option>
									<?php foreach($guru as $q): ?>
										<?php if($q['id_guru'] == $statuspil){ ?>
											<option selected value='<?php echo $q['id_guru']; ?>' data-status-pilihan="<?php echo $q['id_guru']; ?>"><?php echo $q['nama']; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q['id_guru']; ?>' data-status-pilihan="<?php echo $q['id_guru']; ?>"><?php echo $q['nama']; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
								</td>
								
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Periode Penilaian Penilai</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
							<?php 
								$this->load->model('ptk/m_evaluasi_diri');
								$data['isi'] = $this->m_evaluasi_diri->get_data($statuspil)->result_array();
								$isi=$data['isi'];
							?>
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
								<?php //print_r($isi)?>
								<tr>
									<td class='center'><?php echo $no; $no++;?></td>
									<td class='center'><?php  echo tanggal_indonesia($d['tgl_mulai'])?> - <?php  echo tanggal_indonesia($d['tgl_akhir'])?> </td>
									<td class='center'>
										<a href="<?php echo base_url('ptk/evaluasi_diri/download/'.$d['id_guru_evaluasi'])?>" original-title="" class="button small grey tooltip" data-gravity="s"><i class="icon-print"></i> Download</a>
										
										<a href="<?php echo site_url('ptk/evaluasi_diri/delete/'.$d['id_guru_evaluasi'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>
									
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>