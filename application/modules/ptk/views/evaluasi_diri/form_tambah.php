<h1 class="grid_12">Pendidik Dan Tenaga Kependidikan</h1>
  	<form action="<?php echo base_url('ptk/evaluasi_diri/simpan'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
		<fieldset>
			<legend>Tambah Sistem Kerja Pegawai</legend>
			
			<div class="row">
				<label for="f1_normal_input">
					<strong>Periode Mulai</strong>
				</label>
				<div>
					<input type="date" name="tgl_mulai" value="" required/>
				</div>
			</div>

			<div class="row">
				<label for="f1_normal_input">
					<strong>Periode Akhir</strong>
				</label>
				<div>
					<input type="date" name="tgl_akhir" value="" required/>
				</div>
			</div>

			<div class="row">
				<label for="f1_textarea">
					<strong>File</strong>
				</label>
				<div>
					<input type="file" name="userfile" value="" required/>
				</div>
			</div>

			<input type="hidden" name="id_guru" value="<?php echo $id_guru ?>">
		</fieldset>
		
		<div class="actions">
			<div class="left">
				<input type="submit" value="Simpan" name=send />
				 <a href="<?php echo base_url('ptk/evaluasi_diri/index/'); ?>"> <input value="Batal" type="button"></a>
			</div>
			<div class="right">
			</div>
		</div><!-- End of .actions -->
	</form><!-- End of .box -->
</div><!-- End of .grid_4 -->
