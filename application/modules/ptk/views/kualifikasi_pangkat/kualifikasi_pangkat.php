<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Proporsi Golongan/Pangkat</h2>
       </div>
	   <div class="box">
	   <div class="content" style="height: 250px;">
						<table class=chart data-type=pie data-donut=0>
							<thead>
								<?php foreach ($data as $d) {?>
								<tr>
									<th><?php echo $d->golongan; ?></th>
							
								</tr>
								<?php } ?>
							</thead>
							
							<tbody>
							<?php foreach ($data as $d) {?>
								<tr>
									<th><?php echo $d->golongan; ?></th>
									<td><?php echo $d->s; ?></td>
								</tr>	
							<?php } ?>			
							</tbody>	
						</table>
					</div><!-- End of .content -->
				</div><!-- End of .box -->
	   
	   
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Golongan Guru</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1?>
							<?php foreach ($data as $d) {?>
							<tr>
								<td width='' class='center'><?php echo $no ?></td>
								<td width='' class='center'><?php echo $d->golongan; ?><br/></td>
								<td width='' class='center'><?php echo $d->s; ?></td>
								<?php $no=$no+1 ?>
							</tr>
							<?php }?>							
						</tbody>
					</table>
					
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
