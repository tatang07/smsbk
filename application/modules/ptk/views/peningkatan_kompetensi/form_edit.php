	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			
			<form action="<?php echo base_url('ptk/peningkatan_kompetensi/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Sertifikasi</legend>
					<?php foreach($data_edit as $d):?>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama Program</strong>
						</label>
						<div>
							<input type="text" name="nama_program" value="<?php echo $d['nama_program']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Mulai</strong>
						</label>
						<div>
							<input type="date" name="tanggal_mulai" value="<?php echo tanggal_view($d['tanggal_mulai']);?>" />
							<input type="hidden" name="id" value="<?php echo $d['id_guru_peningkatan_kompetensi']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Akhir</strong>
						</label>
						<div>
							<input type="date" name="tanggal_selesai" value="<?php echo tanggal_view($d['tanggal_selesai'])?>" />
							
						</div>
					</div>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Penanggung Jawab</strong>
						</label>
						<div>
					
							
							<input disabled type="text" name="nama" value="<?php echo $d['nama']?>" />
							<input type="hidden" name="id_guru" value="<?php echo $d['id_guru'];?>" />
						</div>
					</div>
				<?php endforeach?>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('ptk/peningkatan_kompetensi/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		