<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
	<form action="" class="grid_12 validate no-box" novalidate="novalidate" method="post" id="valid" enctype="multipart/form-data">
		<fieldset>
			<legend>Penambahan Data Penilaian Pelaksanaan Pekerjaan Pegawai Negeri Sipil (DPPP)</legend>
			<div class="row">
				<label><strong>Periode Mulai</strong></label>
				<div>
					<input type="date" name="periode_mulai" value="" id="periode_mulai" class="novalidate">
				</div>
			</div>
			<div class="row">
				<label><strong>Periode Akhir</strong></label>
				<div>
					<input type="date" name="periode_akhir" value="" id="periode_akhir" class="novalidate">
				</div>
			</div>

		<fieldset>
			<legend>Data Guru</legend>
			<div class="row">
				<label><strong>Guru</strong></label>
				<div>
					<select name="id_guru" id="id_guru">
						<option value="">- pilih guru -</option>
						<?php foreach ($guru as $guru): ?>
							<option value="<?php echo $guru['id_guru']; ?>" <?php if(isset($id_guru) && $id_guru == $guru['id_guru']) echo 'selected="selected"'; ?>><?php echo $guru['nama']; ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>

			<?php if ($id_guru): ?>

			<div class="row">
				<label><strong>Jabatan</strong></label>
				<div>
					<input type="text" name="jabatan" value="" id="jabatan" class="novalidate">
				</div>
			</div>
			<div class="row">
				<label><strong>Unit Organisasi</strong></label>
				<div>
					<input type="text" name="unit_organisasi" value="" id="unit_organisasi" class="novalidate">
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Data Pejabat Penilai</legend>

			<div class="row">
				<label><strong>Nama Pejabat Penilai</strong></label>
				<div>
					<input type="text" name="pejabat_penilai" value="" id="pejabat_penilai" class="novalidate">
				</div>
			</div>
			<div class="row">
				<label><strong>NIP</strong></label>
				<div>
					<input type="text" name="nip_penilai" value="" id="nip_penilai" class="novalidate">
				</div>
			</div>
			<div class="row">
				<label><strong>Pangkat dan Golongan Penilai</strong></label>
				<div>
					<select name="id_pangkat_golongan_penilai">
						<option value="">- pilih pangkat golongan penilai -</option>
						<?php foreach ($golpang as $id_golpang => $val): ?>
							<option value="<?php echo $id_golpang; ?>"><?php echo $val; ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="row">
				<label><strong>Jabatan/Pekerjaan Penilai</strong></label>
				<div>
					<input type="text" name="jabatan_penilai" value="" id="jabatan_penilai" class="novalidate">
				</div>
			</div>
			<div class="row">
				<label><strong>Unit Organisasi Penilai</strong></label>
				<div>
					<input type="text" name="unit_organisasi_penilai" value="" id="unit_organisasi_penilai" class="novalidate">
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Data Atasan Pejabat Penilai</legend>

			<div class="row">
				<label><strong>Nama Atasan</strong></label>
				<div>
					<input type="text" name="atasan" value="" id="atasan" class="novalidate">
				</div>
			</div>
			<div class="row">
				<label><strong>NIP</strong></label>
				<div>
					<input type="text" name="nip_atasan" value="" id="nip_atasan" class="novalidate">
				</div>
			</div>
			<div class="row">
				<label><strong>Pangkat dan Golongan Atasan</strong></label>
				<div>
					<select name="id_pangkat_golongan_atasan">
						<option value="">- pilih pangkat golongan atasan -</option>
						<?php foreach ($golpang as $id_golpang => $val): ?>
							<option value="<?php echo $id_golpang; ?>"><?php echo $val; ?></option>
						<?php endforeach ?>
					</select>
				</div>
			</div>
			<div class="row">
				<label><strong>Jabatan/Pekerjaan Atasan</strong></label>
				<div>
					<input type="text" name="jabatan_atasan" value="" id="jabatan_atasan" class="novalidate">
				</div>
			</div>
			<div class="row">
				<label><strong>Unit Organisasi Atasan</strong></label>
				<div>
					<input type="text" name="unit_organisasi_atasan" value="" id="unit_organisasi_atasan" class="novalidate">
				</div>
			</div>
		</fieldset>

		<fieldset>
			<legend>Penilaian</legend>

			<div class="row">
				<table class="styled">
					<thead>
						<tr>
							<!-- <th>No</th> -->
							<th>Unsur yang dinilai</th>
							<th>Angka</th>
							<th>Keterangan</th>
						</tr>
					</thead>
					<tbody>
						<?php $i = 1; foreach ($butir_dppp as $field): ?>
						<tr>
							<!-- <td><?php echo $i; ?></td> -->
							<td><?php echo $field['butir_dppp']; ?></td>
							<td><input type="text" name="angka[<?php echo $field['id_butir_dppp'] ?>]" style="width:96%"></td>
							<td><input type="text" name="keterangan[<?php echo $field['id_butir_dppp'] ?>]" style="width:96%"></td>
						</tr>
						<?php $i++; endforeach; ?>
					</tbody>
				</table>
			</div>
		</fieldset>

		<fieldset>
			<legend>Kesimpulan</legend>

			<div class="row">
				<label><strong>KEBERATAN DARI PEGAWAI NEGERI SIPIL <br>YANG DINILAI (APABILA ADA)</strong></label>
				<div>
					<textarea name="keberatan" id="keberatan" rows="3"></textarea>
				</div>
			</div>
			<div class="row">
				<label><strong>TANGGAPAN PEJABAT PENILAI ATAS KEBERATAN</strong></label>
				<div>
					<textarea name="tanggapan" id="tanggapan" rows="3"></textarea>
				</div>
			</div>
			<div class="row">
				<label><strong>KEPUTUSAN ATASAN PEJABAT PENILAI KEBERATAN</strong></label>
				<div>
					<textarea name="keputusan" id="keputusan" rows="3"></textarea>
				</div>
			</div>
			<div class="row">
				<label><strong>LAIN-LAIN</strong></label>
				<div>
					<textarea name="lain_lain" id="lain_lain" rows="3"></textarea>
				</div>
			</div>
			<?php endif ?>
		</fieldset>

		<div class="actions">
			<div class="left">
				<input type="submit" value="Simpan">
				<a href="<?php echo base_url('ptk/dp3_guru'); ?>" class="button grey"> Batal</a>
			</div>
		</div>
	</form>
</div>

<script>
	$('#id_guru').chosen();
	$('#id_guru').chosen().change(function(){
		window.location = '<?php echo site_url("ptk/dp3_guru/tambah"); ?>/' + $(this).val();
	});
</script>
<style>
	.radiobutton {margin:0;}
</style>