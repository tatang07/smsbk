<?php
	function cek_sebutan($angka){
		$sebutan = 'Kurang';
		if($angka > 90) $sebutan = 'Amat baik';
		else if($angka > 75) $sebutan = 'Baik';
		else if($angka > 60) $sebutan = 'Cukup';
		else if($angka > 50) $sebutan = 'Sedang';
		return $sebutan;
	}
?>

<h1 class="grid_12">Kepemimpinan</h1>
<div class="grid_12">
	<div class="box with-table">
		<div class="header">
			<h2>Detail Supervisi</h2>
		</div>

		<table class="styled">
			<tbody>
				<tr>
					<th style="width:100px !important">Periode Penilaian</th>
					<td><?php echo date("F Y", strtotime($dp3_guru['periode_mulai'])); ?> - <?php echo date("F Y", strtotime($dp3_guru['periode_akhir'])); ?></td>
				</tr>
				<tr>
					<td colspan="2"></td>
				</tr>
				<tr>
					<th style="width:100px !important">Nama Guru</th>
					<td><strong><?php echo $dp3_guru['nama_guru']; ?></strong></td>
				</tr>
				<tr>
					<th style="width:100px !important">NIP</th>
					<td><?php echo $dp3_guru['nip']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Jabatan</th>
					<td><?php echo $dp3_guru['jabatan']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Unit Organisasi</th>
					<td><?php echo $dp3_guru['unit_organisasi']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Pangkat/Golongan</th>
					<td><?php echo $dp3_guru['pangkat_guru'].'/'.$dp3_guru['golongan_guru']; ?></td>
				</tr>
				<tr>
					<td colspan="2"></td>
				</tr>
				<tr>
					<th style="width:100px !important">Pejabat Penilai</th>
					<td><strong><?php echo $dp3_guru['pejabat_penilai']; ?></strong></td>
				</tr>
				<tr>
					<th style="width:100px !important">NIP</th>
					<td><?php echo $dp3_guru['nip_penilai']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Jabatan</th>
					<td><?php echo $dp3_guru['jabatan_penilai']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Unit Organisasi</th>
					<td><?php echo $dp3_guru['unit_organisasi_penilai']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Pangkat/Golongan</th>
					<td><?php echo $dp3_guru['pangkat_penilai'].'/'.$dp3_guru['golongan_penilai']; ?></td>
				</tr>
				<tr>
					<td colspan="2"></td>
				</tr>
				<tr>
					<th style="width:100px !important">Atasan Pejabat Penilai</th>
					<td><strong><?php echo $dp3_guru['atasan']; ?></strong></td>
				</tr>
				<tr>
					<th style="width:100px !important">NIP</th>
					<td><?php echo $dp3_guru['nip_atasan']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Jabatan</th>
					<td><?php echo $dp3_guru['jabatan_atasan']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Unit Organisasi</th>
					<td><?php echo $dp3_guru['unit_organisasi_atasan']; ?></td>
				</tr>
				<tr>
					<th style="width:100px !important">Pangkat/Golongan</th>
					<td><?php echo $dp3_guru['pangkat_atasan'].'/'.$dp3_guru['golongan_atasan']; ?></td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="box with-table">
		<table class="styled">
			<thead>
				<tr>
					<!-- <th style="width:20px !important">No</th> -->
					<th style="width:300px !important">Butir Penilaian</th>
					<th style="width:50px !important">Angka</th>
					<th style="width:50px !important">Sebutan</th>
					<th>Keterangan</th>
				</tr>
			</thead>
			<tbody>
				<?php $i = 1; foreach ($detail as $value): ?>
				<tr>
					<!-- <td><?php echo $i; ?></td> -->
					<td><?php echo $value['butir_dppp']; ?></td>
					<td style="text-align:center"><?php echo $value['angka']; ?></td>
					<td style="text-align:center"><?php echo cek_sebutan($value['angka']); ?></td>
					<td><?php echo $value['keterangan']; ?></td>
				</tr>
				<?php $i++; endforeach; ?>
				<tr>
					<th>Jumlah</th>
					<td style="text-align:center"><?php echo $dp3_guru['jumlah']; ?></td>
					<td>-</td>
					<td>-</td>
				</tr>
				<tr>
					<th>Rata-rata</th>
					<td style="text-align:center"><?php echo $dp3_guru['rata_rata']; ?></td>
					<td style="text-align:center"><?php echo cek_sebutan($dp3_guru['rata_rata']); ?></td>
					<td>-</td>
				</tr>
			</tbody>
		</table>
	</div>

	<div class="box with-table">
		<table class="styled">
			<tbody>
				<tr>
					<th style="width:300px !important">KEBERATAN DARI PEGAWAI NEGERI SIPIL YANG DINILAI</th>
					<td><?php echo $dp3_guru['keberatan']; ?></td>
				</tr>
				<tr>
					<th>TANGGAPAN PEJABAT PENILAI ATAS KEBERATAN</th>
					<td><?php echo $dp3_guru['tanggapan']; ?></td>
				</tr>
				<tr>
					<th>KEPUTUSAN ATASAN PEJABAT PENILAI ATAS KEBERATAN</th>
					<td><?php echo $dp3_guru['keputusan']; ?></td>
				</tr>
				<tr>
					<th>LAIN-LAIN</th>
					<td><?php echo $dp3_guru['lain_lain']; ?></td>
				</tr>
			</tbody>
		</table>
	</div>

		<div class="actions">
			<a href="<?php echo base_url('ptk/dp3_guru'); ?>" class="button"> Kembali</a>
		</div>
	</div>
</div>
