<?php  
	$statuspil = $statuspil;
	if(isset($_POST['id_potongan'])){
	$a = $_POST['id_potongan'];
	$statuspil = $a; } ?>

<h1 class="grid_12">Pendidik Dan Tenaga Kependidik</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>potongan - potongan</h2>
       </div>
		<div class="tabletools">
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Jenis</span></th>
							<td width=200px align='left'>
								<select name="id_potongan" id="id_potongan" onchange="submitform();">
									<option value="0">-Jenis potongan-</option>
									<?php foreach($data_potongan2 as $da): ?>
										
										<?php if($da->id_potongan == $statuspil){ ?>
											<option selected value='<?php echo $da->id_potongan; ?>' data-status-pilihan="<?php echo $da->id_potongan; ?>"><?php echo $da->nama_potongan; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $da->id_potongan; ?>' data-status-pilihan="<?php echo $da->id_potongan; ?>"><?php echo $da->nama_potongan; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
		
		
			<div class="right">
				<?php 
				$gid=get_usergroup();
				$id_potongan=$this->uri->segment(4);
				
				if($gid!=9){?>
			  	<a href="<?php echo base_url('ptk/potongan_potongan/load_add_detail/'.$statuspil); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php }elseif($gid==9){?>
				
				<?php }?>
			  <br><br>
            </div>
        </div>
			
		
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama</th>
								<th>Gaji Pokok</th>
								<th>Besaran</th>
								<th>%Gaji Pokok</th>
								<th>Faktor Kali</th>
								<th>potongan yang Diterima</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$this->load->model('ptk/m_potongan');
								$id_sekolah=get_id_sekolah();
								$data['isi'] = $this->m_potongan->get_data_detail($statuspil,$id_sekolah)->result_array();
								$isi=$data['isi'];
							?>
							<?php $no=1; foreach($isi as $dt):?>
							<tr>
								<td width='30px' class='center'><?php echo $no;?></td>
								<td width='' class='right'><?php echo $dt['nama'];?></td>								
								<td width='' class='right'><?php echo $dt['gaji'];?></td>								
								<td width='' class='right'><?php echo $dt['besaran_uang'];?></td>
								<td width='' class='right'><?php echo $dt['besaran_prosentase'];?>%</td>
								<td width='' class='center'><?php echo $dt['jumlah'];?></td>								
								<td width='' class='center'><?php 
								$hasil=($dt['besaran_uang']+($dt['gaji']*($dt['besaran_prosentase']/100)))*$dt['jumlah'];
								echo $hasil;
								?>
								
								</td>
								<td width='80' class='center'>
								<a original-title="" href="<?php echo base_url("ptk/potongan_potongan/delete_potongan_detail/$dt[id_potongan_detail]/$id_potongan"); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"> <i class="icon-remove" ></i> Delete</a>
								
								
								
								</td>
							</tr>
							<?php $no++; endforeach ?>
						</tbody>
					</table>
						
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>