	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			
			<form action="<?php echo base_url('ptk/potongan_potongan/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah potongan-potongan</legend>
					<?php foreach($data as $d):?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenis potongan</strong>
						</label>
						<div>
							<input type="text" name="jenis_potongan" value="<?php echo $d['nama_potongan']?>" />
						</div>
					</div>

					<div class="row">
						<label for="f1_normal_input">
							<strong>Besaran</strong>
						</label>
						<div>
							<input type="text" name="besaran" value="<?php echo $d['besaran_uang']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>%Gaji Pokok</strong>
						</label>
						<div>
							<input type="text" name="gaji_pokok" value="<?php echo $d['besaran_prosentase']?>" />
							<input type="hidden" name="id_potongan" value="<?php echo $d['id_potongan']?>" />
						</div>
					</div>
					<?php endforeach;?>
				</fieldset>
				
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('ptk/potongan_potongan/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
	
