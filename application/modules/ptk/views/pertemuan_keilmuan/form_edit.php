	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			
			<form action="<?php echo base_url('ptk/pertemuan_keilmuan/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Sertifikasi</legend>
					<?php foreach($data_edit as $d):?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama</strong>
						</label>
						<div>
					
							
							<input disabled type="text" name="nama" value="<?php echo $d['nama']?>" />
							<input type="hidden" name="id_guru" value="<?php echo $d['id_guru'];?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kegiatan</strong>
						</label>
						<div>
							<input type="text" name="kegiatan" value="<?php echo $d['kegiatan']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tempat</strong>
						</label>
						<div>
							<input type="text" name="tempat" value="<?php echo $d['tempat'];?>" />
							<input type="hidden" name="id" value="<?php echo $d['id_guru_pertemuan_keilmuan']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal Pelaksanaan</strong>
						</label>
						<div>
							<input type="date" name="tanggal_pelaksanaan" value="<?php echo tanggal_view($d['tanggal_pelaksanaan'])?>" />
							
						</div>
					</div>
				<?php endforeach?>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('ptk/pertemuan_keilmuan/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
		