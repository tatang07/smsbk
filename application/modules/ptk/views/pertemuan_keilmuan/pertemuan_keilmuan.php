<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Pertemuan-Pertemuan Keilmuan</h2>
			<?php //echo $statuspil ?>
       </div>
	<?php 
	$gid=get_usergroup();
	if($gid!=9){
	?>
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('ptk/pertemuan_keilmuan/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('ptk/pertemuan_keilmuan/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="40px" align="left"><span class="text">Nama</span></th>
								<td align="left"><input type="text" name="nama" id="nama"></td>
								<td width="10px"></td>
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
	<?php }elseif($gid==9){?>
	
	<?php }?>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >NIP</th>
								<th >Nama</th>
								<th >Kegiatan</th>
								<th >Tempat</th>
								<th >Tanggal Pelaksanaan</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
						
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
							
								<tr>
									<td class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['nip']?>
									</td class='center'>
									<td class='center'>
									<?php  echo $d['nama'];?>
									</td>
									<td class='center'>
									<?php  echo $d['kegiatan'];?>
									</td>
									<td class='center'>
									<?php  echo $d['tempat'];?>
									</td>
									<td class='center'>
									<?php  echo $d['tanggal_pelaksanaan'];?>
									</td>	
									<td class='center'>
									<a href="<?php echo site_url('ptk/pertemuan_keilmuan/load_edit/'.$d['id_guru_pertemuan_keilmuan'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
									<?php if($gid!=9){?>
									<a href="<?php echo site_url('ptk/pertemuan_keilmuan/delete/'.$d['id_guru_pertemuan_keilmuan'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
									<?php }elseif($gid==9){}?>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>