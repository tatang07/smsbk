<?php  
	$statuspil = 0;
	if(isset($_POST['id_tingkat_kelas'])){
	$a = $_POST['id_tingkat_kelas'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar Catatan BP/BK</h2>
			<?php //echo $statuspil ?>
       </div>
	
       <div class="tabletools">
			<div class="right">
			  	<a href="<?php echo base_url('pps/bp_bk/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('pps/bp_bk/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="40px" align="left"><span class="text">Nama</span></th>
								<td align="left"><input type="text" name="nama" id="nama"></td>
								<td width="10px"></td>
								<th width="110px" align="left"><span class="text">Dokumen Kurikulum</span></th>
								<td width="200px" align="left">
								<select name="id_tingkat_kelas" id="id_tingkat_kelas" onchange="submitform();">
									<option value="0">-Tingkat Kelas-</option>
									<?php foreach($ta as $q): ?>
										
										<?php if($q->id_tingkat_kelas == $statuspil){ ?>
											<option selected value='<?php echo $q->id_tingkat_kelas; ?>' data-status-pilihan="<?php echo $q->id_tingkat_kelas; ?>"><?php echo $q->tingkat_kelas; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $q->id_tingkat_kelas; ?>' data-status-pilihan="<?php echo $q->id_tingkat_kelas; ?>"><?php echo $q->tingkat_kelas; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
								</td>
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >Nis</th>
								<th >Nama</th>
								<th >Tanggal Dicatat</th>
								<th >Keterangan</th>
								<th >Aksi</th>
							</tr>
						</thead>
				
						<tbody>
						
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
								<?php //print_r($isi)?>
								<tr>
									<td class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['nis']?>
									</td class='center'>
									<td class='center'>
									<?php  echo $d['nama'];?>
									</td>
									<td class='center'>
									<?php  echo $d['tanggal_catatan'];?>
									</td>
									<td class='center'>
									<?php  echo $d['keterangan'];?>
									</td>
									<td class='center'>
									<a href="<?php echo site_url('pps/bp_bk/load_edit/'.$d['id_siswa_catatan_bk'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> edit</a>
									<a href="<?php echo site_url('pps/bp_bk/delete/'.$d['id_siswa_catatan_bk'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-remove"></i> Hapus</a>
									</td>
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>