	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			
			<form action="<?php echo base_url('ptk/kinerja_guru/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Penilaian Kinerja Guru</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Periode Mulai</strong>
						</label>
						<div>
							<input type="date" name="periode_mulai" value="" />
						</div>
					</div>

					<div class="row">
						<label for="f1_normal_input">
							<strong>Periode Akhir</strong>
						</label>
						<div>
							<input type="date" name="periode_akhir" value="" />
						</div>
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Yang Diniliai</legend>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama</strong>
						</label>
						<div>
							<select name="dinilai" data-placeholder="-- Pilih Yang Dinilai --">
							<option value="">-- Pilih Yang Dinilai --</option>
							<?php foreach($guru as $d):?>
							   <option value="<?php echo $d['id_guru']?>"><?php echo $d['nama']?></option>
							 <?php endforeach ?>
							</select>
						</div>
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Pejabat Penilai</legend>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Nama</strong>
						</label>
						<div>
							<input type="text" name="penilai">
						</div>
					</div>
				</fieldset>
				
				<fieldset>
					<legend>Butir</legend>
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Kompetensi Inti</th>
								<th>Angka</th>
								<th>Keterangan</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($jk_guru as $j): ?>
								<tr>
								<td width=50px class="center"><?php echo $no; ?></td>
								<td colspan=3><b><?php echo $j['jenis_kompetensi_guru']; ?></b></td>
								</tr>
								<?php foreach($bk_guru as $b): ?>
								<?php if($b['id_jenis_kompetensi_guru']==$j['id_jenis_kompetensi_guru']){ ?>
									<tr>
										<td></td>
										<td width=500px><?php echo $b['butir_kompetensi_guru']; ?></td>
										<td width=100px class="center"><input type="text" size=5 name="angka[<?php echo $b['id_butir_kompetensi_guru']; ?>]" /></td>
										<td width=250px class="center"><input type="text" name="keterangan[<?php echo $b['id_butir_kompetensi_guru']; ?>]" /></td>
									</tr>
								<?php }endforeach; ?>
							<?php $no++; endforeach; ?>
						</tbody>
					</table>
				</fieldset>
				
				<fieldset>
					<legend>Kesimpulan</legend>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Kesimpulan</strong>
						</label>
						<div>
							<textarea name="kesimpulan" class="editor full"></textarea>
						</div>
					</div>
				</fieldset>
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('ptk/kinerja_guru/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
