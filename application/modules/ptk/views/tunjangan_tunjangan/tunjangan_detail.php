<?php  
	$statuspil = $statuspil;
	if(isset($_POST['id_tunjangan'])){
	$a = $_POST['id_tunjangan'];
	$statuspil = $a; } ?>

<h1 class="grid_12">Pendidik Dan Tenaga Kependidik</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Tunjangan - Tunjangan</h2>
       </div>
		<div class="tabletools">
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Jenis</span></th>
							<td width=200px align='left'>
								<select name="id_tunjangan" id="id_tunjangan" onchange="submitform();">
									<option value="0">-Jenis tunjangan-</option>
									<?php foreach($data_tunjangan2 as $da): ?>
										
										<?php if($da->id_tunjangan == $statuspil){ ?>
											<option selected value='<?php echo $da->id_tunjangan; ?>' data-status-pilihan="<?php echo $da->id_tunjangan; ?>"><?php echo $da->nama_tunjangan; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $da->id_tunjangan; ?>' data-status-pilihan="<?php echo $da->id_tunjangan; ?>"><?php echo $da->nama_tunjangan; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
		
		
			<div class="right">
				<?php 
				$gid=get_usergroup();
				$id_tunjangan=$this->uri->segment(4);
				
				if($gid!=9){?>
			  	<a href="<?php echo base_url('ptk/tunjangan_tunjangan/load_add_detail/'.$statuspil); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php }elseif($gid==9){?>
				
				<?php }?>
			  <br><br>
            </div>
        </div>
			
		
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nama</th>
								<th>Gaji Pokok</th>
								<th>Besaran</th>
								<th>%Gaji Pokok</th>
								<th>Faktor Kali</th>
								<th>Tunjangan yang Diterima</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php 
								$this->load->model('ptk/m_tunjangan');
								$id_sekolah=get_id_sekolah();
								$data['isi'] = $this->m_tunjangan->get_data_detail($statuspil,$id_sekolah)->result_array();
								$isi=$data['isi'];
							?>
							<?php $no=1; foreach($isi as $dt):?>
							<tr>
								<td width='30px' class='center'><?php echo $no;?></td>
								<td width='' class='right'><?php echo $dt['nama'];?></td>								
								<td width='' class='right'><?php echo $dt['gaji'];?></td>								
								<td width='' class='right'><?php echo $dt['besaran_uang'];?></td>
								<td width='' class='right'><?php echo $dt['besaran_prosentase'];?>%</td>
								<td width='' class='center'><?php echo $dt['jumlah'];?></td>								
								<td width='' class='center'><?php 
								$hasil=($dt['besaran_uang']+($dt['gaji']*($dt['besaran_prosentase']/100)))*$dt['jumlah'];
								echo $hasil;
								?>
								
								</td>
								<td width='80' class='center'>
								<a original-title="" href="<?php echo base_url("ptk/tunjangan_tunjangan/delete_tunjangan_detail/$dt[id_tunjangan_detail]/$id_tunjangan"); ?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"> <i class="icon-remove" ></i> Delete</a>
								
								
								
								</td>
							</tr>
							<?php $no++; endforeach ?>
						</tbody>
					</table>
						
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>