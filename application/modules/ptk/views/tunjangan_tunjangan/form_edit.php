	<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
			
			<form action="<?php echo base_url('ptk/tunjangan_tunjangan/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah Tunjangan-Tunjangan</legend>
					<?php foreach($data as $d):?>
					<div class="row">
						<label for="f1_normal_input">
							<strong>Jenis Tunjangan</strong>
						</label>
						<div>
							<input type="text" name="jenis_tunjangan" value="<?php echo $d['nama_tunjangan']?>" />
						</div>
					</div>

					<div class="row">
						<label for="f1_normal_input">
							<strong>Besaran</strong>
						</label>
						<div>
							<input type="text" name="besaran" value="<?php echo $d['besaran_uang']?>" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>%Gaji Pokok</strong>
						</label>
						<div>
							<input type="text" name="gaji_pokok" value="<?php echo $d['besaran_prosentase']?>" />
							<input type="hidden" name="id_tunjangan" value="<?php echo $d['id_tunjangan']?>" />
						</div>
					</div>
					<?php endforeach;?>
				</fieldset>
				
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('ptk/tunjangan_tunjangan/index/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
	
