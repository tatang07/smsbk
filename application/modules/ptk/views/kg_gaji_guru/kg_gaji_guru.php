<?php //print_r($potongan)?>
<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Gaji</h2>
			<?php //echo $statuspil ?>
       </div>
	
	<?php 
	$gid=get_usergroup();
	?>
	<?php if($gid!=9){?>
       <div class="tabletools">
			<div class="right">
				
			  	<a href="<?php echo base_url('ptk/sertifikasi/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
			  <br><br>
            </div>
			
			<div class="dataTables_filter">
				<form action="<?php echo base_url('ptk/sertifikasi/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="40px" align="left"><span class="text">Nama</span></th>
								<td align="left"><input type="text" name="nama" id="nama"></td>
								<td width="10px"></td>
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form>
			</div>

            
        </div>
	<?php }elseif($gid==9){?>
		
	<?php }?>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th >No.</th>
								<th >NIP</th>
								<th >Nama</th>
								<th >Gaji Pokok</th>
								<th >Tunjangan</th>
								<th >Potongan</th>
								<th >Take Home Pay</th>
							
							</tr>
						</thead>
				
						<tbody>
						
							<?php $no=1; ?>
							
							<?php foreach($isi as $d):?>
							
								<tr>
									<td width="20px" class='center'>
									<?php echo $no; $no++;?>
									</td>
									<td class='center'>
									<?php  echo $d['nip']?>
									</td class='center'>
									<td class='center'>
									<?php  echo $d['nama'];?>
									</td>
									<td class='center'>
									<?php number?>
								    <?php echo 'Rp.'.number_format($d['gaji'],0,',','.');?>
									</td>
									<td class='center'>
									<?php number?>
								    <?php echo 'Rp.'.number_format($d['jml_tunjangan'],0,',','.');?>
									</td>
									<td class='center'>
									<?php number?>
								    <?php echo 'Rp.'.number_format($d['jml_potongan'],0,',','.');?>
									</td>
									<td class='center'>
									<?php number?>
								    <?php echo 'Rp.'.number_format($d['total'],0,',','.');?>
									</td>
							
								</tr>
							<?php endforeach;?>
						</tbody>
					</table>
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
</div>

<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>