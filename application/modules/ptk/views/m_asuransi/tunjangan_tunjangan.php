<h1 class="grid_12">Guru</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Tunjangan - tunjangan</h2>
       </div>

		
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jenis Tunjangan</th>
								<th>Besaran</th>
								<th>% Gaji Pokok</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($data_tunjangan as $dt):?>
							<tr>
								<td width='' class='center'><?php echo $no;?></td>
								<td width='' class='right'><?php echo $dt['nama_tunjangan'];?></td>								
								<td width='' class='center'><?php echo $dt['besaran_uang'];?></td>
								<td width='' class='center'><?php echo $dt['besaran_prosentase']; $no++;?></td>
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
						
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
