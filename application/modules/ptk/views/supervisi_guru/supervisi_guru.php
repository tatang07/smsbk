<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Pembinaan Dan Pembimbingan</h2>
       </div>
       <div class="tabletools">
			<div class="right">
			  	<a href=""><i><img src="http://localhost/smsbkv2//theme/img/icons/packs/iconsweets2/14x14/excel-document.png"></i>Excel</a> 
			  <br/><br/>
            </div>
			
            <div class="dataTables_filter">
                <!--<form action="<?php //echo base_url('ptk/penugasan_dan_penentuan_beban_mengajar_guru/search'); ?>" method="post" enctype="multipart/form-data">
					<table>
						<tbody>
							<tr>
								<th width="40px" align="left"><span class="text">Nama</span></th>
								<td align="left"><input type="text" name="nama" id="nama"></td>
								<td width="10px"></td>
							
								<td width="10px"></td>
								<td width="1px">
									<input type="submit" name=send class="button grey tooltip" value="cari" />
								</td>
							</tr>
						</tbody>
					</table>
				</form> -->
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Index Prestasi</th>
								<th>Waktu</th>
								<th>Nama Guru</th>
								<th>Mata Pelajaran</th>
								<th>Materi</th>
								<th>Kelas</th>
								<th>Term</th>
								<th width="150">Aksi</th>
							
							</tr>
						</thead>
					
						<tbody>
							<?php $no=1;?>
					
							<?php foreach ($data as $d) {?>
							<tr>				
								<td width='30px' class='center'><?php echo $no; ?></td>
								<td width='400px'><?php echo $d['indeks']; ?></td>
								<td width='400px'><?php echo $d['waktu']; ?></td>
								<td width='400px'><?php echo $d['nama']; ?></td>
								<td width='400px'><?php echo $d['pelajaran']; ?></td>
								<td width='400px'><?php echo $d['materi']; ?></td>
								<td width='400px'><?php echo $d['kelas']; ?></td>
								<td width='400px'><?php echo $d['semester']; ?></td>
							
								<td class='center'>
									<a href="<?php echo site_url('ptk/supervisi_guru/detail/'.$d['id_supervisi'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail </a>
								</td>
							</tr>
							<?php $no=$no+1 ?>
							<?php } ?>
						</tbody>
					</table>
			
					
					<div class="footer">
						<div class="dataTables_info">Jumlah Data: <?php echo $no-1;?></div> 
					
						<div class="dataTables_paginate paging_full_numbers">
							<a class="first paginate_button" href="javascript:changePage(1);" tabindex="0">Awal</a>
											<a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
										<span>
																<a style="margin-left:-3px;" tabindex="0" class="paginate_active">1</a>
														</span>
											<a style="margin-left:-3px;" tabindex="0" class="next paginate_button paginate_button_disabled">Selanjutnya</a>
							
							<a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(1);">Akhir</a>
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
