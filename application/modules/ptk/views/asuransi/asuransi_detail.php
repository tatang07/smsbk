<?php  
	// $statuspil = $statuspil;
	if(isset($_POST['id_asuransi'])){
	$a = $_POST['id_asuransi'];
	$statuspil = $a; } ?>
<h1 class="grid_12">Pendidik Dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Asuransi Detail</h2>
       </div>
	   
		<?php //echo $statuspil;?>
		<div class="tabletools">
			
			<div class="dataTables_filter">
				<form action="" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Jenis</span></th>
							<td width=200px align='left'>
								<select name="id_asuransi" id="id_asuransi" onchange="submitform();">
									<option value="0">-Jenis Asuransi-</option>
									<?php foreach($data_asuransi as $da): ?>
										
										<?php if($da->id_asuransi == $statuspil){ ?>
											<option selected value='<?php echo $da->id_asuransi; ?>' data-status-pilihan="<?php echo $da->id_asuransi; ?>"><?php echo $da->asuransi; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $da->id_asuransi; ?>' data-status-pilihan="<?php echo $da->id_asuransi; ?>"><?php echo $da->asuransi; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>
					</table>
				</form>
			</div>
            <div class="right">
				<?php 
				$gid=get_usergroup();
				$id=$statuspil;
				// echo $id;
				if($gid!=9){?>
			  	<a href="<?php echo base_url('ptk/asuransi/load_add_detail/'.$id); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php }elseif($gid==9){?>
				
				<?php }?>
			  <br><br>
            </div>
        </div>
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Nip</th>
								<th>Nama</th>
								<th>Aksi</th>
								
							</tr>
						</thead>
						<tbody>
							<?php 
								$this->load->model('ptk/m_asuransi');
								$id_sekolah=get_id_sekolah();
								$data['isi'] = $this->m_asuransi->get_data_detail($statuspil,$id_sekolah)->result_array();
								$isi=$data['isi'];
							?>
							<?php //print_r($isi)?>
							<?php $no=1; foreach($isi as $dd):?>
							<tr>
								<td width='30px' class='center'><?php echo $no;?></td>
								<td width='100px' class='right'><?php echo $dd['nip'];?></td>								
								<td width='' class='right'><?php echo $dd['nama']; $no++;?></td>								
								<td width="100px"class='center'>
								<?php $id2=$this->uri->segment(4)?>
								
								<a original-title="" href="<?php echo site_url('ptk/asuransi/delete/'.$dd['id_asuransi_detail'].'/'.$id2)?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove" ></i> Hapus</a>
								</td>
						
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
						
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
<script>
	function submitform()
	{
	  document.myform.submit();
	}
</script>