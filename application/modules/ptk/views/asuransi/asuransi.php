<h1 class="grid_12">Guru</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Asuransi</h2>
       </div>

		<div class="tabletools">
			<div class="right">
				<?php 
				$gid=get_usergroup();
				
				if($gid!=9){?>
			  	<a href="<?php echo base_url('ptk/asuransi/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php }elseif($gid==9){?>
				
				<?php }?>
			  <br><br>
            </div>
        </div>
		
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jenis Asuransi</th>
								<th>Aksi</th>
								
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($data_asuransi as $dt):?>
							<tr>
								<td width='' class='center'><?php echo $no; $no++;?></td>
								<td width='' class='right'><?php echo $dt['asuransi'];?></td>								
								<td class='center'>
								<a href="<?php echo site_url('ptk/asuransi/load_detail/'.$dt['id_asuransi'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Detail</a>	
								<a original-title="" href="<?php echo base_url('ptk/asuransi/delete2/'.$dt['id_asuransi'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove" ></i> Delete</a>
								
								</td>
								
						
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
						
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
