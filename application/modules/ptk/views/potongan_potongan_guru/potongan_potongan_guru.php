<pre><?php //print_r($data_potongan)?></pre>
<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>potongan - potongan</h2>
       </div>
		<div class="tabletools">
			<div class="right">
				<?php 
				$gid=get_usergroup();
				
				if($gid!=9){?>
			  	<a href="<?php echo base_url('ptk/potongan_potongan/load_add/'); ?>"><i class="icon-plus"></i>Tambah</a> 
				<?php }elseif($gid==9){?>
				
				<?php }?>
			  <br><br>
            </div>
        </div>
			
		
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jenis potongan</th>
								<th>Besar potongan</th>
								
							</tr>
						</thead>
						<tbody>
							<?php $no=1; foreach($data_potongan as $dt):?>
							<tr>
								<td width='30' class='center'><?php echo $no;?></td>
								<td width='' class='right'><?php echo $dt['nama_potongan'];?></td>								
								<td width='50px' class='center'><?php  
								$hasil=($dt['besaran_uang']+($dt['gaji']*($dt['besaran_prosentase']/100)))*$dt['jumlah'];
								echo $hasil;
								?></td>
								
							</tr>
							<?php endforeach ?>
						</tbody>
					</table>
						
					<div class="footer">
						<div class="dataTables_info">Jumlah Data : <?php echo $no-1; ?></div>
						
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
