<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Kualifikasi Pendidikan <?php echo $title; ?></h2>
       </div>
	   <div class="box">
	   <div class="content" style="height: 250px;">
						<table class=chart data-type=pie data-donut=0>
							<thead>
								<?php foreach ($data as $d) {?>
								<tr>
									<th><?php echo $d->jenjang_pendidikan; ?></th>
							
								</tr>
								<?php } ?>
							</thead>
							
							<tbody>
							<?php foreach ($data as $d) {?>
								<tr>
									<th><?php echo $d->jenjang_pendidikan; ?></th>
									<td><?php echo $d->s; ?></td>
								</tr>	
							<?php } ?>			
							</tbody>	
						</table>
					</div><!-- End of .content -->
				</div><!-- End of .box -->
	   
	   
        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Jenjang Pendidikan Terakhir</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<?php $no=1?>
							<?php foreach ($data as $d) {?>
							<tr>
								<td width='20px' class='center'><?php echo $no ?></td>
								<td width='' class='left'><?php echo $d->jenjang_pendidikan; ?><br/></td>
								<td width='50px' class='center'><?php echo $d->s; ?></td>
								<?php $no=$no+1 ?>
							</tr>
							<?php }?>							
						</tbody>
					</table>
					
						</div>
					</div>
				</div>
            </div>
        </div>        
    </div>
 </div>
