<?php
class m_tenaga_kependidikan extends MY_Model {
    public $table = "m_tenaga_kependidikan";
    public $id = "id_tenaga_kependidikan";
	
	//                  join                    ON 
	public $join_to = array(
			// "r_golongan_darah gd"=>"gd.id_golongan_darah=m_guru.id_golongan_darah",
			// "r_status_pernikahan sp"=>"sp.id_status_pernikahan=m_guru.id_status_pernikahan",
			// "r_agama a"=>"a.id_agama=m_guru.id_agama",
			// "r_status_pegawai rsp"=>"rsp.id_status_pegawai=m_guru.id_status_pegawai",
			// "r_kecamatan k"=>"k.id_kecamatan=m_guru.id_kecamatan",
			// "r_jenjang_pendidikan jp"=>"jp.id_jenjang_pendidikan=m_guru.id_jenjang_pendidikan",
			"r_jenis_kepegawaian jp"=>"jp.id_jenis_kepegawaian=m_tenaga_kependidikan.id_jenis_kepegawaian"
			// "m_sekolah s"=>"s.id_sekolah=m_guru.id_sekolah"
		);
	 public $join_fields = array("jenis_kepegawaian");
	
	function getdetail($id=0){
		$this->db->select('g.*, gd.*,sp.*,a.*,rsp.*,k.*,jp.*,pg.*,s.sekolah');
		$this->db->from('m_tenaga_kependidikan g'); 
		$this->db->join('r_golongan_darah gd', 'gd.id_golongan_darah=g.id_golongan_darah', 'left');
		$this->db->join('r_status_pernikahan sp', 'sp.id_status_pernikahan=g.id_status_pernikahan', 'left');
		$this->db->join('r_agama a', 'a.id_agama=g.id_agama', 'left');
		$this->db->join('r_status_pegawai rsp', 'rsp.id_status_pegawai=g.id_status_pegawai', 'left');
		$this->db->join('r_kecamatan k', 'k.id_kecamatan=g.id_kecamatan', 'left');
		$this->db->join('r_jenjang_pendidikan jp', 'jp.id_jenjang_pendidikan=g.id_jenjang_pendidikan', 'left');
		$this->db->join('r_pangkat_golongan pg', 'pg.id_pangkat_golongan=g.id_pangkat_golongan', 'left');
		$this->db->join('r_jenis_kepegawaian jk', 'jk.id_jenis_kepegawaian=g.id_pangkat_golongan', 'left');
		$this->db->join('m_sekolah s', 's.id_sekolah=g.id_sekolah', 'left');
		$this->db->where('g.id_tenaga_kependidikan',$id);
		$this->db->order_by('g.nip','asc');         
		$query = $this->db->get();
		
		if($query->num_rows() != 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	
	}
}


// public $join_to = array(
			// "r_golongan_darah gd"=>"gd.id_golongan_darah=m_guru.id_golongan_darah",
			// "r_status_pernikahan sp"=>"sp.id_status_pernikahan=m_guru.id_status_pernikahan",
			// "r_agama a"=>"a.id_agama=m_guru.id_agama",
			// "r_status_pegawai rsp"=>"rsp.id_status_pegawai=m_guru.id_status_pegawai",
			// "r_kecamatan k"=>"k.id_kecamatan=m_guru.id_kecamatan",
			// "r_jenjang_pendidikan jp"=>"jp.id_jenjang_pendidikan=m_guru.id_jenjang_pendidikan",
			// "r_pangkat_golongan pg"=>"pg.id_pangkat_golongan=m_guru.id_pangkat_golongan"
			// "m_sekolah s"=>"s.id_sekolah=m_guru.id_sekolah"
		// );
	// public $join_fields = array("golongan_darah","status_pernikahan","agama","status","kecamatan","jenjang_pendidikan","pangkat","golongan");