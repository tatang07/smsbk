<?php
class m_studi_lanjut extends MY_Model {
	
	public function get_data_search($status, $nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM t_guru_studi_lanjut as gsl join m_guru as g on g.id_guru = gsl.id_guru join r_jenjang_pendidikan as rp on rp.id_jenjang_pendidikan = g.id_jenjang_pendidikan where  g.nama  like '%$nama%' and gsl.status = '$status' and g.id_sekolah=$id_sekolah");
		return $query->result_array();
	}
	public function get_data_search_default($nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM t_guru_studi_lanjut as gsl join m_guru as g on g.id_guru = gsl.id_guru join r_jenjang_pendidikan as rp on rp.id_jenjang_pendidikan = g.id_jenjang_pendidikan where g.nama like '%$nama%' and g.id_sekolah=$id_sekolah");
		return $query->result_array();
	}
	function get_data($id_sekolah){
			$this->db->select('*');
			$this->db->from('t_guru_studi_lanjut gsl');
			$this->db->join('m_guru g','g.id_guru = gsl.id_guru');
			$this->db->join('r_jenjang_pendidikan jp','gsl.id_jenjang_pendidikan = jp.id_jenjang_pendidikan');
			$this->db->where('g.id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}
	

	function get_data_guru($id_sekolah,$id_guru){
			$this->db->select('*');
			$this->db->from('t_guru_studi_lanjut gsl');
			$this->db->join('m_guru g','g.id_guru = gsl.id_guru');
			$this->db->join('r_jenjang_pendidikan jp','gsl.id_jenjang_pendidikan = jp.id_jenjang_pendidikan');
			$this->db->where('g.id_sekolah',$id_sekolah);
			$this->db->where('g.id_guru',$id_guru);
			
			return $this->db->get()->result_array();
	}
	function get_siswa($id_sekolah,$id_tingkat_kelas){
			$this->db->select('*');
			$this->db->from('t_rombel_detail rd');
			$this->db->join('m_siswa s','s.id_siswa = rd.id_siswa');		
			$this->db->join('m_rombel r','r.id_rombel = rd.id_rombel');
			$this->db->where('s.id_sekolah',$id_sekolah);
			$this->db->where('r.id_tingkat_kelas',$id_tingkat_kelas);
			return $this->db->get()->result_array();
	}

	function get_data_edit($id_guru){
			$this->db->select('*');
			$this->db->from('t_guru_studi_lanjut gsl');
			$this->db->join('m_guru g','g.id_guru = gsl.id_guru');
			// $this->db->join('r_jenjang_pendidikan jp','g.id_jenjang_pendidikan = jp.id_jenjang_pendidikan');
			$this->db->where('gsl.id_guru_studi_lanjut',$id_guru);
			
			return $this->db->get()->result_array();
	}
	
	
	function select_guru($id_sekolah){
			$this->db->select('*');
			$this->db->from('m_guru');
			$this->db->where('id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}

	function add($data){
			$this->db->insert('t_guru_studi_lanjut',$data);
	}

	function edit($data,$id=0){
			$this->db->where('id_guru_studi_lanjut',$id);
			$this->db->update('t_guru_studi_lanjut',$data);
	}

	function delete($id_guru_studi_lanjut){
			$this->db->where('id_guru_studi_lanjut', $id_guru_studi_lanjut);
			$this->db->delete('t_guru_studi_lanjut');
	}

}