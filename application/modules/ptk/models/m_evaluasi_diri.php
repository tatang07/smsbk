 <?php
class m_evaluasi_diri extends MY_Model {
	function get_guru($id_sekolah){
		$this->db->select('*');
		$this->db->from('m_guru');
		$this->db->where('id_sekolah',$id_sekolah);
		return $this->db->get();
	}
	
	function get_guru_guru($id_sekolah,$id_guru){
		$this->db->select('*');
		$this->db->from('m_guru');
		$this->db->where('id_sekolah',$id_sekolah);
		$this->db->where('id_guru',$id_guru);
		return $this->db->get();
	}
	
	function get_data($id_guru){
		$this->db->select('*');
		$this->db->from('t_guru_evaluasi');
		$this->db->where('id_guru',$id_guru);	
		return $this->db->get();
	}

	function get_data_by($id){
		$this->db->select('*');
		$this->db->from('t_guru_evaluasi');
		$this->db->where('id_guru_evaluasi',$id);	
		return $this->db->get();
	}

 	function insert($id_guru, $tgl_mulai, $tgl_akhir, $lokasi_file){
		$this->db->query("INSERT INTO t_guru_evaluasi(id_guru, tgl_mulai, tgl_akhir, lokasi_file) VALUES ('$id_guru', '$tgl_mulai', '$tgl_akhir', '$lokasi_file');");
	}

	function delete($id){
		$this->db->where('id_guru_evaluasi', $id);
		$this->db->delete('t_guru_evaluasi');
	}
}