<?php

class M_dp3_guru extends MY_Model {

    public $table = "v_penilaian_dppp";
    public $id = "id_penilaian_dppp";

    function get_dppp_detail($id_dppp)
    {
    	$res = $this->db->from('t_penilaian_dppp_detail d')
    			->join('r_butir_dppp b', 'b.id_butir_dppp = d.id_butir_dppp')
    			->where('id_penilaian_dppp', $id_dppp)
    			->get();

    	if($res->num_rows() > 0)
    		return $res->result_array();

    	return false;
    }

    function get_butir_dppp()
    {
    	return $this->db->get('r_butir_dppp')->result_array();
    }

    function get_golpang()
    {
    	$res = $this->db->from('r_pangkat_golongan')->get();

    	if($res->num_rows() > 0)
    		return $res->result_array();

    	return false;
    }

    function get_guru($id)
    {
    	$res = $this->db->from('m_guru')
		->where('id_sekolah', $id)
		->get();

    	if($res->num_rows() > 0)
    		return $res->result_array();

    	return false;
    }

    function insert_dp3_guru($data)
    {
    	$this->db->insert('t_penilaian_dppp', $data);
    	return $this->db->insert_id();
    }

    function insert_dppp_detail($data)
    {
    	return $this->db->insert_batch('t_penilaian_dppp_detail', $data);
    }

    function delete_dp3_guru($id_delete_dp3_guru){
    	if($this->db->where('id_penilaian_dppp', $id_delete_dp3_guru)->delete('t_penilaian_dppp') &&
    		$this->db->where('id_penilaian_dppp', $id_delete_dp3_guru)->delete('t_penilaian_dppp_detail'))
    		return true;

    	return false;
    }
}