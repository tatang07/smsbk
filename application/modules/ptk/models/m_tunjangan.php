<?php
class m_tunjangan extends MY_Model {
    public $table = "m_tunjuangan";
    public $id = "id_tunjangan";
	

	
	function select_data_tunjangan($id){
		$this->db->select('*');
		$this->db->from('m_tunjangan');
		$this->db->where('id_sekolah',$id);
		return $this->db->get();
	}
	
	function select_data_edit($id){
		$this->db->select('*');
		$this->db->from('m_tunjangan');
		$this->db->where('id_tunjangan',$id);
		return $this->db->get();
	}
	
	function update_detail($id,$data){
			$this->db->where('id_tunjangan',$id);
			$this->db->update('m_tunjangan',$data);
	}
	
	function select_data_tunjangan_detail($id){
		$this->db->select('*');
		$this->db->from('t_tunjangan_detail td');
		$this->db->join('m_guru g','g.id_guru=td.id_guru');
		$this->db->where('id_tunjangan',$id);
		return $this->db->get();
	}
	
	function delete_detail($id){
			$this->db->where('id_tunjangan_detail', $id);
			$this->db->delete('t_tunjangan_detail');
	}
	
	function get_data_detail($id_tunjangan=0,$id_sekolah){
			$this->db->select('nip,nama,id_tunjangan_detail,jumlah,gaji,besaran_uang,besaran_prosentase');
			$this->db->from('t_tunjangan_detail as ad');
			$this->db->join('m_tunjangan t', 'ad.id_tunjangan = t.id_tunjangan');
			$this->db->join('m_guru g', 'g.id_guru = ad.id_guru');
			$this->db->where('ad.id_tunjangan',$id_tunjangan);
			$this->db->where('g.id_sekolah',$id_sekolah);
			return $this->db->get();
	}
	
	function delete_detail2($id){
			$this->db->where('id_tunjangan', $id);
			$this->db->delete('t_tunjangan_detail');
	}
	
	function delete($id){
			$this->db->where('id_tunjangan', $id);
			$this->db->delete('m_tunjangan');
	}
	
	function select_data_tunjangan2($id){
	
			$this->db->select('*');
			$this->db->from('m_tunjangan');
			$this->db->where('id_sekolah',$id);
			return $this->db->get();
	}
	
	function select_guru($id){
		$this->db->select('*');
		$this->db->from('m_guru');
		$this->db->where('id_sekolah',$id);
		return $this->db->get();
	}
	
	function add($data){
		$this->db->insert('m_tunjangan',$data);
	}
	
	function add_detail($data){
		$this->db->insert('t_tunjangan_detail',$data);
	}
	
}