<?php
class m_asuransi_guru extends MY_Model {
    public $table = "m_asuransi";
    public $id = "id_asuransi";
	
	function select_data_asuransi($id){
			$this->db->select('*');
			$this->db->from('t_asuransi_detail ad');
			$this->db->join('m_asuransi a', 'a.id_asuransi = ad.id_asuransi');
			$this->db->where('id_guru',$id);
			$this->db->group_by('a.id_asuransi');
			
			return $this->db->get();
	}
		
}