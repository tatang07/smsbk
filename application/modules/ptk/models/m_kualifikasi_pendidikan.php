<?php
class m_kualifikasi_pendidikan extends MY_Model {
    public $table = "m_guru";
    public $id = "id_guru";
	
	function insert_isi($data){
			$this->db->insert($this->table, $data);
	}
	
	function select_golongan($id,$status_pendidik=1){//berdasar id dan status pendidik
			$this->db->select('*,count(m_guru.id_jenjang_pendidikan) s');
			//$this->db->like('id_pangkat_golongan', '1');
			$this->db->from('m_guru');
			$this->db->join('r_jenjang_pendidikan jp', 'jp.id_jenjang_pendidikan = m_guru.id_jenjang_pendidikan');
			$this->db->where(array('m_guru.id_sekolah'=>$id,'m_guru.status_pendidik'=>$status_pendidik));
			$this->db->group_by("jp.id_jenjang_pendidikan");
			return $this->db->get();
		}
	// function select_isi_by_id($id){
			// $this->db->select('*');
			// $this->db->from('t_coba');
			// $this->db->where('id_coba', $id);
			// return $this->db->get();
		// }
}