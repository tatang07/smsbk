<?php
class m_guru extends MY_Model {
    public $table = "m_guru";
    public $id = "id_guru";
	
	//                  join                    ON 
	public $join_to = array(
			// "r_golongan_darah gd"=>"gd.id_golongan_darah=m_guru.id_golongan_darah",
			// "r_status_pernikahan sp"=>"sp.id_status_pernikahan=m_guru.id_status_pernikahan",
			// "r_agama a"=>"a.id_agama=m_guru.id_agama",
			// "r_status_pegawai rsp"=>"rsp.id_status_pegawai=m_guru.id_status_pegawai",
			// "r_kecamatan k"=>"k.id_kecamatan=m_guru.id_kecamatan",
			// "r_jenjang_pendidikan jp"=>"jp.id_jenjang_pendidikan=m_guru.id_jenjang_pendidikan",
			"r_pangkat_golongan pg"=>"pg.id_pangkat_golongan=m_guru.id_pangkat_golongan"
			// "m_sekolah s"=>"s.id_sekolah=m_guru.id_sekolah"
		);
	 public $join_fields = array("pangkat");
	
	public function tambah_user($data=array()){
		$this->db->insert('sys_user', $data);
	}
	
	function getdetail($id=0){
		$this->db->select('g.*, gd.*,sp.*,a.*,rsp.*,k.*,jp.*,pg.*,s.sekolah,jj.*');
		$this->db->from('m_guru g'); 
		$this->db->join('r_golongan_darah gd', 'gd.id_golongan_darah=g.id_golongan_darah', 'left');
		$this->db->join('r_status_pernikahan sp', 'sp.id_status_pernikahan=g.id_status_pernikahan', 'left');
		$this->db->join('r_agama a', 'a.id_agama=g.id_agama', 'left');
		$this->db->join('r_status_pegawai rsp', 'rsp.id_status_pegawai=g.id_status_pegawai', 'left');
		$this->db->join('r_kecamatan k', 'k.id_kecamatan=g.id_kecamatan', 'left');
		$this->db->join('r_jenjang_pendidikan jp', 'jp.id_jenjang_pendidikan=g.id_jenjang_pendidikan', 'left');
		$this->db->join('r_pangkat_golongan pg', 'pg.id_pangkat_golongan=g.id_pangkat_golongan', 'left');
		$this->db->join('m_sekolah s', 's.id_sekolah=g.id_sekolah', 'left');
		$this->db->join('r_jenis_jabatan as jj','jj.id_jenis_jabatan=g.id_jenis_jabatan','left');
		$this->db->where('g.id_guru',$id);
		$this->db->order_by('g.nip','asc');         
		$query = $this->db->get();
		
		if($query->num_rows() != 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	
	}
	
	
	// ------------ bagian jabatan

	function getJabatan($id=0){
		// SELECT * FROM `t_guru_jabatan` as gj join r_jenis_jabatan as jj On gj.`id_jenis_jabatan` = jj.`id_jenis_jabatan` 
		$this->db->select('*');
		$this->db->from('t_guru_jabatan gj'); 
		$this->db->join('r_jenis_jabatan jj', 'gj.id_jenis_jabatan=jj.id_jenis_jabatan');
		$this->db->where('gj.id_guru',$id);
		return $this->db->get();
	}
	
	
	// function cekNip($nip){
		// SELECT * FROM `t_guru_jabatan` as gj join r_jenis_jabatan as jj On gj.`id_jenis_jabatan` = jj.`id_jenis_jabatan` 
		// $this->db->select('*');
		// $this->db->from('m_guru g'); 
		// $this->db->where('nip',$nip);

		// $hasil = $this->db->get();
		// return $hasil;
	// }
		
	function getMacamJabatan(){
		$this->db->select('*');
		$this->db->from('r_jenis_jabatan jj'); 
		return $this->db->get();
	}
	
	function getDetJabatan($id,$id_gj){
		// SELECT * FROM `t_guru_jabatan` as gj join r_jenis_jabatan as jj On gj.`id_jenis_jabatan` = jj.`id_jenis_jabatan` 
		$this->db->select('*');
		$this->db->from('t_guru_jabatan gj'); 
		$this->db->join('r_jenis_jabatan jj', 'gj.id_jenis_jabatan=jj.id_jenis_jabatan');
		$this->db->where('gj.id_guru',$id);
		$this->db->where('gj.id_guru_jabatan',$id_gj);
		return $this->db->get();
	}

	function selectJabatan(){
		$this->db->select('*');
		$this->db->from('r_jenis_jabatan');
		return $this->db->get();
	}
	
	function tambah_jabatan($data){
			$this->db->insert('t_guru_jabatan',$data);
	}
	function update_jabatan($data,$id_guru_jabatan=0){
			$this->db->where('id_guru_jabatan',$id_guru_jabatan);
			$this->db->update('t_guru_jabatan',$data);
	}
	function delete_jabatan($id_guru_jabatan){
			$this->db->where('id_guru_jabatan', $id_guru_jabatan);
			$this->db->delete('t_guru_jabatan');
	}	

	// ------------ bagian penataran

	function getPenataran($id=0){
		// SELECT * FROM `t_guru_jabatan` as gj join r_jenis_jabatan as jj On gj.`id_jenis_jabatan` = jj.`id_jenis_jabatan` 
		$this->db->select('*');
		$this->db->from('t_guru_penataran gp'); 
		$this->db->where('gp.id_guru',$id);
		return $this->db->get();
	}
	
	function getDetPenataran($id,$id_gp){
		$this->db->select('*');
		$this->db->from('t_guru_penataran gp'); 
		$this->db->where('gp.id_guru',$id);
		$this->db->where('gp.id_guru_penataran',$id_gp);
		return $this->db->get();
	}

	function tambah_penataran($data){
		$this->db->insert('t_guru_penataran',$data);
	}

	function update_penataran($data, $id_guru_penataran=0){
		$this->db->where('id_guru_penataran',$id_guru_penataran);
		$this->db->update('t_guru_penataran',$data);
	}

	function delete_penataran($id_guru_penataran){
		$this->db->where('id_guru_penataran', $id_guru_penataran);
		$this->db->delete('t_guru_penataran');
	}

	// ------------ bagian pendidikan

	function getPendidikan($id=0){
		$this->db->select('*');
		$this->db->from('t_guru_pendidikan gp');
		$this->db->join('r_jenjang_pendidikan jp', 'gp.id_jenjang_pendidikan=jp.id_jenjang_pendidikan');	
		$this->db->where('gp.id_guru',$id);
		return $this->db->get();
	}

	function getDetPendidikan($id=0, $id_gp){
		$this->db->select('*');
		$this->db->from('t_guru_pendidikan gp');
		$this->db->join('r_jenjang_pendidikan jp', 'gp.id_jenjang_pendidikan=jp.id_jenjang_pendidikan');	
		$this->db->where('gp.id_guru',$id);
		$this->db->where('gp.id_guru_pendidikan',$id_gp);
		return $this->db->get();
	}

	function getjenjangPendidikan(){
		$this->db->select('*');
		$this->db->from('r_jenjang_pendidikan jp'); 
		return $this->db->get();
	}

	function tambah_pendidikan($data){
		$this->db->insert('t_guru_pendidikan',$data);
	}

	function update_pendidikan($data, $id_guru_pendidikan=0){
		$this->db->where('id_guru_pendidikan',$id_guru_pendidikan);
		$this->db->update('t_guru_pendidikan',$data);
	}

	function delete_pendidikan($id_guru_pendidikan=0){
		$this->db->where('id_guru_pendidikan',$id_guru_pendidikan);
		$this->db->delete('t_guru_pendidikan');
	}
	
	// ------------ bagian pengalaman

	function getPengalaman($id=0){
	
	$this->db->select('*');
	$this->db->from('t_guru_pengalaman gp');
	$this->db->where('gp.id_guru',$id);
	return $this->db->get();
	}
	
	function getDetPengalaman($id,$id_gp){
	
		$this->db->select('*');
		$this->db->from('t_guru_pengalaman gp'); 
		$this->db->where('gp.id_guru',$id);
		$this->db->where('gp.id_guru_pengalaman',$id_gp);
		return $this->db->get();
	}

	function tambah_pengalaman($data){
			$this->db->insert('t_guru_pengalaman',$data);
	}

	function update_pengalaman($data,$id_guru_pengalaman=0){
			$this->db->where('id_guru_pengalaman',$id_guru_pengalaman);
			$this->db->update('t_guru_pengalaman',$data);
	}
	
	function delete_pengalaman($id_guru_pengalaman){
			$this->db->where('id_guru_pengalaman', $id_guru_pengalaman);
			$this->db->delete('t_guru_pengalaman');
	}	
	
	// ------------ bagian prestasi

	function getPrestasi($id=0){
	
		$this->db->select('*');
		$this->db->from('t_guru_prestasi gp');
		$this->db->join('r_tingkat_wilayah tw', 'gp.id_tingkat_wilayah=tw.id_tingkat_wilayah');	
		$this->db->where('gp.id_guru',$id);
		return $this->db->get();
	}

	function getDetPrestasi($id,$id_gp){
		$this->db->select('*');
		$this->db->from('t_guru_prestasi gp');
		$this->db->join('r_tingkat_wilayah tw', 'gp.id_tingkat_wilayah=tw.id_tingkat_wilayah');	
		$this->db->where('gp.id_guru',$id);
		$this->db->where('gp.id_guru_prestasi',$id_gp);
		return $this->db->get();
	}

	function getTingkatWilayah(){
		$this->db->select('*');
		$this->db->from('r_tingkat_wilayah tw'); 
		return $this->db->get();
	}

	function tambah_prestasi($data){
			$this->db->insert('t_guru_prestasi',$data);
	}

	function update_prestasi($data, $id_guru_prestasi=0){
			$this->db->where('id_guru_prestasi', $id_guru_prestasi);
			$this->db->update('t_guru_prestasi',$data);
	}

	function delete_prestasi($id_guru_prestasi){
			$this->db->where('id_guru_prestasi', $id_guru_prestasi);
			$this->db->delete('t_guru_prestasi');
	}
}


// public $join_to = array(
			// "r_golongan_darah gd"=>"gd.id_golongan_darah=m_guru.id_golongan_darah",
			// "r_status_pernikahan sp"=>"sp.id_status_pernikahan=m_guru.id_status_pernikahan",
			// "r_agama a"=>"a.id_agama=m_guru.id_agama",
			// "r_status_pegawai rsp"=>"rsp.id_status_pegawai=m_guru.id_status_pegawai",
			// "r_kecamatan k"=>"k.id_kecamatan=m_guru.id_kecamatan",
			// "r_jenjang_pendidikan jp"=>"jp.id_jenjang_pendidikan=m_guru.id_jenjang_pendidikan",
			// "r_pangkat_golongan pg"=>"pg.id_pangkat_golongan=m_guru.id_pangkat_golongan"
			// "m_sekolah s"=>"s.id_sekolah=m_guru.id_sekolah"
		// );
	// public $join_fields = array("golongan_darah","status_pernikahan","agama","status","kecamatan","jenjang_pendidikan","pangkat","golongan");