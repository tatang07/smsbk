<?php
class m_potongan extends MY_Model {
    public $table = "m_tunjuangan";
    public $id = "id_potongan";
	

	
	function select_data_potongan(){
		$this->db->select('*');
		$this->db->from('m_potongan');
		return $this->db->get();
	}
	
	function select_data_edit($id){
		$this->db->select('*');
		$this->db->from('m_potongan');
		$this->db->where('id_potongan',$id);
		return $this->db->get();
	}
	
	function update_detail($id,$data){
			$this->db->where('id_potongan',$id);
			$this->db->update('m_potongan',$data);
	}
	
	function select_data_potongan_detail($id){
		$this->db->select('*');
		$this->db->from('t_potongan_detail td');
		$this->db->join('m_guru g','g.id_guru=td.id_guru');
		$this->db->where('id_potongan',$id);
		return $this->db->get();
	}
	
	function delete_detail($id){
			$this->db->where('id_potongan_detail', $id);
			$this->db->delete('t_potongan_detail');
	}
	
	function get_data_detail($id_potongan=0,$id_sekolah){
			$this->db->select('nip,nama,id_potongan_detail,jumlah,gaji,besaran_uang,besaran_prosentase');
			$this->db->from('t_potongan_detail as ad');
			$this->db->join('m_potongan t', 'ad.id_potongan = t.id_potongan');
			$this->db->join('m_guru g', 'g.id_guru = ad.id_guru');
			$this->db->where('ad.id_potongan',$id_potongan);
			$this->db->where('g.id_sekolah',$id_sekolah);
			return $this->db->get();
	}
	
	function delete_detail2($id){
			$this->db->where('id_potongan', $id);
			$this->db->delete('t_potongan_detail');
	}
	
	function delete($id){
			$this->db->where('id_potongan', $id);
			$this->db->delete('m_potongan');
	}
	
	function select_data_potongan2($id){
	
			$this->db->select('*');
			$this->db->from('m_potongan');
			$this->db->where('id_sekolah',$id);
			return $this->db->get();
	}
	
	function select_guru($id){
		$this->db->select('*');
		$this->db->from('m_guru');
		$this->db->where('id_sekolah',$id);
		return $this->db->get();
	}
	
	function add($data){
		$this->db->insert('m_potongan',$data);
	}
	
	function add_detail($data){
		$this->db->insert('t_potongan_detail',$data);
	}
	
}