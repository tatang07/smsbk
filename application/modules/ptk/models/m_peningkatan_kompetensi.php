<?php
class m_peningkatan_kompetensi extends MY_Model {
	
	public function get_data_search($nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM t_guru_peningkatan_kompetensi as gpk join m_guru as g on g.id_guru = gpk.id_guru where gpk.nama_program  like '%$nama%' and g.id_sekolah=$id_sekolah");
		return $query->result_array();
	}
	public function get_data_search_detail($nama,$id_guru_peningkatan_kompetensi){
		$query = $this->db->query("SELECT * FROM t_guru_peningkatan_kompetensi_detail as gpkd join m_guru as g on g.id_guru = gpkd.id_guru where g.nama  like '%$nama%' and gpkd.id_guru_peningkatan_kompetensi=$id_guru_peningkatan_kompetensi");
		return $query->result_array();
	}
	function get_data($id_sekolah){
			$this->db->select('*');
			$this->db->from('t_guru_peningkatan_kompetensi gpk');
			$this->db->join('m_guru g','g.id_guru = gpk.id_guru');
			
			$this->db->where('g.id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}
	function get_data_guru($id_sekolah,$id_guru){
			$this->db->select('*');
			$this->db->from('t_guru_peningkatan_kompetensi gpk');
			$this->db->join('m_guru g','g.id_guru = gpk.id_guru');
			
			$this->db->where('g.id_sekolah',$id_sekolah);
			$this->db->where('g.id_guru',$id_guru);
			
			return $this->db->get()->result_array();
	}
	function get_data_detail($id_guru_peningkatan_kompetensi){
			$this->db->select('*');
			$this->db->from('t_guru_peningkatan_kompetensi_detail gpkd');
			$this->db->join('m_guru g','g.id_guru = gpkd.id_guru');
			
			$this->db->where('gpkd.id_guru_peningkatan_kompetensi',$id_guru_peningkatan_kompetensi);
			
			return $this->db->get()->result_array();
	}
	function get_data_detail_guru($id_guru_peningkatan_kompetensi,$id_guru){
			$this->db->select('*');
			$this->db->from('t_guru_peningkatan_kompetensi_detail gpkd');
			$this->db->join('m_guru g','g.id_guru = gpkd.id_guru');
			
			$this->db->where('gpkd.id_guru_peningkatan_kompetensi',$id_guru_peningkatan_kompetensi);
			$this->db->where('g.id_guru',$id_guru);
			
			return $this->db->get()->result_array();
	}
	function get_data_edit($id_guru_peningkatan_kompetensi){
			$this->db->select('*');
			$this->db->from('t_guru_peningkatan_kompetensi gpk');
			$this->db->join('m_guru g','g.id_guru = gpk.id_guru');
			// $this->db->join('r_jenjang_pendidikan jp','g.id_jenjang_pendidikan = jp.id_jenjang_pendidikan');
			$this->db->where('gpk.id_guru_peningkatan_kompetensi',$id_guru_peningkatan_kompetensi);
			
			return $this->db->get()->result_array();
	}
	
	
	function select_guru($id_sekolah){
			$this->db->select('*');
			$this->db->from('m_guru');
			$this->db->where('id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}

	function add($data){
			$this->db->insert('t_guru_peningkatan_kompetensi',$data);
	}
	
	function add_detail($data){
			$this->db->insert('t_guru_peningkatan_kompetensi_detail',$data);
	}
	function edit($data,$id=0){
			$this->db->where('id_guru_peningkatan_kompetensi',$id);
			$this->db->update('t_guru_peningkatan_kompetensi',$data);
	}

	function delete($id_guru_peningkatan_kompetensi){
			$this->db->where('id_guru_peningkatan_kompetensi', $id_guru_peningkatan_kompetensi);
			$this->db->delete('t_guru_peningkatan_kompetensi');
	}
	function delete_detail($id_guru_peningkatan_kompetensi_detail){
			$this->db->where('id_guru_peningkatan_kompetensi_detail', $id_guru_peningkatan_kompetensi_detail);
			$this->db->delete('t_guru_peningkatan_kompetensi_detail');
	}
}