<?php
class m_penugasan_dan_penentuan_beban_mengajar_guru extends MY_Model {
    public $table = "m_guru";
    public $id = "id_guru";
	
	function insert_isi($data){
			$this->db->insert($this->table, $data);
	}
	
	function select_data($id_sekolah,$id_tahun_ajaran,$id_guru){
	
			$this->db->select('*');
			$this->db->from('t_guru_matpel_rombel gmr');
			$this->db->join('m_rombel r', 'r.id_rombel = gmr.id_rombel');
			$this->db->join('m_pelajaran p', 'p.id_pelajaran = gmr.id_pelajaran');
			$this->db->where('r.id_sekolah', $id_sekolah);
			$this->db->where('r.id_tahun_ajaran', $id_tahun_ajaran);
			$this->db->where('gmr.id_guru', $id_guru);
			
			return $this->db->get();
		}
	function get_jumlah_jam($id_pel,$pel){
			$this->db->select('*');
			$this->db->from('m_alokasi_waktu_per_minggu awpm');
			//$this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_pelajaran = awpm.id_pelajaran');
			$this->db->where_in('awpm.id_tingkat_kelas', $pel );
			$this->db->where('awpm.id_pelajaran',$id_pel);
			return $this->db->get()->result_array();
	
	}
	
	function hitung_beban(){
			$this->db->select('*');
			$this->db->from('m_alokasi_waktu_per_minggu awpm');
			$this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_pelajaran = awpm.id_pelajaran');
			return $this->db->get()->result_array();
	
	}

	function get_search_nama(){
		$query = $this->db->query(
					"SELECT * 
					 FROM m_guru
					 JOIN r_jenjang_pendidikan ON (r_jenjang_pendidikan.id_jenjang_pendidikan = m_guru.id_jenjang_pendidikan)
					 JOIN t_guru_matpel_rombel ON (t_guru_matpel_rombel.id_guru = m_guru.id_guru)
					 JOIN m_rombel ON (m_rombel.id_rombel = t_guru_matpel_rombel.id_rombel)
					 WHERE 
					 m.guru.nama = $parameter
					 ;"
				 );
	} 
	
	public function get_data_search($id,$nama){
		$query = $this->db->query("SELECT *,count(gmr.id_pelajaran) s ,sum(jumlah_jam) as jml FROM t_guru_matpel_rombel gmr join m_guru as g on gmr.id_guru=g.id_guru join r_jenjang_pendidikan as jp on jp.id_jenjang_pendidikan = g.id_jenjang_pendidikan join m_rombel as r on r.id_rombel = gmr.id_rombel join m_pelajaran as p on p.id_pelajaran = gmr.id_pelajaran where g.id_sekolah=$id and g.nama like '%$nama%' ");
		return $query->result_array();
	}
	// function hitung_beban_mengajar(){
	
			// $this->db->select('*,count(gmr.id_pelajaran) s ');
			
			// $this->db->from('m_guru');
			// $this->db->join('r_jenjang_pendidikan jp', 'jp.id_jenjang_pendidikan = m_guru.id_jenjang_pendidikan');
			// $this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_guru = m_guru.id_guru');
			
			// $this->db->group_by("gmr.id_guru");
			// return $this->db->get();
		// }
	// function select_isi_by_id($id){
			// $this->db->select('*');
			// $this->db->from('t_coba');
			// $this->db->where('id_coba', $id);
			// return $this->db->get();
		// }
}