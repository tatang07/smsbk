<?php
class m_penugasan_dan_penentuan_beban_mengajar extends MY_Model {
    public $table = "m_guru";
    public $id = "id_guru";
	
	function insert_isi($data){
			$this->db->insert($this->table, $data);
	}
	
	function select_data(){
	
			$this->db->select('*,count(gmr.id_pelajaran) s ');
			//$this->db->like('id_pangkat_golongan', '1');
			$this->db->from('m_guru');
			$this->db->join('r_jenjang_pendidikan jp', 'jp.id_jenjang_pendidikan = m_guru.id_jenjang_pendidikan');
			$this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_guru = m_guru.id_guru');
			$this->db->join('m_rombel r', 'r.id_rombel = gmr.id_rombel');
			//$this->db->join('m_alokasi_aktu_per_minggu awpm', 'awpm.id_pelajaran = gmr.id_pelajaran');
			$this->db->group_by("gmr.id_guru");
			return $this->db->get();
		}
	function get_jumlah_jam($id_pel,$pel){
			$this->db->select('*');
			$this->db->from('m_alokasi_waktu_per_minggu awpm');
			//$this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_pelajaran = awpm.id_pelajaran');
			$this->db->where_in('awpm.id_tingkat_kelas', $pel );
			$this->db->where('awpm.id_pelajaran',$id_pel);
			return $this->db->get()->result_array();
	
	}
	// function hitung_beban_mengajar(){
	
			// $this->db->select('*,count(gmr.id_pelajaran) s ');
			
			// $this->db->from('m_guru');
			// $this->db->join('r_jenjang_pendidikan jp', 'jp.id_jenjang_pendidikan = m_guru.id_jenjang_pendidikan');
			// $this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_guru = m_guru.id_guru');
			
			// $this->db->group_by("gmr.id_guru");
			// return $this->db->get();
		// }
	// function select_isi_by_id($id){
			// $this->db->select('*');
			// $this->db->from('t_coba');
			// $this->db->where('id_coba', $id);
			// return $this->db->get();
		// }
}