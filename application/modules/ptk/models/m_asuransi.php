<?php
class m_asuransi extends MY_Model {
    public $table = "m_asuransi";
    public $id = "id_asuransi";
	
	function select_data_asuransi($id){
			$this->db->select('*');
			$this->db->from('m_asuransi');
			$this->db->where('id_sekolah',$id);
			return $this->db->get();
	}
		
	function select_guru($id){
		$this->db->select('*');
		$this->db->from('m_guru');
		$this->db->where('id_sekolah',$id);
		return $this->db->get();
	}
	
	function add_detail($data){
		$this->db->insert('t_asuransi_detail',$data);
	}
	
	function get_data_detail($id_asuransi=0,$id_sekolah){
	
			$this->db->select('nip,nama,id_asuransi_detail');
			$this->db->from('t_asuransi_detail as ad');
			$this->db->join('m_guru g', 'g.id_guru = ad.id_guru');
			$this->db->where('ad.id_asuransi',$id_asuransi);
			$this->db->where('g.id_sekolah',$id_sekolah);
			return $this->db->get();
	}
	
	function delete($id_asuransi_detail){
			$this->db->where('id_asuransi_detail', $id_asuransi_detail);
			$this->db->delete('t_asuransi_detail');
	}
	
	function delete_utama($id_asuransi){
			$this->db->where('id_asuransi', $id_asuransi);
			$this->db->delete('m_asuransi');
	}
	
	function add($data){
		$this->db->insert('m_asuransi',$data);
	}
}