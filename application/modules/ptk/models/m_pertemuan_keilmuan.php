<?php
class m_pertemuan_keilmuan extends MY_Model {
	
	public function get_data_search($nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM t_guru_pertemuan_keilmuan as gpk join m_guru as g on g.id_guru = gpk.id_guru where g.nama  like '%$nama%' and g.id_sekolah=$id_sekolah");
		return $query->result_array();
	}
	public function get_data_search_default($nama,$id_sekolah){
		$query = $this->db->query("SELECT * FROM t_guru_pertemuan_keilmuan as gpk join m_guru as g on g.id_guru = gpk.id_guru where g.nama  like '%$nama%' and g.id_sekolah=$id_sekolah");
		return $query->result_array();
	}
	function get_data($id_sekolah){
			$this->db->select('*');
			$this->db->from('t_guru_pertemuan_keilmuan gpk');
			$this->db->join('m_guru g','g.id_guru = gpk.id_guru');
			
			$this->db->where('g.id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}
	function get_data_guru($id_sekolah,$id_guru){
			$this->db->select('*');
			$this->db->from('t_guru_pertemuan_keilmuan gpk');
			$this->db->join('m_guru g','g.id_guru = gpk.id_guru');
			
			$this->db->where('g.id_sekolah',$id_sekolah);
			$this->db->where('g.id_guru',$id_guru);
			
			return $this->db->get()->result_array();
	}
	function get_siswa($id_sekolah,$id_tingkat_kelas){
			$this->db->select('*');
			$this->db->from('t_rombel_detail rd');
			$this->db->join('m_siswa s','s.id_siswa = rd.id_siswa');		
			$this->db->join('m_rombel r','r.id_rombel = rd.id_rombel');
			$this->db->where('s.id_sekolah',$id_sekolah);
			$this->db->where('r.id_tingkat_kelas',$id_tingkat_kelas);
			return $this->db->get()->result_array();
	}

	function get_data_edit($id_guru_pertemuan_keilmuan){
			$this->db->select('*');
			$this->db->from('t_guru_pertemuan_keilmuan gpk');
			$this->db->join('m_guru g','g.id_guru = gpk.id_guru');
			// $this->db->join('r_jenjang_pendidikan jp','g.id_jenjang_pendidikan = jp.id_jenjang_pendidikan');
			$this->db->where('gpk.id_guru_pertemuan_keilmuan',$id_guru_pertemuan_keilmuan);
			
			return $this->db->get()->result_array();
	}
	
	
	function select_guru($id_sekolah){
			$this->db->select('*');
			$this->db->from('m_guru');
			$this->db->where('id_sekolah',$id_sekolah);
			
			return $this->db->get()->result_array();
	}

	function add($data){
			$this->db->insert('t_guru_pertemuan_keilmuan',$data);
	}

	function edit($data,$id=0){
			$this->db->where('id_guru_pertemuan_keilmuan',$id);
			$this->db->update('t_guru_pertemuan_keilmuan',$data);
	}

	function delete($id_guru_pertemuan_keilmuan){
			$this->db->where('id_guru_pertemuan_keilmuan', $id_guru_pertemuan_keilmuan);
			$this->db->delete('t_guru_pertemuan_keilmuan');
	}

}