<?php
class m_kualifikasi_pangkat extends MY_Model {
    public $table = "m_guru";
    public $id = "id_guru";
	
	function insert_isi($data){
			$this->db->insert($this->table, $data);
	}
	
	function select_golongan($id){
			$this->db->select('*,count(m_guru.id_pangkat_golongan) s');
			//$this->db->like('id_pangkat_golongan', '1');
			$this->db->from('m_guru');
			$this->db->join('r_pangkat_golongan pg', 'pg.id_pangkat_golongan = m_guru.id_pangkat_golongan');
			$this->db->where('m_guru.id_sekolah', $id);
			$this->db->group_by("pg.id_pangkat_golongan");
			return $this->db->get();
		}
	// function select_isi_by_id($id){
			// $this->db->select('*');
			// $this->db->from('t_coba');
			// $this->db->where('id_coba', $id);
			// return $this->db->get();
		// }
}