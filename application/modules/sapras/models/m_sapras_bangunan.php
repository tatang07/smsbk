<?php
class m_sapras_bangunan extends MY_Model {
    public $table = "m_sapras_bangunan";
    public $id = "id_sapras_bangunan";

    //join key
	public $join_to = array(
							"r_kondisi k"=>"k.id_kondisi=m_sapras_bangunan.id_kondisi",
							"m_sapras_tanah st"=>"st.id_sapras_tanah=m_sapras_bangunan.id_sapras_tanah",
					  );
	
	//join field
	public $join_fields = array(
							"k.kondisi kondisi",
						  );
	
	function get_tanah(){
		$ids=get_id_sekolah();
		$tanah = get_list_filter("m_sapras_tanah","id_sapras_tanah",array("alamat"),array("id_sekolah"=>$ids), array("alamat"));
		return $tanah;
	}
}