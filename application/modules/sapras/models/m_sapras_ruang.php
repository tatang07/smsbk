<?php
class m_sapras_ruang extends MY_Model {
    public $table = "m_sapras_ruang";
    public $id = "id_sapras_ruang";
	
	//join key
	public $join_to = array(
							"r_kondisi k"=>"k.id_kondisi=m_sapras_ruang.id_kondisi",
							"r_kategori_ruang kr"=>"kr.id_kategori_ruang=m_sapras_ruang.id_kategori_ruang",
							"m_sapras_bangunan sb"=>"sb.id_sapras_bangunan=m_sapras_ruang.id_sapras_bangunan",
							"m_sapras_tanah st"=>"st.id_sapras_tanah=sb.id_sapras_tanah",
					  );
	
	//join field
	public $join_fields = array(
							"k.kondisi kondisi",
							"kr.kategori_ruang kategori_ruang",
						  );
	
	// function get_bangunan(){
	// 	$ids=get_id_sekolah();
	// 	$tanah = get_list_filter("m_sapras_tanah","id_sapras_tanah",array("alamat"),array("id_sekolah"=>$ids), array("alamat"));
	// 	return $tanah;
	// }

	function get_bangunan(){
		$this->db->select('sb.id_sapras_bangunan, sb.nama');
		$this->db->from('m_sapras_bangunan sb'); 
		$this->db->join('m_sapras_tanah st', 'st.id_sapras_tanah=sb.id_sapras_tanah');
		$this->db->where('st.id_sekolah',get_id_sekolah());
		$this->db->order_by('sb.nama','asc');
		$query = $this->db->get();
		// echo $this->db->last_query();
		// exit;
		
		$bangunan = $query->result_array();
		$temp = array();
		foreach ($bangunan as $b) {
			$temp[$b['id_sapras_bangunan']] = $b['nama'];
		}
		return $temp;

		// if($query->num_rows() != 0){
		// 	$bangunan = $query->result_array();
		// 	$temp = array();
		// 	foreach ($bangunan as $b) {
		// 		$temp[$b['id_sapras_bangunan']] = $b['nama'];
		// 	}
		// 	return $temp;
		// }else{
		// 	return false;
		// }
		
	}
}