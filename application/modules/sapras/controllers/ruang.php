<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class ruang extends Simple_Controller {
    	public $myconf = array(
			//Nama controller
			"name" 			=> "sapras/ruang",
			"component"		=> "sapras",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "Sarana Prasarana",
			"subtitle"		=> "Ruangan",
			//"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			//"table"			=> array("name"=>"m_sekolah", "id"=>"id_sekolah"),
			"model_name"	=> "m_sapras_ruang",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(		
								"id_sapras_bangunan"=>"Nama Bangunan",
								"nama"=>"Nama Ruangan",
								"luas"=>"Luas (m<sup>2</sup>)",
								"id_kategori_ruang"=>"Kategori Ruangan",
								"posisi_lantai"=>"Lantai",
								"kategori_ruang"=>"Kategori",
								"kapasitas"=>"Kapasitas",
								"id_kondisi"=>"Kondisi",
								"kondisi"=>"Kondisi",
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
								"id_kondisi"=>array("r_kondisi","id_kondisi","kondisi"),
								"id_kategori_ruang"=>array("r_kategori_ruang","id_kategori_ruang","kategori_ruang"),
								// "id_asal_sekolah"=>array("r_asal_sekolah","id_asal_sekolah","nama_sekolah"),
								//"status_aktif"=>array("list"=>array("1"=>"Aktif","0"=>"Tidak Aktif")),
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
								"id_sapras_bangunan","nama","kapasitas","luas", "id_kondisi"
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			// "data_unique"	=> array(
			// 					"telepon"
			// 				),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"model_name"			=> "m_sapras_ruang",
								//"table"					=> array(),
								//"view_file"				=> "simple/datalist",
								//"view_file_datatable"	=> "simple/datatable",
								//"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array("nama","luas","kategori_ruang","kondisi"),
								"field_sort"			=> array("nama","luas","kategori_ruang","kondisi"),
								"field_filter"			=> array("m_sapras_ruang.nama",),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown" => array("id_sapras_bangunan"=>array("label"=>"Nama Banguan",array())),
								"field_comparator"		=> array("id_sapras_bangunan"=>"=","id_sekolah"=>"="),
								"field_operator"		=> array("id_sapras_bangunan"=>"AND","id_sekolah"=>"AND"),
								"field_align"			=> array("luas"=>"center"),
								// "field_filter_dropdown" => array("id_rombel"=>array("label"=>"Kelas",array())),
								// "field_size"			=> array("action_col"=>"150"),
								//"field_separated"		=> array(),
								//"field_sum"				=> array(),
								//"field_summary"			=> array(),
								//"enable_action"			=> TRUE,
								// "custom_action_link"	=> array('detail'=>'pps/siswa/detail_siswa'),					
								//"custom_add_link"		=> array('label'=>'Add','href'=>'school/add' ),
								// "custom_edit_link"		=> array('edit'=>'pps/siswa/edit'),
								//"custom_delete_link"	=> array(),
								// "custom_link"			=> array("Import" => "import/siswa"),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								"subtitle"		=> "Tambah Ruangan",
								//"enable"				=> TRUE,
								//"model_name"			=> "",
								//"table"					=> array(),
								//"view_file"				=> "simple/add",
								// "view_file_form_input"	=> "form_input",
								"field_list"			=> array("id_sapras_bangunan","nama","luas","id_kategori_ruang","posisi_lantai","kapasitas","id_kondisi"),
								//"custom_action"			=> "",
								//"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								//"msg_on_success"		=> "Data berhasil disimpan.",
								//"redirect_link"			=> ""
							),
			//Konfigurasi tampilan edit
			"data_edit"		=> array(
								//"title"			=> "simple",
								"subtitle"		=> "Edit Ruangan",
								// "enable"				=> TRUE,
								// "model_name"			=> "",
								// "table"					=> array(),
								// "view_file"				=> "simple/edit",
								//"view_file_form_input"	=> "form_input",
								// "field_list"			=> array(),
								// "custom_action"			=> "",
								// "msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								// "msg_on_success"		=> "Data berhasil diperbaharui.",
								// "redirect_link"			=> ""
							),
			//Konfigurasi tampilan delete
			/*"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)*/
		);
	

		public function pre_process($data){
			//Method ini akan dieksekusi di setiap awal method datalist,datatable,add,edit,delete
			//Merubah subtitle di masing-masing jenis pihak_komunikasi agar sesuai
			
			
			$data['conf']['data_delete']['redirect_link'] = "sapras/ruang/datalist/";
			$data['conf']['data_add']['redirect_link'] = "sapras/ruang/datalist/";
			$data['conf']['data_edit']['redirect_link'] = "sapras/ruang/datalist/";

			return $data;
		}
		
		public function before_datalist($data){
			//Method ini hanya akan dieksekusi di awal method datalist
			//$_POST['filter']['id_sekolah'] = get_id_sekolah();
			//Membuat input hidden di form datalist sebagai filter yang akan digunakan oleh ajax saat 
			//mengupdate datatable baik ketika search ataupun pindah page
			
			$data['conf']['data_list']["field_filter_dropdown"]	= array("id_sapras_bangunan"=>array("label"=>"Nama Bangunan","list"=>$this->m_sapras_ruang->get_bangunan()));

			$_POST['filter']['id_sekolah'] = get_id_sekolah();
			//Memanipulasi parameter page
			$page = $this->uri->total_rsegments()>3? $this->uri->rsegment(4): 1;
			$data['page'] = $page;
			//Mengkustomisasi link tombol edit dan delete
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'sapras/ruang/edit/' );
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'sapras/ruang/delete/' );
			
			return $data;
		}
		
		public function before_datatable($data){
			//Method ini hanya akan dieksekusi di awal method datatable
			//Mengambil id_pihak yang diposting dari form datalist
			$filter = $this->input->post("filter");
			
			//$id_siswa = $filter['id_sekolah'];
			$_POST['filter']['id_sekolah'] = get_id_sekolah();
			//Mengkustomisasi link tombol edit dan delete
			//$_POST['filter']['id_sekolah'] = get_id_sekolah();
			
			$data['conf']['data_list']['custom_edit_link'] = array('label'=>'Edit','href'=>'sapras/ruang/edit/' );
			$data['conf']['data_list']['custom_delete_link'] = array('label'=>'Delete','href'=>'sapras/ruang/delete/' );
			return $data;
		}
		
				
		public function before_add($data){
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
				//$data['item']['id_sekolah'] = get_id_sekolah();
			}
			
			$data['conf']['data_type']['id_sapras_bangunan']=array("list"=>$this->m_sapras_ruang->get_bangunan());
			return $data;
		}

		public function before_edit($data){
			if($this->input->post()){
				//menambahkan satu field id_sekolah sebelum data disimpan ke db
				//$data['item']['id_sekolah'] = get_id_sekolah();
			}
			
			$data['conf']['data_type']['id_sapras_bangunan']=array("list"=>$this->m_sapras_ruang->get_bangunan());
			return $data;
		}
		
		public function before_delete($data){
			$data['id'] = $this->uri->rsegment(3);
			return $data;
		}
		
}
