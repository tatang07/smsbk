<?php
class m_information extends MY_Model {
    function update($data, $id_sekolah, $information_name){
		$this->db->where('id_sekolah',$id_sekolah);
		$this->db->where('information_name',$information_name);
		$this->db->update('sys_information',$data);
	}

	function add($data){
			$this->db->insert('sys_information',$data);
	}

}