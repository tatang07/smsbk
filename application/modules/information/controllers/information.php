<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class information extends Simple_Controller {
    public function tambah($ppdb_pendaftaran){
		$data['information_name'] = $ppdb_pendaftaran;
		render("information/tambah",$data);
	}
	public function edit($ppdb_pendaftaran){
		$data['information_name'] = $ppdb_pendaftaran;
		$data['info'] = get_information_sekolah($ppdb_pendaftaran);
		render("information/edit",$data);
	}
	public function submit_tambah (){
		$this->load->model('m_information');

		$data['information_label'] = $this->input->post('information_label');
		$data['information_data'] = $this->input->post('information_data');
		$data['information_name'] = $this->input->post('information_name');
		$information_name = $this->input->post('information_name');
		$data['id_sekolah'] = get_id_sekolah();

		$this->m_information->add($data);

		set_success_message('Data Berhasil Ditambah!');
		redirect('information/edit/'. $information_name);
	}
	public function submit_edit (){
		$this->load->model('m_information');

		$data['information_label'] = $this->input->post('information_label');
		$data['information_data'] = $this->input->post('information_data');

		$information_name = $this->input->post('information_name');
		$id_sekolah = get_id_sekolah();

		$this->m_information->update($data, $id_sekolah, $information_name);

		set_success_message('Data Berhasil Diupdate!');
		redirect('information/edit/'. $information_name);
	}
}
