<h1 class="grid_12">Seleksi dan Penerimaan Siswa Baru</h1>

<form action="<?php echo base_url('information/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
	<fieldset>
		<legend>Edit Informasi Pendaftaran</legend>
		<div class="row">
			<label for="f1_normal_input">
				<strong>Label</strong>
			</label>
			<div>
				<input type="text" name="information_label" value="<?php echo $info['information_label'] ?>" />
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Informasi</strong>
			</label>
			<div>
				<textarea name="information_data" rows="10" cols="30"><?php echo $info['information_data'] ?></textarea>				
			</div>
		</div>

		<input type="hidden" name="information_name" value="<?php echo $information_name; ?>">
	</fieldset>
	
	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" name=send />
			 <a href=""> <input value="Batal" type="button"></a>
		</div>
		<div class="right">
		</div>
	</div><!-- End of .actions -->
</form><!-- End of .box -->
