<?php
class t_siswa extends MY_Model {
    public $table = "m_siswa";
    public $id = "id_siswa";
	
	function getdetail($id=0){
		$this->db->select('*');
		$this->db->from('m_siswa sa'); 
		$this->db->join('r_golongan_darah gd', 'gd.id_golongan_darah=sa.id_golongan_darah', 'left');
		$this->db->join('r_agama a', 'a.id_agama=sa.id_agama', 'left');
		$this->db->join('r_status_anak rsa', 'rsa.id_status_anak=sa.id_status_anak', 'left');
		$this->db->join('r_kecamatan k', 'k.id_kecamatan=sa.id_kecamatan', 'left');
		$this->db->join('r_asal_sekolah ras', 'ras.id_asal_sekolah=sa.id_asal_sekolah', 'left');
		$this->db->join('m_sekolah ms', 'ms.id_sekolah=sa.id_sekolah', 'left');
		$this->db->where('sa.id_siswa',$id);
		$this->db->order_by('sa.nis','asc');         
		$query = $this->db->get();
		
		if($query->num_rows() != 0)
		{
			return $query->result_array();
		}
		else
		{
			return false;
		}
	
	}
}