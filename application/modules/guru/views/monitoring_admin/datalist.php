<h1 class="grid_12">Monitoring Data Guru</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>Rekapitulasi Pemasukan Data Guru</h2>
		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>NIP</th>
								<th>Nama Guru</th>
								<th>Jumlah RPP</th>
								<th>Jumlah Agenda</th>
								<th>Jumlah Riwayat</th>
								<th>Aksi</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1;$total=0; ?>
							<?php foreach($guru as $g){ ?>
								<?php $this->load->model('m_monitoring_admin'); ?>
								
								<?php $rpp = $this->m_monitoring_admin->get_rpp($g['id_guru'])->result_array(); ?>
								
								<?php $agenda = $this->m_monitoring_admin->get_agenda($g['id_guru'])->result_array(); ?>
								
								<?php $jabatan = $this->m_monitoring_admin->get_jabatan($g['id_guru'])->result_array(); ?>
								<?php $penataran = $this->m_monitoring_admin->get_penataran($g['id_guru'])->result_array(); ?>
								<?php $pendidikan = $this->m_monitoring_admin->get_pendidikan($g['id_guru'])->result_array(); ?>
								<?php $pengalaman = $this->m_monitoring_admin->get_pengalaman($g['id_guru'])->result_array(); ?>
								<?php $prestasi = $this->m_monitoring_admin->get_prestasi($g['id_guru'])->result_array(); ?>
								
								<tr>
									<td class="center" width="25px"><?php echo $i; $i++; ?></td>
									<td ><?php echo $g['nip'] ?></td>
									<td ><?php echo $g['nama'] ?></td>
									<td align="right" width="100px"><?php echo $rpp[0]['jumlah']; ?></td>
									<td align="right" width="100px"><?php echo $agenda[0]['jumlah'] ?></td>
									<td align="right" width="100px"><?php echo $jabatan[0]['jumlah']+$penataran[0]['jumlah']+$pendidikan[0]['jumlah']+$pengalaman[0]['jumlah']+$prestasi[0]['jumlah'] ?></td>
									<td class="center" width="50px">
										<a href="<?php echo site_url('guru/monitoring_data_guru/datalist/'.$g['id_guru'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-share-alt"></i> detail</a>									
									</td>
								</tr>
							<?php } ?>					
						</tbody>
					</table>
					
					<div class="footer">
						
					</div>
				</div>
			</div>
		</div>       
	</div>
</div>