<h1 class="grid_12">Monitoring Data Guru</h1>
<div class="grid_12">

	<div class="box with-table">
		<div class="header">
			<h2>RPP</h2>
		</div>
	
		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Mata Pelajaran</th>
								<th>Kelas</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1;$total=0; ?>
							<?php foreach($guru_ampu as $g){ ?>
								<?php $this->load->model('m_monitoring_data_guru'); ?>
								<?php $id_guru = $guru ?>
								<?php $data['rpp'] = $this->m_monitoring_data_guru->get_rpp($g['id_pelajaran'], $id_guru)->result_array(); ?>
								<?php $jum_rpp = $data['rpp']; ?>
								<tr>
									<td class="center" width="25px"><?php echo $i; $i++; ?></td>
									<td ><?php echo $g['pelajaran'] ?></td>
									<td width="50px"><?php echo $g['rombel'] ?></td>
									<td align="right" width="50px"><?php echo $jum_rpp[0]['jumlah'] ?></td>
									<?php $total = $total + $jum_rpp[0]['jumlah']; ?>
								</tr>
							<?php } ?>
								<tr>
									<td colspan = '3' class="center"><b>Total</b></td>
									<td align="right"><?php echo $total ?></td>
								</tr>						
						</tbody>
					</table>
					
					<div class="footer">
						
					</div>
				</div>
			</div>
		</div> 

		<div class="header">
			<h2>Agenda Kelas</h2>
		</div> 

		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<thead>
							<tr>
								<th>No.</th>
								<th>Mata Pelajaran</th>
								<th>Kelas</th>
								<th>Jumlah</th>
							</tr>
						</thead>
						<tbody>
							<?php $i=1;$total=0; ?>
							<?php foreach($guru_ampu as $g){ ?>
								<?php $this->load->model('m_monitoring_data_guru'); ?>
								<?php $id_guru = $guru ?>								
								<?php $data['agenda'] = $this->m_monitoring_data_guru->get_agenda($g['id_pelajaran'], $id_guru)->result_array(); ?>
								<?php $jum_agenda = $data['agenda']; ?>
								<tr>
									<td class="center" width="25px"><?php echo $i; $i++; ?></td>
									<td ><?php echo $g['pelajaran'] ?></td>
									<td width="50px"><?php echo $g['rombel'] ?></td>
									<td align="right" width="50px"><?php echo $jum_agenda[0]['jumlah'] ?></td>
									<?php $total = $total + $jum_agenda[0]['jumlah']; ?>
								</tr>
							<?php } ?>
								<tr>
									<td colspan = '3' class="center"><b>Total</b></td>
									<td align="right"><?php echo $total ?></td>
								</tr>						
						</tbody>
					</table>
					
					<div class="footer">
						
					</div>
				</div>
			</div>
		</div> 

		<div class="header">
			<h2>Profil</h2>
		</div> 

		<div class="content">
			<div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled" >
						<tr>
							<th>No.</th>
							<th>Riwayat</th>
							<th>Jumlah</th>
						</tr>
						<tbody>
							<tr>
								<td class="center" width="25px"> 1 </td>
								<td >Riwayat Jabatan</td>
								<td align="right" width="50px"> <?php echo $jabatan['0']['jumlah']; ?> </td>
							</tr>
							<tr>
								<td class="center" width="25px"> 2 </td>
								<td >Riwayat Penataran</td>
								<td align="right" width="50px"> <?php echo $penataran['0']['jumlah']; ?> </td>
							</tr>
							<tr>
								<td class="center" width="25px"> 3 </td>
								<td >Riwayat Pendidikan</td>
								<td align="right" width="50px"> <?php echo $pendidikan['0']['jumlah']; ?> </td>
							</tr>
							<tr>
								<td class="center" width="25px"> 4 </td>
								<td >Riwayat Pengalaman</td>
								<td align="right" width="50px"> <?php echo $pengalaman['0']['jumlah']; ?> </td>
							</tr>
							<tr>
								<td class="center" width="25px"> 5 </td>
								<td >Riwayat Prestasi</td>
								<td align="right" width="50px"> <?php echo $prestasi['0']['jumlah']; ?> </td>
							</tr>
							<tr>
								<?php $total = $jabatan['0']['jumlah']+$penataran['0']['jumlah']+$pendidikan['0']['jumlah']+$pengalaman['0']['jumlah']+$prestasi['0']['jumlah']; ?>
								<td colspan = '2' class="center"><b>Total</b></td>
								<td align="right"><?php echo $total ?></td>
							</tr>						
						</tbody>
					</table>
					
					<div class="footer">
						
					</div>
				</div>
			</div>
		</div>       
	</div>
</div>