<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class monitoring_data_guru extends Simple_Controller {    	
	function datalist($id_guru){
		$this->load->model('m_monitoring_data_guru');
		if($id_guru==0)
			$id_guru = get_id_personal();
		
		$data['guru'] = $id_guru;
		$data['guru_ampu'] = $this->m_monitoring_data_guru->get_mapel_ampu($id_guru)->result_array();

		$data['jabatan'] = $this->m_monitoring_data_guru->get_jabatan($id_guru)->result_array();
		$data['penataran'] = $this->m_monitoring_data_guru->get_penataran($id_guru)->result_array();
		$data['pendidikan'] = $this->m_monitoring_data_guru->get_pendidikan($id_guru)->result_array();
		$data['pengalaman'] = $this->m_monitoring_data_guru->get_pengalaman($id_guru)->result_array();
		$data['prestasi'] = $this->m_monitoring_data_guru->get_prestasi($id_guru)->result_array();
		
		render('guru/datalist', $data);
	}
}
