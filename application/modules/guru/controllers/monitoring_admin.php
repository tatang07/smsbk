<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class monitoring_admin extends Simple_Controller {    	
	public function index(){
		$this->load->model('m_monitoring_admin');
		$id_sekolah = get_id_sekolah();
		$data['guru'] = $this->m_monitoring_admin->get_guru($id_sekolah)->result_array();
		render('monitoring_admin/datalist', $data);
	}
}
