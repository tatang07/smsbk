<?php
class m_monitoring_admin extends MY_Model {
    function get_guru($id){
    	$this->db->select('id_guru, nama, nip');
    	$this->db->from('m_guru');
        $this->db->where('id_sekolah', $id);
        return $this->db->get();        
    }
    function get_rpp($id){
        $this->db->select('count(r.id_rpp) jumlah');
        $this->db->from('t_rpp r');
        $this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_guru_matpel_rombel = r.id_guru_matpel_rombel');
        $this->db->where('gmr.id_guru', $id);
        return $this->db->get();
    }

    function get_agenda($id){
    	$this->db->select('count(ak.id_agenda_kelas) jumlah');
    	$this->db->from('t_agenda_kelas ak');
		$this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_guru_matpel_rombel = ak.id_guru_matpel_rombel');
		$this->db->where('gmr.id_guru', $id);
		return $this->db->get();
    }

    function get_jabatan($id){
    	$this->db->select('count(id_guru_jabatan) jumlah');
    	$this->db->from('t_guru_jabatan');
    	$this->db->where('id_guru', $id);
		return $this->db->get();
    }

    function get_penataran($id){
    	$this->db->select('count(id_guru_penataran) jumlah');
    	$this->db->from('t_guru_penataran');
    	$this->db->where('id_guru', $id);
		return $this->db->get();
    }

    function get_pendidikan($id){
    	$this->db->select('count(id_guru_pendidikan) jumlah');
    	$this->db->from('t_guru_pendidikan');
    	$this->db->where('id_guru', $id);
		return $this->db->get();
    }

    function get_pengalaman($id){
    	$this->db->select('count(id_guru_pengalaman) jumlah');
    	$this->db->from('t_guru_pengalaman');
    	$this->db->where('id_guru', $id);
		return $this->db->get();
    }

    function get_prestasi($id){
    	$this->db->select('count(id_guru_prestasi) jumlah');
    	$this->db->from('t_guru_prestasi');
    	$this->db->where('id_guru', $id);
		return $this->db->get();
    }
}