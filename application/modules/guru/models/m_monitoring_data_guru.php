<?php
class m_monitoring_data_guru extends MY_Model {
    function get_mapel_ampu($id){
    	$this->db->select('p.id_pelajaran, p.pelajaran, g.nama, r.rombel');
    	$this->db->from('t_guru_matpel_rombel gmr');
		$this->db->join('m_pelajaran p', 'gmr.id_pelajaran = p.id_pelajaran');
		$this->db->join('m_guru g', 'g.id_guru = gmr.id_guru');
		$this->db->join('m_rombel r', 'r.id_rombel = gmr.id_rombel');
		$this->db->where('gmr.id_guru', $id);
		return $this->db->get();
    }

    function get_rpp($id, $id_guru){
    	$this->db->select('count(r.id_rpp) jumlah');
    	$this->db->from('t_rpp r');
		$this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_guru_matpel_rombel = r.id_guru_matpel_rombel');
		$this->db->where('gmr.id_pelajaran', $id);
		$this->db->where('gmr.id_guru', $id_guru);
		return $this->db->get();
    }

    function get_agenda($id, $id_guru){
    	$this->db->select('count(ak.id_agenda_kelas) jumlah');
    	$this->db->from('t_agenda_kelas ak');
		$this->db->join('t_guru_matpel_rombel gmr', 'gmr.id_guru_matpel_rombel = ak.id_guru_matpel_rombel');
		$this->db->where('gmr.id_pelajaran', $id);
		$this->db->where('gmr.id_guru', $id_guru);
		return $this->db->get();
    }

    function get_jabatan($id){
    	$this->db->select('count(id_guru_jabatan) jumlah');
    	$this->db->from('t_guru_jabatan');
    	$this->db->where('id_guru', $id);
		return $this->db->get();
    }

    function get_penataran($id){
    	$this->db->select('count(id_guru_penataran) jumlah');
    	$this->db->from('t_guru_penataran');
    	$this->db->where('id_guru', $id);
		return $this->db->get();
    }

    function get_pendidikan($id){
    	$this->db->select('count(id_guru_pendidikan) jumlah');
    	$this->db->from('t_guru_pendidikan');
    	$this->db->where('id_guru', $id);
		return $this->db->get();
    }

    function get_pengalaman($id){
    	$this->db->select('count(id_guru_pengalaman) jumlah');
    	$this->db->from('t_guru_pengalaman');
    	$this->db->where('id_guru', $id);
		return $this->db->get();
    }

    function get_prestasi($id){
    	$this->db->select('count(id_guru_prestasi) jumlah');
    	$this->db->from('t_guru_prestasi');
    	$this->db->where('id_guru', $id);
		return $this->db->get();
    }
}