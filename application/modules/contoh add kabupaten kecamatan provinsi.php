	<h1 class="grid_12">Pengembangan Diri</h1>
			
			<form action="<?php echo site_url("ptk/prestasi_siswa/insertData"); ?>" method="post" enctype="multipart/form-data" class="grid_12">
				<fieldset>
					<legend>Tambah</legend>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>NIS</strong>
						</label>
						<div>
							<input type="text" name="nim" value="" id="nim" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>NAMA</strong>
						</label>
						<div>
							<input disabled type="text" value="" id="nama" />
							<input type="hidden" value="" id="id_siswa" name="id_siswa"/>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>PRESTASI</strong>
						</label>
						<div>
							<input type="text" value="" id="prestasi" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>KATEGORI</strong>
						</label>
						<div>
							<input type="text" value="" id="kategori" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>PERLOMBAAN</strong>
						</label>
						<div>
							<input type="text" value="" id="perlombaan" />
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tingkat Provinsi</strong>
						</label>
						<?php 
							$this->load->model('m_prestasi_siswa');
							$data["provinsi"] = $this->m_prestasi_siswa->getprovinsi()->result();
							$provinsi=$data["provinsi"];							
							?>
						<div>	
							 <select name="provinsi" id="provinsi">
							 <?php foreach($provinsi as $p) {?>
							  <option value="<?php echo $p->id_provinsi ?>"><?php echo $p->provinsi ?> </option>
							  <?php }?>
							 </select>
						</div>
					</div>
					
					<div class="row">
						<label for="f1_normal_input">
							<strong>Tanggal</strong>
						</label>
						<div>
							<input type="date" value="" id="perlombaan" />
						</div>
					</div>
				
					
				</fieldset><!-- End of fieldset -->
				
					<div class="actions">
						<div class="left">
							<input type="submit" value="Simpan" name=send />
							 <a href="<?php echo base_url('pps/prestasi_siswa/datalist/'); ?>"> <input value="Batal" type="button"></a>
						</div>
						<div class="right">
						</div>
					</div><!-- End of .actions -->
				</form><!-- End of .box -->
			</div><!-- End of .grid_4 -->
<script>
$(document).ready(function(){
  $("#nim").on("keyup",function(){
    $.get("<?php echo site_url("pps/prestasi_siswa/cekData")?>/" + $(this).val(),function(data,status){
      var a = JSON.parse(data);
	  if(a){
		console.log(a);
		$("#nama").val(a.nama);
		$("#id_siswa").val(a.id_siswa);
	  }
	  
    });
  });
});
</script>

<script>
$(function(){
	$(document).on('change', '#provinsi', function(){
		var id_provinsi = $(this).val();
		console.log(id_provinsi);
		$.post("<?php echo site_url('pps/prestasi_siswa/getKabupaten'); ?>/" + id_provinsi, function(data){
			$("#kabupaten").html(data);
			$("#kabupaten").trigger("chosen:updated");
		});
	});
	
	$(document).on('change', '#kabupaten', function(){
		var id_kabupaten = $(this).val();
		console.log(id_kabupaten);
		$.post("<?php echo site_url('pps/prestasi_siswa/getKecamatan'); ?>/" + id_kabupaten, function(data){
			$("#kecamatan").html(data);
			$("#kecamatan").trigger("chosen:updated");
		});
	});
});
</script>