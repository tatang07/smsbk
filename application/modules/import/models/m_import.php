<?php

class m_import extends CI_Model {

	function get_id_agama($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('agama', $capt)->get('r_agama')->row_array();
		if($res)
			return $res['id_agama'];
		else
			return 0;
	}

	function get_id_kecamatan($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('kecamatan', trim(str_replace("kec.","",strtolower($capt))))
		->get('r_kecamatan')->row_array();
		if($res)
			return $res['id_kecamatan'];
		else
			return 0;
	}

	function get_id_jenis_tinggal($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('jenis_tinggal', $capt)->get('r_jenis_tinggal')->row_array();
		if($res)
			return $res['id_jenis_tinggal'];
		else
			return 0;
	}

	function get_id_alat_transportasi($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('alat_transportasi', $capt)->get('r_alat_transportasi')->row_array();
		if($res)
			return $res['id_alat_transportasi'];
		else
			return 0;
	}

	function get_id_golongan_darah($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('golongan_darah', $capt)->get('r_golongan_darah')->row_array();
		if($res)
			return $res['id_golongan_darah'];
		else
			return 0;
	}

	function get_id_status_pernikahan($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('status_pernikahan', $capt)->get('r_status_pernikahan')->row_array();
		if($res)
			return $res['id_status_pernikahan'];
		else
			return 0;
	}

	function get_id_status_pegawai($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('status', $capt)->get('r_status_pegawai')->row_array();
		if($res)
			return $res['id_status_pegawai'];
		else {
			$this->db->insert('r_status_pegawai', array('status' => $capt));
			return $this->db->insert_id();
		}
	}

	function get_id_asal_sekolah($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('nama_sekolah', $capt)->get('r_asal_sekolah')->row_array();
		if($res)
			return $res['id_asal_sekolah'];
		else {
			$this->db->insert('r_asal_sekolah', array('nama_sekolah' => $capt));
			return $this->db->insert_id();
		}
	}

	function get_id_pangkat_golongan($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('golongan', $capt)->get('r_pangkat_golongan')->row_array();
		if($res)
			return $res['id_pangkat_golongan'];
		else
			return 0;
	}

	function get_id_kualifikasi_guru($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('kualifikasi_guru', $capt)->get('r_kualifikasi_guru')->row_array();
		if($res)
			return $res['id_kualifikasi_guru'];
		else {
			$this->db->insert('r_kualifikasi_guru', array('kualifikasi_guru' => $capt));
			return $this->db->insert_id();
		}
	}

	function get_id_jenis_jabatan($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('jenis_jabatan', $capt)->get('r_jenis_jabatan')->row_array();
		if($res)
			return $res['id_jenis_jabatan'];
		else {
			$this->db->insert('r_jenis_jabatan', array('jenis_jabatan' => $capt));
			return $this->db->insert_id();
		}
	}

	function get_id_status_anak($capt)
	{
		if(empty($capt)) return 0;

		if(strtolower($capt) == 'kandung') 
			$capt = 'Anak Kandung';
		else if(strtolower($capt) == 'tiri') 
			$capt = 'Anak Tiri';

		$res = $this->db->where('status_anak', $capt)->get('r_status_anak')->row_array();
		if($res)
			return $res['id_status_anak'];
		else
			return 0;
	}

	function insert_siswa($data)
	{
		$where = array(
			'nisn' => $data['nisn'],
			'nama' => $data['nama'],
			'tanggal_lahir' => $data['tanggal_lahir'],
			);

		$res = $this->db->where($where)->get('m_siswa')->row_array();
		if($res)
			return $res['id_siswa'];
		else {
			$this->db->insert('m_siswa', $data);
			return $this->db->insert_id();
		}
	}

	function insert_ptk($data)
	{
		$where = array(
			'nip' => $data['nip'],
			'nama' => $data['nama'],
			'tgl_lahir' => $data['tgl_lahir'],
			);

		$res = $this->db->where($where)->get('m_guru')->row_array();
		if($res)
			return $res['id_guru'];
		else {
			$this->db->insert('m_guru', $data);
			return $this->db->insert_id();
		}
	}

	function get_id_jenjang_pendidikan($capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('jenjang_pendidikan', $capt)->get('r_jenjang_pendidikan')->row_array();
		if($res)
			return $res['id_jenjang_pendidikan'];
		else {
			$this->db->insert('r_jenjang_pendidikan', array('jenjang_pendidikan' => $capt));
			return $this->db->insert_id();
		}
	}

	function get_id_pekerjaan($obj, $capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('pekerjaan', $capt)->get('r_pekerjaan')->row_array();
		if($res)
			return $res['id_pekerjaan'];
		else {
			$this->db->insert('r_pekerjaan', array('pekerjaan' => $capt));
			return $this->db->insert_id();
		}
	}

	function get_id_range_penghasilan($obj, $capt)
	{
		if(empty($capt)) return 0;

		$res = $this->db->where('range_penghasilan', $capt)->get('r_range_penghasilan')->row_array();
		if($res)
			return $res['id_range_penghasilan'];
		else {
			$this->db->insert('r_range_penghasilan', array('range_penghasilan' => $capt));
			return $this->db->insert_id();
		}
	}

	function insert_ortu($data)
	{
		$where = array(
			'nama_ayah' => $data['nama_ayah'],
			'nama_ibu' => $data['nama_ibu'],
			);

		$res = $this->db->where($where)->get('m_ortu')->row_array();
		if($res)
			return $res['id_ortu'];
		else {
			$this->db->insert('m_ortu', $data);
			return $this->db->insert_id();
		}
	}

	function create_nonrombel($id_kurikulum)
	{
		$id_jenjang_sekolah = $this->session->userdata('sekolah')['id_jenjang_sekolah'];
		$temp = $this->db->where('id_jenjang_sekolah', $id_jenjang_sekolah)->get('m_tingkat_kelas')->row_array();
		$id_tingkat_kelas = $temp['id_tingkat_kelas'];

		$data = array(
				'rombel' => 'non-rombel',
				'id_tingkat_kelas' => $id_tingkat_kelas,
				'id_tahun_ajaran' => get_id_tahun_ajaran(),
				'id_sekolah' => get_id_sekolah(),
				'id_kurikulum' => $id_kurikulum
			);

		$res = $this->db->where($data)->get('m_rombel')->row_array();
		if($res)
			return $res['id_rombel'];
		else {
			$this->db->insert('m_rombel', $data);
			return $this->db->insert_id();
		}
	}

	function insert_rombel($data, $id_nonrombel)
	{
		// cek dari rombel
		$rombel = $this->db
						->where('rombel', $data['id_rombel'])
						->where('id_tahun_ajaran', get_id_tahun_ajaran())
						->where('id_sekolah', get_id_sekolah())
						->get('m_rombel')->row_array();

		// kalo rombel ditemukan
		if($rombel){
			$data['id_rombel'] = $rombel['id_rombel'];
		} else {
			// kalo rombel tidak ditemukan
			// gunakan nonrombel
			$data['id_rombel'] = $id_nonrombel;
		}
		
		$rombel_detail = $this->db->where($data)->get('t_rombel_detail')->row_array();
		if($rombel_detail)
			return $rombel_detail['id_rombel_detail'];
		else {
			$this->db->insert('t_rombel_detail', $data);
			return $this->db->insert_id();
		}
	}

	function get_jenjang_sekolah($where = array(), $for_dropdown = false)
	{
		$this->db->from('r_jenjang_sekolah');
		if(!empty($where))
			$this->db->where($where);

		$result = $this->db->get();

		if($result->num_rows() > 0){
			if($for_dropdown){
				foreach ($result->result_array() as $value) {
					$data[$value['id_jenjang_sekolah']] = $value['jenjang_sekolah'];
				}
				return $data;
			} else
				return $result->result_array();
		}

		return false;
	}

}