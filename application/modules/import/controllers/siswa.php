<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class Siswa extends CI_Controller {

	function index()
	{
		$this->load->model('evaluasi/t_nilai_kategori');
		$this->load->model('m_import');

		$d['kurikulum'] = array();
		$kurikulum = $this->t_nilai_kategori->get_kurikulum();
		if($kurikulum){
			foreach ($kurikulum as $kur) {
				$d['kurikulum'][$kur['id_kurikulum']] = $kur['nama_kurikulum'];
			}
		}
		
		$d['jenjang_sekolah'] = $this->m_import->get_jenjang_sekolah(array(), true);

		render('siswa/import', $d);
	}

	function process_import()
	{
		$this->load->model('m_import');

		$data = array();

		$config['upload_path'] = './extras/csv/';
		$config['allowed_types'] = 'csv';
		$config['overwrite'] = TRUE;

		$this->load->library('upload', $config);


		if ($this->upload->do_upload() === FALSE)
		{
			set_error_message($this->upload->display_errors());
			redirect('import/siswa');
		}
		else
		{
			$upload_data = $this->upload->data();
			//print_r($upload_data);

			$fields = $this->get_fields($upload_data['full_path']);
			$schema = $this->evaluate_schema($fields);
			$theData = $this->get_data($upload_data['full_path']);
			//print_r($fields);
			//print_r($schema);
			//print_r($theData);
			$rombel_failed = 0;

			foreach ($theData as $row) {
				if(count($row) > 1){

					// prepare data siswa
					$data_siswa = array();
					foreach ($schema['siswa'] as $field => $idx) {
						$data_siswa[$field] = $row[$idx];
					}

					// prepare relation fields
					$data_siswa['id_agama'] = $this->m_import->get_id_agama($data_siswa['id_agama']);
					$data_siswa['id_kecamatan'] = $this->m_import->get_id_kecamatan($data_siswa['id_kecamatan']);
					$data_siswa['id_jenis_tinggal'] = $this->m_import->get_id_jenis_tinggal($data_siswa['id_jenis_tinggal']);
					$data_siswa['id_alat_transportasi'] = $this->m_import->get_id_alat_transportasi($data_siswa['id_alat_transportasi']);
					$data_siswa['id_golongan_darah'] = $this->m_import->get_id_golongan_darah($data_siswa['id_golongan_darah']);
					$data_siswa['id_asal_sekolah'] = $this->m_import->get_id_asal_sekolah($data_siswa['id_asal_sekolah']);
					$data_siswa['id_status_anak'] = $this->m_import->get_id_status_anak($data_siswa['id_status_anak']);


					// prepare default value fields
					$data_siswa['id_sekolah'] = get_id_sekolah();

					// insert siswa or get id_siswa if exist
					$id_siswa = $this->m_import->insert_siswa($data_siswa);

					// prepare data ortu
					$data_ortu = array();
					foreach ($schema['ortu'] as $field => $idx) {
						$data_ortu[$field] = $row[$idx];
					}

					// prepare relation fields
					$data_ortu['id_jenjang_pendidikan_ayah'] = $this->m_import->get_id_jenjang_pendidikan('ayah', $data_ortu['id_jenjang_pendidikan_ayah']);
					$data_ortu['id_jenjang_pendidikan_ibu'] = $this->m_import->get_id_jenjang_pendidikan('ibu', $data_ortu['id_jenjang_pendidikan_ibu']);
					$data_ortu['id_jenjang_pendidikan_wali'] = $this->m_import->get_id_jenjang_pendidikan('wali', $data_ortu['id_jenjang_pendidikan_wali']);
					$data_ortu['id_pekerjaan_ayah'] = $this->m_import->get_id_pekerjaan('ayah', $data_ortu['id_pekerjaan_ayah']);
					$data_ortu['id_pekerjaan_ibu'] = $this->m_import->get_id_pekerjaan('ibu', $data_ortu['id_pekerjaan_ibu']);
					$data_ortu['id_pekerjaan_wali'] = $this->m_import->get_id_pekerjaan('wali', $data_ortu['id_pekerjaan_wali']);
					$data_ortu['id_range_penghasilan_ayah'] = $this->m_import->get_id_range_penghasilan('ayah', $data_ortu['id_range_penghasilan_ayah']);
					$data_ortu['id_range_penghasilan_ibu'] = $this->m_import->get_id_range_penghasilan('ibu', $data_ortu['id_range_penghasilan_ibu']);
					$data_ortu['id_range_penghasilan_wali'] = $this->m_import->get_id_range_penghasilan('wali', $data_ortu['id_range_penghasilan_wali']);
					$data_ortu['id_siswa'] = $id_siswa;

					$data['schema'][] = $data_ortu;

					// insert siswa or get id_siswa if exist
					$id_ortu = $this->m_import->insert_ortu($data_ortu);

					// print_r($data_ortu);

					// set non-rombel
					$id_nonrombel = $this->m_import->create_nonrombel($this->input->post('id_kurikulum'));

					// insert rombel
					$data_rombel = array();
					foreach ($schema['rombel'] as $field => $idx) {
						$data_rombel[$field] = $row[$idx];
					}
					$data_rombel['id_siswa'] = $id_siswa;
					if(! $this->m_import->insert_rombel($data_rombel, $id_nonrombel))
						$rombel_failed++;
				}
			}

			$data['rombel_failed'] = $rombel_failed;

		}
		render('siswa/import_process', $data);
	}

	function get_fields($csv_file = false)
	{
		if(! file_exists($csv_file)) echo "file not found.";

		$csv_string = file_get_contents($csv_file);
		$csv_rows = explode("\n", $csv_string);

		$titles = str_getcsv($csv_rows[0], ';');

		$fields = array();
		foreach ($titles as $key => $value) {
			$fields[$value] = $key;
		}

		return $fields;
	}

	function get_data($csv_file = false)
	{
		if(! file_exists($csv_file)) echo "file not found.";

		$csv_string = file_get_contents($csv_file);
		$csv_rows = explode("\n", $csv_string);

		// delete first and second rows, the titles
		unset($csv_rows[0]);
		unset($csv_rows[1]);

		$data = array();
		foreach ($csv_rows as $row) {
			$data[] = str_getcsv($row, ';');
		}
		return $data;
	}

	function evaluate_schema($fields)
	{
		// set schema siswa
		if(isset($fields['NIPD'])) 
			$schema['siswa']['nis'] = $fields['NIPD'];
		if(isset($fields['NAMA'])) 
			$schema['siswa']['nama'] = $fields['NAMA'];
		if(isset($fields['JK'])) 
			$schema['siswa']['jenis_kelamin'] = $fields['JK'];
		if(isset($fields['NISN'])) 
			$schema['siswa']['nisn'] = $fields['NISN'];
		if(isset($fields['TEMPAT_LAHIR'])) 
			$schema['siswa']['tempat_lahir'] = $fields['TEMPAT_LAHIR'];
		if(isset($fields['TANGGAL_LAHIR'])) 
			$schema['siswa']['tanggal_lahir'] = $fields['TANGGAL_LAHIR'];
		if(isset($fields['NIK'])) 
			$schema['siswa']['nik'] = $fields['NIK'];
		if(isset($fields['GOLONGAN_DARAH'])) 
			$schema['siswa']['id_golongan_darah'] = $fields['GOLONGAN_DARAH'];
		if(isset($fields['AGAMA'])) 
			$schema['siswa']['id_agama'] = $fields['AGAMA'];
		if(isset($fields['KEBUTUHAN_KHUSUS'])) 
			$schema['siswa']['kebutuhan_khusus'] = $fields['KEBUTUHAN_KHUSUS'];
		if(isset($fields['ALAMAT'])) 
			$schema['siswa']['alamat'] = $fields['ALAMAT'];
		if(isset($fields['RT'])) 
			$schema['siswa']['rt'] = $fields['RT'];
		if(isset($fields['RW'])) 
			$schema['siswa']['rw'] = $fields['RW'];
		if(isset($fields['DUSUN'])) 
			$schema['siswa']['dusun'] = $fields['DUSUN'];
		if(isset($fields['KELURAHAN'])) 
			$schema['siswa']['kelurahan'] = $fields['KELURAHAN'];
		if(isset($fields['KECAMATAN'])) 
			$schema['siswa']['id_kecamatan'] = $fields['KECAMATAN'];
		if(isset($fields['KODE_POS'])) 
			$schema['siswa']['kode_pos'] = $fields['KODE_POS'];
		if(isset($fields['JENIS_TINGGAL'])) 
			$schema['siswa']['id_jenis_tinggal'] = $fields['JENIS_TINGGAL'];
		if(isset($fields['ALAT_TRANSPORTASI'])) 
			$schema['siswa']['id_alat_transportasi'] = $fields['ALAT_TRANSPORTASI'];
		if(isset($fields['TELEPON'])) 
			$schema['siswa']['telepon'] = $fields['TELEPON'];
		if(isset($fields['HP'])) 
			$schema['siswa']['hp'] = $fields['HP'];
		if(isset($fields['EMAIL'])) 
			$schema['siswa']['e_mail'] = $fields['EMAIL'];
		if(isset($fields['SKHUN'])) 
			$schema['siswa']['skhun'] = $fields['SKHUN'];
		if(isset($fields['PENERIMA_KPS'])) 
			$schema['siswa']['penerima_kps'] = $fields['PENERIMA_KPS'];
		if(isset($fields['NO_KPS'])) 
			$schema['siswa']['no_kps'] = $fields['NO_KPS'];
		if(isset($fields['ASAL_SEKOLAH'])) 
			$schema['siswa']['id_asal_sekolah'] = $fields['ASAL_SEKOLAH'];
		if(isset($fields['STATUS_ANAK'])) 
			$schema['siswa']['id_status_anak'] = $fields['STATUS_ANAK'];
		if(isset($fields['ANAK_KE'])) 
			$schema['siswa']['anak_ke'] = $fields['ANAK_KE'];
		if(isset($fields['JUMLAH_SAUDARA'])) 
			$schema['siswa']['jumlah_sdr'] = $fields['JUMLAH_SAUDARA'];

		// set schema ortu
		if(isset($fields['NAMA_AYAH'])) 
			$schema['ortu']['nama_ayah'] = $fields['NAMA_AYAH'];
		if(isset($fields['TAHUN_LAHIR_AYAH'])) 
			$schema['ortu']['tahun_lahir_ayah'] = $fields['TAHUN_LAHIR_AYAH'];
		if(isset($fields['JENJANG_PENDIDIKAN_AYAH'])) 
			$schema['ortu']['id_jenjang_pendidikan_ayah'] = $fields['JENJANG_PENDIDIKAN_AYAH'];
		if(isset($fields['PEKERJAAN_AYAH'])) 
			$schema['ortu']['id_pekerjaan_ayah'] = $fields['PEKERJAAN_AYAH'];
		if(isset($fields['PENGHASILAN_AYAH'])) 
			$schema['ortu']['id_range_penghasilan_ayah'] = $fields['PENGHASILAN_AYAH'];
		if(isset($fields['KEBUTUHAN_KHUSUS_AYAH'])) 
			$schema['ortu']['kebutuhan_khusus_ayah'] = $fields['KEBUTUHAN_KHUSUS_AYAH'];
		if(isset($fields['NAMA_IBU'])) 
			$schema['ortu']['nama_ibu'] = $fields['NAMA_IBU'];
		if(isset($fields['TAHUN_LAHIR_IBU'])) 
			$schema['ortu']['tahun_lahir_ibu'] = $fields['TAHUN_LAHIR_IBU'];
		if(isset($fields['JENJANG_PENDIDIKAN_IBU'])) 
			$schema['ortu']['id_jenjang_pendidikan_ibu'] = $fields['JENJANG_PENDIDIKAN_IBU'];
		if(isset($fields['PEKERJAAN_IBU'])) 
			$schema['ortu']['id_pekerjaan_ibu'] = $fields['PEKERJAAN_IBU'];
		if(isset($fields['PENGHASILAN_IBU'])) 
			$schema['ortu']['id_range_penghasilan_ibu'] = $fields['PENGHASILAN_IBU'];
		if(isset($fields['KEBUTUHAN_KHUSUS_IBU'])) 
			$schema['ortu']['kebutuhan_khusus_ibu'] = $fields['KEBUTUHAN_KHUSUS_IBU'];
		if(isset($fields['NAMA_WALI'])) 
			$schema['ortu']['nama_wali'] = $fields['NAMA_WALI'];
		if(isset($fields['TAHUN_LAHIR_WALI'])) 
			$schema['ortu']['tahun_lahir_wali'] = $fields['TAHUN_LAHIR_WALI'];
		if(isset($fields['JENJANG_PENDIDIKAN_WALI'])) 
			$schema['ortu']['id_jenjang_pendidikan_wali'] = $fields['JENJANG_PENDIDIKAN_WALI'];
		if(isset($fields['PEKERJAAN_WALI'])) 
			$schema['ortu']['id_pekerjaan_wali'] = $fields['PEKERJAAN_WALI'];
		if(isset($fields['PENGHASILAN_WALI'])) 
			$schema['ortu']['id_range_penghasilan_wali'] = $fields['PENGHASILAN_WALI'];
		if(isset($fields['ALAMAT_AYAH'])) 
			$schema['ortu']['alamat_ayah'] = $fields['ALAMAT_AYAH'];
		if(isset($fields['ALAMAT_IBU'])) 
			$schema['ortu']['alamat_ibu'] = $fields['ALAMAT_IBU'];
		if(isset($fields['ALAMAT_WALI'])) 
			$schema['ortu']['alamat_wali'] = $fields['ALAMAT_WALI'];

		// set schema rombel
		if(isset($fields['ROMBEL_SAAT_INI']))
			$schema['rombel']['id_rombel'] = $fields['ROMBEL_SAAT_INI'];

		return $schema;
	}

}
