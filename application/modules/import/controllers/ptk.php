<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class ptk extends CI_Controller {

	function index()
	{
		render('ptk/import');
	}

	function process_import()
	{
		$this->load->model('m_import');

		$data = array();

		$config['upload_path'] = './extras/csv/';
		$config['allowed_types'] = 'csv';
		$config['overwrite'] = TRUE;

		$this->load->library('upload', $config);


		if ($this->upload->do_upload() === FALSE)
		{
			set_error_message($this->upload->display_errors());
			redirect('import/ptk');
		}
		else
		{
			$upload_data = $this->upload->data();
			// $this->debug($upload_data);

			$fields = $this->get_fields($upload_data['full_path']);
			$schema = $this->evaluate_schema($fields);
			$theData = $this->get_data($upload_data['full_path']);

			foreach ($theData as $row) {
				if(count($row) > 1){

					// prepare data siswa
					$data_ptk = array();
					foreach ($schema['ptk'] as $field => $idx) {
						$data_ptk[$field] = $row[$idx];
					}

					// prepare relation fields
					$data_ptk['id_agama'] = $this->m_import->get_id_agama($data_ptk['id_agama']);
					$data_ptk['id_kecamatan'] = $this->m_import->get_id_kecamatan($data_ptk['id_kecamatan']);
					$data_ptk['id_golongan_darah'] = $this->m_import->get_id_golongan_darah($data_ptk['id_golongan_darah']);
					$data_ptk['id_status_pernikahan'] = $this->m_import->get_id_status_pernikahan($data_ptk['id_status_pernikahan']);
					$data_ptk['id_status_pegawai'] = $this->m_import->get_id_status_pegawai($data_ptk['id_status_pegawai']);
					$data_ptk['id_jenjang_pendidikan'] = $this->m_import->get_id_jenjang_pendidikan($data_ptk['id_jenjang_pendidikan']);
					$data_ptk['id_pangkat_golongan'] = $this->m_import->get_id_pangkat_golongan($data_ptk['id_pangkat_golongan']);
					$data_ptk['id_kualifikasi_guru'] = $this->m_import->get_id_kualifikasi_guru($data_ptk['id_kualifikasi_guru']);
					$data_ptk['id_jenis_jabatan'] = $this->m_import->get_id_jenis_jabatan($data_ptk['id_jenis_jabatan']);

					// prepare default value fields
					$data_ptk['id_sekolah'] = get_id_sekolah();

					$data_ptk['status_aktif'] = (strtolower($data_ptk['status_aktif']) == 'aktif') ? 1 : 0;
					$data_ptk['sudah_lisensi_kepsek'] = (strtolower($data_ptk['sudah_lisensi_kepsek']) == 'ya') ? 1 : 0;
					$data_ptk['pernah_diklat_pengawasan'] = (strtolower($data_ptk['pernah_diklat_pengawasan']) == 'ya') ? 1 : 0;
					$data_ptk['keahlian_braille'] = (strtolower($data_ptk['keahlian_braille']) == 'ya') ? 1 : 0;
					$data_ptk['keahlian_bahasa_isyarat'] = (strtolower($data_ptk['keahlian_bahasa_isyarat']) == 'ya') ? 1 : 0;

					// insert siswa or get idd_ptk if exist
					$id_ptk = $this->m_import->insert_ptk($data_ptk);
				}
			}
		}

		render('ptk/import_process', $data);
	}

	function get_fields($csv_file = false)
	{
		if(! file_exists($csv_file)) echo "file not found.";

		$csv_string = file_get_contents($csv_file);
		$csv_rows = explode("\n", $csv_string);

		$titles = str_getcsv($csv_rows[0], ';');

		$fields = array();
		foreach ($titles as $key => $value) {
			$fields[$value] = $key;
		}

		return $fields;
	}

	function get_data($csv_file = false)
	{
		if(! file_exists($csv_file)) echo "file not found.";

		$csv_string = file_get_contents($csv_file);
		$csv_rows = explode("\n", $csv_string);

		// delete first and second rows, the titles
		unset($csv_rows[0]);
		unset($csv_rows[1]);

		$data = array();
		foreach ($csv_rows as $row) {
			$data[] = str_getcsv($row, ';');
		}

		return $data;
	}

	function evaluate_schema($fields)
	{
		// set schema ptk
		if(isset($fields['NAMA'])) 
			$schema['ptk']['nama'] = $fields['NAMA'];
		if(isset($fields['NIP'])) 
			$schema['ptk']['nip'] = $fields['NIP'];
		if(isset($fields['JK'])) 
			$schema['ptk']['jenis_kelamin'] = $fields['JK'];
		if(isset($fields['NISN'])) 
			$schema['ptk']['nisn'] = $fields['NISN'];
		if(isset($fields['TEMPAT_LAHIR'])) 
			$schema['ptk']['tempat_lahir'] = $fields['TEMPAT_LAHIR'];
		if(isset($fields['TANGGAL_LAHIR'])) 
			$schema['ptk']['tgl_lahir'] = $fields['TANGGAL_LAHIR'];
		if(isset($fields['NIK'])) 
			$schema['ptk']['NIK'] = $fields['NIK'];
		if(isset($fields['GOLONGAN_DARAH'])) 
			$schema['ptk']['id_golongan_darah'] = $fields['GOLONGAN_DARAH'];
		if(isset($fields['AGAMA'])) 
			$schema['ptk']['id_agama'] = $fields['AGAMA'];
		if(isset($fields['KODE_GURU'])) 
			$schema['ptk']['kode_guru'] = $fields['KODE_GURU'];
		if(isset($fields['ALAMAT'])) 
			$schema['ptk']['alamat'] = $fields['ALAMAT'];
		if(isset($fields['RT'])) 
			$schema['ptk']['RT'] = $fields['RT'];
		if(isset($fields['RW'])) 
			$schema['ptk']['RW'] = $fields['RW'];
		if(isset($fields['DUSUN'])) 
			$schema['ptk']['nama_dusun'] = $fields['DUSUN'];
		if(isset($fields['DESA_KELURAHAN'])) 
			$schema['ptk']['desa_kelurahan'] = $fields['DESA_KELURAHAN'];
		if(isset($fields['KECAMATAN'])) 
			$schema['ptk']['id_kecamatan'] = $fields['KECAMATAN'];
		if(isset($fields['KODE_POS'])) 
			$schema['ptk']['kode_pos'] = $fields['KODE_POS'];
		if(isset($fields['NUPTK'])) 
			$schema['ptk']['nuptk'] = $fields['NUPTK'];
		if(isset($fields['STATUS_SERTIFIKASI'])) 
			$schema['ptk']['status_sertifikasi'] = $fields['STATUS_SERTIFIKASI'];
		if(isset($fields['TELEPON'])) 
			$schema['ptk']['telepon'] = $fields['TELEPON'];
		if(isset($fields['HP'])) 
			$schema['ptk']['hp'] = $fields['HP'];
		if(isset($fields['EMAIL'])) 
			$schema['ptk']['e_mail'] = $fields['EMAIL'];
		if(isset($fields['TMT_PENGANGKATAN'])) 
			$schema['ptk']['tgl_diangkat'] = $fields['TMT_PENGANGKATAN'];
		if(isset($fields['SK_PENGANGKATAN'])) 
			$schema['ptk']['sk_pengangkatan'] = $fields['SK_PENGANGKATAN'];
		if(isset($fields['TANGGAL_CPNS'])) 
			$schema['ptk']['tgl_cpns'] = $fields['TANGGAL_CPNS'];
		if(isset($fields['SK_CPNS'])) 
			$schema['ptk']['sk_cpns'] = $fields['SK_CPNS'];
		if(isset($fields['TMT_PNS'])) 
			$schema['ptk']['tmt_pns'] = $fields['TMT_PNS'];
		if(isset($fields['GAJI'])) 
			$schema['ptk']['gaji'] = $fields['GAJI'];
		if(isset($fields['STATUS_AKTIF'])) 
			$schema['ptk']['status_aktif'] = $fields['STATUS_AKTIF'];
		if(isset($fields['STATUS_PERKAWINAN'])) 
			$schema['ptk']['id_status_pernikahan'] = $fields['STATUS_PERKAWINAN'];
		if(isset($fields['STATUS_KEPEGAWAIAN'])) 
			$schema['ptk']['id_status_pegawai'] = $fields['STATUS_KEPEGAWAIAN'];
		if(isset($fields['JENJANG_PENDIDIKAN'])) 
			$schema['ptk']['id_jenjang_pendidikan'] = $fields['JENJANG_PENDIDIKAN'];
		if(isset($fields['PANGKAT_GOLONGAN'])) 
			$schema['ptk']['id_pangkat_golongan'] = $fields['PANGKAT_GOLONGAN'];
		if(isset($fields['KUALIFIKASI_GURU'])) 
			$schema['ptk']['id_kualifikasi_guru'] = $fields['KUALIFIKASI_GURU'];
		if(isset($fields['JENIS_JABATAN'])) 
			$schema['ptk']['id_jenis_jabatan'] = $fields['JENIS_JABATAN'];
		if(isset($fields['NIY_NIGK'])) 
			$schema['ptk']['niy_nigk'] = $fields['NIY_NIGK'];
		if(isset($fields['JENIS_PTK'])) 
			$schema['ptk']['jenis_ptk'] = $fields['JENIS_PTK'];
		if(isset($fields['LEMBAGA_PENGANGKATAN'])) 
			$schema['ptk']['lembaga_pengangkatan'] = $fields['LEMBAGA_PENGANGKATAN'];
		if(isset($fields['SUMBER_GAJI'])) 
			$schema['ptk']['sumber_gaji'] = $fields['SUMBER_GAJI'];
		if(isset($fields['NAMA_IBU_KANDUNG'])) 
			$schema['ptk']['nama_ibu_kandung'] = $fields['NAMA_IBU_KANDUNG'];
		if(isset($fields['NAMA_SUAMI_ISTRI'])) 
			$schema['ptk']['nama_suami_atau_istri'] = $fields['NAMA_SUAMI_ISTRI'];
		if(isset($fields['NIP_SUAMI_ISTRI'])) 
			$schema['ptk']['nip_suami_atau_istri'] = $fields['NIP_SUAMI_ISTRI'];
		if(isset($fields['PEKERJAAN_SUAMI_ISTRI'])) 
			$schema['ptk']['pekerjaan_suami_atau_istri'] = $fields['PEKERJAAN_SUAMI_ISTRI'];
		if(isset($fields['NPWP'])) 
			$schema['ptk']['npwp'] = $fields['NPWP'];
		if(isset($fields['KEWARGANEGARAAN'])) 
			$schema['ptk']['kewarganegaraan'] = $fields['KEWARGANEGARAAN'];
		if(isset($fields['SUDAH_LISENSI_KEPALA_SEKOLAH'])) 
			$schema['ptk']['sudah_lisensi_kepsek'] = $fields['SUDAH_LISENSI_KEPALA_SEKOLAH'];
		if(isset($fields['JUMLAH_SEKOLAH_BINAAN'])) 
			$schema['ptk']['jumlah_sekolah_binaan'] = $fields['JUMLAH_SEKOLAH_BINAAN'];
		if(isset($fields['PERNAH_DIKLAT_KEPENGAWASAN'])) 
			$schema['ptk']['pernah_diklat_pengawasan'] = $fields['PERNAH_DIKLAT_KEPENGAWASAN'];
		if(isset($fields['MAMPU_HANDLE_KK'])) 
			$schema['ptk']['mampu_handle_kk'] = $fields['MAMPU_HANDLE_KK'];
		if(isset($fields['KEAHLIAN_BRAILLE'])) 
			$schema['ptk']['keahlian_braille'] = $fields['KEAHLIAN_BRAILLE'];
		if(isset($fields['KEAHLIAN_BAHASA_ISYARAT'])) 
			$schema['ptk']['keahlian_bahasa_isyarat'] = $fields['KEAHLIAN_BAHASA_ISYARAT'];

		return $schema;
	}

}
