<h1 class="grid_12">Pelayanan dan Pembinaan Siswa</h1>
<?php echo form_open_multipart('import/siswa/process_import', 'class="grid_12 validate no-box"'); ?>

<fieldset>
	<legend>Import Data Siswa</legend>
	<div class="row">
		<label><strong>Kurikulum</strong></label>
		<div>
		<?php echo simple_form_dropdown_clear("id_kurikulum", $kurikulum, null, "", null) ?>
		</div>
	</div>
	<div class="row">
		<?php echo form_label('CSV file', 'userfile'); ?>
		<div class="customfile"><?php echo form_upload('userfile'); ?></div>
	</div>
</fieldset>

<div class="actions">
	<div class="left">
		<?php echo form_submit('submit_upload', 'Upload', 'class="btn"'); ?>
		<a href="<?php echo site_url('pps/siswa'); ?>" class="button">Batal</a>
	</div>
	<div class="right">
		<a href="<?php echo base_url('extras/import/pps_format.csv'); ?>"><span class="icon icon-download"></span> Download Format CSV</a>
	</div>
</div>

<?php echo form_close(); ?>