<?php
class referensi extends MY_Model {
	function get_list_referensi($r_table,$isOrder=true){
		$list = $this->get_list('r_'.$r_table,'id_'.$r_table,$r_table,$isOrder);
		return $list;
	}

	function get_list($table,$key,$val,$isOrder=true){
		$this->id = 'id_'.$table;
		$this->table = $table;
        if($isOrder){
            $this->order_fields = array($val);
        }else{
            $this->order_fields = array();
        }
		$data = $this->get_all();
		$list = $this->create_list($data,$key,$val);
		return $list;
	}

    function create_list($data,$key,$value){
        $ret = array();
        foreach($data as $det){
            $ret[$det[$key]] = $det[$value];
        }
        return  $ret;
    }
    
    function get_list_filter($table,$key,$vals,$filter=array(),$order=array()){
		$this->id = 'id_'.$table;
		$this->table = $table;
        
        $this->order_fields = $order;
		$data = $this->get_by_fields($filter);
		//print_r($this->db->last_query());
		$list = $this->create_list2($data,$key,$vals);
		return $list;
	}
    
    function create_list2($data,$key,$values){
        $ret = array();
        foreach($data as $det){
            $hasil = array();
            foreach($values as $val){
                $hasil[] = $det[$val];
            }
            $ret[$det[$key]] = implode(" - ",$hasil);
        }
        return  $ret;
    }
}