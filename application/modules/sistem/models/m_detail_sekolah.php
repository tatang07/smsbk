<?php
class m_detail_sekolah extends MY_Model {
    public $table = "m_sekolah";
    public $id = "id_sekolah";
    public $order_fields = array("sekolah DESC","nss");
	// join ON 
	public $join_to = array(
			"r_jenjang_sekolah js"=>"js.id_jenjang_sekolah=m_sekolah.id_jenjang_sekolah",
			"r_kecamatan k"=>"k.id_kecamatan=m_sekolah.id_kecamatan",
			"r_kabupaten_kota kk"=>"kk.id_kabupaten_kota=k.id_kabupaten_kota",
			
		);
	public $join_fields = array("jenjang_sekolah", "kecamatan", "kabupaten_kota");

	public function get_new_id(){
		$query = "select id_sekolah from m_sekolah order by id_sekolah desc limit 1";
		$result=mysql_query($query);
		$data = mysql_fetch_array($result);
		$new_id = $data['id_sekolah'];
		return $new_id;
	}
	public function tambah_user($data=array()){
		$this->db->insert('sys_user', $data);
	}
}