<?php
class m_user_sekolah extends MY_Model {
    
    function get_user_group(){
    	$this->db->select('*'); 
		$this->db->from('sys_group');
		$this->db->where('id_group >', '1');
		$id_sekolah = get_id_sekolah();
		//$this->db->where('id_sekolah >', get_id_sekolah());
		return $this->db->get();
    }

    function get_user_group_add(){
    	$this->db->select('*'); 
		$this->db->from('sys_group');
		$this->db->where('id_group >', '1');
		$this->db->where('id_group <', '9');
		$id_sekolah = get_id_sekolah();
		//$this->db->where('id_sekolah >', $id_sekolah);
		return $this->db->get();
    }

    function datalist($id, $id_group){
    	$this->db->select('*'); 
		$this->db->from('sys_user u');
		$this->db->join('sys_group g', 'g.id_group = u.id_group');
		$this->db->where('u.id_sekolah', $id);
		$this->db->where('u.id_group', $id_group);
		return $this->db->get();
    }

    function get_cari($cari){
		$id_sekolah = get_id_sekolah();
    	$query = $this->db->query("SELECT * FROM sys_user as u join sys_group as g on g.id_group = u.id_group join v_siswa as s 
		on u.id_personal = s.id_siswa where s.nama like '%$cari%' AND u.id_sekolah=$id_sekolah");
		return $query;
    }

    function add($data){
		$this->db->insert('sys_user',$data);
	}

	function get_user_by($id){
		$this->db->select('*'); 
		$this->db->from('sys_user u');
		$this->db->join('sys_group g', 'g.id_group = u.id_group');
		$this->db->where('u.id_user', $id);
		return $this->db->get();
	}

	function edit($id, $data){
		$this->db->where('id_user', $id);
		$this->db->update('sys_user', $data);
	}

	function delete($id){
		$this->db->where('id_user', $id);
		$this->db->delete('sys_user');
	}
}