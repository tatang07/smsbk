<?php
	class m_user_siswa extends MY_Model {
		function getTingkatKelas(){
			$this->db->select('*'); 
			$this->db->from('m_tingkat_kelas');
			return $this->db->get();
		}
		
		function getRombel($id_tingkat_kelas, $id_tahun_ajaran){
			$this->db->select('*'); 
			$this->db->from('m_rombel');
			$this->db->where('id_tingkat_kelas', $id_tingkat_kelas);
			$this->db->where('id_tahun_ajaran', $id_tahun_ajaran);
			$this->db->where('id_sekolah', get_id_sekolah());
			$this->db->order_by('rombel', 'asc');
			return $this->db->get();
		}

		function get_data($id_rombel, $id_tingkat){
			$this->db->select('*'); 
			$this->db->from('sys_user u');
			$this->db->join('v_siswa s', 'u.id_personal = s.id_siswa');
			$this->db->join('t_rombel_detail rd', 's.id_siswa = rd.id_siswa');
			$this->db->join('m_rombel r', 'r.id_rombel = rd.id_rombel');
			$this->db->where('r.id_rombel', $id_rombel);
			$this->db->where('r.id_tingkat_kelas', $id_tingkat);
			$this->db->where('u.id_sekolah', get_id_sekolah());
			return $this->db->get();
		}
	}
?>