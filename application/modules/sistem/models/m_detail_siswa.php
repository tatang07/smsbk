<?php
class m_detail_siswa extends MY_Model {
    public $table = "v_siswa";
    public $id = "id_siswa";
    public $order_fields = array("nama DESC");
	// join ON 
	public $join_to = array(
			"m_sekolah s"=>"s.id_sekolah=v_siswa.id_sekolah",
			"m_tingkat_kelas tk"=>"tk.id_tingkat_kelas=v_siswa.id_tingkat_kelas",
			"r_jenjang_sekolah js"=>"js.id_jenjang_sekolah=s.id_jenjang_sekolah"
		);
	public $join_fields = array("sekolah", "tingkat_kelas");

	public function get_new_id(){
		$query = "select id_sekolah from m_sekolah order by id_sekolah desc limit 1";
		$result=mysql_query($query);
		$data = mysql_fetch_array($result);
		$new_id = $data['id_sekolah'];
		return $new_id;
	}
	public function tambah_user($data=array()){
		$this->db->insert('sys_user', $data);
	}
}