<?php
	class m_dashboard extends MY_Model {
		
		function get_grup_sekolah(){
			$query = $this->db->query("SELECT s.id_jenjang_sekolah,jenjang_sekolah, count(s.id_jenjang_sekolah) as total FROM `m_sekolah` as s join r_jenjang_sekolah as j on s.id_jenjang_sekolah = j.id_jenjang_sekolah group by id_jenjang_sekolah order by s.id_jenjang_sekolah asc");
			return $query;
		}
		
		function get_grup_guru(){
			$query = $this->db->query("SELECT j.id_jenjang_sekolah,count(jenjang_sekolah) as total_guru,jenjang_sekolah FROM `m_guru` as g join m_sekolah as s on g.id_sekolah=s.id_sekolah join r_jenjang_sekolah as j on s.id_jenjang_sekolah = j.id_jenjang_sekolah group by jenjang_sekolah order by j.id_jenjang_sekolah asc");
			return $query;
		}
		
		function get_grup_siswa(){
			$query = $this->db->query("SELECT j.id_jenjang_sekolah,count(jenjang_sekolah) as total_siswa,jenjang_sekolah FROM `m_siswa` as g join m_sekolah as s on g.id_sekolah=s.id_sekolah join r_jenjang_sekolah as j on s.id_jenjang_sekolah = j.id_jenjang_sekolah group by jenjang_sekolah order by j.id_jenjang_sekolah asc");
			return $query;
		}
		
		function get_grup_rombel(){
			$query = $this->db->query("SELECT j.id_jenjang_sekolah,count(jenjang_sekolah) as total_rombel,jenjang_sekolah FROM `m_rombel` as g join m_sekolah as s on g.id_sekolah=s.id_sekolah join r_jenjang_sekolah as j on s.id_jenjang_sekolah = j.id_jenjang_sekolah group by jenjang_sekolah order by j.id_jenjang_sekolah asc");
			return $query;
		}
		
		function get_status_gaji(){
			$query = $this->db->query("SELECT SUM(CASE WHEN total >= 2626940 THEN 1 ELSE 0 END) as atas_umr, SUM(CASE WHEN total < 2626940 THEN 1 ELSE 0 END) as bawah_umr, count(id_guru) as total_guru FROM `v_gaji`");
			return $query;
		}
		
	}
?>