<?php
class m_detail_guru extends MY_Model {
    public $table = "v_penugasan_dan_penentuan_beban_mengajar";
    public $id = "id_guru";
    public $order_fields = array("nama DESC");
	// join ON 
	public $join_to = array(
			"m_sekolah s"=>"s.id_sekolah=v_penugasan_dan_penentuan_beban_mengajar.id_sekolah",
			"r_pangkat_golongan pg"=>"pg.id_pangkat_golongan=v_penugasan_dan_penentuan_beban_mengajar.id_pangkat_golongan",
		);
	public $join_fields = array("sekolah", "pangkat");

	public function get_new_id(){
		$query = "select id_sekolah from m_sekolah order by id_sekolah desc limit 1";
		$result=mysql_query($query);
		$data = mysql_fetch_array($result);
		$new_id = $data['id_sekolah'];
		return $new_id;
	}
	public function tambah_user($data=array()){
		$this->db->insert('sys_user', $data);
	}
}