<?php
	class m_user_guru extends MY_Model {
		function get_data($id_sekolah){
			$this->db->select('*'); 
			$this->db->from('sys_user u');
			$this->db->join('m_guru g', 'u.id_personal = g.id_guru');
			$this->db->where('u.id_sekolah', $id_sekolah);
			return $this->db->get();
		}

		 function get_cari($cari, $id_sekolah){
	    	$query = $this->db->query("SELECT * FROM sys_user as u join m_guru as g on g.id_guru = u.id_personal where g.nama like '%$cari%' and u.id_sekolah = $id_sekolah");
			return $query;
	    }
	}
?>