<?php
class sys_group extends MY_Model {
    public $table = "sys_group";
    public $id = "id_group";
    public $order_fields = array("groupname");
}