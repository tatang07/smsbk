<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class user_siswa extends MY_Controller {
   public function index()
    {   
        $this->load->model('sistem/m_user_siswa');
        $data['tingkat_kelas'] = get_tingkat_kelas();
        //$data['id_tingkat_kelas']=0;
        render('user_siswa/datalist', $data);
    }

    public function tampil($id_rombel = 0, $id_tingkat_kelas = 0){
        $this->load->model('sistem/m_user_siswa');
        $send = $this->input->post('send');

        $data['tingkat_kelas'] = get_tingkat_kelas();

        if($send=="Tampilkan"){
            $id_tingkat_kelas = $this->input->post('tingkat_kelas');
            $id_rombel = $this->input->post('kelas');
        }
        $data['rombels'] = $this->getRombel($id_tingkat_kelas, $id_rombel, true);

        $data['siswa'] =  $this->m_user_siswa->get_data($id_rombel, $id_tingkat_kelas)->result_array();
        $data['id_rombel'] = $id_rombel;
        $data['id_tingkat_kelas'] = $id_tingkat_kelas;
        //echo $id_tingkat;
        render('user_siswa/datalist', $data);
    }

    public function getRombel($id_tingkat_kelas, $id_rombel = false, $return = false){
        $this->load->model('sistem/m_user_siswa');
        $id_tahun_ajaran = get_id_tahun_ajaran();
        $data['rombel'] = $this->m_user_siswa->getRombel($id_tingkat_kelas, $id_tahun_ajaran)->result();

        $output = "";
        foreach($data['rombel'] as $r){
            if($id_rombel && $r->id_rombel == $id_rombel){
                $output .= "<option selected='selected' value='".$r->id_rombel."'>".$r->rombel."</option>";
            }else{
                $output .= "<option value='".$r->id_rombel."'>".$r->rombel."</option>";             
            }
        }
        
        if($return === false)
            echo $output;
        else
            return $output;
    }

    function print_pdf($id_rombel, $id_tingkat){
        $this->load->model('sistem/m_user_siswa');
        //$id_sekolah=get_id_sekolah();
        $data['user_siswa'] = $this->m_user_siswa->get_data($id_rombel, $id_tingkat)->result_array();

        print_pdf($this->load->view('user_siswa/print', $data, true), "A4");
        redirect(getenv('HTTP_REFERER'));
        //$this->load->view('user_siswa/print', $data);
    }
	
	public function generate_user_siswa(){	
		$id_sekolah = get_id_sekolah();
		$q = 'INSERT INTO sys_user(username,password,default_password,email,id_personal,is_active,id_group,id_sekolah)
		SELECT nisn, md5(nisn), nisn, e_mail,id_siswa,1,10,id_sekolah from m_siswa
		WHERE 
		nisn not in (select username from sys_user)
		AND nisn <> ""
		AND nisn <> "-"
		AND id_sekolah='.$id_sekolah;
		$this->db->query($q);
		set_success_message("User siswa berhasil digenerate, silahkan gunakan username NISN dan password NISN");
		redirect("sistem/user_siswa/tampil");
	}
}