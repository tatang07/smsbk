<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user extends MY_Controller {
    public function index(){
		render('sistem/welcome');
    }
    
    public function datatable($page=1,$return=false,$filter=array()){
        $this->load->model('sys_user');
        if(!$return){
            $filter = isset($_POST['filter'])?$_POST['filter']:array();
            $keywords = isset($filter['username'])?$filter['username']:"";
            $filter['u.username'] = $keywords;
            $filter['g.groupname'] = $keywords;
        }
        
        $offset = ($page-1)*PER_PAGE;
        $perpage = PER_PAGE;
        $count = count($this->sys_user->get_for_pagination(null, $filter));
        $list = $this->sys_user->get_for_pagination(array('perpage' => $perpage, 'offset' => $offset), $filter);
        
	$data_list = populate_pagination($list, $count, $page);
        
	$data['data_list']=$data_list;
        $data['list']=$list;
        $data['offset']=$offset;
        $data['filter']=$filter;
        
        if($return){
            return $data;
        }else{
            $this->load->view("user/datatable", $data);
        }
    }
	
    public function datalist($page=1){
		$data = $this->datatable($page,true);
		render('user/datalist',$data);
    }
    
    public function add(){
		$data = $_POST;
		if(empty($data)){
			render('user/add');
		}else{
			$this->load->model('sys_user');
			$data['password'] = md5($data['password']);
			$this->sys_user->save($data);
			set_success_message("Data Berhasil Disimpan!");
				redirect('sistem/user/datalist');
		}
    }
    
    public function edit($id_user=0) {
        $this->load->model('sys_user');
            
        if (empty($_POST)) {
            $data['user'] = $this->sys_user->get($id_user);
            $_POST = $data['user'];
			$_POST['password'] = '';
            render('user/edit', $data);
        } else {
            $data = $_POST;
			if(empty($data['password'])){
				unset($data['password']);
			}else{
				$data['password'] = md5($data['password']);
			}
            $this->sys_user->update($data);
            set_success_message("Data Berhasil Diperbaharui!");
            redirect('sistem/user/datalist');
        }
    }
    
    public function delete($id_user) {
        $this->load->model('sys_user');
        $this->sys_user->delete($id_user);
        set_success_message("Data Berhasil Dihapus!");
        redirect('sistem/user/datalist');
    }
}
