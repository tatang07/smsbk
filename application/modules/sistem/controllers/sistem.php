<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
//Sample
class sistem extends MY_Controller {
    public function index(){
		$gid = get_usergroup();
		if(is_logged_in()){
			if($gid>1){
				redirect("home");
			}else{
				$this->dasboard();
			}
		}else{
			redirect("portal");
		}
    }
    public function dasboard(){
        $gid = get_usergroup();
		if($gid>1){
			redirect("dashboard");
		}else{
			$this->load->model('m_dashboard');
			$data['data_sekolah'] = $this->m_dashboard->get_grup_sekolah()->result_array();
			$data['data_guru'] = $this->m_dashboard->get_grup_guru()->result_array();
			$data['data_siswa'] = $this->m_dashboard->get_grup_siswa()->result_array();
			$data['data_rombel'] = $this->m_dashboard->get_grup_rombel()->result_array();
			$data['data_gaji'] = $this->m_dashboard->get_status_gaji()->result_array();
			render("sistem/welcome",$data);
		}
    }   
    public function login(){
        if(!empty($_POST)){
            $this->load->model('sys_user');
            $user = $this->sys_user->login($_POST['login_name'],$_POST['login_pw']);
            if(count($user)>0){
                $this->session->set_userdata('is_logged_in',true);
                $this->session->set_userdata('username',$user[0]['username']);
                $this->session->set_userdata('id_user',$user[0]['id_user']);
                $this->session->set_userdata('id_group',$user[0]['id_group']);
                $this->session->set_userdata('id_personal',$user[0]['id_personal']);
				if(isset($user[0]['id_sekolah'])){
					$this->set_session_sekolah($user[0]['id_sekolah']);
				}
                redirect('sistem/index');
            }else{
                set_error_message("Maaf, Username dan Password Tidak Cocok!");
            }
        }
        $this->load->view('layouts/login');
    }
	
	public function _login_as(){
        if(!empty($_POST)){
            $this->load->model('sys_user');
            $user = $this->sys_user->login($_POST['login_name'],$_POST['login_pw']);
            if(count($user)>0){
                $this->session->set_userdata('is_logged_in',true);
                $this->session->set_userdata('username',$user[0]['username']);
                $this->session->set_userdata('id_user',$user[0]['id_user']);
                $this->session->set_userdata('id_group',$user[0]['id_group']);
                $this->session->set_userdata('id_personal',$user[0]['id_personal']);
				if(isset($user[0]['id_sekolah'])){
					$this->set_session_sekolah($user[0]['id_sekolah']);
				}
                redirect('sistem/index');
            }else{
                set_error_message("Maaf, Username dan Password Tidak Cocok!");
            }
        }
        $this->load->view('layouts/login');
    }
	
	public function set_session_sekolah($id_sekolah=1){
		$ids = $id_sekolah;
		if($ids>0){
			$this->referensi->table = "m_sekolah";
			$this->referensi->id = "id_sekolah";
			$sek = $this->referensi->get($ids);
			if(!empty($sek)){
				$this->session->set_userdata('sekolah',$sek);
			}
		}
	}
    
    public function logout(){
        $this->session->unset_userdata('is_logged_in');
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('id_user');
        $this->session->unset_userdata('id_group');
        $this->session->unset_userdata('id_personal');
        $this->session->unset_userdata('sekolah');
        redirect('sistem/login');
    }
    
    public function list_user(){
        $this->load->model('sys_user');
        $data['list'] = $this->sys_user->get_all();
        render('sistem/list_user',$data);
    }
    
    public function add_user(){
        $data = $_POST;
        if(empty($data)){
            render('sistem/add_user');
        }else{
            $this->load->model('sys_user');
            $this->user->save($data);
            set_success_message("Data Berhasil Disimpan!");
            redirect('sistem/list_user');
        }
    }
    
    public function change_password(){
        $data = $_POST;
        if(!empty($data)){
            if($data['password']==$data['password_confirm']){
                $this->load->model('sys_user');
                $user = $this->sys_user->get_by_field('username',$data['username']);
                if(count($user)>0){
                    $user = $user[0];
                    $user['password'] = md5($data['password']); 
                    $this->sys_user->update($user);
                    set_success_message('Password berhasil diperbaharui.');
                }
            }else{
                set_error_message("Perubahan password dibatalkan, karena kolom 'Password' dan 'Ulangi Password di Atas' tidak sama!");
            }
        }
        redirect("sistem/index");
    }
	
	public function setting_sekolah(){
        $data = $_POST;
        if(empty($data)){
            render('sistem/setting_sekolah');
        }else{
            $this->load->model('sekolah/m_sekolah');
			$idta = $_POST["id_tahun_ajaran_aktif"];
			$ids = $_POST["list_id_term"][$idta];
			
			$sek['id_sekolah'] = get_id_sekolah();
			$sek['id_tahun_ajaran_aktif'] = $idta;
			$sek['id_term_aktif'] = $ids;
			
			$this->m_sekolah->update($sek);
			$ids = $sek['id_tahun_ajaran_aktif'];

			$this->set_session_sekolah($ids);
			
            set_success_message("Setting Sekolah Berhasil Disimpan, Silahkan Logout dan Login Kembali.");
            redirect('sistem/setting_sekolah');
        }
    }
	
	public function change_sekolah(){
        $data = $_POST;
        if(empty($data)){
            render('sistem/change_sekolah');
        }else{
			$ids = $_POST["id_sekolah"];

			$this->set_session_sekolah($ids);
			
            set_success_message("Sekolah berhasil diubah");
            redirect('sistem/change_sekolah');
        }
    }
	
	public function generate_user_guru(){
		
		$q = 'INSERT INTO sys_user(username,password,default_password,email,id_personal,is_active,id_group,id_sekolah)
		SELECT NIK, md5(NIK), NIK, e_mail,id_guru,1,9,id_sekolah from m_guru
		WHERE 
		NIK not in (select username from sys_user)
		AND e_mail not in (select email from sys_user WHERE email <> "")
		AND NIK <> ""';
		$this->db->query($q);
		set_success_message("User guru berhasil digenerate, silahkan gunakan username NIK dan password NIK");
		redirect("/");
		/*INSERT INTO sys_user(username,password,default_password,email,id_personal,is_active,id_group,id_sekolah)
		SELECT NIK, md5(NIK), NIK, e_mail,id_guru,1,9,id_sekolah from m_guru
		WHERE 
		NIK not in (select username from sys_user)
		AND e_mail not in (select email from sys_user WHERE email <> '')
		AND NIK <> ""*/
		
		/* INSERT INTO sys_user(username,password,default_password,email,id_personal,is_active,id_group,id_sekolah)
		SELECT nisn, md5(nisn), nisn, e_mail,id_siswa,1,10,id_sekolah from m_siswa
		WHERE 
		nisn not in (select username from sys_user)
		AND e_mail not in (select email from sys_user WHERE email <> '')
		AND nisn <> ""
		AND nisn <> "-" */
	}
	
	public function generate_user_siswa(){		
		/*$q = 'INSERT INTO sys_user(username,password,default_password,email,id_personal,is_active,id_group,id_sekolah)
		SELECT nisn, md5(nisn), nisn, e_mail,id_siswa,1,10,id_sekolah from m_siswa
		WHERE 
		nisn not in (select username from sys_user)
		AND e_mail not in (select email from sys_user WHERE email <> "")
		AND nisn <> ""
		AND nisn <> "-"';
		$this->db->query($q);
		set_success_message("User siswa berhasil digenerate, silahkan gunakan username NISN dan password NISN");
		redirect("/");*/
	}
	
	
}
