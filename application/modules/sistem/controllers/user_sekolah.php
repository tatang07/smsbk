<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class user_sekolah extends MY_Controller {
    public function __construct(){
        parent::__construct();
        $this->load->model('m_user_sekolah');
    }

    public function index(){
		redirect('sistem/user_sekolah/datalist');
    }

    function datalist(){
        $data['user_group'] = $this->m_user_sekolah->get_user_group()->result_array();
        render('user_sekolah/datalist', $data);
    }

    function cari(){
        $cari = $this->input->post('cari');

        $data['cari'] = $this->m_user_sekolah->get_cari($cari)->result_array();
        render('user_sekolah/datalist', $data);
    }

    function load_add(){
        $data['user_group'] = $this->m_user_sekolah->get_user_group_add()->result_array();
        render('user_sekolah/add', $data);   
    }

    function submit_add(){
        $id_sekolah = get_id_sekolah();

        $data['id_sekolah'] = $id_sekolah;
        $data['is_active'] = '1';
        $data['username'] = $this->input->post('nama');
        $data['password'] =  md5($this->input->post('password'));
        $data['email'] = $this->input->post('email');
        $data['id_group'] = $this->input->post('id_group');

        $this->m_user_sekolah->add($data);
        redirect('sistem/user_sekolah/datalist');
    }

    function load_edit($id){
        $data['data_edit'] = $this->m_user_sekolah->get_user_by($id)->result_array();
        $data['user_group'] = $this->m_user_sekolah->get_user_group_add()->result_array();
        
        //print_r($data['data_edit']);
        render('user_sekolah/edit', $data);
    }

    function submit_edit(){
        //$data['password'] =  md5($this->input->post('password'));
        $id = $this->input->post('id_user');
        $data['email'] = $this->input->post('email');
        $data['password'] =  md5($this->input->post('password'));
        $data['id_group'] = $this->input->post('id_group');
        //echo $id;
        $this->m_user_sekolah->edit($id, $data);
        set_success_message("data berhasil di edit");
        redirect('sistem/user_sekolah/datalist');
    }

    function delete(){
        $id = $this->uri->rsegment(3);
        $this->m_user_sekolah->delete($id);
        set_success_message("data berhasil di hapus");
        redirect('sistem/user_sekolah/datalist');
    }

    function print_pdf($id_group){
        $id_sekolah=get_id_sekolah();
        $data['user_sekolah'] = $this->m_user_sekolah->datalist($id_sekolah, $id_group)->result_array();

        print_pdf($this->load->view('user_sekolah/print', $data, true), "A4");
        //$this->load->view('user_sekolah/print', $data);
    }
}