<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class user_guru extends MY_Controller {
   public function index()
    {   
        $this->load->model('sistem/m_user_guru');
        $id_sekolah = get_id_sekolah();
        $data['guru'] = $this->m_user_guru->get_data($id_sekolah)->result_array();
        render('user_guru/datalist', $data);
    }

    function cari(){
        $this->load->model('sistem/m_user_guru');
        $cari = $this->input->post('cari');
        $id_sekolah = get_id_sekolah();

        $data['guru'] = $this->m_user_guru->get_cari($cari, $id_sekolah)->result_array();
        
        //echo "sdjkcknsdjvcndskvc";
        //print_r($data['guru']);
        render('user_guru/datalist', $data);
    }

    function print_pdf(){
        $this->load->model('sistem/m_user_guru');
        $id_sekolah = get_id_sekolah();
        $data['guru'] = $this->m_user_guru->get_data($id_sekolah)->result_array();

        print_pdf($this->load->view('user_guru/print', $data, true), "A4");
        redirect(getenv('HTTP_REFERER'));
        //$this->load->view('user_guru/print', $data);
    }
	
	public function generate_user_guru(){
		$ids = get_id_sekolah();
		$q = 'INSERT INTO sys_user(username,password,default_password,email,id_personal,is_active,id_group,id_sekolah)
		SELECT NIK, md5(NIK), NIK, e_mail,id_guru,1,9,id_sekolah from m_guru
		WHERE 
		NIK not in (select username from sys_user)
		AND id_sekolah='.$ids.' 
		AND NIK <> ""';
		$this->db->query($q);
		set_success_message("User guru berhasil digenerate, silahkan gunakan username NIK dan password NIK");
		redirect("sistem/user_guru");
	}
}