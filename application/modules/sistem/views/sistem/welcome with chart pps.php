
<body>
	<div class="grid_12">
		<h1 class="grid_12">Dashboard</h1>
		<div class="grid_12">
			<p class="grid_12">
				Selamat datang di SMSBK.<br><br>
				SMSBK adalah Sistem Manajemen Sekolah Berbasis Keunggulan dengan menggunakan ICT secara penuh.
				<br><br>
				SMSBK ini penting diterapkan sebagai upaya untuk mewujudkan keunggulan dalam pengelolaan sekolah. Hal ini dimaksudkan untuk pengokohan performa sekolah yang dapat menampung kompleksitas manajemen dalam suatu perangkat kerja computerized yang simple tetapi komprehensif sehingga dapat meningkatkan kapasitas dan kapabilitas manajemen sekolah.
				<br/>&nbsp;
			</p>
		</div>
		<!-- BAGIAN DATA JUMLAH -->
		<div class="grid_12 center-elements">
			<?php foreach($data_sekolah as $s):?>
			<div class="full-stats" style="width:20%">
				<a href="<?php echo site_url('admin/jumlah_sekolah_'.$s['jenjang_sekolah'])?>" class="stat simple" data-value="<?php echo $s['total'] ?>" data-title="<?php echo "Jumlah Sekolah ".$s['jenjang_sekolah']?>" data-format="0,0 Sekolah"></a> <!-- clickable! -->
			</div>
			<?php endforeach ?>
			
		</div><!-- End of .grid_12 -->


		<div class="grid_12 center-elements">
			<?php foreach($data_guru as $s):?>
			<div class="full-stats" style="width:20%">
				<a href="<?php echo site_url('admin/jumlah_guru_'.$s['jenjang_sekolah'])?>" class="stat simple" data-value="<?php echo $s['total_guru'] ?>" data-title="<?php echo "Jumlah Pendidik ".$s['jenjang_sekolah']?>" data-format="0,0 Pendidik"></a> <!-- clickable! -->
			</div>
			<?php endforeach ?>
		</div><!-- End of .grid_12 -->


		<div class="grid_12 center-elements">
			<?php foreach($data_siswa as $s):?>
			<div class="full-stats" style="width:20%">
				<a href="<?php echo site_url('admin/jumlah_siswa_'.$s['jenjang_sekolah'])?>" class="stat simple" data-value="<?php echo $s['total_siswa'] ?>" data-title="<?php echo "Jumlah Siswa ".$s['jenjang_sekolah']?>" data-format="0,0 Siswa"></a> <!-- clickable! -->
			</div>
			<?php endforeach ?>
		</div><!-- End of .grid_12 -->


		<div class="grid_12 center-elements">
			<?php foreach($data_rombel as $s):?>
			<div class="full-stats" style="width:20%">
				<a href="<?php echo site_url('admin/jumlah_rombel_'.$s['jenjang_sekolah'])?>" class="stat simple" data-value="<?php echo $s['total_rombel'] ?>" data-title="<?php echo "Jumlah Rombel ".$s['jenjang_sekolah']?>" data-format="0,0 Rombel"></a> <!-- clickable! -->
			</div>
			<?php endforeach ?>
		</div>
	</div>
	
	
	<!-- BAGIAN GAJI -->
	<h1 class="grid_12">Status Gaji</h1>
	<div class="grid_12 center-elements">				
		<div class="full-stats" style="width:20%">
			<h2 class="center">Persentase di atas UMR</h2>
			<div class="stat circular green" data-valueformat="0,0"
			data-list="[
			{&quot;title&quot;:&quot;UMR&quot;,&quot;val&quot;:<?php echo $data_gaji[0]['atas_umr']?>, &quot;percent&quot;:<?php echo round($data_gaji[0]['atas_umr']/$data_gaji[0]['total_guru'] * 100)?>},
			{&quot;title&quot;:&quot;Total Tendik&quot;,&quot;val&quot;:<?php echo $data_gaji[0]['total_guru']?>, &quot;percent&quot;:0}
			]">
				<!--<canvas height="88" width="88"></canvas>
				<ul>
					<li class="pos"><span class="title">Di atas UMR</span><span class="value"><?php echo $data_gaji[0]['atas_umr']?></span></li>
					<li class="neg"><span class="title">Di bawah UMR</span><span class="value"><?php echo $data_gaji[0]['bawah_umr']?></span></li>
					<li class=""><span class="title">Jumlah Tendik</span><span class="value"><?php echo $data_gaji[0]['total_guru']?></span></li>
				</ul>-->
		</div>
		</div>
		<div class="full-stats" style="width:20%">
			<h2 class="center">Jumlah di atas UMR</h2>
			<div class="stat circular red" data-format="0" data-max="<?php echo $data_gaji[0]['total_guru']?>"
			data-list="[
			{&quot;title&quot;:&quot;UMR&quot;,&quot;val&quot;:<?php echo $data_gaji[0]['atas_umr']?>}
			]">
				<!--<canvas height="88" width="88"></canvas>
				<ul>
					<li class="pos"><span class="title">Di atas UMR</span><span class="value"><?php echo $data_gaji[0]['atas_umr']?></span></li>
					<li class="neg"><span class="title">Di bawah UMR</span><span class="value"><?php echo $data_gaji[0]['bawah_umr']?></span></li>
					<li class=""><span class="title">Jumlah Tendik</span><span class="value"><?php echo $data_gaji[0]['total_guru']?></span></li>
				</ul>-->
			</div>
		</div>
		<div class="full-stats" style="width:20%">
			<h2 class="center">Jumlah di bawah UMR</h2>
			<div class="stat circular red" data-format="0" data-max="<?php echo $data_gaji[0]['total_guru']?>"
			data-list="[
			{&quot;title&quot;:&quot;UMR&quot;,&quot;val&quot;:-<?php echo $data_gaji[0]['bawah_umr']?>},
			{&quot;title&quot;:&quot;Tidak ada info&quot;,&quot;val&quot;:-<?php echo $data_gaji[0]['total_guru'] - $data_gaji[0]['atas_umr'] - $data_gaji[0]['bawah_umr']?>}
			]">
				<!--<canvas height="88" width="88"></canvas>
				<ul>
					<li class="pos"><span class="title">Di atas UMR</span><span class="value"><?php echo $data_gaji[0]['atas_umr']?></span></li>
					<li class="neg"><span class="title">Di bawah UMR</span><span class="value"><?php echo $data_gaji[0]['bawah_umr']?></span></li>
					<li class=""><span class="title">Jumlah Tendik</span><span class="value"><?php echo $data_gaji[0]['total_guru']?></span></li>
				</ul>-->
			</div>
		</div>
		<div class="full-stats" style="width:20%">
			<h2 class="center">Persentase di bawa UMR</h2>
			<div class="stat circular red" data-valueformat="0,0"
			data-list="[
			{&quot;title&quot;:&quot;Bawah UMR&quot;,&quot;val&quot;:-<?php echo $data_gaji[0]['bawah_umr']?>, &quot;percent&quot;:-<?php echo round($data_gaji[0]['bawah_umr']/$data_gaji[0]['total_guru'] * 100)?>},
			{&quot;title&quot;:&quot;Tidak ada info&quot;,&quot;val&quot;:-<?php echo $data_gaji[0]['total_guru'] - $data_gaji[0]['atas_umr'] - $data_gaji[0]['bawah_umr']?>, &quot;percent&quot;:-<?php echo round(($data_gaji[0]['total_guru'] - $data_gaji[0]['atas_umr'] - $data_gaji[0]['bawah_umr'])/$data_gaji[0]['total_guru'] * 100)?>}
			]">
				<!--<canvas height="88" width="88"></canvas>
				<ul>
					<li class="pos"><span class="title">Di atas UMR</span><span class="value"><?php echo $data_gaji[0]['atas_umr']?></span></li>
					<li class="neg"><span class="title">Di bawah UMR</span><span class="value"><?php echo $data_gaji[0]['bawah_umr']?></span></li>
					<li class=""><span class="title">Jumlah Tendik</span><span class="value"><?php echo $data_gaji[0]['total_guru']?></span></li>
				</ul>-->
			</div>
		</div>		
	</div>
	
	
	<h1 class="grid_12">Status Gaji Testing</h1>
	<div class="grid_12 center-elements">				
		<div class="box with-table">
			<div class="header">
				<h2>Proporsi Gender</h2>
			</div>
			<div class="box">
			   <div class="content" style="height: 250px;">
						<table class=chart data-type=bars>
							<thead>
								<tr>
									<th>Data Gaji</th>
								</tr>
							</thead>
								<tbody>
									<tr>
										<th>Total</th>								
										<?php for($i = 1; $i <= $data_gaji[0]['total_guru']; $i++) {?>
											<?php $count=0; ?>
											
												<?php if($count==0){ ?>
												<td width='' class='center'><?php echo $i; ?> </td>
												<?php $count=1 ?>
												<?php } ?>
																						
										<?php }?>	
									</tr>
									<tr>
										<th>Total</th>								
										<?php for($i = 1; $i <= $data_gaji[0]['total_guru']; $i++) {?>
											<?php $count=0; ?>
											
												<?php if($count==0){ ?>
												<td width='' class='center'><?php echo $i; ?> </td>
												<?php $count=1 ?>
												<?php } ?>
																						
										<?php }?>	
									</tr>
									<tr>
										<th>Total</th>								
										<?php for($i = 1; $i <= $data_gaji[0]['total_guru']; $i++) {?>
											<?php $count=0; ?>
											
												<?php if($count==0){ ?>
												<td width='' class='center'><?php echo $i; ?> </td>
												<?php $count=1 ?>
												<?php } ?>
																						
										<?php }?>	
									</tr>
									<tr>
										<th>Total</th>								
										<?php for($i = 1; $i <= $data_gaji[0]['total_guru']; $i++) {?>
											<?php $count=0; ?>
											
												<?php if($count==0){ ?>
												<td width='' class='center'><?php echo $i; ?> </td>
												<?php $count=1 ?>
												<?php } ?>
																						
										<?php }?>	
									</tr>
								</tbody>
						</table>
				</div><!-- End of .content -->
			</div><!-- End of .box -->
		           
		</div>
	</div>
</body>