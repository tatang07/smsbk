	
	<h1 class="grid_12">Setting Sekolah</h1>
    <form action="" class="grid_12 validate" novalidate="novalidate" method="post" id="valid">
	    <fieldset>
	        <legend>Setting Sekolah</legend>
			
			<?php $list_ta = get_list_filter("m_tahun_ajaran","id_tahun_ajaran",array("tahun_awal","tahun_akhir"))?>
			<?php $ta = get_id_tahun_ajaran();?>
			<?php $term = get_id_term(); ?>
			
            <?php echo simple_form_dropdown('id_tahun_ajaran_aktif',$list_ta,$ta," id='idta' onChange=taChanged(this.value)",array('label'=>'Tahun Ajaran')) ?>
			<?php foreach($list_ta as $id=>$det):?>
				<?php $list_term = get_list_filter("m_term","id_term",array("term"),array('id_tahun_ajaran'=>$id)) ?>
				<div id='term<?php echo $id?>' <?php echo $ta!=$id?"style='display:none'":""?> class='listterm'>					
					<?php echo simple_form_dropdown("list_id_term[$id]",$list_term,$term,"",array('label'=>'Semester')) ?>
				</div>	
			<?php endforeach ?>
			
        </fieldset>
        <div class="actions">
            <div class="left">
                <input type="submit" value="Perbaharui" />                
            </div>
	    </div>
    </form>
	
	<script type="text/javascript">		
		function taChanged(id){
			$('.listterm').hide();
			if(id>0){
				$('#term'+id).show();				
			}
		}
	</script>