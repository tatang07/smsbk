<h1 class="grid_12">System User</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar User</h2>
       </div>
       <div class="tabletools">
            <div class="dataTables_filter">
                <form id="formPencarian" name="formPencarian" method="post" onsubmit="return find()">
                    <label>
                        <span class="text">Pencarian:</span>
                        <input name="filter[username]" type="text" aria-controls="DataTables_Table_0">
                        <a href="javascript:changePage(1)" original-title='Cari' class="button grey tooltip"><i class='icon-search'></i></a>
                    </label>
                </form>
            </div>
            <div class="right">
              <?php if(is_allow('sistem/user/add')):?>
					<a href="<?php echo site_url('sistem/user/add')?>"><i class="icon-plus"></i>Tambah Data User</a> 
              <?php endif ?>
            </div>
        </div>
        <div class="content">
            <div id="datatable" url="<?php echo site_url('sistem/user/datatable')?>" >
                <?php $this->load->view("user/datatable"); ?>
            </div>
        </div>
        
    </div>
 </div>
