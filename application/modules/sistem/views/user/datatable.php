        <?php $list_group = get_list('sys_group','id_group','groupname') ?>
        <div class="dataTables_wrapper"  role="grid">    
            <table class="styled" >
                <thead>
                    <tr>
                        <th>No.</th>
                        <th>Nama User</th>
                        <th>Password Default</th>
                        <th>Email</th>
                        <th>Group</th>
                        <th>Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $i=$offset+1 ?>
                    <?php foreach ($list as $a): ?>	
                        <tr>
                            <td width='10px' class='center'><?php echo $i++ ?></td>
                            <td><?php echo $a['username']?></td>
                            <td><?php echo $a['default_password']?></td>
                            <td><?php echo $a['email']?></td>
                            <td><?php echo $list_group[$a['id_group']] ?></td>
                            <td  width='150px' class='center'>
                                <?php if(is_allow('sistem/user/edit')):?>
                                    <a original-title="Edit" href="<?php echo site_url('sistem/user/edit/'.$a['id_user'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-pencil"></i> Edit</a>
                                <?php endif ?>
                                <?php if(is_allow('sistem/user/delete')):?>
                                    <a original-title="Delete" href="<?php echo site_url('sistem/user/delete/'.$a['id_user'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apa anda yakin akan menghapus data ini?')"><i class="icon-remove" ></i> Delete</a>
                                <?php endif ?>
                            </td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>
            <?php $this->load->view("elements/pagination") ?>
            
        </div>
            