<h1 class="grid_12">System User</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
       		<h2>Akses Pengguna Siswa<h2>
       </div>

       <div class="tabletools">
       		<div class="right">
   				<a href="<?php echo site_url('sistem/user_siswa/generate_user_siswa'); ?>">Buat Akun Siswa</a>
	       		<?php if(isset($id_rombel)){ ?>
	       				<a href="<?php echo base_url('sistem/user_siswa/print_pdf/'. $id_rombel.'/'.$id_tingkat_kelas); ?>"><i class="icon-print"></i>Cetak</a>
	       		<?php }?>
            </div>
       		<div class="dataTables_filter">
				<form action="<?php echo base_url('sistem/user_siswa/tampil'); ?>" name="myform" method="post" enctype="multipart/form-data">
					<table>
						<tr>
							<th width=100px align='left'><span class="text"><span>Tingkat</span></th>
							<td width=200px align='left'>
								<select name="tingkat_kelas" id="tingkat_kelas" onchange="submitform();">
									<option value='0'></option>
									<?php foreach($tingkat_kelas as $idt=>$t): ?>
										<?php if($idt == $id_tingkat_kelas){ ?>
											<?php //echo $id_tingkat_kelas; ?>
											<option selected value='<?php echo $idt; ?>' data-status-pilihan="<?php echo $idt; ?>"><?php echo $t; ?></option>
										<?php }else{ ?>
											<option value='<?php echo $idt; ?>' data-status-pilihan="<?php echo $idt; ?>"><?php echo $t; ?></option>
										<?php } ?>
									<?php endforeach; ?>
								</select>
							</td>
						</tr>

						<tr>
							<th width=100px align='left'><span class="text"><span>Kelas</span></th>
							<td width=200px align='left'>
								<select name="kelas" id="kelas" class="chosen-select">
									<?php if(isset($rombels)) echo $rombels; ?>
								</select>
							</td>
						</tr>

						<tr>
							<td colspan= '2' width=300px align='right'>
								<input type="hidden" value="id_tingkat_kelas">
								<input type="submit" value="Tampilkan" name=send />
							</td>
						</tr>
					</table>
				</form>
			</div>
        </div>

        <div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid"> 
					<table class="styled" >
						<thead>
							<tr>
								<th>No</th>
								<th>Username</th>
								<th>Default Password</th>
								<th>Nama Pengguna</th>
								<th>Kelas</th>
							</tr>
						</thead>
				
						<tbody>
							<?php if(isset($siswa)){ ?>
								<?php $i=1; ?>
							 		<?php foreach ($siswa as $s) { ?>
										<tr>
											<td width='7%' class='center'><?php echo $i; ?></td>
											<td width='' class='left'><?php echo $s['username'] ?></td>	
											<td width='' class='left'><?php echo $s['default_password'] ?></td>	
											<td width='' class='left'><?php echo $s['nama'] ?></td>	
											<td width='' class='left'><?php echo $s['rombel'] ?></td>							
										</tr>
										<?php $i++; ?>
									<?php } ?>
							<?php } ?>
						</tbody>
					</table>
					<div class="footer">

					</div>
				</div>
            </div>
        </div>  
    </div>
</div>

<script>
function generate_rombel(id_tingkat_kelas)
{
	$.post("<?php echo site_url('sistem/user_siswa/getRombel'); ?>/" + id_tingkat_kelas, function(data){
		$("#kelas").html(data);
		$("#kelas").trigger("chosen:updated");
	});
}

$(function(){
	$(document).on('change', '#tingkat_kelas', function(){
		generate_rombel($(this).val());
	});
});
</script>