<?php  
    $statuspil = 0;
    if(isset($_POST['id_group'])){
        $a = $_POST['id_group'];
        $statuspil = $a; 
    } 
?>
<h1 class="grid_12">System User</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar User</h2>
       </div>
    
       <div class="tabletools">
            <div class="right">
                <a href="<?php echo base_url('sistem/user_sekolah/print_pdf/'. $statuspil); ?>"><i class="icon-print"></i>Cetak</a> 
                <a href="<?php echo base_url('sistem/user_sekolah/load_add'); ?>"><i class="icon-plus"></i>Tambah</a> 
              <br><br>
            </div>
           <div class="dataTables_filter">
                    <table>
                        <tr>
                            <form action="" name="myform" method="post" enctype="multipart/form-data">
                                <th width=100px align='left'><span class="text"><span>Group User</span></th>
                                <td width=200px align='left'>
                                    <select name="id_group" id="id_group" onchange="submitform();">
                                        <option value="0">-Group-</option>
                                        <?php foreach($user_group as $ug): ?>                                        
                                            <?php if($ug['id_group'] == $statuspil){ ?>
                                                <option selected value='<?php echo $ug['id_group']; ?>' data-status-pilihan="<?php echo $ug['id_group']; ?>"><?php echo $ug['groupname']; ?></option>
                                            <?php }else{ ?>
                                                <option value='<?php echo $ug['id_group']; ?>' data-status-pilihan="<?php echo $ug['id_group']; ?>"><?php echo $ug['groupname']; ?></option>
                                            <?php } ?>
                                        <?php endforeach; ?>
                                    </select>
                                </td>
                            </form>

                            <form action="<?php echo base_url('sistem/user_sekolah/cari'); ?>" method="post" enctype="multipart/form-data">
                                <td width="30px"></td>
                                <td align="left"><input type="text" name="cari" id="cari"></td>
                                <td width="10px"></td>
                                <td width="1px">
                                    <input type="submit" name=send class="button grey tooltip" value="cari" />
                                </td>
                            </form>
                        </tr>
                    </table>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
                <div class="dataTables_wrapper"  role="grid">    
                    <table class="styled" >
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Username</th>
                                <th>Passord Default</th>
                                <th>Email</th>
                                <th>Group</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                                $this->load->model('m_user_sekolah');
                                $id_sekolah=get_id_sekolah();

                                $data['user_sekolah'] = $this->m_user_sekolah->datalist($id_sekolah, $statuspil)->result_array();

                                //print_r($data['user_sekolah']);
                            ?>

                            <?php $i=1; ?>
                            <?php 
                                if(isset($cari)){
                                    foreach($cari  as $u){ 
                            ?>
                                <tr>
                                    <td class='center'> <?php echo $i; ?> </td>
                                    <td class='center'> <?php echo $u['username'] ?> </td>
                                    <td class='center'> <?php echo $u['default_password'] ?> </td>
                                    <td class='center'> <?php echo $u['email'] ?> </td>
                                    <td class='center'> <?php echo $u['groupname'] ?> </td>
                                    <td class='center'> 
                                        <a href="<?php echo site_url('sistem/user_sekolah/load_edit/'.$u['id_user'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-share-alt"></i> edit</a>
                                        <a href="<?php echo site_url('sistem/user_sekolah/delete/'.$u['id_user'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>                                        
                                    </td>
                                </tr>
                            <?php $i++; ?>
                            
                            <?php }
                                }else{
                                    foreach($data['user_sekolah'] as $u){ 
                            ?>
                                <tr>
                                    <td class='center'> <?php echo $i; ?> </td>
                                    <td class='center'> <?php echo $u['username'] ?> </td>
                                    <td class='center'> <?php echo $u['default_password'] ?> </td>
                                    <td class='center'> <?php echo $u['email'] ?> </td>
                                    <td class='center'> <?php echo $u['groupname'] ?> </td>
                                    <td class='center'> 
                                        <a href="<?php echo site_url('sistem/user_sekolah/load_edit/'.$u['id_user'])?>" class="button small grey tooltip" data-gravity="s"><i class="icon-share-alt"></i> edit</a>
                                        <a href="<?php echo site_url('sistem/user_sekolah/delete/'.$u['id_user'])?>" class="button small grey tooltip" data-gravity="s" onclick="return confirm('Apakah Anda yakin akan menghapus data ini?')"><i class="icon-remove"></i> Hapus</a>                                       
                                </td>
                                </tr>
                            <?php $i++; ?>

                             <?php }
                                }
                            ?>
                        </tbody>
                    </table>
                    
                    <div class="footer">
                        <div class="dataTables_info">Jumlah Data : <?php //zecho $no-1; ?></div>
                        
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>
<script>
    function submitform()
    {
      document.myform.submit();
    }
</script>