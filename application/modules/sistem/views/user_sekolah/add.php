<h1 class="grid_12">System User</h1>

<form action="<?php echo base_url('sistem/user_sekolah/submit_add'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
	<fieldset>
		<legend>Penambahan User</legend>
			<div class="row">
				<label for="f1_normal_input">
					<strong>Nama User</strong>
				</label>
				<div>
					<input type="text" name="nama" value="" />
				</div>
			</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Password</strong>
			</label>
			<div>
				<input type="text" name="password" value="" />
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Email</strong>
			</label>
			<div>
				<input type="text" name="email" value="" />
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Group</strong>
			</label>
			<div>
				<select name="id_group">
                    <?php foreach($user_group as $ug): ?>
                        <option value='<?php echo $ug['id_group']; ?>'><?php echo $ug['groupname']; ?></option>
                    <?php endforeach; ?>
                </select>
			</div>
		</div>
	</fieldset>
	
	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" name=send />
			 <a href="<?php echo base_url('sistem/user_sekolah/index/'); ?>"> <input value="Batal" type="button"></a>
		</div>
		<div class="right">
		</div>
	</div><!-- End of .actions -->
</form><!-- End of .box -->