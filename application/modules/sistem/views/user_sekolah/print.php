<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar user untuk grup <?php echo $user_sekolah['0']['groupname']; ?></h2>
       </div>
			
		<div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled">
						<thead>
							<tr>
								<th>No</th>
								<th>Username</th>
								<th>Default Password</th>
								<th>Email</th>
								<th>Group</th>
							</tr>
						</thead>
						<tbody>

							<?php $i = 1; ?>
							<?php foreach($user_sekolah as $us){ ?>
								<tr>
									<td><?php echo $i?></td>
		                            <td><?php echo $us['username'] ?> </td>
		                            <td><?php echo $us['default_password'] ?> </td>
		                            <td><?php echo $us['email'] ?> </td>
		                            <td><?php echo $us['groupname'] ?> </td>
								</tr>
							<?php $i++;} ?>
						</tbody>
					</table>
				</div>		
			</div>		
		</div><!-- End of .box -->		
 	</div>
</div>
