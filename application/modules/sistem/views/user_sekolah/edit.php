<h1 class="grid_12">System User</h1>

<form action="<?php echo base_url('sistem/user_sekolah/submit_edit'); ?>" method="post" enctype="multipart/form-data" class="grid_12">
	<fieldset>
		<?php foreach($data_edit as $de){ ?>
		<legend>Edit User</legend>
			<div class="row">
				<label for="f1_normal_input">
					<strong>Nama User</strong>
				</label>
				<div>
					<input type="hidden" name="id_user" value="<?php echo $de['id_user'] ?>"/>
					<input type="text" name="nama" value="<?php echo $de['username'] ?>" disabled/>
				</div>
			</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Password Baru</strong>
			</label>
			<div>
				<input type="text" name="password" />
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Email</strong>
			</label>
			<div>
				<input type="text" name="email" value="<?php echo $de['email'] ?>" />
			</div>
		</div>
		
		<div class="row">
			<label for="f1_normal_input">
				<strong>Group</strong>
			</label>
			<div>
				<select name="id_group">
                    <?php foreach($user_group as $ug): ?>
                    	<?php if($ug['id_group'] == $de['id_group']){ ?>
                        	<option selected value='<?php echo $ug['id_group']; ?>'><?php echo $ug['groupname']; ?></option>
                    	<?php }else{ ?>
                        	<option value='<?php echo $ug['id_group']; ?>'><?php echo $ug['groupname']; ?></option>
                    	<?php } ?>
                    <?php endforeach; ?>
                </select>
			</div>
		</div>
		<?php } ?>
	</fieldset>
	
	<div class="actions">
		<div class="left">
			<input type="submit" value="Simpan" name=send />
			 <a href="<?php echo base_url('sistem/user_sekolah/index/'); ?>"> <input value="Batal" type="button"></a>
		</div>
		<div class="right">
		</div>
	</div><!-- End of .actions -->
</form><!-- End of .box -->