<h1 class="grid_12">System User</h1>
<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Akses Pengguna Guru</h2>
       </div>
    
       <div class="tabletools">
            <div class="right">
                <a href="<?php echo base_url('sistem/user_guru/generate_user_guru'); ?>">Buat Akun Guru</a> 
                <a href="<?php echo base_url('sistem/user_guru/print_pdf'); ?>"><i class="icon-print"></i>Cetak</a> 
              <br><br>
            </div>
           <div class="dataTables_filter">
                    <table>
                        <tr>
                            <form action="<?php echo base_url('sistem/user_guru/cari'); ?>" method="post" enctype="multipart/form-data">
                                <td width="30px"></td>
                                <td align="left"><input type="text" name="cari" id="cari"></td>
                                <td width="10px"></td>
                                <td width="1px">
                                    <input type="submit" name=send class="button grey tooltip" value="cari" />
                                </td>
                            </form>
                        </tr>
                    </table>
            </div>
            
        </div>
        <div class="content">
            <div id="datatable" url="">
                <div class="dataTables_wrapper"  role="grid">    
                    <table class="styled" >
                        <thead>
                            <tr>
                                <th>No.</th>
                                <th>Username</th>
                                <th>Passord Default</th>
                                <th>Email</th>
                                <th>Nama Pengguna</th>
                            </tr>
                        </thead>
                        <tbody>                           
                            <?php $i=1; foreach($guru as $u){ ?>
                                <tr>
                                    <td class='center'> <?php echo $i; ?> </td>
                                    <td class='left'> <?php echo $u['username'] ?> </td>
                                    <td class='left'> <?php echo $u['default_password'] ?> </td>
                                    <td class='left'> <?php echo $u['email'] ?> </td>
                                    <td class='left'> <?php echo $u['nama'] ?> </td>
                                </tr>
                            	<?php $i++; ?>

                            <?php } ?>
                        </tbody>
                    </table>
                    
                    <div class="footer">
                        <div class="dataTables_info">Jumlah Data : <?php echo $i-1;; ?></div>
                        
                    </div>
                </div>
            </div>
        </div>        
    </div>
</div>