<div class="grid_12">
    <div class="box with-table">
       <div class="header">
            <h2>Daftar user guru</h2>
       </div>
			
		<div class="content">
            <div id="datatable" url="">
				<div class="dataTables_wrapper"  role="grid">    
					<table class="styled">
						<thead>
							<tr>
								<th>No.</th>
                                <th>Username</th>
                                <th>Passord Default</th>
                                <th>Email</th>
                                <th>Nama Pengguna</th>
							</tr>
						</thead>
						<tbody>
							<?php $i = 1; ?>
							<?php foreach($guru as $us){ ?>
								<tr>
									<td><?php echo $i?></td>
		                            <td><?php echo $us['username'] ?> </td>
		                            <td><?php echo $us['default_password'] ?> </td>
		                            <td><?php echo $us['email'] ?> </td>
		                            <td><?php echo $us['nama'] ?> </td>
								</tr>
							<?php $i++;} ?>
						</tbody>
					</table>
				</div>		
			</div>		
		</div>
 	</div>
</div>
