<?php
class m_kpend extends MY_Model {
    public $table = "m_sekolah";
    public $id = "id_sekolah";
	
	function get_all_sekolah(){
		$this->db->select('id_sekolah, sekolah');
		$this->db->from('m_sekolah');
		$this->db->where('status_aktif', 1);
		$this->db->order_by("sekolah");
		return $this->db->get()->result_array();
	}

	function get_sekolah_like($nama=""){
		$this->db->select('id_sekolah, sekolah');
		$this->db->from('m_sekolah');
		$this->db->where('status_aktif', 1);
		$this->db->like('sekolah', $nama);
		$this->db->order_by("sekolah");
		return $this->db->get()->result_array();
	}

	function get_all_jenjang(){
		$this->db->select('jp.id_jenjang_pendidikan, jenjang_pendidikan');
		$this->db->from('r_jenjang_pendidikan jp');
		$this->db->join("m_guru g", "g.id_jenjang_pendidikan = jp.id_jenjang_pendidikan");
		$this->db->join("m_sekolah s", "g.id_sekolah = s.id_sekolah");
		$this->db->group_by("jp.id_jenjang_pendidikan");
		$this->db->order_by("jenjang_pendidikan");
		$this->db->where('s.status_aktif', 1);
		$this->db->where('g.status_aktif', 1);
		return $this->db->get()->result_array();
	}

	function get_num_jenjang($ids, $idjk){
		$this->db->select('COUNT(g.id_jenjang_pendidikan) as count');
		$this->db->from('m_guru g');
		$this->db->where('g.status_aktif', 1);
		$this->db->where('g.id_sekolah', $ids);
		$this->db->where('g.id_jenjang_pendidikan', $idjk);
		$this->db->group_by("id_jenjang_pendidikan");
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			$arr['count']=0;
			return $arr;
		}
	}

	
}
	