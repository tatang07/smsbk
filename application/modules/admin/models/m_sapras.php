<?php
class m_sapras extends MY_Model {
	
	function get_all_sekolah(){
		$this->db->select('id_sekolah, sekolah');
		$this->db->from('m_sekolah');
		$this->db->where('status_aktif', 1);
		$this->db->order_by("sekolah");
		return $this->db->get()->result_array();
	}

	function get_sekolah_like($nama=""){
		$this->db->select('id_sekolah, sekolah');
		$this->db->from('m_sekolah');
		$this->db->where('status_aktif', 1);
		$this->db->like('sekolah', $nama);
		$this->db->order_by("sekolah");
		return $this->db->get()->result_array();
	}

	function get_all_sarana(){
		$this->db->select('*');
		$this->db->from('r_jenis_sarana_prasarana');
		return $this->db->get()->result_array();
	}

	function get_num_sarana($ids, $idsapras){
		$this->db->select('COUNT(id_jenis_sarana_prasarana) as count');
		$this->db->from('m_sarana_prasarana');
		$this->db->where('id_sekolah', $ids);
		$this->db->where('id_jenis_sarana_prasarana', $idsapras);
		$this->db->group_by("id_jenis_sarana_prasarana");
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			$arr['count']=0;
			return $arr;
		}
	}

	
}
	