<?php
class m_kurikulum extends MY_Model {
	
	function get_all_sekolah(){
		$this->db->select('id_sekolah, sekolah');
		$this->db->from('m_sekolah');
		$this->db->where('status_aktif', 1);
		$this->db->order_by("sekolah");
		return $this->db->get()->result_array();
	}

	function get_sekolah_like($nama=""){
		$this->db->select('id_sekolah, sekolah');
		$this->db->from('m_sekolah');
		$this->db->where('status_aktif', 1);
		$this->db->like('sekolah', $nama);
		$this->db->order_by("sekolah");
		return $this->db->get()->result_array();
	}

	function get_num_silabus($ids){
		$this->db->select('COUNT(id_sekolah) as count');
		$this->db->from('m_silabus');
		$this->db->where('id_sekolah', $ids);
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			$arr['count']=0;
			return $arr;
		}
	}
	
	function get_num_rpp($ids){
		$this->db->select('COUNT(id_rpp) as count');
		$this->db->from('t_rpp r');
		$this->db->join("t_guru_matpel_rombel gmr", "gmr.id_guru_matpel_rombel = r.id_guru_matpel_rombel");
		$this->db->join("m_rombel mr", "mr.id_rombel = gmr.id_rombel");
		$this->db->where('mr.id_sekolah', $ids);
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			$arr['count']=0;
			return $arr;
		}
	}

	function get_num_jadwal($ids){
		$this->db->select('COUNT(id_sekolah) as count');
		$this->db->from('m_jadwal_pelajaran');
		$this->db->where('id_sekolah', $ids);
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			$arr['count']=0;
			return $arr;
		}
	}


	function get_num_agenda($ids){
		$this->db->select('COUNT(id_agenda_kelas) as count');
		$this->db->from('t_agenda_kelas r');
		$this->db->join("t_guru_matpel_rombel gmr", "gmr.id_guru_matpel_rombel = r.id_guru_matpel_rombel");
		$this->db->join("m_rombel mr", "mr.id_rombel = gmr.id_rombel");
		$this->db->where('mr.id_sekolah', $ids);
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			$arr['count']=0;
			return $arr;
		}
	}

	function get_num_ekskul($ids){
		$this->db->select('COUNT(id_sekolah) as count');
		$this->db->from('m_ekstra_kurikuler');
		$this->db->where('id_sekolah', $ids);
		$query = $this->db->get();
		
		if($query->num_rows() != 0){
			return $query->row_array();
		}else{
			$arr['count']=0;
			return $arr;
		}
	}
	
}
	