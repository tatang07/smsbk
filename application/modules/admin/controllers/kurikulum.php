<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class kurikulum extends CI_Controller {

	public function index(){	
		$this->load->model('admin/m_kurikulum');
		$data['sekolah'] = $this->m_kurikulum->get_all_sekolah();
		$data['kelengkapan']= array(
										array("id_kelengkapan"=>1, "kelengkapan"=>"Silabus"),
										array("id_kelengkapan"=>2, "kelengkapan"=>"RPP"),
										array("id_kelengkapan"=>3, "kelengkapan"=>"Jadwal Pelajaran"),
										array("id_kelengkapan"=>4, "kelengkapan"=>"Agenda Kelas"),
										array("id_kelengkapan"=>5, "kelengkapan"=>"Ekstakurikuler")
							);
		foreach ($data['sekolah'] as $sek) {
			$data['sum'][$sek['id_sekolah']][1] = $this->m_kurikulum->get_num_silabus($sek['id_sekolah']);
			$data['sum'][$sek['id_sekolah']][2] = $this->m_kurikulum->get_num_rpp($sek['id_sekolah']);
			$data['sum'][$sek['id_sekolah']][3] = $this->m_kurikulum->get_num_jadwal($sek['id_sekolah']);
			$data['sum'][$sek['id_sekolah']][4] = $this->m_kurikulum->get_num_agenda($sek['id_sekolah']);
			$data['sum'][$sek['id_sekolah']][5] = $this->m_kurikulum->get_num_ekskul($sek['id_sekolah']);
		}
		$data['component']="admin";
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// exit;
		render('kurikulum/kurikulum',$data);
	}
	
	public function search(){
		$nama=$this->input->post('nama_sekolah');
		$this->load->model('admin/m_kurikulum');
		$data['sekolah'] = $this->m_kurikulum->get_sekolah_like($nama);
		$data['kelengkapan']= array(
										array("id_kelengkapan"=>1, "kelengkapan"=>"Silabus"),
										array("id_kelengkapan"=>2, "kelengkapan"=>"RPP"),
										array("id_kelengkapan"=>3, "kelengkapan"=>"Jadwal Pelajaran"),
										array("id_kelengkapan"=>4, "kelengkapan"=>"Agenda Kelas"),
										array("id_kelengkapan"=>5, "kelengkapan"=>"Ekstakurikuler")
							);
		foreach ($data['sekolah'] as $sek) {
			$data['sum'][$sek['id_sekolah']][1] = $this->m_kurikulum->get_num_silabus($sek['id_sekolah']);
			$data['sum'][$sek['id_sekolah']][2] = $this->m_kurikulum->get_num_rpp($sek['id_sekolah']);
			$data['sum'][$sek['id_sekolah']][3] = $this->m_kurikulum->get_num_jadwal($sek['id_sekolah']);
			$data['sum'][$sek['id_sekolah']][4] = $this->m_kurikulum->get_num_agenda($sek['id_sekolah']);
			$data['sum'][$sek['id_sekolah']][5] = $this->m_kurikulum->get_num_ekskul($sek['id_sekolah']);
		}
		$data['component']="admin";
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// exit;
		render('kurikulum/kurikulum',$data);
	}

	public function print_pdf(){
		$this->load->model('admin/m_kurikulum');
		$data['sekolah'] = $this->m_kurikulum->get_all_sekolah();
		$data['kelengkapan']= array(
										array("id_kelengkapan"=>1, "kelengkapan"=>"Silabus"),
										array("id_kelengkapan"=>2, "kelengkapan"=>"RPP"),
										array("id_kelengkapan"=>3, "kelengkapan"=>"Jadwal Pelajaran"),
										array("id_kelengkapan"=>4, "kelengkapan"=>"Agenda Kelas"),
										array("id_kelengkapan"=>5, "kelengkapan"=>"Ekstakurikuler")
							);
		foreach ($data['sekolah'] as $sek) {
			$data['sum'][$sek['id_sekolah']][1] = $this->m_kurikulum->get_num_silabus($sek['id_sekolah']);
			$data['sum'][$sek['id_sekolah']][2] = $this->m_kurikulum->get_num_rpp($sek['id_sekolah']);
			$data['sum'][$sek['id_sekolah']][3] = $this->m_kurikulum->get_num_jadwal($sek['id_sekolah']);
			$data['sum'][$sek['id_sekolah']][4] = $this->m_kurikulum->get_num_agenda($sek['id_sekolah']);
			$data['sum'][$sek['id_sekolah']][5] = $this->m_kurikulum->get_num_ekskul($sek['id_sekolah']);
		}
		print_pdf($this->load->view('kurikulum/kurikulum_print', $data, true), "A4");
		render('kurikulum/kurikulum',$data);
	}
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */