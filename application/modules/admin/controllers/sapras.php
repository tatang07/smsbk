<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class sapras extends CI_Controller {

	public function index(){	
		$this->load->model('admin/m_sapras');
		$data['sekolah'] = $this->m_sapras->get_all_sekolah();
		$data['sarana'] = $this->m_sapras->get_all_sarana();
		foreach ($data['sekolah'] as $sek) {
			foreach ($data['sarana'] as $sar) {
				$data['sum'][$sek['id_sekolah']][$sar['id_jenis_sarana_prasarana']] = $this->m_sapras->get_num_sarana($sek['id_sekolah'], $sar['id_jenis_sarana_prasarana']);
			}
		}
		$data['component']="admin";
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// exit;
		render('sapras/sapras',$data);
	}
	
	public function search(){
		$nama=$this->input->post('nama_sekolah');
		$this->load->model('admin/m_sapras');
		$data['sekolah'] = $this->m_sapras->get_sekolah_like($nama);
		$data['sarana'] = $this->m_sapras->get_all_sarana();
		foreach ($data['sekolah'] as $sek) {
			foreach ($data['sarana'] as $sar) {
				$data['sum'][$sek['id_sekolah']][$sar['id_jenis_sarana_prasarana']] = $this->m_sapras->get_num_sarana($sek['id_sekolah'], $sar['id_jenis_sarana_prasarana']);
			}
		}
		$data['component']="admin";
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// exit;
		render('sapras/sapras',$data);
	}

	public function print_pdf(){
		$this->load->model('admin/m_sapras');
		$data['sekolah'] = $this->m_sapras->get_all_sekolah();
		$data['sarana'] = $this->m_sapras->get_all_sarana();
		foreach ($data['sekolah'] as $sek) {
			foreach ($data['sarana'] as $sar) {
				$data['sum'][$sek['id_sekolah']][$sar['id_jenis_sarana_prasarana']] = $this->m_sapras->get_num_sarana($sek['id_sekolah'], $sar['id_jenis_sarana_prasarana']);
			}
		}
		print_pdf($this->load->view('sapras/sapras_print', $data, true), "A4");
		render('sapras/sapras',$data);
	}
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */