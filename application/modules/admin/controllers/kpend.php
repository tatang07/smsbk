<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class kpend extends CI_Controller {

	public function index(){	
		$this->load->model('admin/m_kpend');
		$data['kualifikasi'] = $this->m_kpend->get_all_sekolah();
		$data['jenjang'] = $this->m_kpend->get_all_jenjang();
		foreach ($data['kualifikasi'] as $qual) {
			foreach ($data['jenjang'] as $jenj) {
				$data['sum'][$qual['id_sekolah']][$jenj['id_jenjang_pendidikan']] = $this->m_kpend->get_num_jenjang($qual['id_sekolah'], $jenj['id_jenjang_pendidikan']);
			}
		}
		$data['component']="admin";
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// exit;
		render('kpend/kpend',$data);
	}
	
	public function search(){
		$nama=$this->input->post('nama_sekolah');
		$this->load->model('admin/m_kpend');
		$data['kualifikasi'] = $this->m_kpend->get_sekolah_like($nama);
		$data['jenjang'] = $this->m_kpend->get_all_jenjang();
		foreach ($data['kualifikasi'] as $qual) {
			foreach ($data['jenjang'] as $jenj) {
				$data['sum'][$qual['id_sekolah']][$jenj['id_jenjang_pendidikan']] = $this->m_kpend->get_num_jenjang($qual['id_sekolah'], $jenj['id_jenjang_pendidikan']);
			}
		}
		$data['component']="admin";
		// echo "<pre>";
		// print_r($data);
		// echo "</pre>";
		// exit;
		render('kpend/kpend',$data);
	}

	public function print_pdf(){
		$this->load->model('admin/m_kpend');
		$data['kualifikasi'] = $this->m_kpend->get_all_sekolah();
		$data['jenjang'] = $this->m_kpend->get_all_jenjang();
		foreach ($data['kualifikasi'] as $qual) {
			foreach ($data['jenjang'] as $jenj) {
				$data['sum'][$qual['id_sekolah']][$jenj['id_jenjang_pendidikan']] = $this->m_kpend->get_num_jenjang($qual['id_sekolah'], $jenj['id_jenjang_pendidikan']);
			}
		}
		print_pdf($this->load->view('kpend/kpend_print', $data, true), "A4");
		render('kpend/kpend',$data);
	}
		
}



/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */