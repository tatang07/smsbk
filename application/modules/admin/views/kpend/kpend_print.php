<h1 class="grid_12">Pendidik dan Tenaga Kependidikan</h1>
<div class="grid_12">
   	<div class="box">
			
		<div class="header">
			<h2>Kualifikasi Pendidik dan Tenaga Kependidikan</h2>
		</div>
		<?php //print_r($kepsek);?>
			<div class="dataTables_wrapper"  role="grid">
				<table class="styled" >
					<thead>
						<tr>
							<td width='' class='center' rowspan="2">No.</td>
							<td width='' class='center' rowspan="2">Sekolah</td>
							<td width='' class='center' colspan="<?php echo sizeof($jenjang); ?>">Pendidikan</td>
							<td width='' class='center' rowspan="2">Beban Mengajar (rata-rata)</td>
						</tr>
						<tr>
							<?php foreach ($jenjang as $jenj) { ?>
								<td width='' class='center'><?php echo $jenj['jenjang_pendidikan'] ?></td>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
							if($kualifikasi){
							$no=1;
							foreach($kualifikasi as $temp){
						?>
						<tr>
							<td class='center'>
								<?php
									echo $no;
									$no++;
								?>
							</td>
							<td class='right'><?php echo $temp['sekolah'];?></td>
							<?php foreach ($jenjang as $jenj): ?>
								<td class='center'><?php echo $sum[$temp['id_sekolah']][$jenj['id_jenjang_pendidikan']]['count'];?></td>
							<?php endforeach ?>
							<td class='center'><?php echo "NAN";?></td>
						</tr>
						<?php
							}
							}
						?>
					</tbody>
				</table>
			</div>
		
	</div><!-- End of .box -->		
 </div>
