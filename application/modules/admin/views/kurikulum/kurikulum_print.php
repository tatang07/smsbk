<h1 class="grid_12">Kurikulum</h1>
<div class="grid_12">
   	<div class="box">
			
		<div class="header">
			<h2>Kelengkapan Kurikulum</h2>
		</div>
		<?php //print_r($kepsek);?>
			<div class="dataTables_wrapper"  role="grid">
				<table class="styled" >
					<thead>
						<tr>
							<td width='' class='center' rowspan="2">No.</td>
							<td width='' class='center' rowspan="2">Sekolah</td>
							<td width='' class='center' colspan="<?php echo sizeof($kelengkapan); ?>">Kelengkapan Kurikulum</td>
						</tr>
						<tr>
							<?php foreach ($kelengkapan as $sar) { ?>
								<td width='' class='center'><?php echo $sar['kelengkapan'] ?></td>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
							if($sekolah){
							$no=1;
							foreach($sekolah as $temp){
						?>
						<tr>
							<td class='center'>
								<?php
									echo $no;
									$no++;
								?>
							</td>
							<td class='right'><?php echo $temp['sekolah'];?></td>
							<?php foreach ($kelengkapan as $sar): ?>
								<td class='center'><?php echo $sum[$temp['id_sekolah']][$sar['id_kelengkapan']]['count'];?></td>
							<?php endforeach ?>
						</tr>
						<?php
							}
							}
						?>
					</tbody>
				</table>
			</div>
		
	</div><!-- End of .box -->		
 </div>
