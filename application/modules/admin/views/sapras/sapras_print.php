<h1 class="grid_12">Sarana dan Prasarana</h1>
<div class="grid_12">
   	<div class="box">
			
		<div class="header">
			<h2>Kualifikasi Sarana dan Prasarana</h2>
		</div>
		<?php //print_r($kepsek);?>
			<div class="dataTables_wrapper"  role="grid">
				<table class="styled" >
					<thead>
						<tr>
							<td width='' class='center' rowspan="2">No.</td>
							<td width='' class='center' rowspan="2">Sekolah</td>
							<td width='' class='center' colspan="<?php echo sizeof($sarana); ?>">Sarana</td>
						</tr>
						<tr>
							<?php foreach ($sarana as $sar) { ?>
								<td width='' class='center'><?php echo $sar['jenis_sarana_prasarana'] ?></td>
							<?php } ?>
						</tr>
					</thead>
					<tbody>
						<?php
							if($sekolah){
							$no=1;
							foreach($sekolah as $temp){
						?>
						<tr>
							<td class='center'>
								<?php
									echo $no;
									$no++;
								?>
							</td>
							<td class='right'><?php echo $temp['sekolah'];?></td>
							<?php foreach ($sarana as $sar): ?>
								<td class='center'><?php echo $sum[$temp['id_sekolah']][$sar['id_jenis_sarana_prasarana']]['count'];?></td>
							<?php endforeach ?>
						</tr>
						<?php
							}
							}
						?>
					</tbody>
				</table>
			</div>
		
	</div><!-- End of .box -->		
 </div>
