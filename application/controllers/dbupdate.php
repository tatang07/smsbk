<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dbupdate extends CI_Controller {

	public function index($version = false)
	{
		$this->load->library('migration');

		if($version){
			if ( ! ($schema_version = $this->migration->version($version)))
			{
				show_error($this->migration->error_string());
			}

			// Result of schema version migration
			elseif (is_numeric($schema_version))
			{
				echo 'Database upgraded to version: ' . $schema_version;
			}

		} else {
			if ( ! ($schema_version = $this->migration->latest()))
			{
				show_error($this->migration->error_string());
			}

			// Result of schema version migration
			elseif (is_numeric($schema_version))
			{
				echo 'Database upgraded to version: ' . $schema_version;
			}
			
		}

	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */