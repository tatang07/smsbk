<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function get_session_sekolah($key="1"){
	$CI =& get_instance();
	$sek = $CI->session->userdata("sekolah");
	if(isset($sek[$key]))
		return $sek[$key];
	else
		return false;
}

function get_id_sekolah(){
	return get_session_sekolah("id_sekolah");
}

function get_id_kurikulum(){
	return 2;
}

function get_nama_sekolah(){
	return get_session_sekolah("sekolah");
}

function get_id_tahun_ajaran(){
	return get_session_sekolah("id_tahun_ajaran_aktif");
}

function get_nama_tahun_ajaran(){
	$ta = get_session_sekolah("id_tahun_ajaran_aktif");
	$thn = get_list_filter("m_tahun_ajaran","id_tahun_ajaran",array("tahun_awal","tahun_akhir"));
	return $thn[$ta];
}

function get_id_term(){
	return get_session_sekolah("id_term_aktif");
}

function get_nama_term(){
	$term = get_session_sekolah("id_term_aktif");
	$smt = get_list("m_term","id_term","term");
	return $smt[$term];
}

function get_alamat_sekolah(){
	return get_session_sekolah("alamat");
}

function get_alamat_web(){
	return get_session_sekolah("website");
}

function get_kota(){
	return "Kota Bandung";
}

function get_jenjang_sekolah(){
	return get_session_sekolah("id_jenjang_sekolah");
}

function get_max_tingkat_kelas(){
	$id_tingkat_kelas=0;
	if(get_jenjang_sekolah()==2){
		$id_tingkat_kelas=6;
	}else if(get_jenjang_sekolah()==3){
		$id_tingkat_kelas=9;
	}else if(get_jenjang_sekolah()==4){
		$id_tingkat_kelas=12;
	}
	return $id_tingkat_kelas;
}

function get_rombel(){
	$ids = get_id_sekolah();
	$ta = get_id_tahun_ajaran();
	$rombel = get_list_filter("m_rombel","id_rombel",array("rombel"),array("id_sekolah"=>$ids, "id_tahun_ajaran"=>$ta), array("rombel"));
	return $rombel;
}

function get_rombel_akhir(){
	$ids = get_id_sekolah();
	$ta = get_id_tahun_ajaran();
	$idt = get_max_tingkat_kelas();
	$rombel = get_list_filter("m_rombel","id_rombel",array("rombel"),array("id_sekolah"=>$ids, "id_tahun_ajaran"=>$ta,"id_tingkat_kelas"=>$idt));
	return $rombel;
}

function get_tingkat_kelas(){
	$idj = get_jenjang_sekolah();
	$tk = get_list_filter("m_tingkat_kelas","id_tingkat_kelas",array("tingkat_kelas"),array("id_jenjang_sekolah"=>$idj));
	return $tk;
}

function get_pelajaran_guru_matpel_rombel(){
	$ids = get_id_sekolah();
	$ta = get_id_tahun_ajaran();
	$ci = & get_instance();
	$gid =  get_usergroup();
	$where = $gid==9?" AND gmr.id_guru=".get_id_personal()." " :"";
	$data = $ci->db->query("SELECT * FROM t_guru_matpel_rombel gmr,
							m_pelajaran p,
							m_rombel r
							WHERE p.id_pelajaran=gmr.id_pelajaran
							AND r.id_rombel=gmr.id_rombel
							AND r.id_tahun_ajaran = $ta
							AND r.id_sekolah = $ids
							$where
		")->result_array();
	
	$pel = array();
	foreach($data as $det){
		$pel[$det['id_rombel']][$det['id_guru_matpel_rombel']] = $det['kode_pelajaran']."-".$det['pelajaran'];
	}
	return $pel;
}

function get_id_pelajaran(){
	$ids = get_id_sekolah();
	$ta = get_id_tahun_ajaran();
	$ci = & get_instance();
	$data = $ci->db->query("SELECT * FROM t_guru_matpel_rombel gmr,
							m_pelajaran p,
							m_rombel r
							WHERE p.id_pelajaran=gmr.id_pelajaran
							AND r.id_rombel=gmr.id_rombel
							AND r.id_tahun_ajaran = $ta
							AND r.id_sekolah = $ids
		")->result_array();
	
	$pel = array();
	foreach($data as $det){
		$pel[$det['id_rombel']][$det['id_pelajaran']] = $det['kode_pelajaran']."-".$det['pelajaran'];
	}
	return $pel;
}

function get_rombel_komponen_nilai(){
	$ids = get_id_sekolah();
	$ta = get_id_tahun_ajaran();
	$ci = & get_instance();
	$data = $ci->db->query("SELECT * FROM m_rombel r, m_komponen_nilai k
							WHERE r.id_kurikulum=k.id_kurikulum
							AND r.id_tahun_ajaran = $ta
							AND r.id_sekolah = $ids
		")->result_array();
	
	$komponen = array();
	foreach($data as $det){
		$komponen[$det['id_rombel']][$det['id_komponen_nilai']] = $det['komponen_nilai'];
	}
	return $komponen;
}

function get_nilai_kategori(){
	$ids = get_id_sekolah();
	$ta = get_id_tahun_ajaran();
	$ci = & get_instance();
	$data = $ci->db->query("SELECT * FROM t_nilai_kategori nk
							WHERE nk.id_tahun_ajaran = $ta
							AND nk.id_sekolah = $ids
		")->result_array();
	
	$kat = array();
	foreach($data as $det){
		$kat[$det['id_komponen_nilai']][$det['id_nilai_kategori']] = $det['nama_nilai_kategori'];
	}
	return $kat;
}

function get_kelas_wali($id_guru=0){
	$ids = get_id_sekolah();
	$ta = get_id_tahun_ajaran();
	
	$ci = & get_instance();
	$data = $ci->db->query("SELECT * FROM t_guru_matpel_rombel gmr
							JOIN m_rombel r ON gmr.id_rombel=r.id_rombel
							WHERE r.id_tahun_ajaran = $ta
							AND r.id_sekolah = $ids
							AND 
		")->result_array();
	
	$pel = array();
	foreach($data as $det){
		$pel[$det['id_rombel']][$det['id_guru_matpel_rombel']] = $det['kode_pelajaran']."-".$det['pelajaran'];
	}
	return $pel;
}

function get_rombel_guru_matpel_rombel_by_guru($id_guru=0){
	$ids = get_id_sekolah();
	$ta = get_id_tahun_ajaran();
	$ci = & get_instance();
	$data = $ci->db->query("SELECT * FROM t_guru_matpel_rombel gmr,
							m_pelajaran p,
							m_guru g,
							m_rombel r
							WHERE p.id_pelajaran=gmr.id_pelajaran
							AND r.id_rombel=gmr.id_rombel
							AND g.id_guru=gmr.id_guru
							AND gmr.id_guru = $id_guru
							AND r.id_tahun_ajaran = $ta
							AND r.id_sekolah = $ids
		")->result_array();
	
	$pel = array();
	foreach($data as $det){
		$pel[$det['id_rombel']] = $det['rombel'];
	}
	return $pel;
}

function get_rombel_by_tingkat($id){
	$ids = get_id_sekolah();
	$ta = get_id_tahun_ajaran();
	$rombel = get_list_filter("m_rombel","id_rombel",array("rombel"),array("id_sekolah"=>$ids, "id_tahun_ajaran"=>$ta, "id_tingkat_kelas"=>$id));
	return $rombel;
}

function get_rombel_guru_matpel_rombel_by_guru_tingkat($id_guru=0, $tingkat){
	$ids = get_id_sekolah();
	$ta = get_id_tahun_ajaran();
	$ci = & get_instance();
	$data = $ci->db->query("SELECT * FROM t_guru_matpel_rombel gmr,
							m_pelajaran p,
							m_guru g,
							m_rombel r
							WHERE p.id_pelajaran=gmr.id_pelajaran
							AND r.id_rombel=gmr.id_rombel
							AND g.id_guru=gmr.id_guru
							AND gmr.id_guru = $id_guru
							AND r.id_tahun_ajaran = $ta
							AND r.id_sekolah = $ids
							AND r.id_tingkat_kelas = $tingkat
		")->result_array();
	
	$pel = array();
	foreach($data as $det){
		$pel[$det['id_rombel']] = $det['rombel'];
	}
	return $pel;
}

function get_matpel_per_tingkat($id_kurikulum){
	$tk = get_tingkat_kelas();
	$ci = & get_instance();
	$tk = implode(",",array_keys($tk));
	$q = "SELECT * FROM m_pelajaran p
							LEFT JOIN m_kelompok_pelajaran pk
							ON p.id_kelompok_pelajaran=pk.id_kelompok_pelajaran
							WHERE pk.id_kurikulum=$id_kurikulum AND p.id_tingkat_kelas IN ($tk)";
	$data = $ci->db->query($q)->result_array();
	$pel = array();
	foreach($data as $det){
		$pel[$det['id_tingkat_kelas']][$det['id_pelajaran']] = $det['kode_pelajaran'].'-'.$det['pelajaran'];
	}
	return $pel;
}

function get_kab_per_prop(){
	$ci = & get_instance();
	$data = $ci->db->query("SELECT * FROM r_kabupaten_kota")->result_array();
	$ret = array();
	foreach($data as $det){
		$ret[$det['id_provinsi']][$det['id_kabupaten_kota']] = $det['kabupaten_kota'];
	}
	return $ret;
}

function get_kec_per_kab(){
	$ci = & get_instance();
	$data = $ci->db->query("SELECT * FROM r_kecamatan")->result_array();
	$ret = array();
	foreach($data as $det){
		$ret[$det['id_kabupaten_kota']][$det['id_kecamatan']] = $det['kecamatan'];
	}
	return $ret;
}