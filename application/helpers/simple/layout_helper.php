<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
function render($v='index',$param=array(),$l='default'){
    $CI =& get_instance();
    $data = $param;
    $data['view']=$v;
    $CI->load->view('layouts/'.$l,$data);
}

function cek_page($page_name, $page=1, $is_reset=false){
	$ci = & get_instance();
	$ses_page = $ci->session->userdata('paging.'.$page_name);
	$page = $is_reset?$page:$ses_page;
	$page = empty($page)?1:$page;
	$page = $page*1;
	$ci->session->set_userdata('paging.'.$page_name,$page);
	return $page;
}