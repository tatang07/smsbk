<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
function generateNomor($table="", $save=false, $code_numbering=null, $format="2012#1", $length=3) {
        $CI =& get_instance();
        $CI->db->where("table_name", $table);
        $data = $CI->db->get("sys_code_numbering")->row_array();
        $code = "";
        $arrBulan = array("I","II","III","IV","V","VI","VII","VIII","IX","X","XI","XII");
        $new = false;
        if (empty($data)) {
            $data['table_name'] = $table;
            $data['code_numbering'] = $code_numbering;
            $data['current_state'] = $format;
            $data['length'] = $length;
            $new = true;
        }
        
        if (!empty($data)) {
            if ($code_numbering != null) {
                $data['code_numbering'] = $code_numbering;
            }
            $state = $data['current_state'];
            $tahunSekarang = date('Y');
            $bulanSekarang = date('m');
            $arrState = explode("#",$state);
            $nextState = "";
            if(count($arrState)==1){
                $code = $arrState[0];
                $nextNum = $code + 1;
                $nextState = $nextNum;
            }elseif(count($arrState)==2){
                if($arrState[0]==$tahunSekarang){
                    $code = $arrState[1];
                    $nextNum = $code + 1;
                    $nextState = $tahunSekarang."#".$nextNum;
                }else{
                    $code = "1";
                    $nextNum = $code + 1;
                    $nextState = $tahunSekarang."#".$nextNum;
                }
            }elseif(count($arrState)==3){
                if($arrState[0]==$tahunSekarang && $arrState[1]==$bulanSekarang){
                    $code = $arrState[2];
                    $nextNum = $code + 1;
                    $nextState = $tahunSekarang."#".$bulanSekarang."#".$nextNum;
                }else{
                    $code = "1";
                    $nextNum = $code + 1;
                    $nextState = $tahunSekarang."#".$bulanSekarang."#".$nextNum;
                }
            }else{
                $code = "1";
                $nextNum = "1";
                $nextState = "1";
            }

            $code = "$code";             
            for ($i = strlen($code); $i < $data['length']; $i++) {
                $code = "0" . $code;
            }
            $code = str_ireplace("{NUMBER}",$code,$data['code_numbering']);
            $tahun = date('Y');
            $tahun2 = substr($tahun,strlen($tahun)-2,2);
            $bulan = $arrBulan[date('m')-1];
            $code = str_ireplace("{BULAN_ROMAWI}",$bulan,$code);
            $code = str_ireplace("{BULAN}",date('m'),$code);
            $code = str_ireplace("{TAHUN}",$tahun,$code);
            $code = str_ireplace("{TAHUN_2}",$tahun2,$code);
            $data['current_state'] = $nextState;
            if($save){
                $CI->db->simple_query("update sys_code_numbering set current_state='$nextState' where table_name='$table'");
                if($new)
                    $CI->db->insert('sys_code_numbering',$data);
            }
        }
        return $code;
}