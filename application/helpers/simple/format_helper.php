<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
function tanggal_indonesia($tgl='0000-00-00') {
    $tgl = ($tgl=='0000-00-00')?"1990-01-01":$tgl;
	
    $tanggal = explode("-", $tgl);
    $nama_bulan = get_list_bulan();
    $bulan = $tanggal[1]*1;
	$time = "";
	if(strlen($tanggal[2])>3){
		$time = substr($tanggal[2],3);
		$tanggal[2] = substr($tanggal[2],0,3);
		
	}
    return $tanggal[2]." ".$nama_bulan[$bulan]." ".$tanggal[0]. " ".$time;
}

function get_list_bulan() {
    $data = array(
            1=>"Januari",
            2=>"Februari",
            3=>"Maret",
            4=>"April",
            5=>"Mei",
            6=>"Juni",
            7=>"Juli",
            8=>"Agustus",
            9=>"September",
            10=>"Oktober",
            11=>"November",
            12=>"Desember",
    );
    return $data;
}

function konversi_tanggal($tanggal="0000-00-00") {
    $tgl = explode("-", $tanggal);
	$time = "";
	if(strlen($tgl[2])>4){
		$time = " ".substr($tgl[2],3);
		$tgl[2] = substr($tgl[2],0,3);
		
	}
    return $tgl[2]."-".$tgl[1]."-".$tgl[0].$time;
}

function get_list_tahun($awal=2007,$akhir=0) {
    if($akhir==0) {
        $akhir = date('Y');
    }
    $ret = array();
    for($i=$awal;$i<=$akhir;$i++) {
        $ret["$i"] = $i;
    }
    return $ret;
}

function get_list_hari() {
    $data = array(
            1=>"Minggu",
            2=>"Senin",
            3=>"Selasa",
            4=>"Rabu",
            5=>"Kamis",
            6=>"Jumat",
            7=>"Sabtu"
    );
    return $data;
}

function get_hari($tanggal="1111-11-11") {
    $tgl = explode("-", $tanggal);
    $timestamp = mktime(0, 0, 0, $tgl[1], $tgl[2],  $tgl[0]);
    $day = date("w", $timestamp)+1;
    $listday = get_list_hari();
    return $listday[$day];
}

function tanggal_indonesia_pendek($tanggal='1111-11-11') {
    $tgl = konversi_tanggal($tanggal);
    $tgl = str_replace("-", "/", $tgl);
    return $tgl;
}

function cek_waktu($time) {
    $waktu = explode(":", $time);
    if((int)$waktu[2] >= 60) {
        $waktu[2] = $waktu[2]-60;
        $waktu[1] = $waktu[1]+1;
    }
    if((int)$waktu[1] >= 60) {
        $waktu[1] = $waktu[1]-60;
        $waktu[0] = $waktu[0]+1;
    }
    $time = implode(":", $waktu);
    return $time;
}

function selisih_waktu($time_awal, $time_akhir){
    $awal = explode(":", $time_awal);
    $akhir = explode(":", $time_akhir);
    $s = ($akhir[0]-$awal[0])*3600;
    $s += ($akhir[1]-$awal[1])*60;
    $s += ($akhir[2]-$awal[2]);
    return $s;
}

function get_usia($tgl){
	$bday = new DateTime($tgl);
 	$today = new DateTime('00:00:00'); 
	$diff = $today->diff($bday);
	if($tgl=="0000-00-00")
		return 0;
	return $diff->y;    
}

function get_durasi($tgl_awal, $tgl_akhir){
	$bday = new DateTime($tgl_awal);
 	$today = new DateTime($tgl_akhir); 
	$diff = $today->diff($bday);
    $data['y'] = $diff->y;
    $data['m'] = $diff->m;
    $data['d'] = $diff->d;
	return $data;    
}

function get_durasi_view($tgl_awal, $tgl_akhir){
	
    $awal = explode('-',$tgl_awal);
    $akhir = explode('-',$tgl_akhir);
	
	if($awal[0]==0 || $akhir[0]==0){
		return "Tanggal Kosong";
	}else{
	
		$bulan = get_list_bulan();
		foreach($awal as $id=>$det){
			$awal[$id] = $det * 1;
			$akhir[$id] = $akhir[$id] * 1;
		}
		if($awal[0]==$akhir[0]){
			$ret = $awal[2]." ".$bulan[$awal[1]]." s.d. ".$akhir[2]." ".$bulan[$akhir[1]].' '.$akhir[0];
			if($awal[1]==$akhir[1]){
				$ret = $awal[2]." s.d. ".$akhir[2]." ".$bulan[$akhir[1]].' '.$akhir[0];
				if($awal[2]==$akhir[2]){
					$ret = $awal[2]." ".$bulan[$akhir[1]].' '.$akhir[0];
				}
			}
		}else{
			$ret = $awal[2].'/'.$awal[1].'/'.$awal[0]." s.d. ".$akhir[2].'/'.$akhir[1].'/'.$akhir[0];
		}

		return $ret;
	}
}

function get_durasi_tahun($tgl_awal, $tgl_akhir){
    $data = get_durasi($tgl_awal, $tgl_akhir);
    $tahun = $data['y'];
    $bln = round($data['m']/12);
    if($bln>0.8){
        $bln=1;
    }
    $tahun = $tahun+$bln;
    return $tahun;
}

function tanggal_db($tgl="00/00/0000"){
    $tgl = str_replace("/", "-", $tgl);
    $tgl = konversi_tanggal($tgl);
    return $tgl;
}

function tanggal_view($tgl="0000-00-00"){
    $tgl = konversi_tanggal($tgl);
    $tgl = str_replace("-", "/", $tgl);
    
    return $tgl;
}

function zero_padding($num="0",$length=0){
    while(strlen($num)<$length){
        $num = "0".$num;
    }
    return $num;
}

function generate_random_string($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, strlen($characters) - 1)];
    }
    return $randomString;
}

 function numberToRoman($num)
 {
     // Make sure that we only use the integer portion of the value
     $n = intval($num);
     $result = '';
 
     // Declare a lookup array that we will use to traverse the number:
     $lookup = array('M' => 1000, 'CM' => 900, 'D' => 500, 'CD' => 400,
     'C' => 100, 'XC' => 90, 'L' => 50, 'XL' => 40,
     'X' => 10, 'IX' => 9, 'V' => 5, 'IV' => 4, 'I' => 1);
 
     foreach ($lookup as $roman => $value)
     {
         // Determine the number of matches
         $matches = intval($n / $value);
 
         // Store that many characters
         $result .= str_repeat($roman, $matches);
 
         // Substract that from the number
         $n = $n % $value;
     }
 
     // The Roman numeral should be built, return it
     return $result;
 }
 

 
function number_to_words ($x){
     $nwords = array(  "", "satu", "dua", "tiga", "empat", "lima", "enam", 
	      	  "tujuh", "delapan", "sembilan", "sepuluh", "sebelas", "dua belas", "tiga belas", 
	      	  "empat belas", "lima belas", "enam belas", "tujuh belas", "dela panbelas", 
	     	  "sembilan belas", "dua puluh", 30 => "tiga puluh", 40 => "empat puluh",
                     50 => "lima puluh", 60 => "enam puluh", 70 => "tujuh puluh", 80 => "delapan puluh",
                     90 => "sembilan puluh" );
					 
     if(!is_numeric($x))
     {
         $w = '#';
     }else if(fmod($x, 1) != 0)
     {
         $w = '#';
     }else{
         if($x < 0)
         {
             $w = 'minus ';
             $x = -$x;
         }else{
             $w = '';
         }
         if($x < 21)
         {
             $w .= $nwords[$x];
         }else if($x < 100)
         {
             $w .= $nwords[10 * floor($x/10)];
             $r = fmod($x, 10);
             if($r > 0)
             {
                 $w .= ' '. $nwords[$r];
             }
         } else if($x < 1000)
         {
		 
			$v = floor($x/100);
			if( $v ==  1){
				$w .= 'seratus';
			}else{
				$w .= $nwords[$v] .' ratus';
			}
             
             $r = fmod($x, 100);
             if($r > 0)
             {
                 $w .= ' '. number_to_words($r);
             }
         } 
		 else if($x < 1000000)
         {
			$v = floor($x/1000);
			if($v ==  1){
				$w .= 'seribu';
			}else{
				$w .= number_to_words($v) .' ribu';
			}
		
            $r = fmod($x, 1000);
            if($r > 0)
            {
                $w .= ' ';
                if($r < 100)
                {
					$w .= ' ';
                }
                $w .= number_to_words($r);
            }
         } 
		 else if($x < 1000000000)
         {
		 
			$w .= number_to_words(floor($x/1000000)) .' juta';
             $r = fmod($x, 1000000);
             if($r > 0)
             {
                 $w .= ' ';
                 if($r < 100)
                 {
                     $w .= ' ';
                 }
                 $w .= number_to_words($r);
             }
         } 
		 else if($x < 1000000000000){
			
             $w .= number_to_words(floor($x/1000000000)) .' milyar';
             $r = fmod($x, 1000000000);
             if($r > 0)
             {
                 $w .= ' ';
                 if($r < 100)
                 {
                     $w .= ' ';
                 }
                 $w .= number_to_words($r);
             }
		 }
		 else {
			 $w .= number_to_words(floor($x/1000000000000)) .' triliun';
             $r = fmod($x, 1000000000000);
             if($r > 0)
             {
                 $w .= ' ';
                 if($r < 100)
                 {
                     $w .= ' ';
                 }
                 $w .= number_to_words($r);
             }
         }
     }
     return $w;
}

function dateToHari($date="1997-09-09"){
    $idHari = date("N",strtotime($date));
    $arrHari = array("Senin","Selasa","Rabu","Kamis","Jum'at","Sabtu","Minggu");
    return $arrHari[$idHari-1];
}

function viewUang($val=0){
	return number_format($val, 0, ',', '.');
}