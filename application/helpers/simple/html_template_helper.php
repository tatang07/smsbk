<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
function simple_form_input($data = '', $value = '', $extra = '') {
	$template_data = sf_input($data, $value, $extra);
	$ci = & get_instance();
	$ci->load->view("html_template/input_template", $template_data);
}

function simple_form_input_clear($data = '', $value = '', $extra = '') {
	$template_data = sf_input($data, $value, $extra);
	$ci = & get_instance();
	$ci->load->view("html_template/input_template_clear", $template_data);
}

function simple_form_hidden($data = '', $value = '', $extra = '') {
	$template_data = sf_input($data, $value, $extra);
	$ci = & get_instance();
	$ci->load->view("html_template/hidden_template", $template_data);
}

function simple_form_input2($data = '', $value = '', $extra = '') {
	$template_data = sf_input($data, $value, $extra);
	$ci = & get_instance();
	$ci->load->view("html_template/input_template2", $template_data);
}

function sf_input($data = '', $value = '', $extra = '') {
    $val = !is_array($data)?simple_cek_value($data):simple_cek_value($data['name']);
    if(!empty($val)) {
        $value=$val;
    }
    $defaults = array('type' => 'text', 'name' => (( ! is_array($data)) ? $data : ''), 'value' => $value);     
    //return prefix_open($data)."<input "._parse_form_attributes($data, $defaults).$extra." />".prefix_close();
	
	$template_data = simple_prep_data($data);
	$template_data['extra'] = $extra;
	$template_data['attributes'] = simple_parse_form_attributes($data, $defaults);
	
	return $template_data;
}

function simple_form_textarea($data = '', $value = '', $extra = '') {
	$template_data = sf_textarea($data, $value, $extra);
	$ci = & get_instance();
	$ci->load->view("html_template/textarea_template", $template_data);
}

function simple_form_textarea2($data = '', $value = '', $extra = '') {
	$template_data = sf_textarea($data, $value, $extra);
	$ci = & get_instance();
	$ci->load->view("html_template/textarea_template2", $template_data);
}

function sf_textarea($data = '', $value = '', $extra = '') {
    $val = !is_array($data)?simple_cek_value($data):simple_cek_value($data['name']);
    if(!empty($val)) {
        $value=$val;
    }
    $defaults = array('name' => (( ! is_array($data)) ? $data : ''), 'cols' => '90', 'rows' => '12');

    if ( ! is_array($data) OR ! isset($data['value'])) {
        $val = $value;
    }
    else {
        $val = $data['value'];
        unset($data['value']); // textareas don't use the value attribute
    }

    $name = (is_array($data)) ? $data['name'] : $data;
    //return  prefix_open($data)."<textarea "._parse_form_attributes($data, $defaults).$extra.">".form_prep($val, $name)."</textarea>".prefix_close();
	$template_data = simple_prep_data($data);
	$template_data['extra'] = $extra;
	$template_data['attributes'] = simple_parse_form_attributes($data, $defaults);
	$template_data['value'] = simple_form_prep($val, $name);
			
	return $template_data;
}

function simple_form_wysiwyg($data = '', $value = '', $extra = '') {
	$template_data = sf_textarea($data, $value, $extra);
		
	$ci = & get_instance();
	$ci->load->view("html_template/wysiwyg_template", $template_data);	
}

function simple_form_dropdown_dual($name = '', $options = array(), $selected = array(), $extra = '', $data=array()) {
	$extra .= " multiple ";
    $template_data = sf_dropdown($name, $options, $selected, $extra, $data);
	
	$ci = & get_instance();
	$ci->load->view("html_template/dual_select_template", $template_data);
}

function simple_form_dropdown($name = '', $options = array(), $selected = array(), $extra = '', $data=array()) {
	$template_data = sf_dropdown($name, $options, $selected, $extra, $data);
	
	$ci = & get_instance();
	$ci->load->view("html_template/dropdown_template", $template_data);
}

function simple_form_tags_select($name = '', $options = array(), $selected = array(), $extra = '', $data=array()) {
	$template_data = sf_dropdown($name, $options, $selected, $extra, $data);
	
	$ci = & get_instance();
	$ci->load->view("html_template/tags_select_template", $template_data);
}

function simple_form_dropdown2($name = '', $options = array(), $selected = array(), $extra = '', $data=array()) {
	$template_data = sf_dropdown($name, $options, $selected, $extra, $data);
	
	$ci = & get_instance();
	$ci->load->view("html_template/dropdown_template2", $template_data);
}
function simple_form_dropdown_clear($name = '', $options = array(), $selected = array(), $extra = '', $data=array()) {
	$template_data = sf_dropdown($name, $options, $selected, $extra, $data);
	
	$ci = & get_instance();
	$ci->load->view("html_template/dropdown_template_clear", $template_data);
}
function sf_dropdown($name = '', $options = array(), $selected = array(), $extra = '', $data=array()) {
    $data['name'] = $name;
	//cek for default value
    $val = !is_array($selected)?simple_cek_value($name):$selected;
    if(!empty($val)) {
        $selected=$val;
    }
    if (!is_array($selected)) {
        $selected = array($selected);
    }    
    // If no selected state was submitted we will attempt to set it automatically
    if (count($selected) === 0) {
        // If the form name appears in the $_POST array we have a winner!
        if (isset($_POST[$name])) {
            $selected = array($_POST[$name]);
        }
    }

    if ($extra != '') $extra = ' '.$extra;

    $multiple = (count($selected) > 1 && strpos($extra, 'multiple') === FALSE) ? ' multiple="multiple"' : '';
	$extra = $extra.$multiple;

    //return  prefix_open($data).$form.prefix_close();	
	$template_data = simple_prep_data($data);
	$template_data['selected'] = $selected;
	$template_data['extra'] = $extra;
	$template_data['options'] = $options;
	
	
	return $template_data;
}

function simple_prep_data($data="") {	
    $name = !is_array($data)?$data:"";
    $name = isset($data['name'])?$data['name']:$name;
    
    $subject = "";
    if(is_array($data)) {
        if(isset($data['label']))
            $subject = $data['label'];
        else {
            $subject = str_replace("_", " ", $data['name']);
            $subject = ucwords($subject);
        }
    }elseif(!empty($data)) {
        $subject = str_replace("_", " ", $data);
        $subject = ucwords($subject);
    }
    $class_label = isset($data['class_label'])?$data['class_label']:"";
    $class_div = isset($data['class_div'])?$data['class_div']:"row";
	
	$ret['name'] = $name;
	$ret['subject'] = $subject;
	$ret['class_label'] = $class_label;
	$ret['class_div'] = $class_div;
	
	return $ret;
}

function simple_cek_value($field="") {
    $nama = $field;
    $nama = str_replace("[", ".", $nama);
    $nama = str_replace("]", "", $nama);
    $nama = str_replace("..", ".", $nama);
    $arr = explode(".",$nama);
    $nama = implode("']['",$arr);
    $nama = "['".$nama."']";
    $nilai = "";

    eval("\$ada=isset(\$_POST".$nama.");");
    if($ada) {
        eval("\$nilai=\$_POST".$nama.";");
    }
    return $nilai;
}

//Diambil dari system/helper/form_helper.php
function simple_parse_form_attributes($attributes, $default){
		if (is_array($attributes))
		{
			foreach ($default as $key => $val)
			{
				if (isset($attributes[$key]))
				{
					$default[$key] = $attributes[$key];
					unset($attributes[$key]);
				}
			}

			if (count($attributes) > 0)
			{
				$default = array_merge($default, $attributes);
			}
		}

		$att = '';

		foreach ($default as $key => $val)
		{
			if ($key == 'value')
			{
				$val = simple_form_prep($val, $default['name']);
			}

			$att .= $key . '="' . $val . '" ';
		}

		return $att;
}

//Diambil dari system/helper/form_helper.php
function simple_form_prep($str = '', $field_name = ''){
		static $prepped_fields = array();

		// if the field name is an array we do this recursively
		if (is_array($str))
		{
			foreach ($str as $key => $val)
			{
				$str[$key] = simple_form_prep($val);
			}

			return $str;
		}

		if ($str === '')
		{
			return '';
		}

		// we've already prepped a field with this name
		// @todo need to figure out a way to namespace this so
		// that we know the *exact* field and not just one with
		// the same name
		if (isset($prepped_fields[$field_name]))
		{
			return $str;
		}

		$str = htmlspecialchars($str);

		// In case htmlspecialchars misses these.
		$str = str_replace(array("'", '"'), array("&#39;", "&quot;"), $str);

		if ($field_name != '')
		{
			$prepped_fields[$field_name] = $field_name;
		}

		return $str;
	}


