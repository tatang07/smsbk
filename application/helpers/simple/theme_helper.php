<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

function populate_pagination($list=array(), $total_size, $page){
	$mod = $total_size % PER_PAGE;
	$total_page = floor($total_size/PER_PAGE);
	if($mod != 0){
		$total_page += 1;
	}
		
	$start = ($page-1)*PER_PAGE;
	$point = round(MAX_PAGE_VIEW/2);
		
	if ($page - $point < 1) {
		$first_page = 1;
	} else {
		$first_page = $page - ($point - 1);
	}

	$last_page = $first_page + MAX_PAGE_VIEW - 1;

	if ($last_page > $total_page) {
		$last_page = $total_page;
		$first_page = $total_page - MAX_PAGE_VIEW + 1;
	}
		
	if($first_page<1){
		$first_page = 1;
	}
		
	$data_list = array(
		"list" => $list,
		"first_page" => $first_page,
		"last_page" => $last_page,
		"total_page" => $total_page,
		"total_size" => $total_size,
		"current_page" => $page,
		"next_page" => $page+1,
		"prev_page" => $page-1,
		"show_prev" => ($page > 1),
		"show_next" => ($page < $total_page),
		"start_row" => $start
	);
	return $data_list;
}

function sortby($field){
	return " class='sorting' onClick=sortby('$field') ";
}

function icon_excel_14(){
    return get_img_icon("theme/img/icons/packs/iconsweets2/14x14/excel-document.png");
}

function icon_cetak_14(){
    return get_img_icon("theme/img/icons/packs/iconsweets2/14x14/printer.png");
}

function get_img_icon($src=""){
    return "<img src='".site_url()."/$src'/>";
}
