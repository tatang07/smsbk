<?php
function print_pdf($c = '', $p = 'letter', $o = 'p', $filename="dompdf_out", $header="@", $css="@", $usePaging=true, $margin="10mm 10mm 10mm 10mm"){
	$margin = explode(" ",$margin);
    $nama_organisasi = get_nama_sekolah();
    $daerah = get_kota();
    $alamat = get_alamat_sekolah();
	$web=get_alamat_web();
	$logo = site_url("extras/sekolah/".get_session_sekolah("logo"));

    $cssDefault = "<style type='text/css'>
		<!--
		
		.kotakfoto{
			width: 30mm;
			height: 25mm;
			border: solid 1px black;
			padding-top: 15mm;
			text-align: center;
			margin:0px;
			vertical-align: top;
		}
		
		.kotakfotoisi{
			width: 10mm;
			height: 10mm;
			border: solid 1px black;
			padding: 1mm;
			text-align: center;
			margin:0px;
			margin-left: 0mm;
			vertical-align: middle;
		}
		
		.tbl{
			border: solid 0.1mm black;
			border-spacing:0;
			border-collapse:collapse;
			padding: 3px;			
			font-size: 9pt;
			margin:auto;
			width:100%;
			
		}
		
		.tbl2{
			border: solid 0.1mm black;
			border-spacing:0;
			border-collapse:collapse;
			padding: 3px;			
			font-size: 9pt;
			
			width:100%;
			
		}
		
		.tbl2 th{
			text-align: center;
			vertical-align: middle;
			border: solid 0.3mm black;
			padding:3px;
		}
		
		.tbl2 td{
			border: solid 0.3mm black;
			padding: 3px;
		}
		
		.tbl th{
			text-align: center;
			vertical-align: middle;
			border: solid 0.3mm black;
			padding:3px;
		}
		.center{
			text-align: center;
		}
		.tbl td{
			border: solid 0.3mm black;
			padding: 3px;
		}
		
		.clear{
			border: 0px;
			border-spacing:0;
			border-collapse:collapse;
			padding: 2px;			
			font-size: 10pt;
			text-align: center;
			vertical-align: middle;
			margin:auto;
			width:100%;
		}
		
		.judul{
			font-size: 16px;
			font-weight: bold;
			margin:1px;
		}
		.judultimes{
			font-size: 18px;
			font-weight: bold;
			font-family:Times;
			margin:1px;
		}
		.alamat{
			font-size: 12px;
		}
		
		.pnumber{
			font-size: 8pt;
			text-align:center;
		}
		
		.subjudul{
			font-size: 10pt;
			font-weight: bold;
			text-align:center;
			margin:1px;
		}
		
		.hrtop{
			border-top:1px solid;
			border-bottom:3px solid;
			height:2px;
		}
		.hrbottom{
			border: 1px;
			margin-top: -5px;
		}
		h2{
			font-size: 12pt;
		}
		h6 {page-break-before:always}
		
		.judultop{
			margin-top: -10px;
		}
		-->
		</style>";
    
    $headerDefault = '
    <table class="clear">
        <tr>
            <td style="width: 15%"><img src="'.$logo.'" height="80"/></td>
            <td style="width: 85%">
				<p class=judultimes>'.$nama_organisasi.'</p>
                <p class=alamat>'.$alamat.'<br/>
                '.$web.'</p>
			</td>
			
        </tr>
    </table>
    <div class=hrtop></div>
	<br/>
	';
    $header = $header=="@"?$headerDefault:$header;
    $css = $css=="@"?$cssDefault:$css;
	$pageNumber = $usePaging?'<p class="pnumber">Halaman [[page_cu]] dari [[page_nb]] </p>':"";
	$template = $css.'
	<page backtop="'.$margin[0].'" backbottom="'.$margin[2].'" backleft="'.$margin[3].'" backright="'.$margin[1].'"> 
        <page_header> 
              
        </page_header> 
        <page_footer> 
             '.$pageNumber.'
        </page_footer> 
   
        '.$header.$c.' 
   </page> ';

    $c = $template;

	require_once('libs/html2pdf/html2pdf.class.php');

	$local = array("::1", "127.0.0.1");
	$is_local = in_array($_SERVER['REMOTE_ADDR'], $local);
	$is_local = true;
		
	if($o=='p'){
		$o = 'P';
	}else if($o=='l'){
		$o = 'L';
	}
	
	if(!is_array($p)){
		if(strtolower($p)=='letter'){
			$p='A4';
		}
	}
	
	if ( isset( $c ) && $is_local ) {
		if ( get_magic_quotes_gpc() )
			$c = stripslashes($c);
			

		try
		{
			$html2pdf = new HTML2PDF($o, $p, 'en');
			$html2pdf->setDefaultFont('Arial');
			$html2pdf->writeHTML($c, isset($_GET['vuehtml']));
			$html2pdf->Output($filename.'.pdf');
		}
		catch(HTML2PDF_exception $e) {
			echo $e;
			exit;
		}

	}
}
