<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');

//$list_agama = get_list_referensi("agama",false);
function get_list_referensi($ref_name="ref",$isOrder=true) {
	$ci = & get_instance();
    return $ci->referensi->get_list_referensi($ref_name,$isOrder);
}

//$list_agama = get_list("r_agama","id_agama","agama",false);
function get_list($table,$key,$val,$isOrder=true){	
	$ci = & get_instance();
	return $ci->referensi->get_list($table,$key,$val,$isOrder);
}

//$list_siswa = get_list_filter("m_siswa","id_siswa",
//								array("nis","nama"),
//								array('id_sekolah'=>1,'nama <>'=>'Tony'), 
//								array("nis","nama"));
function get_list_filter($table,$key,$vals,$filter=array(),$order=array()){	
	$ci = & get_instance();
	return $ci->referensi->get_list_filter($table,$key,$vals,$filter,$order);
}

function get_list_including_null($table,$key,$val,$isOrder=true){
	$ci = & get_instance();
	$data = $ci->referensi->get_list($table,$key,$val,$isOrder);
	$ret = array(''=>'Semua Data') + $data;
	return $ret;
}

function get_information($name="@#$"){
	$ci = & get_instance();
	$ci->load->model('sistem/sys_information');
	$ret = $ci->sys_information->get_by_fields(array('information_name'=>$name));
	if(!empty($ret)){
		$ret = $ret[0]['information_data'];
	}
	return $ret;
}

function get_information_sekolah($name="@#$"){
	$ci = & get_instance();
	$ci->load->model('sistem/sys_information');
	$ret = $ci->sys_information->get_by_fields(array('information_name'=>$name,'id_sekolah'=>get_id_sekolah()));
	if(!empty($ret)){
		$ret = array('information_data'=>$ret[0]['information_data'],'information_label'=>$ret[0]['information_label'],'information_type'=>$ret[0]['information_type']);
	}
	return $ret;
}

function get_sekolah_by_name($nama=""){
	$ci = & get_instance();
	$ci->load->model('sekolah/m_sekolah');
	$datas = $ci->m_sekolah->get_by_field("nama_strip",$nama);
	if(count($datas)==0){
		return 0;
	}else{
		return $datas[0];
	}
}