<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
function is_allow($controll='.') {
	$gid = get_usergroup();
	$ret = true;
    return $ret;
}

function set_success_message($msg){
	$ci = & get_instance();
	$ci->session->set_userdata('success',$msg);
}

function set_error_message($msg){
	$ci = & get_instance();
	$ci->session->set_userdata('error',$msg);
}

function set_warning_message($msg){
	$ci = & get_instance();
	$ci->session->set_userdata('warning',$msg);
}

function get_success_message(){
	$ci = & get_instance();
	$ret = $ci->session->userdata('success');
	$ci->session->unset_userdata('success');
	return $ret;
}

function get_error_message(){
	$ci = & get_instance();
	$ret = $ci->session->userdata('error');
	$ci->session->unset_userdata('error');
	return $ret;
}

function get_warning_message(){
	$ci = & get_instance();
	$ret=$ci->session->userdata('warning');
	$ci->session->unset_userdata('warning');
	return $ret;
}

function is_logged_in(){
    $ci = & get_instance();
	$ret=$ci->session->userdata('is_logged_in');
    return $ret;
}

function get_username(){
    $ci = & get_instance();
	$ret=$ci->session->userdata('username');
    return $ret;
}

function get_userid(){
    $ci = & get_instance();
	$ret=$ci->session->userdata('id_user');
    return $ret;
}

function get_usergroup(){
    $ci = & get_instance();
	$ret=$ci->session->userdata('id_group');
    return $ret;
}

function get_id_personal(){
	$CI =& get_instance();
	$per = $CI->session->userdata("id_personal");
	return $per;
}
