<?php

(defined('BASEPATH')) OR exit('No direct script access allowed');

class MY_Model extends CI_Model {

    public $table = "table";
	//public $table = "m_posko";
    public $id = "id";
	//public $id = "id_posko";
    public $use_code_numbering = false;
    public $order_fields = array();
	//public $order_fields = array("id_posko");
    public $new_inserted_id=0;
	public $join_to = array();
	//public $join_to = array("m_cabor"=>"id_cabor","m_kejuaraan"=>"id_kejuaraan");
	public $join_fields = array();
	//public $join_fields = array("m_cabor.nama cabor","m_kejuaraan.nama_kejuaraan kejuaraan");
	public $group_by = array();
	public $select = array();
	public $mydb;
	public $db_setting = "";
	
	public function __construct(){
		if(!empty($this->db_setting)){
			$this->mydb = $this->load->database($this->db_setting, TRUE);
		}else{
			$this->mydb = $this->db;
		}
	}
	
	//Function for saving new data
    public function save($data) {
        if($this->use_code_numbering){
            $data[$this->id] = $this->_get_code_numbering();
        }
        $this->mydb->insert($this->table, $data);
        return $this->new_inserted_id = $this->mydb->insert_id();
    }

    public function new_id() {
        return $this->new_inserted_id;
    }

    public function update($data) {
        $id = $data[$this->id];
        $this->mydb->where($this->id, $id);
        $this->mydb->update($this->table, $data);
    }

    public function delete($id) {
        $this->mydb->delete($this->table, array($this->id => $id));
    }

    public function get_all() {
        $order = implode(",",$this->order_fields);
        $order = empty($order)?"":" order by ".$order;
        $query = "select * from $this->table $order";
        return $this->mydb->query($query)->result_array();
    }

    function get($id=0) {
        return $query = $this->mydb->get_where($this->table, array($this->id => $id))->row_array();
    }

    public function get_by_field($field='kategori', $filter='1') {
        $query = "select * from $this->table where $field='$filter'";
        return $this->mydb->query($query)->result_array();
    }

    public function get_by_fields($kriteria=array(), $order=array()) {
        foreach($this->order_fields as $det){
            $this->mydb->order_by($det);
        }
        return $this->mydb->get_where($this->table, $kriteria)->result_array();
    }
    
    function get_pagination($limit = array(), $filter = array()) {
        $order = implode(",",$this->order_fields);
        $order = empty($order)?"":" order by ".$order;
        
		$strLimit = "";
        if ($limit != NULL) {     
            $strLimit = " LIMIT ".$limit['offset'].",".$limit['perpage'];
		}
		
		$strFilter = "";		
		foreach($filter as $k => $v){
			$strLike = 'lcase('.$k.')'." LIKE '%". strtolower($v) ."%'";
            $strFilter = $strFilter==""?$strLike: $strFilter." OR ".$strLike;
		}
		$strFilter = $strFilter==""?" WHERE 1=1":" WHERE ".$strFilter;
        $strQuery = "SELECT * FROM $this->table ".$strFilter." ";
		$strQuery = $strQuery.$order.$strLimit;
        //echo $strQuery;
		$query = $this->mydb->query($strQuery);
		//print_r($strQuery);
		return $query->result_array();
	}

    public function _get_code_numbering($prefix=null, $sufix=null) {
        $this->mydb->where("table_name", $this->table);
        $data = $this->mydb->get("code_numbering")->row_array();
        $code = "";
        
        if (!empty($data)) {
            if ($prefix != null) {
                $data['prefix'] = $prefix;
            }
            if ($sufix != null) {
                $data['sufix'] = $sufix;
            }
           
            $code = $data['current_state'];
            $code = "$code";             
            for ($i = strlen($code); $i < $data['length']; $i++) {
                $code = "0" . $code;
            }
            $code = $data['prefix'] . $code . $data['sufix'];
            
            $this->mydb->simple_query("update code_numbering set current_state=current_state+1 where table_name='$this->table'");
        }
        return $code;
    }
    
    public function save_batch($data=array()){
        if(count($data)>0){
            $this->mydb->insert_batch($this->table,$data);
        }
    }
    
    public function update_batch($data=array()){
        if(count($data)>0){
            $this->mydb->update_batch($this->table,$data,$this->id);
        }
    }
    
    public function get_by_combination_field($fields=array(),$filter=''){
        $comb_field = implode(",'@',",$fields);
        $query = "select * from $this->table where concat($comb_field)='$filter'";
        return $this->mydb->query($query)->result_array();
    }
    	
	//up to date
	function get_for_pagination($limit = array(), $filter = array(), $operator=array(), $order=array(), $operatorColumn = array()) {
		$order = is_array($order)?$order:array($order);
        $strOrder = implode(",",$order);
        $order = empty($strOrder)?"":" order by ".$strOrder;
        
		$strLimit = "";
        if ($limit != NULL) {     
			$limit['offset'] = $limit['offset']<0?0:$limit['offset'];
            $strLimit = " LIMIT ".$limit['offset'].",".$limit['perpage'];
		}
		
		$strFilter = "";		
		$andFilter = "";
		$orFilter = "";
		foreach($filter as $k => $v){
			$strLike = 'lcase('.$k.')';
			$strKunci = strtolower($v);
			
			if(isset($operatorColumn[$k])){
				$strKunci = $operatorColumn[$k]=='LIKE' || $operatorColumn[$k]=='NOT LIKE'?" ".$operatorColumn[$k]." '%$strKunci%' ":" ".$operatorColumn[$k]." '$strKunci' ";
			}else{
				$strKunci = " LIKE '%$strKunci%' ";
			}
			
			$strOperator = "OR";
			if(isset($operator[$k])){
				$strOperator = $operator[$k];
			}
			$strLike = $strLike.' '.$strKunci;
			if($strOperator=='OR'){
				$orFilter = $orFilter." $strOperator ".$strLike;
			}else{
				$andFilter = $andFilter." $strOperator ".$strLike;
			}
		}
		$orFilter = empty($orFilter)?"":" AND (1=0 $orFilter)";
		$strFilter = $andFilter." ".$orFilter;
		
		$strFilter = " WHERE 1=1 ".$strFilter;
		
		$str_join = "";
		foreach($this->join_to as $tbl=>$key){
			$str_join .= " JOIN $tbl ON $key ";
		}
		
		$group_by = implode(",", $this->group_by);
		$group_by = empty($group_by)?"":" GROUP BY ".$group_by;
		
		$select = implode(",", $this->select);
		$select = empty($select)?"$this->table.*":$select;
		
		$select_alias = implode(",",$this->join_fields);
		$select_alias = empty($select_alias)?"":",".$select_alias;
		
        $strQuery = "SELECT $select $select_alias FROM $this->table ".$str_join.$strFilter." ".$group_by;
		$strQuery = $strQuery.$order.$strLimit;
		
		//echo $strQuery;
		$query = $this->mydb->query($strQuery);
		return $query->result_array();
	}
	
	public function is_duplication($item=array(), $field=array(), $field_comb=array()){
		$where = "";
		if(isset($item[$this->id]) && $item[$this->id]>0){
			//Update
			$where = " AND ".$this->id." <> ".$item[$this->id];
		}
		//Check unique field
		$flag = FALSE;
		foreach($field as $f){
			$query = "select * from $this->table where $f='".$item[$f]."'".$where;
			$res = $this->mydb->query($query)->result_array();
			$state = count($res)>0;
			$flag = $flag || $state;			
		}
		//Check unique combination-field 
		$item_comb = array();
		foreach($field_comb as $f){
			$item_comb[] = isset($item[$f])?$item[$f]:"";
		}
		if(count($field_comb)>0){
			$fm = "'".implode("@",$item_comb)."'";
			$ff = "concat(".implode(",'@',",$field_comb).")";
			$query = "select * from $this->table where $ff=$fm";
			$res = $this->mydb->query($query)->result_array();
			$state = count($res)>0;
			$flag = $flag || $state;
		}
		return $flag;
	}
		
}