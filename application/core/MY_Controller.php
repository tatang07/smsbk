<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class MY_Controller extends CI_Controller {
	
	/* Construct For Log Data*/
	function __construct() {
        parent::__construct();
		$data_log['log_url'] = base_url(uri_string());
		$data_log['log_date'] =date('Y-m-d H:i:s');
		$data_log['log_type'] = $_SERVER['REQUEST_METHOD'];
		$data_log['id_sekolah'] = get_id_sekolah();
		$data_log['id_group'] = get_usergroup();
		
// echo '<pre>';
// print_r($this->session->all_userdata());
// echo '</pre>';exit();
		$this->set_log($data_log);
    }
	
	public function index(){
		
	}
	
	public function set_log($data){
		/* Log Data */
		$this->load->model('simple_model');
		$this->simple_model->set_log($data);
	}
}

/**
*Simple_Controller
*/
class Simple_Controller extends MY_Controller {
	public $conf = array(
			//Nama controller
			"name" 			=> "simple",
			//Jika di masing-masing tampilan didefinisikan title dan subtitile, maka title
			//dan subtitile general ini akan ter-replace
			"title"			=> "simple",
			"subtitle"		=> "simple",
			"view_file_index"=> "simple/index",
			//Nama model general, artinya jika model untuk masing-masing proses datalist/add/edit/delete 
			//tidak ditentukan, maka akan digunakan model ini
			//Urutannya jika didefinisikan table, maka akan diambil dati tabel, jika tidak, diambil dari model_name
			"table"			=> array(),
			"model_name"	=> "",
			//Penamaan/pelabelan untuk masing-masing field yang digunakan di datalist,add,edit,delete,export
			//Format: nama_field=>"Labelnya"
			"data_label"	=> array(								
							),
			//Pendefinisian tipe data, agar tampilan di datalist maupun input disesuaikan dengan tipenya
			//Tipe default jika tidak didefinisikan: String, 
			//pilihan tipe lainnya: date, textarea, number, dropdown (berupa array), popup
			//Format: nama_field => tipe_data_nya
			"data_type"		=> array(
							),
			//Field-field yang harus diisi
			"data_mandatory"=> array(
							),
			//Field-field yang membuat data per row unik, akan digunakan untuk pengecekan duplikasi
			"data_unique"	=> array(
							),
			"data_unique_combination"	=> array(
							),
			//Konfigurasi tampilan datalist
			"data_list"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"model_name"			=> "",
								"table"					=> array(),
								"view_file"				=> "simple/datalist",
								"view_file_datatable"	=> "simple/datatable",
								"view_file_datatable_action"	=> "simple/datatable_action",
								"field_list"			=> array(),
								"field_sort"			=> array(),
								"field_filter"			=> array(),
								"field_filter_hidden"	=> array(),
								"field_filter_dropdown"	=> array(),
								"field_comparator"		=> array(),
								"field_operator"		=> array(),
								"field_align"			=> array(),
								"field_size"			=> array(),
								"field_separated"		=> array(),
								"field_sum"				=> array(),
								"field_summary"			=> array(),
								"enable_action"			=> TRUE,
								"custom_action_link"	=> array(),
								"custom_add_link"		=> array(),
								"custom_edit_link"		=> array(),
								"custom_delete_link"	=> array(),
								"custom_link"			=> array(),
							),
			//Konfigurasi tampilan add				
			"data_add"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"view_file"				=> "simple/add",
								"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array(),
								"custom_action"			=> "",
								"msg_on_duplication"	=> "Maaf data ini sudah tersedia di sistem.",
								"msg_on_success"		=> "Data berhasil disimpan.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan edit
			"data_edit"		=> array(
								//"title"			=> "simple",
								//"subtitle"		=> "simple",
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"view_file"				=> "simple/edit",
								"view_file_form_input"	=> "simple/form_input",
								"field_list"			=> array(),
								"custom_action"			=> "",
								"msg_on_duplication"	=> "Maaf data baru yang akan disimpan sudah tersedia di sistem.",
								"msg_on_success"		=> "Data berhasil diperbaharui.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan delete
			"data_delete"	=> array(
								"enable"				=> TRUE,
								"model_name"			=> "",
								"table"					=> array(),
								"msg_confirmation"		=> "Apakah Anda yakin akan menghapus data ini?",							
								"msg_on_success"		=> "Data berhasil dihapus.",
								"redirect_link"			=> ""
							),
			//Konfigurasi tampilan export
			"data_export"	=> array(
								"enable_pdf"			=> TRUE,
								"enable_xls"			=> TRUE,
								"view_pdf"				=> "simple/laporan_datalist",
								"view_xls"				=> "simple/laporan_datalist",
								"field_size"			=> array(),
								"orientation"			=> "p",
								"special_number"		=> "",
								"enable_header"			=> TRUE,
								"enable_footer"			=> TRUE,
							)
		);
	
	public $myconf = array();
	
	public function get_myconf(){
		//Menyatukan default config dan user defined config
		$ret = $this->myconf + $this->conf;
		//Mngecek sub komponen config
		$comps = array("data_list","data_add","data_edit","data_delete","data_export");
		foreach($comps as $det){
			$comp = isset($this->myconf[$det])?$this->myconf[$det]:array();
			if(!is_array($comp)){
				$comp = array();
			}
			$comp = $comp + $this->conf[$det];
			$ret[$det] = $comp;
		}
		
		//MEngambil ID name dari masing-masing model
		if(!empty($ret["table"])){
				$model = $this->_get_model_from_table($ret["table"]);
				$ret['id_model'] = $model->id;
		}else if(!empty($ret["model_name"])){
				$model = $this->load->model($ret["model_name"]);
				$ret['id_model'] = $model->id;
		}else{
				$ret['id_model'] = "";
		}
			
		foreach($comps as $det){
			if(!empty($ret[$det]["table"])){
				$model = $this->_get_model_from_table($ret[$det]["table"]);
				$ret[$det]['id_model'] = $model->id;
			}else if(!empty($ret[$det]["model_name"])){
				$model = $this->load->model($ret[$det]["model_name"]);
				$ret[$det]['id_model'] = $model->id;
			}else{
				$ret[$det]['id_model'] = $ret['id_model'];
			}
		}
		
		//Mengisi nilai-nilai yang kosong
		$list_conf = array("title","subtitle","table","model_name");
		foreach($comps as $det){
			foreach($list_conf as $fconf){
				$ret[$det][$fconf] = isset($ret[$det][$fconf])?$ret[$det][$fconf]:$ret[$fconf];
				$ret[$det][$fconf] = !empty($ret[$det][$fconf])?$ret[$det][$fconf]:$ret[$fconf];
			}
		}
				
		$ret['data_edit']['field_list'] = empty($ret['data_edit']['field_list'])?$ret['data_add']['field_list']:$ret['data_edit']['field_list'];
		
		$redcomp = array("data_add","data_edit","data_delete");
		foreach($redcomp as $det){
			$ret[$det]["redirect_link"] = empty($ret[$det]["redirect_link"])?$ret["name"]."/datalist/":$ret[$det]["redirect_link"];
		}
		
		if(empty($ret['data_unique'])){
			$ret['data_unique'] = array($ret['id_model']);
		}
		return $ret;
	}
	
	//Fungsi yang menampilkan halaman index modul
    public function index(){	
		//Mengambil data yang diposting
		$data = $this->input->post();
		//Mengambil Konfigurasi
		$conf = $this->get_myconf();
		$data['conf'] = $conf;	
		//Mengeksekusi hook user-defined pre_process			
		$data = $this->pre_process($data);		
		//Mengeksekusi hook user-defined khusus before_index
		$data = $this->before_index($data);
				
		//Mengeksekusi hook user-defined before_render_index
		$data = $this->before_render_index($data);
		$conf = $data['conf'];
		render($conf["view_file_index"], $data);
		//Mengeksekusi hook user-defined khusus after_index
		$this->after_index($data);
		//Mengeksekusi hook user-defined post_process
		$this->post_process($data);
    }
    
	//Fungsi yang melayani pen-generate-an data sesuai filter baik untuk pemanggilan
	//dari method lain maupun pemanggilan menggunakan ajax
    public function datatable($page=1,$return=false){
		//Mengambil data yang diposting
		$data = $this->input->post();
		//Mengambil Konfigurasi
		$conf = $this->get_myconf();
		$data['conf'] = $conf;	
		$data['page'] = $page;
		//Mengeksekusi hook user-defined pre_process
		$data = $this->pre_process($data);
		//Mengeksekusi hook user-defined khusus before_datatable
		$data = $this->before_datatable($data);		
		//Melakukan loading model sesuai konfigurasi
		$conf = $data['conf'];
		if(!empty($conf["data_list"]["table"])){
			//Loading model ketika konfig table diberikan
			$model = $this->_get_model_from_table($conf["data_list"]["table"]);
		}else{
			//Loading model ketika model_name diberikan
			$model = $this->load->model($conf["data_list"]["model_name"]);
		}
		//Mengambil filter yang di post dari tampilan datalist
		//$filter = isset($conf['global']['filter'])?$conf['global']['filter']:array();
		$filter = array();
		$post_filter = $this->input->post("filter");
		if($post_filter){
			foreach($post_filter as $key=>$val){
				if(!empty($val)){
					$key = str_replace("__",".",$key);
					$filter[$key] = $val;
				}
			}
		}
		//Mengambil keyword pencarian umum jika ada        
        $keywords = isset($filter['keywords'])?$filter['keywords']:"";
		unset($filter["keywords"]);
		//Membuat filter pencarian untuk [data_list][field_filter] dengan keywords yang didapat
		$data_filter = $conf["data_list"]["field_filter"];
        foreach($data_filter as $det){
			$filter[$det] = $keywords;
		}
		
		//Mengambil konfigurasi operator dan komparator untuk pencarian/filtering
		$operator = $conf["data_list"]["field_operator"];
		$comparator = $conf["data_list"]["field_comparator"];
		
		//Mensetting filter untuk filter yang berupa dropdown di tampilan datalist (jika ada)
		foreach($conf['data_list']['field_filter_dropdown'] as $key=>$val){
			$comparator[$key] = "=";
			$operator[$key] = "AND";
		}
		
		//Mensetting pengurutan/order sesuai dengan input sorting
		$order_by = $model->order_fields; //mengambil setting model jika ada
		$posted_sort_field = $this->input->post("sorting");
		if($posted_sort_field){
			$order_by = $posted_sort_field;
		}
		
		//Setting limit sesuai halaman yang dipilih
		$page = $data['page'];
        $offset = ($page-1)*PER_PAGE;
        $perpage = PER_PAGE;
		
		//Mensetting session halaman, agar ketika di back ke halaman sebelumnya, tetap  menampilkan page yang sedang aktif
		$page = cek_page($conf['name'].'.datatable',$page,true);
		
		//Mengambil seluruh data yang sesuai dengan kriteria dari DB
		$alldata = $model->get_for_pagination(null, $filter, $operator, $order_by, $comparator);
		//Menghitung jml data yang didapat
        $count = count($alldata);
		//Mengambil data yang hanya ditampilkan pada halaman yang sedang diakses saja
        $list = $model->get_for_pagination(array('perpage' => $perpage, 'offset' => $offset), $filter, $operator, $order_by, $comparator);
        //Memformat data utk ditampilkan di table sesuai dengan list, jml data, dan halamannya
		$data_list = populate_pagination($list, $count, $page);
        
		//Mempersiapkan data yang akan dikirim ke view
		$data['data_list']=$data_list;
        $data['list']=$list;
        $data['offset']=$offset;
        $data['filter']=$filter;
        $data['conf'] = $conf;
		$data['alldata'] = $alldata;
		$data['filter']['keywords'] = $keywords;
		$data['page'] = $page;
				
        if($return){
			//Jika flag return di set TRUE, maka method ini akan me-return data ke pemanggilnya
			//Mengeksekusi hook user-defined after_datatable 
			$this->after_datatable($data);
            return $data;
        }else{
			//Jika flag return di set FALSE, maka method ini akan merender view sesuai konfigurasi
			//Mengeksekusi hook user-defined before_render_datatable
			$data = $this->before_render_datatable($data);
			$conf = $data['conf'];
            $this->load->view($conf["data_list"]["view_file_datatable"], $data);
			//Mengeksekusi hook user-defined after_datatable
			$this->after_datatable($data);
			//Mengeksekusi hook user-defined after_process
			$this->post_process($data);
        }
    }
	
	//Fungsi yang menampilkan halaman datalist
    public function datalist($page=1){
		//Mengambil data yang diposting
		$data = $this->input->post();
		//Mengambil Konfigurasi
		$conf = $this->get_myconf();
		$data['conf'] = $conf;		
		$data['page'] = $page;		
		//Mengeksekusi hook function user-defined
		$data = $this->pre_process($data);
		$data2 = $this->before_datalist($data);
		//Mempersiapkan data
		$page = $data2['page'];

		$data = $this->datatable($page,true);
		$data = $data2 + $data;
		//Mengeksekusi hook function user-defined
		$data = $this->before_render_datalist($data);
		$conf = $data['conf'];
		render($conf["data_list"]["view_file"],$data);
		//Mengeksekusi hook function user-defined
		$this->after_datalist($data);
		$this->post_process($data);
    }
    
	public function mydebug($data){
		echo "<pre>";
		print_r($data);
		echo "</pre>";
	}
	
	//Fungsi untuk menampilkan form input dan melakukan input data
    public function add(){		
		//Mengambil data yang diposting
		$data = $this->input->post();
		//Mengambil Konfigurasi
		$conf = $this->get_myconf();
		$conf['state'] = 'add';
		
		$data['conf'] = $conf;
		
		//Mengeksekusi hook function user-defined
		$data = $this->pre_process($data);		
		$data = $this->before_add($data);
		$conf = $data['conf'];
		//Melakukan loading model sesuai konfigurasi
		if(!empty($conf["data_add"]["table"])){
			//Loading model ketika konfig table diberikan
			$model = $this->_get_model_from_table($conf["data_add"]["table"]);
		}else{
			//Loading model ketika model_name diberikan
			$model = $this->load->model($conf["data_add"]["model_name"]);
		}
		
		if($this->input->post()){			
			//Jika ada data yang di-posting
			//Sesuaikan data dengan tipe data khusus yang telah didefinisikan di config
			$data['item'] = $this->check_data_type($data['item'],"db");
			//Cek Duplikasi
			if($model->is_duplication($data['item'], $conf['data_unique'], $conf['data_unique_combination'])){
				//Jika data duplikasi
				set_error_message($conf['data_add']['msg_on_duplication']);
			}else{
				//Data tidak terduplikasi
				$model->save($data['item']);
				set_success_message($conf['data_add']['msg_on_success']);
				//Cek halaman data ketika sebelum dilakukan input
				$page = cek_page($conf['name'].'.datatable');
				//Mengeksekusi hook function user-defined
				$this->after_add($data);
				$this->post_process($data);
				//Redirect ke halaman sesuai konfig
				redirect($conf['data_add']["redirect_link"].'/'.$page);
			}
		}
		
		//Jika tidak ada data yang di-posting
		//Mengeksekusi hook function user-defined
		$data = $this->before_render_add($data);
		$conf = $data['conf'];
		render($conf["data_add"]["view_file"], $data);
		//Mengeksekusi hook function user-defined
		$this->after_add($data);
		$this->post_process($data);
    }
    
	//Fungsi untuk menampilkan form edit dan memperbaharui data
    public function edit($id=0) {
		//Mengambil data yang diposting
		$data = $this->input->post();
		//Mengambil Konfigurasi
		$conf = $this->get_myconf();
		$conf['state'] = 'edit';
		$data['conf'] = $conf;
		$data['id'] = $id;
		//Mengeksekusi hook function user-defined
		$data = $this->pre_process($data);		
		$data = $this->before_edit($data);
		$conf = $data['conf'];		
        //Melakukan loading model sesuai konfigurasi
		if(!empty($conf["data_edit"]["table"])){
			//Loading model ketika konfig table diberikan
			$model = $this->_get_model_from_table($conf["data_edit"]["table"]);
		}else{
			//Loading model ketika model_name diberikan
			$model = $this->load->model($conf["data_edit"]["model_name"]);
		}
		$id = $data['id'];
        if (!$this->input->post()) {
			
            $data['item'] = $model->get($id);
            $data['item'] = $this->check_data_type($data['item'],"view");
			//Dengan memasukan data ke var $_POST sesuai dengan name di input
			//Maka data akan otomatis dirender ke dalam form
            $_POST['item'] = $data['item'];
			
        } else {
            $item = $data['item']; //$data$this->input->post('item');
			$item = $this->check_data_type($item,"db");
			//Cek Duplikasi
			if($model->is_duplication($item, $conf['data_unique'],$conf['data_unique_combination'])){
				//Jika data duplikasi
				set_error_message($conf['data_edit']['msg_on_duplication']);
			}else{
				$model->update($item);
				set_success_message($conf["data_edit"]["msg_on_success"]);
				//Mengambil sesi page yang sedang aktif ketika ditinggalkan menuju form edit
				$page = cek_page($conf['name'].'.datatable');
				//Mengeksekusi hook function user-defined
				$this->after_edit($data);
				$this->post_process($data);
				redirect($conf["data_edit"]["redirect_link"].'/'.$page);
			}
        }
		
		//Mengeksekusi hook function user-defined
		$data = $this->before_render_edit($data);
		$conf = $data['conf'];
        render($conf["data_edit"]["view_file"], $data);
		//Mengeksekusi hook function user-defined
		$this->after_edit($data);
		$this->post_process($data);
    }
    
    public function delete($id=0) {
		//Mengambil data yang diposting
		$data = $this->input->post();
		//Mengambil Konfigurasi
		$conf = $this->get_myconf();
		$data['conf'] = $conf;
		$data['id'] = $id;
		//Mengeksekusi hook function user-defined		
		$data = $this->pre_process($data);
		$data = $this->before_delete($data);
		$conf = $data['conf'];
        //Melakukan loading model sesuai konfigurasi
		if(!empty($conf["data_edit"]["table"])){
			//Loading model ketika konfig table diberikan
			$model = $this->_get_model_from_table($conf["data_delete"]["table"]);
		}else{
			//Loading model ketika model_name diberikan
			$model = $this->load->model($conf["data_delete"]["model_name"]);
		}
		
		$id = $data['id'];
        $model->delete($id);		
        set_success_message($conf["data_delete"]["msg_on_success"]);
		$page = cek_page($conf['name'].'.datatable');
		//Mengeksekusi hook function user-defined
		$this->after_delete($data);
		$this->post_process($data);
        redirect($conf["data_delete"]["redirect_link"].'/'.$page);
    }
	
	public function check_data_type($data=array(), $state="view"){
		$conf = $this->get_myconf();
		foreach($conf["data_type"] as $field=>$type){
			switch($type){
				case "date":
					if(isset($data[$field])){
						switch($state){
							case "view":
								$data[$field] = tanggal_view($data[$field]);
								break;
							case "db":
								$data[$field] = tanggal_db($data[$field]);
								break;
						}
					}
					break;
			}
		}
		return $data;
	}
	
	public function excel_datalist(){
		$data = $this->datatable(1,true);
		//Mengeksekusi hook function user-defined
		$data = $this->pre_process($data);
		$data = $this->before_excel($data);		
		$conf = $data['conf'];				
		$data['output'] = $conf["name"].".xls";

		//Mengeksekusi hook function user-defined
		$data = $this->before_render_excel($data);
		$conf = $data['conf'];
		render($conf["data_export"]["view_xls"],$data, "excel");
		//Mengeksekusi hook function user-defined
		$this->after_excel($data);
		$this->post_process($data);
    }
    
    public function pdf_datalist(){
		$data = $this->datatable(1,true);
		//Mengeksekusi hook function user-defined
		$data = $this->pre_process($data);
		$data = $this->before_pdf($data);		
		$conf = $data['conf'];		
		
		$fname = $conf["name"].".pdf";
		//Membuat PDF		
		ob_start();
		$data = $this->before_render_pdf($data);
		$conf = $data['conf'];
		render($conf["data_export"]["view_pdf"],$data, "cetak");
		$content = ob_get_clean();
        print_pdf($content,"letter",$conf["data_export"]["orientation"],$fname,"@");	
		$this->after_pdf($data);
		$this->post_process($data);
    }
	
	public function _get_model_from_table($table=array()){
		$name = isset($table['name'])?$table['name']:"";
		$pk = isset($table['id'])?$table['id']:"";
		
		$model = $this->load->model("simple_model");
		$model->table = $name;
		$model->id = $pk;
		return $model;
	}
		
	public function pre_process($data=array()){return $data;}
	public function before_add($data=array()){return $data;}
	public function before_edit($data=array()){return $data;}
	public function before_delete($data=array()){return $data;}
	public function before_datatable($data=array()){return $data;}
	public function before_datalist($data=array()){return $data;}
	public function before_excel($data=array()){return $data;}
	public function before_pdf($data=array()){return $data;}
	public function before_index($data=array()){return $data;}
	public function before_render_add($data=array()){return $data;}
	public function before_render_edit($data=array()){return $data;}
	public function before_render_delete($data=array()){return $data;}
	public function before_render_datatable($data=array()){return $data;}
	public function before_render_datalist($data=array()){return $data;}
	public function before_render_excel($data=array()){return $data;}
	public function before_render_pdf($data=array()){return $data;}
	public function before_render_index($data=array()){return $data;}
	
	
	public function post_process(){}
	public function after_add(){}
	public function after_edit(){}
	public function after_delete(){}
	public function after_datatable(){}
	public function after_datalist(){}
	public function after_excel(){}
	public function after_pdf(){}
	public function after_index(){}
}
