					<div class="<?php echo $class_div?>">
						<label class="<?php echo $class_label?>" for="<?php echo $name ?>">
							<strong><?php echo $subject ?></strong>
						</label>
						<div>
							<textarea  <?php echo $attributes." ".$extra ?> ><?php echo $value ?></textarea>
						</div>
					</div>