<div class="<?php echo $class_div?>">
	<label class="<?php echo $class_label?>" for="<?php echo $name ?>">
		<strong><?php echo $subject ?></strong>
	</label>
	<div>
		<select multiple class="dualselects" data-size=small id="<?php echo $name ?>" name="<?php echo $name ?>" <?php echo $extra?>>
			<?php foreach ($options as $key => $val):?>
				<?php $key = (string) $key; ?>
				<?php if (is_array($val)):?>
					<optgroup label="<?php echo $key?>">
						<?php foreach ($val as $optgroup_key => $optgroup_val):?>
							<?php $sel = (in_array($optgroup_val, $selected)) ? ' selected="selected"' : '';?>
							<option value="<?php echo $optgroup_key?>" name="<?php echo (string) $optgroup_val?>" <?php echo $sel?> > <?php echo (string) $optgroup_val?></option>
						<?php endforeach ?>
					</optgroup>
				<?php else:?>
					<?php $sel = (in_array($val, $selected)) ? ' selected="selected"' : '';?>
					<option value="<?php echo $key ?>" name=" <?php echo (string) $val ?>" <?php echo $sel ?> > <?php echo (string) $val ?></option>
				<?php endif ?>
			<?php endforeach ?>
		</select>
	</div>
</div>






