								<?php 
									/* Contoh param untuk load template ini
									$param['parent'] = $rombel;	// array: id=>nilai							
									$param['child'] = $pelajaran;	//array: id_parent => array:id_child=>nilai							
									$param['parent_name'] = "rombel";														
									$param['current_parent'] = $id_rombel;								
									$param['current_child'] = $id_gmr;								
									$param['child_extra'] = "id='xxx'";	*/							
								?>
								
								<?php $extra = isset($child_extra)?$child_extra:""?>
								<?php foreach($parent as $id=>$det):?>
									<?php $lchild =  array("0"=>"-Pilih-") ?>
									<?php $lchild +=  isset($child[$id])?$child[$id]:array() ?>
									<div id='<?php echo $parent_name.$id?>' <?php echo $current_parent!=$id?"style='display:none'":""?> class='<?php echo "list".$parent_name?>'>					
										<?php echo simple_form_dropdown_clear($name.$id,$lchild,$current_child,$extra,null) ?>
									</div>	
								<?php endforeach ?>
								
								<script type="text/javascript">		
									function <?php echo $parent_name?>Changed(id){
										$('.list<?php echo $parent_name?>').hide();
										if(id>0){
											$('#<?php echo $parent_name?>'+id).show();				
										}
									}
								</script>