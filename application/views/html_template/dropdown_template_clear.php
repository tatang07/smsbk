					
							<select id="<?php echo $name ?>" name="<?php echo $name ?>" <?php echo $extra?> >
								<?php foreach ($options as $key => $val):?>
									<?php $key = (string) $key; ?>
									<?php if (is_array($val)):?>
										<optgroup label="<?php echo $key?>">
											<?php foreach ($val as $optgroup_key => $optgroup_val):?>
												<?php $sel = (in_array($optgroup_key, $selected)) ? ' selected="selected"' : '';?>
												<option value="<?php echo $optgroup_key?>" name="<?php echo (string) $optgroup_val?>" <?php echo $sel?> > <?php echo (string) $optgroup_val?></option>
											<?php endforeach ?>
										</optgroup>
									<?php else:?>
										<?php $sel = (in_array($key, $selected)) ? ' selected="selected"' : '';?>
										<option value="<?php echo $key ?>" name=" <?php echo (string) $val ?>" <?php echo $sel ?> > <?php echo (string) $val ?></option>
									<?php endif ?>
								<?php endforeach ?>
							</select>
						