<?php if(!is_logged_in()){redirect('sistem/login');}?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<?php /*
	<!--link rel="dns-prefetch" href="http://fonts.googleapis.com" /-->
	<!--link rel="dns-prefetch" href="http://themes.googleusercontent.com" /-->
	
	<!--link rel="dns-prefetch" href="http://ajax.googleapis.com" /-->
	<!--link rel="dns-prefetch" href="http://cdnjs.cloudflare.com" /-->
	<!--link rel="dns-prefetch" href="http://agorbatchev.typepad.com" /-->
	*/ ?>
	
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title><?php echo get_information("nama_sistem") ?> <?php echo isset($conf['title'])?" | ".$conf['title']:"" ?></title>
	<script type="text/javascript">
        // var myci_base_url = "<?php echo site_url()?>";
        var myci_base_url = "";
	</script>
	<!-- Mobile viewport optimized: h5bp.com/viewport -->
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<!-- iPhone: Don't render numbers as call links -->
	<meta name="format-detection" content="telephone=no">
	
	<link rel="shortcut icon" href="favicon.ico" />
	<!-- Place favicon.ico and apple-touch-icon.png in the root directory: mathiasbynens.be/notes/touch-icons -->
	<!-- More ideas for your <head> here: h5bp.com/d/head-Tips -->
	
	
	<!-- The Styles -->
	<!-- ---------- -->
	
	<!-- Layout Styles -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/style.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/mystyle.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/grid.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/layout.css">
	
	<!-- Icon Styles -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/icons.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/fonts/font-awesome.css">
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo site_url()?>theme/css/fonts/font-awesome-ie7.css"><![endif]-->
	
	<!-- External Styles -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery-ui-1.8.21.custom.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.chosen.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.cleditor.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.colorpicker.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.elfinder.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.fancybox.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.jgrowl.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.plupload.queue.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/syntaxhighlighter/shCore.css" />
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/syntaxhighlighter/shThemeDefault.css" />
	
	<!-- Elements -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/elements.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/forms.css">
	
	<!-- OPTIONAL: Print Stylesheet for Invoice -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/print-invoice.css">
	
	<!-- Typographics -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/typographics.css">
	
	<!-- Responsive Design -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/media-queries.css">
	
	<!-- Bad IE Styles -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/ie-fixes.css">
	<link href="<?php echo site_url()?>theme/bootstrap-tagsinput/bootstrap-tagsinput.css" rel="stylesheet" type="text/css" />

	<!-- The Scripts -->
	<!-- ----------- -->
	
	<!-- JavaScript at the top (will be cached by browser) -->
	
	<!-- Load Webfont loader -->
	<script type="text/javascript">
        var base_url_js = "<?php echo site_url()?>";
        var constant_per_page = "<?php echo PER_PAGE?>";
		window.WebFontConfig = {
			google: { families: [ 'PT Sans:400,700' ] },
			active: function(){ $(window).trigger('fontsloaded') }
		};
	</script>
	<script defer async src="<?php echo site_url()?>theme/js/libs/webfont.js"></script>
	
	<!-- Essential polyfills -->
	<script src="<?php echo site_url()?>theme/js/mylibs/polyfills/modernizr-2.6.1.min.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/polyfills/respond.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/polyfills/matchmedia.js"></script>
	<!--[if lt IE 9]><script src="<?php echo site_url()?>theme/js/mylibs/polyfills/selectivizr.js"></script><![endif]-->
	<!--[if lt IE 10]><script src="<?php echo site_url()?>theme/js/mylibs/polyfills/excanvas.js"></script><![endif]-->
	<!--[if lt IE 10]><script src="<?php echo site_url()?>theme/js/mylibs/polyfills/classlist.js"></script><![endif]-->
	
	
	
	<!-- Grab frameworks from CDNs -->
		<!-- Grab Google CDN's jQuery, with a protocol relative URL; fall back to local if offline -->
	<!--script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script-->
	<!--script>window.jQuery || document.write('<script src="<?php echo site_url()?>theme/js/libs/jquery-1.7.2.js"><\/script>')</script-->
	<script src="<?php echo site_url()?>theme/js/libs/jquery-1.7.2.js"></script>
	
		<!-- Do the same with jQuery UI -->
	<!--script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.21/jquery-ui.js"></script-->
	<!--script>window.jQuery.ui || document.write('<script src="<?php echo site_url()?>theme/js/libs/jquery-ui-1.8.21.js"><\/script>')</script-->
	<script src="<?php echo site_url()?>theme/js/libs/jquery-ui-1.8.21.js"></script>
	
		<!-- Do the same with Lo-Dash.js -->
	<!--[if gt IE 8]><!-->
	<!--script src="http://cdnjs.cloudflare.com/ajax/libs/lodash.theme/js/0.4.2/lodash.js"></script-->
	<!--script>window._ || document.write('<script src="<?php echo site_url()?>theme/js/libs/lo-dash.js"><\/script>')</script-->
	<script src="<?php echo site_url()?>theme/js/libs/lo-dash.js"></script>
	<!--<![endif]-->
	<!-- IE8 doesn't like lodash -->
	<!--[if lt IE 9]><script src="http://documentcloud.github.com/underscore/underscore.js"></script><![endif]-->
	
	
	
	<!-- scripts concatenated and minified via build script -->
	
	<!-- General Scripts -->
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.hashchange.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.idle-timer.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.plusplus.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.jgrowl.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.scrollTo.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.ui.touch-punch.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.ui.multiaccordion.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/number-functions.js"></script>
	
	<!-- Forms -->
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.autosize.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.checkbox.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.chosen.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.cleditor.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.colorpicker.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.ellipsis.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.fileinput.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.fullcalendar.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.maskedinput.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.mousewheel.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.placeholder.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.pwdmeter.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.ui.datetimepicker.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.ui.spinner.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.validate.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/uploader/plupload.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/uploader/plupload.html5.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/uploader/plupload.html4.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/uploader/plupload.flash.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/uploader/jquery.plupload.queue/jquery.plupload.queue.js"></script>
		
	<!-- Charts -->
	<script src="<?php echo site_url()?>theme/js/mylibs/charts/jquery.flot.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/charts/jquery.flot.orderBars.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/charts/jquery.flot.pie.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/charts/jquery.flot.resize.js"></script>
	
	<!-- Explorer -->
	<script src="<?php echo site_url()?>theme/js/mylibs/explorer/jquery.elfinder.js"></script>
	
	<!-- Fullstats -->
	<script src="<?php echo site_url()?>theme/js/mylibs/fullstats/jquery.css-transform.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/fullstats/jquery.animate-css-rotate-scale.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/fullstats/jquery.sparkline.js"></script>
	
	<!-- Syntax Highlighter -->
	<script src="<?php echo site_url()?>theme/js/mylibs/syntaxhighlighter/shCore.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/syntaxhighlighter/shAutoloader.js"></script>
	
	<!-- Dynamic Tables -->
	<script src="<?php echo site_url()?>theme/js/mylibs/dynamic-tables/jquery.dataTables.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/dynamic-tables/jquery.dataTables.tableTools.zeroClipboard.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/dynamic-tables/jquery.dataTables.tableTools.js"></script>
	
	<!-- Gallery -->
	<script src="<?php echo site_url()?>theme/js/mylibs/gallery/jquery.fancybox.js"></script>
	
	<!-- Tooltips -->
	<script src="<?php echo site_url()?>theme/js/mylibs/tooltips/jquery.tipsy.js"></script>
	
	<!-- Do not touch! -->
	<script src="<?php echo site_url()?>theme/js/mango.js"></script>
	<script src="<?php echo site_url()?>theme/js/plugins.js"></script>
	<script src="<?php echo site_url()?>theme/js/script.js"></script>
	
	<!-- Your custom JS goes here -->
	
	
    <script src="<?php echo site_url()?>theme/js/app.js"></script>
	<!-- end scripts -->

	<style>
		.label-info {
		  background-color: #659be0;
		}
		.bootstrap-tagsinput .tag {
		  margin-right: 2px;
		  color: white;
		}
	</style>
	
</head>
<body>
	<!-- Some dialogs etc. -->

	<!-- The loading box -->
	<!--div id="loading-overlay"></div>
	<div id="loading">
		<span>Loading...</span>
	</div-->
	<!-- End of loading box -->
	
	<!-- The settings dialog -->
    <?php $this->load->view('elements/setting') ?>
	<!-- End of settings dialog -->
	
	<!-- Add Client Example Dialog -->
	<div style="display: none;" id="dialog_add_client" title="Add Client Example Dialog">
		<form action="" class="full validate">
			<div class="row">
				<label for="d2_username">
					<strong>Username</strong>
				</label>
				<div>
					<input class="required" type=text name=d2_username id=d2_username />
				</div>
			</div>
			<div class="row">
				<label for="d2_email">
					<strong>Email Address</strong>
				</label>
				<div>
					<input class="required" type=text name=d2_email id=d2_email />
				</div>
			</div>
			<div class="row">
				<label for="d2_role">
					<strong>Role</strong>
				</label>
				<div>
					<select style="padding-bottom: 10px" name=d2_role id=d2_role class="search required" data-placeholder="Choose a Role">
						<option value=""></option>
						<option value="Applicant">Applicant</option> 
						<option value="Member">Member</option> 
						<option value="Moderator">Moderator</option> 
						<option value="Administrator">Administrator</option> 
					</select>
				</div>
			</div>
		</form>
		<div class="actions">
			<div class="left">
				<button class="grey cancel">Cancel</button>
			</div>
			<div class="right">
				<button class="submit">Add User</button>
			</div>
		</div>
	</div><!-- End if #dialog_add_client -->
	
	<script>
	$$.ready(function() {
		$( "#dialog_add_client" ).dialog({
			autoOpen: false,
			modal: true,
			width: 400,
			open: function(){ $(this).parent().css('overflow', 'visible'); $$.utils.forms.resize() }
		}).find('button.submit').click(function(){
			var $el = $(this).parents('.ui-dialog-content');
			if ($el.validate().form()) {
				$el.find('form')[0].reset();
				$el.dialog('close');
			}
		}).end().find('button.cancel').click(function(){
			var $el = $(this).parents('.ui-dialog-content');
			$el.find('form')[0].reset();
			$el.dialog('close');;
		});
		
		$( ".open-add-client-dialog" ).click(function() {
			$( "#dialog_add_client" ).dialog( "open" );
			return false;
		});
	});
	</script>
	<!--  End of Add Client Example Dialog -->
	
	<!-- Message Dialog -->
	<div style="display: none;" id="dialog_message" title="Conversation: John Doe">
		<div class="spacer"></div>
		<div class="messages full chat">
			
			<div class="msg reply">
				<img src="<?php echo site_url()?>theme/img/icons/packs/iconsweets2/25x25/user-2.png"/>
				<div class="content">
					<h3><a href="#">Super Admin</a> <span>pesan:</span><small>3 jam lalu</small></h3>
					<p>Tolong segera lakukan entry data</p>
				</div>
			</div>
			
			<div class="msg">
				<img src="<?php echo site_url()?>theme/img/icons/packs/iconsweets2/25x25/user-2.png"/>
				<div class="content">
					<h3><a href="#">Admin</a> <span>pesan:</span><small>5 jam lalu</small></h3>
					<p>Apakah sistem sudah ready?</p>
				</div>
			</div>
		
		</div><!-- End of .messages -->
		
		<div class="actions">
			<div class="left">
				<input style="width: 330px;" type="text" name=d3_message id=d3_message placeholder="Type your message..."/>
			</div>
			<div class="right">
				<button>Send</button>
				<button class="grey">Cancel</button>
			</div>
		</div><!-- End of .actions -->
		
	</div><!-- End of #dialog_message -->
	
	<script>
	$$.ready(function() {
		$( "#dialog_message" ).dialog({
			autoOpen: false,
			width: 500,
			modal: true
		}).find('button').click(function(){
			$(this).parents('.ui-dialog-content').dialog('close');
		});
		
		$( ".open-message-dialog" ).click(function() {
			$( "#dialog_message" ).dialog( "open" );
			return false;
		});
	});
	</script>
	<!-- End of Message Dialog -->
	
	<!--------------------------------->
	<!-- Now, the page itself begins -->
	<!--------------------------------->
	
	<!-- The toolbar at the top -->
	<section id="toolbar">
		<div class="container_12">
		
			<!-- Left side -->
			<div class="left">
				<ul class="breadcrumb">
					<li><a href="<?php echo site_url()?>">Home</a></li>
					<li><a href="<?php echo site_url("sistem/dasboard")?>">Dashboard</a></li>
				</ul>
			</div>
			<!-- End of .left -->
			
			<!-- Right side -->
			<?php $gid = get_usergroup() ?>
			<div class="right">
				<ul>
					<li><a href="#"><span class="icon i14_admin-user"></span><?php echo get_username()?></a></li>
					<?php if($gid>1):?>
						<li><a href="#"><span><?php echo get_nama_tahun_ajaran()?></span><?php echo get_nama_term()?></a></li>	
					<?php endif ?>
					<li><a href="#"><span><?php echo tanggal_indonesia(date('Y-m-d'))?></span><label id="sysclock">&nbsp;</label></a></li>				
					<li class="red"><a href="<?php echo site_url('sistem/logout') ?>">Logout</a></li>
					
				</ul>
			</div><!-- End of .right -->
			
			<!-- Phone only items -->
			<div class="phone">
				
				<!-- User Link -->
				<li><a href="pages_profile.html"><span class="icon icon-user"></span></a></li>
				<!-- Navigation -->
				<li><a class="navigation" href="#"><span class="icon icon-list"></span></a></li>
			
			</div><!-- End of phone items -->
			
		</div><!-- End of .container_12 -->
	</section><!-- End of #toolbar -->
	
	<!-- The header containing the logo -->
	<header class="container_12">
	
		<!-- Your logos -->
		<div class="clearfix" style="margin-bottom: -30px; position: inline">
		<?php 
			$id_jenjang = get_jenjang_sekolah();
			$banner = "banner3.jpg";
			switch($id_jenjang){
				case 2;
					$banner = "banner_sd.png";
					break;
				case 3;
					$banner = "banner_smp.png";
					break;
				case 4;
					$banner = "banner_sma.png";
					break;
			}
		?>
		<a href="<?php echo site_url()?>"><img src="<?php echo site_url()?>theme/img/<?php echo $banner?>" alt="logo" height="100%" width="100%"></a>
		<!--<a href="<?php echo site_url()?>"><img src="<?php echo site_url()?>theme/img/banner2.png" alt="logo" height="60"></a>
		<a class="phone-title" href="#"><img src="<?php echo site_url()?>theme/img/banner2.png" alt="logo" height="22" width="70" /></a>-->
		</div>
	</header><!-- End of header -->
	
	<!-- The container of the sidebar and content box -->
	<div role="main" id="main" class="container_12 clearfix">
	
		<!-- The blue toolbar stripe -->
		<section class="toolbar">
			<?php $this->load->view('elements/toolbar') ?>
		</section><!-- End of .toolbar-->
		
		<!-- The sidebar -->
		<aside>
			<div class="top">
				<?php $this->load->view('elements/menu') ?>		
			</div><!-- End of .top -->
			
			<div class="bottom sticky">
				
			</div><!-- End of .bottom -->
			
		</aside><!-- End of sidebar -->

		<!-- Here goes the content. -->
		<section id="content" class="container_12 clearfix" data-sort=true>
﻿			<?php $this->load->view('elements/notification')?>
﻿			<?php $this->load->view($view)?>
		</section><!-- End of #content -->
		
	</div><!-- End of #main -->
	
	<!-- The footer -->
	<footer class="container_12">
		<ul class="grid_6">
		</ul>
		
		<span class="grid_6">
			Copyright &copy; 2014 
		</span>
	</footer><!-- End of footer -->
	
	<!-- Spawn $$.loaded -->
	<script>
		$$.loaded();
	</script>
	<!--[if lt IE 7 ]>
	<script defer src="http://ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
	<script defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
	<![endif]-->
	<script src="<?php echo site_url()?>theme/bootstrap-tagsinput/bootstrap-tagsinput.min.js" type="text/javascript"></script>
</body>
</html>