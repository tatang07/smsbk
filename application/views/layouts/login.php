<?php if(is_logged_in()){redirect('sistem/index');}?>
<!doctype html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js lt-ie9" lang="en"> <![endif]-->

<!-- Consider adding a manifest.appcache: h5bp.com/d/Offline -->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="utf-8">
	<link rel="dns-prefetch" href="http://fonts.googleapis.com" />
	<link rel="dns-prefetch" href="http://themes.googleusercontent.com" />
	<link rel="dns-prefetch" href="http://ajax.googleapis.com" />
	<link rel="dns-prefetch" href="http://cdnjs.cloudflare.com" />
	<link rel="dns-prefetch" href="http://agorbatchev.typepad.com" />

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	
	<title><?php echo get_information("nama_sistem") ?> | Login</title>
	<meta name="description" content="Login sistem">
	<meta name="viewport" content="width=device-width,initial-scale=1">
	<meta name="format-detection" content="telephone=no">
	<link rel="shortcut icon" href="favicon.png" />

	<!-- The Styles -->
	<!-- ---------- -->
	<!-- Layout Styles -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/style.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/grid.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/layout.css">
	<!-- Icon Styles -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/icons.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/fonts/font-awesome.css">
	<!--[if IE 8]><link rel="stylesheet" href="<?php echo site_url()?>theme/css/fonts/font-awesome-ie7.css"><![endif]-->
	
	<!-- External Styles -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery-ui-1.8.21.custom.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.chosen.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.cleditor.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.colorpicker.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.elfinder.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.fancybox.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.jgrowl.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/jquery.plupload.queue.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/syntaxhighlighter/shCore.css" />
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/external/syntaxhighlighter/shThemeDefault.css" />
	
	<!-- Elements -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/elements.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/forms.css">
	
	<!-- OPTIONAL: Print Stylesheet for Invoice -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/print-invoice.css">
	
	<!-- Typographics -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/typographics.css">
	
	<!-- Responsive Design -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/media-queries.css">
	
	<!-- Bad IE Styles -->
	<link rel="stylesheet" href="<?php echo site_url()?>theme/css/ie-fixes.css">
	
	<!-- The Scripts -->
	<!-- ----------- -->
	
	<!-- JavaScript at the top (will be cached by browser) -->
	
	<!-- Load Webfont loader -->
	<script type="text/javascript">
        var base_url_js = "<?php echo site_url()?>";
        var constant_per_page = "<?php echo PER_PAGE?>";
		window.WebFontConfig = {
			google: { families: [ 'PT Sans:400,700' ] },
			active: function(){ $(window).trigger('fontsloaded') }
		};
	</script>
	<!--script defer async src="https://ajax.googleapis.com/ajax/libs/webfont/1.0.28/webfont.js"></script-->
	
	<!-- Essential polyfills -->
	<script src="<?php echo site_url()?>theme/js/mylibs/polyfills/modernizr-2.6.1.min.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/polyfills/respond.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/polyfills/matchmedia.js"></script>
	<!--[if lt IE 9]><script src="<?php echo site_url()?>theme/js/mylibs/polyfills/selectivizr-min.js"></script><![endif]-->
	<!--[if lt IE 10]><script src="<?php echo site_url()?>theme/js/mylibs/charts/excanvas.js"></script><![endif]-->
	<!--[if lt IE 10]><script src="<?php echo site_url()?>theme/js/mylibs/polyfills/classlist.js"></script><![endif]-->
	
	<script src="<?php echo site_url()?>theme/js/libs/jquery-1.7.2.min.js"></script>
	<script src="<?php echo site_url()?>theme/js/libs/jquery-ui-1.8.21.min.js"></script>
	<script src="<?php echo site_url()?>theme/js/libs/lo-dash.min.js"></script>
	
	<!-- General Scripts -->
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.hashchange.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.idle-timer.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.plusplus.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.jgrowl.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.scrollTo.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.ui.touch-punch.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/jquery.ui.multiaccordion.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/number-functions.js"></script>
	
	<!-- Forms -->
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.autosize.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.checkbox.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.chosen.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.cleditor.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.colorpicker.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.ellipsis.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.fileinput.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.fullcalendar.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.maskedinput.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.mousewheel.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.placeholder.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.pwdmeter.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.ui.datetimepicker.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.ui.spinner.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/jquery.validate.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/uploader/plupload.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/uploader/plupload.html5.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/uploader/plupload.html4.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/uploader/plupload.flash.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/forms/uploader/jquery.plupload.queue/jquery.plupload.queue.js"></script>
		
	<!-- Charts -->
	<script src="<?php echo site_url()?>theme/js/mylibs/charts/jquery.flot.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/charts/jquery.flot.orderBars.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/charts/jquery.flot.pie.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/charts/jquery.flot.resize.js"></script>
	
	<!-- Explorer -->
	<script src="<?php echo site_url()?>theme/js/mylibs/explorer/jquery.elfinder.js"></script>
	
	<!-- Fullstats -->
	<script src="<?php echo site_url()?>theme/js/mylibs/fullstats/jquery.css-transform.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/fullstats/jquery.animate-css-rotate-scale.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/fullstats/jquery.sparkline.js"></script>
	
	<!-- Syntax Highlighter -->
	<script src="<?php echo site_url()?>theme/js/mylibs/syntaxhighlighter/shCore.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/syntaxhighlighter/shAutoloader.js"></script>
	
	<!-- Dynamic Tables -->
	<script src="<?php echo site_url()?>theme/js/mylibs/dynamic-tables/jquery.dataTables.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/dynamic-tables/jquery.dataTables.tableTools.zeroClipboard.js"></script>
	<script src="<?php echo site_url()?>theme/js/mylibs/dynamic-tables/jquery.dataTables.tableTools.js"></script>
	
	<!-- Gallery -->
	<script src="<?php echo site_url()?>theme/js/mylibs/gallery/jquery.fancybox.js"></script>
	
	<!-- Tooltips -->
	<script src="<?php echo site_url()?>theme/js/mylibs/tooltips/jquery.tipsy.js"></script>
	
	<!-- Do not touch! -->
	<script src="<?php echo site_url()?>theme/js/mango.js"></script>
	<script src="<?php echo site_url()?>theme/js/plugins.js"></script>
	<script src="<?php echo site_url()?>theme/js/script.js"></script>
	
	<!-- Your custom JS goes here -->
	<script src="<?php echo site_url()?>theme/js/app.js"></script>
	
	<!-- end scripts -->
	
</head>

<body class=login>

	<!-- Some dialogs etc. -->

	<!-- The loading box -->
	<div id="loading-overlay"></div>
	<div id="loading">
		<span>Loading...</span>
	</div>
	<!-- End of loading box -->
	
	<!--------------------------------->
	<!-- Now, the page itself begins -->
	<!--------------------------------->
	
	<!-- The toolbar at the top -->
	<section id="toolbar">
		<div class="container_12">
		
			<!-- Left side -->
			<div class="left">
				<ul class="breadcrumb">					
				</ul>
			</div>
			<!-- End of .left -->
			
			<!-- Right side -->
			<div class="right">
				<ul>					
				</ul>
			</div><!-- End of .right -->
			
			<!-- Phone only items -->
			<div class="phone">
				
				<!-- User Link -->
				<li><a href="#"><span class="icon icon-home"></span></a></li>
				<!-- Navigation -->
				<li><a href="#"><span class="icon icon-heart"></span></a></li>
			
			</div><!-- End of .phone -->
			
		</div><!-- End of .container_12 -->
	</section><!-- End of #toolbar -->
	
	<!-- The header containing the logo -->
	<header class="container_12">

	<style type="text/css">
	.image-side {
		width: 30%;
		margin-left: 4%;
		margin-right: 50%;
		margin-top: -21.4%;
		position: fixed;

	}
	</style>
		<div class="container">
		
			<!-- Your logos -->
			<div style="">
			<a href="<?php echo site_url()?>"><img src="<?php echo site_url()?>theme/img/banner.png" alt="Logo" height="67"></a>
			<a class="phone-title" href="<?php echo site_url('sistem/login')?>"><img src="<?php echo site_url()?>theme/img/banner.png" alt="Logo" height="22" /></a>
			</div>
			<!-- Right link -->
			<div class="right">
			</div>
			
		</div><!-- End of .container -->
	
	</header><!-- End of header -->



	
	<!-- The container of the sidebar and content box -->
	<section id="login" class="container_12 clearfix">
		<div style="">
		<form action="<?php echo site_url('sistem/login') ?>" method="post" class="box validate">
		
			<div class="header">
				<h2><span class="icon icon-lock"></span>Login</h2>
			</div>
			
			<div class="content">
				
				<!-- Login messages -->
				<div class="login-messages">
                    <?php $error = get_error_message() ?>
                    <?php if(!empty($error)) : ?>
                        <div class="message welcome"><?php echo $error ?></div>
                    <?php else:?>
                        <div class="message welcome">Selamat Datang!</div>
                    <?php endif?>
                    
					<div class="message failure">Semua kolom harus diisi!</div>
				</div>
			
				<!-- The form -->
				<div class="form-box">
				
					<div class="row">
						<label for="login_name">
							<strong>Username</strong>
							<small>Identitas</small>
						</label>
						<div style="width:250px; float:right">
							<input tabindex=1 type="text" class="required noerror" name=login_name id=login_name />
						</div>
					</div>
					
					<div class="row">
						<label for="login_pw">
							<strong>Password</strong>
							<small>Kata Kunci</small>
						</label>
						<div style="width:250px; float:right">
							<input tabindex=2 type="password" class="required noerror" name=login_pw id=login_pw />
						</div>
					</div>
					
				</div><!-- End of .form-box -->
				
			</div><!-- End of .content -->
			
			<div class="actions">
				<div class="left">

				</div>
				<div class="right">
					<input tabindex=3 type="submit" value="Sign In" name="login_btn" />
				</div>
			</div><!-- End of .actions -->
			
		</form><!-- End of form -->
	</div>
	
	</section>
	
	<!-- Spawn $$.loaded -->
	<script>
		$$.loaded();
	</script>
	
	<!-- Prompt IE 6 users to install Chrome Frame. Remove this if you want to support IE 6.
	   chromium.org/developers/how-tos/chrome-frame-getting-started -->
	<!--[if lt IE 7 ]>
	<script defer src="http://ajax.googleapis.com/ajax/libs/chrome-frame/1.0.3/CFInstall.min.js"></script>
	<script defer>window.attachEvent('onload',function(){CFInstall.check({mode:'overlay'})})</script>
	<![endif]-->

</body>
</html>