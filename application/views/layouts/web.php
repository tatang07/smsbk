<html>
<head>
	<meta charset="utf-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>WEB SEKOLAH</title>

	<link rel="stylesheet" href="<?php echo site_url()?>theme/websekolah/css/main.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/websekolah/css/bootstrap.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/websekolah/css/font-awesome.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/websekolah/css/custom.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/websekolah/css/responsive.css">
	<link rel="stylesheet" href="<?php echo site_url()?>theme/websekolah/css/magnify.css">

	<script src="<?php echo site_url()?>theme/websekolah/js/jquery.js"></script>
	<script src="<?php echo site_url()?>theme/websekolah/js/bootstrap.min.js"></script>
	<script src="<?php echo site_url()?>theme/websekolah/js/jquery.magnify.js"></script>

</head>

<body>
﻿	<?php $this->load->view($view)?>
</body>
</html>