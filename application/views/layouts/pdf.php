	<?php $use_css = isset($use_css)?$use_css:true; ?>
	<?php if($use_css):?>
	<style type='text/css'>
		<!--
		
		.kotakfoto{
			width: 30mm;
			height: 25mm;
			border: solid 1px black;
			padding-top: 15mm;
			text-align: center;
			margin:0px;
			margin-left: 7mm;
			vertical-align: top;
		}
		
		.tbl{
			border: solid 0.1mm black;
			border-spacing:0;
			border-collapse:collapse;
			padding: 3px;			
			font-size: 9pt;
			margin:auto;
			width:100%;
			
		}
		
		.tbl2{
			border: solid 0.1mm black;
			border-spacing:0;
			border-collapse:collapse;
			padding: 3px;			
			font-size: 9pt;
			
			width:100%;
			
		}
		
		.tbl2 th{
			text-align: center;
			vertical-align: middle;
			border: solid 0.3mm black;
			padding:3px;
		}
		
		.tbl2 td{
			border: solid 0.3mm black;
			padding: 3px;
		}
		
		.tbl th{
			text-align: center;
			vertical-align: middle;
			border: solid 0.3mm black;
			padding:3px;
		}
		.center{
			text-align: center;
		}
		.tbl td{
			border: solid 0.3mm black;
			padding: 3px;
		}
		
		.clear{
			border: 0px;
			border-spacing:0;
			border-collapse:collapse;
			padding: 2px;			
			font-size: 10pt;
			text-align: center;
			vertical-align: middle;
			margin:auto;
			width:100%;
		}
		
		.judul{
			font-size: 16px;
			font-weight: bold;
			margin:1px;
		}
		.judultimes{
			font-size: 18px;
			font-weight: bold;
			font-family:Times;
			margin:1px;
		}
		.alamat{
			font-size: 12px;
		}
		
		.pnumber{
			font-size: 8pt;
			text-align:center;
		}
		
		.subjudul{
			font-size: 10pt;
			font-weight: bold;
			text-align:center;
			margin:1px;
		}
		
		.hrtop{
			border-top:1px solid;
			border-bottom:3px solid;
			height:2px;
		}
		.hrbottom{
			border: 1px;
			margin-top: -5px;
		}
		h2{
			font-size: 12pt;
		}
		h6 {page-break-before:always}
		
		.judultop{
			margin-top: -10px;
		}
		-->
		</style>
		<?php endif ?>
	
	<?php
		$nama_organisasi = "KOMITE OLAHRAGA NASIONAL INDONESIA";
    $daerah = "KOTA BANDUNG";
    $alamat = "Sekretariat: Jl. Jakarta No. 18, Bandung Telp. (022) 7275739 Fax. (022) 7275739";
	$web="website: http://koni-kotabandung.or.id email: info@koni-kotabandung.or.id";
	?>
	<page backtop="10" backbottom="10" backleft="10" backright="10"> 
        <page_header> 
              
        </page_header> 
        <page_footer> 
             
        </page_footer> 
   
        <table class="clear">
			<tr>
				<td style="width: 10%"><img src="<?php echo base_url()?>/img/logo-koni.jpg" height="90"/></td>
				<td style="width: 80%">
					<p class=judultimes><?php echo $nama_organisasi ?></p>
					<p class=judultimes><?php echo $daerah ?></p>
					<p class=alamat><?php echo $alamat ?><br/>
					<?php echo $web ?></p>
				</td>
				<td style="width: 10%"><img src="<?php echo base_url() ?>/img/logo.png" height="80"/></td>
			</tr>
		</table>
		<div class=hrtop></div>
		<br/> 
		<?php $this->load->view($view)?>
   </page> 