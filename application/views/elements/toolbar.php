<?php $gid = get_usergroup() ?>
<div class="user">
	<div class="avatar">
		<img src="<?php echo site_url() ?>theme/img/layout/content/toolbar/user/avatar.png">  
	</div>
	<span>Akun Saya</span>
	<ul>
        <li class="line"></li>
		<li><a href="javascript:openPopup('changePassword');">Ubah Password</a></li>
        <li class="line"></li>		
		<?php if($gid<=8 || $gid==11):?>
			<li><a href="<?php echo site_url('extras/guide/kepsek-manajer-operator.pdf')?>">Pedoman Penggunaan</a></li>
		<?php endif ?>
		<?php if($gid<=9 || $gid==11):?>
			<li><a href="<?php echo site_url('extras/guide/guru.pdf')?>">Pedoman Penggunaan Guru</a></li>
		<?php endif ?>
		<li><a href="<?php echo site_url('extras/guide/siswa-ortu.pdf')?>">Pedoman Penggunaan Siswa</a></li>
		<?php if($gid<9 || $gid==11):?>
		<li class="line"></li>	
		<li><a href="<?php echo site_url('guru/monitoring_admin')?>">Monitoring Guru</a></li>
		
		<?php endif ?>
		
		<?php if($gid==2 || $gid==3):?>
			<li class="line"></li>
			<li><a href="<?php echo site_url('sistem/user_sekolah')?>">Pengelolaan User</a></li>
			<li><a href="<?php echo site_url('sistem/user_guru')?>">Akun Guru</a></li>
			<li><a href="<?php echo site_url('sistem/user_siswa')?>">Akun Siswa</a></li>
		<?php endif ?>
		<?php if($gid==2 || $gid==3):?>
			<li class="line"></li>
			<li><a href="<?php echo site_url('sistem/setting_sekolah')?>">Pengaturan Sistem</a></li>
		<?php endif ?>
		<li class="line"></li>
		<li><a href="<?php echo site_url('sistem/logout')?>">Logout</a></li>
	</ul>
</div>

<ul class="shortcuts">
	<li>
	<a href="#">
		<span style="color: white; font-weight: bold; font-size:13px; line-height:29px">
			<?php echo $gid>1?get_nama_sekolah():"" ?> 
		</span>
	</a>
	</li>	
</ul>


