<?php $gid = get_usergroup() ?>
<?php $component_var = isset($component)?$component:"";?>
<?php $component = isset($conf['component'])?$conf['component']:$component_var;?>

<?php $hak[1] = array(2,3,4,9) //Kurikulum?>
<?php $hak[2] = array(2,3,5)  //Siswa?>
<?php $hak[3] = array(2,3)  //PPDB?>
<?php $hak[4] = array(2,3,9) //PTK?>
<?php $hak[5] = array(2,3,6) //SAPRAS?>
<?php $hak[6] = array(2,3) //KEPEMIMPINAN?>
<?php $hak[7] = array(2,3) //PENGSEK?>
<?php $hak[8] = array(2,3,8) //PEMBIAYAAN?>
<?php $hak[9] = array(2,3,9) //EVALUASI?>
<?php $hak[10] = array(2,3,7) //KOMUNIKASI?>
<?php $id_kelas_wali = $gid<4?-1:0 ?>

<!-- Navigation -->
	<nav><ul class="collapsible accordion">
		<?php if($gid==1 && FALSE):?>
			<li>
				<a href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Administrasi</a>
				<ul>
					<li><a href="<?php echo site_url('sistem/user/datalist')?>"><span class="icon icon-list"></span>Pengguna</a></li>
				</ul>
			</li>
		<?php endif ?>
	
		<?php $komponen = "kurikulum" ?> 
		<?php if(in_array($gid, $hak[1])):?>
		<li <?php echo $component==$komponen?"class='current'":""?>>
				<a  <?php echo $component==$komponen?"class='with_sub open'":""?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Kurikulum</a>
				<ul>
					<!-- <li><a href="<?php //echo site_url('kurikulum/datalist/')?>"><span class="icon icon-list"></span><span class="link-text">Jenis Kurikulum</span></a></li> -->
					<li><a href="#"><span class="icon icon-list"></span><span class="link-text">Struktur Kurikulum</span></a>
						<ul>
							<li><a href="<?php echo site_url('kurikulum/kompetensi_inti/')?>"><span class="icon icon-list"></span><span class="link-text">Kompetensi Inti</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/daftar_pelajaran/')?>"><span class="icon icon-list"></span><span class="link-text">Daftar Mata Pelajaran</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/struktur_kurikulum/')?>"><span class="icon icon-list"></span><span class="link-text">Struktur</span></a></li>
							<?php if(get_jenjang_sekolah()==2){ ?>
								<li><a href="<?php echo site_url('kurikulum/pelajaran_tematik/')?>"><span class="icon icon-list"></span><span class="link-text">Pelajaran Tematik</span></a></li>
							<?php } ?>
						</ul>
					</li>
					<?php if(get_jenjang_sekolah()==4){ ?>
					<li><a href="<?php echo site_url('kurikulum/kur_program/')?>"><span class="icon icon-list"></span><span class="link-text">Program</span></a></li>
					<?php } ?>
					<li><a href="<?php echo site_url('kurikulum/silabus/')?>"><span class="icon icon-list"></span><span class="link-text">Silabus</span></a></li>
					<?php if(get_jenjang_sekolah()==2){ ?>
						<li><a href="<?php echo site_url('kurikulum/ketersediaan_silabus_tematik/')?>"><span class="icon icon-list"></span><span class="link-text">Silabus Tematik</span></a></li>
					<?php } ?>
					<li><a href="<?php echo site_url('kurikulum/ketersediaan_silabus/')?>"><span class="icon icon-list"></span><span class="link-text">Ketersediaan Silabus</span></a></li>					
					<li><a href="<?php echo site_url('kurikulum/rpp/')?>"><span class="icon icon-list"></span><span class="link-text">Rencana Pelaksanaan Pembelajaran</span></a></li>
					<li><a href="<?php echo site_url('kurikulum/ketersediaan_rpp/')?>"><span class="icon icon-list"></span><span class="link-text">Ketersediaan Rencana Pelaksanaan Pembelajaran</span></a></li>
					<?php if(get_jenjang_sekolah()==2){ ?>
						<li><a href="<?php echo site_url('kurikulum/rpp_tematik/')?>"><span class="icon icon-list"></span><span class="link-text">Rencana Pelaksanaan Pembelajaran Tematik</span></a></li>
					<?php } ?>
					<?php if($gid!=9):?>
						<li><a href="<?php echo site_url('kurikulum/penentuan_guru_matpel/')?>"><span class="icon icon-list"></span><span class="link-text">Penentuan Guru Pelajaran Kelas</span></a></li>
					<?php endif ?>
					<li><a href="<?php echo site_url('kurikulum/jadwal_pelajaran/')?>"><span class="icon icon-list"></span><span class="link-text">Jadwal</span></a></li>					
					<li><a href="<?php echo site_url('kurikulum/dokumen_kurikulum/')?>"><span class="icon icon-list"></span><span class="link-text">Dokumen Kurikulum</span></a></li>					
					<li><a href="<?php echo site_url('kurikulum/agenda_kelas/')?>"><span class="icon icon-list"></span><span class="link-text">Agenda Kelas</span></a></li>					
					<li><a href="#"><span class="icon icon-list"></span><span class="link-text">Presensi</span></a>
						<ul>
							<li><a href="<?php echo site_url('kurikulum/presensi_siswa/')?>"><span class="icon icon-list"></span><span class="link-text">Rekap Presensi Siswa</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/presensi_guru/')?>"><span class="icon icon-list"></span><span class="link-text">Rekap Presensi Guru</span></a></li>
						</ul>
					</li>
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Ekstra Kurikuler</span></a>					
						<ul>
							<li><a href="<?php echo site_url('kurikulum/ekskul_jenis/')?>"><span class="icon icon-list"></span><span class="link-text">Jenis dan Penanggung Jawab</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/peserta_ekstrakurikuler/')?>"><span class="icon icon-list"></span><span class="link-text">Peserta</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/jadwal_ekskul/')?>"><span class="icon icon-list"></span><span class="link-text">Jadwal</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/ekskul_fasilitas/')?>"><span class="icon icon-list"></span><span class="link-text">Fasilitas</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/prestasi_siswa/')?>"><span class="icon icon-list"></span><span class="link-text">Prestasi</span></a></li>
						</ul>
					</li>
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Pengembangan Diri</span></a>					
						<ul>
							<li><a href="<?php echo site_url('kurikulum/pengembangan_jenis/')?>"><span class="icon icon-list"></span><span class="link-text">Jenis dan Penanggung Jawab</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/peserta_pengembangan/datalist')?>"><span class="icon icon-list"></span><span class="link-text">Peserta</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/jadwal_pengembangan_diri/')?>"><span class="icon icon-list"></span><span class="link-text">Jadwal</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/pengembangan_fasilitas/')?>"><span class="icon icon-list"></span><span class="link-text">Fasilitas</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/pengembangan_prestasi/')?>"><span class="icon icon-list"></span><span class="link-text">Prestasi</span></a></li>
						</ul>
					</li>
				</ul>
		</li>
		<?php endif ?>
		<?php if(in_array($gid, $hak[2])):?>
		<?php $komponen = "pps" ?>
		<li <?php echo $component==$komponen?"class='current'":""?>>
				<a  <?php echo $component==$komponen?"class='with_sub open'":""?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Pelayanan dan Pembinaan Siswa</a>
				<ul>									
					<li><a href="<?php echo site_url('pps/siswa/datalist')?>"><span class="icon icon-list"></span><span class="link-text">Identitas Siswa</span></a></li>					
					<li><a href="<?php echo site_url('pps/gender/')?>"><span class="icon icon-list"></span><span class="link-text">Proporsi Gender</span></a></li>					
					<li><a href="<?php echo site_url('pps/p_ortu/')?>"><span class="icon icon-list"></span><span class="link-text">Pekerjaan dan Penghasilan Orang Tua</span></a>						
					</li>
					<li><a href="<?php echo site_url('pps/hobi/')?>"><span class="icon icon-list"></span><span class="link-text">Hobi, Bakat dan Minat Siswa</span></a></li>					
					<li><a href="<?php echo site_url('pps/agama/')?>"><span class="icon icon-list"></span><span class="link-text">Proporsi Agama Siswa</span></a></li>					
					<li><a href="<?php echo site_url('pps/rekap_siswa/')?>"><span class="icon icon-list"></span><span class="link-text">Rekapitulasi Jumlah Siswa</span></a></li>					
					<li><a href="<?php echo site_url('pps/keadaan_peserta_didik/')?>"><span class="icon icon-list"></span><span class="link-text">Keadaan Peserta Didik</span></a></li>					
					<li><a href="<?php echo site_url('pps/bp_bk')?>"><span class="icon icon-list"></span><span class="link-text">Catatan BP/BK</span></a></li>					
					<li><a href="<?php echo site_url('pps/mutasi_siswa/datalist')?>"><span class="icon icon-list"></span><span class="link-text">Mutasi Siswa</span></a></li>					
					<li><a href="<?php echo site_url('pps/penentuan_kelas/datalist')?>"><span class="icon icon-list"></span><span class="link-text">Penentuan Rombel Siswa</span></a></li>
					<li><a href="<?php echo site_url('pps/angka_melanjutkan/')?>"><span class="icon icon-list"></span><span class="link-text">Angka Melanjutkan</span></a></li>					
	
					<li><a href="<?php echo site_url('pps/prestasi_siswa/')?>"><span class="icon icon-list"></span><span class="link-text">Siswa Berprestasi</span></a></li>					

					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Organisasi Siswa</span></a>
						<ul>
							<li><a href="<?php echo site_url('pps/organisasi_siswa/')?>"><span class="icon icon-list"></span><span class="link-text">Daftar Organisasi</span></a></li>
							<li><a href="<?php echo site_url('pps/struktur_organisasi_siswa')?>"><span class="icon icon-list"></span><span class="link-text">Struktur Organisasi </span></a></li>
							<li><a href="<?php echo site_url('pps/susunan_pengurus_organisasi')?>"><span class="icon icon-list"></span><span class="link-text">Susunan Pengurus </span></a></li>
							<li><a href="<?php echo site_url('pps/organisasi_program')?>"><span class="icon icon-list"></span><span class="link-text">Kegiatan</span></a></li>
						</ul>
					</li>					
					<li><a href="<?php echo site_url('pps/penelusuran_siswa_berbakat/')?>"><span class="icon icon-list"></span><span class="link-text">Penelusuran dan Pembinaan Siswa Berbakat</span></a></li>					
				</ul>
		</li>
		<?php endif ?>
		<?php if(in_array($gid, $hak[3])):?>
		<?php $komponen = "seleksi_penerimaan" ?>
		<li <?php echo $component==$komponen?"class='current'":""?>>
				<a  <?php echo $component==$komponen?"class='with_sub open'":""?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Seleksi Penerimaan Peserta Didik Baru</a>
				<ul>
					<li><a href="<?php echo site_url('statis/PPDB_info_umum/')?>"><span class="icon icon-list"></span><span class="link-text">Informasi Umum</span></a></li>					
					<li><a href="<?php echo site_url('statis/PPDB_info_pendaftaran/')?>"><span class="icon icon-list"></span><span class="link-text">Informasi Pendaftaran</span></a></li>					
					<li><a href="<?php echo site_url('statis/PPDB_form_pendaftaran/')?>"><span class="icon icon-list"></span><span class="link-text">Formulir Pendaftaran</span></a></li>					
					<li><a href="<?php echo site_url('statis/PPDB_daftar_calon_siswa/')?>"><span class="icon icon-list"></span><span class="link-text">Daftar Calon Siswa Baru</span></a></li>					
					<li><a href="<?php echo site_url('statis/PPDB_hasil_penerimaan_siswa/')?>"><span class="icon icon-list"></span><span class="link-text">Hasil Penerimaan Siswa Baru</span></a></li>					
					<li><a href="<?php echo site_url('statis/PPDB_regestrasi_calon_siswa/')?>"><span class="icon icon-list"></span><span class="link-text">Registrasi Siswa Baru</span></a></li>					
					<li><a href="<?php echo site_url('statis/PPDB_pengkelasan_siswa_baru/')?>"><span class="icon icon-list"></span><span class="link-text">Penentuan Rombel Siswa Baru</span></a></li>					
					<!--li><a href="<?php echo site_url('statis/PPDB_informasi_registrasi/')?>"><span class="icon icon-list"></span><span class="link-text">Informasi Registrasi</span></a></li-->					
				</ul>
		</li>
		<?php endif ?>
		<?php if(in_array($gid, $hak[4])):?>
		<?php $komponen = "pendidik_dan_tenaga_kependidikan" ?>
		<li <?php echo $component==$komponen?"class='current'":""?>>
				<a  <?php echo $component==$komponen?"class='with_sub open'":""?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Pendidik dan Tenaga Kependidikan</a>
				<ul>
				<?php if($gid!=9):?>
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Recruitment</span></a>
						<ul>
							<li><a href="<?php echo site_url('statis/ptk_syarat/')?>"><span class="icon icon-list"></span><span class="link-text">Persyaratan</span></a></li>
							<li><a href="<?php echo site_url('statis/ptk_proses_seleksi/')?>"><span class="icon icon-list"></span><span class="link-text">Proses Seleksi</span></a></li>
							<li><a href="<?php echo site_url('ptk/guru/add')?>"><span class="icon icon-list"></span><span class="link-text">Penerimaan dan Pengangkatan</span></a>
							<ul>
								<li><a href="<?php echo site_url('ptk/guru/add/1')?>"><span class="icon icon-list"></span><span class="link-text">Pendidik</span></a>
								<li><a href="<?php echo site_url('ptk/guru/add/2')?>"><span class="icon icon-list"></span><span class="link-text">Tenaga Kependidikan</span></a>
							</ul>
							</li>
						</ul>
					</li>
				<?php endif?>
				<?php if($gid!=9):?>
					<li><a href="<?php echo site_url('ptk/guru/datalist')?>"><span class="icon icon-list"></span><span class="link-text">Identitas Pendidik dan Tenaga Kependidikan</span></a></li>
				<?php else:?>	
					<li><a href="<?php echo site_url('ptk/guru/detail')?>"><span class="icon icon-list"></span><span class="link-text">Identitas Pendidik dan Tenaga Kependidikan</span></a></li>
				<?php endif ?>
				<?php if($gid!=9):?>
					<li>
					<a href="<?php echo site_url('ptk/kualifikasi_pendidikan/')?>"><span class="icon icon-list"></span><span class="link-text">Kualifikasi Pendidikan</span></a>
						<ul>
							<a href="<?php echo site_url('ptk/kualifikasi_pendidikan/pendidik')?>"><span class="icon icon-list"></span><span class="link-text">Pendidik</span></a>
							<a href="<?php echo site_url('ptk/kualifikasi_pendidikan/tenaga_kependidikan')?>"><span class="icon icon-list"></span><span class="link-text">Tenaga Kependidikan</span></a>
						</ul>
					</li>					
				<?php endif?>	
					<li><a href="<?php echo site_url('statis/kompetensi_guru/')?>"><span class="icon icon-list"></span><span class="link-text">Kompetensi Pendidik dan Tenaga Kependidikan</span></a></li>
				<?php	if($gid!=9){ ?>				
					<li><a href="<?php echo site_url('ptk/pdpbm/datalist')?>"><span class="icon icon-list"></span><span class="link-text">Penugasan dan Penentuan Beban Mengajar</span></a></li>
				<?php }else{ ?>	
					<li><a href="<?php echo site_url('ptk/penugasan_dan_penentuan_beban_mengajar_guru/')?>"><span class="icon icon-list"></span><span class="link-text">Penugasan dan Penentuan Beban Mengajar</span></a></li>
				<?php }?>
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Kinerja</span></a>
						<ul>
							<li><a href="<?php echo site_url('statis/orientasi_guru') ?>"><span class="icon icon-list"></span><span class="link-text">Orientasi Kerja/Jabatan</span></a></li>
							<?php if($gid!=9){?>
							<li><a href="<?php echo site_url('ptk/supervisi/')?>"><span class="icon icon-list"></span><span class="link-text">Pembinaan dan Pembimbingan</span></a></li> <?php }elseif($gid==9){?>
							<li><a href="<?php echo site_url('ptk/supervisi_guru/')?>"><span class="icon icon-list"></span><span class="link-text">Pembinaan dan Pembimbingan</span></a></li>
							<?php } ?>
							
							<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Evaluasi Kinerja</span></a>
								<ul>
									<li><a href="<?php echo site_url('ptk/kinerja_guru/')?>"><span class="icon icon-list"></span><span class="link-text">Penilaian Tugas Pokok</span></a>
									<li><a href="<?php echo site_url('ptk/dp3_guru/')?>"><span class="icon icon-list"></span><span class="link-text">SKP (Sistem Kinerja Pegawai)</span></a>
									<li><a href="<?php echo site_url('ptk/evaluasi_diri/')?>"><span class="icon icon-list"></span><span class="link-text">Evaluasi Diri</span></a>
								</ul>
							</li>
						</ul>
					</li>
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Pengembangan Profesi Berkelanjutan</span></a>
						<ul>
							<li><a href="<?php echo site_url('ptk/studi_lanjut')?>"><span class="icon icon-list"></span><span class="link-text">Studi Lanjut</span></a></li>							
							<li><a href="<?php echo site_url('ptk/sertifikasi')?>"><span class="icon icon-list"></span><span class="link-text">Sertifikasi</span></a></li>							
							<li><a href="<?php echo site_url('ptk/pertemuan_keilmuan')?>"><span class="icon icon-list"></span><span class="link-text">Pertemuan-pertemuan Keilmuan</span></a></li>							
							<li><a href="<?php echo site_url('ptk/peningkatan_kompetensi')?>"><span class="icon icon-list"></span><span class="link-text">Program Peningkatan Kompetensi</span></a></li>							
						</ul>
					</li>					
					<!--<<li><a href="<?php echo site_url('ptk/kualifikasi_pangkat/')?>"><span class="icon icon-list"></span><span class="link-text">Kualifikasi Golongan / Pangkat</span></a></li>		-->			
					<!--<li><a href="<?php echo site_url('ptk/guru/datalist')?>"><span class="icon icon-list"></span><span class="link-text">Identitas Tenaga Pendidik</span></a></li>					-->
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Kesejahteraan Pendidik dan Tenaga Kependidikan</span></a>
						<ul>
							<?php if($gid!=9){?>
							<li><a href="<?php echo site_url('ptk/kg_gaji/datalist')?>"><span class="icon icon-list"></span><span class="link-text">Gaji</span></a></li>
							<?php }elseif($gid==9){?>
							<li><a href="<?php echo site_url('ptk/kg_gaji_guru/')?>"><span class="icon icon-list"></span><span class="link-text">Gaji</span></a></li>
							<?php }?>
							
							<?php if($gid!=9){?>
							<li><a href="<?php echo site_url('ptk/tunjangan_tunjangan')?>"><span class="icon icon-list"></span><span class="link-text">Tunjangan-Tunjangan</span></a></li>
							<?php }elseif($gid==9){?>
							<li><a href="<?php echo site_url('ptk/tunjangan_tunjangan_guru')?>"><span class="icon icon-list"></span><span class="link-text">Tunjangan-Tunjangan</span></a></li>
							<?php }?>
							
							<?php if($gid!=9){?>
							<li><a href="<?php echo site_url('ptk/potongan_potongan')?>"><span class="icon icon-list"></span><span class="link-text">Potongan-Potongan</span></a></li>
							<?php }elseif($gid==9){?>
							<li><a href="<?php echo site_url('ptk/potongan_potongan_guru')?>"><span class="icon icon-list"></span><span class="link-text">Potongan-Potongan</span></a></li>
							<?php }?>
							
							<?php if($gid!=9){?>
							<li><a href="<?php echo site_url('ptk/asuransi/')?>"><span class="icon icon-list"></span><span class="link-text">Asuransi Kesehatan</span></a></li>
							<?php }elseif($gid==9){?>
							<li><a href="<?php echo site_url('ptk/asuransi_guru/')?>"><span class="icon icon-list"></span><span class="link-text">Asuransi Kesehatan</span></a></li>
							<?php }?>
							
							
						</ul>
					</li>										
				</ul>
		</li>
		<?php endif ?>
		<?php if(in_array($gid, $hak[5])):?>
		<?php $komponen = "sapras" ?>
		<li <?php echo $component==$komponen?"class='current'":""?>>
				<a  <?php echo $component==$komponen?"class='with_sub open'":""?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Sarana dan Prasarana Pendidikan</a>
				<ul>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/3')?>"><span class="icon icon-list"></span><span class="link-text">Ruang Kelas</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/5')?>"><span class="icon icon-list"></span><span class="link-text">Laboratorium, Bengkel dan Studio</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/7')?>"><span class="icon icon-list"></span><span class="link-text">Peralatan Pendidikan</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/6')?>"><span class="icon icon-list"></span><span class="link-text">Teknologi Informasi dan Komunikasi</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/8')?>"><span class="icon icon-list"></span><span class="link-text">Perpustakaan</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/4')?>"><span class="icon icon-list"></span><span class="link-text">Fasilitas Olahraga</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/1')?>"><span class="icon icon-list"></span><span class="link-text">Auditorium</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/12')?>"><span class="icon icon-list"></span><span class="link-text">Masjid/Musala</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/13')?>"><span class="icon icon-list"></span><span class="link-text">Toilet</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/11')?>"><span class="icon icon-list"></span><span class="link-text">Kafetaria/Warung</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/14')?>"><span class="icon icon-list"></span><span class="link-text">Listrik dan Air</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/9')?>"><span class="icon icon-list"></span><span class="link-text">Gudang</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/10')?>"><span class="icon icon-list"></span><span class="link-text">Sistem Pengamanan</span></a></li>
					
					<!--li><a href="<?php echo site_url('sarana_prasarana/datalist/2')?>"><span class="icon icon-list"></span><span class="link-text">Rangkuman Profil Sarana dan Prasarana</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/15')?>"><span class="icon icon-list"></span><span class="link-text">Sarana Prasarana Umum</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/16')?>"><span class="icon icon-list"></span><span class="link-text">Sarana Pembelajaran</span></a></li>
					<li><a href="<?php echo site_url('sarana_prasarana/datalist/17')?>"><span class="icon icon-list"></span><span class="link-text">Inventaris Buku</span></a></li-->
				</ul>
		</li>
		<?php endif ?>
		<?php if(in_array($gid, $hak[6])):?>
		<?php $komponen = "kepemimpinan" ?>
		<li <?php echo $component==$komponen?"class='current'":""?>>
				<a  <?php echo $component==$komponen?"class='with_sub open'":""?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Kepemimpinan</a>
				<ul>
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Recruitment</span></a>
						<ul>
							<li><a href="<?php echo site_url('statis/persyaratan_menjadi_kepala_sekolah/')?>"><span class="icon icon-list"></span><span class="link-text">Persyaratan</span></a></li>
							<li><a href="<?php echo site_url('statis/proses_seleksi_kepsek')?>"><span class="icon icon-list"></span><span class="link-text">Proses Seleksi</span></a></li>
							<li><a href="<?php echo site_url('kepemimpinan/kepala_sekolah/add')?>"><span class="icon icon-list"></span><span class="link-text">Penerimaan dan Pengangkatan</span></a></li>
						</ul>
					</li>
					<li><a href="<?php echo site_url('kepemimpinan/kepala_sekolah/')?>"><span class="icon icon-list"></span><span class="link-text">Identitas Kepala Sekolah</span></a></li>
					<!--li><a href="<?php echo site_url('statis/persyaratan_menjadi_kepala_sekolah/')?>"><span class="icon icon-list"></span><span class="link-text">Persyaratan Menjadi Kepala Sekolah</span></a></li-->
					<li><a href="<?php echo site_url('statis/kompetensi_kepsek/')?>"><span class="icon icon-list"></span><span class="link-text">Kompetensi Kepala Sekolah</span></a></li>
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Kinerja Kepala Sekolah</span></a>
						<ul>
							<li><a href="<?php echo site_url('statis/orientasi_kepsek')?>"><span class="icon icon-list"></span><span class="link-text">Orientasi Kerja/Jabatan</span></a></li>
							<li><a href="<?php echo site_url('kepemimpinan/supervisi/')?>"><span class="icon icon-list"></span><span class="link-text">Pembinaan dan Pembimbingan</span></a></li>
							<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Evaluasi Kinerja</span></a>
								<ul>
									<li><a href="<?php echo site_url('kepemimpinan/penilaian_tugas_pokok/')?>"><span class="icon icon-list"></span><span class="link-text">Penilaian Tugas Pokok</span></a>
									<li><a href="<?php echo site_url('kepemimpinan/sistem_kerja_pegawai/')?>"><span class="icon icon-list"></span><span class="link-text">SKP (Sistem Kinerja Pegawai)</span></a>
									<li><a href="<?php echo site_url('kepemimpinan/evaluasi_diri')?>"><span class="icon icon-list"></span><span class="link-text">Evaluasi Diri</span></a>
								</ul>
							</li>
						</ul>
					</li>
					<li><a href="#"><span class="icon icon-list"></span><span class="link-text">Kebijakan Pengembangan Sekolah</span></a>
						<ul>
							<li><a href="<?php echo site_url('kepemimpinan/kebijakan_nasional')?>"><span class="icon icon-list"></span><span class="link-text">Kebijakan Nasional, Provinsi, Kabupaten Kota</span></a></li>
							<li><a href="<?php echo site_url('kepemimpinan/kebijakan_sekolah/')?>"><span class="icon icon-list"></span><span class="link-text">Kebijakan Sekolah</span></a></li>
						</ul>
					</li>
					<li><a href="<?php echo site_url('kepemimpinan/program_kerja_tahunan/')?>"><span class="icon icon-list"></span><span class="link-text">Program Kerja Tahunan</span></a></li>
					
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Pembinaan Profesi Kepala Sekolah</span></a>
						<ul>
							<li><a href="<?php echo site_url('kepemimpinan/studi_lanjut')?>"><span class="icon icon-list"></span><span class="link-text">Studi Lanjut</span></a></li>							
							<li><a href="<?php echo site_url('kepemimpinan/sertifikasi')?>"><span class="icon icon-list"></span><span class="link-text">Sertifikasi</span></a></li>							
							<li><a href="<?php echo site_url('kepemimpinan/pertemuan/')?>"><span class="icon icon-list"></span><span class="link-text">Pertemuan-pertemuan Keilmuan</span></a></li>							
							<li><a href="<?php echo site_url('kepemimpinan/peningkatan_kompetensi')?>"><span class="icon icon-list"></span><span class="link-text">Program Peningkatan Kompetensi</span></a></li>							
						</ul>
					</li>	
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Kesejahteraan Kepala Sekolah</span></a>
						<ul>
							<li><a href="<?php echo site_url('kepemimpinan/gaji')?>"><span class="icon icon-list"></span><span class="link-text">Gaji</span></a></li>
							<li><a href="<?php echo site_url('kepemimpinan/tunjangan_tunjangan')?>"><span class="icon icon-list"></span><span class="link-text">Tunjangan-Tunjangan</span></a></li>
							<li><a href="<?php echo site_url('kepemimpinan/asuransi/')?>"><span class="icon icon-list"></span><span class="link-text">Asuransi Kesehatan</span></a></li>
						</ul>
					</li>
				</ul>
		</li>
		<?php endif ?>
		<?php if(in_array($gid, $hak[7])):?>
		<?php $komponen = "pengelolaan_sekolah" ?>
		<li <?php echo $component==$komponen?"class='current'":""?>>
				<a  <?php echo $component==$komponen?"class='with_sub open'":""?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Pengelolaan Sekolah</a>
				<ul>
					<li><a href="<?php echo site_url('pengelolaan_sekolah/visi_misi/')?>"><span class="icon icon-list"></span><span class="link-text">Visi dan Misi</span></a></li>
					<li><a href="<?php echo site_url('pengelolaan_sekolah/struktur_organisasi/')?>"><span class="icon icon-list"></span><span class="link-text">Struktur Organisasi Sekolah</span></a></li>
					<li><a href="<?php echo site_url('pengelolaan_sekolah/pdftj/')?>"><span class="icon icon-list"></span><span class="link-text">Penentuan Posisi, Distribusi, Kewenangan, Fungsi dan Tanggung Jawab</span></a></li>
					<li><a href="<?php echo site_url('pengelolaan_sekolah/jobdesc/')?>"><span class="icon icon-list"></span><span class="link-text">Uraian Tugas (Job Description)</span></a></li>
					<li><a href="<?php echo site_url('#')?>"><span class="icon icon-list"></span><span class="link-text">Perencanaan Sekolah</span></a>
						<ul>
							<li><a href="<?php echo site_url('pengelolaan_sekolah/rencana_dan_anggaran_tahunan_sekolah')?>"><span class="icon icon-list"></span><span class="link-text">Penyusunan Anggaran</span></a></li>
							<li><a href="<?php echo site_url('pengelolaan_sekolah/kalender_akademik/')?>"><span class="icon icon-list"></span><span class="link-text">Penyusunan Kalender Akademik</span></a></li>
						</ul>
					</li>
					
					<!-- <li><a href="#"><span class="icon icon-list"></span><span class="link-text">Implementasi Perencanaan</span></a></li> -->
					<li><a href="#"><span class="icon icon-list"></span><span class="link-text">Pengawasan</span></a>
						<ul>
							<li><a href="<?php echo site_url('pengelolaan_sekolah/monitoring/')?>"><span class="icon icon-list"></span><span class="link-text">Monitoring</span></a></li>
							<li><a href="#"><span class="icon icon-list"></span><span class="link-text">Review dan Evaluasi</span></a>
								<ul>
									<li><a href="<?php echo site_url('pengelolaan_sekolah/review_proses/')?>"><span class="icon icon-list"></span><span class="link-text">Terhadap Proses Pelaksanaan Program</span></a></li>
									<li><a href="<?php echo site_url('pengelolaan_sekolah/review_pencapaian/')?>"><span class="icon icon-list"></span><span class="link-text">Terhadap Pencapaian Program</span></a></li>
								</ul>
							</li>
						</ul>
					</li>
				</ul>
		</li>
		<?php endif ?>
		<?php if(in_array($gid, $hak[8])):?>
		<?php $komponen = "Pembiayaan" ?>
		<li <?php echo $component==$komponen?"class='current'":""?>>
				<a  <?php echo $component==$komponen?"class='with_sub open'":""?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Pembiayaan</a>
				<ul>
					<li><a href="<?php echo site_url('pembiayaan/rapbs/')?>"><span class="icon icon-list"></span><span class="link-text">Rencana Anggaran Pendapatan dan Belanja Sekolah (RAPBS)</span></a></li>
					<li><a href="<?php echo site_url('#')?>"><span class="icon icon-list"></span><span class="link-text">BOS</span></a>
						<ul>
							<li><a href="<?php echo site_url('bos/sumber_dana/')?>"><span class="icon icon-list"></span><span class="link-text">Sumber Dana</span></a></li>
							<li><a href="<?php echo site_url('bos/bos_program')?>"><span class="icon icon-list"></span><span class="link-text">Program</span></a></li>
							<li><a href="<?php echo site_url('bos/bos_subprogram')?>"><span class="icon icon-list"></span><span class="link-text">Sub Program</span></a></li>
							<li><a href="<?php echo site_url('bos/bos_kegiatan')?>"><span class="icon icon-list"></span><span class="link-text">Kegiatan</span></a></li>
							<li><a href="<?php echo site_url('bos/rencana_pendapatan/')?>"><span class="icon icon-list"></span><span class="link-text">Rencana Pendapatan</span></a></li>
							<li><a href="<?php echo site_url('bos/rencana_pengeluaran/')?>"><span class="icon icon-list"></span><span class="link-text">Rencana Pengeluaran</span></a></li>
							<li><a href="<?php echo site_url('bos/realisasi_pendapatan/')?>"><span class="icon icon-list"></span><span class="link-text">Realisasi Pendapatan</span></a></li>
							<li><a href="<?php echo site_url('bos/realisasi_pengeluaran/')?>"><span class="icon icon-list"></span><span class="link-text">Realisasi Pengeluaran</span></a></li>
						</ul>
					</li>
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Dana Sumbangan Pendidikan Bulanan (DSPB)</span></a>
						<ul>
							<li><a href="<?php echo site_url('pembiayaan/dspb_penentuan_besaran')?>"><span class="icon icon-list"></span><span class="link-text">Penentuan Besaran</span></a></li>
							<li><a href="<?php echo site_url('pembiayaan/dspb_tunggakan')?>"><span class="icon icon-list"></span><span class="link-text">Tunggakan</span></a></li>
							<li><a href="<?php echo site_url('pembiayaan/dspb_keringanan')?>"><span class="icon icon-list"></span><span class="link-text">Keringanan</span></a></li>
							<li><a href="<?php echo site_url('pembiayaan/dspb_pembebasan')?>"><span class="icon icon-list"></span><span class="link-text">Pembebasan</span></a></li>
						</ul>
					</li>
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Dana Sumbangan Pendidikan Tahunan (DSPT)</span></a>
						<ul>
							<li><a href="<?php echo site_url('pembiayaan/dspt_penentuan_besaran')?>"><span class="icon icon-list"></span><span class="link-text">Penentuan Besaran</span></a></li>
							<li><a href="<?php echo site_url('pembiayaan/dspt_tunggakan')?>"><span class="icon icon-list"></span><span class="link-text">Tunggakan</span></a></li>
							<li><a href="<?php echo site_url('pembiayaan/dspt_keringanan')?>"><span class="icon icon-list"></span><span class="link-text">Keringanan</span></a></li>
							<li><a href="<?php echo site_url('pembiayaan/dspt_pembebasan')?>"><span class="icon icon-list"></span><span class="link-text">Pembebasan</span></a></li>
						</ul>
					</li>
					<li><a href="<?php echo site_url('pembiayaan/sumber_lain')?>"><span class="icon icon-list"></span><span class="link-text">Sumber Lain</span></a>
					<li><a href="<?php echo site_url('pembiayaan/pembukuan/')?>"><span class="icon icon-list"></span><span class="link-text">Pembukuan</span></a></li>
					
					<!--li><a href="<?php echo site_url('pembiayaan/rekap_beasiswa/')?>"><span class="icon icon-list"></span><span class="link-text">Rekap Penerima Beasiswa</span></a></li>
					<li><a href="<?php echo site_url('pembiayaan/grafik_beasiswa/')?>"><span class="icon icon-list"></span><span class="link-text">Grafik Penerima Beasiswa</span></a></li>

					<li><a href="<?php echo site_url('pembiayaan/rekap_dspb/')?>"><span class="icon icon-list"></span><span class="link-text">Rekap Pembayaran DSPB</span></a></li>
					<li><a href="<?php echo site_url('pembiayaan/dspt/')?>"><span class="icon icon-list"></span><span class="link-text">Laporan Dana Sumbangan Pendidikan Tahunan</span></a></li>
					<li><a href="<?php echo site_url('pembiayaan/rekap_penerima_keringanan_pembayaran/')?>"><span class="icon icon-list"></span><span class="link-text">Rekap Penerima Keringanan Pembayaran</span></a></li>
					<li><a href="<?php echo site_url('pembiayaan/rekap_keluar_masuk/')?>"><span class="icon icon-list"></span><span class="link-text">Rekap Keluar Masuk</span></a></li-->
				</ul>
		</li>
		<?php endif ?>
		<?php if(in_array($gid, $hak[9])):?>
		<?php $komponen = "evaluasi_pembelajaran" ?>
		<li <?php echo $component==$komponen?"class='current'":""?>>
				<a  <?php echo $component==$komponen?"class='with_sub open'":""?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Evaluasi Pembelajaran</a>
				<ul>
					<li><a href="<?php echo site_url('evaluasi/kkm/')?>"><span class="icon icon-list"></span><span class="link-text">KKM</span></a></li>
					<li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Penilaian Deskriptif</span></a>
						<ul>
							<li><a href="<?php echo site_url('evaluasi/nilai_konversi/')?>"><span class="icon icon-list"></span><span class="link-text">Nilai Konversi</span></a></li>
							<li><a href="<?php echo site_url('evaluasi/nilai_deskriptif/')?>"><span class="icon icon-list"></span><span class="link-text">Nilai Konversi Deskripsi</span></a></li>
						</ul>
					</li>
					<li><a href="<?php echo site_url('evaluasi/komponen_evaluasi/datalist')?>"><span class="icon icon-list"></span><span class="link-text">Komponen Penilaian</span></a></li>
					<li><a href="<?php echo site_url('evaluasi/ranking_siswa/datalist')?>"><span class="icon icon-list"></span><span class="link-text">Ranking Siswa</span></a></li>
					<li><a href="<?php echo site_url('evaluasi/evaluasi_kelas/datalist')?>"><span class="icon icon-list"></span><span class="link-text">Evaluasi Kelas</span></a></li>
					<li><a href="<?php echo site_url('evaluasi/detail_evaluasi_kelas/datalist')?>"><span class="icon icon-list"></span><span class="link-text">Detail Evaluasi Kelas</span></a></li>
					<?php if($id_kelas_wali!=0):?>
						<li><a href="<?php echo site_url('evaluasi/buku_rapor/')?>"><span class="icon icon-list"></span><span class="link-text">Buku Rapor</span></a></li>
						<li><a href="<?php echo site_url('evaluasi/ujian_sekolah/datalist/1')?>"><span class="icon icon-list"></span><span class="link-text">Ujian Sekolah</span></a></li>
						<li><a href="<?php echo site_url('evaluasi/ujian_sekolah/datalist/2')?>"><span class="icon icon-list"></span><span class="link-text">Ujian Nasional</span></a></li>					
						<li><a href="<?php echo site_url('evaluasi/hasil_un/datalist/')?>"><span class="icon icon-list"></span><span class="link-text">Rekap Hasil UN</span></a></li>
					<?php endif ?>
					<?php if($gid!=9):?>
						<li><a href="<?php echo site_url('evaluasi/rekap_kelulusan/')?>"><span class="icon icon-list"></span><span class="link-text">Rekap Lulusan</span></a></li>
						<!-- <li><a href="javascript:void(0)"><span class="icon icon-list"></span><span class="link-text">Transkrip Nilai dan Ijazah</span></a></li> -->
						<li><a href="<?php echo site_url('evaluasi/nilai_unas/')?>"><span class="icon icon-list"></span><span class="link-text">Nilai UN Terbaik</span></a></li>
					<?php endif ?>
				</ul>
		</li>
		<?php endif ?>
		<?php if(in_array($gid, $hak[10])):?>
		<?php $komponen = "komunikasi" ?>
		<?php $list_pihak_komunikasi = get_list("r_pihak_komunikasi", "id_pihak_komunikasi", "pihak_komunikasi", false) ?>
		<li <?php echo $component==$komponen?"class='current'":""?>>
				<a  <?php echo $component==$komponen?"class='with_sub open'":""?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Komunikasi Sosial Sekolah</a>
				<ul>
					<?php foreach($list_pihak_komunikasi as $key => $val):?>
						<li><a href="<?php echo site_url('komunikasi/datalist/'.$key)?>"><span class="icon icon-list"></span><span class="link-text"><?php echo $val ?></span></a></li>
					<?php endforeach ?>
				</ul>
		</li>
		<?php endif ?>
		
		<?php if($gid==10):?>
			<li <?php echo "class='current'"?>>
				<a  <?php echo "class='with_sub open'"?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Menu Utama</a>
				<ul>
					<li><a href="<?php echo site_url('pps/siswa/detail_siswa')?>"><span class="icon icon-list"></span><span class="link-text">Identitas</span></a></li>							
					<li><a href="<?php echo site_url('profil_siswa/mapel')?>"><span class="icon icon-list"></span><span class="link-text">Mata Pelajaran</span></a></li>							
					<li><a href="<?php echo site_url('profil_siswa/jadwal')?>"><span class="icon icon-list"></span><span class="link-text">Jadwal</span></a></li>							
					<li><a href="<?php echo site_url('profil_siswa/presensi')?>"><span class="icon icon-list"></span><span class="link-text">Rekap Kehadiran</span></a></li>							
					<li><a href="<?php echo site_url('profil_siswa/ekskul')?>"><span class="icon icon-list"></span><span class="link-text">Ekstrakurikuler</span></a></li>							
					<li><a href="<?php echo site_url('profil_siswa/pengembangan_diri')?>"><span class="icon icon-list"></span><span class="link-text">Pengembangan Diri</span></a></li>							
					<li><a href="<?php echo site_url('profil_siswa/prestasi')?>"><span class="icon icon-list"></span><span class="link-text">Prestasi</span></a></li>							
					<li><a href="<?php echo site_url('profil_siswa/catatan_bpbk')?>"><span class="icon icon-list"></span><span class="link-text">Catatan BP/BK</span></a></li>							
					<li><a href="<?php echo site_url('profil_siswa/riwayat_nilai')?>"><span class="icon icon-list"></span><span class="link-text">Riwayat Nilai</span></a></li>							
					<li><a href="<?php echo site_url('profil_siswa/buku_rapor')?>"><span class="icon icon-list"></span><span class="link-text">Buku Rapor</span></a></li>							
					<li><a href="<?php echo site_url('profil_siswa/ujian_sekolah')?>"><span class="icon icon-list"></span><span class="link-text">Nilai Ujian Sekolah</span></a></li>							
					<li><a href="<?php echo site_url('profil_siswa/ujian_nasional')?>"><span class="icon icon-list"></span><span class="link-text">Nilai Ujian Nasional</span></a></li>
					<li><a href="<?php echo site_url('profil_siswa/tunggakan_dspb')?>"><span class="icon icon-list"></span><span class="link-text">Tunggakan DSPB</span></a></li>
					<li><a href="<?php echo site_url('profil_siswa/tunggakan_dspt')?>"><span class="icon icon-list"></span><span class="link-text">Tunggakan DSPT</span></a></li>
					
					
				</ul>
			</li>
		<?php endif ?>
		
		<?php if($gid==1):?>
			<li><a href="#"><span class="icon icon-list"></span><span class="link-text">Struktur Kurikulum</span></a>
						<ul>
							<li><a href="<?php echo site_url('kurikulum/kompetensi_inti/')?>"><span class="icon icon-list"></span><span class="link-text">Kompetensi Inti</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/daftar_pelajaran/')?>"><span class="icon icon-list"></span><span class="link-text">Daftar Mata Pelajaran</span></a></li>
							<li><a href="<?php echo site_url('kurikulum/struktur_kurikulum/')?>"><span class="icon icon-list"></span><span class="link-text">Struktur</span></a></li>
							<?php if(get_jenjang_sekolah()==2){ ?>
								<li><a href="<?php echo site_url('kurikulum/pelajaran_tematik/')?>"><span class="icon icon-list"></span><span class="link-text">Pelajaran Tematik</span></a></li>
							<?php } ?>
						</ul>
					</li>
			<li><a href="#"><span class="icon icon-list"></span><span class="link-text">Data SD</span></a>
						<ul>
							<li><a href="<?php echo site_url('sistem/detail_siswa_SD/')?>"><span class="icon icon-list"></span><span class="link-text">Detail Siswa</span></a></li>
							<li><a href="<?php echo site_url('sistem/detail_sekolah_SD/')?>"><span class="icon icon-list"></span><span class="link-text">Detail Sekolah</span></a></li>
							<li><a href="<?php echo site_url('sistem/detail_guru_SD/')?>"><span class="icon icon-list"></span><span class="link-text">Detail Guru</span></a></li>
							<li><a href="<?php echo site_url('admin/jumlah_siswa_SD/')?>"><span class="icon icon-list"></span><span class="link-text">Jumlah Siswa</span></a></li>
							<li><a href="<?php echo site_url('admin/jumlah_sekolah_SD/')?>"><span class="icon icon-list"></span><span class="link-text">Jumlah Sekolah</span></a></li>
							<li><a href="<?php echo site_url('admin/jumlah_guru_SD/')?>"><span class="icon icon-list"></span><span class="link-text">Jumlah Guru</span></a></li>
							<li><a href="<?php echo site_url('admin/jumlah_rombel_SD/')?>"><span class="icon icon-list"></span><span class="link-text">Jumlah Rombongan Belajar</span></a></li>
						</ul>
					</li>
			<li><a href="#"><span class="icon icon-list"></span><span class="link-text">Data SMP</span></a>
						<ul>
							<li><a href="<?php echo site_url('sistem/detail_siswa_SMP/')?>"><span class="icon icon-list"></span><span class="link-text">Detail Siswa</span></a></li>
							<li><a href="<?php echo site_url('sistem/detail_sekolah_SMP/')?>"><span class="icon icon-list"></span><span class="link-text">Detail Sekolah</span></a></li>
							<li><a href="<?php echo site_url('sistem/detail_guru_SMP/')?>"><span class="icon icon-list"></span><span class="link-text">Detail Guru</span></a></li>
							<li><a href="<?php echo site_url('admin/jumlah_siswa_SMP/')?>"><span class="icon icon-list"></span><span class="link-text">Jumlah Siswa</span></a></li>
							<li><a href="<?php echo site_url('admin/jumlah_sekolah_SMP/')?>"><span class="icon icon-list"></span><span class="link-text">Jumlah Sekolah</span></a></li>
							<li><a href="<?php echo site_url('admin/jumlah_guru_SMP/')?>"><span class="icon icon-list"></span><span class="link-text">Jumlah Guru</span></a></li>
							<li><a href="<?php echo site_url('admin/jumlah_rombel_SMP/')?>"><span class="icon icon-list"></span><span class="link-text">Jumlah Rombongan Belajar</span></a></li>
						</ul>
					</li>
			<li><a href="#"><span class="icon icon-list"></span><span class="link-text">Data SMA</span></a>
						<ul>
							<li><a href="<?php echo site_url('sistem/detail_siswa_SMA/')?>"><span class="icon icon-list"></span><span class="link-text">Detail Siswa</span></a></li>
							<li><a href="<?php echo site_url('sistem/detail_sekolah_SMA/')?>"><span class="icon icon-list"></span><span class="link-text">Detail Sekolah</span></a></li>
							<li><a href="<?php echo site_url('sistem/detail_guru_SMA/')?>"><span class="icon icon-list"></span><span class="link-text">Detail Guru</span></a></li>
							<li><a href="<?php echo site_url('admin/jumlah_siswa_SMA/')?>"><span class="icon icon-list"></span><span class="link-text">Jumlah Siswa</span></a></li>
							<li><a href="<?php echo site_url('admin/jumlah_sekolah_SMA/')?>"><span class="icon icon-list"></span><span class="link-text">Jumlah Sekolah</span></a></li>
							<li><a href="<?php echo site_url('admin/jumlah_guru_SMA/')?>"><span class="icon icon-list"></span><span class="link-text">Jumlah Guru</span></a></li>
							<li><a href="<?php echo site_url('admin/jumlah_rombel_SMA/')?>"><span class="icon icon-list"></span><span class="link-text">Jumlah Rombongan Belajar</span></a></li>
						</ul>
					</li>
			<li <?php echo "class='current'"?>>
				<a  <?php echo "class='with_sub open'"?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>Administrator</a>
				<ul>
					<li><a href="<?php echo site_url('sekolah')?>"><span class="icon icon-list"></span><span class="link-text">Sekolah</span></a></li>							
					<li><a href="<?php echo site_url('sistem/change_sekolah')?>"><span class="icon icon-list"></span><span class="link-text">Sesi Sekolah</span></a></li>							
					<li><a href="<?php echo site_url('sistem/setting_sekolah')?>"><span class="icon icon-list"></span><span class="link-text">TA-Semester</span></a></li>							
				</ul>
			</li>
			
		<?php endif ?>
		<li <?php echo "class='current'"?>>
			<a  <?php echo "class='with_sub open'"?> href="javascript:void(0)"><img src="<?php echo site_url()?>theme/img/icons/packs/fugue/16x16/folder.png" alt="" height=16 width=16>UMKM SISWA</a>
			<ul>
				<li><a href="<?php echo site_url('umkm/umkm')?>"><span class="icon icon-list"></span><span class="link-text">Data UMKM</span></a></li>					
			</ul>
		</li>
	</ul></nav>
