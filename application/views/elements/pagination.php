<?php if($data_list["total_size"] > 0):?>
	<div class="footer">
        <div class="dataTables_info">Jumlah Data: <?php echo $data_list["total_size"] ?></div>
        <div class="dataTables_paginate paging_full_numbers">
            <a class="first paginate_button" href="javascript:changePage(1);" tabindex=0>Awal</a>
            <?php if($data_list["show_prev"]): ?>
                <a style="margin-left:-3px;" tabindex=0 class="previous paginate_button" href="javascript:changePage(<?php echo $data_list["prev_page"]?>);">Sebelumnya</a>
            <?php else: ?>
                <a style="margin-left:-3px;" class="previous paginate_button paginate_button_disabled">Sebelumnya</a>
            <?php endif ?>
            <span>
            <?php for($page=$data_list["first_page"];$page<=$data_list["last_page"];$page++): ?>
                <?php if($page == $data_list["current_page"]):?>
                    <a style="margin-left:-3px;" tabindex=0 class="paginate_active"><?php echo $page?></a>
                <?php else: ?>
                    <a style="margin-left:-3px;" tabindex=0 class="paginate_button" href="javascript:changePage(<?php echo $page?>)"><?php echo $page?></a>
                <?php endif ?>
            <?php endfor ?>
            </span>
            <?php if($data_list["show_next"]): ?>
                <a style="margin-left:-3px;" tabindex=0 class="next paginate_button" href="javascript:changePage(<?php echo $data_list["next_page"]?>);">Selanjutnya</a>
            <?php else: ?>
                <a style="margin-left:-3px;" tabindex=0 class="next paginate_button paginate_button_disabled">Selanjutnya</a>
            <?php endif ?>

            <a style="margin-left:-3px;" class="last paginate_button" href="javascript:changePage(<?php echo $data_list["total_page"]?>);">Akhir</a>
        </div>
    </div>
<?php else:?>
    <div class="footer">
        Data tidak ditemukan!
    </div>
<?php endif ?>
