	<div style="display: none;" id="changePassword" title="Perubahan Password Pengguna">
		<form action="<?php echo site_url('sistem/change_password')?>" class="full validate" method="POST">
            
            <?php echo simple_form_input(array('name'=>'username', 'disabled'=>'disabled', 'id'=>'username', 'label'=>'Username','value'=>get_username())) ?>
            <?php echo form_hidden('username',get_username()) ?>
            <?php echo simple_form_input(array('name'=>'password','data-error-type'=>'inline', 'class'=>'required', 'id'=>'password', 'label'=>'Password','type'=>'password')) ?>
            <?php echo simple_form_input(array('name'=>'password_confirm','data-error-type'=>'inline', 'class'=>'required', 'id'=>'password_confirm','type'=>'password', 'label'=>'Ulangi Password di Atas')) ?>	
		</form>
		<div class="actions">
			<div class="left">
				<button class="grey cancel">Batal</button>
			</div>
			<div class="right">
				<button class="submit">Simpan</button>
			</div>
		</div>
	</div>