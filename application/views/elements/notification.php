<?php $success=get_success_message() ?>
<?php $error=get_error_message()  ?>
<?php $warning=get_warning_message()  ?>
<?php if(!empty($success)) : ?>
    <div class="alert success">
		<span class="icon"></span><span class="close">x</span>
		<?php echo $success ?>
    </div>
<?php endif?>
<?php if(!empty($error)) : ?>
    <div class="alert error">
		<span class="icon"></span><span class="close">x</span>
		<?php echo $error ?>
    </div>
<?php endif?>
<?php if(!empty($warning)) : ?>
    <div class="alert warning">
        <span class="icon"></span><span class="close">x</span>
		<?php echo $warning ?>
    </div>
<?php endif?>
<?php $error_validation = validation_errors() ?>
<?php if(!empty($error_validation)) : ?>
    <div class="alert error">
        <span class="icon"></span><span class="close">x</span>
		<strong>Terjadi Error! </strong><br>
        <?php echo $error_validation ?>
    </div>
<?php endif?>