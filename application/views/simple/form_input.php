
<?php $myconf = $conf['state']=='add'?$conf['data_add']:$conf['data_edit'] ?>

<?php foreach( $myconf['field_list'] as $key):?>
	<?php $format = array() ?>
	<?php $type = isset($conf['data_type'][$key])?$conf['data_type'][$key]:"text" ?>
	<?php $dropdown = $type ?>
	<?php $type = is_array($type)?"dropdown":$type ?>
	<?php $format['name'] = "item[$key]" ?>
	<?php if(isset($conf['data_label'][$key])):?>
		<?php $format['label'] = $conf['data_label'][$key] ?>
	<?php endif ?>
	<?php $format['id'] = $key ?>
	<?php $format['class'] = "novalidate" ?>
	<?php if(in_array($key,$conf['data_mandatory'])):?>
		<?php $format['class'] = "required" ?>
	<?php endif ?>

	<?php switch($type): 
		case "text":?>
			<?php echo simple_form_input($format) ?>
			<?php break ?>
		<?php case "money":?>
			<?php echo simple_form_input($format) ?>
			<?php break ?>
		<?php case "textarea":?>
			<?php echo simple_form_textarea($format) ?>
			<?php break ?>
		<?php case "date":?>
			<?php $format['type'] = "date" ?>
			<?php echo simple_form_input($format) ?>
			<?php break ?>
		<?php case "file":?>
			<?php $format['type'] = "file" ?>
			<?php echo simple_form_input($format) ?>
			<?php break ?>
		<?php case "dropdown":?>
			<?php unset($format['name']) ?>
			<?php if(count($dropdown)>4 && !isset($dropdown['list'])):?>
				<?php $list_dropdown = get_list_filter($dropdown[0],$dropdown[1],array($dropdown[2]),array($dropdown[3]=>$dropdown[4])) ?>
			<?php elseif(!isset($dropdown['list'])):?>
				<?php $list_dropdown = get_list($dropdown[0],$dropdown[1],$dropdown[2]) ?>
			<?php elseif(isset($dropdown['list'])):?>
				<?php $list_dropdown = $dropdown['list'] ?>
			<?php endif ?>
			<?php echo simple_form_dropdown("item[".$key."]",$list_dropdown,null,"",$format) ?>
			<?php break ?>
		<?php case "hidden":?>
			<?php $format['type'] = "hidden" ?>
			<?php echo simple_form_hidden($format) ?>
			<?php break ?>
		<?php case "wysiwyg":?>
			<?php echo simple_form_wysiwyg($format) ?>
			<?php break ?>
		<?php case "label":?>
			<fieldset>
				<legend><?php echo $format['label'] ?></legend>
			</fieldset>
			<?php break ?>
		<?php case "nested":?>
			<!--?php unset($format['name']) ?>
			<!--?php $format_p = $format ?>
			<!--?php $format_p['label'] = $conf['data_label'][$key."_parent"];
			
			<!--?php echo form_dropdown("item[".$key."_parent]",$list_parent,null," id='pil_$key' onChange=show_$key(this.value)",$format_p) ?>			
			<!--?php foreach($list_parent as $id=>$det):?>
				<!--?php $list_child = array("0"=>"-Pilih-")+get_list_nomor_or($id) ?>
				<!--div id='nomor<!--?php echo $id?>' <!--?php echo $cabor!=$id?"style='display:none'":""?> class='listnomor'>
					<!--?php $nmr = $cabor==$id? $nomor_or:0?>
					<!--?php echo form_dropdown("id_nomor_or[$id]",$list_nomor,$nmr," id='pil_nomor".$id."' onChange=changeNomor(this.value) ",array('label'=>'Nomor Pertandingan')) ?>
				<!--/div>	
			<!--?php endforeach ?>
			
			<!--?php $format['type'] = "hidden" ?>
			<!--?php echo form_input($format) ?>
			<!--?php break ?>
	<?php endswitch ?>
<?php endforeach ?>
<?php echo form_hidden('item['.$myconf['id_model']."]",isset($_POST['item'][$myconf['id_model']])?$_POST['item'][$myconf['id_model']]:"") ?>
 