    <?php $myconf= $conf['data_add']?>
	<?php $ca = !empty($myconf["custom_action"])?$myconf["custom_action"]:""?>
    <h1 class="grid_12"><?php echo $myconf['title']?></h1>
    <form action="<?php echo $ca ?>" class="grid_12 validate" novalidate="novalidate" method="post" id="valid" enctype="multipart/form-data">
	    <fieldset>
	        <legend><?php echo empty($myconf['subtitle'])?"Penambahan Data ".$myconf['title']:$myconf['subtitle'] ?></legend>
            <?php $this->load->view($conf['data_add']['view_file_form_input']) ?>
        </fieldset>
        <div class="actions">
            <div class="left">
                <input type="submit" value="Simpan" />
				<?php $page = cek_page($conf['name'].'.datatable'); ?>
                <a href="<?php echo site_url($conf['data_add']['redirect_link']."/".$page)?>"> <input type="button" value="Batal" /></a>
            </div>
	    </div>
    </form>

 
 