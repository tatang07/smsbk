    $(document).ready(function() {
        setInterval('updateClock()', 1000);
		$('#pengecekan').val("");
		$('#pengecekan').focus();
    });    
	
    function find(){
        var url_req = $('#datatable').attr('url')+'/1';
		$.ajax({
			type:'post',
			data:$('#formPencarian').serialize(),
			url:url_req,
			success:function(response){
				$('#datatable').html(response);
                
			},
			error:function(){
			},
			dataType:'html'
		});
        return false;
	}
	
	function changePage(page){
        var url_req = $('#datatable').attr('url')+'/'+page;
        
		$.ajax({
			type:'post',
			data:$('#formPencarian').serialize(),
			url:url_req,
			success:function(response){
				$('#datatable').html(response);
			},
			error:function(){
				
			},
			dataType:'html'
		});
	}
    
    function openPopup(idName) {
        idPopup = "#"+idName;
		$(idPopup).dialog({
			autoOpen: false,
			modal: true,
			width: 400,
			open: function(){ $(this).parent().css('overflow', 'visible'); $$.utils.forms.resize() }
		}).find('button.submit').click(function(){
			var $el = $(this).parents('.ui-dialog-content');
			if ($el.validate().form()) {
				$el.find('form')[0].submit();
				$el.dialog('close');
			}
		}).end().find('button.cancel').click(function(){
			var $el = $(this).parents('.ui-dialog-content');
			$el.find('form')[0].reset();
			$el.dialog('close');;
		});
        
		$( idPopup ).dialog( "open" );
	};
	
    function loadHalaman(target_container2,alamat){
		$.ajax({
			type:'post',
			url:alamat,
			success:function(response){
				$('#'+target_container2).html(response);
                
			},
			error:function(){
			},
			dataType:'html'
		});
	}
	
    function updateClock ( )    {
        var currentTime = new Date ( );
        var currentHours = currentTime.getHours ( );
        var currentMinutes = currentTime.getMinutes ( );
        var currentSeconds = currentTime.getSeconds ( );

        // Pad the minutes and seconds with leading zeros, if required
        currentMinutes = ( currentMinutes < 10 ? "0" : "" ) + currentMinutes;
        currentSeconds = ( currentSeconds < 10 ? "0" : "" ) + currentSeconds;

        // Choose either "AM" or "PM" as appropriate
        var timeOfDay = ( currentHours < 12 ) ? "AM" : "PM";

        // Convert the hours component to 12-hour format if needed
        currentHours = ( currentHours > 12 ) ? currentHours - 12 : currentHours;

        // Convert an hours component of "0" to "12"
        currentHours = ( currentHours == 0 ) ? 12 : currentHours;

        // Compose the string for display
        var currentTimeString = currentHours + ":" + currentMinutes + ":" + currentSeconds + " " + timeOfDay;
        $("#sysclock").html(currentTimeString);   
    }
    
    function addLainnya(objek){
        idName = objek.getAttribute("id");
        val = objek.value;
        if(val==0){
            new_val = prompt("Silahkan masukan nama pekerjaan lainnya","");
            $('#'+idName).append(new Option("nama","-"));
        }
    }
    
    function showElement(idName,val){
        if(val=='Lainnya'){
			labelForInstansiKerja(0);
            $('#'+idName).show();
        }else if(val=='Pelajar/Mahasiswa'){
			labelForInstansiKerja(1);
			$('#'+idName).hide();
		}else{
			labelForInstansiKerja(0);
            $('#'+idName).hide();
        }
    }
	
	function labelForInstansiKerja(flag){
		if(flag==1){
			$('#tpt_kerja').html('Sekolah');
			$('#telp_kerja').html('Sekolah');
			$('#alamat_kerja').html('Sekolah');
		}else{
			$('#tpt_kerja').html('Tempat Kerja');
			$('#telp_kerja').html('Tempat Kerja');
			$('#alamat_kerja').html('Tempat Kerja');
		}
	
	}
	
	function showHiddenElement(idName,val,trigger){
        if(val==trigger){
            $('#'+idName).show();
        }else{
            $('#'+idName).hide();
        }
    }
	
	function submitform(idForm,destination){
		$('#'+idForm).attr('action', destination);
		$('#'+idForm).submit();
	}	
	
	function postingform(idForm,destination){
		var tempF = $('#'+idForm).attr('onsubmit');
		var dest = $('#'+idForm).attr('action');
		$('#'+idForm).unbind('submit');
		$('#'+idForm).attr('target', '_self');
		$('#'+idForm).removeAttr('onsubmit');
		
		$('#'+idForm).attr('action', destination);
		$('#'+idForm).attr('target', '_blank');
		$('#'+idForm).submit();	
		$('#'+idForm).attr('action', dest);
		$('#'+idForm).submit(function(e){
			return find();
		});	
				
	}
	
	function sortby(field){
		var url_req = $('#datatable').attr('url')+'/1';
		$('#sorting').val(field);
		var jdata = $('#formPencarian').serialize();
		$.ajax({
			type:'post',
			data:jdata,
			url:url_req,
			success:function(response){
				$('#datatable').html(response);
			},
			error:function(){
				
			},
			dataType:'html'
		});
	}
	
	function submitForm(id){
		$('#'+id).submit();
	}
	
	

