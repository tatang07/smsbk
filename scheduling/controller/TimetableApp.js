/* global hasOwnProperty */
/* global angular */
var timetableApp = angular.module("TimetableApp", ["ngSanitize", "ngCsv"]);

timetableApp.controller("TimeTableCont", ["$scope", "$filter", function($scope, $filter){
    var orderBy = $filter("orderBy"),
        data_kosong = {
            kosong : "kosong"
        },
        ruangan = {},
        temp_obj_selected = [],
        data_temp;
        
    // if(localStorage.data && !confirm("Tekan \"OK\" untuk melanjutkan data lama.\nTekan \"CANCEL\" untuk membuat data baru"))
    //     delete localStorage["data"];

//pengaturan default sistem
    $scope.SettingDefault = {
        showMapel : false,
        showRuang : false,
        durasi_jam_sd : 35,
        durasi_jam_smp : 40,
        durasi_jam_sma : 45,
        modal_tambah_jam : false,
        modal_update_data : false,
        hari : ["SENIN", "SELASA", "RABU", "KAMIS", "JUMAT", "SABTU"]
    };

    $scope.jurusan_aktif = "all";
    
    $scope.opt_update = "deleteAll";

    $scope.select = {};

    $scope.alert = {
            text : "",
            show : false
        };

    function ColorLuminance(hex, lum) {
      hex = String(hex).replace(/[^0-9a-f]/gi, '');
      if (hex.length < 6) {
        hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
      }
      lum = lum || 0;

      var rgb = "#", c, i;
      for (i = 0; i < 3; i++) {
        c = parseInt(hex.substr(i*2,2), 16);
        c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
        rgb += ("00"+c).substr(c.length);
      }
      return rgb;
    }

    function getRandomColor() {
         return '#'+(Math.random()*0xFFFFFF<<0).toString(16);
    }

    function resetMapelShow(){
        angular.forEach($scope.DataTabel.mapel, function(a){
            angular.forEach(a.mapel, function(b){
                b.show = false;
            });
        });
    }

    function resetGuruSelect(){
        angular.forEach($scope.DataTabel.mapel, function(a) {
            angular.forEach(a.mapel, function(b){
                angular.forEach(b.guru, function(OBJ){
                    OBJ.selected = false;
                });
            });
        });
    }

    function resetRuangSelect(){
        angular.forEach($scope.DataTabel.ruang, function(a){
            a.selected = false;
        });
    }

    if(localStorage.data)
    {
        BacaData();

        resetRuangSelect();
        resetGuruSelect();
        resetMapelShow();
    }
    
    // optimasi baca data jadwal (ketika pertama kali membuka aplikasi)
    function BacaData(data){
        var jadwal = [],
            key,
            obj = new Object(),
            interv;
        
        data_temp = JSON.parse(data || localStorage.data);
        key = Object.keys(data_temp.kelas);
        
        for(var i = 0; i < key.length; i++)
        {
            var data_kelas = data_temp.kelas[key[i]].data;
            for(var j = 0; j < data_kelas.length; j++)
            {
                jadwal.push(data_kelas[j].jadwal);
                data_kelas[j].jadwal = [];
            }
            obj[key[i]] = jadwal;
            jadwal = [];
        }
        
        $scope.DataTabel = data_temp;
        
        var iterasi = 0,
            isPaused = false;
            
        interv = setInterval(function(){
            var data_jadwal = $scope.DataTabel.kelas[key[iterasi]].data;
            
            if(!isPaused)
            {
                isPaused = true;
                
                for(var i=0; i<data_jadwal.length; i++)
                {
                    
                    $scope.DataTabel.kelas[key[iterasi]].data[i].jadwal = obj[key[iterasi]][i];
                }
                if(i >= data_jadwal.length)
                    isPaused = false;
            }
                        
            if(iterasi++ >= key.length-1)
            {
                clearInterval(interv);
                
                var el = document.getElementById("mpl");
                setTimeout(function(){
                    el.click();
                }, 1000);
            }
        }, 500);
    };

    $scope.importData = function(){
        var el = document.getElementById("importData");
        el.click();
    };
    
//    kelas order
    $scope.kelasOrder = function(){
        var orderKls = [];
        
        if(!$scope.DataTabel)
            return false;
        
        for(var key in $scope.DataTabel.kelas)
            orderKls.push(key);
        
        return orderKls;
    };
    
    $scope.lengthGuru = function(obj){
        var i = obj.length,
            length = i;
        while(i--)
        {
            if(Array.isArray(obj[i]))
                length--;
            
        }
        return length;
    };

    $scope.ReadImport = function(content){
//        show modal update data
//        if(localStorage.data)
//        {
//            $scope.SettingDefault.modal_update_data = true;
//        }
            
        var key = ["kelas", "guru", "mapel"];
        try
        {
            $scope.DataTabel = JSON.parse(content);
            
            if($scope.DataTabel.ruang <= 0)
               delete $scope.DataTabel["ruang"]; 
            
            for (var i = 0; i < key.length; i++)
            {
                if (!$scope.DataTabel.hasOwnProperty(key[i]))
                {
                    $scope.alert.text = "Maaf, file yang anda masukkan tidak valid!";
                    $scope.alert.show = true;
                    $scope.DataTabel = undefined;
                    return;
                }
            }
            localStorage.data = JSON.stringify($scope.DataTabel);
            resetRuangSelect();
            resetGuruSelect();
            resetMapelShow();
            $scope.FixTable();
        }
        catch(e)
        {
            $scope.alert.text = "Maaf, file yang anda masukkan salah!";
            $scope.alert.show = true;
        }
    };

    $scope.csvData = function(){
        var data = $scope.DataTabel.kelas,
            arr = [];

        for(var k in data)
        {
            var dataObj = data[k].data,
                kelas = data[k].kelas;

            for(var i=0; i<dataObj.length; i++)
            {
                var hari = dataObj[i].hari,
                    jam = dataObj[i].jam,
                    jadwal = dataObj[i].jadwal;
                
                for(var j=0; j<jadwal.length; j++)
                {
                    for(var n=0; n<jadwal[j].length; n++)
                    {
                        if(typeof jadwal[j][n].mapel !== "undefined")
                        {
                            var jam_ke = j + 1,
                                csvObj = {};

                            csvObj.a = hari;
                            csvObj.b = $filter('date')(jam[j].split(":")[0], "HH:mm");
                            csvObj.c = $filter('date')(jam[j].split(":")[1], "HH:mm");
                            csvObj.d = jam_ke;
                            csvObj.e = (typeof jadwal[j][n].ruang === "undefined") ? kelas[n].kelas : jadwal[j][n].ruang;
                            csvObj.f = jadwal[j][n].kode_guru;
                            csvObj.g = kelas[n].kelas;
                            csvObj.h = jadwal[j][n].kode_mapel;
                            
                            arr.push(csvObj);
                        }
                    }
                }
            }
        }

        return $filter('orderBy')(arr, function(a){
            return $scope.SettingDefault.hari.indexOf(a.a);
        });
    };

    function isEmpty(obj){
        if (obj == null) return true;

        if (obj.length > 0)    return false;
        if (obj.length === 0)  return true;

        for (var key in obj) {
            if (hasOwnProperty.call(obj, key)) return false;
        }

        return true;
    }

    $scope.ruang_mapel_show = function(what)
    {
        if(what == "ruang")
        {
            $scope.SettingDefault.showMapel = false;
            $scope.SettingDefault.showRuang = !$scope.SettingDefault.showRuang;
        }
        else if(what == "mapel")
        {
            $scope.SettingDefault.showRuang = false;
            $scope.SettingDefault.showMapel = !$scope.SettingDefault.showMapel;
        }
    };

    //menampilkan tabel kelas (start)
    $scope.aktifKelas = function(obj, b, c){
        angular.forEach(obj, function(a) {
                a.show = false;
        });
        b.show = true;

        $scope.jurusan_aktif = c;
        
        resetRuangSelect();
        resetGuruSelect();
        $scope.select = {};
    };

    $scope.match_guru_kelas = function(kelas){
        if(typeof $scope.guru_kelas === "object")
        {
            for(var i=0; i<$scope.guru_kelas.length; i++)
            {
                if(kelas === $scope.guru_kelas[i])
                    return true;
            }
        }

        return kelas === $scope.guru_kelas;
    };

    //show/hide td matrix berdasarkan peminatan/jurusan (start)
    $scope.tampilkanJurusan = function(peminatan, kls){
        var kelas = !1,
            jurusan = ($scope.jurusan_aktif === "all" || peminatan === "all") ? true : ($scope.jurusan_aktif == peminatan),
            data = $scope.DataTabel.kelas;
        
        for(var k in data)
        {
            if(data[k].show)
            {
                if(typeof kls !== "object")
                {
                    if(kls === "all")
                        kelas = true;
                    else
                        kelas = (kls === k);
                }
                else
                {
                    for(var i=0; i<kls.length; i++)
                    {
                        if(kls[i] === k)
                        {
                            kelas = true;
                            break;
                        }
                        else
                            kelas = false;
                    }
                }
                break;
            }
        }

        return (kelas === true && jurusan === true);
    };

    //pengelompokan kelas berdasar jurusan (start)
    $scope.KelasJurusan = function(kls){
        var data = $scope.DataTabel.kelas,
            arr = [];

        for(var i=0; i<data[kls].kelas.length; i++)
        {
            if(arr.indexOf(data[kls].kelas[i].peminatan) < 0)
                arr.push(data[kls].kelas[i].peminatan);
        }
        return arr;
    };

    //Dropdown Kelas (start)
    $scope.DropdownKelas = function(obj){
        angular.forEach($scope.DataTabel.kelas, function(a){
            if(a !== obj)
                a.expandKelas = false;
        });

        obj.expandKelas = !obj.expandKelas;
    };

    // form control tambah jam start
    $scope.TambahJam = function(){
        $scope.SettingDefault.modal_tambah_jam = true;
        $scope.form_hari = $scope.SettingDefault.hari[0];
        $scope.changeHari();
    };
    
    $scope.changeHari = function(){
        $scope.form_durasi = getDurationTime();
        $scope.form_event = "";

        var data = $scope.DataTabel.kelas,
            hari = $scope.form_hari,
            jam = 0,
            date = new Date(2015, 0, 1);

        for(var k in data)
        {
            var dataJadwal = data[k].data;
            for(var i=0; i<dataJadwal.length; i++)
            {
                if(dataJadwal[i].hari === hari)
                {
                    var dataJam = dataJadwal[i].jam.length - 1,
                        jam_end = new Date();
                        date.setHours(7);
                        date.setMinutes(0);

                    if(dataJam<0)
                        jam_end = date;
                    else
                        jam_end = new Date(parseInt(dataJadwal[i].jam[dataJam].split(":")[1]));

                    jam = $filter('date')(jam_end, "HH:mm");

                    i = dataJadwal.length; // keluar dari pengulangan
                }
            }
            break;
        }
        $scope.form_jam = jam;
    };

    function switch_romawi(val){
        switch(val)
        {
            case "I" : 
                return 1;
            case "V" :
                return 5;
            case "X" :
                return 10;
        }
    }

    function Rom2Dec(n){
        var dec = 0,
            temp_rom = 0,
            pjg = n.length -1;
        for(var i=pjg; i>=0; i--)
        {
            if(switch_romawi(n[i]) < dec && n[i] !== temp_rom)
            {
                dec -= switch_romawi(n[i]);
                temp_rom = n[i];
            }
            else
            {
                dec += switch_romawi(n[i]);
                temp_rom = n[i];
            }
        }
        return dec;
    }

    $scope.jam_valid = function(){
        if(/^(([0-1][0-9])|(2[0-3])):[0-5][0-9]$/.test($scope.form_jam))
        {
            return false;
        }
        return true;
    };

    $scope.durasi_valid = function(){
        var dur = parseInt($scope.form_durasi);
        if(!isNaN(dur) && dur < 1)
            $scope.form_durasi = 15;
    };

    function getDurationTime(){
        var dataObj = $scope.DataTabel.kelas;
        for(var kelas in dataObj)
        {
            if(Rom2Dec(kelas) <= 6)
                return $scope.SettingDefault.durasi_jam_sd;
            else if(Rom2Dec(kelas) <= 9)
                return $scope.SettingDefault.durasi_jam_smp;
            else
                return $scope.SettingDefault.durasi_jam_sma;
        }
    }

    $scope.SubmitJam = function(){
        var data = $scope.DataTabel.kelas,
            jam_final,
            jam_1,
            jam_2,
            hari = $scope.form_hari,
            date = new Date(2015, 0, 1),
            jam = $scope.form_jam.match(/(\d+)(?::(\d\d))?\s*(p?)/),
            durasi = parseInt($scope.form_durasi) || getDurationTime(),
            ev = $scope.form_event,
            obj = data_kosong,
            arr = [];

            date.setHours(jam[1]);
            date.setMinutes(jam[2] || 0);

            jam_1 = date.getTime();

            date.setMinutes((jam[2]) ? (parseInt(jam[2]) + durasi) : durasi);

            jam_2 = date.getTime();

            jam_final = jam_1 + ":" + jam_2;

            for(var k in data)
            {
                var dataJadwal = data[k].data,
                    kelas = data[k].kelas;

                for(var i=0; i<dataJadwal.length; i++)
                {
                    if(dataJadwal[i].hari === hari)
                    {
                        var label = dataJadwal[i].label.length,
                            pengurang = dataJadwal[i].pengurang;

                        if(ev.length>0)
                        {
                            pengurang--;
                            dataJadwal[i].pengurang = pengurang;
                            dataJadwal[i].label.push("EV");
                        }
                        else
                            dataJadwal[i].label.push(label+pengurang);

                        dataJadwal[i].jam.push(jam_final);

                        for(var j=0; j<kelas.length; j++)
                        {
                            if(ev.length > 0)
                            {
                                obj = {
                                    kegiatan : ev,
                                    bg_keg : "gray",
                                    waktu : jam
                                };
                            }

                            arr.push(obj);
                        }
                        dataJadwal[i].jadwal.push(arr);
                        arr = [];

                        i = dataJadwal.length; // keluar dari pengulangan
                    }
                }
            }

            $scope.changeHari();
    };

    $scope.jamJadwal = function(jam_ke) {
        var jam_1 = $filter('date')(jam_ke.split(":")[0], "HH:mm"),
            jam_2 = $filter('date')(jam_ke.split(":")[1], "HH:mm");

            return (jam_1 + " - " + jam_2);
    };

    $scope.NewDocument = function(){
        
        if(!confirm("Data penjadwalan akan dihapus\nTetap ingin melanjutkan?"))
            return false;
        
        for(var key in $scope.DataTabel.kelas)
        {
            $scope.DataTabel.kelas[key].data = [];
        }
        $scope.FixTable();
    };

    $scope.saveFile = function(){
        var MIME_TYPE = 'text/plain';
        window.URL = window.URL || window.webkitURL;
        var text = angular.toJson($scope.DataTabel),
            filename = "SMSBK_" + $filter('date')(Date.now(), "d/M/yyyy HH:mm"),
            bb = new Blob([text], {
                type: MIME_TYPE
            }),
            el = document.getElementById("save");

            el.href = window.URL.createObjectURL(bb);
            el.download = filename + ".smsbk";
    };

    $scope.FixTable = function(){
        if(isEmpty($scope.DataTabel))
            return;

        var dataKelas = $scope.DataTabel.kelas,
            dataGuru = $scope.DataTabel.guru,
            dataMapel = $scope.DataTabel.mapel,
            dataRuang = $scope.DataTabel.ruang,
            hari = $scope.SettingDefault.hari,
            key_object = Object.keys(dataKelas),
            iterasi = key_object.length;
        
        while(iterasi--)
        {
            dataKelas[key_object[iterasi]].show = iterasi > 0 ? false : true;
            
            for(var i=0; i<hari.length; i++)
            {
                var data = dataKelas[key_object[iterasi]].data;
                
                if("undefined" === typeof data[i])
                    data.push({
                        "hari" : hari[i],
                        "show" : true,
                        "jam" : [],
                        "label" : [],
                        "pengurang" : 1,
                        "jadwal" : []
                    });
            }
        }

        if(dataRuang)
        {
            for(var i=0; i<dataRuang.length; i++)
            {
                if(typeof dataRuang[i].bg_ruang === "undefined")
                    dataRuang[i].bg_ruang = ColorLuminance(getRandomColor(), -0.5);
            }
        }

        for(var i=0; i<dataGuru.length; i++)
        {
            if(typeof dataGuru[i].bg === "undefined")
                dataGuru[i].bg = ColorLuminance(getRandomColor(), -0.2);
        }

        for(var i=0; i<dataMapel.length; i++)
        {
            for(var j=0; j<dataMapel[i].mapel.length; j++)
            {
                if(typeof dataMapel[i].mapel[j].bgcolor === "undefined")
                    dataMapel[i].mapel[j].bgcolor = ColorLuminance(getRandomColor(), -0.1);
            }
        }
    };

    function UpdateJamNgajar(nama, jam) {
        if(isEmpty($scope.DataTabel))
            return;

        var DataGuru = $scope.DataTabel.guru,
            i = 0;
        DataGuru = orderBy(DataGuru, "nama");
        
        while(i < DataGuru.length)
        {
            var index = nama.indexOf(DataGuru[i].nama);
            if(index > -1)
                DataGuru[i].jam_ngajar = jam[index + 1];
            else
                DataGuru[i].jam_ngajar = 0;
            i++;
        }
    }

    function HitungJam(guru_temp){
        var nm_guru, ngajar_times = 0, nama = [], jam = [];

        guru_temp.sort();
        for (var i = 0; i < guru_temp.length; i++) {
            if (guru_temp[i] != nm_guru) {
                if (ngajar_times > 0)
                    nama.push(nm_guru);
                    jam.push(ngajar_times);
                nm_guru = guru_temp[i];
                ngajar_times = 1;
            }
            else
                ngajar_times++;
        }
        
        if (ngajar_times > 0)
        {
            nama.push(nm_guru);
            jam.push(ngajar_times);
        }

        UpdateJamNgajar(nama, jam);
    }

    $scope.$watch("DataTabel.kelas", function(newVal, oldVal){
        var guru_temp = [];
        
        for(var key in newVal){
            for(var i=0; i<newVal[key].data.length; i++){
                var dataJadwalKelas = newVal[key].data[i];
                for(var j = 0, baris = dataJadwalKelas.jadwal; j<baris.length; j++) {
                    for(var n=0, kolom=baris[j]; n<kolom.length; n++){
                        if(kolom[n].guru !== "" && "undefined" !== typeof kolom[n].guru)
                            guru_temp.push(kolom[n].guru);
                    }
                }
            }
        }

        HitungJam(guru_temp);
        
        if($scope.DataTabel)
            localStorage.data = JSON.stringify($scope.DataTabel);

    }, true);

    function GuruExist(obj, DataKelas, y, x, kls){
        var data = $scope.DataTabel.kelas;
        for(var key in data)
        {
            var data_kelas = data[key].data;
            for(var i=0; i<data_kelas.length; i++)
            {
                if(data_kelas[i].hari === obj.hari && typeof data_kelas[i].jadwal[y] !== "undefined")
                {
                    for(var j=0; j<data_kelas[i].jadwal[y].length; j++)
                    {
                        if(data[key].kelas[j] !== data[kls].kelas[x] && data_kelas[i].jadwal[y][j].guru === DataKelas.guru)
                            return "Jadwal Guru <strong>" + DataKelas.guru + "</strong> bentrok dengan kelas <strong>" + data[key].kelas[j].kelas + "</strong>";

                        if(data[key].kelas[j] !== data[kls].kelas[x] && data_kelas[i].jadwal[y][j].ruang === DataKelas.ruang)
                            return "Ruangan <strong>" + DataKelas.ruang + "</strong> sudah terjadwal pada kelas <strong>" + data[key].kelas[j].kelas + "</strong>";
                    }

                    i = data_kelas.length; // keluar dari pengulangan
                }
            }
        }
        return false;
    }

    function MapelExist(kls, mapel, y, x){
        var data = $scope.DataTabel.kelas[kls],
            count = 0;

        for (var i=0; i<data.data.length; i++) {
            for(var j=0, kelas = i; j<data.data[i].jadwal.length; j++)
            {
                if("undefined" === typeof data.data[i].jadwal[j][x].mapel)
                    continue;

                if(data.data[i].jadwal[j][x].mapel === mapel)
                    count++;
            }
        }

        return [count >= $scope.select.jam_minggu ? true : false, data.kelas[x]];
    }

    function updateLabel(hari){
        var data = $scope.DataTabel.kelas;

        for(var k in data)
        {
            var dataJadwal = data[k].data;

            for(var i=0; i<dataJadwal.length; i++)
            {
                if(dataJadwal[i].hari === hari)
                {
                    var gb = 1;
                    for(var j=0; j<dataJadwal[i].label.length; j++)
                    {
                        if(dataJadwal[i].label[j]=="EV")
                        {
                            gb--;
                            dataJadwal[i].label[j] = "EV";
                        }
                        else
                            dataJadwal[i].label[j] = j + gb;
                    }
                    dataJadwal[i].pengurang = gb;

                    i = dataJadwal.length; // keluar dari pengulangan
                }
            }
        }
    }

    $scope.hapusJam = function(hari, id){
        var data = $scope.DataTabel.kelas;

        if(!confirm("Tetap ingin menghapus data?"))
            return;

        for(var k in data)
        {
            var dataJadwal = data[k].data;

            for(var i=0; i<dataJadwal.length; i++)
            {
                if(dataJadwal[i].hari === hari)
                {
                    var jam = dataJadwal[i].jam.length - 1;

                    dataJadwal[i].jadwal.splice(id, 1);
                    dataJadwal[i].jam.splice(jam, 1);
                    dataJadwal[i].label.splice(id, 1);
                    updateLabel(hari);

                    i = dataJadwal.length; // keluar dari pengulangan
                }
            }
        }
    };

    $scope.hapus = function(obj, y, x){
        if(typeof obj.jadwal[y][x].kegiatan !== "undefined")
        {
            var data_kelas = $scope.DataTabel.kelas;

            if(!confirm("Tetap ingin menghapus event " + obj.jadwal[y][x].kegiatan + "?"))
                return;

            for(var key in data_kelas)
            {
                var kelas = data_kelas[key].kelas,
                    data = data_kelas[key].data;
                
                for(var i=0; i<data.length; i++)
                {
                    if(data[i].hari === obj.hari)
                    {
                        for(var j=0; j<kelas.length; j++)
                        {
                            if(typeof data[i].jadwal[y][j].mapel !== "undefined")
                                continue;
                            data[i].jadwal[y][j] = data_kosong;
                        }

                        data[i].label[y] = 0;
                        updateLabel(obj.hari);

                        i = data.length; // keluar dari pengulangan
                    }
                }
            }
        }
        else
            obj.jadwal[y][x] = data_kosong;
    };

    $scope.evStatus = function(obj, id){
        return obj.label[id];       
    };

    $scope.insert = function(obj, y, x, kls){
        var select = $scope.select,
            alert = $scope.alert;

            if(typeof obj.jadwal[y][x].kegiatan !== "undefined")
                return;

            if(typeof select.mapel === "undefined" && typeof select.kegiatan === "undefined")
            {
                alert.text = "Pilih salah satu mata pelajaran";
                alert.show = true;
                return;
            }

            var CekGuruDanRuang = "undefined" !== typeof select.guru ? GuruExist(obj, select, y, x, kls) : false,
                CekMapel = obj.jadwal[y][x].mapel === select.mapel ? false : MapelExist(kls, select.mapel, y, x);

            if(CekGuruDanRuang)
            {
                alert.text = CekGuruDanRuang;
                alert.show = true;
                return;
            }
            if(CekMapel[0])
            {
                alert.text = "Mata Pelajaran <strong>" + select.mapel + "</strong> sudah " + select.jam_minggu + " jam untuk kelas <strong>" + CekMapel[1].kelas + "</strong>";
                alert.show = true;
                return;
            }
            
            obj.jadwal[y][x] = select;

            $scope.selected(temp_obj_selected[0], temp_obj_selected[1]);

            alert.show = false;
    };

    $scope.tampilkan = function(obj, obj_show, tabel) {
        if(!tabel)
            angular.forEach(obj, function(a, b) {
                if(a !== obj_show)
                    a.show = false;
            });

        obj_show.show = !obj_show.show;
    };

    $scope.ruanganAktif = function(aktif){
        if(typeof ruangan.kode === "undefined")
        {
            $scope.select.ruang = aktif;
            $scope.select.bg_ruang = "apaaja";
        }
        else
        {
            $scope.select.ruang = ruangan.kode;
            $scope.select.bg_ruang = ruangan.bg_ruang;
        }
    };

    $scope.selectedRuang = function(obj)
    {
        resetRuangSelect();
        
        if(ruangan.kode === obj.kode)
            ruangan = {};
        else
        {
            obj.selected = true;
            ruangan.kode = obj.kode;
            ruangan.bg_ruang = obj.bg_ruang;
        }
    };

    $scope.selected = function(obj, index) {
        var select = new Object();

        temp_obj_selected[0] = obj;
        temp_obj_selected[1] = index;

        resetGuruSelect();

        $scope.guru_kelas = obj.guru[index].kelas;

        obj.guru[index].selected = true;
        select.mapel = obj.mapel;
        select.kode_mapel = obj.kode;
        select.guru = obj.guru[index].guru;
        select.bg_mp = obj.bgcolor;
        select.jam_minggu = obj.jam_minggu;
        angular.forEach($scope.DataTabel.guru, function(a){
            if(a.nama === obj.guru[index].guru)
            {
                select.kode_guru = a.kode;
                select.bg_gr = a.bg;
            }
        });

        $scope.select = select;
    };

    $scope.TotalJamGuru = function(id) {
        for (var i = 0; i < $scope.DataTabel.guru.length; i++) {
            if ($scope.DataTabel.guru[i]["nama"] === id)
                return $scope.DataTabel.guru[i]["jam_ngajar"];
        }
    };
    
    $scope.kodeGuru = function(id) {
        var i = 0,
            guru = $scope.DataTabel.guru;
        if("undefined" === typeof id)
            return;
            
        while(i < guru.length)
        {
            if(guru[i].nama.indexOf(id) > -1)
                return guru[i].kode;            
            i++;
        }
    };
}]);

timetableApp.directive('ngHapus', function($parse) {
    return function(scope, element, attrs) {
        var fn = $parse(attrs.ngHapus);
        element.bind('contextmenu', function(event) {
            scope.$apply(function() {
                event.preventDefault();
                fn(scope, {$event:event});
            });
        });
    };
});

timetableApp.directive("fitScreen", function($timeout, $window){
    return function(scope, element, attrs){
        function setHeight(){
            var headerHeight = angular.element(document.querySelector(".navbar"))[0].offsetHeight,
                tabsHeight = angular.element(document.querySelector(".btn-group-justified"))[0].offsetHeight;
            
            element.css("height", ($window.innerHeight - (headerHeight + tabsHeight)) + "px");
        }
        
        setInterval(setHeight, 16);
    };
});

timetableApp.directive("centerPosition", function($window){
    return function(scope, element, attrs) {
        var wdw = angular.element($window);
        
        function tengah(){
            var atas = (wdw[0].innerHeight / 2) - (element[0].offsetHeight / 2),
                kiri = (wdw[0].innerWidth / 2) - (element[0].offsetWidth / 2);
                
            element.css({
                "position": "fixed",
                "top": atas + "px",
                "left": kiri + "px"
            });
        }
        
        tengah();        
        wdw.on("resize", tengah);
    };
});

timetableApp.directive('readFile', function ($parse) {
    return {
        restrict: 'A',
        scope: false,
        link: function(scope, element, attrs) {
            var fn = $parse(attrs.readFile);
            
            element.on('change', function(onChangeEvent) {
                var reader = new FileReader();
                
                reader.onload = function(onLoadEvent) {
                    scope.$apply(function() {
                        fn(scope, {$fileContent:onLoadEvent.target.result});
                    });
                };

                reader.readAsText((onChangeEvent.srcElement || onChangeEvent.target).files[0]);
            });
        }
    };
});